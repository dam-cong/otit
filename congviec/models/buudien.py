# -*- coding: utf-8 -*-
from odoo import models, fields, api


class PostOffice(models.Model):
    _name = 'post.office'
    _rec_name = 'post'

    post = fields.Char("Số bưu điện")
    kanji_address = fields.Char(string="Địa chỉ Kanji")
    furi_address = fields.Char(string="Địa chỉ Furi")
    english_address = fields.Char(string="Địa chỉ English")

class province(models.Model):
    _name = 'province'
    _rec_name = 'name'

    name = fields.Char("Tên có dấu")
    name_in_jp = fields.Char('Tên tiếng Nhật')

