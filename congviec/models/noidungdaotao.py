# -*- coding: utf-8 -*-
from odoo import models, fields, api


class loaikiemtra(models.Model):
    _name = 'daotaosau.noidungdaotao'
    _rec_name = 'noidungdaotao'

    noidungdaotao = fields.Char(string="Nội dung đào tạo",required=True)  #
    custom_id = fields.Char('Mã số')
    _sql_constraints = [('unique_id', 'UNIQUE(noidungdaotao)', "この内容は存在されました。")]

class loaikiemtraTruoc(models.Model):
    _name = 'daotaotruoc.noidungdaotao'
    _rec_name = 'noidungdaotao'

    noidungdaotao = fields.Char(string="Nội dung đào tạo")
    custom_id = fields.Char('Mã số')
    _sql_constraints = [('noidungdaotao_uniq', 'UNIQUE(noidungdaotao)', "Nội dung này đã tồn tại")]