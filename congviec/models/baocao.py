# -*- coding: utf-8 -*-
from odoo import http
from odoo import models, fields, api
import logging
from tempfile import TemporaryFile, NamedTemporaryFile
import zipfile
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception, content_disposition

_logger = logging.getLogger(__name__)
import os
from io import BytesIO, StringIO
import base64
import unicodedata
import logging

_logger = logging.getLogger(__name__)
import codecs
from docxtpl import DocxTemplate


class baocao(models.Model):
    _name = 'thuctapsinh.baocao'
    _rec_name = 'name'

    name = fields.Char('Tên tài liệu')
    file_name = fields.Char('Tên')
    data = fields.Binary()
