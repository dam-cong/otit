# -*- coding: utf-8 -*-
from odoo import models, fields, api


class loaikiemtra(models.Model):
    _name = 'thuctapsinh.loaikiemtra'
    _rec_name = 'loaikiemtra'

    loaikiemtra = fields.Char(string="Loại kiểm tra")  #


class KiThi(models.Model):
    _name = 'thuctapsinh.kithi'
    _rec_name = 'ten_kithi'

    ten_kithi = fields.Char(string="Tên kì thi")  #


class CapDo(models.Model):
    _name = 'thuctapsinh.capdo'
    _rec_name = 'ten_capdo'

    kiemtra_capdo = fields.Many2one(comodel_name="thuctapsinh.loaikiemtra", string='Kiểm tra')
    ten_capdo = fields.Char(string="Tên cấp độ")  #
