
# -*- coding: utf-8 -*-

from odoo import models, fields, api

class PhanNganhLon(models.Model):
    _name = 'phannganhlon.phannganhlon'
    _rec_name = 'name_phannganh'
    name_phannganh = fields.Char(string='Phân ngành lớn')

class PhanNganhTrungBinh(models.Model):
    _name = 'phannganhtb.phannganhtb'
    _rec_name = 'name_phannganhtb'
    name_phannganhtb = fields.Char(string='Phân ngành trung')
    phannnganhlon_phannganhtrung = fields.Many2one(comodel_name='phannganhlon.phannganhlon', string='Phân ngành lớn')

class PhanNganhNho(models.Model):
    _name = 'phannganhbe.phannganhbe'
    _rec_name = 'name_phannganhbe'
    phannnganhlon_phannganhbe = fields.Many2one(comodel_name='phannganhlon.phannganhlon', string='Phân ngành lớn')

    @api.onchange('phannnganhlon_phannganhbe')
    def onchange_master_phannganhlon_phannganhtrung(self):
        if self.phannnganhlon_phannganhbe:
            return {'domain': {'phannnganhtrung_phannganhbe': [('phannnganhlon_phannganhtrung', '=', self.phannnganhlon_phannganhbe.id)]}}

    phannnganhtrung_phannganhbe = fields.Many2one(comodel_name='phannganhtb.phannganhtb', string='Phân ngành trung')
    name_phannganhbe = fields.Char(string='Phân ngành bé')



