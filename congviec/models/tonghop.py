# -*- coding: utf-8 -*-

from odoo import models, fields, api


class CongViecNganhNghe(models.Model):
    _name = 'tonghop.tonghop'
    _rec_name = 'ma_tonghop'

    congtyphaicu = fields.Many2one(comodel_name='congtyphaicu.congtyphaicu', string='Công ty phải cử')
    xinghiep_tonghop = fields.Many2one(comodel_name='xinghiep.xinghiep')
    xinghiep = fields.Many2one(comodel_name='hoso.xinghiep', string='Công ty phải cử')
    nghiepvu = fields.Many2one(comodel_name='nghiepdoan.nghiepvu', string='Nghiệp vụ')


    ma_tonghop = fields.Char(string='Mã công việc')
    nganhnghe_tonghop = fields.Many2one(comodel_name='nganhnghe.nganhnghe', string='Ngành nghề')

    @api.onchange('nganhnghe_tonghop')
    def onchange_master_nganhnghe_tonghop(self):
        if self.nganhnghe_tonghop:
            return {'domain': {
                'loaicongviec_tonghop': [('nganhnghe_loaicongviec', '=', self.nganhnghe_tonghop.id)]}}

    loaicongviec_tonghop = fields.Many2one(comodel_name='loaicongviec.loaicongviec', string='Loại công việc')

    @api.onchange('loaicongviec_tonghop')
    def onchange_master_loaicongviec_tonghop(self):
        if self.loaicongviec_tonghop:
            return {'domain': {
                'congviec_tonghop': [('loaicongviec_congviec', '=', self.loaicongviec_tonghop.id)]}}

    congviec_tonghop = fields.Many2one(comodel_name='congviec.congviec', string='Công việc')

    @api.onchange('congviec_tonghop')
    def onchange_master_congviec_tonghop(self):
        if self.congviec_tonghop:
            self.ma_tonghop = self.congviec_tonghop.ma_congviec
