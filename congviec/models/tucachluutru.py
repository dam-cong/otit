# -*- coding: utf-8 -*-
import codecs
import datetime
import logging
from io import BytesIO
from tempfile import NamedTemporaryFile
from docxtpl import DocxTemplate, Listing
from odoo import models, fields, api


_logger = logging.getLogger(__name__)


class TuCach(models.Model):
    _name = 'tucach.luutru'
    _rec_name = 'tucach_luutru'

    tucach_luutru = fields.Char(string="Tư cách lưu trú")
    giaidoan_tucach = fields.Selection(string='Giai đoạn',
                                       selection=[('1 go', '1 go'),
                                                  ('2 go', '2 go'),
                                                  ('3 go', '3 go')])