# -*- coding: utf-8 -*-

from odoo import models, fields, api
import random
import string


class GiaiDoan(models.Model):
    _name = 'giaidoan.giaidoan'
    _rec_name = 'ten_giaidoan'

    ten_giaidoan = fields.Char(string='Tên giai đoạn')


class NganhNghe(models.Model):
    _name = 'nganhnghe.nganhnghe'
    _rec_name = 'name_nganhnghe'

    name_nganhnghe = fields.Char(string='Ngành nghề')
    name_nganhnghe_viet = fields.Char(string='Ngành nghề (Tiếng việt)')


class LoaiCongViec(models.Model):
    _name = 'loaicongviec.loaicongviec'
    _rec_name = 'name_loaicongviec'

    nganhnghe_loaicongviec = fields.Many2one(comodel_name='nganhnghe.nganhnghe', string='Ngành nghề')
    name_loaicongviec = fields.Char(string='Loại ngành nghề')
    name_loaicongviec_viet = fields.Char(string='Loại ngành nghề (Tiếng việt)')
    name_loaicongviec_anh = fields.Char(string='Loại ngành nghề (Tiếng anh)')


class CongViec(models.Model):
    _name = 'congviec.congviec'
    _rec_name = 'name_congviec'

    nganhnghe_congviec = fields.Many2one(comodel_name='nganhnghe.nganhnghe', string='Ngành nghề')

    @api.onchange('nganhnghe_congviec')
    def onchange_master_nganhnghe_congviec(self):
        if self.nganhnghe_congviec:
            return {'domain': {'loaicongviec_congviec': [('nganhnghe_loaicongviec', '=', self.nganhnghe_congviec.id)]}}
        print(self.nganhnghe_congviec.id)

    loaicongviec_congviec = fields.Many2one(comodel_name='loaicongviec.loaicongviec', string='Loại công việc')
    ma_congviec = fields.Char(string='Mã công việc')
    name_congviec = fields.Char(string='Công việc')
    name_congviec_viet = fields.Char(string='Công việc (Tiếng việt)')


class ChiTiet(models.Model):
    _name = 'congviec.chitietcongviec'
    _rec_name = 'name_chitiet_congviec'

    ma_chitiet_congviec = fields.Char(string='Mã công việc')
    nganhnghe_chitiet_congviec = fields.Many2one(comodel_name='nganhnghe.nganhnghe', string='Ngành nghề')

    @api.onchange('nganhnghe_chitiet_congviec')
    def onchange_master_nganhnghe_chitiet_congviec(self):
        if self.nganhnghe_chitiet_congviec:
            return {'domain': {
                'loainghe_chitiet_congviec': [('nganhnghe_loaicongviec', '=', self.nganhnghe_chitiet_congviec.id)]}}

    loainghe_chitiet_congviec = fields.Many2one(comodel_name='loaicongviec.loaicongviec', string='Loại công việc')

    @api.onchange('loainghe_chitiet_congviec')
    def onchange_master_loainghe_chitiet_congviec(self):
        if self.loainghe_chitiet_congviec:
            return {'domain': {
                'name_chitiet_congviec': [('loaicongviec_congviec', '=', self.loainghe_chitiet_congviec.id)]}}

    name_chitiet_congviec = fields.Many2one(comodel_name='congviec.congviec', string='Công việc')

    @api.onchange('name_chitiet_congviec')
    def onchange_master_name_chitiet_congviec(self):
        if self.name_chitiet_congviec:
            self.ma_chitiet_congviec = self.name_chitiet_congviec.ma_congviec

    giaidoan_chitiet_congviec = fields.Many2one(comodel_name='giaidoan.giaidoan', string='Giai đoạn')

    noidung_chitiet_congviec_mot = fields.Text(string='Nội dung công việc')
    noidung_chitiet_congviec_hai = fields.Text(string='Nội dung công việc')
    noidung_chitiet_congviec_ba = fields.Text(string='Nội dung công việc')
    noidung_chitiet_congviec_bon = fields.Text(string='Nội dung công việc')
    noidung_chitiet_congviec_nam = fields.Text(string='Nội dung công việc')
    noidung_chitiet_congviec_sau = fields.Text(string='Nội dung công việc')

    nguyenluyen_chitiet_congviec = fields.Text(string='Nguyên liệu công việc')
    congcu_maymoc_chitiet_congviec = fields.Text(string='Công cụ máy móc')
    sanpham_chitiet_congviec = fields.Text(string='Sản phẩm')
    hethong_chidao_chitiet_congviec = fields.Text(string='Hệ thống chỉ đạo')


class GiaiDoanMot(models.Model):
    _name = 'giaidoan.giaidoanmot'
    _rec_name = 'ten_giaidoanmot'

    ten_giaidoanmot = fields.Char('Tên giai đoạn')
    giaidoan_noidung = fields.Selection(string='Giai đoạn 1',
                                        selection=[('1 go', '1 go'),
                                                   ('2 go', '2 go'),
                                                   ('3 go', '3 go')], default='1 go')


class GiaiDoanHai(models.Model):
    _name = 'giaidoan.giaidoanhai'
    _rec_name = 'ten_giaidoanhai'

    ten_giaidoanhai = fields.Char('Tên giai đoạn')
    giaidoan_noidung = fields.Selection(string='Giai đoạn 1',
                                        selection=[('1 go', '1 go'),
                                                   ('2 go', '2 go'),
                                                   ('3 go', '3 go')], default='1 go')


class GiaiDoanBa(models.Model):
    _name = 'giaidoan.giaidoanba'
    _rec_name = 'ten_giaidoanba'

    ten_giaidoanba = fields.Char('Tên giai đoạn')
    giaidoan_noidung = fields.Selection(string='Giai đoạn 1',
                                        selection=[('1 go', '1 go'),
                                                   ('2 go', '2 go'),
                                                   ('3 go', '3 go')], default='1 go')


class NoiDung(models.Model):
    _name = 'noidung.noidung'
    _rec_name = 'ten_noidung'

    ma_random = fields.Char('Mã random')
    ten_noidung = fields.Char(string='Tên nội dung')
    tencha_noidung = fields.Many2one(comodel_name='noidung.noidung', string='Nội dung cha')
    giaidoan_noidung = fields.Selection(string='Giai đoạn 1',
                                        selection=[('1 go', '1 go'),
                                                   ('2 go', '2 go'),
                                                   ('3 go', '3 go')], default='1 go')

    giaidoan_211 = fields.Selection(string='Giai đoạn 2',
                                    selection=[('1号', '1号'),
                                               ('2号［1年目］', '2号［1年目］'),
                                               ('2号［2年目］', '2号［2年目］'),
                                               ('3号', '3号')], default='1号')

    giaidoan_221 = fields.Selection(string='Giai đoạn 3',
                                    selection=[('1号（①ほたてがい）', '1号（①ほたてがい）'),
                                               ('2号［1年目］（①ほたてがい）', '2号［1年目］（①ほたてがい）'),
                                               ('2号［2年目］（①ほたてがい）', '2号［2年目］（①ほたてがい）'),
                                               ('3号（①ほたてがい）', '3号（①ほたてがい）'),
                                               ('1号（②まがき）', '1号（②まがき）'),
                                               ('2号［1年目］（②まがき）', '2号［1年目］（②まがき）'),
                                               ('2号［2年目］（②まがき）', '2号［2年目］（②まがき）'),
                                               ('3号（②まがき）', '3号（②まがき）')], default='1号（①ほたてがい）')

    giaidoan_noidung_mot = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn một')

    @api.onchange('giaidoan_noidung')
    def _giaidoan_noidung_mot(self):
        docs1 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_noidung)], limit=1)
        docs2 = self.env['giaidoan.giaidoanhai'].search([('ten_giaidoanhai', '=', self.giaidoan_211)], limit=1)
        docs3 = self.env['giaidoan.giaidoanba'].search([('ten_giaidoanba', '=', self.giaidoan_221)], limit=1)

        self.giaidoan_noidung_mot = docs1.id
        self.giaidoan_211_hai = docs2.id
        self.giaidoan_221_ba = docs3.id

    giaidoan_211_hai = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn hai')

    giaidoan_221_ba = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn ba')


    @api.onchange('ten_noidung')
    def _ma_random(self):
        stringLength = 10
        if self.ten_noidung:
            letters = string.ascii_lowercase
            self.ma_random = ''.join(random.choice(letters) for i in range(stringLength))
        else:
            letters = string.ascii_lowercase
            self.ma_random = ''.join(random.choice(letters) for i in range(stringLength))

    ma_congviec_noidung = fields.Char(string='Mã công việc')

    nganhnghe_noidung = fields.Many2one(comodel_name='nganhnghe.nganhnghe', string='Ngành nghề')

    # @api.onchange('nganhnghe_noidung')
    # def onchange_master_nganhnghe(self):
    #     if self.nganhnghe_noidung:
    #         return {'domain': {
    #             'loaicongviec_noidung': [('nganhnghe_loaicongviec', '=', self.nganhnghe_noidung.id)]}}

    loaicongviec_noidung = fields.Many2one(comodel_name='loaicongviec.loaicongviec', string='Loại công việc')

    # @api.onchange('loaicongviec_noidung')
    # def onchange_master_loaicongviec(self):
    #     if self.loaicongviec_noidung:
    #         return {'domain': {
    #             'congviec_noidung': [('loaicongviec_congviec', '=', self.loaicongviec_noidung.id)]}}

    congviec_noidung = fields.Many2one(comodel_name='congviec.congviec', string='Công việc')

    @api.onchange('congviec_noidung')
    def onchange_method_congviec(self):
        self.ma_congviec_noidung = self.congviec_noidung.ma_congviec


class QuocTich(models.Model):
    _name = 'quoctich.quoctich'
    _rec_name = 'name_quoctich'

    name_quoctich = fields.Char('Quốc tịch')
    name_quoctich_viet = fields.Char('Quốc tịch (Tiếng việt)')


class Relation(models.Model):
    _name = 'relation'
    _rec_name = 'name'

    name = fields.Char('Quan hệ với TTS')
    name_jp = fields.Char('Quan hệ với TTS(Tiếng nhật)')

class Data(models.Model):
    _name = 'data.data'
    _rec_name = 'column_khong'

    giaidoan_junkai = fields.Selection(string='Giai đoạn',
                                       selection=[('1 go', '1 go'),
                                                  ('2 go', '2 go'),
                                                  ('3 go', '3 go')], required=True)

    ma_congviec = fields.Char('Ma Cong Viec')

    column_khong = fields.Char('Cot 0')
    column_mot = fields.Char('Cot 1')
    column_hai = fields.Char('Cot 2')
    column_ba = fields.Char('Cot 3')
    column_bon = fields.Char('Cot 4')
    column_nam = fields.Char('Cot 5')
