# -*- coding: utf-8 -*-
from odoo import models, fields, api

class library_jp(models.Model):
    _name = 'library.jp'
    _rec_name = 'name'

    name = fields.Char("Tên Tiếng việt")
    name_jp = fields.Char("Tên Japan")
