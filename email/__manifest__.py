# -*- coding: utf-8 -*-
{
    'name': "Email",
    'depends': ['base', 'mail', 'contacts'],
    'data': [
        'views/insert_cron.xml',
        'views/temp_email.xml',
        'views/thituyen.xml',
        'views/tiepkhach.xml',
        'views/dieuxe.xml',
    ],
}
