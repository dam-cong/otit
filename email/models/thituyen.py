# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime


def thu_tuan(self):
    ans = datetime.date(self.year, self.month, self.day)
    substr = ans.strftime("%A")
    if substr == 'Sunday':
        return 'Chủ nhật'
    elif substr == 'Monday':
        return 'Thứ 2'
    elif substr == 'Tuesday':
        return 'Thứ 3'
    elif substr == 'Wednesday':
        return 'Thứ 4'
    elif substr == 'Thursday':
        return 'Thứ 5'
    elif substr == 'Friday':
        return 'Thứ 6'
    elif substr == 'Saturday':
        return 'Thứ 7'
    else:
        return ''


class ThiTuyen(models.Model):
    _name = 'email.thituyen'
    _rec_name = 'day_start'

    day_start = fields.Date(string='Ngày bắt đầu')

    monday = fields.Char(compute='_day_monday')
    tuesday = fields.Char(compute='_day_monday')
    wednesday = fields.Char(compute='_day_monday')
    thursday = fields.Char(compute='_day_monday')
    friday = fields.Char(compute='_day_monday')
    saturday = fields.Char(compute='_day_monday')
    sunday = fields.Char(compute='_day_monday')

    day_monday = fields.Date(compute='_day_monday')
    day_tuesday = fields.Date(compute='_day_monday')
    day_wednesday = fields.Date(compute='_day_monday')
    day_thursday = fields.Date(compute='_day_monday')
    day_friday = fields.Date(compute='_day_monday')
    day_saturday = fields.Date(compute='_day_monday')
    day_sunday = fields.Date(compute='_day_monday')

    content_morning_monday = fields.Text()
    content_morning_tuesday = fields.Text()
    content_morning_wednesday = fields.Text()
    content_morning_thursday = fields.Text()
    content_morning_friday = fields.Text()
    content_morning_saturday = fields.Text()
    content_morning_sunday = fields.Text()

    content_afternoon_monday = fields.Text()
    content_afternoon_tuesday = fields.Text()
    content_afternoon_wednesday = fields.Text()
    content_afternoon_thursday = fields.Text()
    content_afternoon_friday = fields.Text()
    content_afternoon_saturday = fields.Text()
    content_afternoon_sunday = fields.Text()

    monday_1 = fields.Char(compute='_day_monday')
    tuesday_1 = fields.Char(compute='_day_monday')
    wednesday_1 = fields.Char(compute='_day_monday')
    thursday_1 = fields.Char(compute='_day_monday')
    friday_1 = fields.Char(compute='_day_monday')
    saturday_1 = fields.Char(compute='_day_monday')
    sunday_1 = fields.Char(compute='_day_monday')

    day_monday_1 = fields.Date(compute='_day_monday')
    day_tuesday_1 = fields.Date(compute='_day_monday')
    day_wednesday_1 = fields.Date(compute='_day_monday')
    day_thursday_1 = fields.Date(compute='_day_monday')
    day_friday_1 = fields.Date(compute='_day_monday')
    day_saturday_1 = fields.Date(compute='_day_monday')
    day_sunday_1 = fields.Date(compute='_day_monday')

    content_morning_monday_1 = fields.Text()
    content_morning_tuesday_1 = fields.Text()
    content_morning_wednesday_1 = fields.Text()
    content_morning_thursday_1 = fields.Text()
    content_morning_friday_1 = fields.Text()
    content_morning_saturday_1 = fields.Text()
    content_morning_sunday_1 = fields.Text()

    content_afternoon_monday_1 = fields.Text()
    content_afternoon_tuesday_1 = fields.Text()
    content_afternoon_wednesday_1 = fields.Text()
    content_afternoon_thursday_1 = fields.Text()
    content_afternoon_friday_1 = fields.Text()
    content_afternoon_saturday_1 = fields.Text()
    content_afternoon_sunday_1 = fields.Text()

    @api.multi
    def duplicate_thituyen(self):
        mass_mailing_copy = self.copy(default={
            'day_start': self.day_monday_1,
            'content_morning_monday': self.content_morning_monday_1,
            'content_morning_tuesday': self.content_morning_tuesday_1,
            'content_morning_wednesday': self.content_morning_wednesday_1,
            'content_morning_thursday': self.content_morning_thursday_1,
            'content_morning_friday': self.content_morning_friday_1,
            'content_morning_saturday': self.content_morning_saturday_1,

            'content_morning_sunday': self.content_morning_sunday_1,
            'content_afternoon_monday': self.content_afternoon_monday_1,
            'content_afternoon_tuesday': self.content_afternoon_tuesday_1,
            'content_afternoon_wednesday': self.content_afternoon_wednesday_1,
            'content_afternoon_thursday': self.content_afternoon_thursday_1,
            'content_afternoon_friday': self.content_afternoon_friday_1,
            'content_afternoon_saturday': self.content_afternoon_saturday_1,
            'content_afternoon_sunday': self.content_afternoon_sunday_1,

            'content_morning_monday_1': '',
            'content_morning_tuesday_1': '',
            'content_morning_wednesday_1': '',
            'content_morning_thursday_1': '',
            'content_morning_friday_1': '',
            'content_morning_saturday_1': '',
            'content_morning_sunday_1': '',

            'content_afternoon_monday_1': '',
            'content_afternoon_tuesday_1': '',
            'content_afternoon_wednesday_1': '',
            'content_afternoon_thursday_1': '',
            'content_afternoon_friday_1': '',
            'content_afternoon_saturday_1': '',
            'content_afternoon_sunday_1': '',

            'day_monday': None,
            'day_tuesday': None,
            'day_wednesday': None,
            'day_thursday': None,
            'day_friday': None,
            'day_saturday': None,
            'day_sunday': None,

            'day_monday_1': None,
            'day_tuesday_1': None,
            'day_wednesday_1': None,
            'day_thursday_1': None,
            'day_friday_1': None,
            'day_saturday_1': None,
            'day_sunday_1': None,

        })
        if mass_mailing_copy:
            context = dict(self.env.context)
            context['form_view_initial_mode'] = 'edit'
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'email.thituyen',
                'res_id': mass_mailing_copy.id,
                'context': context,
            }
        return False

    @api.one
    @api.depends('day_start')
    def _day_monday(self):
        if self.day_start:
            self.day_monday = self.day_start + datetime.timedelta(days=0)
            self.monday = thu_tuan(self.day_monday)
            self.day_tuesday = self.day_start + datetime.timedelta(days=1)
            self.tuesday = thu_tuan(self.day_tuesday)
            self.day_wednesday = self.day_start + datetime.timedelta(days=2)
            self.wednesday = thu_tuan(self.day_wednesday)
            self.day_thursday = self.day_start + datetime.timedelta(days=3)
            self.thursday = thu_tuan(self.day_thursday)
            self.day_friday = self.day_start + datetime.timedelta(days=4)
            self.friday = thu_tuan(self.day_friday)
            self.day_saturday = self.day_start + datetime.timedelta(days=5)
            self.saturday = thu_tuan(self.day_saturday)
            self.day_sunday = self.day_start + datetime.timedelta(days=6)
            self.sunday = thu_tuan(self.day_sunday)

            self.day_monday_1 = self.day_start + datetime.timedelta(days=7)
            self.monday_1 = thu_tuan(self.day_monday_1)
            self.day_tuesday_1 = self.day_start + datetime.timedelta(days=8)
            self.day_wednesday_1 = self.day_start + datetime.timedelta(days=9)
            self.day_thursday_1 = self.day_start + datetime.timedelta(days=10)
            self.day_friday_1 = self.day_start + datetime.timedelta(days=11)
            self.day_saturday_1 = self.day_start + datetime.timedelta(days=12)
            self.day_sunday_1 = self.day_start + datetime.timedelta(days=13)

            self.tuesday_1 = thu_tuan(self.day_tuesday_1)
            self.wednesday_1 = thu_tuan(self.day_wednesday_1)
            self.thursday_1 = thu_tuan(self.day_thursday_1)
            self.friday_1 = thu_tuan(self.day_friday_1)
            self.saturday_1 = thu_tuan(self.day_saturday_1)
            self.sunday_1 = thu_tuan(self.day_sunday_1)
        else:
            self.day_monday=None
            self.day_tuesday = None
            self.day_wednesday = None
            self.day_thursday = None
            self.day_friday = None
            self.day_saturday = None
            self.day_sunday = None

            self.day_monday_1 = None
            self.day_tuesday_1 = None
            self.day_wednesday_1 = None
            self.day_thursday_1 = None
            self.day_friday_1 = None
            self.day_saturday_1 = None
            self.day_sunday_1 = None

            self.monday = ''
            self.tuesday = ''
            self.wednesday = ''
            self.thursday = ''
            self.friday = ''
            self.saturday = ''
            self.sunday = ''

            self.monday_1 = ''
            self.tuesday_1 = ''
            self.wednesday_1 = ''
            self.thursday_1 = ''
            self.friday_1 = ''
            self.saturday_1 = ''
            self.sunday_1 = ''


