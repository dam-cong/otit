# -*- coding: utf-8 -*-

from odoo import models, fields, api
from docxtpl import DocxTemplate, Listing
import datetime


def thu_tuan(self):
    ans = datetime.date(self.year, self.month, self.day)
    substr = ans.strftime("%A")
    if substr == 'Sunday':
        return 'Chủ nhật'
    elif substr == 'Monday':
        return 'Thứ 2'
    elif substr == 'Tuesday':
        return 'Thứ 3'
    elif substr == 'Wednesday':
        return 'Thứ 4'
    elif substr == 'Thursday':
        return 'Thứ 5'
    elif substr == 'Friday':
        return 'Thứ 6'
    elif substr == 'Saturday':
        return 'Thứ 7'
    else:
        return ''


def ngay_tuan(self):
    if self:
        return str(self.day) + '/' + str(self.month)
    else:
        return ''


def kiemtra(data):
    if data:
        return data
    else:
        return ''


class email(models.Model):
    _name = 'email.email'

    # Mẫu 1: Thi tuyển
    @api.model
    def _cron_do_email(self):
        baudau = fields.date.today()
        demo = baudau + datetime.timedelta(days=-baudau.weekday(), weeks=0)
        ket_thuc = demo + datetime.timedelta(days=7)

        thituyen = self.env['email.thituyen'].search(
            [('day_start', '<', ket_thuc), ('day_start', '>=', demo)], limit=1)

        string = ''
        string += '<p><b><i>Kính gửi: </i></b>'
        string += 'Các phòng ban liên quan,<br/><br/>'
        string += 'Em xin gửi lịch thi tuyển và thị sát ngày ' + str(ngay_tuan(fields.Date.today())) + '.<br/><br/>'
        string += 'Trân trọng!<br/><br/>'
        string += '<table style="width:100%; border: 0px solid black; border-collapse: collapse" >'

        string += '<tr>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143); width:15%;">' + kiemtra(
            thituyen.monday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143); width:15%;">' + kiemtra(
            thituyen.tuesday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143); width:15%;">' + kiemtra(
            thituyen.wednesday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143); width:15%;">' + kiemtra(
            thituyen.thursday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143); width:15%;">' + kiemtra(
            thituyen.friday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143); width:15%;">' + kiemtra(
            thituyen.saturday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143); width:10%;">' + kiemtra(
            thituyen.sunday) + '</th>'
        string += '</tr>'

        string += '<tr>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + ngay_tuan(
            thituyen.day_monday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + ngay_tuan(
            thituyen.day_tuesday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + ngay_tuan(
            thituyen.day_wednesday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + ngay_tuan(
            thituyen.day_thursday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + ngay_tuan(
            thituyen.day_friday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + ngay_tuan(
            thituyen.day_saturday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + ngay_tuan(
            thituyen.day_sunday) + '</th>'
        string += '</tr>'

        string += '<tr>'
        # thuhai
        if thituyen.content_morning_monday and thituyen.content_afternoon_monday:
            string += '<td rowspan=2 style="font-size:10px;height:200px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + thituyen.content_morning_monday + '<br></br> 2/' + thituyen.content_afternoon_monday + '</td>'
        elif thituyen.content_morning_monday:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_morning_monday + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if thituyen.content_morning_tuesday and thituyen.content_afternoon_tuesday:
            string += '<td rowspan=2 style="font-size:10px;height:200px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + thituyen.content_morning_tuesday + '<br></br> 2/' + thituyen.content_afternoon_tuesday + '</td>'
        elif thituyen.content_morning_tuesday:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_morning_tuesday + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if thituyen.content_morning_wednesday and thituyen.content_afternoon_wednesday:
            string += '<td rowspan=2 style="font-size:10px;height:200px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + thituyen.content_morning_wednesday + '<br></br> 2/' + thituyen.content_afternoon_wednesday + '</td>'
        elif thituyen.content_morning_wednesday:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_morning_wednesday + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if thituyen.content_morning_thursday and thituyen.content_afternoon_thursday:
            string += '<td rowspan=2 style="font-size:10px;height:200px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + thituyen.content_morning_thursday + '<br></br> 2/' + thituyen.content_afternoon_thursday + '</td>'
        elif thituyen.content_morning_thursday:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_morning_thursday + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if thituyen.content_morning_friday and thituyen.content_afternoon_friday:
            string += '<td rowspan=2 style="font-size:10px;height:200px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + thituyen.content_morning_friday + '<br></br> 2/' + thituyen.content_afternoon_friday + '</td>'
        elif thituyen.content_morning_friday:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_morning_friday + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if thituyen.content_morning_saturday and thituyen.content_afternoon_saturday:
            string += '<td rowspan=2 style="font-size:10px;height:200px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + thituyen.content_morning_saturday + '<br></br> 2/' + thituyen.content_afternoon_saturday + '</td>'
        elif thituyen.content_morning_saturday:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_morning_saturday + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if thituyen.content_morning_sunday and thituyen.content_afternoon_sunday:
            string += '<td rowspan=2 style="font-size:10px;height:200px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + thituyen.content_morning_sunday + '<br></br> 2/' + thituyen.content_afternoon_sunday + '</td>'
        elif thituyen.content_morning_sunday:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_morning_sunday + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '<tr>'
        # thuhai
        if thituyen.content_morning_monday and thituyen.content_afternoon_monday:
            string += ''
        elif thituyen.content_afternoon_monday:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_afternoon_monday + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if thituyen.content_morning_tuesday and thituyen.content_afternoon_tuesday:
            string += ''
        elif thituyen.content_afternoon_tuesday:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_afternoon_tuesday + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if thituyen.content_morning_wednesday and thituyen.content_afternoon_wednesday:
            string += ''
        elif thituyen.content_afternoon_wednesday:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_afternoon_wednesday + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if thituyen.content_morning_thursday and thituyen.content_afternoon_thursday:
            string += ''
        elif thituyen.content_afternoon_thursday:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_afternoon_thursday + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if thituyen.content_morning_friday and thituyen.content_afternoon_friday:
            string += ''
        elif thituyen.content_afternoon_friday:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_afternoon_friday + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if thituyen.content_morning_saturday and thituyen.content_afternoon_saturday:
            string += ''
        elif thituyen.content_afternoon_saturday:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_afternoon_saturday + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if thituyen.content_morning_sunday and thituyen.content_afternoon_sunday:
            string += ''
        elif thituyen.content_afternoon_sunday:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_afternoon_sunday + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '<tr style="height:50px">'
        # string += '<td style="border: 1px solid gray;"></td>'
        # string += '<td style="border: 1px solid gray;"></td>'
        # string += '<td style="border: 1px solid gray;"></td>'
        # string += '<td style="border: 1px solid gray;"></td>'
        # string += '<td style="border: 1px solid gray;"></td>'
        # string += '<td style="border: 1px solid gray;"></td>'
        # string += '<td style="border: 1px solid gray;"></td>'
        string += '</tr>'

        string += '<tr>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143); width:10%;">' + kiemtra(
            thituyen.monday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143); width:10%;">' + kiemtra(
            thituyen.tuesday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143); width:10%;">' + kiemtra(
            thituyen.wednesday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143); width:10%;">' + kiemtra(
            thituyen.thursday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143); width:10%;">' + kiemtra(
            thituyen.friday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143); width:10%;">' + kiemtra(
            thituyen.saturday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143); width:10%;">' + kiemtra(
            thituyen.sunday_1) + '</th>'
        string += '</tr>'

        string += '<tr>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + ngay_tuan(
            thituyen.day_monday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + ngay_tuan(
            thituyen.day_tuesday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + ngay_tuan(
            thituyen.day_wednesday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + ngay_tuan(
            thituyen.day_thursday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + ngay_tuan(
            thituyen.day_friday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + ngay_tuan(
            thituyen.day_saturday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + ngay_tuan(
            thituyen.day_sunday_1) + '</th>'
        string += '</tr>'

        string += '<tr>'
        # thuhai
        if thituyen.content_morning_monday_1 and thituyen.content_afternoon_monday_1:
            string += '<td rowspan=2 style="font-size:10px;height:200px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + thituyen.content_morning_monday_1 + '<br></br> 2/' + thituyen.content_afternoon_monday_1 + '</td>'
        elif thituyen.content_morning_monday_1:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_morning_monday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if thituyen.content_morning_tuesday_1 and thituyen.content_afternoon_tuesday_1:
            string += '<td rowspan=2 style="font-size:10px;height:200px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + thituyen.content_morning_tuesday_1 + '<br></br> 2/' + thituyen.content_afternoon_tuesday_1 + '</td>'
        elif thituyen.content_morning_tuesday_1:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_morning_tuesday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if thituyen.content_morning_wednesday_1 and thituyen.content_afternoon_wednesday_1:
            string += '<td rowspan=2 style="font-size:10px;height:200px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + thituyen.content_morning_wednesday_1 + '<br></br> 2/' + thituyen.content_afternoon_wednesday_1 + '</td>'
        elif thituyen.content_morning_wednesday_1:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_morning_wednesday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if thituyen.content_morning_thursday_1 and thituyen.content_afternoon_thursday_1:
            string += '<td rowspan=2 style="font-size:10px;height:200px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + thituyen.content_morning_thursday_1 + '<br></br> 2/' + thituyen.content_afternoon_thursday_1 + '</td>'
        elif thituyen.content_morning_thursday_1:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_morning_thursday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if thituyen.content_morning_friday_1 and thituyen.content_afternoon_friday_1:
            string += '<td rowspan=2 style="font-size:10px;height:200px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + thituyen.content_morning_friday_1 + '<br/>' + '1/' + thituyen.content_afternoon_friday_1 + '</td>'
        elif thituyen.content_morning_friday_1:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_morning_friday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if thituyen.content_morning_saturday_1 and thituyen.content_afternoon_saturday_1:
            string += '<td rowspan=2 style="font-size:10px;height:200px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + thituyen.content_morning_saturday_1 + '<br></br> 2/' + thituyen.content_afternoon_saturday_1 + '</td>'
        elif thituyen.content_morning_saturday_1:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_morning_saturday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if thituyen.content_morning_sunday_1 and thituyen.content_afternoon_sunday_1:
            string += '<td rowspan=2 style="font-size:10px;height:200px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + thituyen.content_morning_sunday_1 + '<br></br> 2/' + thituyen.content_afternoon_sunday_1 + '</td>'
        elif thituyen.content_morning_sunday_1:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_morning_sunday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '<tr>'
        # thuhai
        if thituyen.content_morning_monday_1 and thituyen.content_afternoon_monday_1:
            string += ''
        elif thituyen.content_afternoon_monday_1:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_afternoon_monday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if thituyen.content_morning_tuesday_1 and thituyen.content_afternoon_tuesday_1:
            string += ''
        elif thituyen.content_afternoon_tuesday_1:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_afternoon_tuesday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if thituyen.content_morning_wednesday_1 and thituyen.content_afternoon_wednesday_1:
            string += ''
        elif thituyen.content_afternoon_wednesday_1:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_afternoon_wednesday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if thituyen.content_morning_thursday_1 and thituyen.content_afternoon_thursday_1:
            string += ''
        elif thituyen.content_afternoon_thursday_1:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_afternoon_thursday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if thituyen.content_morning_friday_1 and thituyen.content_afternoon_friday_1:
            string += ''
        elif thituyen.content_afternoon_friday_1:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_afternoon_friday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if thituyen.content_morning_saturday_1 and thituyen.content_afternoon_saturday_1:
            string += ''
        elif thituyen.content_afternoon_saturday_1:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_afternoon_saturday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if thituyen.content_morning_sunday_1 and thituyen.content_afternoon_sunday_1:
            string += ''
        elif thituyen.content_afternoon_sunday_1:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + thituyen.content_afternoon_sunday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:100px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '</table>'

        mail = self.env.ref('email.example_email_template')
        origin_mail = mail.body_html
        mail.body_html = origin_mail.replace('{data_body}', string)

        return origin_mail

    @api.model
    def cron_do_email(self):
        origin_mail = self._cron_do_email()
        template = self.env.ref('email.example_email_template')
        ketqua = template.send_mail(template.id, force_send=True)
        template.body_html = origin_mail
        print(ketqua)

    # Mẫu 2: Tiếp khách
    @api.model
    def _cron_do_email_2(self):
        bat_dau = fields.date.today()
        demo = bat_dau + datetime.timedelta(days=-bat_dau.weekday(), weeks=0)
        ket_thuc = demo + datetime.timedelta(days=7)

        tiepkhach = self.env['email.tiepkhach'].search(
            [('day_start', '<', ket_thuc), ('day_start', '>=', demo)], limit=1)

        string = ''
        string += '<p><b><i>Kính gửi: </i></b>'
        string += 'Tổng Giám Đốc,<br/><br/>'
        string += 'Em xin gửi lịch tiếp khách TGĐ ngày ' + str(ngay_tuan(fields.Date.today())) + '.<br/><br/>'
        string += 'Trân trọng!<br/><br/>'
        string += '<table style="width:100%; border: 0px solid black; border-collapse: collapse" >'

        string += '<tr>'
        string += '<th rowspan=2 style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;width:9%;"></th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + tiepkhach.monday + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + tiepkhach.tuesday + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + tiepkhach.wednesday + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + tiepkhach.thursday + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + tiepkhach.friday + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#0066cc;width:13%;">' + tiepkhach.saturday + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#0066cc;width:13%;">' + tiepkhach.sunday + '</th>'
        string += '</tr>'

        string += '<tr>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_monday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_tuesday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_wednesday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_thursday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_friday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_saturday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_sunday) + '</th>'
        string += '</tr>'

        string += '<tr>'
        string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#0066cc;">Sáng</td>'
        # thuhai
        if tiepkhach.content_morning_monday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_monday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if tiepkhach.content_morning_tuesday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_tuesday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if tiepkhach.content_morning_wednesday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_wednesday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if tiepkhach.content_morning_thursday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_thursday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if tiepkhach.content_morning_friday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_friday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if tiepkhach.content_morning_saturday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_saturday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if tiepkhach.content_morning_sunday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_sunday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '<tr>'
        string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#0066cc;">Chiều</td>'
        # thuhai
        if tiepkhach.content_afternoon_monday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_monday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if tiepkhach.content_afternoon_tuesday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_tuesday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if tiepkhach.content_afternoon_wednesday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_wednesday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if tiepkhach.content_afternoon_thursday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_thursday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if tiepkhach.content_afternoon_friday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_friday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if tiepkhach.content_afternoon_saturday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_saturday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if tiepkhach.content_afternoon_sunday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_sunday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '<tr>'
        string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#0066cc;">Tối</td>'
        # thuhai
        if tiepkhach.content_night_monday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_monday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if tiepkhach.content_night_tuesday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_tuesday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if tiepkhach.content_night_wednesday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_wednesday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if tiepkhach.content_night_thursday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_thursday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if tiepkhach.content_night_friday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_friday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if tiepkhach.content_night_saturday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_saturday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if tiepkhach.content_night_sunday:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_sunday + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '<tr style="height:25px">'
        string += '</tr>'

        string += '<tr>'
        string += '<th rowspan=2 style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + tiepkhach.monday_1 + '</th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + tiepkhach.tuesday_1 + '</th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + tiepkhach.wednesday_1 + '</th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + tiepkhach.thursday_1 + '</th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + tiepkhach.friday_1 + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#0066cc;width:10%;">' + tiepkhach.saturday_1 + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#0066cc;width:10%;">' + tiepkhach.sunday_1 + '</th>'
        string += '</tr>'

        string += '<tr>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_monday_1) + '</th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_tuesday_1) + '</th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_wednesday_1) + '</th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_thursday_1) + '</th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_friday_1) + '</th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_saturday_1) + '</th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_sunday_1) + '</th>'
        string += '</tr>'

        string += '<tr>'
        string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#0066cc;">Sáng</td>'
        # thuhai
        if tiepkhach.content_morning_monday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_monday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if tiepkhach.content_morning_tuesday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_tuesday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if tiepkhach.content_morning_wednesday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_wednesday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam

        if tiepkhach.content_morning_thursday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_thursday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if tiepkhach.content_morning_friday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_friday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if tiepkhach.content_morning_saturday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_saturday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if tiepkhach.content_morning_sunday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_sunday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '<tr>'
        string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#0066cc;">Chiều</td>'
        # thuhai
        if tiepkhach.content_afternoon_monday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_monday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if tiepkhach.content_afternoon_tuesday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_tuesday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if tiepkhach.content_afternoon_wednesday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_wednesday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if tiepkhach.content_afternoon_thursday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_thursday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if tiepkhach.content_afternoon_friday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_friday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if tiepkhach.content_afternoon_saturday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_saturday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if tiepkhach.content_afternoon_sunday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_sunday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '<tr>'
        string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#0066cc;">Tối</td>'
        # thuhai
        if tiepkhach.content_night_monday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_monday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if tiepkhach.content_night_tuesday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_tuesday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if tiepkhach.content_night_wednesday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_wednesday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if tiepkhach.content_night_thursday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_thursday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if tiepkhach.content_night_friday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_friday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if tiepkhach.content_night_saturday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_saturday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if tiepkhach.content_night_sunday_1:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_sunday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '<tr style="height:25px">'
        string += '</tr>'

        string += '<tr>'
        string += '<th rowspan=2 style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + tiepkhach.monday_2 + '</th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + tiepkhach.tuesday_2 + '</th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + tiepkhach.wednesday_2 + '</th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + tiepkhach.thursday_2 + '</th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + tiepkhach.friday_2 + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#0066cc;width:10%;">' + tiepkhach.saturday_2 + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#0066cc;width:10%;">' + tiepkhach.sunday_2 + '</th>'
        string += '</tr>'

        string += '<tr>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_monday_2) + '</th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_tuesday_2) + '</th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_wednesday_2) + '</th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_thursday_2) + '</th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_friday_2) + '</th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_saturday_2) + '</th>'
        string += ' <th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#99cc00; width:10%;">' + ngay_tuan(
            tiepkhach.day_sunday_2) + '</th>'
        string += '</tr>'

        string += '<tr>'
        string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#0066cc;">Sáng</td>'
        # thuhai
        if tiepkhach.content_morning_monday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_monday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if tiepkhach.content_morning_tuesday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_tuesday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if tiepkhach.content_morning_wednesday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_wednesday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if tiepkhach.content_morning_thursday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_thursday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if tiepkhach.content_morning_friday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_friday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if tiepkhach.content_morning_saturday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_saturday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if tiepkhach.content_morning_sunday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_morning_sunday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '<tr>'
        string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#0066cc;">Chiều</td>'
        # thuhai
        if tiepkhach.content_afternoon_monday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_monday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if tiepkhach.content_afternoon_tuesday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_tuesday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if tiepkhach.content_afternoon_wednesday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_wednesday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if tiepkhach.content_afternoon_thursday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_thursday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if tiepkhach.content_afternoon_friday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_friday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if tiepkhach.content_afternoon_saturday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_saturday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if tiepkhach.content_afternoon_sunday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_afternoon_sunday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '<tr>'
        string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#0066cc;">Tối</td>'
        # thuhai
        if tiepkhach.content_night_monday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_monday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if tiepkhach.content_night_tuesday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_tuesday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if tiepkhach.content_night_wednesday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_wednesday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if tiepkhach.content_night_thursday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_thursday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if tiepkhach.content_night_friday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_friday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if tiepkhach.content_night_saturday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_saturday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if tiepkhach.content_night_sunday_2:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:#ffff00;">' + tiepkhach.content_night_sunday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '</table>'

        mail = self.env.ref('email.example_email_template')
        origin_mail = mail.body_html
        mail.body_html = origin_mail.replace('{data_body}', string)

        return origin_mail

    @api.model
    def cron_do_email_2(self):
        origin_mail = self._cron_do_email_2()
        template = self.env.ref('email.example_email_template')
        ketqua = template.send_mail(template.id, force_send=True)
        template.body_html = origin_mail
        print(ketqua)

    # Mẫu 3: Điều xe
    @api.model
    def _cron_do_email_3(self):
        bat_dau = fields.date.today()
        demo = bat_dau + datetime.timedelta(days=-bat_dau.weekday(), weeks=0)
        ket_thuc = demo + datetime.timedelta(days=7)
        dieuxe = self.env['email.dieuxe'].search(
            [('day_start', '<', ket_thuc), ('day_start', '>=', demo)], limit=1)

        print(dieuxe)

        string = ''
        string += 'Em gửi lịch điều xe ngày ' + str(ngay_tuan(fields.Date.today())) + '.<br/><br/>'
        string += '<table style="width:100%; border: 0px solid black; border-collapse: collapse" >'

        string += '<tr>'
        string += '<th rowspan=2 style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(166,166,166);width:15%">Lái xe</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);width:6%;">' + kiemtra(
            dieuxe.monday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);width:6%;">' + kiemtra(
            dieuxe.tuesday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);width:6%;">' + kiemtra(
            dieuxe.wednesday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);width:6%;">' + kiemtra(
            dieuxe.thursday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);width:6%;">' + kiemtra(
            dieuxe.friday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);width:6%;">' + kiemtra(
            dieuxe.saturday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);width:6%;">' + kiemtra(
            dieuxe.sunday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);width:6%;">' + kiemtra(
            dieuxe.monday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);width:6%;">' + kiemtra(
            dieuxe.tuesday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);width:6%;">' + kiemtra(
            dieuxe.wednesday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);width:6%;">' + kiemtra(
            dieuxe.thursday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);width:6%;">' + kiemtra(
            dieuxe.friday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);width:6%;">' + kiemtra(
            dieuxe.saturday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);width:6%;">' + kiemtra(
            dieuxe.sunday_1) + '</th>'
        string += '</tr>'

        string += '<tr>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;">' + ngay_tuan(
            dieuxe.day_monday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;">' + ngay_tuan(
            dieuxe.day_tuesday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;">' + ngay_tuan(
            dieuxe.day_wednesday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;">' + ngay_tuan(
            dieuxe.day_thursday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;">' + ngay_tuan(
            dieuxe.day_friday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;">' + ngay_tuan(
            dieuxe.day_saturday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;">' + ngay_tuan(
            dieuxe.day_sunday) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;">' + ngay_tuan(
            dieuxe.day_monday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;">' + ngay_tuan(
            dieuxe.day_tuesday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;">' + ngay_tuan(
            dieuxe.day_wednesday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;">' + ngay_tuan(
            dieuxe.day_thursday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;">' + ngay_tuan(
            dieuxe.day_friday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;">' + ngay_tuan(
            dieuxe.day_saturday_1) + '</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;">' + ngay_tuan(
            dieuxe.day_sunday_1) + '</th>'
        string += '</tr>'

        string += '<tr>'
        string += '<td rowspan=2 style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(255,255,0);">' + kiemtra(
            dieuxe.driver) + '</td>'
        # thuhai
        if dieuxe.content_morning_monday and dieuxe.content_afternoon_monday:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + dieuxe.content_morning_monday + '<br></br> 2/' + dieuxe.content_afternoon_monday + '</td>'
        elif dieuxe.content_morning_monday:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_morning_monday + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if dieuxe.content_morning_tuesday and dieuxe.content_afternoon_tuesday:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + dieuxe.content_morning_tuesday + '<br></br> 2/' + dieuxe.content_afternoon_tuesday + '</td>'
        elif dieuxe.content_morning_tuesday:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_morning_tuesday + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if dieuxe.content_morning_wednesday and dieuxe.content_afternoon_wednesday:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + dieuxe.content_morning_wednesday + '<br></br> 2/' + dieuxe.content_afternoon_wednesday + '</td>'
        elif dieuxe.content_morning_wednesday:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_morning_wednesday + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if dieuxe.content_morning_thursday and dieuxe.content_afternoon_thursday:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + dieuxe.content_morning_thursday + '<br></br> 2/' + dieuxe.content_afternoon_thursday + '</td>'
        elif dieuxe.content_morning_thursday:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_morning_thursday + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if dieuxe.content_morning_friday and dieuxe.content_afternoon_friday:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + dieuxe.content_morning_friday + '<br></br> 2/' + dieuxe.content_afternoon_friday + '</td>'
        elif dieuxe.content_morning_friday:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_morning_friday + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if dieuxe.content_morning_saturday and dieuxe.content_afternoon_saturday:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + dieuxe.content_morning_saturday + '<br></br> 2/' + dieuxe.content_afternoon_saturday + '</td>'
        elif dieuxe.content_morning_saturday:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_morning_saturday + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if dieuxe.content_morning_sunday and dieuxe.content_afternoon_sunday:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + dieuxe.content_morning_sunday + '<br></br> 2/' + dieuxe.content_afternoon_sunday + '</td>'
        elif dieuxe.content_morning_sunday:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_morning_sunday + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'

        # thuhai
        if dieuxe.content_morning_monday_1 and dieuxe.content_afternoon_monday_1:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + dieuxe.content_morning_monday_1 + '<br></br> 2/' + dieuxe.content_afternoon_monday_1 + '</td>'
        elif dieuxe.content_morning_monday_1:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_morning_monday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if dieuxe.content_morning_tuesday_1 and dieuxe.content_afternoon_tuesday_1:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + dieuxe.content_morning_tuesday_1 + '<br></br> 2/' + dieuxe.content_afternoon_tuesday_1 + '</td>'
        elif dieuxe.content_morning_tuesday_1:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_morning_tuesday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if dieuxe.content_morning_wednesday_1 and dieuxe.content_afternoon_wednesday_1:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + dieuxe.content_morning_wednesday_1 + '<br></br> 2/' + dieuxe.content_afternoon_wednesday_1 + '</td>'
        elif dieuxe.content_morning_wednesday_1:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_morning_wednesday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if dieuxe.content_morning_thursday_1 and dieuxe.content_afternoon_thursday_1:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + dieuxe.content_morning_thursday_1 + '<br></br> 2/' + dieuxe.content_afternoon_thursday_1 + '</td>'
        elif dieuxe.content_morning_thursday_1:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_morning_thursday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if dieuxe.content_morning_friday_1 and dieuxe.content_afternoon_friday_1:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + dieuxe.content_morning_friday_1 + '<br></br> 2/' + dieuxe.content_afternoon_friday_1 + '</td>'
        elif dieuxe.content_morning_friday_1:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_morning_friday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if dieuxe.content_morning_saturday_1 and dieuxe.content_afternoon_saturday_1:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + dieuxe.content_morning_saturday_1 + '<br></br> 2/' + dieuxe.content_afternoon_saturday_1 + '</td>'
        elif dieuxe.content_morning_saturday_1:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_morning_saturday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if dieuxe.content_morning_sunday_1 and dieuxe.content_afternoon_sunday_1:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0);">' + '1/' + dieuxe.content_morning_sunday_1 + '<br></br> 2/' + dieuxe.content_afternoon_sunday_1 + '</td>'
        elif dieuxe.content_morning_sunday_1:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_morning_sunday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '<tr>'
        # thuhai
        if dieuxe.content_morning_monday and dieuxe.content_afternoon_monday:
            string += ''
        elif dieuxe.content_afternoon_monday:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_afternoon_monday + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if dieuxe.content_morning_tuesday and dieuxe.content_afternoon_tuesday:
            string += ''
        elif dieuxe.content_afternoon_tuesday:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_afternoon_tuesday + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if dieuxe.content_morning_wednesday and dieuxe.content_afternoon_wednesday:
            string += ''
        elif dieuxe.content_afternoon_wednesday:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_afternoon_wednesday + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if dieuxe.content_morning_thursday and dieuxe.content_afternoon_thursday:
            string += ''
        elif dieuxe.content_afternoon_thursday:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_afternoon_thursday + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if dieuxe.content_morning_friday and dieuxe.content_afternoon_friday:
            string += ''
        elif dieuxe.content_afternoon_friday:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_afternoon_friday + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if dieuxe.content_morning_saturday and dieuxe.content_afternoon_saturday:
            string += ''
        elif dieuxe.content_afternoon_saturday:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_afternoon_saturday + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if dieuxe.content_morning_sunday and dieuxe.content_afternoon_sunday:
            string += ''
        elif dieuxe.content_afternoon_sunday:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_afternoon_sunday + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuhai
        if dieuxe.content_morning_monday_1 and dieuxe.content_afternoon_monday_1:
            string += ''
        elif dieuxe.content_afternoon_monday_1:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_afternoon_monday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if dieuxe.content_morning_tuesday_1 and dieuxe.content_afternoon_tuesday_1:
            string += ''
        elif dieuxe.content_afternoon_tuesday_1:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_afternoon_tuesday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if dieuxe.content_morning_wednesday_1 and dieuxe.content_afternoon_wednesday_1:
            string += ''
        elif dieuxe.content_afternoon_wednesday_1:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_afternoon_wednesday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if dieuxe.content_morning_thursday_1 and dieuxe.content_afternoon_thursday_1:
            string += ''
        elif dieuxe.content_afternoon_thursday_1:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_afternoon_thursday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if dieuxe.content_morning_friday_1 and dieuxe.content_afternoon_friday_1:
            string += ''
        elif dieuxe.content_afternoon_friday_1:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_afternoon_friday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if dieuxe.content_morning_saturday_1 and dieuxe.content_afternoon_saturday_1:
            string += ''
        elif dieuxe.content_afternoon_saturday_1:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_afternoon_saturday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if dieuxe.content_morning_sunday_1 and dieuxe.content_afternoon_sunday_1:
            string += ''
        elif dieuxe.content_afternoon_sunday_1:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(255,255,0)">' + '1/' + dieuxe.content_afternoon_sunday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '<tr>'
        string += '<td rowspan=2 style="font-size:10px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198);">' + kiemtra(
            dieuxe.driver_2) + '</td>'
        # thuhai
        if dieuxe.content_morning_monday_2 and dieuxe.content_afternoon_monday_2:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(70,189,198);">' + '1/' + dieuxe.content_morning_monday_2 + '<br></br> 2/' + dieuxe.content_afternoon_monday_2 + '</td>'
        elif dieuxe.content_morning_monday_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_morning_monday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if dieuxe.content_morning_tuesday_2 and dieuxe.content_afternoon_tuesday_2:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(70,189,198);">' + '1/' + dieuxe.content_morning_tuesday_2 + '<br></br> 2/' + dieuxe.content_afternoon_tuesday_2 + '</td>'
        elif dieuxe.content_morning_tuesday_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_morning_tuesday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if dieuxe.content_morning_wednesday_2 and dieuxe.content_afternoon_wednesday_2:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(70,189,198);">' + '1/' + dieuxe.content_morning_wednesday_2 + '<br></br> 2/' + dieuxe.content_afternoon_wednesday_2 + '</td>'
        elif dieuxe.content_morning_wednesday_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_morning_wednesday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if dieuxe.content_morning_thursday_2 and dieuxe.content_afternoon_thursday_2:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(70,189,198);">' + '1/' + dieuxe.content_morning_thursday_2 + '<br></br> 2/' + dieuxe.content_afternoon_thursday_2 + '</td>'
        elif dieuxe.content_morning_thursday_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_morning_thursday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if dieuxe.content_morning_friday_2 and dieuxe.content_afternoon_friday_2:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(70,189,198);">' + '1/' + dieuxe.content_morning_friday_2 + '<br></br> 2/' + dieuxe.content_afternoon_friday_2 + '</td>'
        elif dieuxe.content_morning_friday_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_morning_friday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if dieuxe.content_morning_saturday_2 and dieuxe.content_afternoon_saturday_2:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(70,189,198);">' + '1/' + dieuxe.content_morning_saturday_2 + '<br></br> 2/' + dieuxe.content_afternoon_saturday_2 + '</td>'
        elif dieuxe.content_morning_saturday_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_morning_saturday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if dieuxe.content_morning_sunday_2 and dieuxe.content_afternoon_sunday_2:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(70,189,198);">' + '1/' + dieuxe.content_morning_sunday_2 + '<br></br> 2/' + dieuxe.content_afternoon_sunday_2 + '</td>'
        elif dieuxe.content_morning_sunday_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_morning_sunday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'

        # thuhai
        if dieuxe.content_morning_monday_1_2 and dieuxe.content_afternoon_monday_1_2:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(70,189,198);">' + '1/' + dieuxe.content_morning_monday_1_2 + '<br></br> 2/' + dieuxe.content_afternoon_monday_1_2 + '</td>'
        elif dieuxe.content_morning_monday_1_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_morning_monday_1_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if dieuxe.content_morning_tuesday_1_2 and dieuxe.content_afternoon_tuesday_1_2:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(70,189,198);">' + '1/' + dieuxe.content_morning_tuesday_1_2 + '<br></br> 2/' + dieuxe.content_afternoon_tuesday_1_2 + '</td>'
        elif dieuxe.content_morning_tuesday_1_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_morning_tuesday_1_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if dieuxe.content_morning_wednesday_1_2 and dieuxe.content_afternoon_wednesday_1_2:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(70,189,198);">' + '1/' + dieuxe.content_morning_wednesday_1_2 + '<br></br> 2/' + dieuxe.content_afternoon_wednesday_1_2 + '</td>'
        elif dieuxe.content_morning_wednesday_1_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_morning_wednesday_1_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if dieuxe.content_morning_thursday_1_2 and dieuxe.content_afternoon_thursday_1_2:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(70,189,198);">' + '1/' + dieuxe.content_morning_thursday_1_2 + '<br></br> 2/' + dieuxe.content_afternoon_thursday_1_2 + '</td>'
        elif dieuxe.content_morning_thursday_1_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_morning_thursday_1_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if dieuxe.content_morning_friday_1 and dieuxe.content_afternoon_friday_1:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(70,189,198);">' + '1/' + dieuxe.content_morning_friday_1 + '<br></br> 2/' + dieuxe.content_afternoon_friday_1 + '</td>'
        elif dieuxe.content_morning_friday_1:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_morning_friday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if dieuxe.content_morning_saturday_1_2 and dieuxe.content_afternoon_saturday_1_2:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(70,189,198);">' + '1/' + dieuxe.content_morning_saturday_1_2 + '<br></br> 2/' + dieuxe.content_afternoon_saturday_1_2 + '</td>'
        elif dieuxe.content_morning_saturday_1_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_morning_saturday_1_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if dieuxe.content_morning_sunday_1_2 and dieuxe.content_afternoon_sunday_1_2:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(70,189,198);">' + '1/' + dieuxe.content_morning_sunday_1_2 + '<br></br> 2/' + dieuxe.content_afternoon_sunday_1_2 + '</td>'
        elif dieuxe.content_morning_sunday_1_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_morning_sunday_1_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '<tr>'
        # thuhai
        if dieuxe.content_morning_monday_2 and dieuxe.content_afternoon_monday_2:
            string += ''
        elif dieuxe.content_afternoon_monday_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_afternoon_monday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if dieuxe.content_morning_tuesday_2 and dieuxe.content_afternoon_tuesday_2:
            string += ''
        elif dieuxe.content_afternoon_tuesday_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_afternoon_tuesday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if dieuxe.content_morning_wednesday_2 and dieuxe.content_afternoon_wednesday_2:
            string += ''
        elif dieuxe.content_afternoon_wednesday_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_afternoon_wednesday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if dieuxe.content_morning_thursday_2 and dieuxe.content_afternoon_thursday_2:
            string += ''
        elif dieuxe.content_afternoon_thursday_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_afternoon_thursday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if dieuxe.content_morning_friday_2 and dieuxe.content_afternoon_friday_2:
            string += ''
        elif dieuxe.content_afternoon_friday_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_afternoon_friday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if dieuxe.content_morning_saturday_2 and dieuxe.content_afternoon_saturday_2:
            string += ''
        elif dieuxe.content_afternoon_saturday_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_afternoon_saturday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if dieuxe.content_morning_sunday_2 and dieuxe.content_afternoon_sunday_2:
            string += ''
        elif dieuxe.content_afternoon_sunday_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_afternoon_sunday_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuhai
        if dieuxe.content_morning_monday_1_2 and dieuxe.content_afternoon_monday_1_2:
            string += ''
        elif dieuxe.content_afternoon_monday_1_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_afternoon_monday_1_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if dieuxe.content_morning_tuesday_1_2 and dieuxe.content_afternoon_tuesday_1_2:
            string += ''
        elif dieuxe.content_afternoon_tuesday_1_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_afternoon_tuesday_1_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if dieuxe.content_morning_wednesday_1_2 and dieuxe.content_afternoon_wednesday_1_2:
            string += ''
        elif dieuxe.content_afternoon_wednesday_1_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_afternoon_wednesday_1_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if dieuxe.content_morning_thursday_1_2 and dieuxe.content_afternoon_thursday_1_2:
            string += ''
        elif dieuxe.content_afternoon_thursday_1_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_afternoon_thursday_1_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if dieuxe.content_morning_friday_1_2 and dieuxe.content_afternoon_friday_1_2:
            string += ''
        elif dieuxe.content_afternoon_friday_1_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_afternoon_friday_1_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if dieuxe.content_morning_saturday_1_2 and dieuxe.content_afternoon_saturday_1_2:
            string += ''
        elif dieuxe.content_afternoon_saturday_1_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_afternoon_saturday_1_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if dieuxe.content_morning_sunday_1_2 and dieuxe.content_afternoon_sunday_1_2:
            string += ''
        elif dieuxe.content_afternoon_sunday_1_2:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(70,189,198)">' + '1/' + dieuxe.content_afternoon_sunday_1_2 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '<tr>'
        string += '<td rowspan=2 style="font-size:10px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + kiemtra(
            dieuxe.driver_3) + '</td>'
        # thuhai
        if dieuxe.content_morning_monday_3 and dieuxe.content_afternoon_monday_3:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + '1/' + dieuxe.content_morning_monday_3 + '<br></br> 2/' + dieuxe.content_afternoon_monday_3 + '</td>'
        elif dieuxe.content_morning_monday_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_morning_monday_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if dieuxe.content_morning_tuesday_3 and dieuxe.content_afternoon_tuesday_3:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + '1/' + dieuxe.content_morning_tuesday_3 + '<br></br> 2/' + dieuxe.content_afternoon_tuesday_3 + '</td>'
        elif dieuxe.content_morning_tuesday_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_morning_tuesday_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if dieuxe.content_morning_wednesday_3 and dieuxe.content_afternoon_wednesday_3:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + '1/' + dieuxe.content_morning_wednesday_3 + '<br></br> 2/' + dieuxe.content_afternoon_wednesday_3 + '</td>'
        elif dieuxe.content_morning_wednesday_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_morning_wednesday_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if dieuxe.content_morning_thursday_3 and dieuxe.content_afternoon_thursday_3:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + '1/' + dieuxe.content_morning_thursday_3 + '<br></br> 2/' + dieuxe.content_afternoon_thursday_3 + '</td>'
        elif dieuxe.content_morning_thursday_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_morning_thursday_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if dieuxe.content_morning_friday_3 and dieuxe.content_afternoon_friday_3:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + '1/' + dieuxe.content_morning_friday_3 + '<br></br> 2/' + dieuxe.content_afternoon_friday_3 + '</td>'
        elif dieuxe.content_morning_friday_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_morning_friday_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if dieuxe.content_morning_saturday_3 and dieuxe.content_afternoon_saturday_3:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + '1/' + dieuxe.content_morning_saturday_3 + '<br></br> 2/' + dieuxe.content_afternoon_saturday_3 + '</td>'
        elif dieuxe.content_morning_saturday_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_morning_saturday_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if dieuxe.content_morning_sunday_3 and dieuxe.content_afternoon_sunday_3:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + '1/' + dieuxe.content_morning_sunday_3 + '<br></br> 2/' + dieuxe.content_afternoon_sunday_3 + '</td>'
        elif dieuxe.content_morning_sunday_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_morning_sunday_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'

        # thuhai
        if dieuxe.content_morning_monday_1_3 and dieuxe.content_afternoon_monday_1_3:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + '1/' + dieuxe.content_morning_monday_1_3 + '<br></br> 2/' + dieuxe.content_afternoon_monday_1_3 + '</td>'
        elif dieuxe.content_morning_monday_1_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_morning_monday_1_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if dieuxe.content_morning_tuesday_1_3 and dieuxe.content_afternoon_tuesday_1_3:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + '1/' + dieuxe.content_morning_tuesday_1_3 + '<br></br> 2/' + dieuxe.content_afternoon_tuesday_1_3 + '</td>'
        elif dieuxe.content_morning_tuesday_1_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_morning_tuesday_1_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if dieuxe.content_morning_wednesday_1_3 and dieuxe.content_afternoon_wednesday_1_3:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + '1/' + dieuxe.content_morning_wednesday_1_3 + '<br></br> 2/' + dieuxe.content_afternoon_wednesday_1_3 + '</td>'
        elif dieuxe.content_morning_wednesday_1_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_morning_wednesday_1_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if dieuxe.content_morning_thursday_1_3 and dieuxe.content_afternoon_thursday_1_3:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + '1/' + dieuxe.content_morning_thursday_1_3 + '<br></br> 2/' + dieuxe.content_afternoon_thursday_1_3 + '</td>'
        elif dieuxe.content_morning_thursday_1_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_morning_thursday_1_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if dieuxe.content_morning_friday_1 and dieuxe.content_afternoon_friday_1:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + '1/' + dieuxe.content_morning_friday_1 + '<br></br> 2/' + dieuxe.content_afternoon_friday_1 + '</td>'
        elif dieuxe.content_morning_friday_1:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_morning_friday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if dieuxe.content_morning_saturday_1_3 and dieuxe.content_afternoon_saturday_1_3:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + '1/' + dieuxe.content_morning_saturday_1_3 + '<br></br> 2/' + dieuxe.content_afternoon_saturday_1_3 + '</td>'
        elif dieuxe.content_morning_saturday_1_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_morning_saturday_1_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if dieuxe.content_morning_sunday_1_3 and dieuxe.content_afternoon_sunday_1_3:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(250,191,143);">' + '1/' + dieuxe.content_morning_sunday_1_3 + '<br></br> 2/' + dieuxe.content_afternoon_sunday_1_3 + '</td>'
        elif dieuxe.content_morning_sunday_1_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_morning_sunday_1_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '<tr>'
        # thuhai
        if dieuxe.content_morning_monday_3 and dieuxe.content_afternoon_monday_3:
            string += ''
        elif dieuxe.content_afternoon_monday_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_afternoon_monday_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if dieuxe.content_morning_tuesday_3 and dieuxe.content_afternoon_tuesday_3:
            string += ''
        elif dieuxe.content_afternoon_tuesday_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_afternoon_tuesday_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if dieuxe.content_morning_wednesday_3 and dieuxe.content_afternoon_wednesday_3:
            string += ''
        elif dieuxe.content_afternoon_wednesday_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_afternoon_wednesday_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if dieuxe.content_morning_thursday_3 and dieuxe.content_afternoon_thursday_3:
            string += ''
        elif dieuxe.content_afternoon_thursday_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_afternoon_thursday_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if dieuxe.content_morning_friday_3 and dieuxe.content_afternoon_friday_3:
            string += ''
        elif dieuxe.content_afternoon_friday_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_afternoon_friday_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if dieuxe.content_morning_saturday_3 and dieuxe.content_afternoon_saturday_3:
            string += ''
        elif dieuxe.content_afternoon_saturday_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_afternoon_saturday_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if dieuxe.content_morning_sunday_3 and dieuxe.content_afternoon_sunday_3:
            string += ''
        elif dieuxe.content_afternoon_sunday_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_afternoon_sunday_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuhai
        if dieuxe.content_morning_monday_1_3 and dieuxe.content_afternoon_monday_1_3:
            string += ''
        elif dieuxe.content_afternoon_monday_1_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_afternoon_monday_1_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if dieuxe.content_morning_tuesday_1_3 and dieuxe.content_afternoon_tuesday_1_3:
            string += ''
        elif dieuxe.content_afternoon_tuesday_1_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_afternoon_tuesday_1_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if dieuxe.content_morning_wednesday_1_3 and dieuxe.content_afternoon_wednesday_1_3:
            string += ''
        elif dieuxe.content_afternoon_wednesday_1_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_afternoon_wednesday_1_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if dieuxe.content_morning_thursday_1_3 and dieuxe.content_afternoon_thursday_1_3:
            string += ''
        elif dieuxe.content_afternoon_thursday_1_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_afternoon_thursday_1_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if dieuxe.content_morning_friday_1_3 and dieuxe.content_afternoon_friday_1_3:
            string += ''
        elif dieuxe.content_afternoon_friday_1_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_afternoon_friday_1_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if dieuxe.content_morning_saturday_1_3 and dieuxe.content_afternoon_saturday_1_3:
            string += ''
        elif dieuxe.content_afternoon_saturday_1_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_afternoon_saturday_1_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if dieuxe.content_morning_sunday_1_3 and dieuxe.content_afternoon_sunday_1_3:
            string += ''
        elif dieuxe.content_afternoon_sunday_1_3:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(250,191,143)">' + '1/' + dieuxe.content_afternoon_sunday_1_3 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '<tr>'
        string += '<td rowspan=2 style="font-size:10px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(162,196,201);">' + kiemtra(
            dieuxe.driver_4) + '</td>'
        # thuhai
        if dieuxe.content_morning_monday_4 and dieuxe.content_afternoon_monday_4:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(162,196,201);">' + '1/' + dieuxe.content_morning_monday_4 + '<br></br> 2/' + dieuxe.content_afternoon_monday_4 + '</td>'
        elif dieuxe.content_morning_monday_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_morning_monday_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if dieuxe.content_morning_tuesday_4 and dieuxe.content_afternoon_tuesday_4:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(162,196,201);">' + '1/' + dieuxe.content_morning_tuesday_4 + '<br></br> 2/' + dieuxe.content_afternoon_tuesday_4 + '</td>'
        elif dieuxe.content_morning_tuesday_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_morning_tuesday_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if dieuxe.content_morning_wednesday_4 and dieuxe.content_afternoon_wednesday_4:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(162,196,201);">' + '1/' + dieuxe.content_morning_wednesday_4 + '<br></br> 2/' + dieuxe.content_afternoon_wednesday_4 + '</td>'
        elif dieuxe.content_morning_wednesday_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_morning_wednesday_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if dieuxe.content_morning_thursday_4 and dieuxe.content_afternoon_thursday_4:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(162,196,201);">' + '1/' + dieuxe.content_morning_thursday_4 + '<br></br> 2/' + dieuxe.content_afternoon_thursday_4 + '</td>'
        elif dieuxe.content_morning_thursday_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_morning_thursday_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if dieuxe.content_morning_friday_4 and dieuxe.content_afternoon_friday_4:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(162,196,201);">' + '1/' + dieuxe.content_morning_friday_4 + '<br></br> 2/' + dieuxe.content_afternoon_friday_4 + '</td>'
        elif dieuxe.content_morning_friday_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_morning_friday_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if dieuxe.content_morning_saturday_4 and dieuxe.content_afternoon_saturday_4:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(162,196,201);">' + '1/' + dieuxe.content_morning_saturday_4 + '<br></br> 2/' + dieuxe.content_afternoon_saturday_4 + '</td>'
        elif dieuxe.content_morning_saturday_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_morning_saturday_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if dieuxe.content_morning_sunday_4 and dieuxe.content_afternoon_sunday_4:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(162,196,201);">' + '1/' + dieuxe.content_morning_sunday_4 + '<br></br> 2/' + dieuxe.content_afternoon_sunday_4 + '</td>'
        elif dieuxe.content_morning_sunday_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_morning_sunday_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'

        # thuhai
        if dieuxe.content_morning_monday_1_4 and dieuxe.content_afternoon_monday_1_4:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(162,196,201);">' + '1/' + dieuxe.content_morning_monday_1_4 + '<br></br> 2/' + dieuxe.content_afternoon_monday_1_4 + '</td>'
        elif dieuxe.content_morning_monday_1_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_morning_monday_1_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if dieuxe.content_morning_tuesday_1_4 and dieuxe.content_afternoon_tuesday_1_4:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(162,196,201);">' + '1/' + dieuxe.content_morning_tuesday_1_4 + '<br></br> 2/' + dieuxe.content_afternoon_tuesday_1_4 + '</td>'
        elif dieuxe.content_morning_tuesday_1_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_morning_tuesday_1_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if dieuxe.content_morning_wednesday_1_4 and dieuxe.content_afternoon_wednesday_1_4:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(162,196,201);">' + '1/' + dieuxe.content_morning_wednesday_1_4 + '<br></br> 2/' + dieuxe.content_afternoon_wednesday_1_4 + '</td>'
        elif dieuxe.content_morning_wednesday_1_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_morning_wednesday_1_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if dieuxe.content_morning_thursday_1_4 and dieuxe.content_afternoon_thursday_1_4:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(162,196,201);">' + '1/' + dieuxe.content_morning_thursday_1_4 + '<br></br> 2/' + dieuxe.content_afternoon_thursday_1_4 + '</td>'
        elif dieuxe.content_morning_thursday_1_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_morning_thursday_1_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if dieuxe.content_morning_friday_1 and dieuxe.content_afternoon_friday_1:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(162,196,201);">' + '1/' + dieuxe.content_morning_friday_1 + '<br></br> 2/' + dieuxe.content_afternoon_friday_1 + '</td>'
        elif dieuxe.content_morning_friday_1:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_morning_friday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if dieuxe.content_morning_saturday_1_4 and dieuxe.content_afternoon_saturday_1_4:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(162,196,201);">' + '1/' + dieuxe.content_morning_saturday_1_4 + '<br></br> 2/' + dieuxe.content_afternoon_saturday_1_4 + '</td>'
        elif dieuxe.content_morning_saturday_1_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_morning_saturday_1_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if dieuxe.content_morning_sunday_1_4 and dieuxe.content_afternoon_sunday_1_4:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:rgb(162,196,201);">' + '1/' + dieuxe.content_morning_sunday_1_4 + '<br></br> 2/' + dieuxe.content_afternoon_sunday_1_4 + '</td>'
        elif dieuxe.content_morning_sunday_1_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_morning_sunday_1_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '<tr>'
        # thuhai
        if dieuxe.content_morning_monday_4 and dieuxe.content_afternoon_monday_4:
            string += ''
        elif dieuxe.content_afternoon_monday_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_afternoon_monday_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if dieuxe.content_morning_tuesday_4 and dieuxe.content_afternoon_tuesday_4:
            string += ''
        elif dieuxe.content_afternoon_tuesday_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_afternoon_tuesday_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if dieuxe.content_morning_wednesday_4 and dieuxe.content_afternoon_wednesday_4:
            string += ''
        elif dieuxe.content_afternoon_wednesday_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_afternoon_wednesday_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if dieuxe.content_morning_thursday_4 and dieuxe.content_afternoon_thursday_4:
            string += ''
        elif dieuxe.content_afternoon_thursday_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_afternoon_thursday_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if dieuxe.content_morning_friday_4 and dieuxe.content_afternoon_friday_4:
            string += ''
        elif dieuxe.content_afternoon_friday_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_afternoon_friday_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if dieuxe.content_morning_saturday_4 and dieuxe.content_afternoon_saturday_4:
            string += ''
        elif dieuxe.content_afternoon_saturday_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_afternoon_saturday_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if dieuxe.content_morning_sunday_4 and dieuxe.content_afternoon_sunday_4:
            string += ''
        elif dieuxe.content_afternoon_sunday_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_afternoon_sunday_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuhai
        if dieuxe.content_morning_monday_1_4 and dieuxe.content_afternoon_monday_1_4:
            string += ''
        elif dieuxe.content_afternoon_monday_1_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_afternoon_monday_1_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if dieuxe.content_morning_tuesday_1_4 and dieuxe.content_afternoon_tuesday_1_4:
            string += ''
        elif dieuxe.content_afternoon_tuesday_1_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_afternoon_tuesday_1_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if dieuxe.content_morning_wednesday_1_4 and dieuxe.content_afternoon_wednesday_1_4:
            string += ''
        elif dieuxe.content_afternoon_wednesday_1_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_afternoon_wednesday_1_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if dieuxe.content_morning_thursday_1_4 and dieuxe.content_afternoon_thursday_1_4:
            string += ''
        elif dieuxe.content_afternoon_thursday_1_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_afternoon_thursday_1_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if dieuxe.content_morning_friday_1_4 and dieuxe.content_afternoon_friday_1_4:
            string += ''
        elif dieuxe.content_afternoon_friday_1_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_afternoon_friday_1_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if dieuxe.content_morning_saturday_1_4 and dieuxe.content_afternoon_saturday_1_4:
            string += ''
        elif dieuxe.content_afternoon_saturday_1_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_afternoon_saturday_1_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if dieuxe.content_morning_sunday_1_4 and dieuxe.content_afternoon_sunday_1_4:
            string += ''
        elif dieuxe.content_afternoon_sunday_1_4:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(162,196,201)">' + '1/' + dieuxe.content_afternoon_sunday_1_4 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '<tr>'
        string += '  <td rowspan=2 style="font-size:10px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80);">' + kiemtra(
            dieuxe.driver_5) + '</td>'
        # thuhai
        if dieuxe.content_morning_monday_5 and dieuxe.content_afternoon_monday_5:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(0,176,80);">' + '1/' + dieuxe.content_morning_monday_5 + '<br></br> 2/' + dieuxe.content_afternoon_monday_5 + '</td>'
        elif dieuxe.content_morning_monday_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_morning_monday_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if dieuxe.content_morning_tuesday_5 and dieuxe.content_afternoon_tuesday_5:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(0,176,80);">' + '1/' + dieuxe.content_morning_tuesday_5 + '<br></br> 2/' + dieuxe.content_afternoon_tuesday_5 + '</td>'
        elif dieuxe.content_morning_tuesday_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_morning_tuesday_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if dieuxe.content_morning_wednesday_5 and dieuxe.content_afternoon_wednesday_5:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(0,176,80);">' + '1/' + dieuxe.content_morning_wednesday_5 + '<br></br> 2/' + dieuxe.content_afternoon_wednesday_5 + '</td>'
        elif dieuxe.content_morning_wednesday_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_morning_wednesday_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if dieuxe.content_morning_thursday_5 and dieuxe.content_afternoon_thursday_5:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(0,176,80);">' + '1/' + dieuxe.content_morning_thursday_5 + '<br></br> 2/' + dieuxe.content_afternoon_thursday_5 + '</td>'
        elif dieuxe.content_morning_thursday_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_morning_thursday_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if dieuxe.content_morning_friday_5 and dieuxe.content_afternoon_friday_5:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(0,176,80);">' + '1/' + dieuxe.content_morning_friday_5 + '<br></br> 2/' + dieuxe.content_afternoon_friday_5 + '</td>'
        elif dieuxe.content_morning_friday_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_morning_friday_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if dieuxe.content_morning_saturday_5 and dieuxe.content_afternoon_saturday_5:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(0,176,80);">' + '1/' + dieuxe.content_morning_saturday_5 + '<br></br> 2/' + dieuxe.content_afternoon_saturday_5 + '</td>'
        elif dieuxe.content_morning_saturday_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_morning_saturday_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if dieuxe.content_morning_sunday_5 and dieuxe.content_afternoon_sunday_5:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(0,176,80);">' + '1/' + dieuxe.content_morning_sunday_5 + '<br></br> 2/' + dieuxe.content_afternoon_sunday_5 + '</td>'
        elif dieuxe.content_morning_sunday_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_morning_sunday_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'

        # thuhai
        if dieuxe.content_morning_monday_1_5 and dieuxe.content_afternoon_monday_1_5:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(0,176,80);">' + '1/' + dieuxe.content_morning_monday_1_5 + '<br></br> 2/' + dieuxe.content_afternoon_monday_1_5 + '</td>'
        elif dieuxe.content_morning_monday_1_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_morning_monday_1_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if dieuxe.content_morning_tuesday_1_5 and dieuxe.content_afternoon_tuesday_1_5:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(0,176,80);">' + '1/' + dieuxe.content_morning_tuesday_1_5 + '<br></br> 2/' + dieuxe.content_afternoon_tuesday_1_5 + '</td>'
        elif dieuxe.content_morning_tuesday_1_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_morning_tuesday_1_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if dieuxe.content_morning_wednesday_1_5 and dieuxe.content_afternoon_wednesday_1_5:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(0,176,80);">' + '1/' + dieuxe.content_morning_wednesday_1_5 + '<br></br> 2/' + dieuxe.content_afternoon_wednesday_1_5 + '</td>'
        elif dieuxe.content_morning_wednesday_1_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_morning_wednesday_1_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if dieuxe.content_morning_thursday_1_5 and dieuxe.content_afternoon_thursday_1_5:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(0,176,80);">' + '1/' + dieuxe.content_morning_thursday_1_5 + '<br></br> 2/' + dieuxe.content_afternoon_thursday_1_5 + '</td>'
        elif dieuxe.content_morning_thursday_1_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_morning_thursday_1_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if dieuxe.content_morning_friday_1 and dieuxe.content_afternoon_friday_1:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(0,176,80);">' + '1/' + dieuxe.content_morning_friday_1 + '<br></br> 2/' + dieuxe.content_afternoon_friday_1 + '</td>'
        elif dieuxe.content_morning_friday_1:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_morning_friday_1 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if dieuxe.content_morning_saturday_1_5 and dieuxe.content_afternoon_saturday_1_5:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(0,176,80);">' + '1/' + dieuxe.content_morning_saturday_1_5 + '<br></br> 2/' + dieuxe.content_afternoon_saturday_1_5 + '</td>'
        elif dieuxe.content_morning_saturday_1_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_morning_saturday_1_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if dieuxe.content_morning_sunday_1_5 and dieuxe.content_afternoon_sunday_1_5:
            string += '<td rowspan=2 style="font-size:10px;height:100px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(0,176,80);">' + '1/' + dieuxe.content_morning_sunday_1_5 + '<br></br> 2/' + dieuxe.content_afternoon_sunday_1_5 + '</td>'
        elif dieuxe.content_morning_sunday_1_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_morning_sunday_1_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string += '<tr>'
        # thuhai
        if dieuxe.content_morning_monday_5 and dieuxe.content_afternoon_monday_5:
            string += ''
        elif dieuxe.content_afternoon_monday_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_afternoon_monday_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if dieuxe.content_morning_tuesday_5 and dieuxe.content_afternoon_tuesday_5:
            string += ''
        elif dieuxe.content_afternoon_tuesday_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_afternoon_tuesday_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if dieuxe.content_morning_wednesday_5 and dieuxe.content_afternoon_wednesday_5:
            string += ''
        elif dieuxe.content_afternoon_wednesday_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_afternoon_wednesday_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if dieuxe.content_morning_thursday_5 and dieuxe.content_afternoon_thursday_5:
            string += ''
        elif dieuxe.content_afternoon_thursday_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_afternoon_thursday_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if dieuxe.content_morning_friday_5 and dieuxe.content_afternoon_friday_5:
            string += ''
        elif dieuxe.content_afternoon_friday_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_afternoon_friday_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if dieuxe.content_morning_saturday_5 and dieuxe.content_afternoon_saturday_5:
            string += ''
        elif dieuxe.content_afternoon_saturday_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_afternoon_saturday_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if dieuxe.content_morning_sunday_5 and dieuxe.content_afternoon_sunday_5:
            string += ''
        elif dieuxe.content_afternoon_sunday_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_afternoon_sunday_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuhai
        if dieuxe.content_morning_monday_1_5 and dieuxe.content_afternoon_monday_1_5:
            string += ''
        elif dieuxe.content_afternoon_monday_1_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_afternoon_monday_1_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thuba
        if dieuxe.content_morning_tuesday_1_5 and dieuxe.content_afternoon_tuesday_1_5:
            string += ''
        elif dieuxe.content_afternoon_tuesday_1_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_afternoon_tuesday_1_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thutu
        if dieuxe.content_morning_wednesday_1_5 and dieuxe.content_afternoon_wednesday_1_5:
            string += ''
        elif dieuxe.content_afternoon_wednesday_1_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_afternoon_wednesday_1_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thunam
        if dieuxe.content_morning_thursday_1_5 and dieuxe.content_afternoon_thursday_1_5:
            string += ''
        elif dieuxe.content_afternoon_thursday_1_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_afternoon_thursday_1_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thusau
        if dieuxe.content_morning_friday_1_5 and dieuxe.content_afternoon_friday_1_5:
            string += ''
        elif dieuxe.content_afternoon_friday_1_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_afternoon_friday_1_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # thubay
        if dieuxe.content_morning_saturday_1_5 and dieuxe.content_afternoon_saturday_1_5:
            string += ''
        elif dieuxe.content_afternoon_saturday_1_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_afternoon_saturday_1_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        # chunhat
        if dieuxe.content_morning_sunday_1_5 and dieuxe.content_afternoon_sunday_1_5:
            string += ''
        elif dieuxe.content_afternoon_sunday_1_5:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,80)">' + '1/' + dieuxe.content_afternoon_sunday_1_5 + '</td>'
        else:
            string += '<td style="font-size:10px;height:50px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
        string += '</tr>'

        string_xethue = ''
        for xethue in dieuxe.xethue:
            if xethue:
                string_xethue += '</tr>'
                string_xethue += '<td style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color: rgb(0,176,240)">' + kiemtra(
                    xethue.driver_6) + '</td>'
                # thuhai
                if xethue.content_morning_monday_6:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color: rgb(0,176,240)">' + xethue.content_morning_monday_6 + '</td>'
                else:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
                # thuba
                if xethue.content_morning_tuesday_6:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(0,176,240)">' + xethue.content_morning_tuesday_6 + '</td>'
                else:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
                # thutu
                if xethue.content_morning_wednesday_6:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(0,176,240)">' + xethue.content_morning_wednesday_6 + '</td>'
                else:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
                # thunam
                if xethue.content_morning_thursday_6:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(0,176,240)">' + xethue.content_morning_thursday_6 + '</td>'
                else:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
                # thusau
                if xethue.content_morning_friday_6:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(0,176,240)">' + xethue.content_morning_friday_6 + '</td>'
                else:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
                # thubay
                if xethue.content_morning_saturday_6:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(0,176,240)">' + xethue.content_morning_saturday_6 + '</td>'
                else:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
                # chunhat
                if xethue.content_morning_sunday_6:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(0,176,240)">' + xethue.content_morning_sunday_6 + '</td>'
                else:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'

                # thuhai
                if xethue.content_morning_monday_1_6:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(0,176,240)">' + xethue.content_morning_monday_1_6 + '</td>'
                else:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
                # thuba
                if xethue.content_morning_tuesday_1_6:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(0,176,240)">' + xethue.content_morning_tuesday_1_6 + '</td>'
                else:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
                # thutu
                if xethue.content_morning_wednesday_1_6:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(0,176,240)">' + xethue.content_morning_wednesday_1_6 + '</td>'
                else:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
                # thunam
                if xethue.content_morning_thursday_1_6:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(0,176,240)">' + xethue.content_morning_thursday_1_6 + '</td>'
                else:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
                # thusau
                if xethue.content_morning_friday_1_6:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(0,176,240)">' + xethue.content_morning_friday_1_6 + '</td>'
                else:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
                # thubay
                if xethue.content_morning_saturday_1_6:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(0,176,240)">' + xethue.content_morning_saturday_1_6 + '</td>'
                else:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
                # chunhat
                if xethue.content_morning_sunday_1_6:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;background-color:rgb(0,176,240)">' + xethue.content_morning_sunday_1_6 + '</td>'
                else:
                    string_xethue += '<td style="font-size:10px;height:100px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;"></td>'
                string_xethue += '</tr>'

        if string_xethue:
            string += '<tr style="height:25px">'
            string += '</tr>'

            string += string_xethue

        string += '</table>'

        mail = self.env.ref('email.example_email_template')
        origin_mail = mail.body_html
        mail.body_html = origin_mail.replace('{data_body}', string)

        return origin_mail

    @api.model
    def cron_do_email_3(self):
        origin_mail = self._cron_do_email_3()
        template = self.env.ref('email.example_email_template')
        ketqua = template.send_mail(template.id, force_send=True)
        template.body_html = origin_mail
        print(ketqua)
