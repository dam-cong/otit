# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime


def thu_tuan(self):
    ans = datetime.date(self.year, self.month, self.day)
    substr = ans.strftime("%A")
    if substr == 'Sunday':
        return 'Chủ nhật'
    elif substr == 'Monday':
        return 'Thứ 2'
    elif substr == 'Tuesday':
        return 'Thứ 3'
    elif substr == 'Wednesday':
        return 'Thứ 4'
    elif substr == 'Thursday':
        return 'Thứ 5'
    elif substr == 'Friday':
        return 'Thứ 6'
    elif substr == 'Saturday':
        return 'Thứ 7'
    else:
        return ''


class XeThue(models.Model):
    _name = 'email.xethue'
    _rec_name = 'monday_6'
    dieuxe_xethue = fields.Many2one(comodel_name="email.dieuxe", string="Xe thuê")
    monday_6 = fields.Char()
    tuesday_6 = fields.Char()
    wednesday_6 = fields.Char()
    thursday_6 = fields.Char()
    friday_6 = fields.Char()
    saturday_6 = fields.Char()
    sunday_6 = fields.Char()
    monday_1_6 = fields.Char()
    tuesday_1_6 = fields.Char()
    wednesday_1_6 = fields.Char()
    thursday_1_6 = fields.Char()
    friday_1_6 = fields.Char()
    saturday_1_6 = fields.Char()
    sunday_1_6 = fields.Char()
    day_monday_6 = fields.Date()
    day_tuesday_6 = fields.Date()
    day_wednesday_6 = fields.Date()
    day_thursday_6 = fields.Date()
    day_friday_6 = fields.Date()
    day_saturday_6 = fields.Date()
    day_sunday_6 = fields.Date()
    day_monday_1_6 = fields.Date()
    day_tuesday_1_6 = fields.Date()
    day_wednesday_1_6 = fields.Date()
    day_thursday_1_6 = fields.Date()
    day_friday_1_6 = fields.Date()
    day_saturday_1_6 = fields.Date()
    day_sunday_1_6 = fields.Date()
    driver_6 = fields.Text("Lái xe thuê (Hoàng Hưng)")
    content_morning_monday_6 = fields.Char('Thứ 2')
    content_morning_tuesday_6 = fields.Char('Thứ 3')
    content_morning_wednesday_6 = fields.Char('Thứ 4')
    content_morning_thursday_6 = fields.Char('Thứ 5')
    content_morning_friday_6 = fields.Char('Thứ 6')
    content_morning_saturday_6 = fields.Char('Thứ 7')
    content_morning_sunday_6 = fields.Char('Chủ nhật')
    content_morning_monday_1_6 = fields.Char('Thứ 2')
    content_morning_tuesday_1_6 = fields.Char('Thứ 3')
    content_morning_wednesday_1_6 = fields.Char('Thứ 4')
    content_morning_thursday_1_6 = fields.Char('Thứ 5')
    content_morning_friday_1_6 = fields.Char('Thứ 6')
    content_morning_saturday_1_6 = fields.Char('Thứ 7')
    content_morning_sunday_1_6 = fields.Char('Chủ nhật')


class DieuXe(models.Model):
    _name = 'email.dieuxe'
    _rec_name = 'day_start'

    day_start = fields.Date(string='Ngày bắt đầu')

    monday = fields.Char(compute='_day_monday')
    tuesday = fields.Char(compute='_day_monday')
    wednesday = fields.Char(compute='_day_monday')
    thursday = fields.Char(compute='_day_monday')
    friday = fields.Char(compute='_day_monday')
    saturday = fields.Char(compute='_day_monday')
    sunday = fields.Char(compute='_day_monday')
    monday_1 = fields.Char(compute='_day_monday')
    tuesday_1 = fields.Char(compute='_day_monday')
    wednesday_1 = fields.Char(compute='_day_monday')
    thursday_1 = fields.Char(compute='_day_monday')
    friday_1 = fields.Char(compute='_day_monday')
    saturday_1 = fields.Char(compute='_day_monday')
    sunday_1 = fields.Char(compute='_day_monday')
    day_monday = fields.Date(compute='_day_monday')
    day_tuesday = fields.Date(compute='_day_monday')
    day_wednesday = fields.Date(compute='_day_monday')
    day_thursday = fields.Date(compute='_day_monday')
    day_friday = fields.Date(compute='_day_monday')
    day_saturday = fields.Date(compute='_day_monday')
    day_sunday = fields.Date(compute='_day_monday')
    day_monday_1 = fields.Date(compute='_day_monday')
    day_tuesday_1 = fields.Date(compute='_day_monday')
    day_wednesday_1 = fields.Date(compute='_day_monday')
    day_thursday_1 = fields.Date(compute='_day_monday')
    day_friday_1 = fields.Date(compute='_day_monday')
    day_saturday_1 = fields.Date(compute='_day_monday')
    day_sunday_1 = fields.Date(compute='_day_monday')
    driver = fields.Text(default="Anh Đức \n (7 chỗ - 30A-729.71) \n 0944.870.542")

    content_morning_monday = fields.Char()
    content_morning_tuesday = fields.Char()
    content_morning_wednesday = fields.Char()
    content_morning_thursday = fields.Char()
    content_morning_friday = fields.Char()
    content_morning_saturday = fields.Char()
    content_morning_sunday = fields.Char()
    content_morning_monday_1 = fields.Char()
    content_morning_tuesday_1 = fields.Char()
    content_morning_wednesday_1 = fields.Char()
    content_morning_thursday_1 = fields.Char()
    content_morning_friday_1 = fields.Char()
    content_morning_saturday_1 = fields.Char()
    content_morning_sunday_1 = fields.Char()
    content_afternoon_monday = fields.Char()
    content_afternoon_tuesday = fields.Char()
    content_afternoon_wednesday = fields.Char()
    content_afternoon_thursday = fields.Char()
    content_afternoon_friday = fields.Char()
    content_afternoon_saturday = fields.Char()
    content_afternoon_sunday = fields.Char()
    content_afternoon_monday_1 = fields.Char()
    content_afternoon_tuesday_1 = fields.Char()
    content_afternoon_wednesday_1 = fields.Char()
    content_afternoon_thursday_1 = fields.Char()
    content_afternoon_friday_1 = fields.Char()
    content_afternoon_saturday_1 = fields.Char()
    content_afternoon_sunday_1 = fields.Char()

    monday_2 = fields.Char()
    tuesday_2 = fields.Char()
    wednesday_2 = fields.Char()
    thursday_2 = fields.Char()
    friday_2 = fields.Char()
    saturday_2 = fields.Char()
    sunday_2 = fields.Char()
    monday_1_2 = fields.Char()
    tuesday_1_2 = fields.Char()
    wednesday_1_2 = fields.Char()
    thursday_1_2 = fields.Char()
    friday_1_2 = fields.Char()
    saturday_1_2 = fields.Char()
    sunday_1_2 = fields.Char()
    day_monday_2 = fields.Date()
    day_tuesday_2 = fields.Date()
    day_wednesday_2 = fields.Date()
    day_thursday_2 = fields.Date()
    day_friday_2 = fields.Date()
    day_saturday_2 = fields.Date()
    day_sunday_2 = fields.Date()
    day_monday_1_2 = fields.Date()
    day_tuesday_1_2 = fields.Date()
    day_wednesday_1_2 = fields.Date()
    day_thursday_1_2 = fields.Date()
    day_friday_1_2 = fields.Date()
    day_saturday_1_2 = fields.Date()
    day_sunday_1_2 = fields.Date()

    driver_2 = fields.Text(default="Anh Tuyn \n (16 chỗ - 29B-407.98) \n 0978.668.331")
    content_morning_monday_2 = fields.Char()
    content_morning_tuesday_2 = fields.Char()
    content_morning_wednesday_2 = fields.Char()
    content_morning_thursday_2 = fields.Char()
    content_morning_friday_2 = fields.Char()
    content_morning_saturday_2 = fields.Char()
    content_morning_sunday_2 = fields.Char()
    content_morning_monday_1_2 = fields.Char()
    content_morning_tuesday_1_2 = fields.Char()
    content_morning_wednesday_1_2 = fields.Char()
    content_morning_thursday_1_2 = fields.Char()
    content_morning_friday_1_2 = fields.Char()
    content_morning_saturday_1_2 = fields.Char()
    content_morning_sunday_1_2 = fields.Char()
    content_afternoon_monday_2 = fields.Char()
    content_afternoon_tuesday_2 = fields.Char()
    content_afternoon_wednesday_2 = fields.Char()
    content_afternoon_thursday_2 = fields.Char()
    content_afternoon_friday_2 = fields.Char()
    content_afternoon_saturday_2 = fields.Char()
    content_afternoon_sunday_2 = fields.Char()
    content_afternoon_monday_1_2 = fields.Char()
    content_afternoon_tuesday_1_2 = fields.Char()
    content_afternoon_wednesday_1_2 = fields.Char()
    content_afternoon_thursday_1_2 = fields.Char()
    content_afternoon_friday_1_2 = fields.Char()
    content_afternoon_saturday_1_2 = fields.Char()
    content_afternoon_sunday_1_2 = fields.Char()

    monday_3 = fields.Char()
    tuesday_3 = fields.Char()
    wednesday_3 = fields.Char()
    thursday_3 = fields.Char()
    friday_3 = fields.Char()
    saturday_3 = fields.Char()
    sunday_3 = fields.Char()
    monday_1_3 = fields.Char()
    tuesday_1_3 = fields.Char()
    wednesday_1_3 = fields.Char()
    thursday_1_3 = fields.Char()
    friday_1_3 = fields.Char()
    saturday_1_3 = fields.Char()
    sunday_1_3 = fields.Char()
    day_monday_3 = fields.Date()
    day_tuesday_3 = fields.Date()
    day_wednesday_3 = fields.Date()
    day_thursday_3 = fields.Date()
    day_friday_3 = fields.Date()
    day_saturday_3 = fields.Date()
    day_sunday_3 = fields.Date()
    day_monday_1_3 = fields.Date()
    day_tuesday_1_3 = fields.Date()
    day_wednesday_1_3 = fields.Date()
    day_thursday_1_3 = fields.Date()
    day_friday_1_3 = fields.Date()
    day_saturday_1_3 = fields.Date()
    day_sunday_1_3 = fields.Date()
    driver_3 = fields.Text(default="Anh Hưởng \n (16 chỗ - 29B-174.68) \n 0976.811.169")
    content_morning_monday_3 = fields.Char()
    content_morning_tuesday_3 = fields.Char()
    content_morning_wednesday_3 = fields.Char()
    content_morning_thursday_3 = fields.Char()
    content_morning_friday_3 = fields.Char()
    content_morning_saturday_3 = fields.Char()
    content_morning_sunday_3 = fields.Char()
    content_morning_monday_1_3 = fields.Char()
    content_morning_tuesday_1_3 = fields.Char()
    content_morning_wednesday_1_3 = fields.Char()
    content_morning_thursday_1_3 = fields.Char()
    content_morning_friday_1_3 = fields.Char()
    content_morning_saturday_1_3 = fields.Char()
    content_morning_sunday_1_3 = fields.Char()
    content_afternoon_monday_3 = fields.Char()
    content_afternoon_tuesday_3 = fields.Char()
    content_afternoon_wednesday_3 = fields.Char()
    content_afternoon_thursday_3 = fields.Char()
    content_afternoon_friday_3 = fields.Char()
    content_afternoon_saturday_3 = fields.Char()
    content_afternoon_sunday_3 = fields.Char()
    content_afternoon_monday_1_3 = fields.Char()
    content_afternoon_tuesday_1_3 = fields.Char()
    content_afternoon_wednesday_1_3 = fields.Char()
    content_afternoon_thursday_1_3 = fields.Char()
    content_afternoon_friday_1_3 = fields.Char()
    content_afternoon_saturday_1_3 = fields.Char()
    content_afternoon_sunday_1_3 = fields.Char()

    monday_4 = fields.Char()
    tuesday_4 = fields.Char()
    wednesday_4 = fields.Char()
    thursday_4 = fields.Char()
    friday_4 = fields.Char()
    saturday_4 = fields.Char()
    sunday_4 = fields.Char()
    monday_1_4 = fields.Char()
    tuesday_1_4 = fields.Char()
    wednesday_1_4 = fields.Char()
    thursday_1_4 = fields.Char()
    friday_1_4 = fields.Char()
    saturday_1_4 = fields.Char()
    sunday_1_4 = fields.Char()
    day_monday_4 = fields.Date()
    day_tuesday_4 = fields.Date()
    day_wednesday_4 = fields.Date()
    day_thursday_4 = fields.Date()
    day_friday_4 = fields.Date()
    day_saturday_4 = fields.Date()
    day_sunday_4 = fields.Date()
    day_monday_1_4 = fields.Date()
    day_tuesday_1_4 = fields.Date()
    day_wednesday_1_4 = fields.Date()
    day_thursday_1_4 = fields.Date()
    day_friday_1_4 = fields.Date()
    day_saturday_1_4 = fields.Date()
    day_sunday_1_4 = fields.Date()
    driver_4 = fields.Text(default="Anh Cường \n (16 chỗ - 29B-169.29) \n 0969.600.622")
    content_morning_monday_4 = fields.Char()
    content_morning_tuesday_4 = fields.Char()
    content_morning_wednesday_4 = fields.Char()
    content_morning_thursday_4 = fields.Char()
    content_morning_friday_4 = fields.Char()
    content_morning_saturday_4 = fields.Char()
    content_morning_sunday_4 = fields.Char()
    content_morning_monday_1_4 = fields.Char()
    content_morning_tuesday_1_4 = fields.Char()
    content_morning_wednesday_1_4 = fields.Char()
    content_morning_thursday_1_4 = fields.Char()
    content_morning_friday_1_4 = fields.Char()
    content_morning_saturday_1_4 = fields.Char()
    content_morning_sunday_1_4 = fields.Char()
    content_afternoon_monday_4 = fields.Char()
    content_afternoon_tuesday_4 = fields.Char()
    content_afternoon_wednesday_4 = fields.Char()
    content_afternoon_thursday_4 = fields.Char()
    content_afternoon_friday_4 = fields.Char()
    content_afternoon_saturday_4 = fields.Char()
    content_afternoon_sunday_4 = fields.Char()
    content_afternoon_monday_1_4 = fields.Char()
    content_afternoon_tuesday_1_4 = fields.Char()
    content_afternoon_wednesday_1_4 = fields.Char()
    content_afternoon_thursday_1_4 = fields.Char()
    content_afternoon_friday_1_4 = fields.Char()
    content_afternoon_saturday_1_4 = fields.Char()
    content_afternoon_sunday_1_4 = fields.Char()

    monday_5 = fields.Char()
    tuesday_5 = fields.Char()
    wednesday_5 = fields.Char()
    thursday_5 = fields.Char()
    friday_5 = fields.Char()
    saturday_5 = fields.Char()
    sunday_5 = fields.Char()
    monday_1_5 = fields.Char()
    tuesday_1_5 = fields.Char()
    wednesday_1_5 = fields.Char()
    thursday_1_5 = fields.Char()
    friday_1_5 = fields.Char()
    saturday_1_5 = fields.Char()
    sunday_1_5 = fields.Char()
    day_monday_5 = fields.Date()
    day_tuesday_5 = fields.Date()
    day_wednesday_5 = fields.Date()
    day_thursday_5 = fields.Date()
    day_friday_5 = fields.Date()
    day_saturday_5 = fields.Date()
    day_sunday_5 = fields.Date()
    day_monday_1_5 = fields.Date()
    day_tuesday_1_5 = fields.Date()
    day_wednesday_1_5 = fields.Date()
    day_thursday_1_5 = fields.Date()
    day_friday_1_5 = fields.Date()
    day_saturday_1_5 = fields.Date()
    day_sunday_1_5 = fields.Date()
    driver_5 = fields.Text(default="Anh Hùng \n (Limousine - 29B-405.95) \n 0978.880.361")
    content_morning_monday_5 = fields.Char()
    content_morning_tuesday_5 = fields.Char()
    content_morning_wednesday_5 = fields.Char()
    content_morning_thursday_5 = fields.Char()
    content_morning_friday_5 = fields.Char()
    content_morning_saturday_5 = fields.Char()
    content_morning_sunday_5 = fields.Char()
    content_morning_monday_1_5 = fields.Char()
    content_morning_tuesday_1_5 = fields.Char()
    content_morning_wednesday_1_5 = fields.Char()
    content_morning_thursday_1_5 = fields.Char()
    content_morning_friday_1_5 = fields.Char()
    content_morning_saturday_1_5 = fields.Char()
    content_morning_sunday_1_5 = fields.Char()
    content_afternoon_monday_5 = fields.Char()
    content_afternoon_tuesday_5 = fields.Char()
    content_afternoon_wednesday_5 = fields.Char()
    content_afternoon_thursday_5 = fields.Char()
    content_afternoon_friday_5 = fields.Char()
    content_afternoon_saturday_5 = fields.Char()
    content_afternoon_sunday_5 = fields.Char()
    content_afternoon_monday_1_5 = fields.Char()
    content_afternoon_tuesday_1_5 = fields.Char()
    content_afternoon_wednesday_1_5 = fields.Char()
    content_afternoon_thursday_1_5 = fields.Char()
    content_afternoon_friday_1_5 = fields.Char()
    content_afternoon_saturday_1_5 = fields.Char()
    content_afternoon_sunday_1_5 = fields.Char()

    xethue = fields.One2many(comodel_name="email.xethue", inverse_name="dieuxe_xethue", string="Xe thuê")

    @api.multi
    def duplicate_dieuxe(self):
        mass_mailing_copy = self.copy(default={
            'day_start': self.day_monday_1,
            'driver': self.driver,
            'driver_2': self.driver_2,
            'driver_3': self.driver_3,
            'driver_4': self.driver_4,
            'driver_5': self.driver_5,
            'content_morning_monday': self.content_morning_monday_1,
            'content_morning_tuesday': self.content_morning_tuesday_1,
            'content_morning_wednesday': self.content_morning_wednesday_1,
            'content_morning_thursday': self.content_morning_thursday_1,
            'content_morning_friday': self.content_morning_friday_1,
            'content_morning_saturday': self.content_morning_saturday_1,
            'content_morning_sunday': self.content_morning_sunday_1,

            'content_afternoon_monday': self.content_afternoon_monday_1,
            'content_afternoon_tuesday': self.content_afternoon_tuesday_1,
            'content_afternoon_wednesday': self.content_afternoon_wednesday_1,
            'content_afternoon_thursday': self.content_afternoon_thursday_1,
            'content_afternoon_friday': self.content_afternoon_friday_1,
            'content_afternoon_saturday': self.content_afternoon_saturday_1,
            'content_afternoon_sunday': self.content_afternoon_sunday_1,

            'content_morning_monday_1': '',
            'content_morning_tuesday_1': '',
            'content_morning_wednesday_1': '',
            'content_morning_thursday_1': '',
            'content_morning_friday_1': '',
            'content_morning_saturday_1': '',
            'content_morning_sunday_1': '',

            'content_afternoon_monday_1': '',
            'content_afternoon_tuesday_1': '',
            'content_afternoon_wednesday_1': '',
            'content_afternoon_thursday_1': '',
            'content_afternoon_friday_1': '',
            'content_afternoon_saturday_1': '',
            'content_afternoon_sunday_1': '',

            'day_monday': None,
            'day_tuesday': None,
            'day_wednesday': None,
            'day_thursday': None,
            'day_friday': None,
            'day_saturday': None,
            'day_sunday': None,

            'day_monday_1': None,
            'day_tuesday_1': None,
            'day_wednesday_1': None,
            'day_thursday_1': None,
            'day_friday_1': None,
            'day_saturday_1': None,
            'day_sunday_1': None,

            'content_morning_monday_2': self.content_morning_monday_1_2,
            'content_morning_tuesday_2': self.content_morning_tuesday_1_2,
            'content_morning_wednesday_2': self.content_morning_wednesday_1_2,
            'content_morning_thursday_2': self.content_morning_thursday_1_2,
            'content_morning_friday_2': self.content_morning_friday_1_2,
            'content_morning_saturday_2': self.content_morning_saturday_1_2,
            'content_morning_sunday_2': self.content_morning_sunday_1_2,

            'content_afternoon_monday_2': self.content_afternoon_monday_1_2,
            'content_afternoon_tuesday_2': self.content_afternoon_tuesday_1_2,
            'content_afternoon_wednesday_2': self.content_afternoon_wednesday_1_2,
            'content_afternoon_thursday_2': self.content_afternoon_thursday_1_2,
            'content_afternoon_friday_2': self.content_afternoon_friday_1_2,
            'content_afternoon_saturday_2': self.content_afternoon_saturday_1_2,
            'content_afternoon_sunday_2': self.content_afternoon_sunday_1_2,

            'content_morning_monday_1_2': '',
            'content_morning_tuesday_1_2': '',
            'content_morning_wednesday_1_2': '',
            'content_morning_thursday_1_2': '',
            'content_morning_friday_1_2': '',
            'content_morning_saturday_1_2': '',
            'content_morning_sunday_1_2': '',

            'content_afternoon_monday_1_2': '',
            'content_afternoon_tuesday_1_2': '',
            'content_afternoon_wednesday_1_2': '',
            'content_afternoon_thursday_1_2': '',
            'content_afternoon_friday_1_2': '',
            'content_afternoon_saturday_1_2': '',
            'content_afternoon_sunday_1_2': '',

            'content_morning_monday_3': self.content_morning_monday_1_3,
            'content_morning_tuesday_3': self.content_morning_tuesday_1_3,
            'content_morning_wednesday_3': self.content_morning_wednesday_1_3,
            'content_morning_thursday_3': self.content_morning_thursday_1_3,
            'content_morning_friday_3': self.content_morning_friday_1_3,
            'content_morning_saturday_3': self.content_morning_saturday_1_3,
            'content_morning_sunday_3': self.content_morning_sunday_1_3,

            'content_afternoon_monday_3': self.content_afternoon_monday_1_3,
            'content_afternoon_tuesday_3': self.content_afternoon_tuesday_1_3,
            'content_afternoon_wednesday_3': self.content_afternoon_wednesday_1_3,
            'content_afternoon_thursday_3': self.content_afternoon_thursday_1_3,
            'content_afternoon_friday_3': self.content_afternoon_friday_1_3,
            'content_afternoon_saturday_3': self.content_afternoon_saturday_1_3,
            'content_afternoon_sunday_3': self.content_afternoon_sunday_1_3,

            'content_morning_monday_1_3': '',
            'content_morning_tuesday_1_3': '',
            'content_morning_wednesday_1_3': '',
            'content_morning_thursday_1_3': '',
            'content_morning_friday_1_3': '',
            'content_morning_saturday_1_3': '',
            'content_morning_sunday_1_3': '',

            'content_afternoon_monday_1_3': '',
            'content_afternoon_tuesday_1_3': '',
            'content_afternoon_wednesday_1_3': '',
            'content_afternoon_thursday_1_3': '',
            'content_afternoon_friday_1_3': '',
            'content_afternoon_saturday_1_3': '',
            'content_afternoon_sunday_1_3': '',

            'content_morning_monday_4': self.content_morning_monday_1_4,
            'content_morning_tuesday_4': self.content_morning_tuesday_1_4,
            'content_morning_wednesday_4': self.content_morning_wednesday_1_4,
            'content_morning_thursday_4': self.content_morning_thursday_1_4,
            'content_morning_friday_4': self.content_morning_friday_1_4,
            'content_morning_saturday_4': self.content_morning_saturday_1_4,
            'content_morning_sunday_4': self.content_morning_sunday_1_4,

            'content_afternoon_monday_4': self.content_afternoon_monday_1_4,
            'content_afternoon_tuesday_4': self.content_afternoon_tuesday_1_4,
            'content_afternoon_wednesday_4': self.content_afternoon_wednesday_1_4,
            'content_afternoon_thursday_4': self.content_afternoon_thursday_1_4,
            'content_afternoon_friday_4': self.content_afternoon_friday_1_4,
            'content_afternoon_saturday_4': self.content_afternoon_saturday_1_4,
            'content_afternoon_sunday_4': self.content_afternoon_sunday_1_4,

            'content_morning_monday_1_4': '',
            'content_morning_tuesday_1_4': '',
            'content_morning_wednesday_1_4': '',
            'content_morning_thursday_1_4': '',
            'content_morning_friday_1_4': '',
            'content_morning_saturday_1_4': '',
            'content_morning_sunday_1_4': '',

            'content_afternoon_monday_1_4': '',
            'content_afternoon_tuesday_1_4': '',
            'content_afternoon_wednesday_1_4': '',
            'content_afternoon_thursday_1_4': '',
            'content_afternoon_friday_1_4': '',
            'content_afternoon_saturday_1_4': '',
            'content_afternoon_sunday_1_4': '',

            'content_morning_monday_5': self.content_morning_monday_1_5,
            'content_morning_tuesday_5': self.content_morning_tuesday_1_5,
            'content_morning_wednesday_5': self.content_morning_wednesday_1_5,
            'content_morning_thursday_5': self.content_morning_thursday_1_5,
            'content_morning_friday_5': self.content_morning_friday_1_5,
            'content_morning_saturday_5': self.content_morning_saturday_1_5,
            'content_morning_sunday_5': self.content_morning_sunday_1_5,

            'content_afternoon_monday_5': self.content_afternoon_monday_1_5,
            'content_afternoon_tuesday_5': self.content_afternoon_tuesday_1_5,
            'content_afternoon_wednesday_5': self.content_afternoon_wednesday_1_5,
            'content_afternoon_thursday_5': self.content_afternoon_thursday_1_5,
            'content_afternoon_friday_5': self.content_afternoon_friday_1_5,
            'content_afternoon_saturday_5': self.content_afternoon_saturday_1_5,
            'content_afternoon_sunday_5': self.content_afternoon_sunday_1_5,

            'content_morning_monday_1_5': '',
            'content_morning_tuesday_1_5': '',
            'content_morning_wednesday_1_5': '',
            'content_morning_thursday_1_5': '',
            'content_morning_friday_1_5': '',
            'content_morning_saturday_1_5': '',
            'content_morning_sunday_1_5': '',

            'content_afternoon_monday_1_5': '',
            'content_afternoon_tuesday_1_5': '',
            'content_afternoon_wednesday_1_5': '',
            'content_afternoon_thursday_1_5': '',
            'content_afternoon_friday_1_5': '',
            'content_afternoon_saturday_1_5': '',
            'content_afternoon_sunday_1_5': '',
        })
        if mass_mailing_copy:
            context = dict(self.env.context)
            context['form_view_initial_mode'] = 'edit'
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'email.dieuxe',
                'res_id': mass_mailing_copy.id,
                'context': context,
            }
        return False

    @api.one
    @api.depends('day_start')
    def _day_monday(self):
        if self.day_start:
            self.day_monday = self.day_start + datetime.timedelta(days=0)
            self.monday = thu_tuan(self.day_monday)
            self.day_tuesday = self.day_start + datetime.timedelta(days=1)
            self.tuesday = thu_tuan(self.day_tuesday)
            self.day_wednesday = self.day_start + datetime.timedelta(days=2)
            self.wednesday = thu_tuan(self.day_wednesday)
            self.day_thursday = self.day_start + datetime.timedelta(days=3)
            self.thursday = thu_tuan(self.day_thursday)
            self.day_friday = self.day_start + datetime.timedelta(days=4)
            self.friday = thu_tuan(self.day_friday)
            self.day_saturday = self.day_start + datetime.timedelta(days=5)
            self.saturday = thu_tuan(self.day_saturday)
            self.day_sunday = self.day_start + datetime.timedelta(days=6)
            self.sunday = thu_tuan(self.day_sunday)

            self.day_monday_1 = self.day_start + datetime.timedelta(days=7)
            self.monday_1 = thu_tuan(self.day_monday_1)
            self.day_tuesday_1 = self.day_start + datetime.timedelta(days=8)
            self.day_wednesday_1 = self.day_start + datetime.timedelta(days=9)
            self.day_thursday_1 = self.day_start + datetime.timedelta(days=10)
            self.day_friday_1 = self.day_start + datetime.timedelta(days=11)
            self.day_saturday_1 = self.day_start + datetime.timedelta(days=12)
            self.day_sunday_1 = self.day_start + datetime.timedelta(days=13)

            self.tuesday_1 = thu_tuan(self.day_tuesday_1)
            self.wednesday_1 = thu_tuan(self.day_wednesday_1)
            self.thursday_1 = thu_tuan(self.day_thursday_1)
            self.friday_1 = thu_tuan(self.day_friday_1)
            self.saturday_1 = thu_tuan(self.day_saturday_1)
            self.sunday_1 = thu_tuan(self.day_sunday_1)
        else:
            self.day_monday = None
            self.day_tuesday = None
            self.day_wednesday = None
            self.day_thursday = None
            self.day_friday = None
            self.day_saturday = None
            self.day_sunday = None

            self.day_monday_1 = None
            self.day_tuesday_1 = None
            self.day_wednesday_1 = None
            self.day_thursday_1 = None
            self.day_friday_1 = None
            self.day_saturday_1 = None
            self.day_sunday_1 = None

            self.monday = ''
            self.tuesday = ''
            self.wednesday = ''
            self.thursday = ''
            self.friday = ''
            self.saturday = ''
            self.sunday = ''

            self.monday_1 = ''
            self.tuesday_1 = ''
            self.wednesday_1 = ''
            self.thursday_1 = ''
            self.friday_1 = ''
            self.saturday_1 = ''
            self.sunday_1 = ''
