# -*- coding: utf-8 -*-

from odoo import models, fields, api


class CVNghiepDoan(models.Model):
    _name = 'congviec.tonghop'
    _rec_name = 'ma_tonghop'
    _order = 'id desc'

    congtyphaicu = fields.Many2one(comodel_name='congtyphaicu.congtyphaicu', string='Công ty phải cử')
    ma_tonghop = fields.Char(string='Mã công việc')
    nganhnghe_tonghop = fields.Many2one(comodel_name='nganhnghe.nganhnghe', string='Ngành nghề')

    @api.onchange('nganhnghe_tonghop')
    def onchange_master_nganhnghe_tonghop(self):
        if self.nganhnghe_tonghop:
            return {'domain': {
                'loaicongviec_tonghop': [('nganhnghe_loaicongviec', '=', self.nganhnghe_tonghop.id)]}}

    loaicongviec_tonghop = fields.Many2one(comodel_name='loaicongviec.loaicongviec', string='Loại công việc')

    @api.onchange('loaicongviec_tonghop')
    def onchange_master_loaicongviec_tonghop(self):
        if self.loaicongviec_tonghop:
            return {'domain': {
                'congviec_tonghop': [('loaicongviec_congviec', '=', self.loaicongviec_tonghop.id)]}}

    congviec_tonghop = fields.Many2one(comodel_name='congviec.congviec', string='Công việc')

    @api.onchange('congviec_tonghop')
    def onchange_master_congviec_tonghop(self):
        if self.congviec_tonghop:
            self.ma_tonghop = self.congviec_tonghop.ma_congviec


class congtyphaicu(models.Model):
    _name = 'congtyphaicu.congtyphaicu'
    _rec_name = 'ten_anh_phaicu'
    _order = 'id desc'

    ten_han_phaicu = fields.Char(string="Tiếng Hán", required=True)  # hhjp_0112
    ten_anh_phaicu = fields.Char(string="Tiếng Anh")  # hhjp_0924
    ten_viet_phaicu = fields.Char(string="Tiếng việt")  # hhjp_0924_v
    diachi_tienganh = fields.Char(string="Tiếng anh")  # hhjp_0502
    diachi_viet_pc = fields.Char(string="Quốc mẫu")  # hhjp_0512
    ngayky_hiepdinh = fields.Date(string="Ngày ký hiệp định với nghiệp đoàn")
    sdt_phaicu = fields.Char(string="Điện thoại")  # hhjp_0513
    sdt_nguoidaidien = fields.Char(string="Điện thoại")  # hhjp_0513
    so_buudien_phaicu = fields.Char(string="Mã bưu điện")  # hhjp_0513
    fax_phaicu = fields.Char(string="Số fax")  # hhjp_0514
    email_phaicu = fields.Char(string="Email")  #
    ten_nguoidaidien = fields.Char(string="Romaji")  # hhjp_0376
    daidien_latinh_phaicu = fields.Char(string="Quốc mẫu")  # hhjp_0510
    chucvu_han_phaicu = fields.Char(string="Tiếng Hán")  # hhjp_0376
    chucvu_latinh_phaicu = fields.Char(string="Quốc mẫu")  # hhjp_0510
    sogiayphep_phaicu = fields.Char(string="Số giấy phép")  #
    sonhanvien_phaicu = fields.Char(string="Số nhân viên")  #
    sovon_phaicu = fields.Float(string="Số vốn")  #
    doanhthunamtruoc_phaicu = fields.Float(string="Doanh thu năm trước")  #
    ngaythanhlap_phaicu = fields.Date(string="Ngày thành lập")  #

    # loaicongviec_phaicu_demo = fields.One2many(comodel_name='congviec.tonghop', inverse_name='congtyphaicu',
    #                                            string='Ngành nghề - Công việc')
    quoctich_phaicu = fields.Many2one(comodel_name='quoctich.quoctich', string="Quốc tịch", required=True)