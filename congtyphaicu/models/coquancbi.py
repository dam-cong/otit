from odoo import models, fields, api


class Coquanchuanbi(models.Model):
    _name = 'coquan.coquan'
    _rec_name = 'ten_coquan'
    _order = 'id desc'

    ten_coquan = fields.Char(string="Tên cơ quan (Tiếng Latinh)")
    ten_coquan_h = fields.Char(string="Tên cơ quan (Tiếng Hán)")
    ten_coquan_v = fields.Char(string="Tên cơ quan (Tiếng mẹ đẻ)")
    nguoi_daidien = fields.Char(string="Người đại diện (Tiếng Lating)")
    nguoi_daidien_h = fields.Char(string="Người đại diện (Tiếng Hán)")
    nguoi_daidien_v = fields.Char(string="Người đại diện (Tiếng mẹ đẻ)")
    diachi_coquan = fields.Char(string="Địa chỉ")
    sdt_coquan = fields.Char(string="Số điện thoại")
    email = fields.Char(string="Email")
    ngay_thanhlap = fields.Date(string="Ngày thành lập")
    daotaotruoc = fields.Boolean(string="Đào tạo trước nhập cảnh")
    khac_coquan = fields.Boolean(string="Khác")
    noidung_coquan = fields.Char(string="Nội dung: ")
    loaicongviec_chinh = fields.Char(string="Loại công việc,công việc chính")
    tienvon = fields.Float(string="Tiền vốn")
    tienvon_n = fields.Float(string="Tiền vốn(Tiếng nhật)")
    doanhso = fields.Float(string="Doanh số những năm gần đây")
    doanhso_n = fields.Float(string="Doanh số những năm gần đây(Tiếng nhật)")
    sonhanvien = fields.Integer(string="Số nhân viên")
    ten_nguoilapvanban = fields.Char(string="Người lập văn bản")
    chucvu_nguoilapvanban = fields.Char(string="Chức vụ")
    # ngay_lapvanban = fields.Date(string="Ngày lập văn bản")