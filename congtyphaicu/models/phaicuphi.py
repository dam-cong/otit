# -*- coding: utf-8 -*-

from odoo import models, fields, api


class LoaiPhi(models.Model):
    _name = 'phaicu.loaiphi'
    _rec_name = 'congtyphaicu_phi'
    _order = 'id desc'

    congtyphaicu_phi = fields.Many2one(comodel_name='congtyphaicu.congtyphaicu', string='Công ty phải cử')
    giaidoan_phi = fields.Selection(string='Giai đoạn',
                                     selection=[('1 go', '1 go'),
                                                ('3 go', '3 go')])

    loaiphi_1 = fields.Char(string="Loại phí(Tiếng nhật)")
    loaiphi_v_1 = fields.Char(string="Loại phí(Tiếng Việt)")
    sotien_viet_1 = fields.Integer(string="Số tiền(Tiếng việt)")
    sotien_nhat_1 = fields.Integer(string="Số tiền (Nhật)")
    # -----------------------------------------------------------------
    loaiphi_2 = fields.Char(string="Loại phí(Tiếng Nhật)")
    loaiphi_v_2 = fields.Char(string="Loại phí(Tiếng Việt)")
    sotien_viet_2 = fields.Integer(string="Số tiền (Việt)")
    sotien_nhat_2 = fields.Integer(string="Số tiền (Nhật)")
    # ------------------------------------------------------------------
    loaiphi_3 = fields.Char(string="Loại phí(Tiếng Nhật)")
    loaiphi_v_3 = fields.Char(string="Loại phí(Tiếng Việt)")
    sotien_viet_3 = fields.Integer(string="Số tiền (Việt)")
    sotien_nhat_3 = fields.Integer(string="Số tiền (Nhật)")
    # -----------------------------------------------------------------
    loaiphi_4 = fields.Char(string="Loại phí(Tiếng Nhật)")
    loaiphi_v_4 = fields.Char(string="Loại phí(Tiếng Việt)")
    sotien_viet_4 = fields.Integer(string="Số tiền (Việt)")
    sotien_nhat_4 = fields.Integer(string="Số tiền (Nhật)")
    # ------------------------------------------------------------------
    loaiphi_5 = fields.Char(string="Loại phí(Tiếng Nhật)")
    loaiphi_v_5 = fields.Char(string="Loại phí(Tiếng Việt)")
    sotien_viet_5 = fields.Integer(string="Số tiền (Việt)")
    sotien_nhat_5 = fields.Integer(string="Số tiền (Nhật)")
