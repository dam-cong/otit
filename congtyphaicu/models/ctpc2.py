# -*- coding: utf-8 -*-

from odoo import models, fields, api


class hoso(models.Model):
    _name = 'ctpc2.ctpc2'
    _rec_name = 'name'
    _order = 'id desc'

    name = fields.Char(u"Tên công ty (Tiếng Anh)", required=True)  # hh_256
    name_vn = fields.Char(u"Tên công ty (Tiếng mẹ đẻ)", required=True)  # hh_257
    # director = fields.Char(u"Người ký (Tiếng Anh)")  # hh_259
    director_n = fields.Char(u"Người ký (Tiếng Nhật)")  # hh_259
    # position_person_sign = fields.Char(u"Chức vụ (Tiếng Anh)")  # hh_260
    position_person_sign_n = fields.Char(u"Chức vụ (Tiếng Nhật)")  # hh_260
    date_create = fields.Date("Ngày thành lập công ty")  # hh_266
