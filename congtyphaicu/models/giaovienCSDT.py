from odoo import models, fields, api


class giaovienPC(models.Model):
    _name = 'giaovien'
    _rec_name = 'ten_giaovien'
    _order = 'id desc'

    ten_cosodaotao = fields.Many2one(comodel_name="csdt.csdt", string="Cơ quan làm việc")  # hhjp_0112
    ten_daotaotruoc = fields.Many2one(comodel_name="daotao.daotaotruoc")
    chucvu = fields.Char(string="Chức vụ")  # hhjp_0924
    ten_giaovien = fields.Char(string="Họ và tên")  # hhjp_0924_v
