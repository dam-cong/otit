from odoo import models, fields, api


class cosodaotaoPC(models.Model):
    _name = 'csdt.csdt'
    _rec_name = 'ten_coso'
    _order = 'id desc'

    ten_phaicu = fields.Many2one(comodel_name="congtyphaicu.congtyphaicu", string="Tên PC",
                                 required=False, )  # hhjp_0112
    ten_coso = fields.Char(string="Tên CSĐT (Tiếng mẹ đẻ)")  # hhjp_0924
    ten_coso_jp = fields.Char(string="Tên CSĐT (Tiếng Nhật)")  # hhjp_0924_v
    diachi_coso = fields.Char(string="Địa chỉ")
    sdt_coso = fields.Char(string="Số điện thoại")
    nguoi_ky = fields.Char(string="Người Ký")
    chuc_vu = fields.Char(string="Chức vụ")
    giaovien_csdt = fields.One2many(comodel_name="giaovien",inverse_name="ten_cosodaotao", string="Giáo viên")
