# -*- coding: utf-8 -*-
{
    'name': "Công ty phái cử",
    'depends': ['base', 'congviec'],
    'data': [
        'security/group_user.xml',
        'security/ir.model.access.csv',
        'views/congtyphaicu.xml',
        # 'views/cosodaotaoPC.xml',
        # 'views/giaoviencsdt.xml',
        # 'views/congtyphaicu2.xml',
        # 'views/coquanchuanbi.xml',
        # 'views/phaicuphi.xml',
    ],
}
