# -*- coding: utf-8 -*-
{
    'name': "Báo cáo",
    'depends': ['base', 'donhang', 'congviec', 'xinghiep', 'nghiepdoan', 'thongbao'],
    'data': [
        # 'security/group_user.xml',
        'security/ir.model.access.csv',
        'views/baojunkai.xml',
        'views/baokansa.xml',
        'views/baokinhdoanh.xml',
        'views/baochidao.xml',
        'views/baotinhtrang.xml',
        'views/chamdiem.xml',
        'views/chamdiem.xml',
        'views/chamdiemnghiepdoan.xml',
    ],
}
