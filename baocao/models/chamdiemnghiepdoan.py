# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime
import logging
from tempfile import TemporaryFile, NamedTemporaryFile
from io import BytesIO, StringIO
import os
import codecs
from docxtpl import DocxTemplate
from docxtpl import DocxTemplate, Listing

_logger = logging.getLogger(__name__)


def kiemtra(data):
    if data:
        return data
    else:
        return ''


def convert_date(self):
    if self:
        return str(self.year) + '年' + \
               str(self.month) + '月' + \
               str(self.day) + '日'
    else:
        return '年　月　日'


def place_value(number):
    if number:
        return ("{:,}".format(number))
    else:
        return ''


def nganhnghe(self):
    if self:
        demo = self.find(':')
        if demo != -1:
            return self.split(":")[1]
        else:
            return self
    else:
        return ''


class ChamDiem(models.Model):
    _name = 'nghiepdoan.chamdiem'
    _rec_name = 'nghiepdoan_chamdiem'

    nghiepdoan_chamdiem = fields.Many2one('nghiepdoan.nghiepdoan', string='Nghiệp đoàn')
    ngaylap_vanban_chamdiem = fields.Date(string='Ngày lập văn bản', default=fields.Date.today())

    # --------------Bảng 1
    muc1_1 = fields.Selection(string="マニュアル等の策定及び監査担当職員への周知	", selection=[('Có', 'Có'), ('Không', 'Không')],
                              default='Có')
    muc1_1_1 = fields.Integer(compute="depends_muc1_1")

    @api.multi
    @api.depends('muc1_1')
    def depends_muc1_1(self):
        if self.muc1_1 == 'Có':
            self.muc1_1_1 = 5
        else:
            self.muc1_1_1 = 0

    muc1_2_1 = fields.Integer()
    muc1_2_2 = fields.Integer()
    tong_muc1_2 = fields.Float(compute="_depends_tong_muc1_2")
    muc1_2_1_1 = fields.Integer(compute='onchange_muc1_2_1_1')

    @api.multi
    @api.depends('tong_muc1_2')
    def onchange_muc1_2_1_1(self):
        if self.tong_muc1_2 < 5:
            self.muc1_2_1_1 = 15
        elif self.tong_muc1_2 < 10:
            self.muc1_2_1_1 = 7
        else:
            self.muc1_2_1_1 = 0

    @api.multi
    @api.depends('muc1_2_1', 'muc1_2_2')
    def _depends_tong_muc1_2(self):
        if self.muc1_2_1 or self.muc1_2_2:
            if self.muc1_2_2 == 0:
                pass
            else:
                self.tong_muc1_2 = round((self.muc1_2_1 / self.muc1_2_2), 1)
        else:
            self.tong_muc1_2 = 0

    muc1_3_1 = fields.Integer()
    muc1_3_2 = fields.Integer()
    tong_muc1_3 = fields.Float(compute="_depends_tong_muc1_3")
    muc1_3_1_1 = fields.Integer(compute="onchange_muc1_3_1_1")

    @api.multi
    @api.depends('tong_muc1_3')
    def onchange_muc1_3_1_1(self):
        if self.tong_muc1_3 >= 60:
            self.muc1_3_1_1 = 10
        elif self.tong_muc1_3 < 60 and self.tong_muc1_3 >= 50:
            self.muc1_3_1_1 = 5
        elif self.tong_muc1_3 < 50:
            self.muc1_3_1_1 = 0

    @api.multi
    @api.depends('muc1_3_1', 'muc1_3_2')
    def _depends_tong_muc1_3(self):
        if self.muc1_3_1 or self.muc1_3_2:
            if self.muc1_3_2 == 0:
                pass
            else:
                self.tong_muc1_3 = self.muc1_3_1 / self.muc1_3_2 * 100
        else:
            self.tong_muc1_3 = 0

    muc1_4_1 = fields.Selection(string='Ⅳ', selection=[('Có', 'Có'), ('Không', 'Không')], default='Có')
    muc1_4_1_1 = fields.Integer(compute="onchange_muc1_4_1_1")

    @api.multi
    @api.depends('muc1_4_1')
    def onchange_muc1_4_1_1(self):
        if self.muc1_4_1 == 'Có':
            self.muc1_4_1_1 = 5
        else:
            self.muc1_4_1_1 = 0

    muc1_4_2 = fields.Text(string="②　①の支援の概要")

    muc1_5 = fields.Selection(string="帰国後の技能実習生のフォローアップ調査への協力の意志の有無", selection=[('Có', 'Có'), ('Không', 'Không')],
                              default='Có')
    muc1_5_1 = fields.Integer(compute="onchange_muc1_5_1")

    @api.multi
    @api.depends('muc1_5')
    def onchange_muc1_5_1(self):
        if self.muc1_5 == 'Có':
            self.muc1_5_1 = 5
        else:
            self.muc1_5_1 = 0

    muc1_6_1 = fields.Selection(string="技能実習生のあっせんに関し、監理団体の役職員が送出国で行っている事前面接の概要",
                                selection=[('Có', 'Có'), ('Không', 'Không')], default='Có')
    muc1_6_1_1 = fields.Integer(compute="onchange_muc1_6_1_1")

    @api.multi
    @api.depends('muc1_6_1')
    def onchange_muc1_6_1_1(self):
        if self.muc1_6_1 == 'Có':
            self.muc1_6_1_1 = 5
        else:
            self.muc1_6_1_1 = 0

    muc1_6_2 = fields.Text()

    muc1_7_1 = fields.Selection(string="帰国後の技能実習生に関し、送出機関と提携して行っている就職先の把握の概",
                                selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')

    muc1_7_1_1 = fields.Integer(compute="onchange_muc1_7_1_1")

    @api.multi
    @api.depends('muc1_7_1')
    def onchange_muc1_7_1_1(self):
        if self.muc1_7_1 == 'Có':
            self.muc1_7_1_1 = 5
        else:
            self.muc1_7_1_1 = 0

    muc1_7_2 = fields.Text()

    thaythe_chamdiem = fields.Boolean(string='ⅡではなくⅡ２を利用する')

    muc2_1_1_1 = fields.Integer(string="現行制度")
    muc2_1_1_2 = fields.Integer()

    tong_muc2_1_1 = fields.Integer(compute="_depends_tong_muc2_1")

    @api.multi
    @api.depends('muc2_1_1_1', 'muc2_1_1_2')
    def _depends_tong_muc2_1(self):
        if self.muc2_1_1_1 or self.muc2_1_1_2:
            self.tong_muc2_1_1 = self.muc2_1_1_1 - self.muc2_1_1_2
        else:
            self.tong_muc2_1_1 = 0

    muc2_1_2_1 = fields.Integer(string="旧制度")
    muc2_1_2_2 = fields.Integer()
    tong_muc2_1_2 = fields.Integer(compute="_depends_tong_muc2_2")

    @api.multi
    @api.depends('muc2_1_2_1', 'muc2_1_2_2')
    def _depends_tong_muc2_2(self):
        if self.muc2_1_2_1 or self.muc2_1_2_2:
            self.tong_muc2_1_2 = self.muc2_1_2_1 - self.muc2_1_2_2
        else:
            self.tong_muc2_1_2 = 0

    tong_muc2_1_3 = fields.Integer(compute="_depends_tong_muc2_1_3")

    @api.multi
    @api.depends('tong_muc2_1_1', 'tong_muc2_1_2')
    def _depends_tong_muc2_1_3(self):
        if self.tong_muc2_1_1 or self.tong_muc2_1_2:
            self.tong_muc2_1_3 = self.tong_muc2_1_1 + self.tong_muc2_1_2
        else:
            self.tong_muc2_1_3 = 0

    muc2_1_3_1 = fields.Integer()
    muc2_1_3_2 = fields.Integer()

    tong_muc2_1_4 = fields.Integer(compute="_depends_tong_muc2_1_4")

    @api.multi
    @api.depends('muc2_1_3_1', 'muc2_1_3_2')
    def _depends_tong_muc2_1_4(self):
        if self.muc2_1_3_1 or self.muc2_1_3_2:
            self.tong_muc2_1_4 = self.muc2_1_3_1 + self.muc2_1_3_2
        else:
            self.tong_muc2_1_4 = 0

    muc2_2_1_1 = fields.Integer(string="現行制度	")
    muc2_2_1_2 = fields.Integer()

    tong_muc2_2_1 = fields.Integer(compute="_depends_tong_muc2_2_1")

    @api.multi
    @api.depends('muc2_2_1_1', 'muc2_2_1_2')
    def _depends_tong_muc2_2_1(self):
        if self.muc2_2_1_1 or self.muc2_2_1_2:
            self.tong_muc2_2_1 = self.muc2_2_1_1 - self.muc2_2_1_2
        else:
            self.tong_muc2_2_1 = 0

    muc2_2_1_3 = fields.Integer()
    muc2_2_1_4 = fields.Integer()

    muc2_2_2_1 = fields.Integer(string="旧制度")
    muc2_2_2_3 = fields.Integer()
    muc2_2_2_4 = fields.Integer()

    tong_muc2_2_3_1 = fields.Integer(string="小計", compute="_depends_tong_muc2_2_3_1")

    @api.multi
    @api.depends('tong_muc2_2_1', 'muc2_2_2_1')
    def _depends_tong_muc2_2_3_1(self):
        if self.tong_muc2_2_1 or self.muc2_2_2_1:
            self.tong_muc2_2_3_1 = self.tong_muc2_2_1 + self.muc2_2_2_1
        else:
            self.tong_muc2_2_3_1 = 0

    tong_muc2_2_3_2 = fields.Integer(compute="_depends_tong_muc2_2_3_2")

    @api.multi
    @api.depends('muc2_2_1_3', 'muc2_2_2_3')
    def _depends_tong_muc2_2_3_2(self):
        if self.muc2_2_1_3 or self.muc2_2_2_3:
            self.tong_muc2_2_3_2 = self.muc2_2_1_3 + self.muc2_2_2_3
        else:
            self.tong_muc2_2_3_2 = 0

    tong_muc2_2_3_3 = fields.Integer(compute="_depends_tong_muc2_2_3_3")

    @api.multi
    @api.depends('muc2_2_1_4', 'muc2_2_2_4')
    def _depends_tong_muc2_2_3_3(self):
        if self.muc2_2_1_4 or self.muc2_2_2_4:
            self.tong_muc2_2_3_3 = self.muc2_2_1_4 + self.muc2_2_2_4
        else:
            self.tong_muc2_2_3_3 = 0

    muc2_2_4_1 = fields.Integer(string="現行制度")
    muc2_2_4_2 = fields.Integer()
    tong_muc2_2_4_1 = fields.Integer(compute="_depends_tong_muc2_2_4_1")

    @api.multi
    @api.depends('muc2_2_4_1', 'muc2_2_4_2')
    def _depends_tong_muc2_2_4_1(self):
        if self.muc2_2_4_1 or self.muc2_2_4_2:
            self.tong_muc2_2_4_1 = self.muc2_2_4_1 - self.muc2_2_4_2
        else:
            self.tong_muc2_2_4_1 = 0

    muc2_2_4_3 = fields.Integer()
    muc2_2_4_4 = fields.Integer()

    tong_muc2_2_5_1 = fields.Integer(compute="_depends_tong_muc2_2_5_1")

    @api.multi
    @api.depends('tong_muc2_2_3_1', 'tong_muc2_2_4_1')
    def _depends_tong_muc2_2_5_1(self):
        if self.tong_muc2_2_3_1 or self.tong_muc2_2_4_1:
            self.tong_muc2_2_5_1 = self.tong_muc2_2_3_1 + self.tong_muc2_2_4_1
        else:
            self.tong_muc2_2_5_1 = 0

    tong_muc2_2_5_2 = fields.Integer(compute="_depends_tong_muc2_2_5_2")

    @api.multi
    @api.depends('tong_muc2_2_3_2', 'muc2_2_4_3')
    def _depends_tong_muc2_2_5_2(self):
        if self.tong_muc2_2_3_2 or self.muc2_2_4_3:
            self.tong_muc2_2_5_2 = self.tong_muc2_2_3_2 + self.muc2_2_4_3
        else:
            self.tong_muc2_2_5_2 = 0

    tong_muc2_2_5_3 = fields.Float(compute="_depends_tong_muc2_2_5_3")

    @api.multi
    @api.depends('tong_muc2_2_3_3', 'muc2_2_4_4')
    def _depends_tong_muc2_2_5_3(self):
        if self.tong_muc2_2_3_3 or self.muc2_2_4_4:
            self.tong_muc2_2_5_3 = round((self.tong_muc2_2_3_3 + self.muc2_2_4_4 * 1.5), 1)
        else:
            self.tong_muc2_2_5_3 = 0

    muc2_tinhtoan_1 = fields.Integer(compute="_depends_muc2_tinhtoan_1")

    muc2_tinhtoan_1_1 = fields.Integer(compute="onchange_muc2_tinhtoan_1_1")

    @api.multi
    @api.depends('muc2_tinhtoan_1', 'thaythe_chamdiem')
    def onchange_muc2_tinhtoan_1_1(self):
        if self.thaythe_chamdiem == False:
            if self.muc2_tinhtoan_1 >= 95:
                self.muc2_tinhtoan_1_1 = 10
            elif self.muc2_tinhtoan_1 >= 80 and self.muc2_tinhtoan_1 < 95:
                self.muc2_tinhtoan_1_1 = 5
            elif self.muc2_tinhtoan_1 >= 75 and self.muc2_tinhtoan_1 < 80:
                self.muc2_tinhtoan_1_1 = -10
            else:
                self.muc2_tinhtoan_1_1 = 0
        else:
            self.muc2_2_1_1 = 0
            self.muc2_2_1_2 = 0
            self.muc2_2_2_1 = 0
            self.muc2_2_4_1 = 0
            self.muc2_2_4_2 = 0

    @api.multi
    @api.depends('tong_muc2_1_3', 'tong_muc2_1_4')
    def _depends_muc2_tinhtoan_1(self):
        if self.tong_muc2_1_3 or self.tong_muc2_1_4:
            if self.tong_muc2_1_3:
                self.muc2_tinhtoan_1 = round((self.tong_muc2_1_4 / self.tong_muc2_1_3 * 100), 1)
            else:
                pass
        else:
            self.muc2_tinhtoan_1 = 0

    muc2_tinhtoan_2 = fields.Integer(compute="_depends_muc2_tinhtoan_2")

    muc2_tinhtoan_1_2 = fields.Integer(compute="onchange_muc2_tinhtoan_1_2")

    @api.multi
    @api.depends('muc2_tinhtoan_2','thaythe_chamdiem')
    def onchange_muc2_tinhtoan_1_2(self):
        if self.thaythe_chamdiem == False:
            if self.muc2_tinhtoan_2 >= 80:
                self.muc2_tinhtoan_1_2 = 20
            elif self.muc2_tinhtoan_2 >= 70 and self.muc2_tinhtoan_2 < 80:
                self.muc2_tinhtoan_1_2 = 15
            elif self.muc2_tinhtoan_2 >= 60 and self.muc2_tinhtoan_2 < 70:
                self.muc2_tinhtoan_1_2 = 10
            elif self.muc2_tinhtoan_2 >= 50 and self.muc2_tinhtoan_2 < 60:
                self.muc2_tinhtoan_1_2 = 0
            else:
                self.muc2_tinhtoan_1_2 = -20
        else:
            self.muc2_2_1_1 = 0
            self.muc2_2_1_2 = 0
            self.muc2_2_2_1 = 0
            self.muc2_2_4_1 = 0
            self.muc2_2_4_2 = 0

    @api.multi
    @api.depends('tong_muc2_2_5_1', 'tong_muc2_2_5_3')
    def _depends_muc2_tinhtoan_2(self):
        if self.tong_muc2_2_5_3 or self.tong_muc2_2_5_1:
            if self.tong_muc2_2_5_1:
                self.muc2_tinhtoan_2 = round((self.tong_muc2_2_5_3 * 1.2 / self.tong_muc2_2_5_1 * 100), 1)
            else:
                pass
        else:
            self.muc2_tinhtoan_2 = 0


    muc2_2_1_1_1 = fields.Integer(compute="onchange_muc2_2_1_1_1")
    muc2_2_1_1_2 = fields.Integer(compute="onchange_muc2_2_1_1_2")
    muc_2_2_1 = fields.Integer(string='実技試験')
    muc_2_2_2 = fields.Integer()

    @api.multi
    @api.depends('muc_2_2_1','thaythe_chamdiem')
    def onchange_muc2_2_1_1_1(self):
        if self.thaythe_chamdiem == True:
            if self.muc_2_2_1 >= 2:
                self.muc2_2_1_1_1 = 15
            elif self.muc_2_2_1 == 1:
                self.muc2_2_1_1_1 = 10
            else:
                self.muc2_2_1_1_1 = -15
        else:
            self.muc2_2_1_1_1 = 0
    @api.multi
    @api.depends('muc_2_2_2','thaythe_chamdiem')
    def onchange_muc2_2_1_1_2(self):
        if self.thaythe_chamdiem == True:
            if self.muc_2_2_2 >= 2:
                self.muc2_2_1_1_2 = 5
            elif self.muc_2_2_2 == 1:
                self.muc2_2_1_1_2 = 3
            else:
                self.muc2_2_1_1_2 = 0
        else:
            self.muc2_2_1_1_2 = 0
    muc2_2_1_1_3 = fields.Integer(compute="onchange_muc2_2_1_1_3")
    muc_2_3_1 = fields.Integer()

    @api.multi
    @api.depends('muc_2_3_1')
    def onchange_muc2_2_1_1_3(self):
        if self.muc_2_3_1 >= 2:
            self.muc2_2_1_1_3 = 5
        elif self.muc_2_3_1 == 1:
            self.muc2_2_1_1_3 = 3
        else:
            self.muc2_2_1_1_3 = 0

    muc2_2_1_1_4 = fields.Integer(compute="onchange_muc2_2_1_1_4")
    muc_2_4_1 = fields.Integer()

    @api.multi
    @api.depends('muc_2_4_1')
    def onchange_muc2_2_1_1_4(self):
        if self.muc_2_4_1 >= 1:
            self.muc2_2_1_1_4 = 5
        else:
            self.muc2_2_1_1_4 = 0

    muc3_1_1_1 = fields.Selection(string="①改善命令", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    muc3_1_1_1_1 = fields.Integer(compute="onchange_muc3_1_1_1")

    muc3_1_1_2 = fields.Date()
    muc3_1_1_3 = fields.Selection(string="①改善命令", selection=[('改善実施', '改善実施'), ('改善未実施', '改善未実施')])

    muc3_1_2_1 = fields.Selection(string="①旧制度の「改善命令相当の行政指導」", selection=[('Có', 'Có'), ('Không', 'Không')],
                                  default='Không')

    @api.multi
    @api.depends('muc3_1_1_1', 'muc3_1_1_3', 'muc3_1_2_1', 'muc3_1_2_3')
    def onchange_muc3_1_1_1(self):
        if self.muc3_1_1_1 == 'Không':
            self.muc3_1_1_2 = ''
            self.muc3_1_1_3 = ''
            if self.muc3_1_2_1 == 'Có':
                if self.muc3_1_2_3 == '改善実施':
                    self.muc3_1_1_1_1 = -30
                elif self.muc3_1_2_3 == '改善未実施':
                    self.muc3_1_1_1_1 = -50
            else:
                self.muc3_1_2_2 = ''
                self.muc3_1_2_3 = ''
                self.muc3_1_1_1_1 = 0
        elif self.muc3_1_1_1 == 'Có':
            if self.muc3_1_1_3 == '改善実施':
                self.muc3_1_1_1_1 = -30
            elif self.muc3_1_1_3 == '改善未実施':
                self.muc3_1_1_1_1 = -50
        else:
            self.muc3_1_1_2 = ''
            self.muc3_1_1_3 = ''
            self.muc3_1_2_2 = ''
            self.muc3_1_2_3 = ''

    muc3_1_2_2 = fields.Date()
    muc3_1_2_3 = fields.Selection(string="①改善命令", selection=[('改善実施', '改善実施'), ('改善未実施', '改善未実施')])

    muc3_2_1 = fields.Integer(string="失踪者")
    muc3_2_2 = fields.Integer()
    tong_muc3_2_1 = fields.Float(compute="depends_tong_muc3_2_1")
    muc3_2_2_1 = fields.Integer(compute="onchange_muc3_2_2_1")

    @api.multi
    @api.depends('tong_muc3_2_1')
    def onchange_muc3_2_2_1(self):
        if self.tong_muc3_2_1 == 0:
            self.muc3_2_2_1 = 5
        elif self.tong_muc3_2_1 > 0 and self.tong_muc3_2_1 < 10 or self.muc3_2_1 <= 1:
            self.muc3_2_2_1 = 0
        elif self.tong_muc3_2_1 >= 10 and self.tong_muc3_2_1 < 20 or self.muc3_2_1 <= 2:
            self.muc3_2_2_1 = -5
        elif self.tong_muc3_2_1 >= 20 or self.muc3_2_1 >= 3:
            self.muc3_2_2_1 = -10

    @api.multi
    @api.depends('muc3_2_1', 'muc3_2_2')
    def depends_tong_muc3_2_1(self):
        if self.muc3_2_1 or self.muc3_2_2:
            if self.muc3_2_2:
                self.tong_muc3_2_1 = round((self.muc3_2_1 / self.muc3_2_2 * 100), 1)
            else:
                pass
        else:
            self.tong_muc3_2_1 = 0

    muc3_3 = fields.Selection(string="責めによるべき失踪", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    muc3_3_1 = fields.Integer(compute="onchange_muc3_3")

    @api.multi
    @api.depends('muc3_3')
    def onchange_muc3_3(self):
        if self.muc3_3 == 'Có':
            self.muc3_3_1 = -50
        else:
            self.muc3_3_1 = 0

    muc3_4_1_1 = fields.Integer()
    muc3_4_1_2 = fields.Integer()
    tong_muc3_4_1 = fields.Float(compute="depends_tong_muc3_4_1")
    muc3_4_1_2_1 = fields.Integer(compute="onchange_tong_muc3_4_1")

    @api.multi
    @api.depends('tong_muc3_4_1')
    def onchange_tong_muc3_4_1(self):
        if self.tong_muc3_4_1 >= 15:
            self.muc3_4_1_2_1 = -10
        elif self.tong_muc3_4_1 >= 10 and self.tong_muc3_4_1 < 15:
            self.muc3_4_1_2_1 = -7
        elif self.tong_muc3_4_1 >= 5 and self.tong_muc3_4_1 < 10:
            self.muc3_4_1_2_1 = -5
        elif self.tong_muc3_4_1 >= 0 and self.tong_muc3_4_1 < 5:
            self.muc3_4_1_2_1 = -3

    @api.multi
    @api.depends('muc3_4_1_1', 'muc3_4_1_2')
    def depends_tong_muc3_4_1(self):
        if self.muc3_4_1_1 or self.muc3_4_1_2:
            if self.muc3_4_1_2:
                self.tong_muc3_4_1 = round((self.muc3_4_1_1 / self.muc3_4_1_2 * 100), 1)
            else:
                pass
        else:
            self.tong_muc3_4_1 = 0

    muc3_4_2_1 = fields.Integer()

    muc3_4_3_1 = fields.Integer()
    muc3_4_3_2 = fields.Integer()
    tong_muc3_4_3 = fields.Float(compute="depends_tong_muc3_4_3")
    muc3_4_3_1_1 = fields.Integer(compute="onchange_muc3_4_3_1_1")

    @api.multi
    @api.depends('tong_muc3_4_3')
    def onchange_muc3_4_3_1_1(self):
        if self.tong_muc3_4_3 >= 15:
            self.muc3_4_3_1_1 = -5
        elif self.tong_muc3_4_3 >= 10 and self.tong_muc3_4_3 < 15:
            self.muc3_4_3_1_1 = -4
        elif self.tong_muc3_4_3 >= 5 and self.tong_muc3_4_3 < 10:
            self.muc3_4_3_1_1 = -3
        elif self.tong_muc3_4_3 >= 0 and self.tong_muc3_4_3 < 5:
            self.muc3_4_3_1_1 = -2

    @api.multi
    @api.depends('muc3_4_3_1', 'muc3_4_3_2')
    def depends_tong_muc3_4_3(self):
        if self.muc3_4_3_1 or self.muc3_4_3_2:
            if self.muc3_4_3_2:
                self.tong_muc3_4_3 = round((self.muc3_4_3_1 / self.muc3_4_3_2 * 100), 1)
            else:
                pass
        else:
            self.tong_muc3_4_3 = 0

    muc3_4_4 = fields.Integer()

    muc4_1 = fields.Selection(string="マニュアル等の策定及び関係職員への周知", selection=[('Có', 'Có'), ('Không', 'Không')], default="Có")
    muc4_1_1 = fields.Integer(compute="onchange_muc4_1_1")

    @api.multi
    @api.depends('muc4_1')
    def onchange_muc4_1_1(self):
        if self.muc4_1 == 'Có':
            self.muc4_1_1 = 5
        else:
            self.muc4_1_1 = 0

    muc4_2 = fields.Selection(string="実習先変更支援のポータルサイトへの登録", selection=[('Có', 'Có'), ('Không', 'Không')], default="Có")

    muc4_1_2 = fields.Integer(compute="onchange_muc4_1_2")

    @api.multi
    @api.depends('muc4_2')
    def onchange_muc4_1_2(self):
        if self.muc4_2 == 'Có':
            self.muc4_1_2 = 5
        else:
            self.muc4_1_2 = 0

    muc4_3 = fields.Selection(string="実習先変更の受入技能実習生", selection=[('Có', 'Có'), ('Không', 'Không')], default="Không")

    muc4_1_3 = fields.Integer(compute="onchange_muc4_1_3")

    @api.multi
    @api.depends('muc4_3')
    def onchange_muc4_1_3(self):
        if self.muc4_3 == 'Có':
            self.muc4_1_3 = 5
        else:
            self.muc4_1_3 = 0

    @api.multi
    @api.depends('muc4_3')
    def onchange_muc4_3(self):
        if self.muc4_3 == 'Không':
            self.muc4_3_2 = None
            self.muc4_3_3_1 = ''
            self.muc4_3_3_2 = ''
            self.muc4_3_3_3 = ''
            self.muc4_4 = ''
            self.muc4_5 = ''
            self.muc4_6 = ''

    muc4_3_2 = fields.Char()
    muc4_3_3_1 = fields.Many2one(comodel_name="quoctich.quoctich", string="国籍")
    muc4_3_3_2 = fields.Selection(string="性別", selection=[('Nam', 'Nam'), ('Nữ', 'Nữ')])
    muc4_3_3_3 = fields.Date(string="生年月日")

    muc4_4 = fields.Date('受入年月日')
    muc4_5 = fields.Char('旧所属監理団体名')
    muc4_6 = fields.Char('実習先変更時の技能実習計画認定番号')

    muc5_1 = fields.Selection(string="日本語の教育の支援を行っている実習実施者への支援の概要", selection=[('Có', 'Có'), ('Không', 'Không'), ],
                              default='Có', )
    muc5_1_1 = fields.Integer(compute="onchange_muc5_1_1")

    @api.multi
    @api.depends('muc5_1')
    def onchange_muc5_1_1(self):
        if self.muc5_1 == 'Có':
            self.muc5_1_1 = 4
        else:
            self.muc5_1_1 = 0
            self.muc5_2 = ''

    muc5_2 = fields.Text()

    muc5_3 = fields.Selection(string="地域社会との交流を行う機会をアレンジしている実習実施者への支援の概要",
                              selection=[('Có', 'Có'), ('Không', 'Không'), ], default='Có', )
    muc5_1_3 = fields.Integer(compute="onchange_muc5_1_3")

    @api.multi
    @api.depends('muc5_3')
    def onchange_muc5_1_3(self):
        if self.muc5_3 == 'Có':
            self.muc5_1_3 = 3
        else:
            self.muc5_1_3 = 0
            self.muc5_4 = ''

    muc5_4 = fields.Text()

    muc5_5 = fields.Selection(string="日本の文化を学ぶ機会をアレンジしている実習実施者への支援の概要", selection=[('Có', 'Có'), ('Không', 'Không'), ],
                              default='Có', )
    muc5_1_5 = fields.Integer(compute="onchange_muc5_1_5")

    @api.multi
    @api.depends('muc5_5')
    def onchange_muc5_1_5(self):
        if self.muc5_5 == 'Có':
            self.muc5_1_5 = 3
        else:
            self.muc5_1_5 = 0
            self.muc5_6 = ''

    muc5_6 = fields.Text()

    end = fields.Integer(compute="onchange_end")

    @api.multi
    @api.depends('muc1_1_1', 'muc1_2_1_1', 'muc1_3_1_1', 'muc1_4_1_1', 'muc1_5_1', 'muc1_6_1_1', 'muc1_7_1_1',
                 'muc2_tinhtoan_1_1', 'muc2_tinhtoan_1_2', 'muc2_2_1_1_1', 'muc2_2_1_1_2', 'muc2_2_1_1_3',
                 'muc2_2_1_1_4', 'muc3_1_1_1_1', 'muc3_2_2_1', 'muc3_3_1', 'muc3_4_1_2_1', 'muc3_4_3_1_1', 'muc4_1_1',
                 'muc4_1_2', 'muc4_1_3', 'muc5_1_1', 'muc5_1_3', 'muc5_1_5')
    def onchange_end(self):
        if self.muc1_1_1 or self.muc1_2_1_1 or self.muc1_3_1_1 or self.muc1_4_1_1 or self.muc1_5_1 or self.muc1_6_1_1 \
                or self.muc1_7_1_1 or self.muc2_tinhtoan_1_1 or self.muc2_tinhtoan_1_2 or self.muc2_2_1_1_1 \
                or self.muc2_2_1_1_2 or self.muc2_2_1_1_3 or self.muc2_2_1_1_4 or self.muc3_1_1_1_1 or self.muc3_2_2_1 \
                or self.muc3_3_1 or self.muc3_4_1_2_1 or self.muc3_4_3_1_1 or self.muc4_1_1 or self.muc4_1_2 \
                or self.muc4_1_3 or self.muc5_1_1 or self.muc5_1_3 or self.muc5_1_5:
            self.end = self.muc1_1_1 + self.muc1_2_1_1 + self.muc1_3_1_1 + self.muc1_4_1_1 + self.muc1_5_1 + self.muc1_6_1_1 + self.muc1_7_1_1 + self.muc2_tinhtoan_1_1 + self.muc2_tinhtoan_1_2 + self.muc2_2_1_1_1 + self.muc2_2_1_1_2 + self.muc2_2_1_1_3 + self.muc2_2_1_1_4 + self.muc3_1_1_1_1 + self.muc3_2_2_1 + self.muc3_3_1 + self.muc3_4_1_2_1 + self.muc3_4_3_1_1 + self.muc4_1_1 + self.muc4_1_2 + self.muc4_1_3 + self.muc5_1_1 + self.muc5_1_3 + self.muc5_1_5
            print(self.end, '1')
        else:
            print(self.end, '2')

    @api.multi
    def baocao_chamdiemnghiepdoan_button(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/chamdiemnghiepdoan/download/%s' % (self.id),
            'target': 'self', }

    @api.multi
    def baocao_chamdiemnghiepdoan(self, chamdiem):
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_98")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            if chamdiem.muc1_1 == u"Có":
                docdata.remove_shape(u'id="56" name="Oval 56"')
                context['nd_0001'] = '5'
            elif chamdiem.muc1_1 == u"Không":
                docdata.remove_shape(u'id="55" name="Oval 55"')
                context['nd_0001'] = '0'
            else:
                docdata.remove_shape(u'id="56" name="Oval 56"')
                docdata.remove_shape(u'id="55" name="Oval 55"')
                context['nd_0001'] = '0'

            context['nd_0003'] = kiemtra(chamdiem.muc1_2_1)
            context['nd_0004'] = kiemtra(chamdiem.muc1_2_2)
            context['nd_0005'] = kiemtra(chamdiem.tong_muc1_2)

            if 0 < chamdiem.tong_muc1_2 < 5:
                context['nd_0002'] = 15
            elif 5 <= chamdiem.tong_muc1_2 < 10:
                context['nd_0002'] = 7
            else:
                context['nd_0002'] = 0

            context['nd_0007'] = kiemtra(chamdiem.muc1_3_1)
            context['nd_0008'] = kiemtra(chamdiem.muc1_3_2)
            context['nd_0009'] = kiemtra(round(chamdiem.tong_muc1_3,2))

            if chamdiem.tong_muc1_3 >= 60:
                context['nd_0006'] = 10
            elif 50 <= chamdiem.tong_muc1_3 < 60:
                context['nd_0006'] = 5
            else:
                context['nd_0006'] = 0

            if chamdiem.muc1_4_1 == u"Có":
                docdata.remove_shape(u'id="58" name="Oval 58"')
                context['nd_0011'] = kiemtra(chamdiem.muc1_4_2)
                context['nd_0010'] = 5
            elif chamdiem.muc1_4_1 == u"Không":
                docdata.remove_shape(u'id="57" name="Oval 57"')
                context['nd_0011'] = ''
                context['nd_0010'] = 0
            else:
                docdata.remove_shape(u'id="57" name="Oval 57"')
                docdata.remove_shape(u'id="58" name="Oval 58"')
                context['nd_0011'] = ''
                context['nd_0010'] = 0

            if chamdiem.muc1_5 == u"Có":
                context['nd_0012'] = 5
                docdata.remove_shape(u'id="60" name="Oval 60"')
            elif chamdiem.muc1_5 == u"Không":
                context['nd_0012'] = 0
                docdata.remove_shape(u'id="59" name="Oval 59"')
            else:
                context['nd_0012'] = 0

            if chamdiem.muc1_6_1 == u"Có":
                context['nd_0013'] = 5
                context['nd_0014'] = kiemtra(chamdiem.muc1_6_2)
            elif chamdiem.muc1_6_1 == u"Không":
                context['nd_0013'] = 0
                context['nd_0014'] = ''
            else:
                context['nd_0013'] = 0
                context['nd_0014'] = ''

            if chamdiem.muc1_7_1 == u"Có":
                context['nd_0015'] = 5
                context['nd_0016'] = kiemtra(chamdiem.muc1_7_2)
            elif chamdiem.muc1_7_1 == u"Không":
                context['nd_0015'] = 0
                context['nd_0016'] = ''
            else:
                context['nd_0015'] = 0
                context['nd_0016'] = ''

            #２ 技能等の修得等に係る実績
            context['nd_0018'] = kiemtra(chamdiem.tong_muc2_1_3)
            context['nd_0019'] = kiemtra(chamdiem.muc2_1_1_1)
            context['nd_0020'] = kiemtra(chamdiem.muc2_1_1_2)
            context['nd_0021'] = kiemtra(chamdiem.tong_muc2_1_1)

            context['nd_0022'] = kiemtra(chamdiem.muc2_1_2_1)
            context['nd_0023'] = kiemtra(chamdiem.muc2_1_2_2)
            context['nd_0024'] = kiemtra(chamdiem.tong_muc2_1_2)

            context['nd_0025'] = kiemtra(chamdiem.tong_muc2_1_4)
            context['nd_0026'] = kiemtra(chamdiem.muc2_1_3_1)
            context['nd_0027'] = kiemtra(chamdiem.muc2_1_3_2)

            if chamdiem.tong_muc2_1_3 and chamdiem.tong_muc2_1_4:
                context['nd_0030'] = (chamdiem.tong_muc2_1_4 / chamdiem.tong_muc2_1_3) * 100
                if context['nd_0030'] >= 95:
                    context['nd_0017'] = 10
                elif 80 <= context['nd_0030'] < 95:
                    context['nd_0017'] = 5
                elif 75 <= context['nd_0030'] < 80:
                    context['nd_0017'] = 0
                else:
                    context['nd_0017'] = -10

            if chamdiem.thaythe_chamdiem == False:
                context['nd_0033'] = chamdiem.muc2_2_1_1
                context['nd_0034'] = chamdiem.muc2_2_1_2
                context['nd_0035'] = chamdiem.tong_muc2_2_1
                context['nd_0036'] = chamdiem.muc2_2_2_1
                context['nd_0037'] = chamdiem.muc2_2_4_1
                context['nd_0038'] = chamdiem.muc2_2_4_2
                context['nd_0039'] = chamdiem.tong_muc2_2_4_1
                context['nd_0032'] = chamdiem.tong_muc2_2_1 + chamdiem.muc2_2_2_1
                context['nd_0031'] = chamdiem.tong_muc2_2_1 + chamdiem.muc2_2_2_1 + chamdiem.tong_muc2_2_4_1
                context['nd_0041'] = chamdiem.muc2_2_1_4 + chamdiem.muc2_2_2_4
                context['nd_0042'] = chamdiem.muc2_2_1_4
                context['nd_0043'] = chamdiem.muc2_2_2_4
                context['nd_0044'] = chamdiem.muc2_2_4_4
                context['nd_0040'] = chamdiem.muc2_2_1_4 + chamdiem.muc2_2_2_4 + chamdiem.muc2_2_4_4
                context['nd_0048'] = chamdiem.muc2_tinhtoan_1_2

                context['nd_0049'] = ''
                context['nd_0050'] = ''
                context['nd_0048_1'] = ''
                context['nd_0051'] = ''
                context['nd_0052'] = ''
                context['nd_0055'] = ''
            else:
                context['nd_0033'] = ''
                context['nd_0034'] = ''
                context['nd_0035'] = ''
                context['nd_0036'] = ''
                context['nd_0037'] = ''
                context['nd_0038'] = ''
                context['nd_0039'] = ''
                context['nd_0032'] = ''
                context['nd_0031'] = ''
                context['nd_0041'] = ''
                context['nd_0042'] = ''
                context['nd_0043'] = ''
                context['nd_0044'] = ''
                context['nd_0040'] = ''
                context['nd_0045'] = ''
                context['nd_0046'] = ''
                context['nd_0047'] = ''
                context['nd_0049'] = chamdiem.muc2_2_1_4 + chamdiem.muc2_2_2_4
                context['nd_0050'] = chamdiem.muc_2_2_1
                context['nd_0048_1'] = chamdiem.muc2_2_1_1_1
                context['nd_0051'] = chamdiem.muc2_2_4_4
                context['nd_0052'] = chamdiem.muc_2_2_2
                context['nd_0055'] = chamdiem.muc2_2_1_1_2
            context['nd_0053'] = chamdiem.tong_muc2_2_5_2
            context['nd_0054'] = chamdiem.muc_2_3_1
            context['nd_0056'] = chamdiem.muc2_2_1_1_3
            context['nd_0058'] = chamdiem.muc2_2_1_1_4
            context['nd_0057'] = chamdiem.muc_2_4_1


            # ３法令違反・問題の発生状況
            if chamdiem.muc3_1_1_1 == u"Có":
                docdata.remove_shape(u'id="61" name="Oval 61"')
                context['nd_0062'] = convert_date(chamdiem.muc3_1_1_2)
                if chamdiem.muc3_1_1_3 == '改善実施':
                    docdata.remove_shape(u'id="64" name="Oval 64"')
                elif chamdiem.muc3_1_1_3 == '改善未実施':
                    docdata.remove_shape(u'id="63" name="Oval 63"')
                else:
                    docdata.remove_shape(u'id="63" name="Oval 63"')
                    docdata.remove_shape(u'id="64" name="Oval 64"')

                docdata.remove_shape(u'id="67" name="Oval 67"')
                docdata.remove_shape(u'id="68" name="Oval 68"')
                docdata.remove_shape(u'id="66" name="Oval 66"')
                docdata.remove_shape(u'id="65" name="Oval 65"')
                context['nd_0063'] = '年　月　日'
            elif chamdiem.muc3_1_1_1 == u"Không":
                docdata.remove_shape(u'id="62" name="Oval 62"')
                docdata.remove_shape(u'id="63" name="Oval 63"')
                docdata.remove_shape(u'id="64" name="Oval 64"')
                context['nd_0062'] = '年　月　日'

                if chamdiem.muc3_1_2_1 == u"Có":
                    docdata.remove_shape(u'id="67" name="Oval 67"')
                    context['nd_0063'] = convert_date(chamdiem.muc3_1_2_2)
                    if chamdiem.muc3_1_2_3 == '改善実施':
                        docdata.remove_shape(u'id="66" name="Oval 66"')
                    elif chamdiem.muc3_1_2_3 == '改善未実施':
                        docdata.remove_shape(u'id="65" name="Oval 65"')
                    else:
                        docdata.remove_shape(u'id="66" name="Oval 66"')
                        docdata.remove_shape(u'id="65" name="Oval 65"')
                elif chamdiem.muc3_1_2_1 == u"Không":
                    docdata.remove_shape(u'id="68" name="Oval 68"')
                    context['nd_0063'] = '年　月　日'
                    docdata.remove_shape(u'id="66" name="Oval 66"')
                    docdata.remove_shape(u'id="65" name="Oval 65"')
                else:
                    docdata.remove_shape(u'id="66" name="Oval 66"')
                    docdata.remove_shape(u'id="65" name="Oval 65"')
            else:
                docdata.remove_shape(u'id="62" name="Oval 62"')
                docdata.remove_shape(u'id="61" name="Oval 61"')
                docdata.remove_shape(u'id="63" name="Oval 63"')
                docdata.remove_shape(u'id="64" name="Oval 64"')
                context['nd_0001'] = 0

            context['nd_0059'] = kiemtra(chamdiem.muc3_1_1_1_1)

            context['nd_0064'] = kiemtra(chamdiem.muc3_2_1)
            context['nd_0065'] = kiemtra(chamdiem.muc3_2_2)
            context['nd_0066'] = kiemtra(chamdiem.tong_muc3_2_1)

            context['nd_0060'] = kiemtra(chamdiem.muc3_2_2_1)

            if chamdiem.muc3_3 == "Có":
                docdata.remove_shape(u'id="69" name="Oval 69"')
            elif chamdiem.muc3_3 == "Không":
                docdata.remove_shape(u'id="70" name="Oval 70"')
            context['nd_0067'] = chamdiem.muc3_3_1 if chamdiem.muc3_3_1 else 0
            context['nd_0068'] = chamdiem.muc3_4_1_2_1 + chamdiem.muc3_4_3_1_1 if chamdiem.muc3_4_1_2_1 or chamdiem.muc3_4_3_1_1 else 0
            context['nd_0069'] = kiemtra(chamdiem.muc3_4_1_1)
            context['nd_0070'] = kiemtra(chamdiem.muc3_4_1_2)
            context['nd_0071'] = kiemtra(chamdiem.tong_muc3_4_1)
            context['nd_0072'] = kiemtra(chamdiem.muc3_4_2_1)
            context['nd_0073'] = kiemtra(chamdiem.muc3_4_3_1)
            context['nd_0074'] = kiemtra(chamdiem.muc3_4_3_2)
            context['nd_0075'] = kiemtra(chamdiem.tong_muc3_4_3)
            context['nd_0076'] = kiemtra(chamdiem.muc3_4_4)

            # ４相談・支援体制
            if chamdiem.muc4_1 == "Có":
                docdata.remove_shape(u'id="72" name="Oval 72"')
            elif chamdiem.muc4_1 == u"Không":
                docdata.remove_shape(u'id="71" name="Oval 71"')
            else:
                docdata.remove_shape(u'id="71" name="Oval 71"')
                docdata.remove_shape(u'id="72" name="Oval 72"')
            context['nd_0077'] = kiemtra(chamdiem.muc4_1_1)


            if chamdiem.muc4_2 == "Có":
                docdata.remove_shape(u'id="78" name="Oval 78"')
            elif chamdiem.muc4_2 == u"Không":
                docdata.remove_shape(u'id="77" name="Oval 77"')
            else:
                docdata.remove_shape(u'id="77" name="Oval 77"')
                docdata.remove_shape(u'id="78" name="Oval 78"')
            context['nd_0078'] = kiemtra(chamdiem.muc4_1_2)


            if chamdiem.muc4_3 == "Có":
                context['nd_0079'] = 5
                docdata.remove_shape(u'id="75" name="Oval 75"')
                context['nd_0080'] = kiemtra(chamdiem.muc4_3_2)
                context['nd_0081'] = kiemtra(chamdiem.muc4_3_3_1.name_quoctich)
                context['nd_0082'] = convert_date(chamdiem.muc4_3_3_3)
                context['nd_0083'] = convert_date(chamdiem.muc4_4)
                context['nd_0084'] = kiemtra(chamdiem.muc4_5)
                context['nd_0085'] = kiemtra(chamdiem.muc4_6)
                if chamdiem.muc4_3_3_2 == "Nam":
                    docdata.remove_shape(u'id="80" name="Oval 80"')
                elif chamdiem.muc4_3_3_2 == "Nữ":
                    docdata.remove_shape(u'id="79" name="Oval 79"')
                else:
                    docdata.remove_shape(u'id="79" name="Oval 79"')
                    docdata.remove_shape(u'id="80" name="Oval 80"')

            elif chamdiem.muc4_3 == u"Không":
                context['nd_0079'] = 0
                docdata.remove_shape(u'id="76" name="Oval 76"')
                context['nd_0080'] = ''
                context['nd_0081'] = ''
                context['nd_0082'] = ''
                context['nd_0083'] = ''
                context['nd_0084'] = ''
                context['nd_0085'] = ''
                docdata.remove_shape(u'id="79" name="Oval 79"')
                docdata.remove_shape(u'id="80" name="Oval 80"')
            else:
                context['nd_0079'] = 0
                docdata.remove_shape(u'id="76" name="Oval 76"')
                docdata.remove_shape(u'id="75" name="Oval 75"')
                docdata.remove_shape(u'id="79" name="Oval 79"')
                docdata.remove_shape(u'id="80" name="Oval 80"')
                context['nd_0080'] = ''
                context['nd_0081'] = ''
                context['nd_0082'] = ''
                context['nd_0083'] = ''
                context['nd_0084'] = ''
                context['nd_0085'] = ''

            # ５地域社会との共生
            if chamdiem.muc5_1 == "Có":
                context['nd_0087'] = kiemtra(chamdiem.muc5_2)
            else:
                context['nd_0087'] = ''
            context['nd_0086'] = kiemtra(chamdiem.muc5_1_1)

            if chamdiem.muc5_3 == "Có":
                context['nd_0089'] = kiemtra(chamdiem.muc5_4)
            else:
                context['nd_0089'] = ''
            context['nd_0088'] = kiemtra(chamdiem.muc5_1_3)

            if chamdiem.muc5_5 == "Có":
                context['nd_0091'] = kiemtra(chamdiem.muc5_6)
            else:
                context['nd_0091'] = ''
            context['nd_0090'] = kiemtra(chamdiem.muc5_1_5)

            # 以上の記載内容は事実と相違ありません。
            context['nd_0092'] = chamdiem.end
            context['nd_0093'] = convert_date(chamdiem.ngaylap_vanban_chamdiem)
            context['nd_0094'] = kiemtra(chamdiem.nghiepdoan_chamdiem.ten_han_nghiepdoan)
            nd_0095 = kiemtra(chamdiem.nghiepdoan_chamdiem.chucvu_nguoinhap) + ' ' +kiemtra(
                chamdiem.nghiepdoan_chamdiem.ten_nguoinhap)
            context['nd_0095'] = kiemtra(nd_0095)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None
