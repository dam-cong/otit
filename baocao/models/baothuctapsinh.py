# -*- coding: utf-8 -*-

from odoo import models, fields, api


class BaoTTS(models.Model):
    _inherit = 'thuctapsinh.thuctapsinh'

    ngayphongvan_truoc_kansa = fields.Date(string='Ngày phỏng vấn lần trước')
    noidung_phongvan_kansa = fields.Char(string='Nội dung phỏng vấn')
