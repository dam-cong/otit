# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime
import logging
from tempfile import TemporaryFile, NamedTemporaryFile
from io import BytesIO, StringIO
import os
import codecs
from docxtpl import DocxTemplate

_logger = logging.getLogger(__name__)


def kiemtra(data):
    if data:
        return data
    else:
        return ''


def convert_date(self):
    if self:
        return str(self.year) + '年' + \
               str(self.month) + '月' + \
               str(self.day) + '日'
    else:
        return '年　月　日'


def nganhnghe(self):
    if self:
        return self.split(":")[1]
    else:
        return ' '


class QuanLy(models.Model):
    _name = 'trachnhiem.quanly'
    _rec_name = 'nguoi_trachnhiem_quanly'

    nguoi_trachnhiem_quanly = fields.Char(string='Người chịu trách nhiệm quản lý')
    trachnhiem_chidao = fields.Many2one(comodel_name='baocao.chidao')


# class XiNghiepTham(models.Model):
#     _name = 'xinghiep.tham'
#     _rec_name = 'xinghiep_tham'
#
#     xinghiep_tham = fields.Char(string='Xí nghiệp tới thăm')
#     xinghiep_tham_chidao = fields.Many2one(comodel_name='baocao.chidao')


class CoChe(models.Model):
    _name = 'xinghiep.coche'
    _rec_name = 'xinghiep_coche'

    xinghiep_coche = fields.Char(string='Họ tên')
    xinghiep_coche_chidao = fields.Many2one(comodel_name='baocao.chidao')


class ChiDao(models.Model):
    _name = 'baocao.chidao'
    _rec_name = 'xinghiep_chidao'

    xinghiep_chidao = fields.Many2one(comodel_name='xinghiep.xinghiep', string='Xí nghiệp',required=True)
    xinghiep_chinhanh_chidao = fields.Many2one(comodel_name='xinghiep.chinhanh', string='Chi nhánh xí nghiệp')
    ngay_baocao = fields.Date(string='Ngày báo cáo',  default=fields.Date.today())

    # 1. Thông tin cơ bản đăng ký chỉ đạo
    ten_bienban_chidao = fields.Char(string='Tên biên bản chỉ đạo')
    batdau_thamquan = fields.Date(string='Bắt đầu')
    ketthuc_thamquan = fields.Date(string='Kết thúc')
    # 2. Ngành nghề
    ma_congviec_chuyennhuong = fields.Char(string='Mã công việc', compute='onchange_master_congviec_chuyennhuong')
    nganhnghe_congviec_chuyennhuong = fields.Many2one(comodel_name='nganhnghe.nganhnghe', string='Ngành nghề')
    loaicongviec_congviec_chuyennhuong = fields.Many2one(comodel_name='loaicongviec.loaicongviec',
                                                         string='Loại công việc')
    congviec_chuyennhuong = fields.Many2one(comodel_name='congviec.congviec', string='Công việc')

    @api.onchange('congviec_chuyennhuong')
    def onchange_master_congviec_chuyennhuong(self):
        if self.congviec_chuyennhuong:
            self.ma_congviec_chuyennhuong = self.congviec_chuyennhuong.ma_congviec

    ma_congviec_nhieucongviec = fields.Char(string='Mã công việc', compute='onchange_master_congviec_nhieucongviec')
    nganhnghe_congviec_nhieucongviec = fields.Many2one(comodel_name='nganhnghe.nganhnghe', string='Ngành nghề')
    loaicongviec_congviec_nhieucongviec = fields.Many2one(comodel_name='loaicongviec.loaicongviec',
                                                          string='Loại công việc')
    congviec_nhieucongviec = fields.Many2one(comodel_name='congviec.congviec', string='Công việc')

    @api.onchange('congviec_nhieucongviec')
    def onchange_master_congviec_nhieucongviec(self):
        if self.congviec_nhieucongviec:
            self.ma_congviec_nhieucongviec = self.congviec_nhieucongviec.ma_congviec

    # 3. Mục tiêu đạt được
    loaikithi_mot_chidao = fields.Many2one(comodel_name="thuctapsinh.loaikiemtra",
                                           string="Loại kiểm tra")  #
    kinang_mot_chidao = fields.Many2one(comodel_name='thuctapsinh.kithi',
                                        string="Tên kì thi")  #
    capdo_mot_chidao = fields.Many2one(comodel_name='thuctapsinh.capdo', string="Cấp độ")  #

    loaikithi_hai_chidao = fields.Many2one(comodel_name="thuctapsinh.loaikiemtra",
                                           string="Loại kiểm tra")  #
    kinang_hai_chidao = fields.Many2one(comodel_name='thuctapsinh.kithi',
                                        string="Tên kì thi")  #
    capdo_hai_chidao = fields.Many2one(comodel_name='thuctapsinh.capdo', string="Cấp độ")  #

    thoigian_chidao = fields.Text(string='Thời gian')
    phuongphap_chidao = fields.Text(string='Phương pháp xác nhận')

    # 4. Danh sách thực tập sinh
    danhsach_thuctapsin_chidao = fields.Many2many(comodel_name='thuctapsinh.thuctapsinh',
                                                  string='Danh sách thực tâp sinh')
    # 5. Cơ chế chỉ đạo nghiệp đoàn
    trachnhiem_chidao = fields.One2many(comodel_name='trachnhiem.quanly', inverse_name='trachnhiem_chidao',
                                        string='Cơ chế chỉ đạo')

    @api.onchange('xinghiep_chidao')
    def _onchange_xinghiep_chidao(self):
        trachnhiem_chidao = self.env['nhanvien.nhanvien'].search(
            [('vaitro_three', '=', True), ('nhanvien_xinghiep', '=', self.xinghiep_chidao.id)])

        trachnhiem_nghiepdoan = self.env['nghiepdoan.nhanvientts'].search(
            [('quanly_vaitro_nghiepdoan', '=', True), ('danhsach_nhanvien_tts', '=', 1)])

        vals = []
        for chidao in trachnhiem_chidao:
            vals_row = {}
            vals_row['xinghiep_coche'] = chidao.ten_han_nhanvien
            chidao_row = self.env['xinghiep.coche'].create(vals_row)
            vals.append(chidao_row.id)
        self.xinghiep_coche_chidao = [(6, 0, vals)]

        vals_nghiepdoan = []
        for chidao_nghiepdoan in trachnhiem_nghiepdoan:
            vals_row_nghiepdoan = {}
            vals_row_nghiepdoan['nguoi_trachnhiem_quanly'] = chidao_nghiepdoan.ten_nvien_han_nghiepdoan
            chidao_row_nghiepdoan = self.env['trachnhiem.quanly'].create(vals_row_nghiepdoan)
            vals_nghiepdoan.append(chidao_row_nghiepdoan.id)
        self.trachnhiem_chidao = [(6, 0, vals_nghiepdoan)]

    xinghiep_tham_chidao_1 = fields.Char(string='Xí nghiệp tới thăm')
    xinghiep_tham_chidao_2 = fields.Char(string='Xí nghiệp tới thăm')
    xinghiep_tham_chidao_3 = fields.Char(string='Xí nghiệp tới thăm')

    # 6. Cơ chế chỉ đạo xí nghiệp
    xinghiep_coche_chidao = fields.One2many(comodel_name='xinghiep.coche', inverse_name='xinghiep_coche_chidao',
                                            string='Xí nghiệp cơ chế')
    # 7. Ghi chú
    ghichu_chidao = fields.Text(string='Ghi chú')

    # 8. Nội dung chỉ đạo
    ngay_thangthunhat = fields.Date()  # bc_0401
    ngay_thangthuhai = fields.Date()  # bc_0402
    ngay_thangthuba = fields.Date()  # bc_0403
    ngay_thangthubon = fields.Date()  # bc_0404
    ngay_thangthunam = fields.Date()  # bc_0405
    ngay_thangthusau = fields.Date()  # bc_0406
    ngay_thangthubay = fields.Date()  # bc_0407
    ngay_thangthutam = fields.Date()  # bc_0408
    ngay_thangthuchin = fields.Date()  # bc_0409
    ngay_thangthumuoi = fields.Date()  # bc_0410
    ngay_thangthumuoimot = fields.Date()  # bc_0411
    ngay_thangthumuoihai = fields.Date()  # bc_0412

    tiendo_thangthunhat = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0413
    tiendo_thangthuhai = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0414
    tiendo_thangthuba = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0415
    tiendo_thangthubon = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0416
    tiendo_thangthunam = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0417
    tiendo_thangthusau = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0418
    tiendo_thangthubay = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0419
    tiendo_thangthutam = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0420
    tiendo_thangthuchin = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0421
    tiendo_thangthumuoi = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0422
    tiendo_thangthumuoimot = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0423
    tiendo_thangthumuoihai = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0424

    mucdo_tiepthu_thangthunhat = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                  default='○')  # bc_0425
    mucdo_tiepthu_thangthuhai = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0426
    mucdo_tiepthu_thangthuba = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0427
    mucdo_tiepthu_thangthubon = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0428
    mucdo_tiepthu_thangthunam = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0429
    mucdo_tiepthu_thangthusau = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0430
    mucdo_tiepthu_thangthubay = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0431
    mucdo_tiepthu_thangthutam = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0432
    mucdo_tiepthu_thangthuchin = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                  default='○')  # bc_0433
    mucdo_tiepthu_thangthumuoi = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                  default='○')  # bc_0434
    mucdo_tiepthu_thangthumuoimot = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                     default='○')  # bc_0435
    mucdo_tiepthu_thangthumuoihai = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                     default='○')  # bc_0436

    phanbo_thoigian_thangthunhat = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                    default='○')  # bc_0437
    phanbo_thoigian_thangthuhai = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                   default='○')  # bc_0438
    phanbo_thoigian_thangthuba = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                  default='○')  # bc_0439
    phanbo_thoigian_thangthubon = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                   default='○')  # bc_0440
    phanbo_thoigian_thangthunam = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                   default='○')  # bc_0441
    phanbo_thoigian_thangthusau = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                   default='○')  # bc_0442
    phanbo_thoigian_thangthubay = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                   default='○')  # bc_0443
    phanbo_thoigian_thangthutam = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                   default='○')  # bc_0444
    phanbo_thoigian_thangthuchin = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                    default='○')  # bc_0445
    phanbo_thoigian_thangthumuoi = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                    default='○')  # bc_0446
    phanbo_thoigian_thangthumuoimot = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                       default='○')  # bc_0447
    phanbo_thoigian_thangthumuoihai = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                       default='○')  # bc_0448

    thaido_thuctap_thangthunhat = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                   default='○')  # bc_0449
    thaido_thuctap_thangthuhai = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                  default='○')  # bc_0450
    thaido_thuctap_thangthuba = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0451
    thaido_thuctap_thangthubon = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                  default='○')  # bc_0452
    thaido_thuctap_thangthunam = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                  default='○')  # bc_0453
    thaido_thuctap_thangthusau = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                  default='○')  # bc_0454
    thaido_thuctap_thangthubay = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                  default='○')  # bc_0455
    thaido_thuctap_thangthutam = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                  default='○')  # bc_0456
    thaido_thuctap_thangthuchin = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                   default='○')  # bc_0457
    thaido_thuctap_thangthumuoi = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                   default='○')  # bc_0458
    thaido_thuctap_thangthumuoimot = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                      default='○')  # bc_0459
    thaido_thuctap_thangthumuoihai = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                      default='○')  # bc_0460

    mucdo_tichcu_thangthunhat = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0461
    mucdo_tichcu_thangthuhai = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0462
    mucdo_tichcu_thangthuba = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0463
    mucdo_tichcu_thangthubon = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0464
    mucdo_tichcu_thangthunam = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0465
    mucdo_tichcu_thangthusau = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0466
    mucdo_tichcu_thangthubay = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0467
    mucdo_tichcu_thangthutam = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0468
    mucdo_tichcu_thangthuchin = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0469
    mucdo_tichcu_thangthumuoi = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0470
    mucdo_tichcu_thangthumuoimot = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                    default='○')  # bc_0471
    mucdo_tichcu_thangthumuoihai = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                    default='○')  # bc_0472

    mucdo_hieu_thangthunhat = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0473
    mucdo_hieu_thangthuhai = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0474
    mucdo_hieu_thangthuba = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0475
    mucdo_hieu_thangthubon = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0476
    mucdo_hieu_thangthunam = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0477
    mucdo_hieu_thangthusau = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0478
    mucdo_hieu_thangthubay = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0479
    mucdo_hieu_thangthutam = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0480
    mucdo_hieu_thangthuchin = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0481
    mucdo_hieu_thangthumuoi = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0482
    mucdo_hieu_thangthumuoimot = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                  default='○')  # bc_0483
    mucdo_hieu_thangthumuoihai = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                  default='○')  # bc_0484

    thaido_song_thangthunhat = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0485
    thaido_song_thangthuhai = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0486
    thaido_song_thangthuba = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0487
    thaido_song_thangthubon = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0488
    thaido_song_thangthunam = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0489
    thaido_song_thangthusau = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0490
    thaido_song_thangthubay = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0491
    thaido_song_thangthutam = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0492
    thaido_song_thangthuchin = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0493
    thaido_song_thangthumuoi = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')], default='○')  # bc_0494
    thaido_song_thangthumuoimot = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                   default='○')  # bc_0495
    thaido_song_thangthumuoihai = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                   default='○')  # bc_0496

    vipham_phapluat_thangthunhat = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                    default='○')  # bc_0497
    vipham_phapluat_thangthuhai = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                   default='○')  # bc_0498
    vipham_phapluat_thangthuba = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                  default='○')  # bc_0499
    vipham_phapluat_thangthubon = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                   default='○')  # bc_0500
    vipham_phapluat_thangthunam = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                   default='○')  # bc_0501
    vipham_phapluat_thangthusau = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                   default='○')  # bc_0502
    vipham_phapluat_thangthubay = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                   default='○')  # bc_0503
    vipham_phapluat_thangthutam = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                   default='○')  # bc_0504
    vipham_phapluat_thangthuchin = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                    default='○')  # bc_0505
    vipham_phapluat_thangthumuoi = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                    default='○')  # bc_0506
    vipham_phapluat_thangthumuoimot = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                       default='○')  # bc_0507
    vipham_phapluat_thangthumuoihai = fields.Selection(selection=[('○', '○'), ('△', '△'), ('×', '×')],
                                                       default='○')  # bc_0508

    ghichu_thangthunhat = fields.Char()  # bc_0509
    ghichu_thangthuhai = fields.Char()  # bc_0510
    ghichu_thangthuba = fields.Char()  # bc_0511
    ghichu_thangthubon = fields.Char()  # bc_0512
    ghichu_thangthunam = fields.Char()  # bc_0513
    ghichu_thangthusau = fields.Char()  # bc_0514
    ghichu_thangthubay = fields.Char()  # bc_0515
    ghichu_thangthutam = fields.Char()  # bc_0516
    ghichu_thangthuchin = fields.Char()  # bc_0517
    ghichu_thangthumuoi = fields.Char()  # bc_0518
    ghichu_thangthumuoimot = fields.Char()  # bc_0519
    ghichu_thangthumuoihai = fields.Char()  # bc_0520

    @api.multi
    def baocao_chidao_button(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/chidao/download/%s' % (self.id),
            'target': 'self', }

    @api.multi
    def baocao_chidao_41(self, chidao):

        nghiepdoan = self.env['nghiepdoan.nghiepdoan'].search([('id', '=', 1)], limit=1)
        nhanvien_nghiepdoan = int(len(chidao.trachnhiem_chidao))
        coche_chidao = int(len(chidao.xinghiep_coche_chidao))

        if nhanvien_nghiepdoan > 1 and coche_chidao > 1:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_41_3")], limit=1)
            print('file_41_3')
        elif nhanvien_nghiepdoan > 1 and coche_chidao <= 1:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_41_1")], limit=1)
            print('file_41_1')
        elif nhanvien_nghiepdoan <= 1 and coche_chidao > 1:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_41_2")], limit=1)
            print('file_41_2')
        else:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_41")], limit=1)
            print('file_41')

        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            context['bc_0394'] = convert_date(chidao.batdau_thamquan)
            context['bc_0395'] = convert_date(chidao.ketthuc_thamquan)

            if chidao.nganhnghe_congviec_chuyennhuong.name_nganhnghe:
                bc_0396 = '%s・%s・%s' % (nganhnghe(chidao.nganhnghe_congviec_chuyennhuong.name_nganhnghe),
                                        nganhnghe(chidao.loaicongviec_congviec_chuyennhuong.name_loaicongviec),
                                        nganhnghe(chidao.congviec_chuyennhuong.name_congviec))
            else:
                bc_0396 = ''

            context['bc_0396'] = '%s' % (bc_0396)

            if chidao.nganhnghe_congviec_nhieucongviec.name_nganhnghe:
                bc_0396_1 = '%s・%s・%s' % (nganhnghe(chidao.nganhnghe_congviec_nhieucongviec.name_nganhnghe),
                                          nganhnghe(chidao.loaicongviec_congviec_nhieucongviec.name_loaicongviec),
                                          nganhnghe(chidao.congviec_nhieucongviec.name_congviec))
            else:
                bc_0396_1 = ''

            context['bc_0396_1'] = '%s' % (bc_0396_1)

            if chidao.loaikithi_mot_chidao.loaikiemtra and chidao.kinang_mot_chidao.ten_kithi and chidao.capdo_mot_chidao.ten_capdo:
                context['bc_0397_1'] = '%s・%s・%s' % (
                    kiemtra(chidao.loaikithi_mot_chidao.loaikiemtra), kiemtra(chidao.kinang_mot_chidao.ten_kithi),
                    kiemtra(chidao.capdo_mot_chidao.ten_capdo))
            else:
                context['bc_0397_1'] = ''

            if chidao.loaikithi_hai_chidao.loaikiemtra and chidao.kinang_hai_chidao.ten_kithi and chidao.capdo_hai_chidao.ten_capdo:
                context['bc_0397_2'] = '%s・%s・%s' % (
                    kiemtra(chidao.loaikithi_hai_chidao.loaikiemtra), kiemtra(chidao.kinang_hai_chidao.ten_kithi),
                    kiemtra(chidao.capdo_hai_chidao.ten_capdo))
            else:
                context['bc_0397_2'] = ''

            context['bc_0398'] = kiemtra(chidao.thoigian_chidao)
            context['bc_0399'] = kiemtra(chidao.phuongphap_chidao)

            context['hhjp_0097'] = kiemtra(nghiepdoan.ten_han_nghiepdoan)

            if len(chidao.trachnhiem_chidao) > 1:
                item = 1
                for danhsach_nhanvien_tts in chidao.trachnhiem_chidao:
                    if item == 1:
                        context['hhjp_0105_1'] = kiemtra(danhsach_nhanvien_tts.nguoi_trachnhiem_quanly)
                        item += 1
                    elif item == 2:
                        context['hhjp_0105_2'] = kiemtra(danhsach_nhanvien_tts.nguoi_trachnhiem_quanly)
                        item += 1
                    elif item == 3:
                        context['hhjp_0105_3'] = kiemtra(danhsach_nhanvien_tts.nguoi_trachnhiem_quanly)
                        item += 1
                    elif item == 4:
                        context['hhjp_0105_4'] = kiemtra(danhsach_nhanvien_tts.nguoi_trachnhiem_quanly)
                        item += 1
                    elif item == 5:
                        context['hhjp_0105_5'] = kiemtra(danhsach_nhanvien_tts.nguoi_trachnhiem_quanly)
                        break
                    else:
                        item = 1
            else:
                for danhsach_nhanvien_tts in chidao.trachnhiem_chidao:
                    context['hhjp_0105'] = kiemtra(danhsach_nhanvien_tts.nguoi_trachnhiem_quanly)
                    break

            context['bc_0400_1'] = kiemtra(chidao.xinghiep_tham_chidao_1)
            context['bc_0400_2'] = kiemtra(chidao.xinghiep_tham_chidao_2)
            context['bc_0400_3'] = kiemtra(chidao.xinghiep_tham_chidao_3)

            context['hhjp_0010'] = kiemtra(chidao.xinghiep_chidao.ten_han_xnghiep)

            if coche_chidao > 1:
                item = 1
                for xinghiep_coche_chidao in chidao.xinghiep_coche_chidao:
                    if item == 1:
                        context['hhjp_0048_1'] = kiemtra(xinghiep_coche_chidao.xinghiep_coche)
                        item += 1
                    elif item == 2:
                        context['hhjp_0048_2'] = kiemtra(xinghiep_coche_chidao.xinghiep_coche)
                        item += 1
                    elif item == 3:
                        context['hhjp_0048_3'] = kiemtra(xinghiep_coche_chidao.xinghiep_coche)
                        item += 1
                    elif item == 4:
                        context['hhjp_0048_4'] = kiemtra(xinghiep_coche_chidao.xinghiep_coche)
                        item += 1
                    elif item == 5:
                        context['hhjp_0048_5'] = kiemtra(xinghiep_coche_chidao.xinghiep_coche)
                        item += 1

            else:
                for xinghiep_coche_chidao in chidao.xinghiep_coche_chidao:
                    context['hhjp_0048'] = kiemtra(xinghiep_coche_chidao.xinghiep_coche)
                    break

            context['bc_0401'] = convert_date(chidao.ngay_thangthunhat)
            context['bc_0402'] = convert_date(chidao.ngay_thangthuhai)
            context['bc_0403'] = convert_date(chidao.ngay_thangthuba)
            context['bc_0404'] = convert_date(chidao.ngay_thangthubon)
            context['bc_0405'] = convert_date(chidao.ngay_thangthunam)
            context['bc_0406'] = convert_date(chidao.ngay_thangthusau)
            context['bc_0407'] = convert_date(chidao.ngay_thangthubay)
            context['bc_0408'] = convert_date(chidao.ngay_thangthutam)
            context['bc_0409'] = convert_date(chidao.ngay_thangthuchin)
            context['bc_0410'] = convert_date(chidao.ngay_thangthumuoi)
            context['bc_0411'] = convert_date(chidao.ngay_thangthumuoimot)
            context['bc_0412'] = convert_date(chidao.ngay_thangthumuoihai)

            context['bc_0413'] = kiemtra(chidao.tiendo_thangthunhat)
            context['bc_0414'] = kiemtra(chidao.tiendo_thangthuhai)
            context['bc_0415'] = kiemtra(chidao.tiendo_thangthuba)
            context['bc_0416'] = kiemtra(chidao.tiendo_thangthubon)
            context['bc_0417'] = kiemtra(chidao.tiendo_thangthunam)
            context['bc_0418'] = kiemtra(chidao.tiendo_thangthusau)
            context['bc_0419'] = kiemtra(chidao.tiendo_thangthubay)
            context['bc_0420'] = kiemtra(chidao.tiendo_thangthutam)
            context['bc_0421'] = kiemtra(chidao.tiendo_thangthuchin)
            context['bc_0422'] = kiemtra(chidao.tiendo_thangthumuoi)
            context['bc_0423'] = kiemtra(chidao.tiendo_thangthumuoimot)
            context['bc_0424'] = kiemtra(chidao.tiendo_thangthumuoihai)

            context['bc_0425'] = kiemtra(chidao.mucdo_tiepthu_thangthunhat)
            context['bc_0426'] = kiemtra(chidao.mucdo_tiepthu_thangthuhai)
            context['bc_0427'] = kiemtra(chidao.mucdo_tiepthu_thangthuba)
            context['bc_0428'] = kiemtra(chidao.mucdo_tiepthu_thangthubon)
            context['bc_0429'] = kiemtra(chidao.mucdo_tiepthu_thangthunam)
            context['bc_0430'] = kiemtra(chidao.mucdo_tiepthu_thangthusau)
            context['bc_0431'] = kiemtra(chidao.mucdo_tiepthu_thangthubay)
            context['bc_0432'] = kiemtra(chidao.mucdo_tiepthu_thangthutam)
            context['bc_0433'] = kiemtra(chidao.mucdo_tiepthu_thangthuchin)
            context['bc_0434'] = kiemtra(chidao.mucdo_tiepthu_thangthumuoi)
            context['bc_0435'] = kiemtra(chidao.mucdo_tiepthu_thangthumuoimot)
            context['bc_0436'] = kiemtra(chidao.mucdo_tiepthu_thangthumuoihai)

            context['bc_0437'] = kiemtra(chidao.phanbo_thoigian_thangthunhat)
            context['bc_0438'] = kiemtra(chidao.phanbo_thoigian_thangthuhai)
            context['bc_0439'] = kiemtra(chidao.phanbo_thoigian_thangthuba)
            context['bc_0440'] = kiemtra(chidao.phanbo_thoigian_thangthubon)
            context['bc_0441'] = kiemtra(chidao.phanbo_thoigian_thangthunam)
            context['bc_0442'] = kiemtra(chidao.phanbo_thoigian_thangthusau)
            context['bc_0443'] = kiemtra(chidao.phanbo_thoigian_thangthubay)
            context['bc_0444'] = kiemtra(chidao.phanbo_thoigian_thangthutam)
            context['bc_0445'] = kiemtra(chidao.phanbo_thoigian_thangthuchin)
            context['bc_0446'] = kiemtra(chidao.phanbo_thoigian_thangthumuoi)
            context['bc_0447'] = kiemtra(chidao.phanbo_thoigian_thangthumuoimot)
            context['bc_0448'] = kiemtra(chidao.phanbo_thoigian_thangthumuoihai)

            context['bc_0449'] = kiemtra(chidao.thaido_thuctap_thangthunhat)
            context['bc_0450'] = kiemtra(chidao.thaido_thuctap_thangthuhai)
            context['bc_0451'] = kiemtra(chidao.thaido_thuctap_thangthuba)
            context['bc_0452'] = kiemtra(chidao.thaido_thuctap_thangthubon)
            context['bc_0453'] = kiemtra(chidao.thaido_thuctap_thangthunam)
            context['bc_0454'] = kiemtra(chidao.thaido_thuctap_thangthusau)
            context['bc_0455'] = kiemtra(chidao.thaido_thuctap_thangthubay)
            context['bc_0456'] = kiemtra(chidao.thaido_thuctap_thangthutam)
            context['bc_0457'] = kiemtra(chidao.thaido_thuctap_thangthuchin)
            context['bc_0458'] = kiemtra(chidao.thaido_thuctap_thangthumuoi)
            context['bc_0459'] = kiemtra(chidao.thaido_thuctap_thangthumuoimot)
            context['bc_0460'] = kiemtra(chidao.thaido_thuctap_thangthumuoihai)

            context['bc_0461'] = kiemtra(chidao.mucdo_tichcu_thangthunhat)
            context['bc_0462'] = kiemtra(chidao.mucdo_tichcu_thangthuhai)
            context['bc_0463'] = kiemtra(chidao.mucdo_tichcu_thangthuba)
            context['bc_0464'] = kiemtra(chidao.mucdo_tichcu_thangthubon)
            context['bc_0465'] = kiemtra(chidao.mucdo_tichcu_thangthunam)
            context['bc_0466'] = kiemtra(chidao.mucdo_tichcu_thangthusau)
            context['bc_0467'] = kiemtra(chidao.mucdo_tichcu_thangthubay)
            context['bc_0468'] = kiemtra(chidao.mucdo_tichcu_thangthutam)
            context['bc_0469'] = kiemtra(chidao.mucdo_tichcu_thangthuchin)
            context['bc_0470'] = kiemtra(chidao.mucdo_tichcu_thangthumuoi)
            context['bc_0471'] = kiemtra(chidao.mucdo_tichcu_thangthumuoimot)
            context['bc_0472'] = kiemtra(chidao.mucdo_tichcu_thangthumuoihai)

            context['bc_0473'] = kiemtra(chidao.mucdo_hieu_thangthunhat)
            context['bc_0474'] = kiemtra(chidao.mucdo_hieu_thangthuhai)
            context['bc_0475'] = kiemtra(chidao.mucdo_hieu_thangthuba)
            context['bc_0476'] = kiemtra(chidao.mucdo_hieu_thangthubon)
            context['bc_0477'] = kiemtra(chidao.mucdo_hieu_thangthunam)
            context['bc_0478'] = kiemtra(chidao.mucdo_hieu_thangthusau)
            context['bc_0479'] = kiemtra(chidao.mucdo_hieu_thangthubay)
            context['bc_0480'] = kiemtra(chidao.mucdo_hieu_thangthutam)
            context['bc_0481'] = kiemtra(chidao.mucdo_hieu_thangthuchin)
            context['bc_0482'] = kiemtra(chidao.mucdo_hieu_thangthumuoi)
            context['bc_0483'] = kiemtra(chidao.mucdo_hieu_thangthumuoimot)
            context['bc_0484'] = kiemtra(chidao.mucdo_hieu_thangthumuoihai)

            context['bc_0485'] = kiemtra(chidao.thaido_song_thangthunhat)
            context['bc_0486'] = kiemtra(chidao.thaido_song_thangthuhai)
            context['bc_0487'] = kiemtra(chidao.thaido_song_thangthuba)
            context['bc_0488'] = kiemtra(chidao.thaido_song_thangthubon)
            context['bc_0489'] = kiemtra(chidao.thaido_song_thangthunam)
            context['bc_0490'] = kiemtra(chidao.thaido_song_thangthusau)
            context['bc_0491'] = kiemtra(chidao.thaido_song_thangthubay)
            context['bc_0492'] = kiemtra(chidao.thaido_song_thangthutam)
            context['bc_0493'] = kiemtra(chidao.thaido_song_thangthuchin)
            context['bc_0494'] = kiemtra(chidao.thaido_song_thangthumuoi)
            context['bc_0495'] = kiemtra(chidao.thaido_song_thangthumuoimot)
            context['bc_0496'] = kiemtra(chidao.thaido_song_thangthumuoihai)

            context['bc_0497'] = kiemtra(chidao.vipham_phapluat_thangthunhat)
            context['bc_0498'] = kiemtra(chidao.vipham_phapluat_thangthuhai)
            context['bc_0499'] = kiemtra(chidao.vipham_phapluat_thangthuba)
            context['bc_0500'] = kiemtra(chidao.vipham_phapluat_thangthubon)
            context['bc_0501'] = kiemtra(chidao.vipham_phapluat_thangthunam)
            context['bc_0502'] = kiemtra(chidao.vipham_phapluat_thangthusau)
            context['bc_0503'] = kiemtra(chidao.vipham_phapluat_thangthubay)
            context['bc_0504'] = kiemtra(chidao.vipham_phapluat_thangthutam)
            context['bc_0505'] = kiemtra(chidao.vipham_phapluat_thangthuchin)
            context['bc_0506'] = kiemtra(chidao.vipham_phapluat_thangthumuoi)
            context['bc_0507'] = kiemtra(chidao.vipham_phapluat_thangthumuoimot)
            context['bc_0508'] = kiemtra(chidao.vipham_phapluat_thangthumuoihai)

            context['bc_0509'] = kiemtra(chidao.ghichu_thangthunhat)
            context['bc_0510'] = kiemtra(chidao.ghichu_thangthuhai)
            context['bc_0511'] = kiemtra(chidao.ghichu_thangthuba)
            context['bc_0512'] = kiemtra(chidao.ghichu_thangthubon)
            context['bc_0513'] = kiemtra(chidao.ghichu_thangthunam)
            context['bc_0514'] = kiemtra(chidao.ghichu_thangthusau)
            context['bc_0515'] = kiemtra(chidao.ghichu_thangthubay)
            context['bc_0516'] = kiemtra(chidao.ghichu_thangthutam)
            context['bc_0517'] = kiemtra(chidao.ghichu_thangthuchin)
            context['bc_0518'] = kiemtra(chidao.ghichu_thangthumuoi)
            context['bc_0519'] = kiemtra(chidao.ghichu_thangthumuoimot)
            context['bc_0520'] = kiemtra(chidao.ghichu_thangthumuoihai)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None
