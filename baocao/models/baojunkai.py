# -*- coding: utf-8 -*-

from odoo import models, fields, api
from calendar import monthrange
from datetime import datetime
import calendar
import datetime
import logging
from tempfile import TemporaryFile, NamedTemporaryFile
from io import BytesIO, StringIO
import os
import codecs
from docxtpl import DocxTemplate

_logger = logging.getLogger(__name__)


def kiemtra(data):
    if data:
        return data
    else:
        return ''


def convert_date(self):
    if self:
        return str(self.year) + '年' + \
               str(self.month) + '月' + \
               str(self.day) + '日'
    else:
        return '年　月　日'


def convert_th(self):
    if self:
        ngay = str(self.day)

        ans = datetime.date(self.year, self.month, self.day)
        substr = ans.strftime("%A")
        if substr == 'Sunday':
            return '%s(日)' % (ngay)
        elif substr == 'Monday':
            return '%s(月)' % (ngay)
        elif substr == 'Tuesday':
            return '%s(火)' % (ngay)
        elif substr == 'Wednesday':
            return '%s(水)' % (ngay)
        elif substr == 'Thursday':
            return '%s(木)' % (ngay)
        elif substr == 'Friday':
            return '%s(金)' % (ngay)
        elif substr == 'Saturday':
            return '%s(土)' % (ngay)
        else:
            return ''


def nganhnghe_0(self):
    if self:
        return self.split(":")[0]
    else:
        return ' '


def nganhnghe_1(self):
    if self:
        return self.split(":")[1]
    else:
        return ' '


class BaoJunkai(models.Model):
    _name = 'baocao.junkai'
    _rec_name = 'donhang_junkai'

    donhang_junkai = fields.Many2one(comodel_name='donhang.donhang', string='Đơn hàng', required=True)
    xinghiep_junkai = fields.Many2one(comodel_name='xinghiep.xinghiep', string='Xí nghiệp',
                                      related='donhang_junkai.xinghiep_donhang')
    chinhanh_junkai = fields.Many2many(comodel_name='xinghiep.chinhanh', string='Chi nhánh', required=True)

    giaidoan_junkai = fields.Selection(string='Giai đoạn 1',
                                       selection=[('1 go', '1 go'),
                                                  ('2 go', '2 go'),
                                                  ('3 go', '3 go')], required=True)

    ngaytao_junkai = fields.Date(string='Ngày tạo', default=fields.Date.today())
    ma_congviec = fields.Char(string='Mã công việc', related='donhang_junkai.ma_congviec_donhang')
    nganhnghe = fields.Many2one(comodel_name='nganhnghe.nganhnghe', string='Ngành nghề',
                                related='donhang_junkai.nganhnghe_donhang')
    loaicongviec = fields.Many2one(comodel_name='loaicongviec.loaicongviec', string='Loại công việc',
                                   related='donhang_junkai.loainghe_donhang')
    congviec = fields.Many2one(comodel_name='congviec.congviec', string='Công việc',
                               related='donhang_junkai.congviec_donhang')
    thuctapsinh_junkai = fields.Many2many(comodel_name='thuctapsinh.thuctapsinh',
                                          string='Danh sách thực tập sinh áp dụng', required=True)

    day_junkai = fields.Date(string='Dự kiến Junkai')
    thang_lich_junkai = fields.Selection([('01', '01'), ('02', '02'), ('03', '03'), ('04', '04'),
                                          ('05', '05'), ('06', '06'), ('07', '07'), ('08', '08'),
                                          ('09', '09'), ('10', '10'), ('11', '11'), ('12', '12'), ], "Tháng",
                                         store=True, required=True)  #
    nam_lich_junkai = fields.Char(string="Năm", store=True, required=True)  #
    lich_junkai = fields.Char(string="Lịch Junkai tháng")  #

    ngay_thu_1 = fields.Char()
    ngay_nghi_1 = fields.Boolean()

    @api.onchange('ngay_nghi_1')
    def onchange_method_ngay_nghi_1(self):
        if self.ngay_nghi_1:
            self.noidung_sang_1 = ()
            self.mot_sang_1 = ()
            self.hai_sang_1 = ()
            self.ba_sang_1 = ()
            self.bon_sang_1 = ()
            self.thuctap_sang_1 = '休日'
            self.daotao_sang_1 = ''
            self.chidao_sang_1 = ()

            self.noidung_chieu_1 = ()
            self.mot_chieu_1 = ()
            self.hai_chieu_1 = ()
            self.ba_chieu_1 = ()
            self.bon_chieu_1 = ()
            self.thuctap_chieu_1 = '休日'
            self.daotao_chieu_1 = ''
            self.chidao_chieu_1 = ()
        else:
            self.noidung_sang_1 = ()
            self.mot_sang_1 = ()
            self.hai_sang_1 = ()
            self.ba_sang_1 = ()
            self.bon_sang_1 = ()
            self.thuctap_sang_1 = ''
            self.daotao_sang_1 = ''
            self.chidao_sang_1 = ()

            self.noidung_chieu_1 = ()
            self.mot_chieu_1 = ()
            self.hai_chieu_1 = ()
            self.ba_chieu_1 = ()
            self.bon_chieu_1 = ()
            self.thuctap_chieu_1 = ''
            self.daotao_chieu_1 = ''
            self.chidao_chieu_1 = ()

    ma_sang_1 = fields.Char(related='ma_congviec')
    giaidoan_sang_1 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_1 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_1 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    @api.onchange('giaidoan_junkai')
    def _giaidoan_junkai(self):
        if self.ma_congviec == '2-2-1':
            giaidoanmot_sang = self.env['giaidoan.giaidoanmot'].search(
                [('ten_giaidoanmot', '=', '1号')], limit=1)
            giaidoanhai_sang = self.env['giaidoan.giaidoanhai'].search(
                [('ten_giaidoanhai', '=', '1号')], limit=1)
            giaidoanba_sang = self.env['giaidoan.giaidoanba'].search(
                [('giaidoan_noidung', '=', self.giaidoan_junkai)], limit=1)

            # Ngay _1
            self.giaidoan_sang_1 = giaidoanmot_sang.id
            self.giaidoan_211_sang_1 = giaidoanhai_sang.id
            self.giaidoan_221_sang_1 = giaidoanba_sang.id
            self.giaidoan_chieu_1 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_1 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_1 = giaidoanba_sang.id
            # Ngay _2
            self.giaidoan_sang_2 = giaidoanmot_sang.id
            self.giaidoan_211_sang_2 = giaidoanhai_sang.id
            self.giaidoan_221_sang_2 = giaidoanba_sang.id
            self.giaidoan_chieu_2 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_2 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_2 = giaidoanba_sang.id
            # Ngay _3
            self.giaidoan_sang_3 = giaidoanmot_sang.id
            self.giaidoan_211_sang_3 = giaidoanhai_sang.id
            self.giaidoan_221_sang_3 = giaidoanba_sang.id
            self.giaidoan_chieu_3 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_3 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_3 = giaidoanba_sang.id
            # Ngay _4
            self.giaidoan_sang_4 = giaidoanmot_sang.id
            self.giaidoan_211_sang_4 = giaidoanhai_sang.id
            self.giaidoan_221_sang_4 = giaidoanba_sang.id
            self.giaidoan_chieu_4 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_4 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_4 = giaidoanba_sang.id
            # Ngay _5
            self.giaidoan_sang_5 = giaidoanmot_sang.id
            self.giaidoan_211_sang_5 = giaidoanhai_sang.id
            self.giaidoan_221_sang_5 = giaidoanba_sang.id
            self.giaidoan_chieu_5 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_5 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_5 = giaidoanba_sang.id
            # Ngay _6
            self.giaidoan_sang_6 = giaidoanmot_sang.id
            self.giaidoan_211_sang_6 = giaidoanhai_sang.id
            self.giaidoan_221_sang_6 = giaidoanba_sang.id
            self.giaidoan_chieu_6 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_6 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_6 = giaidoanba_sang.id
            # Ngay _7
            self.giaidoan_sang_7 = giaidoanmot_sang.id
            self.giaidoan_211_sang_7 = giaidoanhai_sang.id
            self.giaidoan_221_sang_7 = giaidoanba_sang.id
            self.giaidoan_chieu_7 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_7 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_7 = giaidoanba_sang.id
            # Ngay _8
            self.giaidoan_sang_8 = giaidoanmot_sang.id
            self.giaidoan_211_sang_8 = giaidoanhai_sang.id
            self.giaidoan_221_sang_8 = giaidoanba_sang.id
            self.giaidoan_chieu_8 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_8 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_8 = giaidoanba_sang.id
            # Ngay _9
            self.giaidoan_sang_9 = giaidoanmot_sang.id
            self.giaidoan_211_sang_9 = giaidoanhai_sang.id
            self.giaidoan_221_sang_9 = giaidoanba_sang.id
            self.giaidoan_chieu_9 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_9 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_9 = giaidoanba_sang.id
            # Ngay _10
            self.giaidoan_sang_10 = giaidoanmot_sang.id
            self.giaidoan_211_sang_10 = giaidoanhai_sang.id
            self.giaidoan_221_sang_10 = giaidoanba_sang.id
            self.giaidoan_chieu_10 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_10 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_10 = giaidoanba_sang.id
            # Ngay _11
            self.giaidoan_sang_11 = giaidoanmot_sang.id
            self.giaidoan_211_sang_11 = giaidoanhai_sang.id
            self.giaidoan_221_sang_11 = giaidoanba_sang.id
            self.giaidoan_chieu_11 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_11 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_11 = giaidoanba_sang.id
            # Ngay _12
            self.giaidoan_sang_12 = giaidoanmot_sang.id
            self.giaidoan_211_sang_12 = giaidoanhai_sang.id
            self.giaidoan_221_sang_12 = giaidoanba_sang.id
            self.giaidoan_chieu_12 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_12 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_12 = giaidoanba_sang.id
            # Ngay _13
            self.giaidoan_sang_13 = giaidoanmot_sang.id
            self.giaidoan_211_sang_13 = giaidoanhai_sang.id
            self.giaidoan_221_sang_13 = giaidoanba_sang.id
            self.giaidoan_chieu_13 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_13 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_13 = giaidoanba_sang.id
            # Ngay _14
            self.giaidoan_sang_14 = giaidoanmot_sang.id
            self.giaidoan_211_sang_14 = giaidoanhai_sang.id
            self.giaidoan_221_sang_14 = giaidoanba_sang.id
            self.giaidoan_chieu_14 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_14 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_14 = giaidoanba_sang.id
            # Ngay _15
            self.giaidoan_sang_15 = giaidoanmot_sang.id
            self.giaidoan_211_sang_15 = giaidoanhai_sang.id
            self.giaidoan_221_sang_15 = giaidoanba_sang.id
            self.giaidoan_chieu_15 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_15 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_15 = giaidoanba_sang.id
            # Ngay _16
            self.giaidoan_sang_16 = giaidoanmot_sang.id
            self.giaidoan_211_sang_16 = giaidoanhai_sang.id
            self.giaidoan_221_sang_16 = giaidoanba_sang.id
            self.giaidoan_chieu_16 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_16 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_16 = giaidoanba_sang.id
            # Ngay _17
            self.giaidoan_sang_17 = giaidoanmot_sang.id
            self.giaidoan_211_sang_17 = giaidoanhai_sang.id
            self.giaidoan_221_sang_17 = giaidoanba_sang.id
            self.giaidoan_chieu_17 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_17 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_17 = giaidoanba_sang.id
            # Ngay _18
            self.giaidoan_sang_18 = giaidoanmot_sang.id
            self.giaidoan_211_sang_18 = giaidoanhai_sang.id
            self.giaidoan_221_sang_18 = giaidoanba_sang.id
            self.giaidoan_chieu_18 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_18 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_18 = giaidoanba_sang.id
            # Ngay _19
            self.giaidoan_sang_19 = giaidoanmot_sang.id
            self.giaidoan_211_sang_19 = giaidoanhai_sang.id
            self.giaidoan_221_sang_19 = giaidoanba_sang.id
            self.giaidoan_chieu_19 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_19 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_19 = giaidoanba_sang.id
            # Ngay _20
            self.giaidoan_sang_20 = giaidoanmot_sang.id
            self.giaidoan_211_sang_20 = giaidoanhai_sang.id
            self.giaidoan_221_sang_20 = giaidoanba_sang.id
            self.giaidoan_chieu_20 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_20 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_20 = giaidoanba_sang.id
            # Ngay _21
            self.giaidoan_sang_21 = giaidoanmot_sang.id
            self.giaidoan_211_sang_21 = giaidoanhai_sang.id
            self.giaidoan_221_sang_21 = giaidoanba_sang.id
            self.giaidoan_chieu_21 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_21 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_21 = giaidoanba_sang.id
            # Ngay _22
            self.giaidoan_sang_22 = giaidoanmot_sang.id
            self.giaidoan_211_sang_22 = giaidoanhai_sang.id
            self.giaidoan_221_sang_22 = giaidoanba_sang.id
            self.giaidoan_chieu_22 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_22 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_22 = giaidoanba_sang.id
            # Ngay _23
            self.giaidoan_sang_23 = giaidoanmot_sang.id
            self.giaidoan_211_sang_23 = giaidoanhai_sang.id
            self.giaidoan_221_sang_23 = giaidoanba_sang.id
            self.giaidoan_chieu_23 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_23 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_23 = giaidoanba_sang.id
            # Ngay _24
            self.giaidoan_sang_24 = giaidoanmot_sang.id
            self.giaidoan_211_sang_24 = giaidoanhai_sang.id
            self.giaidoan_221_sang_24 = giaidoanba_sang.id
            self.giaidoan_chieu_24 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_24 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_24 = giaidoanba_sang.id
            # Ngay _25
            self.giaidoan_sang_25 = giaidoanmot_sang.id
            self.giaidoan_211_sang_25 = giaidoanhai_sang.id
            self.giaidoan_221_sang_25 = giaidoanba_sang.id
            self.giaidoan_chieu_25 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_25 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_25 = giaidoanba_sang.id
            # Ngay _26
            self.giaidoan_sang_26 = giaidoanmot_sang.id
            self.giaidoan_211_sang_26 = giaidoanhai_sang.id
            self.giaidoan_221_sang_26 = giaidoanba_sang.id
            self.giaidoan_chieu_26 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_26 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_26 = giaidoanba_sang.id
            # Ngay _27
            self.giaidoan_sang_27 = giaidoanmot_sang.id
            self.giaidoan_211_sang_27 = giaidoanhai_sang.id
            self.giaidoan_221_sang_27 = giaidoanba_sang.id
            self.giaidoan_chieu_27 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_27 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_27 = giaidoanba_sang.id
            # Ngay _28
            self.giaidoan_sang_28 = giaidoanmot_sang.id
            self.giaidoan_211_sang_28 = giaidoanhai_sang.id
            self.giaidoan_221_sang_28 = giaidoanba_sang.id
            self.giaidoan_chieu_28 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_28 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_28 = giaidoanba_sang.id
            # Ngay _29
            self.giaidoan_sang_29 = giaidoanmot_sang.id
            self.giaidoan_211_sang_29 = giaidoanhai_sang.id
            self.giaidoan_221_sang_29 = giaidoanba_sang.id
            self.giaidoan_chieu_29 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_29 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_29 = giaidoanba_sang.id
            # Ngay _30
            self.giaidoan_sang_30 = giaidoanmot_sang.id
            self.giaidoan_211_sang_30 = giaidoanhai_sang.id
            self.giaidoan_221_sang_30 = giaidoanba_sang.id
            self.giaidoan_chieu_30 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_30 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_30 = giaidoanba_sang.id
            # Ngay _31
            self.giaidoan_sang_31 = giaidoanmot_sang.id
            self.giaidoan_211_sang_31 = giaidoanhai_sang.id
            self.giaidoan_221_sang_31 = giaidoanba_sang.id
            self.giaidoan_chieu_31 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_31 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_31 = giaidoanba_sang.id

            return {'domain': {'giaidoan_221_sang_1': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_1': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_2': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_2': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_3': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_3': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_4': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_4': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_5': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_5': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_6': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_6': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_7': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_7': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_8': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_8': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_9': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_9': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_10': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_10': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_11': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_11': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_12': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_12': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_13': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_13': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_14': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_14': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_15': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_15': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_16': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_16': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_17': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_17': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_18': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_18': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_19': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_19': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_20': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_20': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_21': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_21': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_22': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_22': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_23': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_23': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_24': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_24': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_25': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_25': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_26': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_26': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_27': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_27': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_28': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_28': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_29': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_29': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_30': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_30': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_sang_31': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_221_chieu_31': [('giaidoan_noidung', '=', self.giaidoan_junkai)],}}

        elif self.ma_congviec == '2-1-1' or self.ma_congviec == '2-1-2' or self.ma_congviec == '2-1-3' or self.ma_congviec == '2-1-4' or self.ma_congviec == '2-1-5' or self.ma_congviec == '2-1-6' or self.ma_congviec == '2-1-7' or self.ma_congviec == '2-1-8':
            giaidoanmot_sang = self.env['giaidoan.giaidoanmot'].search(
                [('ten_giaidoanmot', '=', '1号')], limit=1)
            giaidoanhai_sang = self.env['giaidoan.giaidoanhai'].search(
                [('giaidoan_noidung', '=', self.giaidoan_junkai)], limit=1)
            giaidoanba_sang = self.env['giaidoan.giaidoanba'].search(
                [('ten_giaidoanba', '=', '1号（①ほたてがい）')], limit=1)

            # Ngay _1
            self.giaidoan_sang_1 = giaidoanmot_sang.id
            self.giaidoan_211_sang_1 = giaidoanhai_sang.id
            self.giaidoan_221_sang_1 = giaidoanba_sang.id
            self.giaidoan_chieu_1 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_1 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_1 = giaidoanba_sang.id
            # Ngay _2
            self.giaidoan_sang_2 = giaidoanmot_sang.id
            self.giaidoan_211_sang_2 = giaidoanhai_sang.id
            self.giaidoan_221_sang_2 = giaidoanba_sang.id
            self.giaidoan_chieu_2 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_2 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_2 = giaidoanba_sang.id
            # Ngay _3
            self.giaidoan_sang_3 = giaidoanmot_sang.id
            self.giaidoan_211_sang_3 = giaidoanhai_sang.id
            self.giaidoan_221_sang_3 = giaidoanba_sang.id
            self.giaidoan_chieu_3 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_3 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_3 = giaidoanba_sang.id
            # Ngay _4
            self.giaidoan_sang_4 = giaidoanmot_sang.id
            self.giaidoan_211_sang_4 = giaidoanhai_sang.id
            self.giaidoan_221_sang_4 = giaidoanba_sang.id
            self.giaidoan_chieu_4 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_4 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_4 = giaidoanba_sang.id
            # Ngay _5
            self.giaidoan_sang_5 = giaidoanmot_sang.id
            self.giaidoan_211_sang_5 = giaidoanhai_sang.id
            self.giaidoan_221_sang_5 = giaidoanba_sang.id
            self.giaidoan_chieu_5 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_5 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_5 = giaidoanba_sang.id
            # Ngay _6
            self.giaidoan_sang_6 = giaidoanmot_sang.id
            self.giaidoan_211_sang_6 = giaidoanhai_sang.id
            self.giaidoan_221_sang_6 = giaidoanba_sang.id
            self.giaidoan_chieu_6 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_6 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_6 = giaidoanba_sang.id
            # Ngay _7
            self.giaidoan_sang_7 = giaidoanmot_sang.id
            self.giaidoan_211_sang_7 = giaidoanhai_sang.id
            self.giaidoan_221_sang_7 = giaidoanba_sang.id
            self.giaidoan_chieu_7 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_7 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_7 = giaidoanba_sang.id
            # Ngay _8
            self.giaidoan_sang_8 = giaidoanmot_sang.id
            self.giaidoan_211_sang_8 = giaidoanhai_sang.id
            self.giaidoan_221_sang_8 = giaidoanba_sang.id
            self.giaidoan_chieu_8 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_8 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_8 = giaidoanba_sang.id
            # Ngay _9
            self.giaidoan_sang_9 = giaidoanmot_sang.id
            self.giaidoan_211_sang_9 = giaidoanhai_sang.id
            self.giaidoan_221_sang_9 = giaidoanba_sang.id
            self.giaidoan_chieu_9 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_9 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_9 = giaidoanba_sang.id
            # Ngay _10
            self.giaidoan_sang_10 = giaidoanmot_sang.id
            self.giaidoan_211_sang_10 = giaidoanhai_sang.id
            self.giaidoan_221_sang_10 = giaidoanba_sang.id
            self.giaidoan_chieu_10 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_10 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_10 = giaidoanba_sang.id
            # Ngay _11
            self.giaidoan_sang_11 = giaidoanmot_sang.id
            self.giaidoan_211_sang_11 = giaidoanhai_sang.id
            self.giaidoan_221_sang_11 = giaidoanba_sang.id
            self.giaidoan_chieu_11 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_11 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_11 = giaidoanba_sang.id
            # Ngay _12
            self.giaidoan_sang_12 = giaidoanmot_sang.id
            self.giaidoan_211_sang_12 = giaidoanhai_sang.id
            self.giaidoan_221_sang_12 = giaidoanba_sang.id
            self.giaidoan_chieu_12 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_12 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_12 = giaidoanba_sang.id
            # Ngay _13
            self.giaidoan_sang_13 = giaidoanmot_sang.id
            self.giaidoan_211_sang_13 = giaidoanhai_sang.id
            self.giaidoan_221_sang_13 = giaidoanba_sang.id
            self.giaidoan_chieu_13 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_13 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_13 = giaidoanba_sang.id
            # Ngay _14
            self.giaidoan_sang_14 = giaidoanmot_sang.id
            self.giaidoan_211_sang_14 = giaidoanhai_sang.id
            self.giaidoan_221_sang_14 = giaidoanba_sang.id
            self.giaidoan_chieu_14 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_14 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_14 = giaidoanba_sang.id
            # Ngay _15
            self.giaidoan_sang_15 = giaidoanmot_sang.id
            self.giaidoan_211_sang_15 = giaidoanhai_sang.id
            self.giaidoan_221_sang_15 = giaidoanba_sang.id
            self.giaidoan_chieu_15 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_15 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_15 = giaidoanba_sang.id
            # Ngay _16
            self.giaidoan_sang_16 = giaidoanmot_sang.id
            self.giaidoan_211_sang_16 = giaidoanhai_sang.id
            self.giaidoan_221_sang_16 = giaidoanba_sang.id
            self.giaidoan_chieu_16 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_16 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_16 = giaidoanba_sang.id
            # Ngay _17
            self.giaidoan_sang_17 = giaidoanmot_sang.id
            self.giaidoan_211_sang_17 = giaidoanhai_sang.id
            self.giaidoan_221_sang_17 = giaidoanba_sang.id
            self.giaidoan_chieu_17 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_17 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_17 = giaidoanba_sang.id
            # Ngay _18
            self.giaidoan_sang_18 = giaidoanmot_sang.id
            self.giaidoan_211_sang_18 = giaidoanhai_sang.id
            self.giaidoan_221_sang_18 = giaidoanba_sang.id
            self.giaidoan_chieu_18 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_18 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_18 = giaidoanba_sang.id
            # Ngay _19
            self.giaidoan_sang_19 = giaidoanmot_sang.id
            self.giaidoan_211_sang_19 = giaidoanhai_sang.id
            self.giaidoan_221_sang_19 = giaidoanba_sang.id
            self.giaidoan_chieu_19 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_19 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_19 = giaidoanba_sang.id
            # Ngay _20
            self.giaidoan_sang_20 = giaidoanmot_sang.id
            self.giaidoan_211_sang_20 = giaidoanhai_sang.id
            self.giaidoan_221_sang_20 = giaidoanba_sang.id
            self.giaidoan_chieu_20 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_20 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_20 = giaidoanba_sang.id
            # Ngay _21
            self.giaidoan_sang_21 = giaidoanmot_sang.id
            self.giaidoan_211_sang_21 = giaidoanhai_sang.id
            self.giaidoan_221_sang_21 = giaidoanba_sang.id
            self.giaidoan_chieu_21 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_21 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_21 = giaidoanba_sang.id
            # Ngay _22
            self.giaidoan_sang_22 = giaidoanmot_sang.id
            self.giaidoan_211_sang_22 = giaidoanhai_sang.id
            self.giaidoan_221_sang_22 = giaidoanba_sang.id
            self.giaidoan_chieu_22 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_22 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_22 = giaidoanba_sang.id
            # Ngay _23
            self.giaidoan_sang_23 = giaidoanmot_sang.id
            self.giaidoan_211_sang_23 = giaidoanhai_sang.id
            self.giaidoan_221_sang_23 = giaidoanba_sang.id
            self.giaidoan_chieu_23 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_23 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_23 = giaidoanba_sang.id
            # Ngay _24
            self.giaidoan_sang_24 = giaidoanmot_sang.id
            self.giaidoan_211_sang_24 = giaidoanhai_sang.id
            self.giaidoan_221_sang_24 = giaidoanba_sang.id
            self.giaidoan_chieu_24 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_24 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_24 = giaidoanba_sang.id
            # Ngay _25
            self.giaidoan_sang_25 = giaidoanmot_sang.id
            self.giaidoan_211_sang_25 = giaidoanhai_sang.id
            self.giaidoan_221_sang_25 = giaidoanba_sang.id
            self.giaidoan_chieu_25 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_25 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_25 = giaidoanba_sang.id
            # Ngay _26
            self.giaidoan_sang_26 = giaidoanmot_sang.id
            self.giaidoan_211_sang_26 = giaidoanhai_sang.id
            self.giaidoan_221_sang_26 = giaidoanba_sang.id
            self.giaidoan_chieu_26 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_26 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_26 = giaidoanba_sang.id
            # Ngay _27
            self.giaidoan_sang_27 = giaidoanmot_sang.id
            self.giaidoan_211_sang_27 = giaidoanhai_sang.id
            self.giaidoan_221_sang_27 = giaidoanba_sang.id
            self.giaidoan_chieu_27 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_27 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_27 = giaidoanba_sang.id
            # Ngay _28
            self.giaidoan_sang_28 = giaidoanmot_sang.id
            self.giaidoan_211_sang_28 = giaidoanhai_sang.id
            self.giaidoan_221_sang_28 = giaidoanba_sang.id
            self.giaidoan_chieu_28 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_28 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_28 = giaidoanba_sang.id
            # Ngay _29
            self.giaidoan_sang_29 = giaidoanmot_sang.id
            self.giaidoan_211_sang_29 = giaidoanhai_sang.id
            self.giaidoan_221_sang_29 = giaidoanba_sang.id
            self.giaidoan_chieu_29 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_29 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_29 = giaidoanba_sang.id
            # Ngay _30
            self.giaidoan_sang_30 = giaidoanmot_sang.id
            self.giaidoan_211_sang_30 = giaidoanhai_sang.id
            self.giaidoan_221_sang_30 = giaidoanba_sang.id
            self.giaidoan_chieu_30 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_30 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_30 = giaidoanba_sang.id
            # Ngay _31
            self.giaidoan_sang_31 = giaidoanmot_sang.id
            self.giaidoan_211_sang_31 = giaidoanhai_sang.id
            self.giaidoan_221_sang_31 = giaidoanba_sang.id
            self.giaidoan_chieu_31 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_31 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_31 = giaidoanba_sang.id

            return {'domain': {'giaidoan_211_sang_1': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_1': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_2': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_2': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_3': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_3': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_4': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_4': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_5': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_5': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_6': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_6': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_7': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_7': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_8': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_8': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_9': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_9': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_10': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_10': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_11': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_11': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_12': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_12': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_13': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_13': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_14': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_14': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_15': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_15': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_16': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_16': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_17': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_17': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_18': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_18': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_19': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_19': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_20': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_20': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_21': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_21': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_22': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_22': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_23': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_23': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_24': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_24': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_25': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_25': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_26': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_26': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_27': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_27': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_28': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_28': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_29': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_29': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_30': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_30': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_sang_31': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_211_chieu_31': [('giaidoan_noidung', '=', self.giaidoan_junkai)],}}

        else:
            giaidoanmot_sang = self.env['giaidoan.giaidoanmot'].search(
                [('giaidoan_noidung', '=', self.giaidoan_junkai)], limit=1)
            giaidoanhai_sang = self.env['giaidoan.giaidoanhai'].search(
                [('ten_giaidoanhai', '=', '1号')], limit=1)
            giaidoanba_sang = self.env['giaidoan.giaidoanba'].search(
                [('ten_giaidoanba', '=', '1号（①ほたてがい）')], limit=1)

            # Ngay _1
            self.giaidoan_sang_1 = giaidoanmot_sang.id
            self.giaidoan_211_sang_1 = giaidoanhai_sang.id
            self.giaidoan_221_sang_1 = giaidoanba_sang.id
            self.giaidoan_chieu_1 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_1 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_1 = giaidoanba_sang.id
            # Ngay _2
            self.giaidoan_sang_2 = giaidoanmot_sang.id
            self.giaidoan_211_sang_2 = giaidoanhai_sang.id
            self.giaidoan_221_sang_2 = giaidoanba_sang.id
            self.giaidoan_chieu_2 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_2 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_2 = giaidoanba_sang.id
            # Ngay _3
            self.giaidoan_sang_3 = giaidoanmot_sang.id
            self.giaidoan_211_sang_3 = giaidoanhai_sang.id
            self.giaidoan_221_sang_3 = giaidoanba_sang.id
            self.giaidoan_chieu_3 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_3 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_3 = giaidoanba_sang.id
            # Ngay _4
            self.giaidoan_sang_4 = giaidoanmot_sang.id
            self.giaidoan_211_sang_4 = giaidoanhai_sang.id
            self.giaidoan_221_sang_4 = giaidoanba_sang.id
            self.giaidoan_chieu_4 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_4 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_4 = giaidoanba_sang.id
            # Ngay _5
            self.giaidoan_sang_5 = giaidoanmot_sang.id
            self.giaidoan_211_sang_5 = giaidoanhai_sang.id
            self.giaidoan_221_sang_5 = giaidoanba_sang.id
            self.giaidoan_chieu_5 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_5 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_5 = giaidoanba_sang.id
            # Ngay _6
            self.giaidoan_sang_6 = giaidoanmot_sang.id
            self.giaidoan_211_sang_6 = giaidoanhai_sang.id
            self.giaidoan_221_sang_6 = giaidoanba_sang.id
            self.giaidoan_chieu_6 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_6 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_6 = giaidoanba_sang.id
            # Ngay _7
            self.giaidoan_sang_7 = giaidoanmot_sang.id
            self.giaidoan_211_sang_7 = giaidoanhai_sang.id
            self.giaidoan_221_sang_7 = giaidoanba_sang.id
            self.giaidoan_chieu_7 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_7 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_7 = giaidoanba_sang.id
            # Ngay _8
            self.giaidoan_sang_8 = giaidoanmot_sang.id
            self.giaidoan_211_sang_8 = giaidoanhai_sang.id
            self.giaidoan_221_sang_8 = giaidoanba_sang.id
            self.giaidoan_chieu_8 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_8 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_8 = giaidoanba_sang.id
            # Ngay _9
            self.giaidoan_sang_9 = giaidoanmot_sang.id
            self.giaidoan_211_sang_9 = giaidoanhai_sang.id
            self.giaidoan_221_sang_9 = giaidoanba_sang.id
            self.giaidoan_chieu_9 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_9 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_9 = giaidoanba_sang.id
            # Ngay _10
            self.giaidoan_sang_10 = giaidoanmot_sang.id
            self.giaidoan_211_sang_10 = giaidoanhai_sang.id
            self.giaidoan_221_sang_10 = giaidoanba_sang.id
            self.giaidoan_chieu_10 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_10 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_10 = giaidoanba_sang.id
            # Ngay _11
            self.giaidoan_sang_11 = giaidoanmot_sang.id
            self.giaidoan_211_sang_11 = giaidoanhai_sang.id
            self.giaidoan_221_sang_11 = giaidoanba_sang.id
            self.giaidoan_chieu_11 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_11 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_11 = giaidoanba_sang.id
            # Ngay _12
            self.giaidoan_sang_12 = giaidoanmot_sang.id
            self.giaidoan_211_sang_12 = giaidoanhai_sang.id
            self.giaidoan_221_sang_12 = giaidoanba_sang.id
            self.giaidoan_chieu_12 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_12 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_12 = giaidoanba_sang.id
            # Ngay _13
            self.giaidoan_sang_13 = giaidoanmot_sang.id
            self.giaidoan_211_sang_13 = giaidoanhai_sang.id
            self.giaidoan_221_sang_13 = giaidoanba_sang.id
            self.giaidoan_chieu_13 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_13 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_13 = giaidoanba_sang.id
            # Ngay _14
            self.giaidoan_sang_14 = giaidoanmot_sang.id
            self.giaidoan_211_sang_14 = giaidoanhai_sang.id
            self.giaidoan_221_sang_14 = giaidoanba_sang.id
            self.giaidoan_chieu_14 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_14 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_14 = giaidoanba_sang.id
            # Ngay _15
            self.giaidoan_sang_15 = giaidoanmot_sang.id
            self.giaidoan_211_sang_15 = giaidoanhai_sang.id
            self.giaidoan_221_sang_15 = giaidoanba_sang.id
            self.giaidoan_chieu_15 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_15 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_15 = giaidoanba_sang.id
            # Ngay _16
            self.giaidoan_sang_16 = giaidoanmot_sang.id
            self.giaidoan_211_sang_16 = giaidoanhai_sang.id
            self.giaidoan_221_sang_16 = giaidoanba_sang.id
            self.giaidoan_chieu_16 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_16 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_16 = giaidoanba_sang.id
            # Ngay _17
            self.giaidoan_sang_17 = giaidoanmot_sang.id
            self.giaidoan_211_sang_17 = giaidoanhai_sang.id
            self.giaidoan_221_sang_17 = giaidoanba_sang.id
            self.giaidoan_chieu_17 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_17 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_17 = giaidoanba_sang.id
            # Ngay _18
            self.giaidoan_sang_18 = giaidoanmot_sang.id
            self.giaidoan_211_sang_18 = giaidoanhai_sang.id
            self.giaidoan_221_sang_18 = giaidoanba_sang.id
            self.giaidoan_chieu_18 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_18 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_18 = giaidoanba_sang.id
            # Ngay _19
            self.giaidoan_sang_19 = giaidoanmot_sang.id
            self.giaidoan_211_sang_19 = giaidoanhai_sang.id
            self.giaidoan_221_sang_19 = giaidoanba_sang.id
            self.giaidoan_chieu_19 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_19 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_19 = giaidoanba_sang.id
            # Ngay _20
            self.giaidoan_sang_20 = giaidoanmot_sang.id
            self.giaidoan_211_sang_20 = giaidoanhai_sang.id
            self.giaidoan_221_sang_20 = giaidoanba_sang.id
            self.giaidoan_chieu_20 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_20 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_20 = giaidoanba_sang.id
            # Ngay _21
            self.giaidoan_sang_21 = giaidoanmot_sang.id
            self.giaidoan_211_sang_21 = giaidoanhai_sang.id
            self.giaidoan_221_sang_21 = giaidoanba_sang.id
            self.giaidoan_chieu_21 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_21 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_21 = giaidoanba_sang.id
            # Ngay _22
            self.giaidoan_sang_22 = giaidoanmot_sang.id
            self.giaidoan_211_sang_22 = giaidoanhai_sang.id
            self.giaidoan_221_sang_22 = giaidoanba_sang.id
            self.giaidoan_chieu_22 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_22 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_22 = giaidoanba_sang.id
            # Ngay _23
            self.giaidoan_sang_23 = giaidoanmot_sang.id
            self.giaidoan_211_sang_23 = giaidoanhai_sang.id
            self.giaidoan_221_sang_23 = giaidoanba_sang.id
            self.giaidoan_chieu_23 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_23 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_23 = giaidoanba_sang.id
            # Ngay _24
            self.giaidoan_sang_24 = giaidoanmot_sang.id
            self.giaidoan_211_sang_24 = giaidoanhai_sang.id
            self.giaidoan_221_sang_24 = giaidoanba_sang.id
            self.giaidoan_chieu_24 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_24 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_24 = giaidoanba_sang.id
            # Ngay _25
            self.giaidoan_sang_25 = giaidoanmot_sang.id
            self.giaidoan_211_sang_25 = giaidoanhai_sang.id
            self.giaidoan_221_sang_25 = giaidoanba_sang.id
            self.giaidoan_chieu_25 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_25 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_25 = giaidoanba_sang.id
            # Ngay _26
            self.giaidoan_sang_26 = giaidoanmot_sang.id
            self.giaidoan_211_sang_26 = giaidoanhai_sang.id
            self.giaidoan_221_sang_26 = giaidoanba_sang.id
            self.giaidoan_chieu_26 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_26 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_26 = giaidoanba_sang.id
            # Ngay _27
            self.giaidoan_sang_27 = giaidoanmot_sang.id
            self.giaidoan_211_sang_27 = giaidoanhai_sang.id
            self.giaidoan_221_sang_27 = giaidoanba_sang.id
            self.giaidoan_chieu_27 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_27 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_27 = giaidoanba_sang.id
            # Ngay _28
            self.giaidoan_sang_28 = giaidoanmot_sang.id
            self.giaidoan_211_sang_28 = giaidoanhai_sang.id
            self.giaidoan_221_sang_28 = giaidoanba_sang.id
            self.giaidoan_chieu_28 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_28 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_28 = giaidoanba_sang.id
            # Ngay _29
            self.giaidoan_sang_29 = giaidoanmot_sang.id
            self.giaidoan_211_sang_29 = giaidoanhai_sang.id
            self.giaidoan_221_sang_29 = giaidoanba_sang.id
            self.giaidoan_chieu_29 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_29 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_29 = giaidoanba_sang.id
            # Ngay _30
            self.giaidoan_sang_30 = giaidoanmot_sang.id
            self.giaidoan_211_sang_30 = giaidoanhai_sang.id
            self.giaidoan_221_sang_30 = giaidoanba_sang.id
            self.giaidoan_chieu_30 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_30 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_30 = giaidoanba_sang.id
            # Ngay _31
            self.giaidoan_sang_31 = giaidoanmot_sang.id
            self.giaidoan_211_sang_31 = giaidoanhai_sang.id
            self.giaidoan_221_sang_31 = giaidoanba_sang.id
            self.giaidoan_chieu_31 = giaidoanmot_sang.id
            self.giaidoan_211_chieu_31 = giaidoanhai_sang.id
            self.giaidoan_221_chieu_31 = giaidoanba_sang.id

            return {'domain': {'giaidoan_sang_1': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_1': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_2': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_2': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_3': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_3': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_4': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_4': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_5': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_5': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_6': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_6': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_7': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_7': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_8': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_8': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_9': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_9': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_10': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_10': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_11': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_11': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_12': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_12': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_13': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_13': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_14': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_14': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_15': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_15': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_16': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_16': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_17': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_17': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_18': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_18': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_19': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_19': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_20': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_20': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_21': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_21': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_22': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_22': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_23': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_23': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_24': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_24': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_25': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_25': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_26': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_26': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_27': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_27': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_28': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_28': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_29': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_29': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_30': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_30': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_sang_31': [('giaidoan_noidung', '=', self.giaidoan_junkai)],
                               'giaidoan_chieu_31': [('giaidoan_noidung', '=', self.giaidoan_junkai)],}}

    noidung_sang_1 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_1 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_1 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_1 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_1 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_1 = fields.Char()

    @api.onchange('mot_sang_1', 'hai_sang_1', 'ba_sang_1', 'bon_sang_1')
    def onchange_method_thuctap_sang_1(self):
        if self.mot_sang_1:
            self.thuctap_sang_1 = self.mot_sang_1.ten_noidung
            if self.hai_sang_1:
                self.thuctap_sang_1 = self.hai_sang_1.ten_noidung
                if self.ba_sang_1:
                    self.thuctap_sang_1 = self.ba_sang_1.ten_noidung
                    if self.bon_sang_1:
                        self.thuctap_sang_1 = self.bon_sang_1.ten_noidung

        elif self.ngay_nghi_1:
            self.thuctap_sang_1 = '休日'
        else:
            self.thuctap_sang_1 = ''

    daotao_sang_1 = fields.Char()
    chidao_sang_1 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_1 = fields.Char(related='ma_congviec')
    giaidoan_chieu_1 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_1 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_1 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_1 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_1 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_1 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_1 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_1 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_1 = fields.Char()

    @api.onchange('mot_chieu_1', 'hai_chieu_1', 'ba_chieu_1', 'bon_chieu_1')
    def onchange_method_thuctap_chieu_1(self):
        if self.mot_chieu_1:
            self.thuctap_chieu_1 = self.mot_chieu_1.ten_noidung
            if self.hai_chieu_1:
                self.thuctap_chieu_1 = self.hai_chieu_1.ten_noidung
                if self.ba_sang_1:
                    self.ba_chieu_1 = self.ba_chieu_1.ten_noidung
                    if self.bon_chieu_1:
                        self.thuctap_chieu_1 = self.bon_chieu_1.ten_noidung

        elif self.ngay_nghi_1:
            self.thuctap_chieu_1 = '休日'
        else:
            self.thuctap_chieu_1 = ''

    daotao_chieu_1 = fields.Char()
    chidao_chieu_1 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _2
    ngay_thu_2 = fields.Char()
    ngay_nghi_2 = fields.Boolean()

    @api.onchange('ngay_nghi_2')
    def onchange_method_ngay_nghi_2(self):
        if self.ngay_nghi_2:
            self.noidung_sang_2 = ()
            self.mot_sang_2 = ()
            self.hai_sang_2 = ()
            self.ba_sang_2 = ()
            self.bon_sang_2 = ()
            self.thuctap_sang_2 = '休日'
            self.daotao_sang_2 = ''
            self.chidao_sang_2 = ()

            self.noidung_chieu_2 = ()
            self.mot_chieu_2 = ()
            self.hai_chieu_2 = ()
            self.ba_chieu_2 = ()
            self.bon_chieu_2 = ()
            self.thuctap_chieu_2 = '休日'
            self.daotao_chieu_2 = ''
            self.chidao_chieu_2 = ()
        else:
            self.noidung_sang_2 = ()
            self.mot_sang_2 = ()
            self.hai_sang_2 = ()
            self.ba_sang_2 = ()
            self.bon_sang_2 = ()
            self.thuctap_sang_2 = ''
            self.daotao_sang_2 = ''
            self.chidao_sang_2 = ()

            self.noidung_chieu_2 = ()
            self.mot_chieu_2 = ()
            self.hai_chieu_2 = ()
            self.ba_chieu_2 = ()
            self.bon_chieu_2 = ()
            self.thuctap_chieu_2 = ''
            self.daotao_chieu_2 = ''
            self.chidao_chieu_2 = ()

    ma_sang_2 = fields.Char(related='ma_congviec')
    giaidoan_sang_2 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_2 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_2 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_sang_2 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_2 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_2 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_2 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_2 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_2 = fields.Char()

    @api.onchange('mot_sang_2', 'hai_sang_2', 'ba_sang_2', 'bon_sang_2')
    def onchange_method_thuctap_sang_2(self):
        if self.mot_sang_2:
            self.thuctap_sang_2 = self.mot_sang_2.ten_noidung
            if self.hai_sang_2:
                self.thuctap_sang_2 = self.hai_sang_2.ten_noidung
                if self.ba_sang_2:
                    self.thuctap_sang_2 = self.ba_sang_2.ten_noidung
                    if self.bon_sang_2:
                        self.thuctap_sang_2 = self.bon_sang_2.ten_noidung

        elif self.ngay_nghi_2:
            self.thuctap_sang_2 = '休日'
        else:
            self.thuctap_sang_2 = ''

    daotao_sang_2 = fields.Char()
    chidao_sang_2 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_2 = fields.Char(related='ma_congviec')
    giaidoan_chieu_2 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_2 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_2 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_2 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_2 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_2 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_2 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_2 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_2 = fields.Char()

    @api.onchange('mot_chieu_2', 'hai_chieu_2', 'ba_chieu_2', 'bon_chieu_2')
    def onchange_method_thuctap_chieu_2(self):
        if self.mot_chieu_2:
            self.thuctap_chieu_2 = self.mot_chieu_2.ten_noidung
            if self.hai_chieu_2:
                self.thuctap_chieu_2 = self.hai_chieu_2.ten_noidung
                if self.ba_sang_2:
                    self.ba_chieu_2 = self.ba_chieu_2.ten_noidung
                    if self.bon_chieu_2:
                        self.thuctap_chieu_2 = self.bon_chieu_2.ten_noidung

        elif self.ngay_nghi_2:
            self.thuctap_chieu_2 = '休日'
        else:
            self.thuctap_chieu_2 = ''

    daotao_chieu_2 = fields.Char()
    chidao_chieu_2 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _3
    ngay_thu_3 = fields.Char()
    ngay_nghi_3 = fields.Boolean()

    @api.onchange('ngay_nghi_3')
    def onchange_method_ngay_nghi_3(self):
        if self.ngay_nghi_3:
            self.noidung_sang_3 = ()
            self.mot_sang_3 = ()
            self.hai_sang_3 = ()
            self.ba_sang_3 = ()
            self.bon_sang_3 = ()
            self.thuctap_sang_3 = '休日'
            self.daotao_sang_3 = ''
            self.chidao_sang_3 = ()

            self.noidung_chieu_3 = ()
            self.mot_chieu_3 = ()
            self.hai_chieu_3 = ()
            self.ba_chieu_3 = ()
            self.bon_chieu_3 = ()
            self.thuctap_chieu_3 = '休日'
            self.daotao_chieu_3 = ''
            self.chidao_chieu_3 = ()
        else:
            self.noidung_sang_3 = ()
            self.mot_sang_3 = ()
            self.hai_sang_3 = ()
            self.ba_sang_3 = ()
            self.bon_sang_3 = ()
            self.thuctap_sang_3 = ''
            self.daotao_sang_3 = ''
            self.chidao_sang_3 = ()

            self.noidung_chieu_3 = ()
            self.mot_chieu_3 = ()
            self.hai_chieu_3 = ()
            self.ba_chieu_3 = ()
            self.bon_chieu_3 = ()
            self.thuctap_chieu_3 = ''
            self.daotao_chieu_3 = ''
            self.chidao_chieu_3 = ()

    ma_sang_3 = fields.Char(related='ma_congviec')
    giaidoan_sang_3 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_3 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_3 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_3(self):
    #     giaidoanmot_sang_3 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                  limit=1)
    #     giaidoanhai_sang_3 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                  limit=1)
    #     giaidoanba_sang_3 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                limit=1)
    #
    #     self.giaidoan_sang_3 = giaidoanmot_sang_3.id
    #     self.giaidoan_211_sang_3 = giaidoanhai_sang_3.id
    #     self.giaidoan_221_sang_3 = giaidoanba_sang_3.id
    #     self.giaidoan_chieu_3 = giaidoanmot_sang_3.id
    #     self.giaidoan_211_chieu_3 = giaidoanhai_sang_3.id
    #     self.giaidoan_221_chieu_3 = giaidoanba_sang_3.id

    noidung_sang_3 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_3 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_3 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_3 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_3 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_3 = fields.Char()

    @api.onchange('mot_sang_3', 'hai_sang_3', 'ba_sang_3', 'bon_sang_3')
    def onchange_method_thuctap_sang_3(self):
        if self.mot_sang_3:
            self.thuctap_sang_3 = self.mot_sang_3.ten_noidung
            if self.hai_sang_3:
                self.thuctap_sang_3 = self.hai_sang_3.ten_noidung
                if self.ba_sang_3:
                    self.thuctap_sang_3 = self.ba_sang_3.ten_noidung
                    if self.bon_sang_3:
                        self.thuctap_sang_3 = self.bon_sang_3.ten_noidung

        elif self.ngay_nghi_3:
            self.thuctap_sang_3 = '休日'
        else:
            self.thuctap_sang_3 = ''

    daotao_sang_3 = fields.Char()
    chidao_sang_3 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_3 = fields.Char(related='ma_congviec')
    giaidoan_chieu_3 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_3 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_3 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_3 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_3 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_3 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_3 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_3 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_3 = fields.Char()

    @api.onchange('mot_chieu_3', 'hai_chieu_3', 'ba_chieu_3', 'bon_chieu_3')
    def onchange_method_thuctap_chieu_3(self):
        if self.mot_chieu_3:
            self.thuctap_chieu_3 = self.mot_chieu_3.ten_noidung
            if self.hai_chieu_3:
                self.thuctap_chieu_3 = self.hai_chieu_3.ten_noidung
                if self.ba_sang_3:
                    self.ba_chieu_3 = self.ba_chieu_3.ten_noidung
                    if self.bon_chieu_3:
                        self.thuctap_chieu_3 = self.bon_chieu_3.ten_noidung

        elif self.ngay_nghi_3:
            self.thuctap_chieu_3 = '休日'
        else:
            self.thuctap_chieu_3 = ''

    daotao_chieu_3 = fields.Char()
    chidao_chieu_3 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _4
    ngay_thu_4 = fields.Char()
    ngay_nghi_4 = fields.Boolean()

    @api.onchange('ngay_nghi_4')
    def onchange_method_ngay_nghi_4(self):
        if self.ngay_nghi_4:
            self.noidung_sang_4 = ()
            self.mot_sang_4 = ()
            self.hai_sang_4 = ()
            self.ba_sang_4 = ()
            self.bon_sang_4 = ()
            self.thuctap_sang_4 = '休日'
            self.daotao_sang_4 = ''
            self.chidao_sang_4 = ()

            self.noidung_chieu_4 = ()
            self.mot_chieu_4 = ()
            self.hai_chieu_4 = ()
            self.ba_chieu_4 = ()
            self.bon_chieu_4 = ()
            self.thuctap_chieu_4 = '休日'
            self.daotao_chieu_4 = ''
            self.chidao_chieu_4 = ()
        else:
            self.noidung_sang_4 = ()
            self.mot_sang_4 = ()
            self.hai_sang_4 = ()
            self.ba_sang_4 = ()
            self.bon_sang_4 = ()
            self.thuctap_sang_4 = ''
            self.daotao_sang_4 = ''
            self.chidao_sang_4 = ()

            self.noidung_chieu_4 = ()
            self.mot_chieu_4 = ()
            self.hai_chieu_4 = ()
            self.ba_chieu_4 = ()
            self.bon_chieu_4 = ()
            self.thuctap_chieu_4 = ''
            self.daotao_chieu_4 = ''
            self.chidao_chieu_4 = ()

    ma_sang_4 = fields.Char(related='ma_congviec')
    giaidoan_sang_4 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_4 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_4 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_4(self):
    #     giaidoanmot_sang_4 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                  limit=1)
    #     giaidoanhai_sang_4 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                  limit=1)
    #     giaidoanba_sang_4 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                limit=1)
    #
    #     self.giaidoan_sang_4 = giaidoanmot_sang_4.id
    #     self.giaidoan_211_sang_4 = giaidoanhai_sang_4.id
    #     self.giaidoan_221_sang_4 = giaidoanba_sang_4.id
    #     self.giaidoan_chieu_4 = giaidoanmot_sang_4.id
    #     self.giaidoan_211_chieu_4 = giaidoanhai_sang_4.id
    #     self.giaidoan_221_chieu_4 = giaidoanba_sang_4.id

    noidung_sang_4 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_4 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_4 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_4 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_4 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_4 = fields.Char()

    @api.onchange('mot_sang_4', 'hai_sang_4', 'ba_sang_4', 'bon_sang_4')
    def onchange_method_thuctap_sang_4(self):
        if self.mot_sang_4:
            self.thuctap_sang_4 = self.mot_sang_4.ten_noidung
            if self.hai_sang_4:
                self.thuctap_sang_4 = self.hai_sang_4.ten_noidung
                if self.ba_sang_4:
                    self.thuctap_sang_4 = self.ba_sang_4.ten_noidung
                    if self.bon_sang_4:
                        self.thuctap_sang_4 = self.bon_sang_4.ten_noidung

        elif self.ngay_nghi_4:
            self.thuctap_sang_4 = '休日'
        else:
            self.thuctap_sang_4 = ''

    daotao_sang_4 = fields.Char()
    chidao_sang_4 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_4 = fields.Char(related='ma_congviec')
    giaidoan_chieu_4 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_4 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_4 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_4 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_4 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_4 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_4 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_4 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_4 = fields.Char()

    @api.onchange('mot_chieu_4', 'hai_chieu_4', 'ba_chieu_4', 'bon_chieu_4')
    def onchange_method_thuctap_chieu_4(self):
        if self.mot_chieu_4:
            self.thuctap_chieu_4 = self.mot_chieu_4.ten_noidung
            if self.hai_chieu_4:
                self.thuctap_chieu_4 = self.hai_chieu_4.ten_noidung
                if self.ba_sang_4:
                    self.ba_chieu_4 = self.ba_chieu_4.ten_noidung
                    if self.bon_chieu_4:
                        self.thuctap_chieu_4 = self.bon_chieu_4.ten_noidung

        elif self.ngay_nghi_4:
            self.thuctap_chieu_4 = '休日'
        else:
            self.thuctap_chieu_4 = ''

    daotao_chieu_4 = fields.Char()
    chidao_chieu_4 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _5
    ngay_thu_5 = fields.Char()
    ngay_nghi_5 = fields.Boolean()

    @api.onchange('ngay_nghi_5')
    def onchange_method_ngay_nghi_5(self):
        if self.ngay_nghi_5:
            self.noidung_sang_5 = ()
            self.mot_sang_5 = ()
            self.hai_sang_5 = ()
            self.ba_sang_5 = ()
            self.bon_sang_5 = ()
            self.thuctap_sang_5 = '休日'
            self.daotao_sang_5 = ''
            self.chidao_sang_5 = ()

            self.noidung_chieu_5 = ()
            self.mot_chieu_5 = ()
            self.hai_chieu_5 = ()
            self.ba_chieu_5 = ()
            self.bon_chieu_5 = ()
            self.thuctap_chieu_5 = '休日'
            self.daotao_chieu_5 = ''
            self.chidao_chieu_5 = ()
        else:
            self.noidung_sang_5 = ()
            self.mot_sang_5 = ()
            self.hai_sang_5 = ()
            self.ba_sang_5 = ()
            self.bon_sang_5 = ()
            self.thuctap_sang_5 = ''
            self.daotao_sang_5 = ''
            self.chidao_sang_5 = ()

            self.noidung_chieu_5 = ()
            self.mot_chieu_5 = ()
            self.hai_chieu_5 = ()
            self.ba_chieu_5 = ()
            self.bon_chieu_5 = ()
            self.thuctap_chieu_5 = ''
            self.daotao_chieu_5 = ''
            self.chidao_chieu_5 = ()

    ma_sang_5 = fields.Char(related='ma_congviec')
    giaidoan_sang_5 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_5 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_5 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_5(self):
    #     giaidoanmot_sang_5 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                  limit=1)
    #     giaidoanhai_sang_5 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                  limit=1)
    #     giaidoanba_sang_5 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                limit=1)
    #
    #     self.giaidoan_sang_5 = giaidoanmot_sang_5.id
    #     self.giaidoan_211_sang_5 = giaidoanhai_sang_5.id
    #     self.giaidoan_221_sang_5 = giaidoanba_sang_5.id
    #     self.giaidoan_chieu_5 = giaidoanmot_sang_5.id
    #     self.giaidoan_211_chieu_5 = giaidoanhai_sang_5.id
    #     self.giaidoan_221_chieu_5 = giaidoanba_sang_5.id

    noidung_sang_5 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_5 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_5 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_5 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_5 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_5 = fields.Char()

    @api.onchange('mot_sang_5', 'hai_sang_5', 'ba_sang_5', 'bon_sang_5')
    def onchange_method_thuctap_sang_5(self):
        if self.mot_sang_5:
            self.thuctap_sang_5 = self.mot_sang_5.ten_noidung
            if self.hai_sang_5:
                self.thuctap_sang_5 = self.hai_sang_5.ten_noidung
                if self.ba_sang_5:
                    self.thuctap_sang_5 = self.ba_sang_5.ten_noidung
                    if self.bon_sang_5:
                        self.thuctap_sang_5 = self.bon_sang_5.ten_noidung

        elif self.ngay_nghi_5:
            self.thuctap_sang_5 = '休日'
        else:
            self.thuctap_sang_5 = ''

    daotao_sang_5 = fields.Char()
    chidao_sang_5 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_5 = fields.Char(related='ma_congviec')
    giaidoan_chieu_5 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_5 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_5 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_5 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_5 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_5 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_5 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_5 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_5 = fields.Char()

    @api.onchange('mot_chieu_5', 'hai_chieu_5', 'ba_chieu_5', 'bon_chieu_5')
    def onchange_method_thuctap_chieu_5(self):
        if self.mot_chieu_5:
            self.thuctap_chieu_5 = self.mot_chieu_5.ten_noidung
            if self.hai_chieu_5:
                self.thuctap_chieu_5 = self.hai_chieu_5.ten_noidung
                if self.ba_sang_5:
                    self.ba_chieu_5 = self.ba_chieu_5.ten_noidung
                    if self.bon_chieu_5:
                        self.thuctap_chieu_5 = self.bon_chieu_5.ten_noidung

        elif self.ngay_nghi_5:
            self.thuctap_chieu_5 = '休日'
        else:
            self.thuctap_chieu_5 = ''

    daotao_chieu_5 = fields.Char()
    chidao_chieu_5 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _6
    ngay_thu_6 = fields.Char()
    ngay_nghi_6 = fields.Boolean()

    @api.onchange('ngay_nghi_6')
    def onchange_method_ngay_nghi_6(self):
        if self.ngay_nghi_6:
            self.noidung_sang_6 = ()
            self.mot_sang_6 = ()
            self.hai_sang_6 = ()
            self.ba_sang_6 = ()
            self.bon_sang_6 = ()
            self.thuctap_sang_6 = '休日'
            self.daotao_sang_6 = ''
            self.chidao_sang_6 = ()

            self.noidung_chieu_6 = ()
            self.mot_chieu_6 = ()
            self.hai_chieu_6 = ()
            self.ba_chieu_6 = ()
            self.bon_chieu_6 = ()
            self.thuctap_chieu_6 = '休日'
            self.daotao_chieu_6 = ''
            self.chidao_chieu_6 = ()
        else:
            self.noidung_sang_6 = ()
            self.mot_sang_6 = ()
            self.hai_sang_6 = ()
            self.ba_sang_6 = ()
            self.bon_sang_6 = ()
            self.thuctap_sang_6 = ''
            self.daotao_sang_6 = ''
            self.chidao_sang_6 = ()

            self.noidung_chieu_6 = ()
            self.mot_chieu_6 = ()
            self.hai_chieu_6 = ()
            self.ba_chieu_6 = ()
            self.bon_chieu_6 = ()
            self.thuctap_chieu_6 = ''
            self.daotao_chieu_6 = ''
            self.chidao_chieu_6 = ()

    ma_sang_6 = fields.Char(related='ma_congviec')
    giaidoan_sang_6 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_6 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_6 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_6(self):
    #     giaidoanmot_sang_6 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                  limit=1)
    #     giaidoanhai_sang_6 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                  limit=1)
    #     giaidoanba_sang_6 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                limit=1)
    #
    #     self.giaidoan_sang_6 = giaidoanmot_sang_6.id
    #     self.giaidoan_211_sang_6 = giaidoanhai_sang_6.id
    #     self.giaidoan_221_sang_6 = giaidoanba_sang_6.id
    #     self.giaidoan_chieu_6 = giaidoanmot_sang_6.id
    #     self.giaidoan_211_chieu_6 = giaidoanhai_sang_6.id
    #     self.giaidoan_221_chieu_6 = giaidoanba_sang_6.id

    noidung_sang_6 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_6 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_6 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_6 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_6 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_6 = fields.Char()

    @api.onchange('mot_sang_6', 'hai_sang_6', 'ba_sang_6', 'bon_sang_6')
    def onchange_method_thuctap_sang_6(self):
        if self.mot_sang_6:
            self.thuctap_sang_6 = self.mot_sang_6.ten_noidung
            if self.hai_sang_6:
                self.thuctap_sang_6 = self.hai_sang_6.ten_noidung
                if self.ba_sang_6:
                    self.thuctap_sang_6 = self.ba_sang_6.ten_noidung
                    if self.bon_sang_6:
                        self.thuctap_sang_6 = self.bon_sang_6.ten_noidung

        elif self.ngay_nghi_6:
            self.thuctap_sang_6 = '休日'
        else:
            self.thuctap_sang_6 = ''

    daotao_sang_6 = fields.Char()
    chidao_sang_6 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_6 = fields.Char(related='ma_congviec')
    giaidoan_chieu_6 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_6 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_6 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_6 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_6 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_6 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_6 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_6 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_6 = fields.Char()

    @api.onchange('mot_chieu_6', 'hai_chieu_6', 'ba_chieu_6', 'bon_chieu_6')
    def onchange_method_thuctap_chieu_6(self):
        if self.mot_chieu_6:
            self.thuctap_chieu_6 = self.mot_chieu_6.ten_noidung
            if self.hai_chieu_6:
                self.thuctap_chieu_6 = self.hai_chieu_6.ten_noidung
                if self.ba_sang_6:
                    self.ba_chieu_6 = self.ba_chieu_6.ten_noidung
                    if self.bon_chieu_6:
                        self.thuctap_chieu_6 = self.bon_chieu_6.ten_noidung

        elif self.ngay_nghi_6:
            self.thuctap_chieu_6 = '休日'
        else:
            self.thuctap_chieu_6 = ''

    daotao_chieu_6 = fields.Char()
    chidao_chieu_6 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _7
    ngay_thu_7 = fields.Char()
    ngay_nghi_7 = fields.Boolean()

    @api.onchange('ngay_nghi_7')
    def onchange_method_ngay_nghi_7(self):
        if self.ngay_nghi_7:
            self.noidung_sang_7 = ()
            self.mot_sang_7 = ()
            self.hai_sang_7 = ()
            self.ba_sang_7 = ()
            self.bon_sang_7 = ()
            self.thuctap_sang_7 = '休日'
            self.daotao_sang_7 = ''
            self.chidao_sang_7 = ()

            self.noidung_chieu_7 = ()
            self.mot_chieu_7 = ()
            self.hai_chieu_7 = ()
            self.ba_chieu_7 = ()
            self.bon_chieu_7 = ()
            self.thuctap_chieu_7 = '休日'
            self.daotao_chieu_7 = ''
            self.chidao_chieu_7 = ()
        else:
            self.noidung_sang_7 = ()
            self.mot_sang_7 = ()
            self.hai_sang_7 = ()
            self.ba_sang_7 = ()
            self.bon_sang_7 = ()
            self.thuctap_sang_7 = ''
            self.daotao_sang_7 = ''
            self.chidao_sang_7 = ()

            self.noidung_chieu_7 = ()
            self.mot_chieu_7 = ()
            self.hai_chieu_7 = ()
            self.ba_chieu_7 = ()
            self.bon_chieu_7 = ()
            self.thuctap_chieu_7 = ''
            self.daotao_chieu_7 = ''
            self.chidao_chieu_7 = ()

    ma_sang_7 = fields.Char(related='ma_congviec')
    giaidoan_sang_7 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_7 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_7 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_7(self):
    #     giaidoanmot_sang_7 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                  limit=1)
    #     giaidoanhai_sang_7 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                  limit=1)
    #     giaidoanba_sang_7 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                limit=1)
    #
    #     self.giaidoan_sang_7 = giaidoanmot_sang_7.id
    #     self.giaidoan_211_sang_7 = giaidoanhai_sang_7.id
    #     self.giaidoan_221_sang_7 = giaidoanba_sang_7.id
    #     self.giaidoan_chieu_7 = giaidoanmot_sang_7.id
    #     self.giaidoan_211_chieu_7 = giaidoanhai_sang_7.id
    #     self.giaidoan_221_chieu_7 = giaidoanba_sang_7.id

    noidung_sang_7 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_7 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_7 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_7 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_7 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_7 = fields.Char()

    @api.onchange('mot_sang_7', 'hai_sang_7', 'ba_sang_7', 'bon_sang_7')
    def onchange_method_thuctap_sang_7(self):
        if self.mot_sang_7:
            self.thuctap_sang_7 = self.mot_sang_7.ten_noidung
            if self.hai_sang_7:
                self.thuctap_sang_7 = self.hai_sang_7.ten_noidung
                if self.ba_sang_7:
                    self.thuctap_sang_7 = self.ba_sang_7.ten_noidung
                    if self.bon_sang_7:
                        self.thuctap_sang_7 = self.bon_sang_7.ten_noidung

        elif self.ngay_nghi_7:
            self.thuctap_sang_7 = '休日'
        else:
            self.thuctap_sang_7 = ''

    daotao_sang_7 = fields.Char()
    chidao_sang_7 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_7 = fields.Char(related='ma_congviec')
    giaidoan_chieu_7 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_7 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_7 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_7 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_7 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_7 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_7 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_7 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_7 = fields.Char()

    @api.onchange('mot_chieu_7', 'hai_chieu_7', 'ba_chieu_7', 'bon_chieu_7')
    def onchange_method_thuctap_chieu_7(self):
        if self.mot_chieu_7:
            self.thuctap_chieu_7 = self.mot_chieu_7.ten_noidung
            if self.hai_chieu_7:
                self.thuctap_chieu_7 = self.hai_chieu_7.ten_noidung
                if self.ba_sang_7:
                    self.ba_chieu_7 = self.ba_chieu_7.ten_noidung
                    if self.bon_chieu_7:
                        self.thuctap_chieu_7 = self.bon_chieu_7.ten_noidung

        elif self.ngay_nghi_7:
            self.thuctap_chieu_7 = '休日'
        else:
            self.thuctap_chieu_7 = ''

    daotao_chieu_7 = fields.Char()
    chidao_chieu_7 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _8
    ngay_thu_8 = fields.Char()
    ngay_nghi_8 = fields.Boolean()

    @api.onchange('ngay_nghi_8')
    def onchange_method_ngay_nghi_8(self):
        if self.ngay_nghi_8:
            self.noidung_sang_8 = ()
            self.mot_sang_8 = ()
            self.hai_sang_8 = ()
            self.ba_sang_8 = ()
            self.bon_sang_8 = ()
            self.thuctap_sang_8 = '休日'
            self.daotao_sang_8 = ''
            self.chidao_sang_8 = ()

            self.noidung_chieu_8 = ()
            self.mot_chieu_8 = ()
            self.hai_chieu_8 = ()
            self.ba_chieu_8 = ()
            self.bon_chieu_8 = ()
            self.thuctap_chieu_8 = '休日'
            self.daotao_chieu_8 = ''
            self.chidao_chieu_8 = ()
        else:
            self.noidung_sang_8 = ()
            self.mot_sang_8 = ()
            self.hai_sang_8 = ()
            self.ba_sang_8 = ()
            self.bon_sang_8 = ()
            self.thuctap_sang_8 = ''
            self.daotao_sang_8 = ''
            self.chidao_sang_8 = ()

            self.noidung_chieu_8 = ()
            self.mot_chieu_8 = ()
            self.hai_chieu_8 = ()
            self.ba_chieu_8 = ()
            self.bon_chieu_8 = ()
            self.thuctap_chieu_8 = ''
            self.daotao_chieu_8 = ''
            self.chidao_chieu_8 = ()

    ma_sang_8 = fields.Char(related='ma_congviec')
    giaidoan_sang_8 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_8 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_8 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_8(self):
    #     giaidoanmot_sang_8 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                  limit=1)
    #     giaidoanhai_sang_8 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                  limit=1)
    #     giaidoanba_sang_8 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                limit=1)
    #
    #     self.giaidoan_sang_8 = giaidoanmot_sang_8.id
    #     self.giaidoan_211_sang_8 = giaidoanhai_sang_8.id
    #     self.giaidoan_221_sang_8 = giaidoanba_sang_8.id
    #     self.giaidoan_chieu_8 = giaidoanmot_sang_8.id
    #     self.giaidoan_211_chieu_8 = giaidoanhai_sang_8.id
    #     self.giaidoan_221_chieu_8 = giaidoanba_sang_8.id

    noidung_sang_8 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_8 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_8 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_8 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_8 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_8 = fields.Char()

    @api.onchange('mot_sang_8', 'hai_sang_8', 'ba_sang_8', 'bon_sang_8')
    def onchange_method_thuctap_sang_8(self):
        if self.mot_sang_8:
            self.thuctap_sang_8 = self.mot_sang_8.ten_noidung
            if self.hai_sang_8:
                self.thuctap_sang_8 = self.hai_sang_8.ten_noidung
                if self.ba_sang_8:
                    self.thuctap_sang_8 = self.ba_sang_8.ten_noidung
                    if self.bon_sang_8:
                        self.thuctap_sang_8 = self.bon_sang_8.ten_noidung

        elif self.ngay_nghi_8:
            self.thuctap_sang_8 = '休日'
        else:
            self.thuctap_sang_8 = ''

    daotao_sang_8 = fields.Char()
    chidao_sang_8 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_8 = fields.Char(related='ma_congviec')
    giaidoan_chieu_8 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_8 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_8 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_8 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_8 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_8 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_8 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_8 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_8 = fields.Char()

    @api.onchange('mot_chieu_8', 'hai_chieu_8', 'ba_chieu_8', 'bon_chieu_8')
    def onchange_method_thuctap_chieu_8(self):
        if self.mot_chieu_8:
            self.thuctap_chieu_8 = self.mot_chieu_8.ten_noidung
            if self.hai_chieu_8:
                self.thuctap_chieu_8 = self.hai_chieu_8.ten_noidung
                if self.ba_sang_8:
                    self.ba_chieu_8 = self.ba_chieu_8.ten_noidung
                    if self.bon_chieu_8:
                        self.thuctap_chieu_8 = self.bon_chieu_8.ten_noidung

        elif self.ngay_nghi_8:
            self.thuctap_chieu_8 = '休日'
        else:
            self.thuctap_chieu_8 = ''

    daotao_chieu_8 = fields.Char()
    chidao_chieu_8 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _9
    ngay_thu_9 = fields.Char()
    ngay_nghi_9 = fields.Boolean()

    @api.onchange('ngay_nghi_9')
    def onchange_method_ngay_nghi_9(self):
        if self.ngay_nghi_9:
            self.noidung_sang_9 = ()
            self.mot_sang_9 = ()
            self.hai_sang_9 = ()
            self.ba_sang_9 = ()
            self.bon_sang_9 = ()
            self.thuctap_sang_9 = '休日'
            self.daotao_sang_9 = ''
            self.chidao_sang_9 = ()

            self.noidung_chieu_9 = ()
            self.mot_chieu_9 = ()
            self.hai_chieu_9 = ()
            self.ba_chieu_9 = ()
            self.bon_chieu_9 = ()
            self.thuctap_chieu_9 = '休日'
            self.daotao_chieu_9 = ''
            self.chidao_chieu_9 = ()
        else:
            self.noidung_sang_9 = ()
            self.mot_sang_9 = ()
            self.hai_sang_9 = ()
            self.ba_sang_9 = ()
            self.bon_sang_9 = ()
            self.thuctap_sang_9 = ''
            self.daotao_sang_9 = ''
            self.chidao_sang_9 = ()

            self.noidung_chieu_9 = ()
            self.mot_chieu_9 = ()
            self.hai_chieu_9 = ()
            self.ba_chieu_9 = ()
            self.bon_chieu_9 = ()
            self.thuctap_chieu_9 = ''
            self.daotao_chieu_9 = ''
            self.chidao_chieu_9 = ()

    ma_sang_9 = fields.Char(related='ma_congviec')
    giaidoan_sang_9 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_9 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_9 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_9(self):
    #     giaidoanmot_sang_9 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                  limit=1)
    #     giaidoanhai_sang_9 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                  limit=1)
    #     giaidoanba_sang_9 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                limit=1)
    #
    #     self.giaidoan_sang_9 = giaidoanmot_sang_9.id
    #     self.giaidoan_211_sang_9 = giaidoanhai_sang_9.id
    #     self.giaidoan_221_sang_9 = giaidoanba_sang_9.id
    #     self.giaidoan_chieu_9 = giaidoanmot_sang_9.id
    #     self.giaidoan_211_chieu_9 = giaidoanhai_sang_9.id
    #     self.giaidoan_221_chieu_9 = giaidoanba_sang_9.id

    noidung_sang_9 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_9 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_9 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_9 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_9 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_9 = fields.Char()

    @api.onchange('mot_sang_9', 'hai_sang_9', 'ba_sang_9', 'bon_sang_9')
    def onchange_method_thuctap_sang_9(self):
        if self.mot_sang_9:
            self.thuctap_sang_9 = self.mot_sang_9.ten_noidung
            if self.hai_sang_9:
                self.thuctap_sang_9 = self.hai_sang_9.ten_noidung
                if self.ba_sang_9:
                    self.thuctap_sang_9 = self.ba_sang_9.ten_noidung
                    if self.bon_sang_9:
                        self.thuctap_sang_9 = self.bon_sang_9.ten_noidung

        elif self.ngay_nghi_9:
            self.thuctap_sang_9 = '休日'
        else:
            self.thuctap_sang_9 = ''

    daotao_sang_9 = fields.Char()
    chidao_sang_9 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_9 = fields.Char(related='ma_congviec')
    giaidoan_chieu_9 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_9 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_9 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_9 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_9 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_9 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_9 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_9 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_9 = fields.Char()

    @api.onchange('mot_chieu_9', 'hai_chieu_9', 'ba_chieu_9', 'bon_chieu_9')
    def onchange_method_thuctap_chieu_9(self):
        if self.mot_chieu_9:
            self.thuctap_chieu_9 = self.mot_chieu_9.ten_noidung
            if self.hai_chieu_9:
                self.thuctap_chieu_9 = self.hai_chieu_9.ten_noidung
                if self.ba_sang_9:
                    self.ba_chieu_9 = self.ba_chieu_9.ten_noidung
                    if self.bon_chieu_9:
                        self.thuctap_chieu_9 = self.bon_chieu_9.ten_noidung

        elif self.ngay_nghi_9:
            self.thuctap_chieu_9 = '休日'
        else:
            self.thuctap_chieu_9 = ''

    daotao_chieu_9 = fields.Char()
    chidao_chieu_9 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _10
    ngay_thu_10 = fields.Char()
    ngay_nghi_10 = fields.Boolean()

    @api.onchange('ngay_nghi_10')
    def onchange_method_ngay_nghi_10(self):
        if self.ngay_nghi_10:
            self.noidung_sang_10 = ()
            self.mot_sang_10 = ()
            self.hai_sang_10 = ()
            self.ba_sang_10 = ()
            self.bon_sang_10 = ()
            self.thuctap_sang_10 = '休日'
            self.daotao_sang_10 = ''
            self.chidao_sang_10 = ()

            self.noidung_chieu_10 = ()
            self.mot_chieu_10 = ()
            self.hai_chieu_10 = ()
            self.ba_chieu_10 = ()
            self.bon_chieu_10 = ()
            self.thuctap_chieu_10 = '休日'
            self.daotao_chieu_10 = ''
            self.chidao_chieu_10 = ()
        else:
            self.noidung_sang_10 = ()
            self.mot_sang_10 = ()
            self.hai_sang_10 = ()
            self.ba_sang_10 = ()
            self.bon_sang_10 = ()
            self.thuctap_sang_10 = ''
            self.daotao_sang_10 = ''
            self.chidao_sang_10 = ()

            self.noidung_chieu_10 = ()
            self.mot_chieu_10 = ()
            self.hai_chieu_10 = ()
            self.ba_chieu_10 = ()
            self.bon_chieu_10 = ()
            self.thuctap_chieu_10 = ''
            self.daotao_chieu_10 = ''
            self.chidao_chieu_10 = ()

    ma_sang_10 = fields.Char(related='ma_congviec')
    giaidoan_sang_10 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_10 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_10 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_10(self):
    #     giaidoanmot_sang_10 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_10 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_10 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_10 = giaidoanmot_sang_10.id
    #     self.giaidoan_211_sang_10 = giaidoanhai_sang_10.id
    #     self.giaidoan_221_sang_10 = giaidoanba_sang_10.id
    #     self.giaidoan_chieu_10 = giaidoanmot_sang_10.id
    #     self.giaidoan_211_chieu_10 = giaidoanhai_sang_10.id
    #     self.giaidoan_221_chieu_10 = giaidoanba_sang_10.id

    noidung_sang_10 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_10 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_10 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_10 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_10 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_10 = fields.Char()

    @api.onchange('mot_sang_10', 'hai_sang_10', 'ba_sang_10', 'bon_sang_10')
    def onchange_method_thuctap_sang_10(self):
        if self.mot_sang_10:
            self.thuctap_sang_10 = self.mot_sang_10.ten_noidung
            if self.hai_sang_10:
                self.thuctap_sang_10 = self.hai_sang_10.ten_noidung
                if self.ba_sang_10:
                    self.thuctap_sang_10 = self.ba_sang_10.ten_noidung
                    if self.bon_sang_10:
                        self.thuctap_sang_10 = self.bon_sang_10.ten_noidung

        elif self.ngay_nghi_10:
            self.thuctap_sang_10 = '休日'
        else:
            self.thuctap_sang_10 = ''

    daotao_sang_10 = fields.Char()
    chidao_sang_10 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_10 = fields.Char(related='ma_congviec')
    giaidoan_chieu_10 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_10 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_10 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_10 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_10 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_10 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_10 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_10 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_10 = fields.Char()

    @api.onchange('mot_chieu_10', 'hai_chieu_10', 'ba_chieu_10', 'bon_chieu_10')
    def onchange_method_thuctap_chieu_10(self):
        if self.mot_chieu_10:
            self.thuctap_chieu_10 = self.mot_chieu_10.ten_noidung
            if self.hai_chieu_10:
                self.thuctap_chieu_10 = self.hai_chieu_10.ten_noidung
                if self.ba_sang_10:
                    self.ba_chieu_10 = self.ba_chieu_10.ten_noidung
                    if self.bon_chieu_10:
                        self.thuctap_chieu_10 = self.bon_chieu_10.ten_noidung

        elif self.ngay_nghi_10:
            self.thuctap_chieu_10 = '休日'
        else:
            self.thuctap_chieu_10 = ''

    daotao_chieu_10 = fields.Char()
    chidao_chieu_10 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _11
    ngay_thu_11 = fields.Char()
    ngay_nghi_11 = fields.Boolean()

    @api.onchange('ngay_nghi_11')
    def onchange_method_ngay_nghi_11(self):
        if self.ngay_nghi_11:
            self.noidung_sang_11 = ()
            self.mot_sang_11 = ()
            self.hai_sang_11 = ()
            self.ba_sang_11 = ()
            self.bon_sang_11 = ()
            self.thuctap_sang_11 = '休日'
            self.daotao_sang_11 = ''
            self.chidao_sang_11 = ()

            self.noidung_chieu_11 = ()
            self.mot_chieu_11 = ()
            self.hai_chieu_11 = ()
            self.ba_chieu_11 = ()
            self.bon_chieu_11 = ()
            self.thuctap_chieu_11 = '休日'
            self.daotao_chieu_11 = ''
            self.chidao_chieu_11 = ()
        else:
            self.noidung_sang_11 = ()
            self.mot_sang_11 = ()
            self.hai_sang_11 = ()
            self.ba_sang_11 = ()
            self.bon_sang_11 = ()
            self.thuctap_sang_11 = ''
            self.daotao_sang_11 = ''
            self.chidao_sang_11 = ()

            self.noidung_chieu_11 = ()
            self.mot_chieu_11 = ()
            self.hai_chieu_11 = ()
            self.ba_chieu_11 = ()
            self.bon_chieu_11 = ()
            self.thuctap_chieu_11 = ''
            self.daotao_chieu_11 = ''
            self.chidao_chieu_11 = ()

    ma_sang_11 = fields.Char(related='ma_congviec')
    giaidoan_sang_11 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_11 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_11 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_11(self):
    #     giaidoanmot_sang_11 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_11 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_11 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_11 = giaidoanmot_sang_11.id
    #     self.giaidoan_211_sang_11 = giaidoanhai_sang_11.id
    #     self.giaidoan_221_sang_11 = giaidoanba_sang_11.id
    #     self.giaidoan_chieu_11 = giaidoanmot_sang_11.id
    #     self.giaidoan_211_chieu_11 = giaidoanhai_sang_11.id
    #     self.giaidoan_221_chieu_11 = giaidoanba_sang_11.id

    noidung_sang_11 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_11 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_11 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_11 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_11 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_11 = fields.Char()

    @api.onchange('mot_sang_11', 'hai_sang_11', 'ba_sang_11', 'bon_sang_11')
    def onchange_method_thuctap_sang_11(self):
        if self.mot_sang_11:
            self.thuctap_sang_11 = self.mot_sang_11.ten_noidung
            if self.hai_sang_11:
                self.thuctap_sang_11 = self.hai_sang_11.ten_noidung
                if self.ba_sang_11:
                    self.thuctap_sang_11 = self.ba_sang_11.ten_noidung
                    if self.bon_sang_11:
                        self.thuctap_sang_11 = self.bon_sang_11.ten_noidung

        elif self.ngay_nghi_11:
            self.thuctap_sang_11 = '休日'
        else:
            self.thuctap_sang_11 = ''

    daotao_sang_11 = fields.Char()
    chidao_sang_11 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_11 = fields.Char(related='ma_congviec')
    giaidoan_chieu_11 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_11 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_11 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_11 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_11 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_11 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_11 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_11 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_11 = fields.Char()

    @api.onchange('mot_chieu_11', 'hai_chieu_11', 'ba_chieu_11', 'bon_chieu_11')
    def onchange_method_thuctap_chieu_11(self):
        if self.mot_chieu_11:
            self.thuctap_chieu_11 = self.mot_chieu_11.ten_noidung
            if self.hai_chieu_11:
                self.thuctap_chieu_11 = self.hai_chieu_11.ten_noidung
                if self.ba_sang_11:
                    self.ba_chieu_11 = self.ba_chieu_11.ten_noidung
                    if self.bon_chieu_11:
                        self.thuctap_chieu_11 = self.bon_chieu_11.ten_noidung

        elif self.ngay_nghi_11:
            self.thuctap_chieu_11 = '休日'
        else:
            self.thuctap_chieu_11 = ''

    daotao_chieu_11 = fields.Char()
    chidao_chieu_11 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _12
    ngay_thu_12 = fields.Char()
    ngay_nghi_12 = fields.Boolean()

    @api.onchange('ngay_nghi_12')
    def onchange_method_ngay_nghi_12(self):
        if self.ngay_nghi_12:
            self.noidung_sang_12 = ()
            self.mot_sang_12 = ()
            self.hai_sang_12 = ()
            self.ba_sang_12 = ()
            self.bon_sang_12 = ()
            self.thuctap_sang_12 = '休日'
            self.daotao_sang_12 = ''
            self.chidao_sang_12 = ()

            self.noidung_chieu_12 = ()
            self.mot_chieu_12 = ()
            self.hai_chieu_12 = ()
            self.ba_chieu_12 = ()
            self.bon_chieu_12 = ()
            self.thuctap_chieu_12 = '休日'
            self.daotao_chieu_12 = ''
            self.chidao_chieu_12 = ()
        else:
            self.noidung_sang_12 = ()
            self.mot_sang_12 = ()
            self.hai_sang_12 = ()
            self.ba_sang_12 = ()
            self.bon_sang_12 = ()
            self.thuctap_sang_12 = ''
            self.daotao_sang_12 = ''
            self.chidao_sang_12 = ()

            self.noidung_chieu_12 = ()
            self.mot_chieu_12 = ()
            self.hai_chieu_12 = ()
            self.ba_chieu_12 = ()
            self.bon_chieu_12 = ()
            self.thuctap_chieu_12 = ''
            self.daotao_chieu_12 = ''
            self.chidao_chieu_12 = ()

    ma_sang_12 = fields.Char(related='ma_congviec')
    giaidoan_sang_12 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_12 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_12 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_12(self):
    #     giaidoanmot_sang_12 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_12 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_12 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_12 = giaidoanmot_sang_12.id
    #     self.giaidoan_211_sang_12 = giaidoanhai_sang_12.id
    #     self.giaidoan_221_sang_12 = giaidoanba_sang_12.id
    #     self.giaidoan_chieu_12 = giaidoanmot_sang_12.id
    #     self.giaidoan_211_chieu_12 = giaidoanhai_sang_12.id
    #     self.giaidoan_221_chieu_12 = giaidoanba_sang_12.id

    noidung_sang_12 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_12 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_12 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_12 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_12 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_12 = fields.Char()

    @api.onchange('mot_sang_12', 'hai_sang_12', 'ba_sang_12', 'bon_sang_12')
    def onchange_method_thuctap_sang_12(self):
        if self.mot_sang_12:
            self.thuctap_sang_12 = self.mot_sang_12.ten_noidung
            if self.hai_sang_12:
                self.thuctap_sang_12 = self.hai_sang_12.ten_noidung
                if self.ba_sang_12:
                    self.thuctap_sang_12 = self.ba_sang_12.ten_noidung
                    if self.bon_sang_12:
                        self.thuctap_sang_12 = self.bon_sang_12.ten_noidung

        elif self.ngay_nghi_12:
            self.thuctap_sang_12 = '休日'
        else:
            self.thuctap_sang_12 = ''

    daotao_sang_12 = fields.Char()
    chidao_sang_12 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_12 = fields.Char(related='ma_congviec')
    giaidoan_chieu_12 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_12 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_12 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_12 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_12 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_12 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_12 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_12 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_12 = fields.Char()

    @api.onchange('mot_chieu_12', 'hai_chieu_12', 'ba_chieu_12', 'bon_chieu_12')
    def onchange_method_thuctap_chieu_12(self):
        if self.mot_chieu_12:
            self.thuctap_chieu_12 = self.mot_chieu_12.ten_noidung
            if self.hai_chieu_12:
                self.thuctap_chieu_12 = self.hai_chieu_12.ten_noidung
                if self.ba_sang_12:
                    self.ba_chieu_12 = self.ba_chieu_12.ten_noidung
                    if self.bon_chieu_12:
                        self.thuctap_chieu_12 = self.bon_chieu_12.ten_noidung

        elif self.ngay_nghi_12:
            self.thuctap_chieu_12 = '休日'
        else:
            self.thuctap_chieu_12 = ''

    daotao_chieu_12 = fields.Char()
    chidao_chieu_12 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _13
    ngay_thu_13 = fields.Char()
    ngay_nghi_13 = fields.Boolean()

    @api.onchange('ngay_nghi_13')
    def onchange_method_ngay_nghi_13(self):
        if self.ngay_nghi_13:
            self.noidung_sang_13 = ()
            self.mot_sang_13 = ()
            self.hai_sang_13 = ()
            self.ba_sang_13 = ()
            self.bon_sang_13 = ()
            self.thuctap_sang_13 = '休日'
            self.daotao_sang_13 = ''
            self.chidao_sang_13 = ()

            self.noidung_chieu_13 = ()
            self.mot_chieu_13 = ()
            self.hai_chieu_13 = ()
            self.ba_chieu_13 = ()
            self.bon_chieu_13 = ()
            self.thuctap_chieu_13 = '休日'
            self.daotao_chieu_13 = ''
            self.chidao_chieu_13 = ()
        else:
            self.noidung_sang_13 = ()
            self.mot_sang_13 = ()
            self.hai_sang_13 = ()
            self.ba_sang_13 = ()
            self.bon_sang_13 = ()
            self.thuctap_sang_13 = ''
            self.daotao_sang_13 = ''
            self.chidao_sang_13 = ()

            self.noidung_chieu_13 = ()
            self.mot_chieu_13 = ()
            self.hai_chieu_13 = ()
            self.ba_chieu_13 = ()
            self.bon_chieu_13 = ()
            self.thuctap_chieu_13 = ''
            self.daotao_chieu_13 = ''
            self.chidao_chieu_13 = ()

    ma_sang_13 = fields.Char(related='ma_congviec')
    giaidoan_sang_13 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_13 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_13 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_13(self):
    #     giaidoanmot_sang_13 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_13 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_13 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_13 = giaidoanmot_sang_13.id
    #     self.giaidoan_211_sang_13 = giaidoanhai_sang_13.id
    #     self.giaidoan_221_sang_13 = giaidoanba_sang_13.id
    #     self.giaidoan_chieu_13 = giaidoanmot_sang_13.id
    #     self.giaidoan_211_chieu_13 = giaidoanhai_sang_13.id
    #     self.giaidoan_221_chieu_13 = giaidoanba_sang_13.id

    noidung_sang_13 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_13 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_13 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_13 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_13 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_13 = fields.Char()

    @api.onchange('mot_sang_13', 'hai_sang_13', 'ba_sang_13', 'bon_sang_13')
    def onchange_method_thuctap_sang_13(self):
        if self.mot_sang_13:
            self.thuctap_sang_13 = self.mot_sang_13.ten_noidung
            if self.hai_sang_13:
                self.thuctap_sang_13 = self.hai_sang_13.ten_noidung
                if self.ba_sang_13:
                    self.thuctap_sang_13 = self.ba_sang_13.ten_noidung
                    if self.bon_sang_13:
                        self.thuctap_sang_13 = self.bon_sang_13.ten_noidung

        elif self.ngay_nghi_13:
            self.thuctap_sang_13 = '休日'
        else:
            self.thuctap_sang_13 = ''

    daotao_sang_13 = fields.Char()
    chidao_sang_13 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_13 = fields.Char(related='ma_congviec')
    giaidoan_chieu_13 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_13 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_13 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_13 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_13 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_13 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_13 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_13 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_13 = fields.Char()

    @api.onchange('mot_chieu_13', 'hai_chieu_13', 'ba_chieu_13', 'bon_chieu_13')
    def onchange_method_thuctap_chieu_13(self):
        if self.mot_chieu_13:
            self.thuctap_chieu_13 = self.mot_chieu_13.ten_noidung
            if self.hai_chieu_13:
                self.thuctap_chieu_13 = self.hai_chieu_13.ten_noidung
                if self.ba_sang_13:
                    self.ba_chieu_13 = self.ba_chieu_13.ten_noidung
                    if self.bon_chieu_13:
                        self.thuctap_chieu_13 = self.bon_chieu_13.ten_noidung

        elif self.ngay_nghi_13:
            self.thuctap_chieu_13 = '休日'
        else:
            self.thuctap_chieu_13 = ''

    daotao_chieu_13 = fields.Char()
    chidao_chieu_13 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _14
    ngay_thu_14 = fields.Char()
    ngay_nghi_14 = fields.Boolean()

    @api.onchange('ngay_nghi_14')
    def onchange_method_ngay_nghi_14(self):
        if self.ngay_nghi_14:
            self.noidung_sang_14 = ()
            self.mot_sang_14 = ()
            self.hai_sang_14 = ()
            self.ba_sang_14 = ()
            self.bon_sang_14 = ()
            self.thuctap_sang_14 = '休日'
            self.daotao_sang_14 = ''
            self.chidao_sang_14 = ()

            self.noidung_chieu_14 = ()
            self.mot_chieu_14 = ()
            self.hai_chieu_14 = ()
            self.ba_chieu_14 = ()
            self.bon_chieu_14 = ()
            self.thuctap_chieu_14 = '休日'
            self.daotao_chieu_14 = ''
            self.chidao_chieu_14 = ()
        else:
            self.noidung_sang_14 = ()
            self.mot_sang_14 = ()
            self.hai_sang_14 = ()
            self.ba_sang_14 = ()
            self.bon_sang_14 = ()
            self.thuctap_sang_14 = ''
            self.daotao_sang_14 = ''
            self.chidao_sang_14 = ()

            self.noidung_chieu_14 = ()
            self.mot_chieu_14 = ()
            self.hai_chieu_14 = ()
            self.ba_chieu_14 = ()
            self.bon_chieu_14 = ()
            self.thuctap_chieu_14 = ''
            self.daotao_chieu_14 = ''
            self.chidao_chieu_14 = ()

    ma_sang_14 = fields.Char(related='ma_congviec')
    giaidoan_sang_14 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_14 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_14 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_14(self):
    #     giaidoanmot_sang_14 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_14 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_14 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_14 = giaidoanmot_sang_14.id
    #     self.giaidoan_211_sang_14 = giaidoanhai_sang_14.id
    #     self.giaidoan_221_sang_14 = giaidoanba_sang_14.id
    #     self.giaidoan_chieu_14 = giaidoanmot_sang_14.id
    #     self.giaidoan_211_chieu_14 = giaidoanhai_sang_14.id
    #     self.giaidoan_221_chieu_14 = giaidoanba_sang_14.id

    noidung_sang_14 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_14 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_14 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_14 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_14 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_14 = fields.Char()

    @api.onchange('mot_sang_14', 'hai_sang_14', 'ba_sang_14', 'bon_sang_14')
    def onchange_method_thuctap_sang_14(self):
        if self.mot_sang_14:
            self.thuctap_sang_14 = self.mot_sang_14.ten_noidung
            if self.hai_sang_14:
                self.thuctap_sang_14 = self.hai_sang_14.ten_noidung
                if self.ba_sang_14:
                    self.thuctap_sang_14 = self.ba_sang_14.ten_noidung
                    if self.bon_sang_14:
                        self.thuctap_sang_14 = self.bon_sang_14.ten_noidung

        elif self.ngay_nghi_14:
            self.thuctap_sang_14 = '休日'
        else:
            self.thuctap_sang_14 = ''

    daotao_sang_14 = fields.Char()
    chidao_sang_14 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_14 = fields.Char(related='ma_congviec')
    giaidoan_chieu_14 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_14 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_14 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_14 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_14 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_14 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_14 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_14 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_14 = fields.Char()

    @api.onchange('mot_chieu_14', 'hai_chieu_14', 'ba_chieu_14', 'bon_chieu_14')
    def onchange_method_thuctap_chieu_14(self):
        if self.mot_chieu_14:
            self.thuctap_chieu_14 = self.mot_chieu_14.ten_noidung
            if self.hai_chieu_14:
                self.thuctap_chieu_14 = self.hai_chieu_14.ten_noidung
                if self.ba_sang_14:
                    self.ba_chieu_14 = self.ba_chieu_14.ten_noidung
                    if self.bon_chieu_14:
                        self.thuctap_chieu_14 = self.bon_chieu_14.ten_noidung

        elif self.ngay_nghi_14:
            self.thuctap_chieu_14 = '休日'
        else:
            self.thuctap_chieu_14 = ''

    daotao_chieu_14 = fields.Char()
    chidao_chieu_14 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _15
    ngay_thu_15 = fields.Char()
    ngay_nghi_15 = fields.Boolean()

    @api.onchange('ngay_nghi_15')
    def onchange_method_ngay_nghi_15(self):
        if self.ngay_nghi_15:
            self.noidung_sang_15 = ()
            self.mot_sang_15 = ()
            self.hai_sang_15 = ()
            self.ba_sang_15 = ()
            self.bon_sang_15 = ()
            self.thuctap_sang_15 = '休日'
            self.daotao_sang_15 = ''
            self.chidao_sang_15 = ()

            self.noidung_chieu_15 = ()
            self.mot_chieu_15 = ()
            self.hai_chieu_15 = ()
            self.ba_chieu_15 = ()
            self.bon_chieu_15 = ()
            self.thuctap_chieu_15 = '休日'
            self.daotao_chieu_15 = ''
            self.chidao_chieu_15 = ()
        else:
            self.noidung_sang_15 = ()
            self.mot_sang_15 = ()
            self.hai_sang_15 = ()
            self.ba_sang_15 = ()
            self.bon_sang_15 = ()
            self.thuctap_sang_15 = ''
            self.daotao_sang_15 = ''
            self.chidao_sang_15 = ()

            self.noidung_chieu_15 = ()
            self.mot_chieu_15 = ()
            self.hai_chieu_15 = ()
            self.ba_chieu_15 = ()
            self.bon_chieu_15 = ()
            self.thuctap_chieu_15 = ''
            self.daotao_chieu_15 = ''
            self.chidao_chieu_15 = ()

    ma_sang_15 = fields.Char(related='ma_congviec')
    giaidoan_sang_15 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_15 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_15 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_15(self):
    #     giaidoanmot_sang_15 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_15 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_15 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_15 = giaidoanmot_sang_15.id
    #     self.giaidoan_211_sang_15 = giaidoanhai_sang_15.id
    #     self.giaidoan_221_sang_15 = giaidoanba_sang_15.id
    #     self.giaidoan_chieu_15 = giaidoanmot_sang_15.id
    #     self.giaidoan_211_chieu_15 = giaidoanhai_sang_15.id
    #     self.giaidoan_221_chieu_15 = giaidoanba_sang_15.id

    noidung_sang_15 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_15 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_15 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_15 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_15 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_15 = fields.Char()

    @api.onchange('mot_sang_15', 'hai_sang_15', 'ba_sang_15', 'bon_sang_15')
    def onchange_method_thuctap_sang_15(self):
        if self.mot_sang_15:
            self.thuctap_sang_15 = self.mot_sang_15.ten_noidung
            if self.hai_sang_15:
                self.thuctap_sang_15 = self.hai_sang_15.ten_noidung
                if self.ba_sang_15:
                    self.thuctap_sang_15 = self.ba_sang_15.ten_noidung
                    if self.bon_sang_15:
                        self.thuctap_sang_15 = self.bon_sang_15.ten_noidung

        elif self.ngay_nghi_15:
            self.thuctap_sang_15 = '休日'
        else:
            self.thuctap_sang_15 = ''

    daotao_sang_15 = fields.Char()
    chidao_sang_15 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_15 = fields.Char(related='ma_congviec')
    giaidoan_chieu_15 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_15 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_15 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_15 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_15 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_15 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_15 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_15 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_15 = fields.Char()

    @api.onchange('mot_chieu_15', 'hai_chieu_15', 'ba_chieu_15', 'bon_chieu_15')
    def onchange_method_thuctap_chieu_15(self):
        if self.mot_chieu_15:
            self.thuctap_chieu_15 = self.mot_chieu_15.ten_noidung
            if self.hai_chieu_15:
                self.thuctap_chieu_15 = self.hai_chieu_15.ten_noidung
                if self.ba_sang_15:
                    self.ba_chieu_15 = self.ba_chieu_15.ten_noidung
                    if self.bon_chieu_15:
                        self.thuctap_chieu_15 = self.bon_chieu_15.ten_noidung

        elif self.ngay_nghi_15:
            self.thuctap_chieu_15 = '休日'
        else:
            self.thuctap_chieu_15 = ''

    daotao_chieu_15 = fields.Char()
    chidao_chieu_15 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _16
    ngay_thu_16 = fields.Char()
    ngay_nghi_16 = fields.Boolean()

    @api.onchange('ngay_nghi_16')
    def onchange_method_ngay_nghi_16(self):
        if self.ngay_nghi_16:
            self.noidung_sang_16 = ()
            self.mot_sang_16 = ()
            self.hai_sang_16 = ()
            self.ba_sang_16 = ()
            self.bon_sang_16 = ()
            self.thuctap_sang_16 = '休日'
            self.daotao_sang_16 = ''
            self.chidao_sang_16 = ()

            self.noidung_chieu_16 = ()
            self.mot_chieu_16 = ()
            self.hai_chieu_16 = ()
            self.ba_chieu_16 = ()
            self.bon_chieu_16 = ()
            self.thuctap_chieu_16 = '休日'
            self.daotao_chieu_16 = ''
            self.chidao_chieu_16 = ()
        else:
            self.noidung_sang_16 = ()
            self.mot_sang_16 = ()
            self.hai_sang_16 = ()
            self.ba_sang_16 = ()
            self.bon_sang_16 = ()
            self.thuctap_sang_16 = ''
            self.daotao_sang_16 = ''
            self.chidao_sang_16 = ()

            self.noidung_chieu_16 = ()
            self.mot_chieu_16 = ()
            self.hai_chieu_16 = ()
            self.ba_chieu_16 = ()
            self.bon_chieu_16 = ()
            self.thuctap_chieu_16 = ''
            self.daotao_chieu_16 = ''
            self.chidao_chieu_16 = ()

    ma_sang_16 = fields.Char(related='ma_congviec')
    giaidoan_sang_16 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_16 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_16 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_16(self):
    #     giaidoanmot_sang_16 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_16 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_16 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_16 = giaidoanmot_sang_16.id
    #     self.giaidoan_211_sang_16 = giaidoanhai_sang_16.id
    #     self.giaidoan_221_sang_16 = giaidoanba_sang_16.id
    #     self.giaidoan_chieu_16 = giaidoanmot_sang_16.id
    #     self.giaidoan_211_chieu_16 = giaidoanhai_sang_16.id
    #     self.giaidoan_221_chieu_16 = giaidoanba_sang_16.id

    noidung_sang_16 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_16 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_16 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_16 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_16 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_16 = fields.Char()

    @api.onchange('mot_sang_16', 'hai_sang_16', 'ba_sang_16', 'bon_sang_16')
    def onchange_method_thuctap_sang_16(self):
        if self.mot_sang_16:
            self.thuctap_sang_16 = self.mot_sang_16.ten_noidung
            if self.hai_sang_16:
                self.thuctap_sang_16 = self.hai_sang_16.ten_noidung
                if self.ba_sang_16:
                    self.thuctap_sang_16 = self.ba_sang_16.ten_noidung
                    if self.bon_sang_16:
                        self.thuctap_sang_16 = self.bon_sang_16.ten_noidung

        elif self.ngay_nghi_16:
            self.thuctap_sang_16 = '休日'
        else:
            self.thuctap_sang_16 = ''

    daotao_sang_16 = fields.Char()
    chidao_sang_16 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_16 = fields.Char(related='ma_congviec')
    giaidoan_chieu_16 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_16 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_16 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_16 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_16 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_16 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_16 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_16 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_16 = fields.Char()

    @api.onchange('mot_chieu_16', 'hai_chieu_16', 'ba_chieu_16', 'bon_chieu_16')
    def onchange_method_thuctap_chieu_16(self):
        if self.mot_chieu_16:
            self.thuctap_chieu_16 = self.mot_chieu_16.ten_noidung
            if self.hai_chieu_16:
                self.thuctap_chieu_16 = self.hai_chieu_16.ten_noidung
                if self.ba_sang_16:
                    self.ba_chieu_16 = self.ba_chieu_16.ten_noidung
                    if self.bon_chieu_16:
                        self.thuctap_chieu_16 = self.bon_chieu_16.ten_noidung

        elif self.ngay_nghi_16:
            self.thuctap_chieu_16 = '休日'
        else:
            self.thuctap_chieu_16 = ''

    daotao_chieu_16 = fields.Char()
    chidao_chieu_16 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _17
    ngay_thu_17 = fields.Char()
    ngay_nghi_17 = fields.Boolean()

    @api.onchange('ngay_nghi_17')
    def onchange_method_ngay_nghi_17(self):
        if self.ngay_nghi_17:
            self.noidung_sang_17 = ()
            self.mot_sang_17 = ()
            self.hai_sang_17 = ()
            self.ba_sang_17 = ()
            self.bon_sang_17 = ()
            self.thuctap_sang_17 = '休日'
            self.daotao_sang_17 = ''
            self.chidao_sang_17 = ()

            self.noidung_chieu_17 = ()
            self.mot_chieu_17 = ()
            self.hai_chieu_17 = ()
            self.ba_chieu_17 = ()
            self.bon_chieu_17 = ()
            self.thuctap_chieu_17 = '休日'
            self.daotao_chieu_17 = ''
            self.chidao_chieu_17 = ()
        else:
            self.noidung_sang_17 = ()
            self.mot_sang_17 = ()
            self.hai_sang_17 = ()
            self.ba_sang_17 = ()
            self.bon_sang_17 = ()
            self.thuctap_sang_17 = ''
            self.daotao_sang_17 = ''
            self.chidao_sang_17 = ()

            self.noidung_chieu_17 = ()
            self.mot_chieu_17 = ()
            self.hai_chieu_17 = ()
            self.ba_chieu_17 = ()
            self.bon_chieu_17 = ()
            self.thuctap_chieu_17 = ''
            self.daotao_chieu_17 = ''
            self.chidao_chieu_17 = ()

    ma_sang_17 = fields.Char(related='ma_congviec')
    giaidoan_sang_17 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_17 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_17 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_17(self):
    #     giaidoanmot_sang_17 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_17 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_17 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_17 = giaidoanmot_sang_17.id
    #     self.giaidoan_211_sang_17 = giaidoanhai_sang_17.id
    #     self.giaidoan_221_sang_17 = giaidoanba_sang_17.id
    #     self.giaidoan_chieu_17 = giaidoanmot_sang_17.id
    #     self.giaidoan_211_chieu_17 = giaidoanhai_sang_17.id
    #     self.giaidoan_221_chieu_17 = giaidoanba_sang_17.id

    noidung_sang_17 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_17 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_17 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_17 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_17 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_17 = fields.Char()

    @api.onchange('mot_sang_17', 'hai_sang_17', 'ba_sang_17', 'bon_sang_17')
    def onchange_method_thuctap_sang_17(self):
        if self.mot_sang_17:
            self.thuctap_sang_17 = self.mot_sang_17.ten_noidung
            if self.hai_sang_17:
                self.thuctap_sang_17 = self.hai_sang_17.ten_noidung
                if self.ba_sang_17:
                    self.thuctap_sang_17 = self.ba_sang_17.ten_noidung
                    if self.bon_sang_17:
                        self.thuctap_sang_17 = self.bon_sang_17.ten_noidung

        elif self.ngay_nghi_17:
            self.thuctap_sang_17 = '休日'
        else:
            self.thuctap_sang_17 = ''

    daotao_sang_17 = fields.Char()
    chidao_sang_17 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_17 = fields.Char(related='ma_congviec')
    giaidoan_chieu_17 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_17 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_17 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_17 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_17 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_17 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_17 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_17 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_17 = fields.Char()

    @api.onchange('mot_chieu_17', 'hai_chieu_17', 'ba_chieu_17', 'bon_chieu_17')
    def onchange_method_thuctap_chieu_17(self):
        if self.mot_chieu_17:
            self.thuctap_chieu_17 = self.mot_chieu_17.ten_noidung
            if self.hai_chieu_17:
                self.thuctap_chieu_17 = self.hai_chieu_17.ten_noidung
                if self.ba_sang_17:
                    self.ba_chieu_17 = self.ba_chieu_17.ten_noidung
                    if self.bon_chieu_17:
                        self.thuctap_chieu_17 = self.bon_chieu_17.ten_noidung

        elif self.ngay_nghi_17:
            self.thuctap_chieu_17 = '休日'
        else:
            self.thuctap_chieu_17 = ''

    daotao_chieu_17 = fields.Char()
    chidao_chieu_17 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _18
    ngay_thu_18 = fields.Char()
    ngay_nghi_18 = fields.Boolean()

    @api.onchange('ngay_nghi_18')
    def onchange_method_ngay_nghi_18(self):
        if self.ngay_nghi_18:
            self.noidung_sang_18 = ()
            self.mot_sang_18 = ()
            self.hai_sang_18 = ()
            self.ba_sang_18 = ()
            self.bon_sang_18 = ()
            self.thuctap_sang_18 = '休日'
            self.daotao_sang_18 = ''
            self.chidao_sang_18 = ()

            self.noidung_chieu_18 = ()
            self.mot_chieu_18 = ()
            self.hai_chieu_18 = ()
            self.ba_chieu_18 = ()
            self.bon_chieu_18 = ()
            self.thuctap_chieu_18 = '休日'
            self.daotao_chieu_18 = ''
            self.chidao_chieu_18 = ()
        else:
            self.noidung_sang_18 = ()
            self.mot_sang_18 = ()
            self.hai_sang_18 = ()
            self.ba_sang_18 = ()
            self.bon_sang_18 = ()
            self.thuctap_sang_18 = ''
            self.daotao_sang_18 = ''
            self.chidao_sang_18 = ()

            self.noidung_chieu_18 = ()
            self.mot_chieu_18 = ()
            self.hai_chieu_18 = ()
            self.ba_chieu_18 = ()
            self.bon_chieu_18 = ()
            self.thuctap_chieu_18 = ''
            self.daotao_chieu_18 = ''
            self.chidao_chieu_18 = ()

    ma_sang_18 = fields.Char(related='ma_congviec')
    giaidoan_sang_18 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_18 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_18 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_18(self):
    #     giaidoanmot_sang_18 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_18 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_18 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_18 = giaidoanmot_sang_18.id
    #     self.giaidoan_211_sang_18 = giaidoanhai_sang_18.id
    #     self.giaidoan_221_sang_18 = giaidoanba_sang_18.id
    #     self.giaidoan_chieu_18 = giaidoanmot_sang_18.id
    #     self.giaidoan_211_chieu_18 = giaidoanhai_sang_18.id
    #     self.giaidoan_221_chieu_18 = giaidoanba_sang_18.id

    noidung_sang_18 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_18 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_18 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_18 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_18 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_18 = fields.Char()

    @api.onchange('mot_sang_18', 'hai_sang_18', 'ba_sang_18', 'bon_sang_18')
    def onchange_method_thuctap_sang_18(self):
        if self.mot_sang_18:
            self.thuctap_sang_18 = self.mot_sang_18.ten_noidung
            if self.hai_sang_18:
                self.thuctap_sang_18 = self.hai_sang_18.ten_noidung
                if self.ba_sang_18:
                    self.thuctap_sang_18 = self.ba_sang_18.ten_noidung
                    if self.bon_sang_18:
                        self.thuctap_sang_18 = self.bon_sang_18.ten_noidung

        elif self.ngay_nghi_18:
            self.thuctap_sang_18 = '休日'
        else:
            self.thuctap_sang_18 = ''

    daotao_sang_18 = fields.Char()
    chidao_sang_18 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_18 = fields.Char(related='ma_congviec')
    giaidoan_chieu_18 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_18 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_18 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_18 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_18 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_18 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_18 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_18 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_18 = fields.Char()

    @api.onchange('mot_chieu_18', 'hai_chieu_18', 'ba_chieu_18', 'bon_chieu_18')
    def onchange_method_thuctap_chieu_18(self):
        if self.mot_chieu_18:
            self.thuctap_chieu_18 = self.mot_chieu_18.ten_noidung
            if self.hai_chieu_18:
                self.thuctap_chieu_18 = self.hai_chieu_18.ten_noidung
                if self.ba_sang_18:
                    self.ba_chieu_18 = self.ba_chieu_18.ten_noidung
                    if self.bon_chieu_18:
                        self.thuctap_chieu_18 = self.bon_chieu_18.ten_noidung

        elif self.ngay_nghi_18:
            self.thuctap_chieu_18 = '休日'
        else:
            self.thuctap_chieu_18 = ''

    daotao_chieu_18 = fields.Char()
    chidao_chieu_18 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _19
    ngay_thu_19 = fields.Char()
    ngay_nghi_19 = fields.Boolean()

    @api.onchange('ngay_nghi_19')
    def onchange_method_ngay_nghi_19(self):
        if self.ngay_nghi_19:
            self.noidung_sang_19 = ()
            self.mot_sang_19 = ()
            self.hai_sang_19 = ()
            self.ba_sang_19 = ()
            self.bon_sang_19 = ()
            self.thuctap_sang_19 = '休日'
            self.daotao_sang_19 = ''
            self.chidao_sang_19 = ()

            self.noidung_chieu_19 = ()
            self.mot_chieu_19 = ()
            self.hai_chieu_19 = ()
            self.ba_chieu_19 = ()
            self.bon_chieu_19 = ()
            self.thuctap_chieu_19 = '休日'
            self.daotao_chieu_19 = ''
            self.chidao_chieu_19 = ()
        else:
            self.noidung_sang_19 = ()
            self.mot_sang_19 = ()
            self.hai_sang_19 = ()
            self.ba_sang_19 = ()
            self.bon_sang_19 = ()
            self.thuctap_sang_19 = ''
            self.daotao_sang_19 = ''
            self.chidao_sang_19 = ()

            self.noidung_chieu_19 = ()
            self.mot_chieu_19 = ()
            self.hai_chieu_19 = ()
            self.ba_chieu_19 = ()
            self.bon_chieu_19 = ()
            self.thuctap_chieu_19 = ''
            self.daotao_chieu_19 = ''
            self.chidao_chieu_19 = ()

    ma_sang_19 = fields.Char(related='ma_congviec')
    giaidoan_sang_19 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_19 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_19 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_19(self):
    #     giaidoanmot_sang_19 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_19 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_19 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_19 = giaidoanmot_sang_19.id
    #     self.giaidoan_211_sang_19 = giaidoanhai_sang_19.id
    #     self.giaidoan_221_sang_19 = giaidoanba_sang_19.id
    #     self.giaidoan_chieu_19 = giaidoanmot_sang_19.id
    #     self.giaidoan_211_chieu_19 = giaidoanhai_sang_19.id
    #     self.giaidoan_221_chieu_19 = giaidoanba_sang_19.id

    noidung_sang_19 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_19 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_19 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_19 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_19 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_19 = fields.Char()

    @api.onchange('mot_sang_19', 'hai_sang_19', 'ba_sang_19', 'bon_sang_19')
    def onchange_method_thuctap_sang_19(self):
        if self.mot_sang_19:
            self.thuctap_sang_19 = self.mot_sang_19.ten_noidung
            if self.hai_sang_19:
                self.thuctap_sang_19 = self.hai_sang_19.ten_noidung
                if self.ba_sang_19:
                    self.thuctap_sang_19 = self.ba_sang_19.ten_noidung
                    if self.bon_sang_19:
                        self.thuctap_sang_19 = self.bon_sang_19.ten_noidung

        elif self.ngay_nghi_19:
            self.thuctap_sang_19 = '休日'
        else:
            self.thuctap_sang_19 = ''

    daotao_sang_19 = fields.Char()
    chidao_sang_19 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_19 = fields.Char(related='ma_congviec')
    giaidoan_chieu_19 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_19 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_19 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_19 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_19 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_19 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_19 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_19 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_19 = fields.Char()

    @api.onchange('mot_chieu_19', 'hai_chieu_19', 'ba_chieu_19', 'bon_chieu_19')
    def onchange_method_thuctap_chieu_19(self):
        if self.mot_chieu_19:
            self.thuctap_chieu_19 = self.mot_chieu_19.ten_noidung
            if self.hai_chieu_19:
                self.thuctap_chieu_19 = self.hai_chieu_19.ten_noidung
                if self.ba_sang_19:
                    self.ba_chieu_19 = self.ba_chieu_19.ten_noidung
                    if self.bon_chieu_19:
                        self.thuctap_chieu_19 = self.bon_chieu_19.ten_noidung

        elif self.ngay_nghi_19:
            self.thuctap_chieu_19 = '休日'
        else:
            self.thuctap_chieu_19 = ''

    daotao_chieu_19 = fields.Char()
    chidao_chieu_19 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _20
    ngay_thu_20 = fields.Char()
    ngay_nghi_20 = fields.Boolean()

    @api.onchange('ngay_nghi_20')
    def onchange_method_ngay_nghi_20(self):
        if self.ngay_nghi_20:
            self.noidung_sang_20 = ()
            self.mot_sang_20 = ()
            self.hai_sang_20 = ()
            self.ba_sang_20 = ()
            self.bon_sang_20 = ()
            self.thuctap_sang_20 = '休日'
            self.daotao_sang_20 = ''
            self.chidao_sang_20 = ()

            self.noidung_chieu_20 = ()
            self.mot_chieu_20 = ()
            self.hai_chieu_20 = ()
            self.ba_chieu_20 = ()
            self.bon_chieu_20 = ()
            self.thuctap_chieu_20 = '休日'
            self.daotao_chieu_20 = ''
            self.chidao_chieu_20 = ()
        else:
            self.noidung_sang_20 = ()
            self.mot_sang_20 = ()
            self.hai_sang_20 = ()
            self.ba_sang_20 = ()
            self.bon_sang_20 = ()
            self.thuctap_sang_20 = ''
            self.daotao_sang_20 = ''
            self.chidao_sang_20 = ()

            self.noidung_chieu_20 = ()
            self.mot_chieu_20 = ()
            self.hai_chieu_20 = ()
            self.ba_chieu_20 = ()
            self.bon_chieu_20 = ()
            self.thuctap_chieu_20 = ''
            self.daotao_chieu_20 = ''
            self.chidao_chieu_20 = ()

    ma_sang_20 = fields.Char(related='ma_congviec')
    giaidoan_sang_20 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_20 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_20 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_20(self):
    #     giaidoanmot_sang_20 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_20 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_20 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_20 = giaidoanmot_sang_20.id
    #     self.giaidoan_211_sang_20 = giaidoanhai_sang_20.id
    #     self.giaidoan_221_sang_20 = giaidoanba_sang_20.id
    #     self.giaidoan_chieu_20 = giaidoanmot_sang_20.id
    #     self.giaidoan_211_chieu_20 = giaidoanhai_sang_20.id
    #     self.giaidoan_221_chieu_20 = giaidoanba_sang_20.id

    noidung_sang_20 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_20 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_20 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_20 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_20 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_20 = fields.Char()

    @api.onchange('mot_sang_20', 'hai_sang_20', 'ba_sang_20', 'bon_sang_20')
    def onchange_method_thuctap_sang_20(self):
        if self.mot_sang_20:
            self.thuctap_sang_20 = self.mot_sang_20.ten_noidung
            if self.hai_sang_20:
                self.thuctap_sang_20 = self.hai_sang_20.ten_noidung
                if self.ba_sang_20:
                    self.thuctap_sang_20 = self.ba_sang_20.ten_noidung
                    if self.bon_sang_20:
                        self.thuctap_sang_20 = self.bon_sang_20.ten_noidung

        elif self.ngay_nghi_20:
            self.thuctap_sang_20 = '休日'
        else:
            self.thuctap_sang_20 = ''

    daotao_sang_20 = fields.Char()
    chidao_sang_20 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_20 = fields.Char(related='ma_congviec')
    giaidoan_chieu_20 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_20 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_20 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_20 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_20 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_20 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_20 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_20 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_20 = fields.Char()

    @api.onchange('mot_chieu_20', 'hai_chieu_20', 'ba_chieu_20', 'bon_chieu_20')
    def onchange_method_thuctap_chieu_20(self):
        if self.mot_chieu_20:
            self.thuctap_chieu_20 = self.mot_chieu_20.ten_noidung
            if self.hai_chieu_20:
                self.thuctap_chieu_20 = self.hai_chieu_20.ten_noidung
                if self.ba_sang_20:
                    self.ba_chieu_20 = self.ba_chieu_20.ten_noidung
                    if self.bon_chieu_20:
                        self.thuctap_chieu_20 = self.bon_chieu_20.ten_noidung

        elif self.ngay_nghi_20:
            self.thuctap_chieu_20 = '休日'
        else:
            self.thuctap_chieu_20 = ''

    daotao_chieu_20 = fields.Char()
    chidao_chieu_20 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _21
    ngay_thu_21 = fields.Char()
    ngay_nghi_21 = fields.Boolean()

    @api.onchange('ngay_nghi_21')
    def onchange_method_ngay_nghi_21(self):
        if self.ngay_nghi_21:
            self.noidung_sang_21 = ()
            self.mot_sang_21 = ()
            self.hai_sang_21 = ()
            self.ba_sang_21 = ()
            self.bon_sang_21 = ()
            self.thuctap_sang_21 = '休日'
            self.daotao_sang_21 = ''
            self.chidao_sang_21 = ()

            self.noidung_chieu_21 = ()
            self.mot_chieu_21 = ()
            self.hai_chieu_21 = ()
            self.ba_chieu_21 = ()
            self.bon_chieu_21 = ()
            self.thuctap_chieu_21 = '休日'
            self.daotao_chieu_21 = ''
            self.chidao_chieu_21 = ()
        else:
            self.noidung_sang_21 = ()
            self.mot_sang_21 = ()
            self.hai_sang_21 = ()
            self.ba_sang_21 = ()
            self.bon_sang_21 = ()
            self.thuctap_sang_21 = ''
            self.daotao_sang_21 = ''
            self.chidao_sang_21 = ()

            self.noidung_chieu_21 = ()
            self.mot_chieu_21 = ()
            self.hai_chieu_21 = ()
            self.ba_chieu_21 = ()
            self.bon_chieu_21 = ()
            self.thuctap_chieu_21 = ''
            self.daotao_chieu_21 = ''
            self.chidao_chieu_21 = ()

    ma_sang_21 = fields.Char(related='ma_congviec')
    giaidoan_sang_21 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_21 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_21 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_21(self):
    #     giaidoanmot_sang_21 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_21 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_21 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_21 = giaidoanmot_sang_21.id
    #     self.giaidoan_211_sang_21 = giaidoanhai_sang_21.id
    #     self.giaidoan_221_sang_21 = giaidoanba_sang_21.id
    #     self.giaidoan_chieu_21 = giaidoanmot_sang_21.id
    #     self.giaidoan_211_chieu_21 = giaidoanhai_sang_21.id
    #     self.giaidoan_221_chieu_21 = giaidoanba_sang_21.id

    noidung_sang_21 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_21 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_21 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_21 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_21 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_21 = fields.Char()

    @api.onchange('mot_sang_21', 'hai_sang_21', 'ba_sang_21', 'bon_sang_21')
    def onchange_method_thuctap_sang_21(self):
        if self.mot_sang_21:
            self.thuctap_sang_21 = self.mot_sang_21.ten_noidung
            if self.hai_sang_21:
                self.thuctap_sang_21 = self.hai_sang_21.ten_noidung
                if self.ba_sang_21:
                    self.thuctap_sang_21 = self.ba_sang_21.ten_noidung
                    if self.bon_sang_21:
                        self.thuctap_sang_21 = self.bon_sang_21.ten_noidung

        elif self.ngay_nghi_21:
            self.thuctap_sang_21 = '休日'
        else:
            self.thuctap_sang_21 = ''

    daotao_sang_21 = fields.Char()
    chidao_sang_21 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_21 = fields.Char(related='ma_congviec')
    giaidoan_chieu_21 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_21 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_21 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_21 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_21 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_21 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_21 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_21 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_21 = fields.Char()

    @api.onchange('mot_chieu_21', 'hai_chieu_21', 'ba_chieu_21', 'bon_chieu_21')
    def onchange_method_thuctap_chieu_21(self):
        if self.mot_chieu_21:
            self.thuctap_chieu_21 = self.mot_chieu_21.ten_noidung
            if self.hai_chieu_21:
                self.thuctap_chieu_21 = self.hai_chieu_21.ten_noidung
                if self.ba_sang_21:
                    self.ba_chieu_21 = self.ba_chieu_21.ten_noidung
                    if self.bon_chieu_21:
                        self.thuctap_chieu_21 = self.bon_chieu_21.ten_noidung

        elif self.ngay_nghi_21:
            self.thuctap_chieu_21 = '休日'
        else:
            self.thuctap_chieu_21 = ''

    daotao_chieu_21 = fields.Char()
    chidao_chieu_21 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _22
    ngay_thu_22 = fields.Char()
    ngay_nghi_22 = fields.Boolean()

    @api.onchange('ngay_nghi_22')
    def onchange_method_ngay_nghi_22(self):
        if self.ngay_nghi_22:
            self.noidung_sang_22 = ()
            self.mot_sang_22 = ()
            self.hai_sang_22 = ()
            self.ba_sang_22 = ()
            self.bon_sang_22 = ()
            self.thuctap_sang_22 = '休日'
            self.daotao_sang_22 = ''
            self.chidao_sang_22 = ()

            self.noidung_chieu_22 = ()
            self.mot_chieu_22 = ()
            self.hai_chieu_22 = ()
            self.ba_chieu_22 = ()
            self.bon_chieu_22 = ()
            self.thuctap_chieu_22 = '休日'
            self.daotao_chieu_22 = ''
            self.chidao_chieu_22 = ()
        else:
            self.noidung_sang_22 = ()
            self.mot_sang_22 = ()
            self.hai_sang_22 = ()
            self.ba_sang_22 = ()
            self.bon_sang_22 = ()
            self.thuctap_sang_22 = ''
            self.daotao_sang_22 = ''
            self.chidao_sang_22 = ()

            self.noidung_chieu_22 = ()
            self.mot_chieu_22 = ()
            self.hai_chieu_22 = ()
            self.ba_chieu_22 = ()
            self.bon_chieu_22 = ()
            self.thuctap_chieu_22 = ''
            self.daotao_chieu_22 = ''
            self.chidao_chieu_22 = ()

    ma_sang_22 = fields.Char(related='ma_congviec')
    giaidoan_sang_22 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_22 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_22 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_22(self):
    #     giaidoanmot_sang_22 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_22 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_22 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_22 = giaidoanmot_sang_22.id
    #     self.giaidoan_211_sang_22 = giaidoanhai_sang_22.id
    #     self.giaidoan_221_sang_22 = giaidoanba_sang_22.id
    #     self.giaidoan_chieu_22 = giaidoanmot_sang_22.id
    #     self.giaidoan_211_chieu_22 = giaidoanhai_sang_22.id
    #     self.giaidoan_221_chieu_22 = giaidoanba_sang_22.id

    noidung_sang_22 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_22 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_22 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_22 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_22 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_22 = fields.Char()

    @api.onchange('mot_sang_22', 'hai_sang_22', 'ba_sang_22', 'bon_sang_22')
    def onchange_method_thuctap_sang_22(self):
        if self.mot_sang_22:
            self.thuctap_sang_22 = self.mot_sang_22.ten_noidung
            if self.hai_sang_22:
                self.thuctap_sang_22 = self.hai_sang_22.ten_noidung
                if self.ba_sang_22:
                    self.thuctap_sang_22 = self.ba_sang_22.ten_noidung
                    if self.bon_sang_22:
                        self.thuctap_sang_22 = self.bon_sang_22.ten_noidung

        elif self.ngay_nghi_22:
            self.thuctap_sang_22 = '休日'
        else:
            self.thuctap_sang_22 = ''

    daotao_sang_22 = fields.Char()
    chidao_sang_22 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_22 = fields.Char(related='ma_congviec')
    giaidoan_chieu_22 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_22 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_22 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_22 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_22 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_22 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_22 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_22 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_22 = fields.Char()

    @api.onchange('mot_chieu_22', 'hai_chieu_22', 'ba_chieu_22', 'bon_chieu_22')
    def onchange_method_thuctap_chieu_22(self):
        if self.mot_chieu_22:
            self.thuctap_chieu_22 = self.mot_chieu_22.ten_noidung
            if self.hai_chieu_22:
                self.thuctap_chieu_22 = self.hai_chieu_22.ten_noidung
                if self.ba_sang_22:
                    self.ba_chieu_22 = self.ba_chieu_22.ten_noidung
                    if self.bon_chieu_22:
                        self.thuctap_chieu_22 = self.bon_chieu_22.ten_noidung

        elif self.ngay_nghi_22:
            self.thuctap_chieu_22 = '休日'
        else:
            self.thuctap_chieu_22 = ''

    daotao_chieu_22 = fields.Char()
    chidao_chieu_22 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _23
    ngay_thu_23 = fields.Char()
    ngay_nghi_23 = fields.Boolean()

    @api.onchange('ngay_nghi_23')
    def onchange_method_ngay_nghi_23(self):
        if self.ngay_nghi_23:
            self.noidung_sang_23 = ()
            self.mot_sang_23 = ()
            self.hai_sang_23 = ()
            self.ba_sang_23 = ()
            self.bon_sang_23 = ()
            self.thuctap_sang_23 = '休日'
            self.daotao_sang_23 = ''
            self.chidao_sang_23 = ()

            self.noidung_chieu_23 = ()
            self.mot_chieu_23 = ()
            self.hai_chieu_23 = ()
            self.ba_chieu_23 = ()
            self.bon_chieu_23 = ()
            self.thuctap_chieu_23 = '休日'
            self.daotao_chieu_23 = ''
            self.chidao_chieu_23 = ()
        else:
            self.noidung_sang_23 = ()
            self.mot_sang_23 = ()
            self.hai_sang_23 = ()
            self.ba_sang_23 = ()
            self.bon_sang_23 = ()
            self.thuctap_sang_23 = ''
            self.daotao_sang_23 = ''
            self.chidao_sang_23 = ()

            self.noidung_chieu_23 = ()
            self.mot_chieu_23 = ()
            self.hai_chieu_23 = ()
            self.ba_chieu_23 = ()
            self.bon_chieu_23 = ()
            self.thuctap_chieu_23 = ''
            self.daotao_chieu_23 = ''
            self.chidao_chieu_23 = ()

    ma_sang_23 = fields.Char(related='ma_congviec')
    giaidoan_sang_23 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_23 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_23 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_23(self):
    #     giaidoanmot_sang_23 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_23 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_23 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_23 = giaidoanmot_sang_23.id
    #     self.giaidoan_211_sang_23 = giaidoanhai_sang_23.id
    #     self.giaidoan_221_sang_23 = giaidoanba_sang_23.id
    #     self.giaidoan_chieu_23 = giaidoanmot_sang_23.id
    #     self.giaidoan_211_chieu_23 = giaidoanhai_sang_23.id
    #     self.giaidoan_221_chieu_23 = giaidoanba_sang_23.id

    noidung_sang_23 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_23 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_23 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_23 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_23 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_23 = fields.Char()

    @api.onchange('mot_sang_23', 'hai_sang_23', 'ba_sang_23', 'bon_sang_23')
    def onchange_method_thuctap_sang_23(self):
        if self.mot_sang_23:
            self.thuctap_sang_23 = self.mot_sang_23.ten_noidung
            if self.hai_sang_23:
                self.thuctap_sang_23 = self.hai_sang_23.ten_noidung
                if self.ba_sang_23:
                    self.thuctap_sang_23 = self.ba_sang_23.ten_noidung
                    if self.bon_sang_23:
                        self.thuctap_sang_23 = self.bon_sang_23.ten_noidung

        elif self.ngay_nghi_23:
            self.thuctap_sang_23 = '休日'
        else:
            self.thuctap_sang_23 = ''

    daotao_sang_23 = fields.Char()
    chidao_sang_23 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_23 = fields.Char(related='ma_congviec')
    giaidoan_chieu_23 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_23 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_23 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_23 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_23 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_23 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_23 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_23 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_23 = fields.Char()

    @api.onchange('mot_chieu_23', 'hai_chieu_23', 'ba_chieu_23', 'bon_chieu_23')
    def onchange_method_thuctap_chieu_23(self):
        if self.mot_chieu_23:
            self.thuctap_chieu_23 = self.mot_chieu_23.ten_noidung
            if self.hai_chieu_23:
                self.thuctap_chieu_23 = self.hai_chieu_23.ten_noidung
                if self.ba_sang_23:
                    self.ba_chieu_23 = self.ba_chieu_23.ten_noidung
                    if self.bon_chieu_23:
                        self.thuctap_chieu_23 = self.bon_chieu_23.ten_noidung

        elif self.ngay_nghi_23:
            self.thuctap_chieu_23 = '休日'
        else:
            self.thuctap_chieu_23 = ''

    daotao_chieu_23 = fields.Char()
    chidao_chieu_23 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _24
    ngay_thu_24 = fields.Char()
    ngay_nghi_24 = fields.Boolean()

    @api.onchange('ngay_nghi_24')
    def onchange_method_ngay_nghi_24(self):
        if self.ngay_nghi_24:
            self.noidung_sang_24 = ()
            self.mot_sang_24 = ()
            self.hai_sang_24 = ()
            self.ba_sang_24 = ()
            self.bon_sang_24 = ()
            self.thuctap_sang_24 = '休日'
            self.daotao_sang_24 = ''
            self.chidao_sang_24 = ()

            self.noidung_chieu_24 = ()
            self.mot_chieu_24 = ()
            self.hai_chieu_24 = ()
            self.ba_chieu_24 = ()
            self.bon_chieu_24 = ()
            self.thuctap_chieu_24 = '休日'
            self.daotao_chieu_24 = ''
            self.chidao_chieu_24 = ()
        else:
            self.noidung_sang_24 = ()
            self.mot_sang_24 = ()
            self.hai_sang_24 = ()
            self.ba_sang_24 = ()
            self.bon_sang_24 = ()
            self.thuctap_sang_24 = ''
            self.daotao_sang_24 = ''
            self.chidao_sang_24 = ()

            self.noidung_chieu_24 = ()
            self.mot_chieu_24 = ()
            self.hai_chieu_24 = ()
            self.ba_chieu_24 = ()
            self.bon_chieu_24 = ()
            self.thuctap_chieu_24 = ''
            self.daotao_chieu_24 = ''
            self.chidao_chieu_24 = ()

    ma_sang_24 = fields.Char(related='ma_congviec')
    giaidoan_sang_24 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_24 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_24 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_24(self):
    #     giaidoanmot_sang_24 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_24 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_24 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_24 = giaidoanmot_sang_24.id
    #     self.giaidoan_211_sang_24 = giaidoanhai_sang_24.id
    #     self.giaidoan_221_sang_24 = giaidoanba_sang_24.id
    #     self.giaidoan_chieu_24 = giaidoanmot_sang_24.id
    #     self.giaidoan_211_chieu_24 = giaidoanhai_sang_24.id
    #     self.giaidoan_221_chieu_24 = giaidoanba_sang_24.id

    noidung_sang_24 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_24 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_24 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_24 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_24 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_24 = fields.Char()

    @api.onchange('mot_sang_24', 'hai_sang_24', 'ba_sang_24', 'bon_sang_24')
    def onchange_method_thuctap_sang_24(self):
        if self.mot_sang_24:
            self.thuctap_sang_24 = self.mot_sang_24.ten_noidung
            if self.hai_sang_24:
                self.thuctap_sang_24 = self.hai_sang_24.ten_noidung
                if self.ba_sang_24:
                    self.thuctap_sang_24 = self.ba_sang_24.ten_noidung
                    if self.bon_sang_24:
                        self.thuctap_sang_24 = self.bon_sang_24.ten_noidung

        elif self.ngay_nghi_24:
            self.thuctap_sang_24 = '休日'
        else:
            self.thuctap_sang_24 = ''

    daotao_sang_24 = fields.Char()
    chidao_sang_24 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_24 = fields.Char(related='ma_congviec')
    giaidoan_chieu_24 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_24 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_24 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_24 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_24 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_24 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_24 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_24 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_24 = fields.Char()

    @api.onchange('mot_chieu_24', 'hai_chieu_24', 'ba_chieu_24', 'bon_chieu_24')
    def onchange_method_thuctap_chieu_24(self):
        if self.mot_chieu_24:
            self.thuctap_chieu_24 = self.mot_chieu_24.ten_noidung
            if self.hai_chieu_24:
                self.thuctap_chieu_24 = self.hai_chieu_24.ten_noidung
                if self.ba_sang_24:
                    self.ba_chieu_24 = self.ba_chieu_24.ten_noidung
                    if self.bon_chieu_24:
                        self.thuctap_chieu_24 = self.bon_chieu_24.ten_noidung

        elif self.ngay_nghi_24:
            self.thuctap_chieu_24 = '休日'
        else:
            self.thuctap_chieu_24 = ''

    daotao_chieu_24 = fields.Char()
    chidao_chieu_24 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _25
    ngay_thu_25 = fields.Char()
    ngay_nghi_25 = fields.Boolean()

    @api.onchange('ngay_nghi_25')
    def onchange_method_ngay_nghi_25(self):
        if self.ngay_nghi_25:
            self.noidung_sang_25 = ()
            self.mot_sang_25 = ()
            self.hai_sang_25 = ()
            self.ba_sang_25 = ()
            self.bon_sang_25 = ()
            self.thuctap_sang_25 = '休日'
            self.daotao_sang_25 = ''
            self.chidao_sang_25 = ()

            self.noidung_chieu_25 = ()
            self.mot_chieu_25 = ()
            self.hai_chieu_25 = ()
            self.ba_chieu_25 = ()
            self.bon_chieu_25 = ()
            self.thuctap_chieu_25 = '休日'
            self.daotao_chieu_25 = ''
            self.chidao_chieu_25 = ()
        else:
            self.noidung_sang_25 = ()
            self.mot_sang_25 = ()
            self.hai_sang_25 = ()
            self.ba_sang_25 = ()
            self.bon_sang_25 = ()
            self.thuctap_sang_25 = ''
            self.daotao_sang_25 = ''
            self.chidao_sang_25 = ()

            self.noidung_chieu_25 = ()
            self.mot_chieu_25 = ()
            self.hai_chieu_25 = ()
            self.ba_chieu_25 = ()
            self.bon_chieu_25 = ()
            self.thuctap_chieu_25 = ''
            self.daotao_chieu_25 = ''
            self.chidao_chieu_25 = ()

    ma_sang_25 = fields.Char(related='ma_congviec')
    giaidoan_sang_25 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_25 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_25 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_25(self):
    #     giaidoanmot_sang_25 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_25 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_25 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_25 = giaidoanmot_sang_25.id
    #     self.giaidoan_211_sang_25 = giaidoanhai_sang_25.id
    #     self.giaidoan_221_sang_25 = giaidoanba_sang_25.id
    #     self.giaidoan_chieu_25 = giaidoanmot_sang_25.id
    #     self.giaidoan_211_chieu_25 = giaidoanhai_sang_25.id
    #     self.giaidoan_221_chieu_25 = giaidoanba_sang_25.id

    noidung_sang_25 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_25 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_25 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_25 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_25 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_25 = fields.Char()

    @api.onchange('mot_sang_25', 'hai_sang_25', 'ba_sang_25', 'bon_sang_25')
    def onchange_method_thuctap_sang_25(self):
        if self.mot_sang_25:
            self.thuctap_sang_25 = self.mot_sang_25.ten_noidung
            if self.hai_sang_25:
                self.thuctap_sang_25 = self.hai_sang_25.ten_noidung
                if self.ba_sang_25:
                    self.thuctap_sang_25 = self.ba_sang_25.ten_noidung
                    if self.bon_sang_25:
                        self.thuctap_sang_25 = self.bon_sang_25.ten_noidung

        elif self.ngay_nghi_25:
            self.thuctap_sang_25 = '休日'
        else:
            self.thuctap_sang_25 = ''

    daotao_sang_25 = fields.Char()
    chidao_sang_25 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_25 = fields.Char(related='ma_congviec')
    giaidoan_chieu_25 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_25 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_25 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_25 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_25 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_25 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_25 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_25 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_25 = fields.Char()

    @api.onchange('mot_chieu_25', 'hai_chieu_25', 'ba_chieu_25', 'bon_chieu_25')
    def onchange_method_thuctap_chieu_25(self):
        if self.mot_chieu_25:
            self.thuctap_chieu_25 = self.mot_chieu_25.ten_noidung
            if self.hai_chieu_25:
                self.thuctap_chieu_25 = self.hai_chieu_25.ten_noidung
                if self.ba_sang_25:
                    self.ba_chieu_25 = self.ba_chieu_25.ten_noidung
                    if self.bon_chieu_25:
                        self.thuctap_chieu_25 = self.bon_chieu_25.ten_noidung

        elif self.ngay_nghi_25:
            self.thuctap_chieu_25 = '休日'
        else:
            self.thuctap_chieu_25 = ''

    daotao_chieu_25 = fields.Char()
    chidao_chieu_25 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _26
    ngay_thu_26 = fields.Char()
    ngay_nghi_26 = fields.Boolean()

    @api.onchange('ngay_nghi_26')
    def onchange_method_ngay_nghi_26(self):
        if self.ngay_nghi_26:
            self.noidung_sang_26 = ()
            self.mot_sang_26 = ()
            self.hai_sang_26 = ()
            self.ba_sang_26 = ()
            self.bon_sang_26 = ()
            self.thuctap_sang_26 = '休日'
            self.daotao_sang_26 = ''
            self.chidao_sang_26 = ()

            self.noidung_chieu_26 = ()
            self.mot_chieu_26 = ()
            self.hai_chieu_26 = ()
            self.ba_chieu_26 = ()
            self.bon_chieu_26 = ()
            self.thuctap_chieu_26 = '休日'
            self.daotao_chieu_26 = ''
            self.chidao_chieu_26 = ()
        else:
            self.noidung_sang_26 = ()
            self.mot_sang_26 = ()
            self.hai_sang_26 = ()
            self.ba_sang_26 = ()
            self.bon_sang_26 = ()
            self.thuctap_sang_26 = ''
            self.daotao_sang_26 = ''
            self.chidao_sang_26 = ()

            self.noidung_chieu_26 = ()
            self.mot_chieu_26 = ()
            self.hai_chieu_26 = ()
            self.ba_chieu_26 = ()
            self.bon_chieu_26 = ()
            self.thuctap_chieu_26 = ''
            self.daotao_chieu_26 = ''
            self.chidao_chieu_26 = ()

    ma_sang_26 = fields.Char(related='ma_congviec')
    giaidoan_sang_26 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_26 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_26 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_26(self):
    #     giaidoanmot_sang_26 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_26 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_26 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_26 = giaidoanmot_sang_26.id
    #     self.giaidoan_211_sang_26 = giaidoanhai_sang_26.id
    #     self.giaidoan_221_sang_26 = giaidoanba_sang_26.id
    #     self.giaidoan_chieu_26 = giaidoanmot_sang_26.id
    #     self.giaidoan_211_chieu_26 = giaidoanhai_sang_26.id
    #     self.giaidoan_221_chieu_26 = giaidoanba_sang_26.id

    noidung_sang_26 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_26 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_26 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_26 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_26 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_26 = fields.Char()

    @api.onchange('mot_sang_26', 'hai_sang_26', 'ba_sang_26', 'bon_sang_26')
    def onchange_method_thuctap_sang_26(self):
        if self.mot_sang_26:
            self.thuctap_sang_26 = self.mot_sang_26.ten_noidung
            if self.hai_sang_26:
                self.thuctap_sang_26 = self.hai_sang_26.ten_noidung
                if self.ba_sang_26:
                    self.thuctap_sang_26 = self.ba_sang_26.ten_noidung
                    if self.bon_sang_26:
                        self.thuctap_sang_26 = self.bon_sang_26.ten_noidung

        elif self.ngay_nghi_26:
            self.thuctap_sang_26 = '休日'
        else:
            self.thuctap_sang_26 = ''

    daotao_sang_26 = fields.Char()
    chidao_sang_26 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_26 = fields.Char(related='ma_congviec')
    giaidoan_chieu_26 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_26 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_26 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_26 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_26 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_26 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_26 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_26 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_26 = fields.Char()

    @api.onchange('mot_chieu_26', 'hai_chieu_26', 'ba_chieu_26', 'bon_chieu_26')
    def onchange_method_thuctap_chieu_26(self):
        if self.mot_chieu_26:
            self.thuctap_chieu_26 = self.mot_chieu_26.ten_noidung
            if self.hai_chieu_26:
                self.thuctap_chieu_26 = self.hai_chieu_26.ten_noidung
                if self.ba_sang_26:
                    self.ba_chieu_26 = self.ba_chieu_26.ten_noidung
                    if self.bon_chieu_26:
                        self.thuctap_chieu_26 = self.bon_chieu_26.ten_noidung

        elif self.ngay_nghi_26:
            self.thuctap_chieu_26 = '休日'
        else:
            self.thuctap_chieu_26 = ''

    daotao_chieu_26 = fields.Char()
    chidao_chieu_26 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _27
    ngay_thu_27 = fields.Char()
    ngay_nghi_27 = fields.Boolean()

    @api.onchange('ngay_nghi_27')
    def onchange_method_ngay_nghi_27(self):
        if self.ngay_nghi_27:
            self.noidung_sang_27 = ()
            self.mot_sang_27 = ()
            self.hai_sang_27 = ()
            self.ba_sang_27 = ()
            self.bon_sang_27 = ()
            self.thuctap_sang_27 = '休日'
            self.daotao_sang_27 = ''
            self.chidao_sang_27 = ()

            self.noidung_chieu_27 = ()
            self.mot_chieu_27 = ()
            self.hai_chieu_27 = ()
            self.ba_chieu_27 = ()
            self.bon_chieu_27 = ()
            self.thuctap_chieu_27 = '休日'
            self.daotao_chieu_27 = ''
            self.chidao_chieu_27 = ()
        else:
            self.noidung_sang_27 = ()
            self.mot_sang_27 = ()
            self.hai_sang_27 = ()
            self.ba_sang_27 = ()
            self.bon_sang_27 = ()
            self.thuctap_sang_27 = ''
            self.daotao_sang_27 = ''
            self.chidao_sang_27 = ()

            self.noidung_chieu_27 = ()
            self.mot_chieu_27 = ()
            self.hai_chieu_27 = ()
            self.ba_chieu_27 = ()
            self.bon_chieu_27 = ()
            self.thuctap_chieu_27 = ''
            self.daotao_chieu_27 = ''
            self.chidao_chieu_27 = ()

    ma_sang_27 = fields.Char(related='ma_congviec')
    giaidoan_sang_27 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_27 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_27 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_27(self):
    #     giaidoanmot_sang_27 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_27 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_27 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_27 = giaidoanmot_sang_27.id
    #     self.giaidoan_211_sang_27 = giaidoanhai_sang_27.id
    #     self.giaidoan_221_sang_27 = giaidoanba_sang_27.id
    #     self.giaidoan_chieu_27 = giaidoanmot_sang_27.id
    #     self.giaidoan_211_chieu_27 = giaidoanhai_sang_27.id
    #     self.giaidoan_221_chieu_27 = giaidoanba_sang_27.id

    noidung_sang_27 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_27 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_27 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_27 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_27 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_27 = fields.Char()

    @api.onchange('mot_sang_27', 'hai_sang_27', 'ba_sang_27', 'bon_sang_27')
    def onchange_method_thuctap_sang_27(self):
        if self.mot_sang_27:
            self.thuctap_sang_27 = self.mot_sang_27.ten_noidung
            if self.hai_sang_27:
                self.thuctap_sang_27 = self.hai_sang_27.ten_noidung
                if self.ba_sang_27:
                    self.thuctap_sang_27 = self.ba_sang_27.ten_noidung
                    if self.bon_sang_27:
                        self.thuctap_sang_27 = self.bon_sang_27.ten_noidung

        elif self.ngay_nghi_27:
            self.thuctap_sang_27 = '休日'
        else:
            self.thuctap_sang_27 = ''

    daotao_sang_27 = fields.Char()
    chidao_sang_27 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_27 = fields.Char(related='ma_congviec')
    giaidoan_chieu_27 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_27 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_27 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_27 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_27 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_27 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_27 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_27 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_27 = fields.Char()

    @api.onchange('mot_chieu_27', 'hai_chieu_27', 'ba_chieu_27', 'bon_chieu_27')
    def onchange_method_thuctap_chieu_27(self):
        if self.mot_chieu_27:
            self.thuctap_chieu_27 = self.mot_chieu_27.ten_noidung
            if self.hai_chieu_27:
                self.thuctap_chieu_27 = self.hai_chieu_27.ten_noidung
                if self.ba_sang_27:
                    self.ba_chieu_27 = self.ba_chieu_27.ten_noidung
                    if self.bon_chieu_27:
                        self.thuctap_chieu_27 = self.bon_chieu_27.ten_noidung

        elif self.ngay_nghi_27:
            self.thuctap_chieu_27 = '休日'
        else:
            self.thuctap_chieu_27 = ''

    daotao_chieu_27 = fields.Char()
    chidao_chieu_27 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _28
    ngay_thu_28 = fields.Char()
    ngay_nghi_28 = fields.Boolean()

    @api.onchange('ngay_nghi_28')
    def onchange_method_ngay_nghi_28(self):
        if self.ngay_nghi_28:
            self.noidung_sang_28 = ()
            self.mot_sang_28 = ()
            self.hai_sang_28 = ()
            self.ba_sang_28 = ()
            self.bon_sang_28 = ()
            self.thuctap_sang_28 = '休日'
            self.daotao_sang_28 = ''
            self.chidao_sang_28 = ()

            self.noidung_chieu_28 = ()
            self.mot_chieu_28 = ()
            self.hai_chieu_28 = ()
            self.ba_chieu_28 = ()
            self.bon_chieu_28 = ()
            self.thuctap_chieu_28 = '休日'
            self.daotao_chieu_28 = ''
            self.chidao_chieu_28 = ()
        else:
            self.noidung_sang_28 = ()
            self.mot_sang_28 = ()
            self.hai_sang_28 = ()
            self.ba_sang_28 = ()
            self.bon_sang_28 = ()
            self.thuctap_sang_28 = ''
            self.daotao_sang_28 = ''
            self.chidao_sang_28 = ()

            self.noidung_chieu_28 = ()
            self.mot_chieu_28 = ()
            self.hai_chieu_28 = ()
            self.ba_chieu_28 = ()
            self.bon_chieu_28 = ()
            self.thuctap_chieu_28 = ''
            self.daotao_chieu_28 = ''
            self.chidao_chieu_28 = ()

    ma_sang_28 = fields.Char(related='ma_congviec')
    giaidoan_sang_28 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_28 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_28 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_28(self):
    #     giaidoanmot_sang_28 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_28 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_28 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_28 = giaidoanmot_sang_28.id
    #     self.giaidoan_211_sang_28 = giaidoanhai_sang_28.id
    #     self.giaidoan_221_sang_28 = giaidoanba_sang_28.id
    #     self.giaidoan_chieu_28 = giaidoanmot_sang_28.id
    #     self.giaidoan_211_chieu_28 = giaidoanhai_sang_28.id
    #     self.giaidoan_221_chieu_28 = giaidoanba_sang_28.id

    noidung_sang_28 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_28 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_28 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_28 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_28 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_28 = fields.Char()

    @api.onchange('mot_sang_28', 'hai_sang_28', 'ba_sang_28', 'bon_sang_28')
    def onchange_method_thuctap_sang_28(self):
        if self.mot_sang_28:
            self.thuctap_sang_28 = self.mot_sang_28.ten_noidung
            if self.hai_sang_28:
                self.thuctap_sang_28 = self.hai_sang_28.ten_noidung
                if self.ba_sang_28:
                    self.thuctap_sang_28 = self.ba_sang_28.ten_noidung
                    if self.bon_sang_28:
                        self.thuctap_sang_28 = self.bon_sang_28.ten_noidung

        elif self.ngay_nghi_28:
            self.thuctap_sang_28 = '休日'
        else:
            self.thuctap_sang_28 = ''

    daotao_sang_28 = fields.Char()
    chidao_sang_28 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_28 = fields.Char(related='ma_congviec')
    giaidoan_chieu_28 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_28 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_28 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_28 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_28 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_28 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_28 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_28 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_28 = fields.Char()

    @api.onchange('mot_chieu_28', 'hai_chieu_28', 'ba_chieu_28', 'bon_chieu_28')
    def onchange_method_thuctap_chieu_28(self):
        if self.mot_chieu_28:
            self.thuctap_chieu_28 = self.mot_chieu_28.ten_noidung
            if self.hai_chieu_28:
                self.thuctap_chieu_28 = self.hai_chieu_28.ten_noidung
                if self.ba_sang_28:
                    self.ba_chieu_28 = self.ba_chieu_28.ten_noidung
                    if self.bon_chieu_28:
                        self.thuctap_chieu_28 = self.bon_chieu_28.ten_noidung

        elif self.ngay_nghi_28:
            self.thuctap_chieu_28 = '休日'
        else:
            self.thuctap_chieu_28 = ''

    daotao_chieu_28 = fields.Char()
    chidao_chieu_28 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _29
    ngay_thu_29 = fields.Char()
    ngay_nghi_29 = fields.Boolean()

    @api.onchange('ngay_nghi_29')
    def onchange_method_ngay_nghi_29(self):
        if self.ngay_nghi_29:
            self.noidung_sang_29 = ()
            self.mot_sang_29 = ()
            self.hai_sang_29 = ()
            self.ba_sang_29 = ()
            self.bon_sang_29 = ()
            self.thuctap_sang_29 = '休日'
            self.daotao_sang_29 = ''
            self.chidao_sang_29 = ()

            self.noidung_chieu_29 = ()
            self.mot_chieu_29 = ()
            self.hai_chieu_29 = ()
            self.ba_chieu_29 = ()
            self.bon_chieu_29 = ()
            self.thuctap_chieu_29 = '休日'
            self.daotao_chieu_29 = ''
            self.chidao_chieu_29 = ()
        else:
            self.noidung_sang_29 = ()
            self.mot_sang_29 = ()
            self.hai_sang_29 = ()
            self.ba_sang_29 = ()
            self.bon_sang_29 = ()
            self.thuctap_sang_29 = ''
            self.daotao_sang_29 = ''
            self.chidao_sang_29 = ()

            self.noidung_chieu_29 = ()
            self.mot_chieu_29 = ()
            self.hai_chieu_29 = ()
            self.ba_chieu_29 = ()
            self.bon_chieu_29 = ()
            self.thuctap_chieu_29 = ''
            self.daotao_chieu_29 = ''
            self.chidao_chieu_29 = ()

    ma_sang_29 = fields.Char(related='ma_congviec')
    giaidoan_sang_29 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_29 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_29 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_29(self):
    #     giaidoanmot_sang_29 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_29 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_29 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_29 = giaidoanmot_sang_29.id
    #     self.giaidoan_211_sang_29 = giaidoanhai_sang_29.id
    #     self.giaidoan_221_sang_29 = giaidoanba_sang_29.id
    #     self.giaidoan_chieu_29 = giaidoanmot_sang_29.id
    #     self.giaidoan_211_chieu_29 = giaidoanhai_sang_29.id
    #     self.giaidoan_221_chieu_29 = giaidoanba_sang_29.id

    noidung_sang_29 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_29 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_29 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_29 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_29 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_29 = fields.Char()

    @api.onchange('mot_sang_29', 'hai_sang_29', 'ba_sang_29', 'bon_sang_29')
    def onchange_method_thuctap_sang_29(self):
        if self.mot_sang_29:
            self.thuctap_sang_29 = self.mot_sang_29.ten_noidung
            if self.hai_sang_29:
                self.thuctap_sang_29 = self.hai_sang_29.ten_noidung
                if self.ba_sang_29:
                    self.thuctap_sang_29 = self.ba_sang_29.ten_noidung
                    if self.bon_sang_29:
                        self.thuctap_sang_29 = self.bon_sang_29.ten_noidung

        elif self.ngay_nghi_29:
            self.thuctap_sang_29 = '休日'
        else:
            self.thuctap_sang_29 = ''

    daotao_sang_29 = fields.Char()
    chidao_sang_29 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_29 = fields.Char(related='ma_congviec')
    giaidoan_chieu_29 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_29 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_29 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_29 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_29 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_29 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_29 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_29 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_29 = fields.Char()

    @api.onchange('mot_chieu_29', 'hai_chieu_29', 'ba_chieu_29', 'bon_chieu_29')
    def onchange_method_thuctap_chieu_29(self):
        if self.mot_chieu_29:
            self.thuctap_chieu_29 = self.mot_chieu_29.ten_noidung
            if self.hai_chieu_29:
                self.thuctap_chieu_29 = self.hai_chieu_29.ten_noidung
                if self.ba_sang_29:
                    self.ba_chieu_29 = self.ba_chieu_29.ten_noidung
                    if self.bon_chieu_29:
                        self.thuctap_chieu_29 = self.bon_chieu_29.ten_noidung

        elif self.ngay_nghi_29:
            self.thuctap_chieu_29 = '休日'
        else:
            self.thuctap_chieu_29 = ''

    daotao_chieu_29 = fields.Char()
    chidao_chieu_29 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _30
    ngay_thu_30 = fields.Char()
    ngay_nghi_30 = fields.Boolean()

    @api.onchange('ngay_nghi_30')
    def onchange_method_ngay_nghi_30(self):
        if self.ngay_nghi_30:
            self.noidung_sang_30 = ()
            self.mot_sang_30 = ()
            self.hai_sang_30 = ()
            self.ba_sang_30 = ()
            self.bon_sang_30 = ()
            self.thuctap_sang_30 = '休日'
            self.daotao_sang_30 = ''
            self.chidao_sang_30 = ()

            self.noidung_chieu_30 = ()
            self.mot_chieu_30 = ()
            self.hai_chieu_30 = ()
            self.ba_chieu_30 = ()
            self.bon_chieu_30 = ()
            self.thuctap_chieu_30 = '休日'
            self.daotao_chieu_30 = ''
            self.chidao_chieu_30 = ()
        else:
            self.noidung_sang_30 = ()
            self.mot_sang_30 = ()
            self.hai_sang_30 = ()
            self.ba_sang_30 = ()
            self.bon_sang_30 = ()
            self.thuctap_sang_30 = ''
            self.daotao_sang_30 = ''
            self.chidao_sang_30 = ()

            self.noidung_chieu_30 = ()
            self.mot_chieu_30 = ()
            self.hai_chieu_30 = ()
            self.ba_chieu_30 = ()
            self.bon_chieu_30 = ()
            self.thuctap_chieu_30 = ''
            self.daotao_chieu_30 = ''
            self.chidao_chieu_30 = ()

    ma_sang_30 = fields.Char(related='ma_congviec')
    giaidoan_sang_30 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_30 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_30 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_30(self):
    #     giaidoanmot_sang_30 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_30 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_30 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_30 = giaidoanmot_sang_30.id
    #     self.giaidoan_211_sang_30 = giaidoanhai_sang_30.id
    #     self.giaidoan_221_sang_30 = giaidoanba_sang_30.id
    #     self.giaidoan_chieu_30 = giaidoanmot_sang_30.id
    #     self.giaidoan_211_chieu_30 = giaidoanhai_sang_30.id
    #     self.giaidoan_221_chieu_30 = giaidoanba_sang_30.id

    noidung_sang_30 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_30 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_30 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_30 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_30 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_30 = fields.Char()

    @api.onchange('mot_sang_30', 'hai_sang_30', 'ba_sang_30', 'bon_sang_30')
    def onchange_method_thuctap_sang_30(self):
        if self.mot_sang_30:
            self.thuctap_sang_30 = self.mot_sang_30.ten_noidung
            if self.hai_sang_30:
                self.thuctap_sang_30 = self.hai_sang_30.ten_noidung
                if self.ba_sang_30:
                    self.thuctap_sang_30 = self.ba_sang_30.ten_noidung
                    if self.bon_sang_30:
                        self.thuctap_sang_30 = self.bon_sang_30.ten_noidung

        elif self.ngay_nghi_30:
            self.thuctap_sang_30 = '休日'
        else:
            self.thuctap_sang_30 = ''

    daotao_sang_30 = fields.Char()
    chidao_sang_30 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_30 = fields.Char(related='ma_congviec')
    giaidoan_chieu_30 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_30 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_30 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_30 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_30 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_30 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_30 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_30 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_30 = fields.Char()

    @api.onchange('mot_chieu_30', 'hai_chieu_30', 'ba_chieu_30', 'bon_chieu_30')
    def onchange_method_thuctap_chieu_30(self):
        if self.mot_chieu_30:
            self.thuctap_chieu_30 = self.mot_chieu_30.ten_noidung
            if self.hai_chieu_30:
                self.thuctap_chieu_30 = self.hai_chieu_30.ten_noidung
                if self.ba_sang_30:
                    self.ba_chieu_30 = self.ba_chieu_30.ten_noidung
                    if self.bon_chieu_30:
                        self.thuctap_chieu_30 = self.bon_chieu_30.ten_noidung

        elif self.ngay_nghi_30:
            self.thuctap_chieu_30 = '休日'
        else:
            self.thuctap_chieu_30 = ''

    daotao_chieu_30 = fields.Char()
    chidao_chieu_30 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # ngày _31
    ngay_thu_31 = fields.Char()
    ngay_nghi_31 = fields.Boolean()

    @api.onchange('ngay_nghi_31')
    def onchange_method_ngay_nghi_31(self):
        if self.ngay_nghi_31:
            self.noidung_sang_31 = ()
            self.mot_sang_31 = ()
            self.hai_sang_31 = ()
            self.ba_sang_31 = ()
            self.bon_sang_31 = ()
            self.thuctap_sang_31 = '休日'
            self.daotao_sang_31 = ''
            self.chidao_sang_31 = ()

            self.noidung_chieu_31 = ()
            self.mot_chieu_31 = ()
            self.hai_chieu_31 = ()
            self.ba_chieu_31 = ()
            self.bon_chieu_31 = ()
            self.thuctap_chieu_31 = '休日'
            self.daotao_chieu_31 = ''
            self.chidao_chieu_31 = ()
        else:
            self.noidung_sang_31 = ()
            self.mot_sang_31 = ()
            self.hai_sang_31 = ()
            self.ba_sang_31 = ()
            self.bon_sang_31 = ()
            self.thuctap_sang_31 = ''
            self.daotao_sang_31 = ''
            self.chidao_sang_31 = ()

            self.noidung_chieu_31 = ()
            self.mot_chieu_31 = ()
            self.hai_chieu_31 = ()
            self.ba_chieu_31 = ()
            self.bon_chieu_31 = ()
            self.thuctap_chieu_31 = ''
            self.daotao_chieu_31 = ''
            self.chidao_chieu_31 = ()

    ma_sang_31 = fields.Char(related='ma_congviec')
    giaidoan_sang_31 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_sang_31 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_sang_31 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    # @api.onchange('giaidoan_junkai')
    # def _giaidoan_sang_31(self):
    #     giaidoanmot_sang_31 = self.env['giaidoan.giaidoanmot'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanhai_sang_31 = self.env['giaidoan.giaidoanhai'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                   limit=1)
    #     giaidoanba_sang_31 = self.env['giaidoan.giaidoanba'].search([('giaidoan_noidung', '=', self.giaidoan_junkai)],
    #                                                                 limit=1)
    #
    #     self.giaidoan_sang_31 = giaidoanmot_sang_31.id
    #     self.giaidoan_211_sang_31 = giaidoanhai_sang_31.id
    #     self.giaidoan_221_sang_31 = giaidoanba_sang_31.id
    #     self.giaidoan_chieu_31 = giaidoanmot_sang_31.id
    #     self.giaidoan_211_chieu_31 = giaidoanhai_sang_31.id
    #     self.giaidoan_221_chieu_31 = giaidoanba_sang_31.id

    noidung_sang_31 = fields.Many2one(comodel_name='noidung.noidung')
    mot_sang_31 = fields.Many2one(comodel_name='noidung.noidung')
    hai_sang_31 = fields.Many2one(comodel_name='noidung.noidung')
    ba_sang_31 = fields.Many2one(comodel_name='noidung.noidung')
    bon_sang_31 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_sang_31 = fields.Char()

    @api.onchange('mot_sang_31', 'hai_sang_31', 'ba_sang_31', 'bon_sang_31')
    def onchange_method_thuctap_sang_31(self):
        if self.mot_sang_31:
            self.thuctap_sang_31 = self.mot_sang_31.ten_noidung
            if self.hai_sang_31:
                self.thuctap_sang_31 = self.hai_sang_31.ten_noidung
                if self.ba_sang_31:
                    self.thuctap_sang_31 = self.ba_sang_31.ten_noidung
                    if self.bon_sang_31:
                        self.thuctap_sang_31 = self.bon_sang_31.ten_noidung

        elif self.ngay_nghi_31:
            self.thuctap_sang_31 = '休日'
        else:
            self.thuctap_sang_31 = ''

    daotao_sang_31 = fields.Char()
    chidao_sang_31 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    ma_chieu_31 = fields.Char(related='ma_congviec')
    giaidoan_chieu_31 = fields.Many2one(comodel_name='giaidoan.giaidoanmot', string='Giai đoạn')
    giaidoan_211_chieu_31 = fields.Many2one(comodel_name='giaidoan.giaidoanhai', string='Giai đoạn')
    giaidoan_221_chieu_31 = fields.Many2one(comodel_name='giaidoan.giaidoanba', string='Giai đoạn')

    noidung_chieu_31 = fields.Many2one(comodel_name='noidung.noidung')
    mot_chieu_31 = fields.Many2one(comodel_name='noidung.noidung')
    hai_chieu_31 = fields.Many2one(comodel_name='noidung.noidung')
    ba_chieu_31 = fields.Many2one(comodel_name='noidung.noidung')
    bon_chieu_31 = fields.Many2one(comodel_name='noidung.noidung')
    thuctap_chieu_31 = fields.Char()

    @api.onchange('mot_chieu_31', 'hai_chieu_31', 'ba_chieu_31', 'bon_chieu_31')
    def onchange_method_thuctap_chieu_31(self):
        if self.mot_chieu_31:
            self.thuctap_chieu_31 = self.mot_chieu_31.ten_noidung
            if self.hai_chieu_31:
                self.thuctap_chieu_31 = self.hai_chieu_31.ten_noidung
                if self.ba_sang_31:
                    self.ba_chieu_31 = self.ba_chieu_31.ten_noidung
                    if self.bon_chieu_31:
                        self.thuctap_chieu_31 = self.bon_chieu_31.ten_noidung

        elif self.ngay_nghi_31:
            self.thuctap_chieu_31 = '休日'
        else:
            self.thuctap_chieu_31 = ''

    daotao_chieu_31 = fields.Char()
    chidao_chieu_31 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    # laplich_hoso = fields.One2many(comodel_name='junkai.junkai', inverse_name='hoso')

    @api.onchange('thang_lich_junkai', 'nam_lich_junkai')
    def onchange_method(self):
        # @api.multi
        # def button_taolich(self):
        # Ngay _1
        self.ngay_thu_1 = ''
        self.ngay_nghi_1 = False

        self.noidung_sang_1 = ()
        self.mot_sang_1 = ()
        self.hai_sang_1 = ()
        self.ba_sang_1 = ()
        self.bon_sang_1 = ()
        self.thuctap_sang_1 = ''
        self.daotao_sang_1 = ''
        self.chidao_sang_1 = ()

        self.noidung_chieu_1 = ()
        self.mot_chieu_1 = ()
        self.hai_chieu_1 = ()
        self.ba_chieu_1 = ()
        self.bon_chieu_1 = ()
        self.thuctap_chieu_1 = ''
        self.daotao_chieu_1 = ''
        self.chidao_chieu_1 = ()

        # Ngay _2
        self.ngay_thu_2 = ''
        self.ngay_nghi_2 = False

        self.noidung_sang_2 = ()
        self.mot_sang_2 = ()
        self.hai_sang_2 = ()
        self.ba_sang_2 = ()
        self.bon_sang_2 = ()
        self.thuctap_sang_2 = ''
        self.daotao_sang_2 = ''
        self.chidao_sang_2 = ()

        self.noidung_chieu_2 = ()
        self.mot_chieu_2 = ()
        self.hai_chieu_2 = ()
        self.ba_chieu_2 = ()
        self.bon_chieu_2 = ()
        self.thuctap_chieu_2 = ''
        self.daotao_chieu_2 = ''
        self.chidao_chieu_2 = ()

        # Ngay _3
        self.ngay_thu_3 = ''
        self.ngay_nghi_3 = False

        self.noidung_sang_3 = ()
        self.mot_sang_3 = ()
        self.hai_sang_3 = ()
        self.ba_sang_3 = ()
        self.bon_sang_3 = ()
        self.thuctap_sang_3 = ''
        self.daotao_sang_3 = ''
        self.chidao_sang_3 = ()

        self.noidung_chieu_3 = ()
        self.mot_chieu_3 = ()
        self.hai_chieu_3 = ()
        self.ba_chieu_3 = ()
        self.bon_chieu_3 = ()
        self.thuctap_chieu_3 = ''
        self.daotao_chieu_3 = ''
        self.chidao_chieu_3 = ()

        # Ngay _4
        self.ngay_thu_4 = ''
        self.ngay_nghi_4 = False

        self.noidung_sang_4 = ()
        self.mot_sang_4 = ()
        self.hai_sang_4 = ()
        self.ba_sang_4 = ()
        self.bon_sang_4 = ()
        self.thuctap_sang_4 = ''
        self.daotao_sang_4 = ''
        self.chidao_sang_4 = ()

        self.noidung_chieu_4 = ()
        self.mot_chieu_4 = ()
        self.hai_chieu_4 = ()
        self.ba_chieu_4 = ()
        self.bon_chieu_4 = ()
        self.thuctap_chieu_4 = ''
        self.daotao_chieu_4 = ''
        self.chidao_chieu_4 = ()

        # Ngay _5
        self.ngay_thu_5 = ''
        self.ngay_nghi_5 = False

        self.noidung_sang_5 = ()
        self.mot_sang_5 = ()
        self.hai_sang_5 = ()
        self.ba_sang_5 = ()
        self.bon_sang_5 = ()
        self.thuctap_sang_5 = ''
        self.daotao_sang_5 = ''
        self.chidao_sang_5 = ()

        self.noidung_chieu_5 = ()
        self.mot_chieu_5 = ()
        self.hai_chieu_5 = ()
        self.ba_chieu_5 = ()
        self.bon_chieu_5 = ()
        self.thuctap_chieu_5 = ''
        self.daotao_chieu_5 = ''
        self.chidao_chieu_5 = ()

        # Ngay _6
        self.ngay_thu_6 = ''
        self.ngay_nghi_6 = False

        self.noidung_sang_6 = ()
        self.mot_sang_6 = ()
        self.hai_sang_6 = ()
        self.ba_sang_6 = ()
        self.bon_sang_6 = ()
        self.thuctap_sang_6 = ''
        self.daotao_sang_6 = ''
        self.chidao_sang_6 = ()

        self.noidung_chieu_6 = ()
        self.mot_chieu_6 = ()
        self.hai_chieu_6 = ()
        self.ba_chieu_6 = ()
        self.bon_chieu_6 = ()
        self.thuctap_chieu_6 = ''
        self.daotao_chieu_6 = ''
        self.chidao_chieu_6 = ()

        # Ngay _7
        self.ngay_thu_7 = ''
        self.ngay_nghi_7 = False

        self.noidung_sang_7 = ()
        self.mot_sang_7 = ()
        self.hai_sang_7 = ()
        self.ba_sang_7 = ()
        self.bon_sang_7 = ()
        self.thuctap_sang_7 = ''
        self.daotao_sang_7 = ''
        self.chidao_sang_7 = ()

        self.noidung_chieu_7 = ()
        self.mot_chieu_7 = ()
        self.hai_chieu_7 = ()
        self.ba_chieu_7 = ()
        self.bon_chieu_7 = ()
        self.thuctap_chieu_7 = ''
        self.daotao_chieu_7 = ''
        self.chidao_chieu_7 = ()

        # Ngay _8
        self.ngay_thu_8 = ''
        self.ngay_nghi_8 = False

        self.noidung_sang_8 = ()
        self.mot_sang_8 = ()
        self.hai_sang_8 = ()
        self.ba_sang_8 = ()
        self.bon_sang_8 = ()
        self.thuctap_sang_8 = ''
        self.daotao_sang_8 = ''
        self.chidao_sang_8 = ()

        self.noidung_chieu_8 = ()
        self.mot_chieu_8 = ()
        self.hai_chieu_8 = ()
        self.ba_chieu_8 = ()
        self.bon_chieu_8 = ()
        self.thuctap_chieu_8 = ''
        self.daotao_chieu_8 = ''
        self.chidao_chieu_8 = ()

        # Ngay _9
        self.ngay_thu_9 = ''
        self.ngay_nghi_9 = False

        self.noidung_sang_9 = ()
        self.mot_sang_9 = ()
        self.hai_sang_9 = ()
        self.ba_sang_9 = ()
        self.bon_sang_9 = ()
        self.thuctap_sang_9 = ''
        self.daotao_sang_9 = ''
        self.chidao_sang_9 = ()

        self.noidung_chieu_9 = ()
        self.mot_chieu_9 = ()
        self.hai_chieu_9 = ()
        self.ba_chieu_9 = ()
        self.bon_chieu_9 = ()
        self.thuctap_chieu_9 = ''
        self.daotao_chieu_9 = ''
        self.chidao_chieu_9 = ()

        # Ngay _10
        self.ngay_thu_10 = ''
        self.ngay_nghi_10 = False

        self.noidung_sang_10 = ()
        self.mot_sang_10 = ()
        self.hai_sang_10 = ()
        self.ba_sang_10 = ()
        self.bon_sang_10 = ()
        self.thuctap_sang_10 = ''
        self.daotao_sang_10 = ''
        self.chidao_sang_10 = ()

        self.noidung_chieu_10 = ()
        self.mot_chieu_10 = ()
        self.hai_chieu_10 = ()
        self.ba_chieu_10 = ()
        self.bon_chieu_10 = ()
        self.thuctap_chieu_10 = ''
        self.daotao_chieu_10 = ''
        self.chidao_chieu_10 = ()

        # Ngay _11
        self.ngay_thu_11 = ''
        self.ngay_nghi_11 = False

        self.noidung_sang_11 = ()
        self.mot_sang_11 = ()
        self.hai_sang_11 = ()
        self.ba_sang_11 = ()
        self.bon_sang_11 = ()
        self.thuctap_sang_11 = ''
        self.daotao_sang_11 = ''
        self.chidao_sang_11 = ()

        self.noidung_chieu_11 = ()
        self.mot_chieu_11 = ()
        self.hai_chieu_11 = ()
        self.ba_chieu_11 = ()
        self.bon_chieu_11 = ()
        self.thuctap_chieu_11 = ''
        self.daotao_chieu_11 = ''
        self.chidao_chieu_11 = ()

        # Ngay _12
        self.ngay_thu_12 = ''
        self.ngay_nghi_12 = False

        self.noidung_sang_12 = ()
        self.mot_sang_12 = ()
        self.hai_sang_12 = ()
        self.ba_sang_12 = ()
        self.bon_sang_12 = ()
        self.thuctap_sang_12 = ''
        self.daotao_sang_12 = ''
        self.chidao_sang_12 = ()

        self.noidung_chieu_12 = ()
        self.mot_chieu_12 = ()
        self.hai_chieu_12 = ()
        self.ba_chieu_12 = ()
        self.bon_chieu_12 = ()
        self.thuctap_chieu_12 = ''
        self.daotao_chieu_12 = ''
        self.chidao_chieu_12 = ()

        # Ngay _13
        self.ngay_thu_13 = ''
        self.ngay_nghi_13 = False

        self.noidung_sang_13 = ()
        self.mot_sang_13 = ()
        self.hai_sang_13 = ()
        self.ba_sang_13 = ()
        self.bon_sang_13 = ()
        self.thuctap_sang_13 = ''
        self.daotao_sang_13 = ''
        self.chidao_sang_13 = ()

        self.noidung_chieu_13 = ()
        self.mot_chieu_13 = ()
        self.hai_chieu_13 = ()
        self.ba_chieu_13 = ()
        self.bon_chieu_13 = ()
        self.thuctap_chieu_13 = ''
        self.daotao_chieu_13 = ''
        self.chidao_chieu_13 = ()

        # Ngay _14
        self.ngay_thu_14 = ''
        self.ngay_nghi_14 = False

        self.noidung_sang_14 = ()
        self.mot_sang_14 = ()
        self.hai_sang_14 = ()
        self.ba_sang_14 = ()
        self.bon_sang_14 = ()
        self.thuctap_sang_14 = ''
        self.daotao_sang_14 = ''
        self.chidao_sang_14 = ()

        self.noidung_chieu_14 = ()
        self.mot_chieu_14 = ()
        self.hai_chieu_14 = ()
        self.ba_chieu_14 = ()
        self.bon_chieu_14 = ()
        self.thuctap_chieu_14 = ''
        self.daotao_chieu_14 = ''
        self.chidao_chieu_14 = ()

        # Ngay _15
        self.ngay_thu_15 = ''
        self.ngay_nghi_15 = False

        self.noidung_sang_15 = ()
        self.mot_sang_15 = ()
        self.hai_sang_15 = ()
        self.ba_sang_15 = ()
        self.bon_sang_15 = ()
        self.thuctap_sang_15 = ''
        self.daotao_sang_15 = ''
        self.chidao_sang_15 = ()

        self.noidung_chieu_15 = ()
        self.mot_chieu_15 = ()
        self.hai_chieu_15 = ()
        self.ba_chieu_15 = ()
        self.bon_chieu_15 = ()
        self.thuctap_chieu_15 = ''
        self.daotao_chieu_15 = ''
        self.chidao_chieu_15 = ()

        # Ngay _16
        self.ngay_thu_16 = ''
        self.ngay_nghi_16 = False

        self.noidung_sang_16 = ()
        self.mot_sang_16 = ()
        self.hai_sang_16 = ()
        self.ba_sang_16 = ()
        self.bon_sang_16 = ()
        self.thuctap_sang_16 = ''
        self.daotao_sang_16 = ''
        self.chidao_sang_16 = ()

        self.noidung_chieu_16 = ()
        self.mot_chieu_16 = ()
        self.hai_chieu_16 = ()
        self.ba_chieu_16 = ()
        self.bon_chieu_16 = ()
        self.thuctap_chieu_16 = ''
        self.daotao_chieu_16 = ''
        self.chidao_chieu_16 = ()

        # Ngay _17
        self.ngay_thu_17 = ''
        self.ngay_nghi_17 = False

        self.noidung_sang_17 = ()
        self.mot_sang_17 = ()
        self.hai_sang_17 = ()
        self.ba_sang_17 = ()
        self.bon_sang_17 = ()
        self.thuctap_sang_17 = ''
        self.daotao_sang_17 = ''
        self.chidao_sang_17 = ()

        self.noidung_chieu_17 = ()
        self.mot_chieu_17 = ()
        self.hai_chieu_17 = ()
        self.ba_chieu_17 = ()
        self.bon_chieu_17 = ()
        self.thuctap_chieu_17 = ''
        self.daotao_chieu_17 = ''
        self.chidao_chieu_17 = ()

        # Ngay _18
        self.ngay_thu_18 = ''
        self.ngay_nghi_18 = False

        self.noidung_sang_18 = ()
        self.mot_sang_18 = ()
        self.hai_sang_18 = ()
        self.ba_sang_18 = ()
        self.bon_sang_18 = ()
        self.thuctap_sang_18 = ''
        self.daotao_sang_18 = ''
        self.chidao_sang_18 = ()

        self.noidung_chieu_18 = ()
        self.mot_chieu_18 = ()
        self.hai_chieu_18 = ()
        self.ba_chieu_18 = ()
        self.bon_chieu_18 = ()
        self.thuctap_chieu_18 = ''
        self.daotao_chieu_18 = ''
        self.chidao_chieu_18 = ()

        # Ngay _19
        self.ngay_thu_19 = ''
        self.ngay_nghi_19 = False

        self.noidung_sang_19 = ()
        self.mot_sang_19 = ()
        self.hai_sang_19 = ()
        self.ba_sang_19 = ()
        self.bon_sang_19 = ()
        self.thuctap_sang_19 = ''
        self.daotao_sang_19 = ''
        self.chidao_sang_19 = ()

        self.noidung_chieu_19 = ()
        self.mot_chieu_19 = ()
        self.hai_chieu_19 = ()
        self.ba_chieu_19 = ()
        self.bon_chieu_19 = ()
        self.thuctap_chieu_19 = ''
        self.daotao_chieu_19 = ''
        self.chidao_chieu_19 = ()

        # Ngay _20
        self.ngay_thu_20 = ''
        self.ngay_nghi_20 = False

        self.noidung_sang_20 = ()
        self.mot_sang_20 = ()
        self.hai_sang_20 = ()
        self.ba_sang_20 = ()
        self.bon_sang_20 = ()
        self.thuctap_sang_20 = ''
        self.daotao_sang_20 = ''
        self.chidao_sang_20 = ()

        self.noidung_chieu_20 = ()
        self.mot_chieu_20 = ()
        self.hai_chieu_20 = ()
        self.ba_chieu_20 = ()
        self.bon_chieu_20 = ()
        self.thuctap_chieu_20 = ''
        self.daotao_chieu_20 = ''
        self.chidao_chieu_20 = ()

        # Ngay _21
        self.ngay_thu_21 = ''
        self.ngay_nghi_21 = False

        self.noidung_sang_21 = ()
        self.mot_sang_21 = ()
        self.hai_sang_21 = ()
        self.ba_sang_21 = ()
        self.bon_sang_21 = ()
        self.thuctap_sang_21 = ''
        self.daotao_sang_21 = ''
        self.chidao_sang_21 = ()

        self.noidung_chieu_21 = ()
        self.mot_chieu_21 = ()
        self.hai_chieu_21 = ()
        self.ba_chieu_21 = ()
        self.bon_chieu_21 = ()
        self.thuctap_chieu_21 = ''
        self.daotao_chieu_21 = ''
        self.chidao_chieu_21 = ()

        # Ngay _22
        self.ngay_thu_22 = ''
        self.ngay_nghi_22 = False

        self.noidung_sang_22 = ()
        self.mot_sang_22 = ()
        self.hai_sang_22 = ()
        self.ba_sang_22 = ()
        self.bon_sang_22 = ()
        self.thuctap_sang_22 = ''
        self.daotao_sang_22 = ''
        self.chidao_sang_22 = ()

        self.noidung_chieu_22 = ()
        self.mot_chieu_22 = ()
        self.hai_chieu_22 = ()
        self.ba_chieu_22 = ()
        self.bon_chieu_22 = ()
        self.thuctap_chieu_22 = ''
        self.daotao_chieu_22 = ''
        self.chidao_chieu_22 = ()

        # Ngay _23
        self.ngay_thu_23 = ''
        self.ngay_nghi_23 = False

        self.noidung_sang_23 = ()
        self.mot_sang_23 = ()
        self.hai_sang_23 = ()
        self.ba_sang_23 = ()
        self.bon_sang_23 = ()
        self.thuctap_sang_23 = ''
        self.daotao_sang_23 = ''
        self.chidao_sang_23 = ()

        self.noidung_chieu_23 = ()
        self.mot_chieu_23 = ()
        self.hai_chieu_23 = ()
        self.ba_chieu_23 = ()
        self.bon_chieu_23 = ()
        self.thuctap_chieu_23 = ''
        self.daotao_chieu_23 = ''
        self.chidao_chieu_23 = ()

        # Ngay _24
        self.ngay_thu_24 = ''
        self.ngay_nghi_24 = False

        self.noidung_sang_24 = ()
        self.mot_sang_24 = ()
        self.hai_sang_24 = ()
        self.ba_sang_24 = ()
        self.bon_sang_24 = ()
        self.thuctap_sang_24 = ''
        self.daotao_sang_24 = ''
        self.chidao_sang_24 = ()

        self.noidung_chieu_24 = ()
        self.mot_chieu_24 = ()
        self.hai_chieu_24 = ()
        self.ba_chieu_24 = ()
        self.bon_chieu_24 = ()
        self.thuctap_chieu_24 = ''
        self.daotao_chieu_24 = ''
        self.chidao_chieu_24 = ()

        # Ngay _25
        self.ngay_thu_25 = ''
        self.ngay_nghi_25 = False

        self.noidung_sang_25 = ()
        self.mot_sang_25 = ()
        self.hai_sang_25 = ()
        self.ba_sang_25 = ()
        self.bon_sang_25 = ()
        self.thuctap_sang_25 = ''
        self.daotao_sang_25 = ''
        self.chidao_sang_25 = ()

        self.noidung_chieu_25 = ()
        self.mot_chieu_25 = ()
        self.hai_chieu_25 = ()
        self.ba_chieu_25 = ()
        self.bon_chieu_25 = ()
        self.thuctap_chieu_25 = ''
        self.daotao_chieu_25 = ''
        self.chidao_chieu_25 = ()

        # Ngay _26
        self.ngay_thu_26 = ''
        self.ngay_nghi_26 = False

        self.noidung_sang_26 = ()
        self.mot_sang_26 = ()
        self.hai_sang_26 = ()
        self.ba_sang_26 = ()
        self.bon_sang_26 = ()
        self.thuctap_sang_26 = ''
        self.daotao_sang_26 = ''
        self.chidao_sang_26 = ()

        self.noidung_chieu_26 = ()
        self.mot_chieu_26 = ()
        self.hai_chieu_26 = ()
        self.ba_chieu_26 = ()
        self.bon_chieu_26 = ()
        self.thuctap_chieu_26 = ''
        self.daotao_chieu_26 = ''
        self.chidao_chieu_26 = ()

        # Ngay _27
        self.ngay_thu_27 = ''
        self.ngay_nghi_27 = False

        self.noidung_sang_27 = ()
        self.mot_sang_27 = ()
        self.hai_sang_27 = ()
        self.ba_sang_27 = ()
        self.bon_sang_27 = ()
        self.thuctap_sang_27 = ''
        self.daotao_sang_27 = ''
        self.chidao_sang_27 = ()

        self.noidung_chieu_27 = ()
        self.mot_chieu_27 = ()
        self.hai_chieu_27 = ()
        self.ba_chieu_27 = ()
        self.bon_chieu_27 = ()
        self.thuctap_chieu_27 = ''
        self.daotao_chieu_27 = ''
        self.chidao_chieu_27 = ()

        # Ngay _28
        self.ngay_thu_28 = ''
        self.ngay_nghi_28 = False

        self.noidung_sang_28 = ()
        self.mot_sang_28 = ()
        self.hai_sang_28 = ()
        self.ba_sang_28 = ()
        self.bon_sang_28 = ()
        self.thuctap_sang_28 = ''
        self.daotao_sang_28 = ''
        self.chidao_sang_28 = ()

        self.noidung_chieu_28 = ()
        self.mot_chieu_28 = ()
        self.hai_chieu_28 = ()
        self.ba_chieu_28 = ()
        self.bon_chieu_28 = ()
        self.thuctap_chieu_28 = ''
        self.daotao_chieu_28 = ''
        self.chidao_chieu_28 = ()

        # Ngay _29
        self.ngay_thu_29 = ''
        self.ngay_nghi_29 = False

        self.noidung_sang_29 = ()
        self.mot_sang_29 = ()
        self.hai_sang_29 = ()
        self.ba_sang_29 = ()
        self.bon_sang_29 = ()
        self.thuctap_sang_29 = ''
        self.daotao_sang_29 = ''
        self.chidao_sang_29 = ()

        self.noidung_chieu_29 = ()
        self.mot_chieu_29 = ()
        self.hai_chieu_29 = ()
        self.ba_chieu_29 = ()
        self.bon_chieu_29 = ()
        self.thuctap_chieu_29 = ''
        self.daotao_chieu_29 = ''
        self.chidao_chieu_29 = ()

        # Ngay _30
        self.ngay_thu_30 = ''
        self.ngay_nghi_30 = False

        self.noidung_sang_30 = ()
        self.mot_sang_30 = ()
        self.hai_sang_30 = ()
        self.ba_sang_30 = ()
        self.bon_sang_30 = ()
        self.thuctap_sang_30 = ''
        self.daotao_sang_30 = ''
        self.chidao_sang_30 = ()

        self.noidung_chieu_30 = ()
        self.mot_chieu_30 = ()
        self.hai_chieu_30 = ()
        self.ba_chieu_30 = ()
        self.bon_chieu_30 = ()
        self.thuctap_chieu_30 = ''
        self.daotao_chieu_30 = ''
        self.chidao_chieu_30 = ()

        # Ngay _31
        self.ngay_thu_31 = ''
        self.ngay_nghi_31 = False

        self.noidung_sang_31 = ()
        self.mot_sang_31 = ()
        self.hai_sang_31 = ()
        self.ba_sang_31 = ()
        self.bon_sang_31 = ()
        self.thuctap_sang_31 = ''
        self.daotao_sang_31 = ''
        self.chidao_sang_31 = ()

        self.noidung_chieu_31 = ()
        self.mot_chieu_31 = ()
        self.hai_chieu_31 = ()
        self.ba_chieu_31 = ()
        self.bon_chieu_31 = ()
        self.thuctap_chieu_31 = ''
        self.daotao_chieu_31 = ''
        self.chidao_chieu_31 = ()

        if self.nam_lich_junkai and self.thang_lich_junkai:
            num_days = monthrange(int(self.nam_lich_junkai), int(self.thang_lich_junkai))
            date_time_start = datetime.datetime(int(self.nam_lich_junkai), int(self.thang_lich_junkai), 1, 0, 0, 0)

            item = 0
            for i in range(int(num_days[1])):
                item += 1
                if item == 1:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_1 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_1 = False
                    else:
                        self.ngay_nghi_1 = True
                        self.thuctap_sang_1 = '休日'
                        self.thuctap_chieu_1 = '休日'
                if item == 2:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_2 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_2 = False
                    else:
                        self.ngay_nghi_2 = True
                        self.thuctap_sang_2 = '休日'
                        self.thuctap_chieu_2 = '休日'
                if item == 3:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_3 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_3 = False
                    else:
                        self.ngay_nghi_3 = True
                        self.thuctap_sang_3 = '休日'
                        self.thuctap_chieu_3 = '休日'
                if item == 4:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_4 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_4 = False
                    else:
                        self.ngay_nghi_4 = True
                        self.thuctap_sang_4 = '休日'
                        self.thuctap_chieu_4 = '休日'
                if item == 5:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_5 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_5 = False
                    else:
                        self.ngay_nghi_5 = True
                        self.thuctap_sang_5 = '休日'
                        self.thuctap_chieu_5 = '休日'
                if item == 6:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_6 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_6 = False
                    else:
                        self.ngay_nghi_6 = True
                        self.thuctap_sang_6 = '休日'
                        self.thuctap_chieu_6 = '休日'
                if item == 7:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_7 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_7 = False
                    else:
                        self.ngay_nghi_7 = True
                        self.thuctap_sang_7 = '休日'
                        self.thuctap_chieu_7 = '休日'
                if item == 8:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_8 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_8 = False
                    else:
                        self.ngay_nghi_8 = True
                        self.thuctap_sang_8 = '休日'
                        self.thuctap_chieu_8 = '休日'
                if item == 9:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_9 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_9 = False
                    else:
                        self.ngay_nghi_9 = True
                        self.thuctap_sang_9 = '休日'
                        self.thuctap_chieu_9 = '休日'
                if item == 10:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_10 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_10 = False
                    else:
                        self.ngay_nghi_10 = True
                        self.thuctap_sang_10 = '休日'
                        self.thuctap_chieu_10 = '休日'
                if item == 11:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_11 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_11 = False
                    else:
                        self.ngay_nghi_11 = True
                        self.thuctap_sang_11 = '休日'
                        self.thuctap_chieu_11 = '休日'
                if item == 12:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_12 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_12 = False
                    else:
                        self.ngay_nghi_12 = True
                        self.thuctap_sang_12 = '休日'
                        self.thuctap_chieu_12 = '休日'
                if item == 13:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_13 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_13 = False
                    else:
                        self.ngay_nghi_13 = True
                        self.thuctap_sang_13 = '休日'
                        self.thuctap_chieu_13 = '休日'
                if item == 14:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_14 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_14 = False
                    else:
                        self.ngay_nghi_14 = True
                        self.thuctap_sang_14 = '休日'
                        self.thuctap_chieu_14 = '休日'
                if item == 15:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_15 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_15 = False
                    else:
                        self.ngay_nghi_15 = True
                        self.thuctap_sang_15 = '休日'
                        self.thuctap_chieu_15 = '休日'
                if item == 16:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_16 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_16 = False
                    else:
                        self.ngay_nghi_16 = True
                        self.thuctap_sang_16 = '休日'
                        self.thuctap_chieu_16 = '休日'
                if item == 17:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_17 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_17 = False
                    else:
                        self.ngay_nghi_17 = True
                        self.thuctap_sang_17 = '休日'
                        self.thuctap_chieu_17 = '休日'
                if item == 18:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_18 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_18 = False
                    else:
                        self.ngay_nghi_18 = True
                        self.thuctap_sang_18 = '休日'
                        self.thuctap_chieu_18 = '休日'
                if item == 19:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_19 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_19 = False
                    else:
                        self.ngay_nghi_19 = True
                        self.thuctap_sang_19 = '休日'
                        self.thuctap_chieu_19 = '休日'
                if item == 20:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_20 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_20 = False
                    else:
                        self.ngay_nghi_20 = True
                        self.thuctap_sang_20 = '休日'
                        self.thuctap_chieu_20 = '休日'
                if item == 21:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_21 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_21 = False
                    else:
                        self.ngay_nghi_21 = True
                        self.thuctap_sang_21 = '休日'
                        self.thuctap_chieu_21 = '休日'
                if item == 22:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_22 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_22 = False
                    else:
                        self.ngay_nghi_22 = True
                        self.thuctap_sang_22 = '休日'
                        self.thuctap_chieu_22 = '休日'
                if item == 23:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_23 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_23 = False
                    else:
                        self.ngay_nghi_23 = True
                        self.thuctap_sang_23 = '休日'
                        self.thuctap_chieu_23 = '休日'
                if item == 24:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_24 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_24 = False
                    else:
                        self.ngay_nghi_24 = True
                        self.thuctap_sang_24 = '休日'
                        self.thuctap_chieu_24 = '休日'
                if item == 25:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_25 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_25 = False
                    else:
                        self.ngay_nghi_25 = True
                        self.thuctap_sang_25 = '休日'
                        self.thuctap_chieu_25 = '休日'
                if item == 26:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_26 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_26 = False
                    else:
                        self.ngay_nghi_26 = True
                        self.thuctap_sang_26 = '休日'
                        self.thuctap_chieu_26 = '休日'
                if item == 27:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_27 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_27 = False
                    else:
                        self.ngay_nghi_27 = True
                        self.thuctap_sang_27 = '休日'
                        self.thuctap_chieu_27 = '休日'
                if item == 28:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_28 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_28 = False
                    else:
                        self.ngay_nghi_28 = True
                        self.thuctap_sang_28 = '休日'
                        self.thuctap_chieu_28 = '休日'
                if item == 29:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_29 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_29 = False
                    else:
                        self.ngay_nghi_29 = True
                        self.thuctap_sang_29 = '休日'
                        self.thuctap_chieu_29 = '休日'
                if item == 30:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_30 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_30 = False
                    else:
                        self.ngay_nghi_30 = True
                        self.thuctap_sang_30 = '休日'
                        self.thuctap_chieu_30 = '休日'
                if item == 31:
                    ngay = date_time_start + datetime.timedelta(days=i)
                    self.ngay_thu_31 = convert_th(ngay)
                    demo = (date_time_start + datetime.timedelta(days=i)).weekday() in (
                        calendar.SATURDAY, calendar.SUNDAY)
                    if demo == False:
                        self.ngay_nghi_31 = False
                    else:
                        self.ngay_nghi_31 = True
                        self.thuctap_sang_31 = '休日'
                        self.thuctap_chieu_31 = '休日'
        else:
            self.lich_junkai = ""

    @api.multi
    def baocao_junkai_button(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/junkai/download/%s' % (self.id),
            'target': 'self', }

    @api.multi
    def baocao_junkai(self, junkai):
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_38")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            context['bc_0160'] = kiemtra(junkai.nam_lich_junkai)
            context['bc_0020'] = kiemtra(junkai.thang_lich_junkai)

            table_nhatky = []

            # Ngay thu _1
            if junkai.ngay_nghi_1 == False:

                if junkai.thuctap_sang_1 == False and junkai.thuctap_chieu_1 == False:
                    infor_nhatky_1_1 = {}
                    infor_nhatky_1_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_1
                    infor_nhatky_1_1['bc_0164'] = ''
                    if infor_nhatky_1_1:
                        table_nhatky.append(infor_nhatky_1_1)
                else:
                    infor_nhatky_1_1 = {}
                    if junkai.thuctap_sang_1:
                        infor_nhatky_1_1['bc_0161'] = junkai.thang_lich_junkai +"/"+ junkai.ngay_thu_1 if junkai.ngay_thu_1 else ''
                        infor_nhatky_1_1['bc_0164'] = kiemtra(junkai.thuctap_sang_1)
                        infor_nhatky_1_1['bc_0165'] = junkai.noidung_sang_1.ten_noidung.strip()[0] if junkai.noidung_sang_1 else ''
                        infor_nhatky_1_1['bc_0166'] = kiemtra(junkai.daotao_sang_1)
                        infor_nhatky_1_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_1.ten_han_nhanvien)
                    if infor_nhatky_1_1:
                        table_nhatky.append(infor_nhatky_1_1)

                    infor_nhatky_1_2 = {}
                    if junkai.thuctap_chieu_1:
                        infor_nhatky_1_2['bc_0161'] = ''
                        infor_nhatky_1_2['bc_0164'] = junkai.thuctap_chieu_1
                        infor_nhatky_1_2['bc_0165'] = kiemtra(junkai.noidung_chieu_1.ten_noidung.strip()[0])
                        infor_nhatky_1_2['bc_0166'] = kiemtra(junkai.daotao_chieu_1)
                        infor_nhatky_1_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_1.ten_han_nhanvien)
                    if infor_nhatky_1_2:
                        table_nhatky.append(infor_nhatky_1_2)
            else:

                infor_nhatky_1_1 = {}
                if junkai.thuctap_sang_1:
                    infor_nhatky_1_1['bc_0161'] = junkai.thang_lich_junkai +"/"+junkai.ngay_thu_1
                    infor_nhatky_1_1['bc_0164'] = junkai.thuctap_sang_1
                if infor_nhatky_1_1:
                    table_nhatky.append(infor_nhatky_1_1)

            # Ngay thu _2
            if junkai.ngay_nghi_2 == False:
                if junkai.thuctap_sang_2 == False and junkai.thuctap_chieu_2 == False:
                    infor_nhatky_2_1 = {}
                    infor_nhatky_2_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_2
                    infor_nhatky_2_1['bc_0164'] = ''
                    if infor_nhatky_2_1:
                        table_nhatky.append(infor_nhatky_2_1)
                else:
                    infor_nhatky_2_1 = {}
                    if junkai.thuctap_sang_2:
                        infor_nhatky_2_1['bc_0161'] = junkai.thang_lich_junkai +"/"+ junkai.ngay_thu_2 if junkai.ngay_thu_2 else ''
                        infor_nhatky_2_1['bc_0164'] = kiemtra(junkai.thuctap_sang_2)
                        infor_nhatky_2_1['bc_0165'] = junkai.noidung_sang_2.ten_noidung.strip()[
                            0] if junkai.noidung_sang_2 else ''
                        infor_nhatky_2_1['bc_0166'] = kiemtra(junkai.daotao_sang_2)
                        infor_nhatky_2_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_2.ten_han_nhanvien)
                    if infor_nhatky_2_1:
                        table_nhatky.append(infor_nhatky_2_1)

                    infor_nhatky_2_2 = {}
                    if junkai.thuctap_chieu_2:
                        infor_nhatky_2_2['bc_0161'] = ''
                        infor_nhatky_2_2['bc_0164'] = junkai.thuctap_chieu_2
                        infor_nhatky_2_2['bc_0165'] = kiemtra(junkai.noidung_chieu_2.ten_noidung.strip()[0])
                        infor_nhatky_2_2['bc_0166'] = kiemtra(junkai.daotao_chieu_2)
                        infor_nhatky_2_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_2.ten_han_nhanvien)
                    if infor_nhatky_2_2:
                        table_nhatky.append(infor_nhatky_2_2)
            else:

                infor_nhatky_2_1 = {}
                if junkai.thuctap_sang_2:
                    infor_nhatky_2_1['bc_0161'] = junkai.thang_lich_junkai +"/"+junkai.ngay_thu_2
                    infor_nhatky_2_1['bc_0164'] = junkai.thuctap_sang_2
                if infor_nhatky_2_1:
                    table_nhatky.append(infor_nhatky_2_1)

            # Ngay thu _3
            if junkai.ngay_nghi_3 == False:
                if junkai.thuctap_sang_3 == False and junkai.thuctap_chieu_3 == False:
                    infor_nhatky_3_1 = {}
                    if junkai.thuctap_sang_3:
                        infor_nhatky_3_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_3
                        infor_nhatky_3_1['bc_0164'] = junkai.thuctap_sang_3
                    if infor_nhatky_3_1:
                        table_nhatky.append(infor_nhatky_3_1)
                else:
                    infor_nhatky_3_1 = {}
                    if junkai.thuctap_sang_3:
                        infor_nhatky_3_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_3 if junkai.ngay_thu_3 else ''
                        infor_nhatky_3_1['bc_0164'] = kiemtra(junkai.thuctap_sang_3)
                        infor_nhatky_3_1['bc_0165'] = junkai.noidung_sang_3.ten_noidung.strip()[
                            0] if junkai.noidung_sang_3 else ''
                        infor_nhatky_3_1['bc_0166'] = kiemtra(junkai.daotao_sang_3)
                        infor_nhatky_3_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_3.ten_han_nhanvien)
                    if infor_nhatky_3_1:
                        table_nhatky.append(infor_nhatky_3_1)

                    infor_nhatky_3_2 = {}
                    if junkai.thuctap_chieu_3:
                        infor_nhatky_3_2['bc_0161'] = ''
                        infor_nhatky_3_2['bc_0164'] = junkai.thuctap_chieu_3
                        infor_nhatky_3_2['bc_0165'] = kiemtra(junkai.noidung_chieu_3.ten_noidung.strip()[0])
                        infor_nhatky_3_2['bc_0166'] = kiemtra(junkai.daotao_chieu_3)
                        infor_nhatky_3_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_3.ten_han_nhanvien)
                    if infor_nhatky_3_2:
                        table_nhatky.append(infor_nhatky_3_2)
            else:

                infor_nhatky_3_1 = {}
                if junkai.thuctap_sang_3:
                    infor_nhatky_3_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_3
                    infor_nhatky_3_1['bc_0164'] = junkai.thuctap_sang_3
                if infor_nhatky_3_1:
                    table_nhatky.append(infor_nhatky_3_1)

            # Ngay thu _4
            if junkai.ngay_nghi_4 == False:
                if junkai.thuctap_sang_4 == False and junkai.thuctap_chieu_4 == False:
                    infor_nhatky_4_1 = {}
                    infor_nhatky_4_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_4
                    infor_nhatky_4_1['bc_0164'] = ''
                    if infor_nhatky_4_1:
                        table_nhatky.append(infor_nhatky_4_1)
                else:
                    infor_nhatky_4_1 = {}
                    if junkai.thuctap_sang_4:
                        infor_nhatky_4_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_4 if junkai.ngay_thu_4 else ''
                        infor_nhatky_4_1['bc_0164'] = kiemtra(junkai.thuctap_sang_4)
                        infor_nhatky_4_1['bc_0165'] = junkai.noidung_sang_4.ten_noidung.strip()[
                            0] if junkai.noidung_sang_4 else ''
                        infor_nhatky_4_1['bc_0166'] = kiemtra(junkai.daotao_sang_4)
                        infor_nhatky_4_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_4.ten_han_nhanvien)
                    if infor_nhatky_4_1:
                        table_nhatky.append(infor_nhatky_4_1)

                    infor_nhatky_4_2 = {}
                    if junkai.thuctap_chieu_4:
                        infor_nhatky_4_2['bc_0161'] = ''
                        infor_nhatky_4_2['bc_0164'] = junkai.thuctap_chieu_4
                        infor_nhatky_4_2['bc_0165'] = kiemtra(junkai.noidung_chieu_4.ten_noidung.strip()[0])
                        infor_nhatky_4_2['bc_0166'] = kiemtra(junkai.daotao_chieu_4)
                        infor_nhatky_4_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_4.ten_han_nhanvien)
                    if infor_nhatky_4_2:
                        table_nhatky.append(infor_nhatky_4_2)
            else:

                infor_nhatky_4_1 = {}
                if junkai.thuctap_sang_4:
                    infor_nhatky_4_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_4
                    infor_nhatky_4_1['bc_0164'] = junkai.thuctap_sang_4
                if infor_nhatky_4_1:
                    table_nhatky.append(infor_nhatky_4_1)

            # Ngay thu _5
            if junkai.ngay_nghi_5 == False:
                if junkai.thuctap_sang_5 == False and junkai.thuctap_chieu_5 == False:
                    infor_nhatky_5_1 = {}
                    infor_nhatky_5_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_5
                    infor_nhatky_5_1['bc_0164'] = ''
                    if infor_nhatky_5_1:
                        table_nhatky.append(infor_nhatky_5_1)
                else:
                    infor_nhatky_5_1 = {}
                    if junkai.thuctap_sang_5:
                        infor_nhatky_5_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_5 if junkai.ngay_thu_5 else ''
                        infor_nhatky_5_1['bc_0164'] = kiemtra(junkai.thuctap_sang_5)
                        infor_nhatky_5_1['bc_0165'] = junkai.noidung_sang_5.ten_noidung.strip()[
                            0] if junkai.noidung_sang_5 else ''
                        infor_nhatky_5_1['bc_0166'] = kiemtra(junkai.daotao_sang_5)
                        infor_nhatky_5_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_5.ten_han_nhanvien)
                    if infor_nhatky_5_1:
                        table_nhatky.append(infor_nhatky_5_1)

                    infor_nhatky_5_2 = {}
                    if junkai.thuctap_chieu_5:
                        infor_nhatky_5_2['bc_0161'] = ''
                        infor_nhatky_5_2['bc_0164'] = junkai.thuctap_chieu_5
                        infor_nhatky_5_2['bc_0165'] = kiemtra(junkai.noidung_chieu_5.ten_noidung.strip()[0])
                        infor_nhatky_5_2['bc_0166'] = kiemtra(junkai.daotao_chieu_5)
                        infor_nhatky_5_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_5.ten_han_nhanvien)
                    if infor_nhatky_5_2:
                        table_nhatky.append(infor_nhatky_5_2)
            else:

                infor_nhatky_5_1 = {}
                if junkai.thuctap_sang_5:
                    infor_nhatky_5_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_5
                    infor_nhatky_5_1['bc_0164'] = junkai.thuctap_sang_5
                if infor_nhatky_5_1:
                    table_nhatky.append(infor_nhatky_5_1)

            # Ngay thu _6
            if junkai.ngay_nghi_6 == False:
                if junkai.thuctap_sang_6 == False and junkai.thuctap_chieu_6 == False:
                    infor_nhatky_6_1 = {}
                    infor_nhatky_6_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_6
                    infor_nhatky_6_1['bc_0164'] = ''
                    if infor_nhatky_6_1:
                        table_nhatky.append(infor_nhatky_6_1)
                else:
                    infor_nhatky_6_1 = {}
                    if junkai.thuctap_sang_6:
                        infor_nhatky_6_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_6 if junkai.ngay_thu_6 else ''
                        infor_nhatky_6_1['bc_0164'] = kiemtra(junkai.thuctap_sang_6)
                        infor_nhatky_6_1['bc_0165'] = junkai.noidung_sang_6.ten_noidung.strip()[
                            0] if junkai.noidung_sang_6 else ''
                        infor_nhatky_6_1['bc_0166'] = kiemtra(junkai.daotao_sang_6)
                        infor_nhatky_6_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_6.ten_han_nhanvien)
                    if infor_nhatky_6_1:
                        table_nhatky.append(infor_nhatky_6_1)

                    infor_nhatky_6_2 = {}
                    if junkai.thuctap_chieu_6:
                        infor_nhatky_6_2['bc_0161'] = ''
                        infor_nhatky_6_2['bc_0164'] = junkai.thuctap_chieu_6
                        infor_nhatky_6_2['bc_0165'] = kiemtra(junkai.noidung_chieu_6.ten_noidung.strip()[0])
                        infor_nhatky_6_2['bc_0166'] = kiemtra(junkai.daotao_chieu_6)
                        infor_nhatky_6_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_6.ten_han_nhanvien)
                    if infor_nhatky_6_2:
                        table_nhatky.append(infor_nhatky_6_2)
            else:

                infor_nhatky_6_1 = {}
                if junkai.thuctap_sang_6:
                    infor_nhatky_6_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_6
                    infor_nhatky_6_1['bc_0164'] = junkai.thuctap_sang_6
                if infor_nhatky_6_1:
                    table_nhatky.append(infor_nhatky_6_1)

            # Ngay thu _7
            if junkai.ngay_nghi_7 == False:
                if junkai.thuctap_sang_7 == False and junkai.thuctap_chieu_7 == False:
                    infor_nhatky_7_1 = {}
                    infor_nhatky_7_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_7
                    infor_nhatky_7_1['bc_0164'] = ''
                    if infor_nhatky_7_1:
                        table_nhatky.append(infor_nhatky_7_1)
                else:
                    infor_nhatky_7_1 = {}
                    if junkai.thuctap_sang_7:
                        infor_nhatky_7_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_7 if junkai.ngay_thu_7 else ''
                        infor_nhatky_7_1['bc_0164'] = kiemtra(junkai.thuctap_sang_7)
                        infor_nhatky_7_1['bc_0165'] = junkai.noidung_sang_7.ten_noidung.strip()[
                            0] if junkai.noidung_sang_7 else ''
                        infor_nhatky_7_1['bc_0166'] = kiemtra(junkai.daotao_sang_7)
                        infor_nhatky_7_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_7.ten_han_nhanvien)
                    if infor_nhatky_7_1:
                        table_nhatky.append(infor_nhatky_7_1)

                    infor_nhatky_7_2 = {}
                    if junkai.thuctap_chieu_7:
                        infor_nhatky_7_2['bc_0161'] = ''
                        infor_nhatky_7_2['bc_0164'] = junkai.thuctap_chieu_7
                        infor_nhatky_7_2['bc_0165'] = kiemtra(junkai.noidung_chieu_7.ten_noidung.strip()[0])
                        infor_nhatky_7_2['bc_0166'] = kiemtra(junkai.daotao_chieu_7)
                        infor_nhatky_7_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_7.ten_han_nhanvien)
                    if infor_nhatky_7_2:
                        table_nhatky.append(infor_nhatky_7_2)
            else:

                infor_nhatky_7_1 = {}
                if junkai.thuctap_sang_7:
                    infor_nhatky_7_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_7
                    infor_nhatky_7_1['bc_0164'] = junkai.thuctap_sang_7
                if infor_nhatky_7_1:
                    table_nhatky.append(infor_nhatky_7_1)

            # Ngay thu _8
            if junkai.ngay_nghi_8 == False:
                if junkai.thuctap_sang_8 == False and junkai.thuctap_chieu_8 == False:
                    infor_nhatky_8_1 = {}
                    infor_nhatky_8_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_8
                    infor_nhatky_8_1['bc_0164'] = ''
                    if infor_nhatky_8_1:
                        table_nhatky.append(infor_nhatky_8_1)
                else:
                    infor_nhatky_8_1 = {}
                    if junkai.thuctap_sang_8:
                        infor_nhatky_8_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_8 if junkai.ngay_thu_8 else ''
                        infor_nhatky_8_1['bc_0164'] = kiemtra(junkai.thuctap_sang_8)
                        infor_nhatky_8_1['bc_0165'] = junkai.noidung_sang_8.ten_noidung.strip()[
                            0] if junkai.noidung_sang_8 else ''
                        infor_nhatky_8_1['bc_0166'] = kiemtra(junkai.daotao_sang_8)
                        infor_nhatky_8_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_8.ten_han_nhanvien)
                    if infor_nhatky_8_1:
                        table_nhatky.append(infor_nhatky_8_1)

                    infor_nhatky_8_2 = {}
                    if junkai.thuctap_chieu_8:
                        infor_nhatky_3_2['bc_0161'] = ''
                        infor_nhatky_8_2['bc_0164'] = junkai.thuctap_chieu_8
                        infor_nhatky_8_2['bc_0165'] = kiemtra(junkai.noidung_chieu_8.ten_noidung.strip()[0])
                        infor_nhatky_8_2['bc_0166'] = kiemtra(junkai.daotao_chieu_8)
                        infor_nhatky_8_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_8.ten_han_nhanvien)
                    if infor_nhatky_8_2:
                        table_nhatky.append(infor_nhatky_8_2)
            else:

                infor_nhatky_8_1 = {}
                if junkai.thuctap_sang_8:
                    infor_nhatky_8_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_8
                    infor_nhatky_8_1['bc_0164'] = junkai.thuctap_sang_8
                if infor_nhatky_8_1:
                    table_nhatky.append(infor_nhatky_8_1)

            # Ngay thu _9
            if junkai.ngay_nghi_9 == False:
                if junkai.thuctap_sang_9 == False and junkai.thuctap_chieu_9 == False:
                    infor_nhatky_9_1 = {}
                    infor_nhatky_9_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_9
                    infor_nhatky_9_1['bc_0164'] = ''
                    if infor_nhatky_9_1:
                        table_nhatky.append(infor_nhatky_9_1)
                else:
                    infor_nhatky_9_1 = {}
                    if junkai.thuctap_sang_9:
                        infor_nhatky_9_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_9 if junkai.ngay_thu_9 else ''
                        infor_nhatky_9_1['bc_0164'] = kiemtra(junkai.thuctap_sang_9)
                        infor_nhatky_9_1['bc_0165'] = junkai.noidung_sang_9.ten_noidung.strip()[
                            0] if junkai.noidung_sang_9 else ''
                        infor_nhatky_9_1['bc_0166'] = kiemtra(junkai.daotao_sang_9)
                        infor_nhatky_9_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_9.ten_han_nhanvien)
                    if infor_nhatky_9_1:
                        table_nhatky.append(infor_nhatky_9_1)

                    infor_nhatky_9_2 = {}
                    if junkai.thuctap_chieu_9:
                        infor_nhatky_9_2['bc_0161'] = ''
                        infor_nhatky_9_2['bc_0164'] = junkai.thuctap_chieu_9
                        infor_nhatky_9_2['bc_0165'] = kiemtra(junkai.noidung_chieu_9.ten_noidung.strip()[0])
                        infor_nhatky_9_2['bc_0166'] = kiemtra(junkai.daotao_chieu_9)
                        infor_nhatky_9_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_9.ten_han_nhanvien)
                    if infor_nhatky_9_2:
                        table_nhatky.append(infor_nhatky_9_2)
            else:

                infor_nhatky_9_1 = {}
                if junkai.thuctap_sang_9:
                    infor_nhatky_9_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_9
                    infor_nhatky_9_1['bc_0164'] = junkai.thuctap_sang_9
                if infor_nhatky_9_1:
                    table_nhatky.append(infor_nhatky_9_1)

            # Ngay thu _10
            if junkai.ngay_nghi_10 == False:
                if junkai.thuctap_sang_10 == False and junkai.thuctap_chieu_10 == False:
                    infor_nhatky_10_1 = {}
                    infor_nhatky_10_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_10
                    infor_nhatky_10_1['bc_0164'] = ''
                    if infor_nhatky_10_1:
                        table_nhatky.append(infor_nhatky_10_1)
                else:
                    infor_nhatky_10_1 = {}
                    if junkai.thuctap_sang_10:
                        infor_nhatky_10_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_10 if junkai.ngay_thu_10 else ''
                        infor_nhatky_10_1['bc_0164'] = kiemtra(junkai.thuctap_sang_10)
                        infor_nhatky_10_1['bc_0165'] = junkai.noidung_sang_10.ten_noidung.strip()[
                            0] if junkai.noidung_sang_10 else ''
                        infor_nhatky_10_1['bc_0166'] = kiemtra(junkai.daotao_sang_10)
                        infor_nhatky_10_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_10.ten_han_nhanvien)
                    if infor_nhatky_10_1:
                        table_nhatky.append(infor_nhatky_10_1)

                    infor_nhatky_10_2 = {}
                    if junkai.thuctap_chieu_10:
                        infor_nhatky_10_2['bc_0161'] = ''
                        infor_nhatky_10_2['bc_0164'] = junkai.thuctap_chieu_10
                        infor_nhatky_10_2['bc_0165'] = kiemtra(junkai.noidung_chieu_10.ten_noidung.strip()[0])
                        infor_nhatky_10_2['bc_0166'] = kiemtra(junkai.daotao_chieu_10)
                        infor_nhatky_10_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_10.ten_han_nhanvien)
                    if infor_nhatky_10_2:
                        table_nhatky.append(infor_nhatky_10_2)
            else:

                infor_nhatky_10_1 = {}
                if junkai.thuctap_sang_10:
                    infor_nhatky_10_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_10
                    infor_nhatky_10_1['bc_0164'] = junkai.thuctap_sang_10
                if infor_nhatky_10_1:
                    table_nhatky.append(infor_nhatky_10_1)

            # Ngay thu _11
            if junkai.ngay_nghi_11 == False:
                if junkai.thuctap_sang_11 == False and junkai.thuctap_chieu_11 == False:
                    infor_nhatky_11_1 = {}
                    infor_nhatky_11_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_11
                    infor_nhatky_11_1['bc_0164'] = ''
                    if infor_nhatky_11_1:
                        table_nhatky.append(infor_nhatky_11_1)
                else:
                    infor_nhatky_11_1 = {}
                    if junkai.thuctap_sang_11:
                        infor_nhatky_11_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_11 if junkai.ngay_thu_11 else ''
                        infor_nhatky_11_1['bc_0164'] = kiemtra(junkai.thuctap_sang_11)
                        infor_nhatky_11_1['bc_0165'] = junkai.noidung_sang_11.ten_noidung.strip()[
                            0] if junkai.noidung_sang_11 else ''
                        infor_nhatky_11_1['bc_0166'] = kiemtra(junkai.daotao_sang_11)
                        infor_nhatky_11_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_11.ten_han_nhanvien)
                    if infor_nhatky_11_1:
                        table_nhatky.append(infor_nhatky_11_1)

                    infor_nhatky_11_2 = {}
                    if junkai.thuctap_chieu_11:
                        infor_nhatky_11_2['bc_0161'] = ''
                        infor_nhatky_11_2['bc_0164'] = junkai.thuctap_chieu_11
                        infor_nhatky_11_2['bc_0165'] = kiemtra(junkai.noidung_chieu_11.ten_noidung.strip()[0])
                        infor_nhatky_11_2['bc_0166'] = kiemtra(junkai.daotao_chieu_11)
                        infor_nhatky_11_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_11.ten_han_nhanvien)
                    if infor_nhatky_11_2:
                        table_nhatky.append(infor_nhatky_11_2)
            else:

                infor_nhatky_11_1 = {}
                if junkai.thuctap_sang_11:
                    infor_nhatky_11_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_11
                    infor_nhatky_11_1['bc_0164'] = junkai.thuctap_sang_11
                if infor_nhatky_11_1:
                    table_nhatky.append(infor_nhatky_11_1)

            # Ngay thu _12
            if junkai.ngay_nghi_12 == False:
                if junkai.thuctap_sang_12 == False and junkai.thuctap_chieu_12 == False:
                    infor_nhatky_12_1 = {}
                    infor_nhatky_12_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_12
                    infor_nhatky_12_1['bc_0164'] = ''
                    if infor_nhatky_12_1:
                        table_nhatky.append(infor_nhatky_12_1)
                else:
                    infor_nhatky_12_1 = {}
                    if junkai.thuctap_sang_12:
                        infor_nhatky_12_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_12 if junkai.ngay_thu_12 else ''
                        infor_nhatky_12_1['bc_0164'] = kiemtra(junkai.thuctap_sang_12)
                        infor_nhatky_12_1['bc_0165'] = junkai.noidung_sang_12.ten_noidung.strip()[
                            0] if junkai.noidung_sang_12 else ''
                        infor_nhatky_12_1['bc_0166'] = kiemtra(junkai.daotao_sang_12)
                        infor_nhatky_12_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_12.ten_han_nhanvien)
                    if infor_nhatky_12_1:
                        table_nhatky.append(infor_nhatky_12_1)

                    infor_nhatky_12_2 = {}
                    if junkai.thuctap_chieu_12:
                        infor_nhatky_12_2['bc_0161'] = ''
                        infor_nhatky_12_2['bc_0164'] = junkai.thuctap_chieu_12
                        infor_nhatky_12_2['bc_0165'] = kiemtra(junkai.noidung_chieu_12.ten_noidung.strip()[0])
                        infor_nhatky_12_2['bc_0166'] = kiemtra(junkai.daotao_chieu_12)
                        infor_nhatky_12_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_12.ten_han_nhanvien)
                    if infor_nhatky_12_2:
                        table_nhatky.append(infor_nhatky_12_2)
            else:

                infor_nhatky_12_1 = {}
                if junkai.thuctap_sang_12:
                    infor_nhatky_12_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_12
                    infor_nhatky_12_1['bc_0164'] = junkai.thuctap_sang_12
                if infor_nhatky_12_1:
                    table_nhatky.append(infor_nhatky_12_1)

            # Ngay thu _13
            if junkai.ngay_nghi_13 == False:
                if junkai.thuctap_sang_13 == False and junkai.thuctap_chieu_13 == False:
                    infor_nhatky_13_1 = {}
                    infor_nhatky_13_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_13
                    infor_nhatky_13_1['bc_0164'] = ''
                    if infor_nhatky_13_1:
                        table_nhatky.append(infor_nhatky_13_1)
                else:
                    infor_nhatky_13_1 = {}
                    if junkai.thuctap_sang_13:
                        infor_nhatky_13_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_13 if junkai.ngay_thu_13 else ''
                        infor_nhatky_13_1['bc_0164'] = kiemtra(junkai.thuctap_sang_13)
                        infor_nhatky_13_1['bc_0165'] = junkai.noidung_sang_13.ten_noidung.strip()[
                            0] if junkai.noidung_sang_13 else ''
                        infor_nhatky_13_1['bc_0166'] = kiemtra(junkai.daotao_sang_13)
                        infor_nhatky_13_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_13.ten_han_nhanvien)
                    if infor_nhatky_13_1:
                        table_nhatky.append(infor_nhatky_13_1)

                    infor_nhatky_13_2 = {}
                    if junkai.thuctap_chieu_13:
                        infor_nhatky_13_2['bc_0161'] = ''
                        infor_nhatky_13_2['bc_0164'] = junkai.thuctap_chieu_13
                        infor_nhatky_13_2['bc_0165'] = kiemtra(junkai.noidung_chieu_13.ten_noidung.strip()[0])
                        infor_nhatky_13_2['bc_0166'] = kiemtra(junkai.daotao_chieu_13)
                        infor_nhatky_13_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_13.ten_han_nhanvien)
                    if infor_nhatky_13_2:
                        table_nhatky.append(infor_nhatky_13_2)
            else:

                infor_nhatky_13_1 = {}
                if junkai.thuctap_sang_13:
                    infor_nhatky_13_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_13
                    infor_nhatky_13_1['bc_0164'] = junkai.thuctap_sang_13
                if infor_nhatky_13_1:
                    table_nhatky.append(infor_nhatky_13_1)

            # Ngay thu _14
            if junkai.ngay_nghi_14 == False:
                if junkai.thuctap_sang_14 == False and junkai.thuctap_chieu_14 == False:
                    infor_nhatky_14_1 = {}
                    infor_nhatky_14_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_14
                    infor_nhatky_14_1['bc_0164'] = ''
                    if infor_nhatky_14_1:
                        table_nhatky.append(infor_nhatky_14_1)
                else:
                    infor_nhatky_14_1 = {}
                    if junkai.thuctap_sang_14:
                        infor_nhatky_14_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_14 if junkai.ngay_thu_14 else ''
                        infor_nhatky_14_1['bc_0164'] = kiemtra(junkai.thuctap_sang_14)
                        infor_nhatky_14_1['bc_0165'] = junkai.noidung_sang_14.ten_noidung.strip()[
                            0] if junkai.noidung_sang_14 else ''
                        infor_nhatky_14_1['bc_0166'] = kiemtra(junkai.daotao_sang_14)
                        infor_nhatky_14_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_14.ten_han_nhanvien)
                    if infor_nhatky_14_1:
                        table_nhatky.append(infor_nhatky_14_1)

                    infor_nhatky_14_2 = {}
                    if junkai.thuctap_chieu_14:
                        infor_nhatky_14_2['bc_0161'] = ''
                        infor_nhatky_14_2['bc_0164'] = junkai.thuctap_chieu_14
                        infor_nhatky_14_2['bc_0165'] = kiemtra(junkai.noidung_chieu_14.ten_noidung.strip()[0])
                        infor_nhatky_14_2['bc_0166'] = kiemtra(junkai.daotao_chieu_14)
                        infor_nhatky_14_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_14.ten_han_nhanvien)
                    if infor_nhatky_14_2:
                        table_nhatky.append(infor_nhatky_14_2)
            else:

                infor_nhatky_14_1 = {}
                if junkai.thuctap_sang_14:
                    infor_nhatky_14_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_14
                    infor_nhatky_14_1['bc_0164'] = junkai.thuctap_sang_14
                if infor_nhatky_14_1:
                    table_nhatky.append(infor_nhatky_14_1)

            # Ngay thu _15
            if junkai.ngay_nghi_15 == False:
                if junkai.thuctap_sang_15 == False and junkai.thuctap_chieu_15 == False:
                    infor_nhatky_15_1 = {}
                    infor_nhatky_15_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_15
                    infor_nhatky_15_1['bc_0164'] = ''
                    if infor_nhatky_15_1:
                        table_nhatky.append(infor_nhatky_15_1)
                else:
                    infor_nhatky_15_1 = {}
                    if junkai.thuctap_sang_15:
                        infor_nhatky_15_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_15 if junkai.ngay_thu_15 else ''
                        infor_nhatky_15_1['bc_0164'] = kiemtra(junkai.thuctap_sang_15)
                        infor_nhatky_15_1['bc_0165'] = junkai.noidung_sang_15.ten_noidung.strip()[
                            0] if junkai.noidung_sang_15 else ''
                        infor_nhatky_15_1['bc_0166'] = kiemtra(junkai.daotao_sang_15)
                        infor_nhatky_15_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_15.ten_han_nhanvien)
                    if infor_nhatky_15_1:
                        table_nhatky.append(infor_nhatky_15_1)

                    infor_nhatky_15_2 = {}
                    if junkai.thuctap_chieu_15:
                        infor_nhatky_15_2['bc_0161'] = ''
                        infor_nhatky_15_2['bc_0164'] = junkai.thuctap_chieu_15
                        infor_nhatky_15_2['bc_0165'] = kiemtra(junkai.noidung_chieu_15.ten_noidung.strip()[0])
                        infor_nhatky_15_2['bc_0166'] = kiemtra(junkai.daotao_chieu_15)
                        infor_nhatky_15_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_15.ten_han_nhanvien)
                    if infor_nhatky_15_2:
                        table_nhatky.append(infor_nhatky_15_2)
            else:

                infor_nhatky_15_1 = {}
                if junkai.thuctap_sang_15:
                    infor_nhatky_15_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_15
                    infor_nhatky_15_1['bc_0164'] = junkai.thuctap_sang_15
                if infor_nhatky_15_1:
                    table_nhatky.append(infor_nhatky_15_1)

            # Ngay thu _16
            if junkai.ngay_nghi_16 == False:
                if junkai.thuctap_sang_16 == False and junkai.thuctap_chieu_16 == False:
                    infor_nhatky_16_1 = {}
                    infor_nhatky_16_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_16
                    infor_nhatky_16_1['bc_0164'] = ''
                    if infor_nhatky_16_1:
                        table_nhatky.append(infor_nhatky_16_1)
                else:
                    infor_nhatky_16_1 = {}
                    if junkai.thuctap_sang_16:
                        infor_nhatky_16_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_16 if junkai.ngay_thu_16 else ''
                        infor_nhatky_16_1['bc_0164'] = kiemtra(junkai.thuctap_sang_16)
                        infor_nhatky_16_1['bc_0165'] = junkai.noidung_sang_16.ten_noidung.strip()[
                            0] if junkai.noidung_sang_16 else ''
                        infor_nhatky_16_1['bc_0166'] = kiemtra(junkai.daotao_sang_16)
                        infor_nhatky_16_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_16.ten_han_nhanvien)
                    if infor_nhatky_16_1:
                        table_nhatky.append(infor_nhatky_16_1)

                    infor_nhatky_16_2 = {}
                    if junkai.thuctap_chieu_16:
                        infor_nhatky_16_2['bc_0161'] = ''
                        infor_nhatky_16_2['bc_0164'] = junkai.thuctap_chieu_16
                        infor_nhatky_16_2['bc_0165'] = kiemtra(junkai.noidung_chieu_16.ten_noidung.strip()[0])
                        infor_nhatky_16_2['bc_0166'] = kiemtra(junkai.daotao_chieu_16)
                        infor_nhatky_16_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_16.ten_han_nhanvien)
                    if infor_nhatky_16_2:
                        table_nhatky.append(infor_nhatky_16_2)
            else:

                infor_nhatky_16_1 = {}
                if junkai.thuctap_sang_16:
                    infor_nhatky_16_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_16
                    infor_nhatky_16_1['bc_0164'] = junkai.thuctap_sang_16
                if infor_nhatky_16_1:
                    table_nhatky.append(infor_nhatky_16_1)

            # Ngay thu _17
            if junkai.ngay_nghi_17 == False:
                if junkai.thuctap_sang_17 == False and junkai.thuctap_chieu_17 == False:
                    infor_nhatky_17_1 = {}
                    infor_nhatky_17_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_17
                    infor_nhatky_17_1['bc_0164'] = ''
                    if infor_nhatky_17_1:
                        table_nhatky.append(infor_nhatky_17_1)
                else:
                    infor_nhatky_17_1 = {}
                    if junkai.thuctap_sang_17:
                        infor_nhatky_17_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_17 if junkai.ngay_thu_17 else ''
                        infor_nhatky_17_1['bc_0164'] = kiemtra(junkai.thuctap_sang_17)
                        infor_nhatky_17_1['bc_0165'] = junkai.noidung_sang_17.ten_noidung.strip()[
                            0] if junkai.noidung_sang_17 else ''
                        infor_nhatky_17_1['bc_0166'] = kiemtra(junkai.daotao_sang_17)
                        infor_nhatky_17_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_17.ten_han_nhanvien)
                    if infor_nhatky_17_1:
                        table_nhatky.append(infor_nhatky_17_1)

                    infor_nhatky_17_2 = {}
                    if junkai.thuctap_chieu_17:
                        infor_nhatky_17_2['bc_0161'] = ''
                        infor_nhatky_17_2['bc_0164'] = junkai.thuctap_chieu_17
                        infor_nhatky_17_2['bc_0165'] = kiemtra(junkai.noidung_chieu_17.ten_noidung.strip()[0])
                        infor_nhatky_17_2['bc_0166'] = kiemtra(junkai.daotao_chieu_17)
                        infor_nhatky_17_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_17.ten_han_nhanvien)
                    if infor_nhatky_17_2:
                        table_nhatky.append(infor_nhatky_17_2)
            else:

                infor_nhatky_17_1 = {}
                if junkai.thuctap_sang_17:
                    infor_nhatky_17_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_17
                    infor_nhatky_17_1['bc_0164'] = junkai.thuctap_sang_17
                if infor_nhatky_17_1:
                    table_nhatky.append(infor_nhatky_17_1)

            # Ngay thu _18
            if junkai.ngay_nghi_18 == False:
                if junkai.thuctap_sang_18 == False and junkai.thuctap_chieu_18 == False:
                    infor_nhatky_18_1 = {}
                    infor_nhatky_18_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_18
                    infor_nhatky_18_1['bc_0164'] = ''
                    if infor_nhatky_18_1:
                        table_nhatky.append(infor_nhatky_18_1)
                else:
                    infor_nhatky_18_1 = {}
                    if junkai.thuctap_sang_18:
                        infor_nhatky_18_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_18 if junkai.ngay_thu_18 else ''
                        infor_nhatky_18_1['bc_0164'] = kiemtra(junkai.thuctap_sang_18)
                        infor_nhatky_18_1['bc_0165'] = junkai.noidung_sang_18.ten_noidung.strip()[
                            0] if junkai.noidung_sang_18 else ''
                        infor_nhatky_18_1['bc_0166'] = kiemtra(junkai.daotao_sang_18)
                        infor_nhatky_18_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_18.ten_han_nhanvien)
                    if infor_nhatky_18_1:
                        table_nhatky.append(infor_nhatky_18_1)

                    infor_nhatky_18_2 = {}
                    if junkai.thuctap_chieu_18:
                        infor_nhatky_18_2['bc_0161'] = ''
                        infor_nhatky_18_2['bc_0164'] = junkai.thuctap_chieu_18
                        infor_nhatky_18_2['bc_0165'] = kiemtra(junkai.noidung_chieu_18.ten_noidung.strip()[0])
                        infor_nhatky_18_2['bc_0166'] = kiemtra(junkai.daotao_chieu_18)
                        infor_nhatky_18_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_18.ten_han_nhanvien)
                    if infor_nhatky_18_2:
                        table_nhatky.append(infor_nhatky_18_2)
            else:

                infor_nhatky_18_1 = {}
                if junkai.thuctap_sang_18:
                    infor_nhatky_18_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_18
                    infor_nhatky_18_1['bc_0164'] = junkai.thuctap_sang_18
                if infor_nhatky_18_1:
                    table_nhatky.append(infor_nhatky_18_1)

            # Ngay thu _19
            if junkai.ngay_nghi_19 == False:
                if junkai.thuctap_sang_19 == False and junkai.thuctap_chieu_19 == False:
                    infor_nhatky_19_1 = {}
                    infor_nhatky_19_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_19
                    infor_nhatky_19_1['bc_0164'] = ''
                    if infor_nhatky_19_1:
                        table_nhatky.append(infor_nhatky_19_1)
                else:
                    infor_nhatky_19_1 = {}
                    if junkai.thuctap_sang_19:
                        infor_nhatky_19_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_19 if junkai.ngay_thu_19 else ''
                        infor_nhatky_19_1['bc_0164'] = kiemtra(junkai.thuctap_sang_19)
                        infor_nhatky_19_1['bc_0165'] = junkai.noidung_sang_19.ten_noidung.strip()[
                            0] if junkai.noidung_sang_19 else ''
                        infor_nhatky_19_1['bc_0166'] = kiemtra(junkai.daotao_sang_19)
                        infor_nhatky_19_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_19.ten_han_nhanvien)
                    if infor_nhatky_19_1:
                        table_nhatky.append(infor_nhatky_19_1)

                    infor_nhatky_19_2 = {}
                    if junkai.thuctap_chieu_19:
                        infor_nhatky_19_2['bc_0161'] = ''
                        infor_nhatky_19_2['bc_0164'] = junkai.thuctap_chieu_19
                        infor_nhatky_19_2['bc_0165'] = kiemtra(junkai.noidung_chieu_19.ten_noidung.strip()[0])
                        infor_nhatky_19_2['bc_0166'] = kiemtra(junkai.daotao_chieu_19)
                        infor_nhatky_19_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_19.ten_han_nhanvien)
                    if infor_nhatky_19_2:
                        table_nhatky.append(infor_nhatky_19_2)
            else:

                infor_nhatky_19_1 = {}
                if junkai.thuctap_sang_19:
                    infor_nhatky_19_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_19
                    infor_nhatky_19_1['bc_0164'] = junkai.thuctap_sang_19
                if infor_nhatky_19_1:
                    table_nhatky.append(infor_nhatky_19_1)

            # Ngay thu _20
            if junkai.ngay_nghi_20 == False:
                if junkai.thuctap_sang_20 == False and junkai.thuctap_chieu_20 == False:
                    infor_nhatky_20_1 = {}
                    infor_nhatky_20_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_20
                    infor_nhatky_20_1['bc_0164'] = ''
                    if infor_nhatky_20_1:
                        table_nhatky.append(infor_nhatky_20_1)
                else:
                    infor_nhatky_20_1 = {}
                    if junkai.thuctap_sang_20:
                        infor_nhatky_20_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_20 if junkai.ngay_thu_20 else ''
                        infor_nhatky_20_1['bc_0164'] = kiemtra(junkai.thuctap_sang_20)
                        infor_nhatky_20_1['bc_0165'] = junkai.noidung_sang_20.ten_noidung.strip()[
                            0] if junkai.noidung_sang_20 else ''
                        infor_nhatky_20_1['bc_0166'] = kiemtra(junkai.daotao_sang_20)
                        infor_nhatky_20_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_20.ten_han_nhanvien)
                    if infor_nhatky_20_1:
                        table_nhatky.append(infor_nhatky_20_1)

                    infor_nhatky_20_2 = {}
                    if junkai.thuctap_chieu_20:
                        infor_nhatky_20_2['bc_0161'] = ''
                        infor_nhatky_20_2['bc_0164'] = junkai.thuctap_chieu_20
                        infor_nhatky_20_2['bc_0165'] = kiemtra(junkai.noidung_chieu_20.ten_noidung.strip()[0])
                        infor_nhatky_20_2['bc_0166'] = kiemtra(junkai.daotao_chieu_20)
                        infor_nhatky_20_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_20.ten_han_nhanvien)
                    if infor_nhatky_20_2:
                        table_nhatky.append(infor_nhatky_20_2)
            else:

                infor_nhatky_20_1 = {}
                if junkai.thuctap_sang_20:
                    infor_nhatky_20_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_20
                    infor_nhatky_20_1['bc_0164'] = junkai.thuctap_sang_20
                if infor_nhatky_20_1:
                    table_nhatky.append(infor_nhatky_20_1)

            # Ngay thu _21
            if junkai.ngay_nghi_21 == False:
                if junkai.thuctap_sang_21 == False and junkai.thuctap_chieu_21 == False:
                    infor_nhatky_21_1 = {}
                    infor_nhatky_21_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_21
                    infor_nhatky_21_1['bc_0164'] = ''
                    if infor_nhatky_21_1:
                        table_nhatky.append(infor_nhatky_21_1)
                else:
                    infor_nhatky_21_1 = {}
                    if junkai.thuctap_sang_21:
                        infor_nhatky_21_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_21 if junkai.ngay_thu_21 else ''
                        infor_nhatky_21_1['bc_0164'] = kiemtra(junkai.thuctap_sang_21)
                        infor_nhatky_21_1['bc_0165'] = junkai.noidung_sang_21.ten_noidung.strip()[
                            0] if junkai.noidung_sang_21 else ''
                        infor_nhatky_21_1['bc_0166'] = kiemtra(junkai.daotao_sang_21)
                        infor_nhatky_21_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_21.ten_han_nhanvien)
                    if infor_nhatky_21_1:
                        table_nhatky.append(infor_nhatky_21_1)

                    infor_nhatky_21_2 = {}
                    if junkai.thuctap_chieu_21:
                        infor_nhatky_21_2['bc_0161'] = ''
                        infor_nhatky_21_2['bc_0164'] = junkai.thuctap_chieu_21
                        infor_nhatky_21_2['bc_0165'] = kiemtra(junkai.noidung_chieu_21.ten_noidung.strip()[0])
                        infor_nhatky_21_2['bc_0166'] = kiemtra(junkai.daotao_chieu_21)
                        infor_nhatky_21_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_21.ten_han_nhanvien)
                    if infor_nhatky_21_2:
                        table_nhatky.append(infor_nhatky_21_2)
            else:

                infor_nhatky_21_1 = {}
                if junkai.thuctap_sang_21:
                    infor_nhatky_21_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_21
                    infor_nhatky_21_1['bc_0164'] = junkai.thuctap_sang_21
                if infor_nhatky_21_1:
                    table_nhatky.append(infor_nhatky_21_1)

            # Ngay thu _22
            if junkai.ngay_nghi_22 == False:
                if junkai.thuctap_sang_22 == False and junkai.thuctap_chieu_22 == False:
                    infor_nhatky_22_1 = {}
                    infor_nhatky_22_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_22
                    infor_nhatky_22_1['bc_0164'] = ''
                    if infor_nhatky_22_1:
                        table_nhatky.append(infor_nhatky_22_1)
                else:
                    infor_nhatky_22_1 = {}
                    if junkai.thuctap_sang_22:
                        infor_nhatky_22_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_22 if junkai.ngay_thu_22 else ''
                        infor_nhatky_22_1['bc_0164'] = kiemtra(junkai.thuctap_sang_22)
                        infor_nhatky_22_1['bc_0165'] = junkai.noidung_sang_22.ten_noidung.strip()[
                            0] if junkai.noidung_sang_22 else ''
                        infor_nhatky_22_1['bc_0166'] = kiemtra(junkai.daotao_sang_22)
                        infor_nhatky_22_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_22.ten_han_nhanvien)
                    if infor_nhatky_22_1:
                        table_nhatky.append(infor_nhatky_22_1)

                    infor_nhatky_22_2 = {}
                    if junkai.thuctap_chieu_22:
                        infor_nhatky_22_2['bc_0161'] = ''
                        infor_nhatky_22_2['bc_0164'] = junkai.thuctap_chieu_22
                        infor_nhatky_22_2['bc_0165'] = kiemtra(junkai.noidung_chieu_22.ten_noidung.strip()[0])
                        infor_nhatky_22_2['bc_0166'] = kiemtra(junkai.daotao_chieu_22)
                        infor_nhatky_22_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_22.ten_han_nhanvien)
                    if infor_nhatky_22_2:
                        table_nhatky.append(infor_nhatky_22_2)
            else:

                infor_nhatky_22_1 = {}
                if junkai.thuctap_sang_22:
                    infor_nhatky_22_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_22
                    infor_nhatky_22_1['bc_0164'] = junkai.thuctap_sang_22
                if infor_nhatky_22_1:
                    table_nhatky.append(infor_nhatky_22_1)

            # Ngay thu _23
            if junkai.ngay_nghi_23 == False:
                if junkai.thuctap_sang_23 == False and junkai.thuctap_chieu_23 == False:
                    infor_nhatky_23_1 = {}
                    infor_nhatky_23_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_23
                    infor_nhatky_23_1['bc_0164'] = ''
                    if infor_nhatky_23_1:
                        table_nhatky.append(infor_nhatky_23_1)
                else:
                    infor_nhatky_23_1 = {}
                    if junkai.thuctap_sang_23:
                        infor_nhatky_23_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_23 if junkai.ngay_thu_23 else ''
                        infor_nhatky_23_1['bc_0164'] = kiemtra(junkai.thuctap_sang_23)
                        infor_nhatky_23_1['bc_0165'] = junkai.noidung_sang_23.ten_noidung.strip()[
                            0] if junkai.noidung_sang_23 else ''
                        infor_nhatky_23_1['bc_0166'] = kiemtra(junkai.daotao_sang_23)
                        infor_nhatky_23_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_23.ten_han_nhanvien)
                    if infor_nhatky_23_1:
                        table_nhatky.append(infor_nhatky_23_1)

                    infor_nhatky_23_2 = {}
                    if junkai.thuctap_chieu_23:
                        infor_nhatky_23_2['bc_0161'] = ''
                        infor_nhatky_23_2['bc_0164'] = junkai.thuctap_chieu_23
                        infor_nhatky_23_2['bc_0165'] = kiemtra(junkai.noidung_chieu_23.ten_noidung.strip()[0])
                        infor_nhatky_23_2['bc_0166'] = kiemtra(junkai.daotao_chieu_23)
                        infor_nhatky_23_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_23.ten_han_nhanvien)
                    if infor_nhatky_23_2:
                        table_nhatky.append(infor_nhatky_23_2)
            else:

                infor_nhatky_23_1 = {}
                if junkai.thuctap_sang_23:
                    infor_nhatky_23_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_23
                    infor_nhatky_23_1['bc_0164'] = junkai.thuctap_sang_23
                if infor_nhatky_23_1:
                    table_nhatky.append(infor_nhatky_23_1)

            # Ngay thu _24
            if junkai.ngay_nghi_24 == False:
                if junkai.thuctap_sang_24 == False and junkai.thuctap_chieu_24 == False:
                    infor_nhatky_24_1 = {}
                    infor_nhatky_24_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_24
                    infor_nhatky_24_1['bc_0164'] = ''
                    if infor_nhatky_24_1:
                        table_nhatky.append(infor_nhatky_24_1)
                else:
                    infor_nhatky_24_1 = {}
                    if junkai.thuctap_sang_24:
                        infor_nhatky_24_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_24 if junkai.ngay_thu_24 else ''
                        infor_nhatky_24_1['bc_0164'] = kiemtra(junkai.thuctap_sang_24)
                        infor_nhatky_24_1['bc_0165'] = junkai.noidung_sang_24.ten_noidung.strip()[
                            0] if junkai.noidung_sang_24 else ''
                        infor_nhatky_24_1['bc_0166'] = kiemtra(junkai.daotao_sang_24)
                        infor_nhatky_24_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_24.ten_han_nhanvien)
                    if infor_nhatky_24_1:
                        table_nhatky.append(infor_nhatky_24_1)

                    infor_nhatky_24_2 = {}
                    if junkai.thuctap_chieu_24:
                        infor_nhatky_24_2['bc_0161'] = ''
                        infor_nhatky_24_2['bc_0164'] = junkai.thuctap_chieu_24
                        infor_nhatky_24_2['bc_0165'] = kiemtra(junkai.noidung_chieu_24.ten_noidung.strip()[0])
                        infor_nhatky_24_2['bc_0166'] = kiemtra(junkai.daotao_chieu_24)
                        infor_nhatky_24_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_24.ten_han_nhanvien)
                    if infor_nhatky_24_2:
                        table_nhatky.append(infor_nhatky_24_2)
            else:

                infor_nhatky_24_1 = {}
                if junkai.thuctap_sang_24:
                    infor_nhatky_24_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_24
                    infor_nhatky_24_1['bc_0164'] = junkai.thuctap_sang_24
                if infor_nhatky_24_1:
                    table_nhatky.append(infor_nhatky_24_1)

            # Ngay thu _25
            if junkai.ngay_nghi_25 == False:
                if junkai.thuctap_sang_25 == False and junkai.thuctap_chieu_25 == False:
                    infor_nhatky_25_1 = {}
                    infor_nhatky_25_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_25
                    infor_nhatky_25_1['bc_0164'] = ''
                    if infor_nhatky_25_1:
                        table_nhatky.append(infor_nhatky_25_1)
                else:
                    infor_nhatky_25_1 = {}
                    if junkai.thuctap_sang_25:
                        infor_nhatky_25_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_25 if junkai.ngay_thu_25 else ''
                        infor_nhatky_25_1['bc_0164'] = kiemtra(junkai.thuctap_sang_25)
                        infor_nhatky_25_1['bc_0165'] = junkai.noidung_sang_25.ten_noidung.strip()[
                            0] if junkai.noidung_sang_25 else ''
                        infor_nhatky_25_1['bc_0166'] = kiemtra(junkai.daotao_sang_25)
                        infor_nhatky_25_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_25.ten_han_nhanvien)
                    if infor_nhatky_25_1:
                        table_nhatky.append(infor_nhatky_25_1)

                    infor_nhatky_25_2 = {}
                    if junkai.thuctap_chieu_25:
                        infor_nhatky_25_2['bc_0161'] = ''
                        infor_nhatky_25_2['bc_0164'] = junkai.thuctap_chieu_25
                        infor_nhatky_25_2['bc_0165'] = kiemtra(junkai.noidung_chieu_25.ten_noidung.strip()[0])
                        infor_nhatky_25_2['bc_0166'] = kiemtra(junkai.daotao_chieu_25)
                        infor_nhatky_25_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_25.ten_han_nhanvien)
                    if infor_nhatky_25_2:
                        table_nhatky.append(infor_nhatky_25_2)
            else:

                infor_nhatky_25_1 = {}
                if junkai.thuctap_sang_25:
                    infor_nhatky_25_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_25
                    infor_nhatky_25_1['bc_0164'] = junkai.thuctap_sang_25
                if infor_nhatky_25_1:
                    table_nhatky.append(infor_nhatky_25_1)

            # Ngay thu _26
            if junkai.ngay_nghi_26 == False:
                if junkai.thuctap_sang_26 == False and junkai.thuctap_chieu_26 == False:
                    infor_nhatky_26_1 = {}
                    infor_nhatky_26_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_26
                    infor_nhatky_26_1['bc_0164'] = ''
                    if infor_nhatky_26_1:
                        table_nhatky.append(infor_nhatky_26_1)
                else:
                    infor_nhatky_26_1 = {}
                    if junkai.thuctap_sang_26:
                        infor_nhatky_26_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_26 if junkai.ngay_thu_26 else ''
                        infor_nhatky_26_1['bc_0164'] = kiemtra(junkai.thuctap_sang_26)
                        infor_nhatky_26_1['bc_0165'] = junkai.noidung_sang_26.ten_noidung.strip()[
                            0] if junkai.noidung_sang_26 else ''
                        infor_nhatky_26_1['bc_0166'] = kiemtra(junkai.daotao_sang_26)
                        infor_nhatky_26_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_26.ten_han_nhanvien)
                    if infor_nhatky_26_1:
                        table_nhatky.append(infor_nhatky_26_1)

                    infor_nhatky_26_2 = {}
                    if junkai.thuctap_chieu_26:
                        infor_nhatky_26_2['bc_0161'] = ''
                        infor_nhatky_26_2['bc_0164'] = junkai.thuctap_chieu_26
                        infor_nhatky_26_2['bc_0165'] = kiemtra(junkai.noidung_chieu_26.ten_noidung.strip()[0])
                        infor_nhatky_26_2['bc_0166'] = kiemtra(junkai.daotao_chieu_26)
                        infor_nhatky_26_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_26.ten_han_nhanvien)
                    if infor_nhatky_26_2:
                        table_nhatky.append(infor_nhatky_26_2)
            else:

                infor_nhatky_26_1 = {}
                if junkai.thuctap_sang_26:
                    infor_nhatky_26_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_26
                    infor_nhatky_26_1['bc_0164'] = junkai.thuctap_sang_26
                if infor_nhatky_26_1:
                    table_nhatky.append(infor_nhatky_26_1)

            # Ngay thu _27
            if junkai.ngay_nghi_27 == False:
                if junkai.thuctap_sang_27 == False and junkai.thuctap_chieu_27 == False:
                    infor_nhatky_27_1 = {}
                    infor_nhatky_27_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_27
                    infor_nhatky_27_1['bc_0164'] = ''
                    if infor_nhatky_27_1:
                        table_nhatky.append(infor_nhatky_27_1)
                else:
                    infor_nhatky_27_1 = {}
                    if junkai.thuctap_sang_27:
                        infor_nhatky_27_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_27 if junkai.ngay_thu_27 else ''
                        infor_nhatky_27_1['bc_0164'] = kiemtra(junkai.thuctap_sang_27)
                        infor_nhatky_27_1['bc_0165'] = junkai.noidung_sang_27.ten_noidung.strip()[
                            0] if junkai.noidung_sang_27 else ''
                        infor_nhatky_27_1['bc_0166'] = kiemtra(junkai.daotao_sang_27)
                        infor_nhatky_27_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_27.ten_han_nhanvien)
                    if infor_nhatky_27_1:
                        table_nhatky.append(infor_nhatky_27_1)

                    infor_nhatky_27_2 = {}
                    if junkai.thuctap_chieu_27:
                        infor_nhatky_27_2['bc_0161'] = ''
                        infor_nhatky_27_2['bc_0164'] = junkai.thuctap_chieu_27
                        infor_nhatky_27_2['bc_0165'] = kiemtra(junkai.noidung_chieu_27.ten_noidung.strip()[0])
                        infor_nhatky_27_2['bc_0166'] = kiemtra(junkai.daotao_chieu_27)
                        infor_nhatky_27_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_27.ten_han_nhanvien)
                    if infor_nhatky_27_2:
                        table_nhatky.append(infor_nhatky_27_2)
            else:

                infor_nhatky_27_1 = {}
                if junkai.thuctap_sang_27:
                    infor_nhatky_27_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_27
                    infor_nhatky_27_1['bc_0164'] = junkai.thuctap_sang_27
                if infor_nhatky_27_1:
                    table_nhatky.append(infor_nhatky_27_1)

            # Ngay thu _28
            if junkai.ngay_nghi_28 == False:
                if junkai.thuctap_sang_28 == False and junkai.thuctap_chieu_28 == False:
                    infor_nhatky_28_1 = {}
                    infor_nhatky_28_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_28
                    infor_nhatky_28_1['bc_0164'] = ''
                    if infor_nhatky_28_1:
                        table_nhatky.append(infor_nhatky_28_1)
                else:
                    infor_nhatky_28_1 = {}
                    if junkai.thuctap_sang_28:
                        infor_nhatky_28_1[
                            'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_28 if junkai.ngay_thu_28 else ''
                        infor_nhatky_28_1['bc_0164'] = kiemtra(junkai.thuctap_sang_28)
                        infor_nhatky_28_1['bc_0165'] = junkai.noidung_sang_28.ten_noidung.strip()[
                            0] if junkai.noidung_sang_28 else ''
                        infor_nhatky_28_1['bc_0166'] = kiemtra(junkai.daotao_sang_28)
                        infor_nhatky_28_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_28.ten_han_nhanvien)
                    if infor_nhatky_28_1:
                        table_nhatky.append(infor_nhatky_28_1)

                    infor_nhatky_28_2 = {}
                    if junkai.thuctap_chieu_28:
                        infor_nhatky_28_2['bc_0161'] = ''
                        infor_nhatky_28_2['bc_0164'] = junkai.thuctap_chieu_28
                        infor_nhatky_28_2['bc_0165'] = kiemtra(junkai.noidung_chieu_28.ten_noidung.strip()[0])
                        infor_nhatky_28_2['bc_0166'] = kiemtra(junkai.daotao_chieu_28)
                        infor_nhatky_28_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_28.ten_han_nhanvien)
                    if infor_nhatky_28_2:
                        table_nhatky.append(infor_nhatky_28_2)
            else:

                infor_nhatky_28_1 = {}
                if junkai.thuctap_sang_28:
                    infor_nhatky_28_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_28
                    infor_nhatky_28_1['bc_0164'] = junkai.thuctap_sang_28
                if infor_nhatky_28_1:
                    table_nhatky.append(infor_nhatky_28_1)

            # Ngay thu _29
            if junkai.ngay_thu_29:
                if junkai.ngay_nghi_29 == False:
                    if junkai.thuctap_sang_29 == False and junkai.thuctap_chieu_29 == False:
                        infor_nhatky_29_1 = {}
                        infor_nhatky_29_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_29
                        infor_nhatky_29_1['bc_0164'] = ''
                        if infor_nhatky_29_1:
                            table_nhatky.append(infor_nhatky_29_1)
                    else:
                        infor_nhatky_29_1 = {}
                        if junkai.thuctap_sang_29:
                            infor_nhatky_29_1[
                                'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_29 if junkai.ngay_thu_29 else ''
                            infor_nhatky_29_1['bc_0164'] = kiemtra(junkai.thuctap_sang_29)
                            infor_nhatky_29_1['bc_0165'] = junkai.noidung_sang_29.ten_noidung.strip()[
                                0] if junkai.noidung_sang_29 else ''
                            infor_nhatky_29_1['bc_0166'] = kiemtra(junkai.daotao_sang_29)
                            infor_nhatky_29_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_29.ten_han_nhanvien)
                        if infor_nhatky_29_1:
                            table_nhatky.append(infor_nhatky_29_1)

                        infor_nhatky_29_2 = {}
                        if junkai.thuctap_chieu_29:
                            infor_nhatky_29_2['bc_0161'] = ''
                            infor_nhatky_29_2['bc_0164'] = junkai.thuctap_chieu_29
                            infor_nhatky_29_2['bc_0165'] = kiemtra(junkai.noidung_chieu_29.ten_noidung.strip()[0])
                            infor_nhatky_29_2['bc_0166'] = kiemtra(junkai.daotao_chieu_29)
                            infor_nhatky_29_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_29.ten_han_nhanvien)
                        if infor_nhatky_29_2:
                            table_nhatky.append(infor_nhatky_29_2)
                else:

                    infor_nhatky_29_1 = {}
                    if junkai.thuctap_sang_29:
                        infor_nhatky_29_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_29
                        infor_nhatky_29_1['bc_0164'] = junkai.thuctap_sang_29
                    if infor_nhatky_29_1:
                        table_nhatky.append(infor_nhatky_29_1)

            # Ngay thu _30
            if junkai.ngay_thu_30:
                if junkai.ngay_nghi_30 == False:
                    if junkai.thuctap_sang_30 == False and junkai.thuctap_chieu_30 == False:
                        infor_nhatky_30_1 = {}
                        infor_nhatky_30_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_30
                        infor_nhatky_30_1['bc_0164'] = ''
                        if infor_nhatky_30_1:
                            table_nhatky.append(infor_nhatky_30_1)
                    else:
                        infor_nhatky_30_1 = {}
                        if junkai.thuctap_sang_30:
                            infor_nhatky_30_1[
                                'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_30 if junkai.ngay_thu_30 else ''
                            infor_nhatky_30_1['bc_0164'] = kiemtra(junkai.thuctap_sang_30)
                            infor_nhatky_30_1['bc_0165'] = junkai.noidung_sang_30.ten_noidung.strip()[
                                0] if junkai.noidung_sang_30 else ''
                            infor_nhatky_30_1['bc_0166'] = kiemtra(junkai.daotao_sang_30)
                            infor_nhatky_30_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_30.ten_han_nhanvien)
                        if infor_nhatky_30_1:
                            table_nhatky.append(infor_nhatky_30_1)

                        infor_nhatky_30_2 = {}
                        if junkai.thuctap_chieu_30:
                            infor_nhatky_30_2['bc_0161'] = ''
                            infor_nhatky_30_2['bc_0164'] = junkai.thuctap_chieu_30
                            infor_nhatky_30_2['bc_0165'] = kiemtra(junkai.noidung_chieu_30.ten_noidung.strip()[0])
                            infor_nhatky_30_2['bc_0166'] = kiemtra(junkai.daotao_chieu_30)
                            infor_nhatky_30_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_30.ten_han_nhanvien)
                        if infor_nhatky_30_2:
                            table_nhatky.append(infor_nhatky_30_2)
                else:
                    infor_nhatky_30_1 = {}
                    if junkai.thuctap_sang_30:
                        infor_nhatky_30_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_30
                        infor_nhatky_30_1['bc_0164'] = junkai.thuctap_sang_30
                    if infor_nhatky_30_1:
                        table_nhatky.append(infor_nhatky_30_1)

            # Ngay thu _31
            if junkai.ngay_thu_31:
                if junkai.ngay_nghi_31 == False:
                    if junkai.thuctap_sang_31 == False and junkai.thuctap_chieu_31 == False:
                        infor_nhatky_31_1 = {}
                        infor_nhatky_31_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_31
                        infor_nhatky_31_1['bc_0164'] = ''
                        if infor_nhatky_31_1:
                            table_nhatky.append(infor_nhatky_31_1)
                    else:
                        infor_nhatky_31_1 = {}
                        if junkai.thuctap_sang_31:
                            infor_nhatky_31_1[
                                'bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_31 if junkai.ngay_thu_31 else ''
                            infor_nhatky_31_1['bc_0164'] = kiemtra(junkai.thuctap_sang_31)
                            infor_nhatky_31_1['bc_0165'] = junkai.noidung_sang_31.ten_noidung.strip()[
                                0] if junkai.noidung_sang_31 else ''
                            infor_nhatky_31_1['bc_0166'] = kiemtra(junkai.daotao_sang_31)
                            infor_nhatky_31_1['hhjp_0064'] = kiemtra(junkai.chidao_sang_31.ten_han_nhanvien)
                        if infor_nhatky_31_1:
                            table_nhatky.append(infor_nhatky_31_1)

                        infor_nhatky_31_2 = {}
                        if junkai.thuctap_chieu_31:
                            infor_nhatky_31_2['bc_0161'] = ''
                            infor_nhatky_31_2['bc_0164'] = junkai.thuctap_chieu_31
                            infor_nhatky_31_2['bc_0165'] = kiemtra(junkai.noidung_chieu_31.ten_noidung.strip()[0])
                            infor_nhatky_31_2['bc_0166'] = kiemtra(junkai.daotao_chieu_31)
                            infor_nhatky_31_2['hhjp_0064'] = kiemtra(junkai.chidao_chieu_31.ten_han_nhanvien)
                        if infor_nhatky_31_2:
                            table_nhatky.append(infor_nhatky_31_2)
                else:

                    infor_nhatky_31_1 = {}
                    if junkai.thuctap_sang_31:
                        infor_nhatky_31_1['bc_0161'] = junkai.thang_lich_junkai + "/" + junkai.ngay_thu_31
                        infor_nhatky_31_1['bc_0164'] = junkai.thuctap_sang_31
                    if infor_nhatky_31_1:
                        table_nhatky.append(infor_nhatky_31_1)

            context['tbl_nhatky'] = table_nhatky

            table_tts = []
            for thuctapsinh_hoso in junkai.thuctapsinh_junkai:
                infor_tts = {}
                infor_tts['hhjp_0001'] = kiemtra(thuctapsinh_hoso.ten_lt_tts)
                infor_tts['hhjp_0113'] = kiemtra(thuctapsinh_hoso.daotao_hientai.name_kehoachdaotao)

                if junkai.giaidoan_junkai == '1 go':
                    if thuctapsinh_hoso.batdau_thucte_namnhat:
                        infor_tts['bc_0167'] = convert_date(thuctapsinh_hoso.batdau_thucte_namnhat)
                        infor_tts['bc_0168'] = convert_date(thuctapsinh_hoso.ketthuc_thucte_namnhat)
                    else:
                        infor_tts['bc_0167'] = convert_date(thuctapsinh_hoso.batdau_kehoach_namnhat)
                        infor_tts['bc_0168'] = convert_date(thuctapsinh_hoso.ketthuc_kehoach_namnhat)

                elif junkai.giaidoan_junkai == '2 go':
                    if thuctapsinh_hoso.batdau_thucte_namhai:
                        infor_tts['bc_0167'] = convert_date(thuctapsinh_hoso.batdau_thucte_namhai)
                        infor_tts['bc_0168'] = convert_date(thuctapsinh_hoso.ketthuc_thucte_namhai)
                    else:
                        infor_tts['bc_0167'] = convert_date(thuctapsinh_hoso.batdau_kehoach_namhai)
                        infor_tts['bc_0168'] = convert_date(thuctapsinh_hoso.ketthuc_kehoach_namhai)

                elif junkai.giaidoan_junkai == '3 go':
                    if thuctapsinh_hoso.batdau_thucte_namba:
                        infor_tts['bc_0167'] = convert_date(thuctapsinh_hoso.batdau_thucte_namba)
                        infor_tts['bc_0168'] = convert_date(thuctapsinh_hoso.ketthuc_thucte_namba)
                    else:
                        infor_tts['bc_0167'] = convert_date(thuctapsinh_hoso.batdau_kehoach_namba)
                        infor_tts['bc_0168'] = convert_date(thuctapsinh_hoso.ketthuc_kehoach_namba)

                infor_tts['hhjp_0599'] = kiemtra(thuctapsinh_hoso.thongtinkhac_tts)
                table_tts.append(infor_tts)
            context['tbl_tts'] = table_tts

            context['hhjp_0193'] = convert_date(junkai.ngaytao_junkai)

            context['hhjp_0010'] = kiemtra(junkai.xinghiep_junkai.ten_han_xnghiep)
            context['hhjp_0016_han'] = kiemtra(junkai.xinghiep_junkai.daidien_han_xnghiep)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None
