# -*- coding: utf-8 -*-

from odoo import models, fields, api
import logging
from tempfile import TemporaryFile, NamedTemporaryFile
from io import BytesIO, StringIO
import codecs
from docxtpl import DocxTemplate

_logger = logging.getLogger(__name__)


def kiemtra(data):
    if data:
        return data
    else:
        return ''


def convert_date(self):
    if self:
        return str(self.year) + '年' + \
               str(self.month) + '月' + \
               str(self.day) + '日'
    else:
        return '年　月　日'


class BaoKansa(models.Model):
    _name = 'baocao.kansa'
    _rec_name = 'xinghiep_kansa'

    # 1. Thông tin giám sát
    daygiamsat_kansa = fields.Date(string='Ngày thực hiện giám sát', default=fields.Date.today())  # bc_0007
    daybaocao_kansa = fields.Date(string='Ngày báo cáo', default=fields.Date.today())
    sobaocao_kansa = fields.Char(string='Số chấp nhận báo cáo')

    @api.model
    def _default_trachnhiem_quanly_kansa(self):
        return self.env['nghiepdoan.nghiepdoan'].search([('id', '=', 1)], limit=1)

    cosogiamsat_kansa = fields.Many2one(comodel_name='nghiepdoan.chinhanh', string='Cơ sở giám sát',required=True)

    # cosogiamsat_nghiepdoan = fields.Many2one(comodel_name='nghiepdoan.nghiepdoan', string='Cơ sở giám sát', limit=1)

    @api.onchange('cosogiamsat_kansa')
    def onchange_master_cosogiamsat_nghiepdoan(self):
        if self.cosogiamsat_kansa:
            cosogiamsat_nghiepdoan = self.env['nghiepdoan.nghiepdoan'].search([('id', '=', 1)], limit=1)
            self.giayphepnghiepdoan_kansa = cosogiamsat_nghiepdoan.so_giayphep_nghiepdoan

    vanphonggiamsat_kansa = fields.Char(string='Mã số chi nhánh nghiệp đoàn')  # bc_0001
    giayphepnghiepdoan_kansa = fields.Char(string='Số giấy phép kinh doanh của nghiệp đoàn')
    # 2. Thực tập sinh
    xinghiep_kansa = fields.Many2one(comodel_name='xinghiep.xinghiep', string='Xí nghiệp',required=True)

    @api.onchange('xinghiep_kansa')
    def onchange_method_xinghiep_kansa(self):
        if self.xinghiep_kansa != ():
            self.sopheduyet_kynangtts_kansa = self.xinghiep_kansa.so_chapnhan_daotao
            self.diachi_xinghiep__kansa = self.xinghiep_kansa.diachi_han_xnghiep
            self.kynang_1_kansa = self.xinghiep_kansa.ttskn_first
            self.kynang_2_kansa = self.xinghiep_kansa.ttskn_second
            self.kynang_3_kansa = self.xinghiep_kansa.ttskn_third
            self.kynang_tong_kansa = self.xinghiep_kansa.ttskn_first + self.xinghiep_kansa.ttskn_second + self.xinghiep_kansa.ttskn_third
            self.thuctapsinh_kansa = None
            # return {'domain': {'thuctapsinh_kansa': [('xinghiep_thuctapsinh', '=', self.xinghiep_kansa.id)]}}

    sopheduyet_kynangtts_kansa = fields.Char(string='Số phê duyệt thực tập kĩ năng của xí nghiệp')
    diachi_xinghiep__kansa = fields.Char(string='Địa chỉ xí nghiệp')

    kynang_1_kansa = fields.Integer(string='TTS kỹ năng số 1')  # bc_0003
    kynang_2_kansa = fields.Integer(string='TTS kỹ năng số 2')  # bc_0004
    kynang_3_kansa = fields.Integer(string='TTS kỹ năng số 3')  # bc_0005

    kynang_tong_kansa = fields.Integer(string='Tổng thực tập sinh kỹ năng', compute='_kynang_tong_kansa')  # bc_0002

    @api.onchange('xinghiep_kansa')
    def onchange_method_xinghiep_kansa_2(self):
        if self.xinghiep_kansa == ():
            self.sopheduyet_kynangtts_kansa = ''
            self.diachi_xinghiep__kansa = ''
            self.kynang_1_kansa = ''
            self.kynang_2_kansa = ''
            self.kynang_3_kansa = ''
            self.kynang_tong_kansa = ''

    @api.multi
    @api.depends('kynang_1_kansa', 'kynang_2_kansa', 'kynang_3_kansa')
    def _kynang_tong_kansa(self):
        if self.kynang_1_kansa or self.kynang_2_kansa or self.kynang_3_kansa:
            self.kynang_tong_kansa = self.kynang_1_kansa + self.kynang_2_kansa + self.kynang_3_kansa

    # 3. Nhân viên liên quan

    trachnhiem_kansa = fields.Many2many(comodel_name='nhanvien.nhanvien', relation='trachnhiem_kansa',
                                        string='Người chịu trách nhiệm thục tập', compute='onchange_xinghiep_kansa')

    thuctap_kansa = fields.Many2many(comodel_name='nhanvien.nhanvien', relation='thuctap_kansa',
                                     string='Người chỉ đạo thực tập', compute='onchange_xinghiep_kansa')

    doisong_kansa = fields.Many2many(comodel_name='nghiepdoan.nhanvientts',
                                     string='Người chịu trách nhiệm Kansa',
                                     compute='onchange_xinghiep_kansa')  # bc_0008

    @api.onchange('xinghiep_kansa')
    def onchange_xinghiep_kansa(self):
        huongdanthuctap = self.env['nhanvien.nhanvien'].search(
            [('nhanvien_xinghiep', '=', self.xinghiep_kansa.id), ('vaitro_three', '=', True)])
        chidaothuctap = self.env['nhanvien.nhanvien'].search(
            [('nhanvien_xinghiep', '=', self.xinghiep_kansa.id), ('vaitro_six', '=', True)])

        chiutrachnhiemKansa = self.env['nghiepdoan.nhanvientts'].search(
            [('danhsach_nhanvien_tts', '=', 1), ('quanly_vaitro_nghiepdoan', '=', True)])

        if self.xinghiep_kansa:
            self.trachnhiem_kansa = huongdanthuctap
            self.thuctap_kansa = chidaothuctap
            self.doisong_kansa = chiutrachnhiemKansa

    # related='xinghiep_kansa.danhsach_nhanvien',
    # domain="[('vaitro_five', '=', True)]")
    troly_kansa = fields.One2many(comodel_name='baocao.troly', inverse_name='troly_kansa',
                                  string='Trợ lý')  # bc_0009
    # 4. Trụ sở xác nhận thực địa
    noithuctap_truso_kansa = fields.Many2one(comodel_name='xinghiep.chinhanh', string='Nơi thực tập')  # bc_0010

    diadiem_thuctap = fields.Selection(string='実習実施場所', selection=[('事業所と同じ', '事業所と同じ'), ('その他', 'その他')],
                                       default='事業所と同じ')

    diachi_truso_kansa = fields.Char(string='Địa chỉ')

    @api.onchange('noithuctap_truso_kansa')
    def onchange_noithuctap_truso_kansa(self):
        self.diachi_truso_kansa = self.noithuctap_truso_kansa.diachi_chinhanh

    @api.onchange('diadiem_thuctap')
    def onchange_diadiem_thuctap(self):
        if self.diadiem_thuctap == '事業所と同じ':
            self.diachi_truso_kansa = self.noithuctap_truso_kansa.diachi_chinhanh
        else:
            self.diachi_truso_kansa = ''

    nha_kansa = fields.One2many(comodel_name='baocao.nhao', inverse_name='nha_kansa',
                                string='Nhà ở trong quá trình thực tập')
    baocao_thuctap_kansa = fields.Selection(string='Báo cáo từ người chỉ đạo thực tập và hướng dẫn thực tập',
                                            selection=[('Có', 'Có'), ('Không', 'Không')], default='Có')

    ngaydi_cokhong = fields.Selection(string='従前の監査の実施', selection=[('有', '有'), ('無', '無')], default='無')
    ngaydi_kansagan = fields.Date(string="従前の監査の実施")

    trachnhiem_quanly_kansa = fields.Many2one('nghiepdoan.nghiepdoan', string="Nghiệp đoàn",
                                              default=_default_trachnhiem_quanly_kansa)

    thietbi_kansa = fields.Selection(string='Xác nhận thiết bị và tài liệu',
                                     selection=[('Có', 'Có'), ('Không', 'Không')], default='Có')  # bc_0015
    choo_kansa = fields.Selection(string='Xác nhận chỗ ở và môi trường sống',
                                  selection=[('Có', 'Có'), ('Không', 'Không')], default='Có')  # bc_0016
    # 5. Danh sách thực tập sinh phỏng vấn
    thuctapsinh_kansa = fields.Many2many(comodel_name='thuctapsinh.thuctapsinh', string='Thực tập sinh phỏng vấn',required=True)

    @api.onchange('thuctapsinh_kansa')
    def onchange_method_thuctapsinh_kansa(self):
        if self.thuctapsinh_kansa:
            so_1 = 0
            so_2 = 0
            so_3 = 0
            for tts_kynang in self.thuctapsinh_kansa:
                if tts_kynang.daotao_hientai.dapan_kehoachdaotao == 'A' or tts_kynang.daotao_hientai.dapan_kehoachdaotao == 'D':
                    so_1 = so_1 + 1
                if tts_kynang.daotao_hientai.dapan_kehoachdaotao == 'B' or tts_kynang.daotao_hientai.dapan_kehoachdaotao == 'E':
                    so_2 = so_2 + 1
                if tts_kynang.daotao_hientai.dapan_kehoachdaotao == 'C' or tts_kynang.daotao_hientai.dapan_kehoachdaotao == 'F':
                    so_3 = so_3 + 1
            self.kynang_1phongvan_kansa = so_1
            self.kynang_2phongvan_kansa = so_2
            self.kynang_3phongvan_kansa = so_3
            self.tong_kynang = so_1 + so_2 + so_2

    kynang_1phongvan_kansa = fields.Integer(string='TTS kỹ năng số 1')  # bc_0011
    kynang_2phongvan_kansa = fields.Integer(string='TTS kỹ năng số 2')  # bc_0012
    kynang_3phongvan_kansa = fields.Integer(string='TTS kỹ năng số 3')  # bc_0013
    tong_kynang = fields.Integer(string='Tổng thực tập sinh kỹ năng')  # bc_0014
    # 6. Khác
    vande_kansa = fields.Text(string='Vấn đề đặc biệt')  # bc_0017
    ketqua_kansa = fields.Text(string='Kết quả kiểm tra')  # bc_0018
    danhgia_kansa = fields.Text(string='Đánh giá hoàn thiện')  # bc_0019
    chuanbi_kansa = fields.Text(string='Chuẩn bị')  # bc_0020
    # 7. Báo cáo Kansa
    ghichu_1 = fields.Char()
    hiendien1_1 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu1_1 = fields.Char()
    hiendien1_2 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu1_2 = fields.Char()
    hiendien1_3 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu1_3 = fields.Char()
    hiendien1_4 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu1_4 = fields.Char()
    hiendien1_5 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu1_5 = fields.Char()
    hiendien1_6 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')])
    ghichu1_6 = fields.Char()

    ghichu_2 = fields.Char()
    hiendien2_1 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu2_1 = fields.Char()
    hiendien2_2 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu2_2 = fields.Char()
    hiendien2_3 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu2_3 = fields.Char()
    hiendien2_4 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu2_4 = fields.Char()
    hiendien2_5 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu2_5 = fields.Char()
    hiendien2_6 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu2_6 = fields.Char()
    hiendien2_7 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu2_7 = fields.Char()
    hiendien2_8 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu2_8 = fields.Char()
    hiendien2_9 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu2_9 = fields.Char()
    hiendien2_10 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu2_10 = fields.Char()
    hiendien2_11 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu2_11 = fields.Char()
    hiendien2_12 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu2_12 = fields.Char()
    hiendien2_13 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu2_13 = fields.Char()
    hiendien2_14 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu2_14 = fields.Char()
    hiendien2_15 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu2_15 = fields.Char()
    hiendien2_16 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu2_16 = fields.Char()
    hiendien2_17 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu2_17 = fields.Char()
    hiendien2_18 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu2_18 = fields.Char()
    hiendien2_19 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu2_19 = fields.Char()
    hiendien2_20 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu2_20 = fields.Char()
    hiendien2_21 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')])
    ghichu2_21 = fields.Char()

    ghichu_3 = fields.Char()
    hiendien3_1 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu3_1 = fields.Char()
    hiendien3_2 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu3_2 = fields.Char()
    hiendien3_3 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu3_3 = fields.Char()
    hiendien3_4 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu3_4 = fields.Char()
    hiendien3_5 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu3_5 = fields.Char()
    hiendien3_6 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu3_6 = fields.Char()
    hiendien3_7 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu3_7 = fields.Char()
    hiendien3_8 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu3_8 = fields.Char()
    hiendien3_9 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu3_9 = fields.Char()
    hiendien3_10 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu3_10 = fields.Char()
    hiendien3_11 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu3_11 = fields.Char()
    hiendien3_12 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu3_12 = fields.Char()
    hiendien3_13 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu3_13 = fields.Char()
    hiendien3_14 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu3_14 = fields.Char()
    hiendien3_15 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu3_15 = fields.Char()
    hiendien3_16 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu3_16 = fields.Char()
    hiendien3_17 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')])
    ghichu3_17 = fields.Char()

    ghichu_4 = fields.Char()
    hiendien4_1 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu4_1 = fields.Char()
    hiendien4_2 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu4_2 = fields.Char()
    hiendien4_3 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu4_3 = fields.Char()
    hiendien4_4 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu4_4 = fields.Char()
    hiendien4_5 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu4_5 = fields.Char()
    hiendien4_6 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu4_6 = fields.Char()
    hiendien4_7 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu4_7 = fields.Char()
    hiendien4_8 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu4_8 = fields.Char()
    hiendien4_9 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu4_9 = fields.Char()
    hiendien4_10 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu4_10 = fields.Char()
    hiendien4_11 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu4_11 = fields.Char()
    hiendien4_12 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu4_12 = fields.Char()
    hiendien4_13 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu4_13 = fields.Char()
    hiendien4_14 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu4_14 = fields.Char()
    hiendien4_15 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu4_15 = fields.Char()
    hiendien4_16 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu4_16 = fields.Char()
    hiendien4_17 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu4_17 = fields.Char()
    hiendien4_18 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu4_18 = fields.Char()
    hiendien4_19 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu4_19 = fields.Char()
    hiendien4_20 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu4_20 = fields.Char()
    hiendien4_21 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')])
    ghichu4_21 = fields.Char()

    ghichu_5 = fields.Char()
    hiendien5_1 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu5_1 = fields.Char()
    hiendien5_2 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu5_2 = fields.Char()
    hiendien5_3 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')])
    ghichu5_3 = fields.Char()
    ghichu_6 = fields.Text()
    ghichu_7 = fields.Text()

    thongbao_kansa = fields.Boolean(string='Đặt lịch đi Kansa', default=False)

    @api.multi
    def thongbao_kansa_button_name(self):
        if self.thongbao_kansa == False:
            if self.daygiamsat_kansa:
                day_kansa = []
                day_kansa_row = {}

                day_kansa_row['coso_kansa'] = self.cosogiamsat_kansa.ten_han_nghiepdoan
                day_kansa_row['xinghiep_thongbao'] = self.xinghiep_kansa.ten_han_xnghiep
                day_kansa_row['diachixinghiep_thongbao'] = self.diachi_xinghiep__kansa
                day_kansa_row['start_date'] = self.daygiamsat_kansa
                day_kansa_row['end_date'] = self.daygiamsat_kansa

                junkai = self.env['thongbao.lich'].create(day_kansa_row)
                day_kansa.append(junkai.id)
                [(6, 0, day_kansa)]
                self.thongbao_kansa = True
        elif self.thongbao_kansa == True:
            if self.daygiamsat_kansa:
                self.env["thongbao.lich"].search([('coso_kansa', '=', self.cosogiamsat_kansa.ten_han_nghiepdoan),
                                                  ('start_date', '=', self.daygiamsat_kansa)], limit=1).unlink()

                self.thongbao_kansa = False

    @api.multi
    def baocao_kansa_button(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/kansa/download/%s' % (self.id),
            'target': 'self', }

    @api.multi
    def baocao_kansa_39(self, kansa):
        if (len(kansa.nha_kansa) <= 1) and (len(kansa.thuctapsinh_kansa) <= 5):
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_39")], limit=1)
            print("file_39")
        elif (len(kansa.nha_kansa) > 1) and (len(kansa.thuctapsinh_kansa) <= 5):
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_39_1")], limit=1)
            print("file_39_1")
        elif (len(kansa.nha_kansa) <= 1) and (len(kansa.thuctapsinh_kansa) > 5):
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_39_2")], limit=1)
            print("file_39_2")
        elif (len(kansa.nha_kansa) > 1) and (len(kansa.thuctapsinh_kansa) > 5):
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_39_3")], limit=1)
            print("file_39_3")
        else:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_39")], limit=1)
            print("file_39")

        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)

            cosogiamsat_nghiepdoan = self.env['nghiepdoan.nghiepdoan'].search([('id', '=', 1)], limit=1)
            context = {}
            context['hhjp_0193'] = convert_date(kansa.daybaocao_kansa)
            context['hhjp_0100'] = kiemtra(cosogiamsat_nghiepdoan.so_giayphep_nghiepdoan)
            context['hhjp_0096'] = kiemtra(cosogiamsat_nghiepdoan.ten_phienam_nghiepdoan)
            context['hhjp_0097'] = kiemtra(cosogiamsat_nghiepdoan.ten_han_nghiepdoan)
            context['hhjp_0106'] = kiemtra(kansa.cosogiamsat_kansa.ten_phienam_chinhanh)
            context['hhjp_0107'] = kiemtra(kansa.cosogiamsat_kansa.ten_han_chinhanh)
            context['bc_0001'] = kiemtra(kansa.vanphonggiamsat_kansa)
            context['hhjp_0811'] = kiemtra(kansa.xinghiep_kansa.so_chapnhan_daotao)
            context['hhjp_0009'] = kiemtra(kansa.xinghiep_kansa.ten_furi_xnghiep)
            context['hhjp_0010'] = kiemtra(kansa.xinghiep_kansa.ten_han_xnghiep)
            if kansa.xinghiep_kansa.sobuudien_xnghiep:
                context['bs_0006'] = kansa.xinghiep_kansa.sobuudien_xnghiep[0:3] + '-' + kansa.xinghiep_kansa.sobuudien_xnghiep[3:]
            else:
                context['bs_0006'] = ''
            context['hhjp_0011'] = kiemtra(kansa.xinghiep_kansa.diachi_han_xnghiep)
            context['hhjp_0012'] = kiemtra(kansa.xinghiep_kansa.sdt_xnghiep)
            context['bc_0002'] = kiemtra(kansa.kynang_tong_kansa)
            context['bc_0003'] = kiemtra(kansa.kynang_1_kansa)
            context['bc_0004'] = kiemtra(kansa.kynang_2_kansa)
            context['bc_0005'] = kiemtra(kansa.kynang_3_kansa)

            context['bc_0159'] = kiemtra(cosogiamsat_nghiepdoan.chucvu_daidien_phienam_nghiepdoan)
            context['hhjp_0103'] = kiemtra(cosogiamsat_nghiepdoan.ten_daidien_han_nghiepdoan)

            ten_hannv = ''
            for trachnhiem_kansa in kansa.trachnhiem_kansa:
                ten_hannv += trachnhiem_kansa.ten_han_nhanvien + '       '

            context['hhjp_0048'] = kiemtra(ten_hannv)

            ten_hannvtt = ''
            for thuctap_kansa in kansa.thuctap_kansa:
                ten_hannvtt += thuctap_kansa.ten_han_nhanvien + '       '

            context['hhjp_0064'] = kiemtra(ten_hannvtt)

            if kansa.ngaydi_cokhong == '有':
                docdata.remove_shape(u'id="6" name="Oval 6"')
                context['bc_0006_M'] = kansa.ngaydi_kansagan.month
                context['bc_0006_D'] = kansa.ngaydi_kansagan.day
            elif kansa.ngaydi_cokhong == '無':
                docdata.remove_shape(u'id="5" name="Oval 5"')
                context['bc_0006_M'] = ''
                context['bc_0006_D'] = ''
            else:
                docdata.remove_shape(u'id="5" name="Oval 5"')
                docdata.remove_shape(u'id="6" name="Oval 6"')
                context['bc_0006_M'] = ''
                context['bc_0006_D'] = ''

            if kansa.daybaocao_kansa:
                context['bc_0007'] = str(kansa.daybaocao_kansa.year) + '年' + \
                                     str(kansa.daybaocao_kansa.month) + '月' + \
                                     str(kansa.daybaocao_kansa.day) + '日'
            else:
                context['bc_0007'] = '年　月　日'

            ten_doisong_kansa = ''
            for doisong_kansa in kansa.doisong_kansa:
                ten_doisong_kansa += doisong_kansa.ten_nvien_han_nghiepdoan + '          '
            context['bc_0008'] = kiemtra(ten_doisong_kansa)

            ten_troly_kansa = ''
            for troly_kansa in kansa.troly_kansa:
                ten_troly_kansa += troly_kansa.name_troly + '          '
            context['bc_0009'] = kiemtra(ten_troly_kansa)

            context['hhjp_0041_1'] = kiemtra(kansa.noithuctap_truso_kansa.ten_chinhanh_furl)
            context['hhjp_0042_2'] = kiemtra(kansa.noithuctap_truso_kansa.ten_chinhanh_han)
            context['hhjp_0042_3'] = kiemtra(kansa.diachi_truso_kansa)

            if (len(kansa.nha_kansa) <= 1):
                for item in kansa.nha_kansa:
                    context['hhjp_0157'] = kiemtra(item.tennha)
                    context['hhjp_0158'] = kiemtra(item.diachi)
                    break
            else:
                table_tts = []
                for item in kansa.nha_kansa:
                    infor = {}
                    infor['hhjp_0157'] = kiemtra(item.tennha)
                    infor['hhjp_0158'] = kiemtra(item.diachi)
                    table_tts.append(infor)
                context['tbl_coso'] = table_tts

            if kansa.baocao_thuctap_kansa == 'Có':
                docdata.remove_shape(u'id="8" name="Oval 8"')
            elif kansa.baocao_thuctap_kansa == 'Không':
                docdata.remove_shape(u'id="7" name="Oval 7"')
            else:
                docdata.remove_shape(u'id="7" name="Oval 7"')
                docdata.remove_shape(u'id="8" name="Oval 8"')

            context['bc_0011'] = kiemtra(kansa.kynang_1phongvan_kansa)
            context['bc_0012'] = kiemtra(kansa.kynang_2phongvan_kansa)
            context['bc_0013'] = kiemtra(kansa.kynang_3phongvan_kansa)
            context['bc_0014'] = kiemtra(kansa.tong_kynang)

            if len(kansa.thuctapsinh_kansa) <= 5:
                dem = 1
                for tts in kansa.thuctapsinh_kansa:
                    if tts.sochungnhan_namba:
                        context['hhjp_0911_%d' % dem] = tts.sochungnhan_namba
                    elif tts.sochungnhan_namhai:
                        context['hhjp_0911_%d' % dem] = tts.sochungnhan_namhai
                    elif tts.sochungnhan_namnhat:
                        context['hhjp_0911_%d' % dem] = tts.sochungnhan_namnhat
                    else:
                        context['hhjp_0911_%d' % dem] = ''
                    context['hhjp_0001_%d' % dem] = kiemtra(tts.ten_lt_tts)
                    dem += 1
            else:
                table_tts = []
                for tts in kansa.thuctapsinh_kansa:
                    infor = {}
                    if tts.sochungnhan_namba:
                        infor['hhjp_0911'] = tts.sochungnhan_namba
                    elif tts.sochungnhan_namhai:
                        infor['hhjp_0911'] = tts.sochungnhan_namhai
                    elif tts.sochungnhan_namnhat:
                        infor['hhjp_0911'] = tts.sochungnhan_namnhat
                    else:
                        infor['hhjp_0911'] = ''
                    infor['hhjp_0001'] = kiemtra(tts.ten_lt_tts)
                    table_tts.append(infor)
                context['tbl_tts'] = table_tts

            # context['hhjp_0911']
            if kansa.thietbi_kansa == u'Có':
                docdata.remove_shape(u'id="4" name="Oval 4"')
            elif kansa.thietbi_kansa == u'Không':
                docdata.remove_shape(u'id="3" name="Oval 3"')
            else:
                docdata.remove_shape(u'id="3" name="Oval 3"')
                docdata.remove_shape(u'id="4" name="Oval 4"')

            if kansa.choo_kansa == u'Có':
                docdata.remove_shape(u'id="1" name="Oval 1"')
            elif kansa.choo_kansa == u'Không':
                docdata.remove_shape(u'id="2" name="Oval 2"')
            else:
                docdata.remove_shape(u'id="2" name="Oval 2"')
                docdata.remove_shape(u'id="1" name="Oval 1"')

            context['bc_0017'] = kiemtra(kansa.vande_kansa)
            context['bc_0018'] = kiemtra(kansa.ketqua_kansa)
            context['bc_0019'] = kiemtra(kansa.danhgia_kansa)
            context['bc_0020'] = kiemtra(kansa.chuanbi_kansa)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_kansa_40(self, kansa):
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_40")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            cosogiamsat_nghiepdoan = self.env['nghiepdoan.nghiepdoan'].search([('id', '=', 1)], limit=1)
            context = {}

            if kansa.hiendien1_1 == 'Có':
                docdata.remove_shape(u'id="2" name="Oval 2"')
                context['bc_0022'] = kiemtra(kansa.ghichu1_1)
            else:
                docdata.remove_shape(u'id="1" name="Oval 1"')
                context['bc_0022'] = ''

            if kansa.hiendien1_2 == 'Có':
                docdata.remove_shape(u'id="4" name="Oval 4"')
                context['bc_0024'] = kiemtra(kansa.ghichu1_2)
            else:
                docdata.remove_shape(u'id="3" name="Oval 3"')
                context['bc_0024'] = ''

            if kansa.hiendien1_3 == 'Có':
                docdata.remove_shape(u'id="6" name="Oval 6"')
                context['bc_0026'] = kiemtra(kansa.ghichu1_3)
            else:
                docdata.remove_shape(u'id="5" name="Oval 5"')
                context['bc_0026'] = ''

            if kansa.hiendien1_4 == 'Có':
                docdata.remove_shape(u'id="8" name="Oval 8"')
                context['bc_0028'] = kiemtra(kansa.ghichu1_4)
            else:
                docdata.remove_shape(u'id="7" name="Oval 7"')
                context['bc_0028'] = ''

            if kansa.hiendien1_5 == 'Có':
                docdata.remove_shape(u'id="10" name="Oval 10"')
                context['bc_0030'] = kiemtra(kansa.ghichu1_5)
            else:
                docdata.remove_shape(u'id="9" name="Oval 9"')
                context['bc_0030'] = ''

            if kansa.hiendien1_6 == 'Có':
                docdata.remove_shape(u'id="12" name="Oval 12"')
                context['bc_0032'] = kiemtra(kansa.ghichu1_6)
            elif kansa.hiendien1_6 == 'Không':
                docdata.remove_shape(u'id="11" name="Oval 11"')
                context['bc_0032'] = ''
            else:
                docdata.remove_shape(u'id="11" name="Oval 11"')
                context['bc_0032'] = ''
                docdata.remove_shape(u'id="12" name="Oval 12"')

            if kansa.hiendien2_1 == 'Có':
                docdata.remove_shape(u'id="14" name="Oval 14"')
                context['bc_0034'] = kiemtra(kansa.ghichu2_1)
            else:
                docdata.remove_shape(u'id="13" name="Oval 13"')
                context['bc_0034'] = ''

            if kansa.hiendien2_2 == 'Có':
                docdata.remove_shape(u'id="16" name="Oval 16"')
                context['bc_0036'] = kiemtra(kansa.ghichu2_2)
            else:
                docdata.remove_shape(u'id="15" name="Oval 15"')
                context['bc_0036'] = ''

            if kansa.hiendien2_3 == 'Có':
                docdata.remove_shape(u'id="18" name="Oval 18"')
                context['bc_0038'] = kiemtra(kansa.ghichu2_3)
            else:
                docdata.remove_shape(u'id="17" name="Oval 17"')
                context['bc_0038'] = ''

            if kansa.hiendien2_4 == 'Có':
                docdata.remove_shape(u'id="20" name="Oval 20"')
                context['bc_0040'] = kiemtra(kansa.ghichu2_4)
            else:
                docdata.remove_shape(u'id="19" name="Oval 19"')
                context['bc_0040'] = ''

            if kansa.hiendien2_5 == 'Có':
                docdata.remove_shape(u'id="22" name="Oval 22"')
                context['bc_0042'] = kiemtra(kansa.ghichu2_5)
            else:
                docdata.remove_shape(u'id="21" name="Oval 21"')
                context['bc_0042'] = ''

            if kansa.hiendien2_6 == 'Có':
                docdata.remove_shape(u'id="24" name="Oval 24"')
                context['bc_0044'] = kiemtra(kansa.ghichu2_6)
            else:
                docdata.remove_shape(u'id="23" name="Oval 23"')
                context['bc_0044'] = ''

            if kansa.hiendien2_7 == 'Có':
                docdata.remove_shape(u'id="26" name="Oval 26"')
                context['bc_0046'] = kiemtra(kansa.ghichu2_7)
            else:
                docdata.remove_shape(u'id="25" name="Oval 25"')
                context['bc_0046'] = ''

            if kansa.hiendien2_8 == 'Có':
                docdata.remove_shape(u'id="28" name="Oval 28"')
                context['bc_0048'] = kiemtra(kansa.ghichu2_8)
            else:
                docdata.remove_shape(u'id="27" name="Oval 27"')
                context['bc_0048'] = ''

            if kansa.hiendien2_9 == 'Có':
                docdata.remove_shape(u'id="30" name="Oval 30"')
                context['bc_0050'] = kiemtra(kansa.ghichu2_9)
            else:
                docdata.remove_shape(u'id="29" name="Oval 29"')
                context['bc_0050'] = ''

            if kansa.hiendien2_10 == 'Có':
                docdata.remove_shape(u'id="32" name="Oval 32"')
                context['bc_0052'] = kiemtra(kansa.ghichu2_10)
            else:
                docdata.remove_shape(u'id="31" name="Oval 31"')
                context['bc_0052'] = ''

            if kansa.hiendien2_11 == 'Có':
                docdata.remove_shape(u'id="34" name="Oval 34"')
                context['bc_0054'] = kiemtra(kansa.ghichu2_11)
            else:
                docdata.remove_shape(u'id="33" name="Oval 33"')
                context['bc_0054'] = ''

            if kansa.hiendien2_12 == 'Có':
                docdata.remove_shape(u'id="36" name="Oval 36"')
                context['bc_0056'] = kiemtra(kansa.ghichu2_12)
            else:
                docdata.remove_shape(u'id="35" name="Oval 35"')
                context['bc_0056'] = ''

            if kansa.hiendien2_13 == 'Có':
                docdata.remove_shape(u'id="38" name="Oval 38"')
                context['bc_0058'] = kiemtra(kansa.ghichu2_13)
            else:
                docdata.remove_shape(u'id="37" name="Oval 37"')
                context['bc_0058'] = ''

            if kansa.hiendien2_14 == 'Có':
                docdata.remove_shape(u'id="40" name="Oval 40"')
                context['bc_0060'] = kiemtra(kansa.ghichu2_14)
            else:
                docdata.remove_shape(u'id="39" name="Oval 39"')
                context['bc_0060'] = ''

            if kansa.hiendien2_15 == 'Có':
                docdata.remove_shape(u'id="42" name="Oval 42"')
                context['bc_0062'] = kiemtra(kansa.ghichu2_15)
            else:
                docdata.remove_shape(u'id="41" name="Oval 41"')
                context['bc_0062'] = ''

            if kansa.hiendien2_16 == 'Có':
                docdata.remove_shape(u'id="44" name="Oval 44"')
                context['bc_0064'] = kiemtra(kansa.ghichu2_16)
            else:
                docdata.remove_shape(u'id="43" name="Oval 43"')
                context['bc_0064'] = ''

            if kansa.hiendien2_17 == 'Có':
                docdata.remove_shape(u'id="46" name="Oval 46"')
                context['bc_0066'] = kiemtra(kansa.ghichu2_17)
            else:
                docdata.remove_shape(u'id="45" name="Oval 45"')
                context['bc_0066'] = ''

            if kansa.hiendien2_18 == 'Có':
                docdata.remove_shape(u'id="48" name="Oval 48"')
                context['bc_0068'] = kiemtra(kansa.ghichu2_18)
            else:
                docdata.remove_shape(u'id="47" name="Oval 47"')
                context['bc_0068'] = ''

            if kansa.hiendien2_19 == 'Có':
                docdata.remove_shape(u'id="50" name="Oval 50"')
                context['bc_0070'] = kiemtra(kansa.ghichu2_19)
            else:
                docdata.remove_shape(u'id="49" name="Oval 49"')
                context['bc_0070'] = ''

            if kansa.hiendien2_20 == 'Có':
                docdata.remove_shape(u'id="52" name="Oval 52"')
                context['bc_0072'] = kiemtra(kansa.ghichu2_14)
            else:
                docdata.remove_shape(u'id="51" name="Oval 51"')
                context['bc_0072'] = ''

            if kansa.hiendien2_21 == 'Có':
                docdata.remove_shape(u'id="54" name="Oval 54"')
                context['bc_0074'] = kiemtra(kansa.ghichu2_21)
            elif kansa.hiendien2_21 == 'Không':
                docdata.remove_shape(u'id="53" name="Oval 53"')
                context['bc_0032'] = ''
            else:
                docdata.remove_shape(u'id="53" name="Oval 53"')
                docdata.remove_shape(u'id="54" name="Oval 54"')
                context['bc_0074'] = ''

            if kansa.hiendien3_1 == 'Có':
                docdata.remove_shape(u'id="56" name="Oval 56"')
                context['bc_0076'] = kiemtra(kansa.ghichu3_1)
            else:
                docdata.remove_shape(u'id="55" name="Oval 55"')
                context['bc_0076'] = ''

            if kansa.hiendien3_2 == 'Có':
                docdata.remove_shape(u'id="58" name="Oval 58"')
                context['bc_0078'] = kiemtra(kansa.ghichu3_2)
            else:
                docdata.remove_shape(u'id="57" name="Oval 57"')
                context['bc_0078'] = ''

            if kansa.hiendien3_3 == 'Có':
                docdata.remove_shape(u'id="60" name="Oval 60"')
                context['bc_0080'] = kiemtra(kansa.ghichu3_3)
            else:
                docdata.remove_shape(u'id="59" name="Oval 59"')
                context['bc_0080'] = ''

            if kansa.hiendien3_4 == 'Có':
                docdata.remove_shape(u'id="62" name="Oval 62"')
                context['bc_0082'] = kiemtra(kansa.ghichu3_4)
            else:
                docdata.remove_shape(u'id="61" name="Oval 61"')
                context['bc_0082'] = ''

            if kansa.hiendien3_5 == 'Có':
                docdata.remove_shape(u'id="64" name="Oval 64"')
                context['bc_0084'] = kiemtra(kansa.ghichu3_5)
            else:
                docdata.remove_shape(u'id="63" name="Oval 63"')
                context['bc_0084'] = ''

            if kansa.hiendien3_6 == 'Có':
                docdata.remove_shape(u'id="66" name="Oval 66"')
                context['bc_0086'] = kiemtra(kansa.ghichu3_6)
            else:
                docdata.remove_shape(u'id="65" name="Oval 65"')
                context['bc_0086'] = ''
            #
            if kansa.hiendien3_7 == 'Có':
                docdata.remove_shape(u'id="68" name="Oval 68"')
                context['bc_0088'] = kiemtra(kansa.ghichu3_7)
            else:
                docdata.remove_shape(u'id="67" name="Oval 67"')
                context['bc_0088'] = ''

            if kansa.hiendien3_8 == 'Có':
                docdata.remove_shape(u'id="70" name="Oval 70"')
                context['bc_0090'] = kiemtra(kansa.ghichu3_8)
            else:
                docdata.remove_shape(u'id="69" name="Oval 69"')
                context['bc_0090'] = ''
            #
            if kansa.hiendien3_9 == 'Có':
                docdata.remove_shape(u'id="72" name="Oval 72"')
                context['bc_0092'] = kiemtra(kansa.ghichu3_9)
            else:
                docdata.remove_shape(u'id="71" name="Oval 71"')
                context['bc_0092'] = ''
            #
            if kansa.hiendien3_10 == 'Có':
                docdata.remove_shape(u'id="74" name="Oval 74"')
                context['bc_0094'] = kiemtra(kansa.ghichu3_10)
            else:
                docdata.remove_shape(u'id="73" name="Oval 73"')
                context['bc_0094'] = ''
            #
            if kansa.hiendien3_11 == 'Có':
                docdata.remove_shape(u'id="76" name="Oval 76"')
                context['bc_0096'] = kiemtra(kansa.ghichu3_11)
            else:
                docdata.remove_shape(u'id="75" name="Oval 75"')
                context['bc_0096'] = ''
            #
            if kansa.hiendien3_12 == 'Có':
                docdata.remove_shape(u'id="78" name="Oval 78"')
                context['bc_0098'] = kiemtra(kansa.ghichu3_12)
            else:
                docdata.remove_shape(u'id="77" name="Oval 77"')
                context['bc_0098'] = ''
            #
            if kansa.hiendien3_13 == 'Có':
                docdata.remove_shape(u'id="80" name="Oval 80"')
                context['bc_0100'] = kiemtra(kansa.ghichu3_13)
            else:
                docdata.remove_shape(u'id="79" name="Oval 79"')
                context['bc_0100'] = ''
            #
            if kansa.hiendien3_14 == 'Có':
                docdata.remove_shape(u'id="82" name="Oval 82"')
                context['bc_0102'] = kiemtra(kansa.ghichu3_14)
            else:
                docdata.remove_shape(u'id="81" name="Oval 81"')
                context['bc_0102'] = ''
            #
            if kansa.hiendien3_15 == 'Có':
                docdata.remove_shape(u'id="84" name="Oval 84"')
                context['bc_0104'] = kiemtra(kansa.ghichu3_15)
            else:
                docdata.remove_shape(u'id="83" name="Oval 83"')
                context['bc_0104'] = ''
            #
            if kansa.hiendien3_16 == 'Có':
                docdata.remove_shape(u'id="86" name="Oval 86"')
                context['bc_0106'] = kiemtra(kansa.ghichu3_16)
            else:
                docdata.remove_shape(u'id="85" name="Oval 85"')
                context['bc_0106'] = ''
            #
            if kansa.hiendien3_17 == 'Có':
                docdata.remove_shape(u'id="88" name="Oval 88"')
                context['bc_0108'] = kiemtra(kansa.ghichu3_17)
            elif kansa.hiendien3_17 == 'Không':
                docdata.remove_shape(u'id="87" name="Oval 87"')
                context['bc_0108'] = ''
            else:
                docdata.remove_shape(u'id="87" name="Oval 87"')
                docdata.remove_shape(u'id="88" name="Oval 88"')
                context['bc_0108'] = ''

            #
            if kansa.hiendien4_1 == 'Có':
                docdata.remove_shape(u'id="90" name="Oval 90"')
                context['bc_0110'] = kiemtra(kansa.ghichu3_16)
            else:
                docdata.remove_shape(u'id="89" name="Oval 89"')
                context['bc_0110'] = ''
            #
            if kansa.hiendien4_2 == 'Có':
                docdata.remove_shape(u'id="92" name="Oval 92"')
                context['bc_0112'] = kiemtra(kansa.ghichu4_2)
            else:
                docdata.remove_shape(u'id="91" name="Oval 91"')
                context['bc_0112'] = ''
            #
            if kansa.hiendien4_3 == 'Có':
                docdata.remove_shape(u'id="94" name="Oval 94"')
                context['bc_0114'] = kiemtra(kansa.ghichu4_3)
            else:
                docdata.remove_shape(u'id="93" name="Oval 93"')
                context['bc_0114'] = ''
                #
            if kansa.hiendien4_4 == 'Có':
                docdata.remove_shape(u'id="96" name="Oval 96"')
                context['bc_0116'] = kiemtra(kansa.ghichu4_4)
            else:
                docdata.remove_shape(u'id="95" name="Oval 95"')
                context['bc_0116'] = ''
            #
            if kansa.hiendien4_5 == 'Có':
                docdata.remove_shape(u'id="98" name="Oval 98"')
                context['bc_0118'] = kiemtra(kansa.ghichu4_5)
            else:
                docdata.remove_shape(u'id="97" name="Oval 97"')
                context['bc_0118'] = ''
                #
            if kansa.hiendien4_6 == 'Có':
                docdata.remove_shape(u'id="100" name="Oval 100"')
                context['bc_0120'] = kiemtra(kansa.ghichu4_6)
            else:
                docdata.remove_shape(u'id="99" name="Oval 99"')
                context['bc_0120'] = ''
            #
            if kansa.hiendien4_7 == 'Có':
                docdata.remove_shape(u'id="102" name="Oval 102"')
                context['bc_0122'] = kiemtra(kansa.ghichu4_7)
            else:
                docdata.remove_shape(u'id="101" name="Oval 101"')
                context['bc_0122'] = ''
                #
            if kansa.hiendien4_8 == 'Có':
                docdata.remove_shape(u'id="104" name="Oval 104"')
                context['bc_0124'] = kiemtra(kansa.ghichu4_8)
            else:
                docdata.remove_shape(u'id="103" name="Oval 103"')
                context['bc_0124'] = ''
            #
            if kansa.hiendien4_9 == 'Có':
                docdata.remove_shape(u'id="106" name="Oval 106"')
                context['bc_0126'] = kiemtra(kansa.ghichu4_9)
            else:
                docdata.remove_shape(u'id="105" name="Oval 105"')
                context['bc_0126'] = ''
            #
            if kansa.hiendien4_10 == 'Có':
                docdata.remove_shape(u'id="108" name="Oval 108"')
                context['bc_0128'] = kiemtra(kansa.ghichu4_10)
            else:
                docdata.remove_shape(u'id="107" name="Oval 107"')
                context['bc_0128'] = ''
            #
            if kansa.hiendien4_11 == 'Có':
                docdata.remove_shape(u'id="110" name="Oval 110"')
                context['bc_0130'] = kiemtra(kansa.ghichu4_11)
            else:
                docdata.remove_shape(u'id="109" name="Oval 109"')
                context['bc_0130'] = ''
            #
            if kansa.hiendien4_12 == 'Có':
                docdata.remove_shape(u'id="112" name="Oval 112"')
                context['bc_0132'] = kiemtra(kansa.ghichu4_12)
            else:
                docdata.remove_shape(u'id="111" name="Oval 111"')
                context['bc_0132'] = ''
            #
            #
            if kansa.hiendien4_13 == 'Có':
                docdata.remove_shape(u'id="114" name="Oval 114"')
                context['bc_0134'] = kiemtra(kansa.ghichu4_13)
            else:
                docdata.remove_shape(u'id="113" name="Oval 113"')
                context['bc_0134'] = ''
            #
            if kansa.hiendien4_14 == 'Có':
                docdata.remove_shape(u'id="116" name="Oval 116"')
                context['bc_0136'] = kiemtra(kansa.ghichu4_14)
            else:
                docdata.remove_shape(u'id="115" name="Oval 115"')
                context['bc_0136'] = ''
            #
            if kansa.hiendien4_15 == 'Có':
                docdata.remove_shape(u'id="118" name="Oval 118"')
                context['bc_0138'] = kiemtra(kansa.ghichu4_15)
            else:
                docdata.remove_shape(u'id="117" name="Oval 117"')
                context['bc_0138'] = ''
            #
            if kansa.hiendien4_16 == 'Có':
                docdata.remove_shape(u'id="120" name="Oval 120"')
                context['bc_0140'] = kiemtra(kansa.ghichu4_16)
            else:
                docdata.remove_shape(u'id="119" name="Oval 119"')
                context['bc_0140'] = ''
            #
            if kansa.hiendien4_17 == 'Có':
                docdata.remove_shape(u'id="122" name="Oval 122"')
                context['bc_0142'] = kiemtra(kansa.ghichu4_17)
            else:
                docdata.remove_shape(u'id="121" name="Oval 121"')
                context['bc_0142'] = ''
            #
            if kansa.hiendien4_18 == 'Có':
                docdata.remove_shape(u'id="124" name="Oval 124"')
                context['bc_0144'] = kiemtra(kansa.ghichu4_18)
            else:
                docdata.remove_shape(u'id="123" name="Oval 123"')
                context['bc_0144'] = ''
            #
            if kansa.hiendien4_19 == 'Có':
                docdata.remove_shape(u'id="126" name="Oval 126"')
                context['bc_0146'] = kiemtra(kansa.ghichu4_19)
            else:
                docdata.remove_shape(u'id="125" name="Oval 125"')
                context['bc_0146'] = ''

            if kansa.hiendien4_20 == 'Có':
                docdata.remove_shape(u'id="128" name="Oval 128"')
                context['bc_0148'] = kiemtra(kansa.ghichu4_20)
            else:
                docdata.remove_shape(u'id="127" name="Oval 127"')
                context['bc_0148'] = ''
            #
            if kansa.hiendien4_21 == 'Có':
                docdata.remove_shape(u'id="130" name="Oval 130"')
                context['bc_0150'] = kiemtra(kansa.ghichu4_21)
            elif kansa.hiendien4_21 == 'Không':
                docdata.remove_shape(u'id="129" name="Oval 129"')
                context['bc_0150'] = ''
            else:
                docdata.remove_shape(u'id="129" name="Oval 129"')
                context['bc_0150'] = ''
                docdata.remove_shape(u'id="130" name="Oval 130"')
            #
            #
            if kansa.hiendien5_1 == 'Có':
                docdata.remove_shape(u'id="132" name="Oval 132"')
                context['bc_0152'] = kiemtra(kansa.ghichu5_1)
            else:
                docdata.remove_shape(u'id="131" name="Oval 131"')
                context['bc_0152'] = ''

            if kansa.hiendien5_2 == 'Có':
                docdata.remove_shape(u'id="134" name="Oval 134"')
                context['bc_0154'] = kiemtra(kansa.ghichu5_2)
            else:
                docdata.remove_shape(u'id="133" name="Oval 133"')
                context['bc_0154'] = ''
            #
            if kansa.hiendien5_3 == 'Có':
                docdata.remove_shape(u'id="136" name="Oval 136"')
                context['bc_0156'] = kiemtra(kansa.ghichu5_3)
            elif kansa.hiendien5_3 == 'Không':
                docdata.remove_shape(u'id="135" name="Oval 135"')
                context['bc_0156'] = ''
            else:
                docdata.remove_shape(u'id="135" name="Oval 135"')
                context['bc_0156'] = ''
                docdata.remove_shape(u'id="136" name="Oval 136"')

            context['bc_0157'] = kiemtra(kansa.ghichu_6)
            context['bc_0158'] = kiemtra(kansa.ghichu_7)
            context['hhjp_0193'] = convert_date(kansa.daybaocao_kansa)

            for nhanvien in cosogiamsat_nghiepdoan.danhsach_nhanvien_tts:
                if nhanvien.quanly_vaitro_nghiepdoan == True:
                    context['bc_0159'] = kiemtra(nhanvien.chucvu_nghiepdoan_han)
                    context['hhjp_0103'] = kiemtra(nhanvien.ten_nvien_han_nghiepdoan)
                    break

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None


class NhanVienXiNghiep(models.Model):
    _inherit = "nhanvien.nhanvien"

    trachnhiem_kansa = fields.Many2one(comodel_name='baocao.kansa')
    thuctap_kansa = fields.Many2one(comodel_name='baocao.kansa')
