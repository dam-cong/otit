# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Junkai(models.Model):
    _name = 'junkai.junkai'
    _rec_name = 'ngay_junkai'

    hoso = fields.Many2one(comodel_name='baocao.junkai')

    ma_congviec = fields.Char(related='hoso.ma_congviec')
    vaitro_six = fields.Many2one(related='hoso.xinghiep_junkai.danhsach_nhanvien.nhanvien_xinghiep')

    ngay_junkai = fields.Date(string='Ngày')
    nghi_junkai = fields.Boolean(string='Nghỉ')

    tuongung_junkai = fields.Many2one(comodel_name='noidung.noidung', string='Mục tưong ứng')
    chitiet_1 = fields.Many2one(comodel_name='noidung.noidung', string='Chi tiết 1')
    chitiet_2 = fields.Many2one(comodel_name='noidung.noidung', string='Chi tiết 2')
    chitiet_3 = fields.Many2one(comodel_name='noidung.noidung', string='Chi tiết 3')
    chitiet_4 = fields.Many2one(comodel_name='noidung.noidung', string='Chi tiết 4')

    # ten_chitiet = fields.Char(string='Tên chi tiết', compute='_ten_chitiet')
    #
    # @api.multi
    # @api.depends('chitiet_1', 'chitiet_2', 'chitiet_3', 'chitiet_4')
    # def _ten_chitiet(self):
    #     if self.chitiet_1:
    #         self.ten_chitiet = self.chitiet_1
    #     elif self.chitiet_2:
    #         self.ten_chitiet = self.chitiet_2
    #     elif self.chitiet_3:
    #         self.ten_chitiet = self.chitiet_3
    #     elif self.chitiet_4:
    #         self.ten_chitiet = self.chitiet_4
    #     else:
    #         self.ten_chitiet = ''

    chidao_junkai = fields.Many2one(comodel_name='nhanvien.nhanvien', string='Người chỉ đạo')

    noidung_thuctap = fields.Char(string='Nội dung thực tập')
    noidung_daotao = fields.Char(string='Nội dung đào tạo')
