# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime
import logging
from tempfile import TemporaryFile, NamedTemporaryFile
from io import BytesIO, StringIO
import os
import codecs
from docxtpl import DocxTemplate
from docxtpl import DocxTemplate, Listing

_logger = logging.getLogger(__name__)


def kiemtra(data):
    if data:
        return data
    else:
        return ''


def convert_date(self):
    if self:
        return str(self.year) + '年' + \
               str(self.month) + '月' + \
               str(self.day) + '日'
    else:
        return '年　月　日'

def convert_date_date(self):
    if self:
        return str(int(self.year) - 2019) + '年' + \
               str(self.month) + '月' + \
               str(self.day) + '日'
    else:
        return '年　月　日'
    
def nganhnghe(self):
    if self:
        demo = self.find(':')
        if demo != -1:
            return self.split(":")[1]
        else:
            return self
    else:
        return ''


class TinhTrang(models.Model):
    _name = 'baocao.tinhtrang'
    _rec_name = 'xinghiep_tinhtrang'

    xinghiep_tinhtrang = fields.Many2one(comodel_name='xinghiep.xinghiep', string='Xí nghiệp',required=True)

    ngay_baocao = fields.Date(string='Ngày báo cáo', default=fields.Date.today())
    nam_baocao = fields.Char(string="Năm báo cáo",required=True)

    nam_baocao_tinhtrang = fields.Char(string='Năm báo cáo')  # bc_0267
    motgo_tts_tinhtrang = fields.Integer(string='1 go')  # bc_0269
    haigo_tts_tinhtrang = fields.Integer(string='2 go')  # bc_0270
    bago_tts_tinhtrang = fields.Integer(string='3 go')  # bc_0271
    tongtts_tinhtrang = fields.Integer(string='Tổng số', compute='_tongtts_tinhtrang')  # bc_0272

    @api.one
    @api.depends('motgo_tts_tinhtrang', 'haigo_tts_tinhtrang', 'bago_tts_tinhtrang')
    def _tongtts_tinhtrang(self):
        for i in self:
            if i.motgo_tts_tinhtrang or i.haigo_tts_tinhtrang or i.bago_tts_tinhtrang:
                i.tongtts_tinhtrang = i.motgo_tts_tinhtrang + i.haigo_tts_tinhtrang + i.bago_tts_tinhtrang
            else:
                i.tongtts_tinhtrang = 0

    # 1. Tình trạng dự thi kỹ năng
    socap_motgo_hoanthanh_tinhtrang_thuchanh = fields.Integer()  #
    socap_motgo_soluong_nguoi_tinhtrang_thuchanh = fields.Integer()  #
    socap_motgo_ttsdo_tinhtrang_thuchanh = fields.Integer()  #

    socap_motgo_hoanthanh_tinhtrang_lythuyet = fields.Integer()  #
    socap_motgo_soluong_nguoi_tinhtrang_lythuyet = fields.Integer()  #
    socap_motgo_ttsdo_tinhtrang_lythuyet = fields.Integer()  #

    haikyu_bago_thuchanh_hoanthanh_tinhtrang_thuchanh = fields.Integer()  #
    haikyu_bago_thuchanh_soluong_nguoi_tinhtrang_thuchanh = fields.Integer()  #
    haikyu_bago_thuchanh_ttsdo_tinhtrang_thuchanh = fields.Integer()  #

    haikyu_bago_thuchanh_hoanthanh_tinhtrang_lythuyet = fields.Integer()  #
    haikyu_bago_thuchanh_soluong_nguoi_tinhtrang_lythuyet = fields.Integer()  #
    haikyu_bago_thuchanh_ttsdo_tinhtrang_lythuyet = fields.Integer()  #

    bakyu_haigo_thuchanh_hoanthanh_tinhtrang_thuchanh = fields.Integer()  #
    bakyu_haigo_thuchanh_soluong_nguoi_tinhtrang_thuchanh = fields.Integer()  #
    bakyu_haigo_thuchanh_ttsdo_tinhtrang_thuchanh = fields.Integer()  #

    bakyu_haigo_thuchanh_hoanthanh_tinhtrang_lythuyet = fields.Integer()  #
    bakyu_haigo_thuchanh_soluong_nguoi_tinhtrang_lythuyet = fields.Integer()  #
    bakyu_haigo_thuchanh_ttsdo_tinhtrang_lythuyet = fields.Integer()  #

    # 2. Loại công việc
    ma_congviec = fields.Char(string='Mã công việc', compute='_ma_congviec')
    nganhnghe = fields.Many2one(comodel_name='nganhnghe.nganhnghe', string='Ngành nghề')
    loainghe = fields.Many2one(comodel_name='loaicongviec.loaicongviec', string='Loại công việc')
    congviec = fields.Many2one(comodel_name='congviec.congviec', string='Công việc')

    @api.depends('congviec')
    def _ma_congviec(self):
        if self.congviec:
            self.ma_congviec = self.congviec.ma_congviec

    @api.onchange('nganhnghe')
    def onchange_nganhnghe(self):
        self.loainghe = ()
        self.congviec = ()
        self.ma_congviec = ''

    # 3. Điều kiện lao động ở xí nghiệp
    songay_lamviec_thucte_1 = fields.Integer()
    songay_lamviec_thucte_2 = fields.Integer()
    songay_lamviec_thucte_3 = fields.Integer()

    sogio_lamviec_thucte_lichtrinh_1 = fields.Integer()
    sogio_lamviec_thucte_lichtrinh_2 = fields.Integer()
    sogio_lamviec_thucte_lichtrinh_3 = fields.Integer()

    quagio_lamviec_thucte_1 = fields.Integer()
    quagio_lamviec_thucte_2 = fields.Integer()
    quagio_lamviec_thucte_3 = fields.Integer()

    tienluong_phaitra_1 = fields.Integer()
    tienluong_phaitra_2 = fields.Integer()
    tienluong_phaitra_3 = fields.Integer()

    tienluong_lamthem_gio_1 = fields.Integer()
    tienluong_lamthem_gio_2 = fields.Integer()
    tienluong_lamthem_gio_3 = fields.Integer()

    phucap_dilai_1 = fields.Integer()
    phucap_dilai_2 = fields.Integer()
    phucap_dilai_3 = fields.Integer()

    phucap_chuyencan_1 = fields.Integer()
    phucap_chuyencan_2 = fields.Integer()
    phucap_chuyencan_3 = fields.Integer()

    phucap_giadinh_1 = fields.Integer()
    phucap_giadinh_2 = fields.Integer()
    phucap_giadinh_3 = fields.Integer()

    mucluong_dacbiet_1 = fields.Integer()
    mucluong_dacbiet_2 = fields.Integer()
    mucluong_dacbiet_3 = fields.Integer()

    chiphi_anuong_1 = fields.Integer()
    chiphi_anuong_2 = fields.Integer()
    chiphi_anuong_3 = fields.Integer()

    chiphi_nhao_1 = fields.Integer()
    chiphi_nhao_2 = fields.Integer()
    chiphi_nhao_3 = fields.Integer()

    thue_baohiem_xahoi_1 = fields.Integer()
    thue_baohiem_xahoi_2 = fields.Integer()
    thue_baohiem_xahoi_3 = fields.Integer()

    sotien_khac_1 = fields.Integer()
    sotien_khac_2 = fields.Integer()
    sotien_khac_3 = fields.Integer()

    tangluong_2 = fields.Integer()
    tangluong_3 = fields.Integer()

    # 4. Tình trạng bỏ trốn
    songuoi_tong_tinhtrang = fields.Integer(string='Tổng số')
    songuoi_botron_tinhtrang = fields.Integer(string='Số người bỏ trốn')  # bc_0387
    tyle_botron_tinhtrang = fields.Float(string='Tỷ lệ bỏ trốn', compute='_tyle_botron_tinhtrang')  # bc_0388

    @api.multi
    @api.depends('tongtts_tinhtrang', 'songuoi_botron_tinhtrang')
    def _tyle_botron_tinhtrang(self):
        if self.tongtts_tinhtrang and self.songuoi_botron_tinhtrang:
            self.tyle_botron_tinhtrang = ((self.songuoi_botron_tinhtrang / self.tongtts_tinhtrang) * 100)

    # 5. Tiếp nhận thực tập sinh từ xí nghiệp khác khi gặp vấn đề khó khăn
    songuoi_tiepnhan_tinhtrang = fields.Integer(string='Số người')  # bc_0389
    dangky_web_hotro_tinhtrang = fields.Selection(string='Có đăng kí trên web hỗ trợ tìm nơi thực tập mới',
                                                  selection=[('Có', 'Có'), ('Không', 'Không')])  #

    #  6. Tình hình tạo điều kiện giao lưu với xã hội
    hoctien_tinhtrang = fields.Text(string='Hỗ trợ học tiếng Nhật')  # bc_0390
    giaoluu_tinhtrang = fields.Text(string='Hỗ trợ giao lưu xã hội')  # bc_0391
    vanhoa_tinhtrang = fields.Text(string='Hỗ trợ học văn hóa Nhật')  # bc_0392
    # 7. Ghi chú
    ghichu_tinhtrang = fields.Text(string='Ghi chú')  # bc_0393

    @api.multi
    def baocao_tinhtrang_button(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/tinhtrang/download/%s' % (self.id),
            'target': 'self', }

    @api.multi
    def baocao_tinhtrang_37(self, tinhtrang):
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_37")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            nghiepdoan = self.env['nghiepdoan.nghiepdoan'].search([('id', '=', 1)], limit=1)
            context = {}

            context['hhjp_0193'] = convert_date_date(tinhtrang.ngay_baocao)
            context['hhjp_0010'] = kiemtra(tinhtrang.xinghiep_tinhtrang.ten_han_xnghiep)
            context['hhjp_0097'] = kiemtra(nghiepdoan.ten_han_nghiepdoan)
            context['hhjp_0014_2'] = kiemtra(nghiepdoan.ten_daidien_han_nghiepdoan)
            context['hhjp_0097_1'] = kiemtra(nghiepdoan.chucvu_daidien_phienam_nghiepdoan)

            context['hhjp_0014'] = kiemtra(tinhtrang.xinghiep_tinhtrang.daidien_han_xnghiep)
            context['hhjp_0014_1'] = kiemtra(tinhtrang.xinghiep_tinhtrang.chucvu_daidien_han)

            context['hhjp_0811'] = kiemtra(tinhtrang.xinghiep_tinhtrang.so_chapnhan_daotao)
            context['hhjp_0009'] = kiemtra(tinhtrang.xinghiep_tinhtrang.ten_furi_xnghiep)
            if tinhtrang.xinghiep_tinhtrang.sobuudien_xnghiep:
                context['hhjp_0011_bd'] = tinhtrang.xinghiep_tinhtrang.sobuudien_xnghiep[
                                          0:3] + '-' + tinhtrang.xinghiep_tinhtrang.sobuudien_xnghiep[
                                                       3:]
            else:
                context['hhjp_0011_bd'] = ''

            context['hhjp_0011'] = kiemtra(tinhtrang.xinghiep_tinhtrang.diachi_han_xnghiep)
            context['hhjp_0012'] = kiemtra(tinhtrang.xinghiep_tinhtrang.sdt_xnghiep)
            if tinhtrang.nam_baocao:
                context['bc_0267'] = int(tinhtrang.nam_baocao) - 2019
                context['bc_0268'] = int(context['bc_0267']) + 1
            else:
                context['bc_0267'] = ''
                context['bc_0268'] = ''
            context['hhjp_0017'] = kiemtra(tinhtrang.xinghiep_tinhtrang.nganhmax_xnghiep.name_phannganh)
            context['hhjp_0018'] = kiemtra(tinhtrang.xinghiep_tinhtrang.nganhmin_xnghiep.name_phannganhbe)
            context['xn_0019'] = kiemtra(tinhtrang.ma_congviec)
            context['xn_0020'] = nganhnghe(tinhtrang.loainghe.name_loaicongviec)
            context['bc_0270'] = kiemtra(tinhtrang.motgo_tts_tinhtrang)
            context['bc_0271'] = kiemtra(tinhtrang.haigo_tts_tinhtrang)
            context['bc_0272'] = kiemtra(tinhtrang.bago_tts_tinhtrang)

            context['xn_0001'] = kiemtra(tinhtrang.socap_motgo_hoanthanh_tinhtrang_thuchanh)
            context['xn_0002'] = kiemtra(tinhtrang.socap_motgo_soluong_nguoi_tinhtrang_thuchanh)
            context['xn_0003'] = kiemtra(tinhtrang.socap_motgo_ttsdo_tinhtrang_thuchanh)

            context['xn_0004'] = kiemtra(tinhtrang.socap_motgo_hoanthanh_tinhtrang_lythuyet)
            context['xn_0005'] = kiemtra(tinhtrang.socap_motgo_soluong_nguoi_tinhtrang_lythuyet)
            context['xn_0006'] = kiemtra(tinhtrang.socap_motgo_ttsdo_tinhtrang_lythuyet)

            context['xn_0007'] = kiemtra(tinhtrang.haikyu_bago_thuchanh_hoanthanh_tinhtrang_thuchanh)
            context['xn_0008'] = kiemtra(tinhtrang.haikyu_bago_thuchanh_soluong_nguoi_tinhtrang_thuchanh)
            context['xn_0009'] = kiemtra(tinhtrang.haikyu_bago_thuchanh_ttsdo_tinhtrang_thuchanh)

            context['xn_0010'] = kiemtra(tinhtrang.haikyu_bago_thuchanh_hoanthanh_tinhtrang_lythuyet)
            context['xn_0011'] = kiemtra(tinhtrang.haikyu_bago_thuchanh_soluong_nguoi_tinhtrang_lythuyet)
            context['xn_0012'] = kiemtra(tinhtrang.haikyu_bago_thuchanh_ttsdo_tinhtrang_lythuyet)

            context['xn_0013'] = kiemtra(tinhtrang.bakyu_haigo_thuchanh_hoanthanh_tinhtrang_thuchanh)
            context['xn_0014'] = kiemtra(tinhtrang.bakyu_haigo_thuchanh_soluong_nguoi_tinhtrang_thuchanh)
            context['xn_0015'] = kiemtra(tinhtrang.bakyu_haigo_thuchanh_ttsdo_tinhtrang_thuchanh)

            context['xn_0016'] = kiemtra(tinhtrang.bakyu_haigo_thuchanh_hoanthanh_tinhtrang_lythuyet)
            context['xn_0017'] = kiemtra(tinhtrang.bakyu_haigo_thuchanh_soluong_nguoi_tinhtrang_lythuyet)
            context['xn_0018'] = kiemtra(tinhtrang.bakyu_haigo_thuchanh_ttsdo_tinhtrang_lythuyet)

            context['xn_0021'] = kiemtra(tinhtrang.songay_lamviec_thucte_1)
            context['xn_0022'] = kiemtra(tinhtrang.songay_lamviec_thucte_2)
            context['xn_0023'] = kiemtra(tinhtrang.songay_lamviec_thucte_3)

            context['xn_0024'] = kiemtra(tinhtrang.sogio_lamviec_thucte_lichtrinh_1)
            context['xn_0025'] = kiemtra(tinhtrang.sogio_lamviec_thucte_lichtrinh_2)
            context['xn_0026'] = kiemtra(tinhtrang.sogio_lamviec_thucte_lichtrinh_3)

            context['xn_0027'] = kiemtra(tinhtrang.quagio_lamviec_thucte_1)
            context['xn_0028'] = kiemtra(tinhtrang.quagio_lamviec_thucte_2)
            context['xn_0029'] = kiemtra(tinhtrang.quagio_lamviec_thucte_3)

            context['xn_0030'] = kiemtra(tinhtrang.tienluong_phaitra_1)
            context['xn_0031'] = kiemtra(tinhtrang.tienluong_phaitra_2)
            context['xn_0032'] = kiemtra(tinhtrang.tienluong_phaitra_3)

            context['xn_0033'] = kiemtra(tinhtrang.tienluong_lamthem_gio_1)
            context['xn_0034'] = kiemtra(tinhtrang.tienluong_lamthem_gio_2)
            context['xn_0035'] = kiemtra(tinhtrang.tienluong_lamthem_gio_3)

            context['xn_0036'] = kiemtra(tinhtrang.phucap_dilai_1)
            context['xn_0037'] = kiemtra(tinhtrang.phucap_dilai_2)
            context['xn_0038'] = kiemtra(tinhtrang.phucap_dilai_3)

            context['xn_0039'] = kiemtra(tinhtrang.phucap_chuyencan_1)
            context['xn_0040'] = kiemtra(tinhtrang.phucap_chuyencan_2)
            context['xn_0041'] = kiemtra(tinhtrang.phucap_chuyencan_3)

            context['xn_0042'] = kiemtra(tinhtrang.phucap_giadinh_1)
            context['xn_0043'] = kiemtra(tinhtrang.phucap_giadinh_2)
            context['xn_0044'] = kiemtra(tinhtrang.phucap_giadinh_3)

            context['xn_0045'] = kiemtra(tinhtrang.mucluong_dacbiet_1)
            context['xn_0046'] = kiemtra(tinhtrang.mucluong_dacbiet_2)
            context['xn_0047'] = kiemtra(tinhtrang.mucluong_dacbiet_3)

            context['xn_0048'] = kiemtra(tinhtrang.chiphi_anuong_1)
            context['xn_0049'] = kiemtra(tinhtrang.chiphi_anuong_2)
            context['xn_0050'] = kiemtra(tinhtrang.chiphi_anuong_3)

            context['xn_0051'] = kiemtra(tinhtrang.chiphi_nhao_1)
            context['xn_0052'] = kiemtra(tinhtrang.chiphi_nhao_2)
            context['xn_0053'] = kiemtra(tinhtrang.chiphi_nhao_3)

            context['xn_0054'] = kiemtra(tinhtrang.thue_baohiem_xahoi_1)
            context['xn_0055'] = kiemtra(tinhtrang.thue_baohiem_xahoi_2)
            context['xn_0056'] = kiemtra(tinhtrang.thue_baohiem_xahoi_3)

            context['xn_0057'] = kiemtra(tinhtrang.sotien_khac_1)
            context['xn_0058'] = kiemtra(tinhtrang.sotien_khac_2)
            context['xn_0059'] = kiemtra(tinhtrang.sotien_khac_3)

            context['bc_0376'] = kiemtra(tinhtrang.tangluong_2)
            context['bc_0377'] = kiemtra(tinhtrang.tangluong_3)

            context['xn_0060'] = kiemtra(tinhtrang.songuoi_tong_tinhtrang)

            context['bc_0387'] = kiemtra(tinhtrang.songuoi_botron_tinhtrang)
            context['bc_0388'] = kiemtra(round(tinhtrang.tyle_botron_tinhtrang,2))
            context['bc_0389'] = kiemtra(tinhtrang.songuoi_tiepnhan_tinhtrang)

            if tinhtrang.dangky_web_hotro_tinhtrang == 'Có':
                docdata.remove_shape(u'id="1" name="Oval 1"')
            elif tinhtrang.dangky_web_hotro_tinhtrang == 'Không':
                docdata.remove_shape(u'id="2" name="Oval 2"')
            else:
                docdata.remove_shape(u'id="1" name="Oval 1"')
                docdata.remove_shape(u'id="2" name="Oval 2"')

            context['bc_0390'] = kiemtra(tinhtrang.hoctien_tinhtrang)
            context['bc_0391'] = kiemtra(tinhtrang.giaoluu_tinhtrang)
            context['bc_0392'] = kiemtra(tinhtrang.vanhoa_tinhtrang)

            context['bc_0393'] = kiemtra(tinhtrang.ghichu_tinhtrang)

            docdata.render(context)
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None
