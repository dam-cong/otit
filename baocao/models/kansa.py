# -*- coding: utf-8 -*-

from odoo import models, fields, api


# class BaoTrachNhiem(models.Model):
#     _name = 'baocao.trachnhiem'
#     _rec_name = 'nhanvientrachnhiem'
#
#     nhanvientrachnhiem = fields.Char(string='Người chịu trách nhiệm thực tập')
#     trachnhiem_kansa = fields.Many2one(comodel_name='baocao.kansa')
#
# class BaoThucTap(models.Model):
#     _name = 'baocao.thuctap'
#     _rec_name = 'nhanvienthuctap'
#
#     nhanvienthuctap = fields.Char(string='Người chỉ đạo thực tập')
#     thuctap_kansa = fields.Many2one(comodel_name='baocao.kansa')
#
# class BaoDoiSong(models.Model):
#     _name = 'baocao.doisong'
#     _rec_name = 'nhanviendoisong'
#
#     nhanviendoisong = fields.Char(string='Người chỉ đạo đời sống')
#     doisong_kansa = fields.Many2one(comodel_name='baocao.kansa')

class BaoTroLy(models.Model):
    _name = 'baocao.troly'
    _rec_name = 'name_troly'

    name_troly = fields.Char(string='Trợ lý')
    troly_kansa = fields.Many2one(comodel_name='baocao.kansa')


class BaoNhaO(models.Model):
    _name = 'baocao.nhao'
    _rec_name = 'tennha'

    tennha = fields.Char(string='Tên cơ sở nhà ở')
    diachi = fields.Char(string='Địa chỉ')
    nha_kansa = fields.Many2one(comodel_name='baocao.kansa')


class Kansa(models.Model):
    _name = 'kansa.kansa'
    _rec_name = 'name'

    name = fields.Char()
