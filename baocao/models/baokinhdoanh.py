# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime
import logging
from tempfile import TemporaryFile, NamedTemporaryFile
from io import BytesIO, StringIO
import os
import codecs
from docxtpl import DocxTemplate
from docxtpl import DocxTemplate, Listing

_logger = logging.getLogger(__name__)


def kiemtra(data):
    if data:
        return data
    else:
        return ''


def convert_date(self):
    if self:
        return str(self.year) + '年' + \
               str(self.month) + '月' + \
               str(self.day) + '日'
    else:
        return '年　月　日'


def convert_date_date(self):
    if self:
        return str(int(self.year) - 2019) + '年' + \
               str(self.month) + '月' + \
               str(self.day) + '日'
    else:
        return '年　月　日'


def place_value(number):
    if number:
        return ("{:,}".format(number))
    else:
        return ''


class QuanLy(models.Model):
    _name = 'kinhdoanh.quanly'
    _rec_name = 'hoten_quanly'

    kinhdoanh_quanly = fields.Many2one(comodel_name='baocao.kinhdoanh')
    hoten_quanly = fields.Char(string='Họ và tên')
    motkhoahoc_quanly = fields.Char(string='(1) Tên khóa học')
    motngay_thamdu_quanly = fields.Date(string='Ngày tham dự')
    haikhoahoc_quanly = fields.Char(string='(2) Tên khóa học')
    haingay_thamdu_quanly = fields.Date(string='Ngày tham dự')
    bakhoahoc_quanly = fields.Char(string='(3) Tên khóa học')
    bangay_thamdu_quanly = fields.Date(string='Ngày tham dự')


class Khac(models.Model):
    _name = 'kinhdoanh.khac'
    _rec_name = 'hoten_khac'

    kinhdoanh_khac = fields.Many2one(comodel_name='baocao.kinhdoanh')
    hoten_khac = fields.Char(string='Họ và tên')
    motkhoahoc_khac = fields.Char(string='(1) Tên khóa học')
    motngay_thamdu_khac = fields.Date(string='Ngày tham dự')
    haikhoahoc_khac = fields.Char(string='(2) Tên khóa học')
    haingay_thamdu_khac = fields.Date(string='Ngày tham dự')
    bakhoahoc_khac = fields.Char(string='(3) Tên khóa học')
    bangay_thamdu_khac = fields.Date(string='Ngày tham dự')


# class QuocTich(models.Model):
#     _name = 'quoctich.quoctich'
#     _rec_name = 'ten_quoctich'
#
#     ten_quoctich = fields.Char(string='Quốc tịch')


class BaoKinhdoanh(models.Model):
    _name = 'baocao.kinhdoanh'
    _rec_name = 'vanphong_kinhdoanh'

    ngay_baocao = fields.Date(string='Ngày báo cáo',  default=fields.Date.today())
    # Lựa chọn mục tiêu báo cáo kinh doanh
    vanphong_kinhdoanh = fields.Many2one(comodel_name='nghiepdoan.chinhanh', string='Chi nhánh nghiệp đoàn',required=True)
    so_chinhanh_kinhdoanh = fields.Char(string='Số chi nhánh nghiệp đoàn')  # bc_0171
    nam_daotao_kinhdoanh = fields.Char(string='Năm đào tạo kỹ thuật')  # bc_0169
    soluong_tts_kinhdoanh = fields.Integer(string='Số lượng thực tập sinh giám sát')  # bc_0172

    # 1. Số lượng TTS được giám sát
    # Bảng 1
    go1_tss_kinhdoanh = fields.Integer(string='Go 1')  # bc_0174
    go2_tss_kinhdoanh = fields.Integer(string='Go 2')  # bc_0175
    go3_tss_kinhdoanh = fields.Integer(string='Go 3')  # bc_0176

    tong_tts_kinhdoanh = fields.Integer(string='Tổng', compute='_tong_tts_kinhdoanh')  # bc_0173

    @api.multi
    @api.depends('go1_tss_kinhdoanh', 'go2_tss_kinhdoanh', 'go3_tss_kinhdoanh')
    def _tong_tts_kinhdoanh(self):
        if self.go1_tss_kinhdoanh or self.go2_tss_kinhdoanh or self.go3_tss_kinhdoanh:
            self.tong_tts_kinhdoanh = self.go1_tss_kinhdoanh + self.go2_tss_kinhdoanh + self.go3_tss_kinhdoanh

    # Bảng 2
    phanmot_quoctich_kinhdoanh = fields.Many2one(comodel_name='quoctich.quoctich', string='Phần 1')
    phanhai_quoctich_kinhdoanh = fields.Many2one(comodel_name='quoctich.quoctich', string='Phần 2')
    phanba_quoctich_kinhdoanh = fields.Many2one(comodel_name='quoctich.quoctich', string='Phần 3')
    phanbon_quoctich_kinhdoanh = fields.Many2one(comodel_name='quoctich.quoctich', string='Phần 4')
    phannam_quoctich_kinhdoanh = fields.Many2one(comodel_name='quoctich.quoctich', string='Phần 5')
    phansau_quoctich_kinhdoanh = fields.Many2one(comodel_name='quoctich.quoctich', string='Phần 6')
    phanbay_quoctich_kinhdoanh = fields.Many2one(comodel_name='quoctich.quoctich', string='Phần 7')
    phantam_quoctich_kinhdoanh = fields.Many2one(comodel_name='quoctich.quoctich', string='Phần 8')

    phanmot_songuoi_kinhdoanh = fields.Integer()
    phanhai_songuoi_kinhdoanh = fields.Integer()
    phanba_songuoi_kinhdoanh = fields.Integer()
    phanbon_songuoi_kinhdoanh = fields.Integer()
    phannam_songuoi_kinhdoanh = fields.Integer()
    phansau_songuoi_kinhdoanh = fields.Integer()
    phanbay_songuoi_kinhdoanh = fields.Integer()
    phantam_songuoi_kinhdoanh = fields.Integer()

    tong_songuoi_kinhdoanh = fields.Integer(string='Tổng', compute='_tong_songuoi_kinhdoanh')

    @api.multi
    @api.depends('phanmot_songuoi_kinhdoanh', 'phanhai_songuoi_kinhdoanh', 'phanba_songuoi_kinhdoanh',
                 'phanbon_songuoi_kinhdoanh', 'phannam_songuoi_kinhdoanh', 'phansau_songuoi_kinhdoanh',
                 'phanbay_songuoi_kinhdoanh', 'phantam_songuoi_kinhdoanh')
    def _tong_songuoi_kinhdoanh(self):
        if self.phanmot_songuoi_kinhdoanh or self.phanhai_songuoi_kinhdoanh or self.phanba_songuoi_kinhdoanh or self.phanbon_songuoi_kinhdoanh or self.phannam_songuoi_kinhdoanh or self.phansau_songuoi_kinhdoanh or self.phanbay_songuoi_kinhdoanh or self.phantam_songuoi_kinhdoanh:
            self.tong_songuoi_kinhdoanh = self.phanmot_songuoi_kinhdoanh + self.phanhai_songuoi_kinhdoanh + self.phanba_songuoi_kinhdoanh + self.phanbon_songuoi_kinhdoanh + self.phannam_songuoi_kinhdoanh + self.phansau_songuoi_kinhdoanh + self.phanbay_songuoi_kinhdoanh + self.phantam_songuoi_kinhdoanh

    # 2. Số lượng nhân viên tham gia quản lý
    thuongxuyen_kinhdoanh = fields.Integer(string='Thường xuyên')  # bc_0179
    khongthuongxuyen_kinhdoanh = fields.Integer(string='Không thường xuyên')  # bc_0180
    tongcong_kinhdoanh = fields.Integer(string='Tổng', compute='_tongcong_kinhdoanh')  # bc_0181

    @api.multi
    @api.depends('thuongxuyen_kinhdoanh', 'khongthuongxuyen_kinhdoanh')
    def _tongcong_kinhdoanh(self):
        if self.thuongxuyen_kinhdoanh or self.khongthuongxuyen_kinhdoanh:
            self.tongcong_kinhdoanh = self.thuongxuyen_kinhdoanh + self.khongthuongxuyen_kinhdoanh

    # 3. Hệ thống triển khai
    quanly_kinhdoanh = fields.One2many(comodel_name='kinhdoanh.quanly', inverse_name='kinhdoanh_quanly',
                                       string='Giám sát viên tham dự khóa học')

    khac_kinhdoanh = fields.One2many(comodel_name='kinhdoanh.khac', inverse_name='kinhdoanh_khac',
                                     string='Các cán  bộ và nhân viên khác tham dự khóa học')

    # 4. Tình hình dự kì thi thực tập kĩ năng
    capdo_coban_hoanthanh_kinhdoanh = fields.Integer()  # bc_0188
    capdo_coban_soluongtts_kinhdoanh = fields.Integer()  # bc_0189
    capdo_coban_A_kinhdoanh = fields.Integer(compute='_capdo_coban_A_kinhdoanh')  # bc_0190
    capdo_coban_ttsdo_kinhdoanh = fields.Integer()  # bc_0191
    capdo_coban_tyledo_kinhdoanh = fields.Float(compute='_capdo_coban_tyledo_kinhdoanh', digits=(42, 1))  # bc_0192

    @api.multi
    @api.depends('capdo_coban_hoanthanh_kinhdoanh', 'capdo_coban_soluongtts_kinhdoanh')
    def _capdo_coban_A_kinhdoanh(self):
        if self.capdo_coban_hoanthanh_kinhdoanh or self.capdo_coban_soluongtts_kinhdoanh:
            self.capdo_coban_A_kinhdoanh = self.capdo_coban_hoanthanh_kinhdoanh - self.capdo_coban_soluongtts_kinhdoanh

    @api.multi
    @api.depends('capdo_coban_A_kinhdoanh', 'capdo_coban_ttsdo_kinhdoanh')
    def _capdo_coban_tyledo_kinhdoanh(self):
        if self.capdo_coban_A_kinhdoanh != 0:
            if self.capdo_coban_ttsdo_kinhdoanh != 0:
                self.capdo_coban_tyledo_kinhdoanh = float(
                    (self.capdo_coban_ttsdo_kinhdoanh / self.capdo_coban_A_kinhdoanh) * 100)

    capdo_ba_hoanthanh_kinhdoanh = fields.Integer()  # bc_0193
    capdo_ba_soluongtts_kinhdoanh = fields.Integer()  # bc_0194
    capdo_ba_A_kinhdoanh = fields.Integer(compute='_capdo_ba_A_kinhdoanh')  # bc_0195
    capdo_ba_ttsdo_kinhdoanh = fields.Integer()  # bc_0196
    capdo_ba_tyledo_kinhdoanh = fields.Float(compute='_capdo_ba_tyledo_kinhdoanh', digits=(42, 1))  # bc_0197

    @api.multi
    @api.depends('capdo_ba_hoanthanh_kinhdoanh', 'capdo_ba_soluongtts_kinhdoanh')
    def _capdo_ba_A_kinhdoanh(self):
        if self.capdo_ba_hoanthanh_kinhdoanh or self.capdo_ba_soluongtts_kinhdoanh:
            self.capdo_ba_A_kinhdoanh = self.capdo_ba_hoanthanh_kinhdoanh - self.capdo_ba_soluongtts_kinhdoanh

    @api.multi
    @api.depends('capdo_ba_A_kinhdoanh', 'capdo_ba_ttsdo_kinhdoanh')
    def _capdo_ba_tyledo_kinhdoanh(self):
        if self.capdo_ba_A_kinhdoanh != 0:
            if self.capdo_ba_ttsdo_kinhdoanh != 0:
                self.capdo_ba_tyledo_kinhdoanh = float(
                    (self.capdo_ba_ttsdo_kinhdoanh / self.capdo_ba_A_kinhdoanh) * 100)

    capdo_hai_hoanthanh_kinhdoanh = fields.Integer()  # bc_0198
    capdo_hai_soluongtts_kinhdoanh = fields.Integer()  # bc_0199
    capdo_hai_A_kinhdoanh = fields.Integer(compute='_capdo_hai_A_kinhdoanh')  # bc_0200
    capdo_hai_ttsdo_kinhdoanh = fields.Integer()  # bc_0201
    capdo_hai_tyledo_kinhdoanh = fields.Float(compute='_capdo_hai_tyledo_kinhdoanh', digits=(42, 1))  # bc_0202

    @api.multi
    @api.depends('capdo_hai_hoanthanh_kinhdoanh', 'capdo_hai_soluongtts_kinhdoanh')
    def _capdo_hai_A_kinhdoanh(self):
        if self.capdo_hai_hoanthanh_kinhdoanh or self.capdo_hai_soluongtts_kinhdoanh:
            self.capdo_hai_A_kinhdoanh = self.capdo_hai_hoanthanh_kinhdoanh - self.capdo_hai_soluongtts_kinhdoanh

    @api.multi
    @api.depends('capdo_hai_A_kinhdoanh', 'capdo_hai_ttsdo_kinhdoanh')
    def _capdo_hai_tyledo_kinhdoanh(self):
        if self.capdo_hai_A_kinhdoanh != 0:
            if self.capdo_hai_ttsdo_kinhdoanh != 0:
                self.capdo_hai_tyledo_kinhdoanh = float(
                    (self.capdo_hai_ttsdo_kinhdoanh / self.capdo_hai_A_kinhdoanh) * 100)

    capdo_ba_duthi_kinhdoanh = fields.Integer()  # bc_0203
    capdo_ba_do_kinhdoanh = fields.Integer()  # bc_0204
    capdo_ba_tyle_kinhdoanh = fields.Float(compute='_capdo_ba_tyle_kinhdoanh', digits=(42, 1))  # bc_0205

    @api.multi
    @api.depends('capdo_ba_duthi_kinhdoanh', 'capdo_ba_do_kinhdoanh')
    def _capdo_ba_tyle_kinhdoanh(self):
        if self.capdo_ba_duthi_kinhdoanh != 0:
            if self.capdo_ba_do_kinhdoanh != 0:
                self.capdo_ba_tyle_kinhdoanh = float(
                    (self.capdo_ba_do_kinhdoanh / self.capdo_ba_duthi_kinhdoanh) * 100)

    capdo_hai_duthi_kinhdoanh = fields.Integer()  # bc_0206
    capdo_hai_do_kinhdoanh = fields.Integer()  # bc_0207
    capdo_hai_tyle_kinhdoanh = fields.Float(compute='_capdo_hai_tyle_kinhdoanh', digits=(42, 1))  # bc_0208

    @api.multi
    @api.depends('capdo_hai_duthi_kinhdoanh', 'capdo_hai_do_kinhdoanh')
    def _capdo_hai_tyle_kinhdoanh(self):
        if self.capdo_hai_duthi_kinhdoanh != 0:
            if self.capdo_hai_do_kinhdoanh != 0:
                self.capdo_hai_tyle_kinhdoanh = float(
                    (self.capdo_hai_do_kinhdoanh / self.capdo_hai_duthi_kinhdoanh) * 100)

    # 5. Tình trạng thực tập sinh bỏ trốn
    tts_botron_kinhdoanh = fields.Integer(string='Thực tập sinh bỏ trốn')  # bc_0209
    tyle_tron_kinhdoanh = fields.Float(string='Tỷ lệ trốn', compute='_tyle_tron_kinhdoanh')  # bc_0210

    @api.multi
    @api.depends('tts_botron_kinhdoanh', 'tong_tts_kinhdoanh')
    def _tyle_tron_kinhdoanh(self):
        if self.tts_botron_kinhdoanh and self.tong_tts_kinhdoanh:
            self.tyle_tron_kinhdoanh = self.tts_botron_kinhdoanh/self.tong_tts_kinhdoanh * 100

    # 6. Tiếp nhận tts gặp khó khăn trong việc tiếp tục đào tạo thực tập tại công ty khác
    songuoi_kinhdoanh = fields.Integer(string='Số người')  # bc_0211
    hotro_web_kinhdoanh = fields.Selection(string='Có đăng kí trang web hỗ trợ đổi nơi thực tập',
                                           selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')  # bc_0212

    #  7. Thực hiện sáng kiến để cùng giao lưu với địa phương
    hotro_nhat_kinhdoanh = fields.Text(string='Hỗ trợ học tiếng Nhật')  # bc_0213
    cohoi_tuongtac_kinhdoanh = fields.Text(string='Có tương tác với cộng đồng địa phương')  # bc_0214
    cohoi_hochoi_kinhdoanh = fields.Text(string='Có cơ hội học hỏi văn hóa Nhật')  # bc_0215

    # 8. Tình trạng thu phí giám sát
    soluong_xinghiep_kinhdoanh = fields.Integer(string='Số lượng xí nghiệp đã thu')  # bc_0216
    sotien_mottts_1go_kinhdoanh = fields.Integer(string='1 Go')  # bc_0217
    sotien_mottts_2go_kinhdoanh = fields.Integer(string='2 Go')  # bc_0218
    sotien_mottts_3go_kinhdoanh = fields.Integer(string='3 Go')  # bc_0219

    # Chi tiết
    mot_tongtien_thuduoc = fields.Integer(compute='_mot_tongtien_thuduoc')  # bc_0220
    mot_tongtien_chi = fields.Integer(compute='_mot_tongtien_chi')  # bc_0221

    @api.multi
    @api.depends('hai_tong_thu', 'ba_tong_thu', 'bon_tong_thu', 'nam_tong_thu')
    def _mot_tongtien_thuduoc(self):
        if self.hai_tong_thu or self.ba_tong_thu or self.bon_tong_thu or self.nam_tong_thu:
            self.mot_tongtien_thuduoc = self.hai_tong_thu + self.ba_tong_thu + self.bon_tong_thu + self.nam_tong_thu

    @api.multi
    @api.depends('hai_tong_chi', 'ba_tong_chi', 'bon_tong_chi', 'nam_tong_chi')
    def _mot_tongtien_chi(self):
        if self.hai_tong_chi or self.ba_tong_chi or self.bon_tong_chi or self.nam_tong_chi:
            self.mot_tongtien_chi = self.hai_tong_chi + self.ba_tong_chi + self.bon_tong_chi + self.nam_tong_chi

    hai_tong_thu = fields.Integer(compute='_hai_tong_thu')  # bc_0222
    hai_tong_chi = fields.Integer(compute='_hai_tong_chi')  # bc_0223
    hai_laodong_thu = fields.Integer()  # bc_0224
    hai_laodong_chi = fields.Integer()  # bc_0225
    hai_vanchuyen_thu = fields.Integer()  # bc_0226
    hai_vanchuyen_chi = fields.Integer()  # bc_0227
    hai_phaicu_thu = fields.Integer()  # bc_0228
    hai_phaicu_chi = fields.Integer()  # bc_0229
    hai_khac = fields.Char()  # bc_0230
    hai_khac_thu = fields.Integer()  # bc_0231
    hai_khac_chi = fields.Integer()  # bc_0232

    @api.multi
    @api.depends('hai_laodong_thu', 'hai_vanchuyen_thu', 'hai_phaicu_thu', 'hai_khac_thu')
    def _hai_tong_thu(self):
        if self.hai_laodong_thu or self.hai_vanchuyen_thu or self.hai_phaicu_thu or self.hai_khac_thu:
            self.hai_tong_thu = self.hai_laodong_thu + self.hai_vanchuyen_thu + self.hai_phaicu_thu + self.hai_khac_thu

    @api.multi
    @api.depends('hai_laodong_chi', 'hai_vanchuyen_chi', 'hai_phaicu_chi', 'hai_khac_chi')
    def _hai_tong_chi(self):
        if self.hai_laodong_chi or self.hai_vanchuyen_chi or self.hai_phaicu_chi or self.hai_khac_chi:
            self.hai_tong_chi = self.hai_laodong_chi + self.hai_vanchuyen_chi + self.hai_phaicu_chi + self.hai_khac_chi

    bon_tong_thu = fields.Integer(compute='_bon_tong_thu')  # bc_0246
    bon_tong_chi = fields.Integer(compute='_bon_tong_chi')  # bc_0247

    bon_laodong_thu = fields.Integer()  # bc_0248
    bon_laodong_chi = fields.Integer()  # bc_0249
    bon_vanchuyen_thu = fields.Integer()  # bc_0250
    bon_vanchuyen_chi = fields.Integer()  # bc_0251
    bon_khac = fields.Char()  # bc_0252
    bon_khac_thu = fields.Integer()  # bc_0253
    bon_khac_chi = fields.Integer()  # bc_0254

    @api.multi
    @api.depends('bon_laodong_thu', 'bon_vanchuyen_thu', 'bon_khac_thu')
    def _bon_tong_thu(self):
        if self.bon_laodong_thu or self.bon_vanchuyen_thu or self.bon_khac_thu:
            self.bon_tong_thu = self.bon_laodong_thu + self.bon_vanchuyen_thu + self.bon_khac_thu

    @api.multi
    @api.depends('bon_laodong_chi', 'bon_vanchuyen_chi', 'bon_khac_chi')
    def _bon_tong_chi(self):
        if self.bon_laodong_chi or self.bon_vanchuyen_chi or self.bon_khac_chi:
            self.bon_tong_chi = self.bon_laodong_chi + self.bon_vanchuyen_chi + self.bon_khac_chi

    ba_tong_thu = fields.Integer(compute='_ba_tong_thu')  # bc_0233
    ba_tong_chi = fields.Integer(compute='_ba_tong_chi')  # bc_0234

    ba_coso_thu = fields.Integer()  # bc_0235
    ba_coso_chi = fields.Integer()  # bc_0236
    ba_camon_thu = fields.Integer()  # bc_0237
    ba_camon_chi = fields.Integer()  # bc_0238
    ba_tailieu_thu = fields.Integer()  # bc_0239
    ba_tailieu_chi = fields.Integer()  # bc_0240
    ba_trocap_thu = fields.Integer()  # bc_0241
    ba_trocap_chi = fields.Integer()  # bc_0242
    ba_khac = fields.Char()  # bc_0243
    ba_khac_thu = fields.Integer()  # bc_0244
    ba_khac_chi = fields.Integer()  # bc_0245

    @api.multi
    @api.depends('ba_coso_thu', 'ba_camon_thu', 'ba_tailieu_thu', 'ba_trocap_thu', 'ba_khac_thu')
    def _ba_tong_thu(self):
        if self.ba_coso_thu or self.ba_camon_thu or self.ba_tailieu_thu or self.ba_trocap_thu or self.ba_khac_thu:
            self.ba_tong_thu = self.ba_coso_thu + self.ba_camon_thu + self.ba_tailieu_thu + self.ba_trocap_thu + self.ba_khac_thu

    @api.multi
    @api.depends('ba_coso_chi', 'ba_camon_chi', 'ba_tailieu_chi', 'ba_trocap_chi', 'ba_khac_chi')
    def _ba_tong_chi(self):
        if self.ba_coso_chi or self.ba_camon_chi or self.ba_tailieu_chi or self.ba_trocap_chi or self.ba_khac_chi:
            self.ba_tong_chi = self.ba_coso_chi + self.ba_camon_chi + self.ba_tailieu_chi + self.ba_trocap_chi + self.ba_khac_chi

    nam_tong_thu = fields.Integer(compute='_nam_tong_thu')  # bc_0255
    nam_tong_chi = fields.Integer(compute='_nam_tong_chi')  # bc_0256

    nam_khac_mot = fields.Char()  # bc_0257
    nam_khac_mot_chi = fields.Integer()  # bc_0258
    nam_khac_mot_thu = fields.Integer()  # bc_0259
    nam_khac_hai = fields.Char()  # bc_0260
    nam_khac_hai_chi = fields.Integer()  # bc_0261
    nam_khac_hai_thu = fields.Integer()  # bc_0262
    nam_khac_ba = fields.Char()  # bc_0263
    nam_khac_ba_chi = fields.Integer()  # bc_0264
    nam_khac_ba_thu = fields.Integer()  # bc_0265

    @api.multi
    @api.depends('nam_khac_mot_thu', 'nam_khac_hai_thu', 'nam_khac_ba_thu')
    def _nam_tong_thu(self):
        if self.nam_khac_mot_thu or self.nam_khac_hai_thu or self.nam_khac_ba_thu:
            self.nam_tong_thu = self.nam_khac_mot_thu + self.nam_khac_hai_thu + self.nam_khac_ba_thu

    @api.multi
    @api.depends('nam_khac_mot_chi', 'nam_khac_hai_chi', 'nam_khac_ba_chi')
    def _nam_tong_chi(self):
        if self.nam_khac_mot_chi or self.nam_khac_hai_chi or self.nam_khac_ba_chi:
            self.nam_tong_chi = self.nam_khac_mot_chi + self.nam_khac_hai_chi + self.nam_khac_ba_chi

    # 9. Ghi chú
    ghichu_kinhdoanh = fields.Text(string='Ghi chú')

    @api.multi
    def baocao_kinhdoanh_button(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/kinhdoanh/download/%s' % (self.id),
            'target': 'self', }

    @api.multi
    def baocao_kinhdoanh_36(self, kinhdoanh):
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_36")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            nghiepdoan = self.env['nghiepdoan.nghiepdoan'].search([('id', '=', 1)], limit=1)

            context['hhjp_0193'] = convert_date(kinhdoanh.ngay_baocao)
            context['hhjp_0097'] = kiemtra(nghiepdoan.ten_han_nghiepdoan)
            context['bc_0159'] = kiemtra(nghiepdoan.chucvu_daidien_phienam_nghiepdoan)
            context['hhjp_0103'] = kiemtra(nghiepdoan.ten_daidien_han_nghiepdoan)

            context['bc_0169'] = kiemtra(kinhdoanh.nam_daotao_kinhdoanh)
            context['bc_0170'] = kiemtra(int(kinhdoanh.nam_daotao_kinhdoanh) + 1)

            context['hhjp_0100'] = kiemtra(nghiepdoan.so_giayphep_nghiepdoan)
            context['hhjp_0096'] = kiemtra(nghiepdoan.ten_phienam_nghiepdoan)

            context['bs_0007'] = nghiepdoan.buudien_nghiepdoan[0:3] + '-' + nghiepdoan.buudien_nghiepdoan[3:]
            context['hhjp_0098'] = kiemtra(nghiepdoan.diachi_nghiepdoan)
            context['hhjp_0099'] = kiemtra(nghiepdoan.dienthoai_nghiepdoan)
            context['hhjp_0106'] = kiemtra(kinhdoanh.vanphong_kinhdoanh.ten_phienam_chinhanh)
            context['hhjp_0107'] = kiemtra(kinhdoanh.vanphong_kinhdoanh.ten_han_chinhanh)
            context['bs_0008'] = kinhdoanh.vanphong_kinhdoanh.buudien_chinhanh[
                                 0:3] + '-' + kinhdoanh.vanphong_kinhdoanh.buudien_chinhanh[3:]
            context['hhjp_0108'] = kiemtra(kinhdoanh.vanphong_kinhdoanh.dichi_chinhanh)
            context['hhjp_0109'] = kiemtra(kinhdoanh.vanphong_kinhdoanh.dienthoai_chinhanh)
            context['bc_0171'] = kiemtra(kinhdoanh.so_chinhanh_kinhdoanh)

            context['bc_0172'] = kiemtra(kinhdoanh.soluong_tts_kinhdoanh)
            context['bc_0173'] = kiemtra(kinhdoanh.tong_tts_kinhdoanh)
            context['bc_0174'] = kiemtra(kinhdoanh.go1_tss_kinhdoanh)
            context['bc_0175'] = kiemtra(kinhdoanh.go2_tss_kinhdoanh)
            context['bc_0176'] = kiemtra(kinhdoanh.go3_tss_kinhdoanh)

            table_quoctich = []
            if kinhdoanh.phanmot_quoctich_kinhdoanh:
                info1 = {}
                info1['bc_0177'] = kiemtra(kinhdoanh.phanmot_quoctich_kinhdoanh.name_quoctich)
                info1['bc_0178'] = kiemtra(kinhdoanh.phanmot_songuoi_kinhdoanh)
                table_quoctich.append(info1)
            if kinhdoanh.phanhai_quoctich_kinhdoanh:
                info2 = {}
                info2['bc_0177'] = kiemtra(kinhdoanh.phanhai_quoctich_kinhdoanh.name_quoctich)
                info2['bc_0178'] = kiemtra(kinhdoanh.phanhai_songuoi_kinhdoanh)
                table_quoctich.append(info2)
            if kinhdoanh.phanba_quoctich_kinhdoanh:
                info3 = {}
                info3['bc_0177'] = kiemtra(kinhdoanh.phanba_quoctich_kinhdoanh.name_quoctich)
                info3['bc_0178'] = kiemtra(kinhdoanh.phanba_songuoi_kinhdoanh)
                table_quoctich.append(info3)
            if kinhdoanh.phanbon_quoctich_kinhdoanh:
                info4 = {}
                info4['bc_0177'] = kiemtra(kinhdoanh.phanbon_quoctich_kinhdoanh.name_quoctich)
                info4['bc_0178'] = kiemtra(kinhdoanh.phanbon_songuoi_kinhdoanh)
                table_quoctich.append(info4)
            if kinhdoanh.phannam_quoctich_kinhdoanh:
                info5 = {}
                info5['bc_0177'] = kiemtra(kinhdoanh.phannam_quoctich_kinhdoanh.name_quoctich)
                info5['bc_0178'] = kiemtra(kinhdoanh.phannam_quoctich_kinhdoanh)
                table_quoctich.append(info5)
            if kinhdoanh.phansau_quoctich_kinhdoanh:
                info6 = {}
                info6['bc_0177'] = kiemtra(kinhdoanh.phansau_quoctich_kinhdoanh.name_quoctich)
                info6['bc_0178'] = kiemtra(kinhdoanh.phansau_songuoi_kinhdoanh)
                table_quoctich.append(info6)
            if kinhdoanh.phanbay_quoctich_kinhdoanh:
                info7 = {}
                info7['bc_0177'] = kiemtra(kinhdoanh.phanbay_quoctich_kinhdoanh.name_quoctich)
                info7['bc_0178'] = kiemtra(kinhdoanh.phanbay_songuoi_kinhdoanh)
                table_quoctich.append(info7)
            if kinhdoanh.phantam_quoctich_kinhdoanh:
                info8 = {}
                info8['bc_0177'] = kiemtra(kinhdoanh.phantam_quoctich_kinhdoanh.name_quoctich)
                info8['bc_0178'] = kiemtra(kinhdoanh.phantam_songuoi_kinhdoanh)
                table_quoctich.append(info8)

            context['tbl_qttts'] = table_quoctich

            table_quanly = []
            for quanly_kinhdoanh in kinhdoanh.quanly_kinhdoanh:
                info = {}
                info['bc_0182'] = kiemtra(quanly_kinhdoanh.hoten_quanly)
                info['bc_0183_1'] = kiemtra(quanly_kinhdoanh.motkhoahoc_quanly)
                info['bc_0184_1'] = convert_date(quanly_kinhdoanh.motngay_thamdu_quanly)
                info['bc_0183_2'] = kiemtra(quanly_kinhdoanh.haikhoahoc_quanly)
                info['bc_0184_2'] = convert_date(quanly_kinhdoanh.haingay_thamdu_quanly)
                info['bc_0183_3'] = kiemtra(quanly_kinhdoanh.bakhoahoc_quanly)
                info['bc_0184_3'] = convert_date(quanly_kinhdoanh.bangay_thamdu_quanly)
                table_quanly.append(info)
            context['tbl_qltts'] = table_quanly

            table_quanlykhac = []
            for khac_kinhdoanh in kinhdoanh.khac_kinhdoanh:
                info1 = {}
                info1['bc_0185'] = kiemtra(khac_kinhdoanh.hoten_khac)
                info1['bc_0186_1'] = kiemtra(khac_kinhdoanh.motkhoahoc_khac)
                info1['bc_0187_1'] = convert_date(khac_kinhdoanh.motngay_thamdu_khac)
                info1['bc_0186_2'] = kiemtra(khac_kinhdoanh.haikhoahoc_khac)
                info1['bc_0187_2'] = convert_date(khac_kinhdoanh.haingay_thamdu_khac)
                info1['bc_0186_3'] = kiemtra(khac_kinhdoanh.bakhoahoc_khac)
                info1['bc_0187_3'] = convert_date(khac_kinhdoanh.bangay_thamdu_khac)
                table_quanlykhac.append(info1)
            context['tbl_qlkhac'] = table_quanlykhac

            context['bc_0181'] = kiemtra(kinhdoanh.tongcong_kinhdoanh)
            context['bc_0179'] = kiemtra(kinhdoanh.thuongxuyen_kinhdoanh)
            context['bc_0180'] = kiemtra(kinhdoanh.khongthuongxuyen_kinhdoanh)

            context['bc_0188'] = kiemtra(kinhdoanh.capdo_coban_hoanthanh_kinhdoanh)
            context['bc_0189'] = kiemtra(kinhdoanh.capdo_coban_soluongtts_kinhdoanh)
            context['bc_0190'] = kiemtra(kinhdoanh.capdo_coban_A_kinhdoanh)
            context['bc_0191'] = kiemtra(kinhdoanh.capdo_coban_ttsdo_kinhdoanh)
            context['bc_0192'] = kiemtra(round(kinhdoanh.capdo_coban_tyledo_kinhdoanh,2))

            context['bc_0193'] = kiemtra(kinhdoanh.capdo_ba_hoanthanh_kinhdoanh)
            context['bc_0194'] = kiemtra(kinhdoanh.capdo_ba_soluongtts_kinhdoanh)
            context['bc_0195'] = kiemtra(kinhdoanh.capdo_ba_A_kinhdoanh)
            context['bc_0196'] = kiemtra(kinhdoanh.capdo_ba_ttsdo_kinhdoanh)
            context['bc_0197'] = kiemtra(round(kinhdoanh.capdo_ba_tyledo_kinhdoanh,2))

            context['bc_0198'] = kiemtra(kinhdoanh.capdo_hai_hoanthanh_kinhdoanh)
            context['bc_0199'] = kiemtra(kinhdoanh.capdo_hai_soluongtts_kinhdoanh)
            context['bc_0200'] = kiemtra(kinhdoanh.capdo_hai_A_kinhdoanh)
            context['bc_0201'] = kiemtra(kinhdoanh.capdo_hai_ttsdo_kinhdoanh)
            context['bc_0202'] = kiemtra(round(kinhdoanh.capdo_hai_tyledo_kinhdoanh,2))

            context['bc_0203'] = kiemtra(kinhdoanh.capdo_ba_duthi_kinhdoanh)
            context['bc_0204'] = kiemtra(kinhdoanh.capdo_ba_do_kinhdoanh)
            context['bc_0205'] = kiemtra(kinhdoanh.capdo_ba_tyle_kinhdoanh)

            context['bc_0206'] = kiemtra(kinhdoanh.capdo_hai_duthi_kinhdoanh)
            context['bc_0207'] = kiemtra(kinhdoanh.capdo_hai_do_kinhdoanh)
            context['bc_0208'] = kiemtra(kinhdoanh.capdo_hai_tyle_kinhdoanh)

            context['bc_0209'] = kiemtra(kinhdoanh.tts_botron_kinhdoanh)
            context['bc_0210'] = kiemtra(round(kinhdoanh.tyle_tron_kinhdoanh,2))
            context['bc_0211'] = kiemtra(kinhdoanh.songuoi_kinhdoanh)

            if kinhdoanh.hotro_web_kinhdoanh == u'Có':
                docdata.remove_shape(u'id="2" name="Oval 2"')
            elif kinhdoanh.hotro_web_kinhdoanh == u'Không':
                docdata.remove_shape(u'id="1" name="Oval 1"')
            else:
                docdata.remove_shape(u'id="2" name="Oval 2"')
                docdata.remove_shape(u'id="1" name="Oval 1"')

            context['bc_0213'] = kiemtra(kinhdoanh.hotro_nhat_kinhdoanh)
            context['bc_0214'] = kiemtra(kinhdoanh.cohoi_tuongtac_kinhdoanh)
            context['bc_0215'] = kiemtra(kinhdoanh.cohoi_hochoi_kinhdoanh)

            context['bc_0216'] = kiemtra(kinhdoanh.soluong_xinghiep_kinhdoanh)
            context['bc_0217'] = kiemtra(kinhdoanh.sotien_mottts_1go_kinhdoanh)
            context['bc_0218'] = kiemtra(kinhdoanh.sotien_mottts_2go_kinhdoanh)
            context['bc_0219'] = kiemtra(kinhdoanh.sotien_mottts_3go_kinhdoanh)

            context['bc_0220'] = place_value(kinhdoanh.mot_tongtien_thuduoc)
            context['bc_0221'] = place_value(kinhdoanh.mot_tongtien_chi)
            context['bc_0222'] = place_value(kinhdoanh.hai_tong_thu)
            context['bc_0223'] = place_value(kinhdoanh.hai_tong_chi)
            context['bc_0224'] = place_value(kinhdoanh.hai_laodong_thu)
            context['bc_0225'] = place_value(kinhdoanh.hai_laodong_chi)
            context['bc_0226'] = place_value(kinhdoanh.hai_vanchuyen_thu)
            context['bc_0227'] = place_value(kinhdoanh.hai_vanchuyen_chi)
            context['bc_0228'] = place_value(kinhdoanh.hai_phaicu_thu)
            context['bc_0229'] = place_value(kinhdoanh.hai_phaicu_chi)
            context['bc_0230'] = kiemtra(kinhdoanh.hai_khac)
            context['bc_0231'] = place_value(kinhdoanh.hai_khac_thu)
            context['bc_0232'] = place_value(kinhdoanh.hai_khac_chi)

            context['bc_0233'] = place_value(kinhdoanh.ba_tong_thu)
            context['bc_0234'] = place_value(kinhdoanh.ba_tong_chi)
            context['bc_0235'] = place_value(kinhdoanh.ba_coso_thu)
            context['bc_0236'] = place_value(kinhdoanh.ba_coso_chi)
            context['bc_0237'] = place_value(kinhdoanh.ba_camon_thu)
            context['bc_0238'] = place_value(kinhdoanh.ba_camon_chi)
            context['bc_0239'] = place_value(kinhdoanh.ba_tailieu_thu)
            context['bc_0240'] = place_value(kinhdoanh.ba_tailieu_chi)
            context['bc_0241'] = place_value(kinhdoanh.ba_trocap_thu)
            context['bc_0242'] = place_value(kinhdoanh.ba_trocap_chi)
            context['bc_0243'] = kiemtra(kinhdoanh.ba_khac)
            context['bc_0244'] = place_value(kinhdoanh.ba_khac_thu)
            context['bc_0245'] = place_value(kinhdoanh.ba_khac_chi)

            context['bc_0246'] = place_value(kinhdoanh.bon_tong_thu)
            context['bc_0247'] = place_value(kinhdoanh.bon_tong_chi)
            context['bc_0248'] = place_value(kinhdoanh.bon_laodong_thu)
            context['bc_0249'] = place_value(kinhdoanh.bon_laodong_chi)
            context['bc_0250'] = place_value(kinhdoanh.bon_vanchuyen_thu)
            context['bc_0251'] = place_value(kinhdoanh.bon_vanchuyen_chi)
            context['bc_0252'] = kiemtra(kinhdoanh.bon_khac)
            context['bc_0253'] = place_value(kinhdoanh.bon_khac_thu)
            context['bc_0254'] = place_value(kinhdoanh.bon_khac_chi)

            context['bc_0255'] = place_value(kinhdoanh.nam_tong_thu)
            context['bc_0256'] = place_value(kinhdoanh.nam_tong_chi)
            context['bc_0257'] = kiemtra(kinhdoanh.nam_khac_mot)
            context['bc_0258'] = place_value(kinhdoanh.nam_khac_mot_chi)
            context['bc_0259'] = place_value(kinhdoanh.nam_khac_mot_thu)
            context['bc_0260'] = kiemtra(kinhdoanh.nam_khac_hai)
            context['bc_0261'] = place_value(kinhdoanh.nam_khac_hai_chi)
            context['bc_0262'] = place_value(kinhdoanh.nam_khac_hai_thu)
            context['bc_0263'] = kiemtra(kinhdoanh.nam_khac_ba)
            context['bc_0264'] = place_value(kinhdoanh.nam_khac_ba_chi)
            context['bc_0265'] = place_value(kinhdoanh.nam_khac_ba_thu)

            context['bc_0266'] = Listing(kinhdoanh.ghichu_kinhdoanh) if kinhdoanh.ghichu_kinhdoanh else ''

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None
