# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime
import logging
from tempfile import TemporaryFile, NamedTemporaryFile
from io import BytesIO, StringIO
import os
import codecs
from docxtpl import DocxTemplate
from docxtpl import DocxTemplate, Listing

_logger = logging.getLogger(__name__)


def kiemtra(data):
    if data:
        return data
    else:
        return ''


def convert_date(self):
    if self:
        return str(self.year) + '年' + \
               str(self.month) + '月' + \
               str(self.day) + '日'
    else:
        return '年　月　日'


def place_value(number):
    if number:
        return ("{:,}".format(number))
    else:
        return ''


class ChamDiem(models.Model):
    _name = 'xinghiep.chamdiem'
    _rec_name = 'xinghiep_chamdiem'

    xinghiep_chamdiem = fields.Many2one('xinghiep.xinghiep', string='Xí nghiệp')
    ngaylap_vanban_chamdiem = fields.Date(string='Ngày lập văn bản',  default=fields.Date.today())
    thaythe_chamdiem = fields.Boolean(string='Sử dụng II2 thay vì II')

    # Bảng 1
    hoanthanh_hientai_1 = fields.Integer()  # xn_0002
    kiemtra_hientai_1 = fields.Integer()  # xn_0003
    soluong_kiemtra_hientai_1 = fields.Integer(compute='_soluong_kiemtra_hientai_1')  # xn_0004

    @api.multi
    @api.depends('hoanthanh_hientai_1', 'kiemtra_hientai_1')
    def _soluong_kiemtra_hientai_1(self):
        if self.hoanthanh_hientai_1 or self.kiemtra_hientai_1:
            self.soluong_kiemtra_hientai_1 = self.hoanthanh_hientai_1 - self.kiemtra_hientai_1

    ungvien_thanhcong_hientai_1 = fields.Integer()  # xn_0009

    hoanthanh_cu_1 = fields.Integer()  # xn_0005
    kiemtra_cu_1 = fields.Integer()  # xn_0006
    soluong_kiemtra_cu_1 = fields.Integer(compute='_soluong_kiemtra_cu_1')  # xn_0007

    @api.multi
    @api.depends('hoanthanh_cu_1', 'kiemtra_cu_1')
    def _soluong_kiemtra_cu_1(self):
        if self.hoanthanh_cu_1 or self.kiemtra_cu_1:
            self.soluong_kiemtra_cu_1 = self.hoanthanh_cu_1 - self.kiemtra_cu_1

    ungvien_thanhcong_cu_1 = fields.Integer()
    soluong_kiemtra_tongphu_1 = fields.Integer(compute='_soluong_kiemtra_tongphu_1')  # xn_0001

    @api.multi
    @api.depends('soluong_kiemtra_hientai_1', 'soluong_kiemtra_cu_1')
    def _soluong_kiemtra_tongphu_1(self):
        if self.soluong_kiemtra_hientai_1 or self.soluong_kiemtra_cu_1:
            self.soluong_kiemtra_tongphu_1 = self.soluong_kiemtra_hientai_1 + self.soluong_kiemtra_cu_1

    ungvien_thanhcong_tongphu_1 = fields.Integer(compute='_ungvien_thanhcong_tongphu_1')  # xn_0008

    @api.multi
    @api.depends('ungvien_thanhcong_hientai_1', 'ungvien_thanhcong_cu_1')  # xn_0010
    def _ungvien_thanhcong_tongphu_1(self):
        if self.ungvien_thanhcong_hientai_1 or self.ungvien_thanhcong_cu_1:
            self.ungvien_thanhcong_tongphu_1 = self.ungvien_thanhcong_hientai_1 + self.ungvien_thanhcong_cu_1

    hoanthanh_hientai_2 = fields.Integer()  # xn_0014
    kiemtra_hientai_2 = fields.Integer()  # xn_0015
    soluong_kiemtra_hientai_2 = fields.Integer(compute='_soluong_kiemtra_hientai_2')  # xn_0016

    @api.multi
    @api.depends('hoanthanh_hientai_2', 'kiemtra_hientai_2')
    def _soluong_kiemtra_hientai_2(self):
        if self.hoanthanh_hientai_2 or self.kiemtra_hientai_2:
            self.soluong_kiemtra_hientai_2 = self.hoanthanh_hientai_2 - self.kiemtra_hientai_2

    thisinh_thanhcong_hientai_2 = fields.Integer()
    vuotqua_kiemtra_hientai_2 = fields.Integer()  # xn_0023

    soluong_kiemtra_cu_2 = fields.Integer()  # xn_0017
    thisinh_thanhcong_cu_2 = fields.Integer()
    vuotqua_kiemtra_cu_2 = fields.Integer()  # xn_0024

    soluong_kiemtra_tongphu_2 = fields.Integer(compute='_soluong_kiemtra_tongphu_2')  # xn_0013

    @api.multi
    @api.depends('soluong_kiemtra_hientai_2', 'soluong_kiemtra_cu_2')
    def _soluong_kiemtra_tongphu_2(self):
        if self.soluong_kiemtra_hientai_2 or self.soluong_kiemtra_cu_2:
            self.soluong_kiemtra_tongphu_2 = self.soluong_kiemtra_hientai_2 + self.soluong_kiemtra_cu_2

    thisinh_thanhcong_tongphu_2 = fields.Integer(compute='_thisinh_thanhcong_tongphu_2')

    @api.multi
    @api.depends('thisinh_thanhcong_hientai_2', 'thisinh_thanhcong_cu_2')
    def _thisinh_thanhcong_tongphu_2(self):
        if self.thisinh_thanhcong_hientai_2 or self.thisinh_thanhcong_cu_2:
            self.thisinh_thanhcong_tongphu_2 = self.thisinh_thanhcong_hientai_2 + self.thisinh_thanhcong_cu_2

    vuotqua_kiemtra_tongphu_2 = fields.Integer(compute='_vuotqua_kiemtra_tongphu_2')  # xn_0022

    @api.multi
    @api.depends('vuotqua_kiemtra_hientai_2', 'vuotqua_kiemtra_cu_2')
    def _vuotqua_kiemtra_tongphu_2(self):
        if self.vuotqua_kiemtra_hientai_2 or self.vuotqua_kiemtra_cu_2:
            self.vuotqua_kiemtra_tongphu_2 = self.vuotqua_kiemtra_hientai_2 + self.vuotqua_kiemtra_cu_2

    hoanthanh_hientai_3 = fields.Integer()  # xn_0018
    kiemtra_hientai_3 = fields.Integer()  # xn_0019
    soluong_kiemtra_hientai_3 = fields.Integer(compute='_soluong_kiemtra_hientai_3')  # xn_0020


    tong_vuotqua_kiemtra_hientai = fields.Integer(string="Ⅱ②",compute='_tong_vuotqua_kiemtra_hientai')

    @api.multi
    @api.depends('vuotqua_kiemtra_tongphu_2', 'vuotqua_kiemtra_hientai_3')
    def _tong_vuotqua_kiemtra_hientai(self):
        if self.vuotqua_kiemtra_hientai_3 or self.vuotqua_kiemtra_tongphu_2:
            self.tong_vuotqua_kiemtra_hientai = self.vuotqua_kiemtra_tongphu_2 + self.vuotqua_kiemtra_hientai_3

    @api.onchange('field_name')
    def onchange_method(self):
        self.field_name = ''

    @api.multi
    @api.depends('hoanthanh_hientai_3', 'kiemtra_hientai_3')
    def _soluong_kiemtra_hientai_3(self):
        if self.hoanthanh_hientai_3 or self.kiemtra_hientai_3:
            self.soluong_kiemtra_hientai_3 = self.hoanthanh_hientai_3 - self.kiemtra_hientai_3

    thisinh_thanhcong_hientai_3 = fields.Integer()
    vuotqua_kiemtra_hientai_3 = fields.Integer()  # xn_0025

    soluong_tong = fields.Integer(compute='_soluong_tong')  # xn_0012

    @api.multi
    @api.depends('soluong_kiemtra_tongphu_2', 'soluong_kiemtra_hientai_3')
    def _soluong_tong(self):
        if self.soluong_kiemtra_tongphu_2 or self.soluong_kiemtra_hientai_3:
            self.soluong_tong = self.soluong_kiemtra_tongphu_2 + self.soluong_kiemtra_hientai_3

    tong_thanhcong = fields.Integer(compute='_tong_thanhcong')  # xn_0030

    @api.multi
    @api.depends('thisinh_thanhcong_tongphu_2', 'thisinh_thanhcong_hientai_3')
    def _tong_thanhcong(self):
        if self.thisinh_thanhcong_tongphu_2 or self.thisinh_thanhcong_hientai_3:
            self.tong_thanhcong = self.thisinh_thanhcong_tongphu_2 + self.thisinh_thanhcong_hientai_3

    kithi_coban_I2 = fields.Integer(related='ungvien_thanhcong_tongphu_1')
    kithi_coban_I1 = fields.Integer(related='soluong_kiemtra_tongphu_1')
    tile_kithi_coban = fields.Float(compute='_tile_kithi_coban')  # xn_0011

    @api.multi
    @api.depends('kithi_coban_I2', 'kithi_coban_I1')
    def _tile_kithi_coban(self):
        if self.kithi_coban_I2 and self.kithi_coban_I1:
            self.tile_kithi_coban = round((self.kithi_coban_I2 / self.kithi_coban_I1) * 100)

    kithi_lop_coban_I2 = fields.Float(compute='_kithi_lop_coban_I2')  # xn_0026

    @api.multi
    @api.depends('vuotqua_kiemtra_tongphu_2', 'vuotqua_kiemtra_hientai_3')
    def _kithi_lop_coban_I2(self):
        if self.vuotqua_kiemtra_tongphu_2 or self.vuotqua_kiemtra_hientai_3:
            print(self.vuotqua_kiemtra_tongphu_2 + self.vuotqua_kiemtra_hientai_3 * 1.5)
            print(round(self.vuotqua_kiemtra_tongphu_2 + self.vuotqua_kiemtra_hientai_3 * 1.5,1))
            self.kithi_lop_coban_I2 = round((self.vuotqua_kiemtra_tongphu_2 + self.vuotqua_kiemtra_hientai_3 * 1.5),1)

    kithi_lop_coban_I1 = fields.Integer(compute='_kithi_lop_coban_I1')

    @api.multi
    @api.depends('soluong_tong')
    def _kithi_lop_coban_I1(self):
        if self.soluong_tong:
            self.kithi_lop_coban_I1 = self.soluong_tong

    tile_lop_kithi_coban = fields.Float(compute='_tile_lop_kithi_coban')  # xn_0027

    @api.multi
    @api.depends('kithi_lop_coban_I2', 'kithi_lop_coban_I1')
    def _tile_lop_kithi_coban(self):
        if self.kithi_lop_coban_I2 and self.kithi_lop_coban_I1:
            self.tile_lop_kithi_coban = round((((self.kithi_lop_coban_I2 * 1.2) / self.kithi_lop_coban_I1)) * 100)

    ketqua_kiemtra_1 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')  # xn_0034
    noidung_kiemtra_a_1 = fields.Text()  # xn_0031
    noidung_kiemtra_b_1 = fields.Text()  # xn_0032
    noidung_kiemtra_c_1 = fields.Text()  # xn_0033

    diem_1_1_chamdiem = fields.Integer(compute='_diem_1_1_chamdiem')  # xn_0071

    @api.multi
    @api.depends('tile_kithi_coban')
    def _diem_1_1_chamdiem(self):
        if self.tile_kithi_coban >= 95:
            self.diem_1_1_chamdiem = 20
        elif self.tile_kithi_coban >= 80 and self.tile_kithi_coban < 95:
            self.diem_1_1_chamdiem = 10
        elif self.tile_kithi_coban >= 75 and self.tile_kithi_coban < 80:
            self.diem_1_1_chamdiem = 0
        elif self.tile_kithi_coban < 75:
            self.diem_1_1_chamdiem = -20

    dat_1_1_chamdiem = fields.Integer(default=20)
    khong_1_1_chamdiem = fields.Integer(default=-20)
    diem_2_1_chamdiem = fields.Integer(compute='_diem_2_1_chamdiem')  # xn_0072

    @api.multi
    @api.depends('tile_lop_kithi_coban')
    def _diem_2_1_chamdiem(self):
        if self.tile_lop_kithi_coban >= 80:
            self.diem_2_1_chamdiem = 40
        elif self.tile_lop_kithi_coban >= 70 and self.tile_lop_kithi_coban < 80:
            self.diem_2_1_chamdiem = 30
        elif self.tile_lop_kithi_coban >= 60 and self.tile_lop_kithi_coban < 70:
            self.diem_2_1_chamdiem = 20
        elif self.tile_lop_kithi_coban >= 50 and self.tile_lop_kithi_coban < 60:
            self.diem_2_1_chamdiem = 0
        elif self.tile_lop_kithi_coban < 50:
            self.diem_2_1_chamdiem = -40

    dat_2_1_chamdiem = fields.Integer(default=40)
    khong_2_1_chamdiem = fields.Integer(default=-40)
    diem_3_1_chamdiem = fields.Integer(compute='_diem_3_1_chamdiem')  # xn_0073

    @api.multi
    @api.depends('vuotqua_kiemtra_tongphu_2')
    def _diem_3_1_chamdiem(self):
        if self.vuotqua_kiemtra_tongphu_2 == 0:
            self.diem_3_1_chamdiem = -35
        elif self.vuotqua_kiemtra_tongphu_2 == 1:
            self.diem_3_1_chamdiem = 15
        elif self.vuotqua_kiemtra_tongphu_2 == 2:
            self.diem_3_1_chamdiem = 25
        elif self.vuotqua_kiemtra_tongphu_2 >= 3:
            self.diem_3_1_chamdiem = 35

    dat_3_1_chamdiem = fields.Integer(default=35)
    khong_3_1_chamdiem = fields.Integer(default=35)
    diem_4_1_chamdiem = fields.Integer(compute='_diem_4_1_chamdiem')  # xn_0074

    @api.multi
    @api.depends('vuotqua_kiemtra_hientai_3')
    def _diem_4_1_chamdiem(self):
        if self.vuotqua_kiemtra_hientai_3 == 0:
            self.diem_4_1_chamdiem = 0
        elif self.vuotqua_kiemtra_hientai_3 == 1:
            self.diem_4_1_chamdiem = 3
        elif self.vuotqua_kiemtra_hientai_3 >= 2:
            self.diem_4_1_chamdiem = 5

    dat_4_1_chamdiem = fields.Integer(default=5)
    khong_4_1_chamdiem = fields.Integer(default=0)
    diem_5_1_chamdiem = fields.Integer(compute='_diem_5_1_chamdiem')  # xn_0075

    @api.multi
    @api.depends('tong_thanhcong')
    def _diem_5_1_chamdiem(self):
        if self.tong_thanhcong == 0:
            self.diem_5_1_chamdiem = 0
        elif self.tong_thanhcong == 1:
            self.diem_5_1_chamdiem = 3
        elif self.tong_thanhcong >= 2:
            self.diem_5_1_chamdiem = 5

    dat_5_1_chamdiem = fields.Integer(default=5)
    khong_5_1_chamdiem = fields.Integer(default=0)
    diem_6_1_chamdiem = fields.Integer(compute='_diem_6_1_chamdiem')  # xn_0076

    @api.multi
    @api.depends('ketqua_kiemtra_1')
    def _diem_6_1_chamdiem(self):
        if self.ketqua_kiemtra_1 == u'Có':
            self.diem_6_1_chamdiem = 5
        elif self.ketqua_kiemtra_1 == u'Không':
            self.diem_6_1_chamdiem = 0

    dat_6_1_chamdiem = fields.Integer(default=5)
    khong_6_1_chamdiem = fields.Integer(default=0)

    # Bảng 2
    kythuat_thuctapsinh_2 = fields.Integer()  # xn_0036
    kythuat_nguoithamgia_2 = fields.Integer()  # xn_0037
    huongdan_thuctapsinh_2 = fields.Integer()  # xn_0039
    huongdan_nguoithamgia_2 = fields.Integer()  # xn_0040

    diem_1_2_chamdiem = fields.Integer(compute='_diem_1_2_chamdiem')  # xn_0077

    @api.multi
    @api.depends('kythuat_thuctapsinh_2', 'kythuat_nguoithamgia_2')
    def _diem_1_2_chamdiem(self):
        if self.kythuat_thuctapsinh_2 or self.kythuat_nguoithamgia_2:
            if self.kythuat_thuctapsinh_2 == self.kythuat_nguoithamgia_2:
                self.diem_1_2_chamdiem = 5
            else:
                self.diem_1_2_chamdiem = 0

    dat_1_2_chamdiem = fields.Integer(default=5)
    khong_1_2_chamdiem = fields.Integer(default=0)
    diem_2_2_chamdiem = fields.Integer(compute='_diem_2_2_chamdiem')  # xn_0078

    @api.multi
    @api.depends('huongdan_thuctapsinh_2', 'huongdan_nguoithamgia_2')
    def _diem_2_2_chamdiem(self):
        if self.huongdan_thuctapsinh_2 or self.huongdan_nguoithamgia_2:
            if self.huongdan_thuctapsinh_2 == self.huongdan_nguoithamgia_2:
                self.diem_2_2_chamdiem = 5
            else:
                self.diem_2_2_chamdiem = 0

    dat_2_2_chamdiem = fields.Integer(default=5)
    khong_2_2_chamdiem = fields.Integer(default=0)

    # Bảng 3
    phongdaotao_3 = fields.Many2one(comodel_name='xinghiep.chinhanh')
    thuctapkythuat_3 = fields.Many2one(comodel_name='thuctapsinh.thuctapsinh')  # xn_0044

    luong_thap_3 = fields.Integer()  # xn_0041
    luong_toithieu_3 = fields.Integer()  # xn_0042
    tyle_luong_3 = fields.Float(compute='_tyle_luong_3')  # xn_0043

    @api.multi
    @api.depends('luong_thap_3', 'luong_toithieu_3')
    def _tyle_luong_3(self):
        if self.luong_thap_3 and self.luong_toithieu_3:
            self.tyle_luong_3 = round((self.luong_thap_3 / self.luong_toithieu_3) * 100,1)

    loai_luongtoithieu_3 = fields.Selection(
        selection=[('Mức lương tối thiểu theo vùng', 'Mức lương tối thiểu theo vùng'),
                   ('Mức lương tối thiểu quy định', 'Mức lương tối thiểu quy định')],
        default='Mức lương tối thiểu quy định')  # xn_0045
    hoso_chuyengiao_3 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    chuyen_1_2_daotao_3 = fields.Float()  # xn_0046
    chuyen_2_3_daotao_3 = fields.Float()  # xn_0047

    diem_1_3_chamdiem = fields.Integer(compute='_diem_1_3_chamdiem')  # xn_0079

    @api.multi
    @api.depends('tyle_luong_3')
    def _diem_1_3_chamdiem(self):
        if self.tyle_luong_3 >= 115:
            self.diem_1_3_chamdiem = 5
        elif self.tyle_luong_3 >= 105:
            self.diem_1_3_chamdiem = 3
        else:
            self.diem_1_3_chamdiem = 0

    dat_1_3_chamdiem = fields.Integer(default=5)
    khong_1_3_chamdiem = fields.Integer(default=0)
    diem_2_3_chamdiem = fields.Integer(compute='_diem_2_3_chamdiem')  # xn_0080

    @api.multi
    @api.depends('chuyen_1_2_daotao_3', 'chuyen_2_3_daotao_3')
    def _diem_2_3_chamdiem(self):
        if self.hoso_chuyengiao_3 == "Không":
            if self.chuyen_1_2_daotao_3 >= 5:
                self.diem_2_3_chamdiem = 5
            elif 3 <= self.chuyen_1_2_daotao_3 < 5:
                self.diem_2_3_chamdiem = 3
            else:
                self.diem_2_3_chamdiem = 0
        else:
            demo = min(self.chuyen_1_2_daotao_3, self.chuyen_2_3_daotao_3)
            if demo >= 5:
                self.diem_2_3_chamdiem = 5
            elif 3 <= demo < 5:
                self.diem_2_3_chamdiem = 3
            else:
                self.diem_2_3_chamdiem = 0

    dat_2_3_chamdiem = fields.Integer(default=5)
    khong_2_3_chamdiem = fields.Integer(default=0)

    # Bảng 4
    huongdan_caitien_4 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')  # xn_0048
    noidung_huongdan_caitien_4 = fields.Date()  # xn_0049

    thuchien_huongdan_caitien_4 = fields.Selection(selection=[('Thực hiện cải tiến', 'Thực hiện cải tiến'),
                                                              ('Không thực hiện cải tiến',
                                                               'Không thực hiện cải tiến')],
                                                   default='Không thực hiện cải tiến')  # xn_0050

    huongdan_quantri_4 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')  # xn_0051
    noidung_huongdan_quantri_4 = fields.Date()  # xn_0052

    thuchien_huongdan_quantri_4 = fields.Selection(selection=[('Thực hiện cải tiến', 'Thực hiện cải tiến'),
                                                              ('Không thực hiện cải tiến',
                                                               'Không thực hiện cải tiến')],
                                                   default='Không thực hiện cải tiến')  # xn_0053

    nguoi_mattich_4 = fields.Integer()  # xn_0054
    nguoi_duocnhan_4 = fields.Integer()  # xn_0055
    tyle_nguoi_4 = fields.Float(compute='_tyle_nguoi_4')  # xn_0056

    @api.multi
    @api.depends('nguoi_mattich_4', 'nguoi_duocnhan_4')
    def _tyle_nguoi_4(self):
        if self.nguoi_mattich_4 and self.nguoi_duocnhan_4:
            self.tyle_nguoi_4 = round((self.nguoi_mattich_4 / self.nguoi_duocnhan_4) * 100,1)

    doloi_4 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')  # xn_0057

    diem_1_4_chamdiem = fields.Integer(compute='_diem_1_4_chamdiem')  # xn_0081

    @api.multi
    @api.depends('huongdan_caitien_4', 'thuchien_huongdan_caitien_4','huongdan_quantri_4','thuchien_huongdan_quantri_4')
    def _diem_1_4_chamdiem(self):
        if self.huongdan_caitien_4 == u'Có':
            if self.thuchien_huongdan_caitien_4 == u'Thực hiện cải tiến':
                self.diem_1_4_chamdiem = -30
            else:
                self.diem_1_4_chamdiem = -50
        else:
            if self.huongdan_quantri_4 == u'Có':
                if self.thuchien_huongdan_quantri_4 == u'Thực hiện cải tiến':
                    self.diem_1_4_chamdiem = -30
                else:
                    self.diem_1_4_chamdiem = -50
            else:
                self.diem_1_4_chamdiem = 0


    dat_1_4_chamdiem = fields.Integer(default=0)
    khong_1_4_chamdiem = fields.Integer(default=-50)
    diem_2_4_chamdiem = fields.Integer(compute='_diem_2_4_chamdiem')  # xn_0082

    @api.multi
    @api.depends('tyle_nguoi_4','nguoi_mattich_4')
    def _diem_2_4_chamdiem(self):
        if self.tyle_nguoi_4 >= 20 or self.nguoi_mattich_4 >= 3:
            self.diem_2_4_chamdiem = -10
        elif (10 <= self.tyle_nguoi_4 < 20) or self.nguoi_mattich_4 == 2:
            self.diem_2_4_chamdiem = -5
        elif (0 < self.tyle_nguoi_4 < 10) or self.nguoi_mattich_4 == 1:
            self.diem_2_4_chamdiem = 0
        elif self.tyle_nguoi_4 == 0:
            self.diem_2_4_chamdiem = 5

    dat_2_4_chamdiem = fields.Integer(default=5)
    khong_2_4_chamdiem = fields.Integer(default=-10)
    diem_3_4_chamdiem = fields.Integer(compute='_diem_3_4_chamdiem')  # xn_0083

    @api.multi
    @api.depends('doloi_4')
    def _diem_3_4_chamdiem(self):
        if self.doloi_4 == u'Có':
            self.diem_3_4_chamdiem = -50
        else:
            self.diem_3_4_chamdiem = 0

    dat_3_4_chamdiem = fields.Integer(default = 0)
    khong_3_4_chamdiem = fields.Integer(default = -50)

    #Bảng 5
    nhansu_lienquan_5 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')  # xn_0058
    dambao_5 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')  # xn_0059
    thaydoi_daotao_5 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')  # xn_0060
    nhanvien_kythuat_5 = fields.Char()  # xn_0061
    ngaynhan_5 = fields.Date()  # xn_0065
    souyquyen_5 = fields.Char()  # xn_0066
    ngaysinh_5 = fields.Date()  # xn_0064
    quoctich_5 = fields.Char()  # xn_0062
    gioitinh_5 = fields.Selection(selection=[('Nam', 'Nam'), ('Nữ', 'Nữ')], default='Nam')  # xn_0063
    diem_1_5_chamdiem = fields.Integer(compute='_diem_1_5_chamdiem')  # xn_0084

    @api.multi
    @api.depends('nhansu_lienquan_5')
    def _diem_1_5_chamdiem(self):
        if self.nhansu_lienquan_5 == u'Có':
            self.diem_1_5_chamdiem = 5
        else:
            self.diem_1_5_chamdiem = 0

    dat_1_5_chamdiem = fields.Integer(default=5)
    khong_1_5_chamdiem = fields.Integer(default=0)
    diem_2_5_chamdiem = fields.Integer(compute='_diem_2_5_chamdiem')  # xn_0085

    @api.multi
    @api.depends('dambao_5')
    def _diem_2_5_chamdiem(self):
        if self.dambao_5 == u'Có':
            self.diem_2_5_chamdiem = 5
        else:
            self.diem_2_5_chamdiem = 0

    dat_2_5_chamdiem = fields.Integer(default=5)
    khong_2_5_chamdiem = fields.Integer(default=0)
    diem_3_5_chamdiem = fields.Integer(compute='_diem_3_5_chamdiem')  # xn_0086

    @api.multi
    @api.depends('thaydoi_daotao_5')
    def _diem_3_5_chamdiem(self):
        if self.thaydoi_daotao_5 == u'Có':
            self.diem_3_5_chamdiem = 5
        else:
            self.diem_3_5_chamdiem = 0

    dat_3_5_chamdiem = fields.Integer(default=5)
    khong_3_5_chamdiem = fields.Integer(default=0)

    # Bảng 6
    decuong_6 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    noidung_decuong_6 = fields.Text()  # xn_0067
    tuongtac_6 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    noidung_tuongtac_6 = fields.Text()  # xn_0068
    vanhoa_6 = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    noidung_vanhoa_6 = fields.Text()  # xn_0069

    diem_1_6_chamdiem = fields.Integer(compute='_diem_1_6_chamdiem')  # xn_0087

    @api.multi
    @api.depends('decuong_6')
    def _diem_1_6_chamdiem(self):
        if self.decuong_6 == u'Có':
            self.diem_1_6_chamdiem = 4
        else:
            self.diem_1_6_chamdiem = 0

    dat_1_6_chamdiem = fields.Integer(default=4)
    khong_1_6_chamdiem = fields.Integer(default=0)
    diem_2_6_chamdiem = fields.Integer(compute='_diem_2_6_chamdiem')  # xn_0088

    @api.multi
    @api.depends('tuongtac_6')
    def _diem_2_6_chamdiem(self):
        if self.tuongtac_6 == u'Có':
            self.diem_2_6_chamdiem = 3
        else:
            self.diem_2_6_chamdiem = 0

    dat_2_6_chamdiem = fields.Integer(default=3)
    khong_2_6_chamdiem = fields.Integer(default=0)
    diem_3_6_chamdiem = fields.Integer(compute='_diem_3_6_chamdiem')  # xn_0089

    @api.multi
    @api.depends('vanhoa_6')
    def _diem_3_6_chamdiem(self):
        if self.vanhoa_6 == u'Có':
            self.diem_3_6_chamdiem = 3
        else:
            self.diem_3_6_chamdiem = 0

    dat_3_6_chamdiem = fields.Integer(default=3)
    khong_3_6_chamdiem = fields.Integer(default=0)
    diem_4_6_chamdiem = fields.Integer(compute='_diem_4_6_chamdiem')  # xn_0070

    @api.multi
    @api.depends('diem_1_1_chamdiem', 'diem_2_1_chamdiem', 'diem_3_1_chamdiem', 'diem_4_1_chamdiem',
                 'diem_5_1_chamdiem', 'diem_6_1_chamdiem', 'diem_1_2_chamdiem', 'diem_2_2_chamdiem',
                 'diem_1_3_chamdiem', 'diem_2_3_chamdiem', 'diem_1_4_chamdiem', 'diem_2_4_chamdiem',
                 'diem_3_4_chamdiem', 'diem_1_5_chamdiem', 'diem_2_5_chamdiem', 'diem_3_5_chamdiem',
                 'diem_1_6_chamdiem', 'diem_2_6_chamdiem', 'diem_3_6_chamdiem')
    def _diem_4_6_chamdiem(self):
        self.diem_4_6_chamdiem = self.diem_1_1_chamdiem + self.diem_2_1_chamdiem + self.diem_3_1_chamdiem + self.diem_4_1_chamdiem \
                                 + self.diem_5_1_chamdiem + self.diem_6_1_chamdiem + self.diem_1_2_chamdiem + self.diem_2_2_chamdiem \
                                 + self.diem_1_3_chamdiem + self.diem_2_3_chamdiem + self.diem_1_4_chamdiem + self.diem_2_4_chamdiem \
                                 + self.diem_3_4_chamdiem + self.diem_1_5_chamdiem + self.diem_2_5_chamdiem + self.diem_3_5_chamdiem \
                                 + self.diem_1_6_chamdiem + self.diem_2_6_chamdiem + self.diem_3_6_chamdiem

    dat_4_6_chamdiem = fields.Integer(default=120)
    khong_4_6_chamdiem = fields.Integer(default=-170)

    @api.multi
    def baocao_chamdiem_button(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/chamdiem/download/%s' % (self.id),
            'target': 'self', }

    @api.multi
    def baocao_chamdiem_32(self, chamdiem):
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_32")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            context['hhjp_0193'] = convert_date(chamdiem.ngaylap_vanban_chamdiem)
            context['hhjp_0010'] = kiemtra(chamdiem.xinghiep_chamdiem.ten_han_xnghiep)
            context['xn_0001'] = kiemtra(chamdiem.soluong_kiemtra_tongphu_1)
            context['xn_0002'] = kiemtra(chamdiem.hoanthanh_hientai_1)
            context['xn_0003'] = kiemtra(chamdiem.kiemtra_hientai_1)
            context['xn_0004'] = kiemtra(chamdiem.soluong_kiemtra_hientai_1)
            context['xn_0071'] = kiemtra(chamdiem.diem_1_1_chamdiem)
            context['xn_0005'] = kiemtra(chamdiem.hoanthanh_cu_1)
            context['xn_0006'] = kiemtra(chamdiem.kiemtra_cu_1)
            context['xn_0007'] = kiemtra(chamdiem.soluong_kiemtra_cu_1)
            context['xn_0008'] = kiemtra(chamdiem.ungvien_thanhcong_tongphu_1)
            context['xn_0009'] = kiemtra(chamdiem.ungvien_thanhcong_hientai_1)
            context['xn_0010'] = kiemtra(chamdiem.ungvien_thanhcong_cu_1)
            context['xn_0011'] = kiemtra(chamdiem.tile_kithi_coban)
            context['xn_0012'] = kiemtra(chamdiem.soluong_tong)
            context['xn_0013'] = kiemtra(chamdiem.soluong_kiemtra_tongphu_2)
            context['xn_0014'] = kiemtra(chamdiem.hoanthanh_hientai_2)
            context['xn_0015'] = kiemtra(chamdiem.kiemtra_hientai_2)
            context['xn_0016'] = kiemtra(chamdiem.soluong_kiemtra_hientai_2)
            context['xn_0017'] = kiemtra(chamdiem.soluong_kiemtra_cu_2)
            context['xn_0018'] = kiemtra(chamdiem.hoanthanh_hientai_3)

            context['xn_0019'] = kiemtra(chamdiem.kiemtra_hientai_3)
            context['xn_0020'] = kiemtra(chamdiem.soluong_kiemtra_hientai_3)
            context[
                'xn_0021'] = chamdiem.vuotqua_kiemtra_tongphu_2 + chamdiem.vuotqua_kiemtra_hientai_3 if chamdiem.vuotqua_kiemtra_tongphu_2 else ''
            context['xn_0022'] = kiemtra(chamdiem.vuotqua_kiemtra_tongphu_2)
            context['xn_0023'] = kiemtra(chamdiem.vuotqua_kiemtra_hientai_2)
            context['xn_0024'] = kiemtra(chamdiem.vuotqua_kiemtra_cu_2)
            context['xn_0025'] = kiemtra(chamdiem.vuotqua_kiemtra_hientai_3)
            context['xn_0072'] = kiemtra(chamdiem.diem_2_1_chamdiem)
            context['xn_0073'] = chamdiem.diem_3_1_chamdiem
            context['xn_0026'] = kiemtra(chamdiem.kithi_lop_coban_I2)
            context['xn_0027'] = kiemtra(chamdiem.tile_lop_kithi_coban)
            context['xn_0074'] = chamdiem.diem_4_1_chamdiem
            context['xn_0075'] = chamdiem.diem_5_1_chamdiem
            context['xn_0076'] = chamdiem.diem_6_1_chamdiem
            context['xn_0077'] = chamdiem.diem_1_2_chamdiem
            context['xn_0078'] = chamdiem.diem_2_2_chamdiem
            context['xn_0079'] = chamdiem.diem_1_3_chamdiem
            context['xn_0080'] = chamdiem.diem_2_3_chamdiem
            context['xn_0081'] = chamdiem.diem_1_4_chamdiem
            context['xn_0082'] = chamdiem.diem_2_4_chamdiem
            context['xn_0083'] = chamdiem.diem_3_4_chamdiem
            context['xn_0084'] = chamdiem.diem_1_5_chamdiem
            context['xn_0085'] = chamdiem.diem_2_5_chamdiem
            context['xn_0086'] = chamdiem.diem_3_5_chamdiem
            context['xn_0087'] = chamdiem.diem_1_6_chamdiem
            context['xn_0088'] = chamdiem.diem_2_6_chamdiem
            context['xn_0089'] = chamdiem.diem_3_6_chamdiem

            if chamdiem.ketqua_kiemtra_1 == 'Có':
                context['xn_0034_1'] = u'■'
                context['xn_0034_2'] = u'□'
            elif chamdiem.ketqua_kiemtra_1 == 'Không':
                context['xn_0034_1'] = u'□'
                context['xn_0034_2'] = u'■'
            else:
                context['xn_0034_1'] = u'□'
                context['xn_0034_2'] = u'□'

            context['xn_0030'] = kiemtra(chamdiem.tong_thanhcong)
            context['xn_0031'] = kiemtra(chamdiem.noidung_kiemtra_a_1)
            context['xn_0032'] = kiemtra(chamdiem.noidung_kiemtra_b_1)
            context['xn_0033'] = kiemtra(chamdiem.noidung_kiemtra_c_1)

            if chamdiem.kythuat_thuctapsinh_2 != False:
                context['xn_0035_1'] = u'■'
                context['xn_0035_2'] = u'□'
            elif chamdiem.kythuat_thuctapsinh_2 == False:
                context['xn_0035_1'] = u'□'
                context['xn_0035_2'] = u'■'
            else:
                context['xn_0035_1'] = u'□'
                context['xn_0035_2'] = u'□'

            context['xn_0036'] = kiemtra(chamdiem.kythuat_thuctapsinh_2)
            context['xn_0037'] = kiemtra(chamdiem.kythuat_nguoithamgia_2)

            if chamdiem.huongdan_thuctapsinh_2 != False:
                context['xn_0038_1'] = u'■'
                context['xn_0038_2'] = u'□'
            elif chamdiem.huongdan_thuctapsinh_2 == False:
                context['xn_0038_1'] = u'□'
                context['xn_0038_2'] = u'■'
            else:
                context['xn_0038_1'] = u'□'
                context['xn_0038_2'] = u'□'

            context['xn_0039'] = kiemtra(chamdiem.huongdan_thuctapsinh_2)
            context['xn_0040'] = kiemtra(chamdiem.huongdan_nguoithamgia_2)
            context['xn_0041'] = kiemtra(chamdiem.luong_thap_3)
            context['xn_0042'] = kiemtra(chamdiem.luong_toithieu_3)
            context['xn_0043'] = kiemtra(chamdiem.tyle_luong_3)
            context['xn_0044'] = kiemtra(chamdiem.thuctapkythuat_3.ten_lt_tts)

            if chamdiem.loai_luongtoithieu_3 == 'Mức lương tối thiểu theo vùng':
                context['xn_0045_1'] = u'■'
                context['xn_0045_2'] = u'□'
            elif chamdiem.loai_luongtoithieu_3 == 'Mức lương tối thiểu quy định':
                context['xn_0045_1'] = u'□'
                context['xn_0045_2'] = u'■'
            else:
                context['xn_0045_1'] = u'□'
                context['xn_0045_2'] = u'□'

            context['xn_0046'] = kiemtra(chamdiem.chuyen_1_2_daotao_3)
            context['xn_0047'] = kiemtra(chamdiem.chuyen_2_3_daotao_3)

            if chamdiem.huongdan_caitien_4 == 'Có':
                context['xn_0048_1'] = u'□'
                context['xn_0048_2'] = u'■'
                if chamdiem.thuchien_huongdan_caitien_4 == 'Thực hiện cải tiến':
                    context['xn_0050_1'] = u'■'
                    context['xn_0050_2'] = u'□'
                elif chamdiem.thuchien_huongdan_caitien_4 == 'Không thực hiện cải tiến':
                    context['xn_0050_1'] = u'□'
                    context['xn_0050_2'] = u'■'
                else:
                    context['xn_0050_1'] = u'□'
                    context['xn_0050_2'] = u'□'
            elif chamdiem.huongdan_caitien_4 == 'Không':
                context['xn_0048_1'] = u'■'
                context['xn_0048_2'] = u'□'
            else:
                context['xn_0048_1'] = u'□'
                context['xn_0048_2'] = u'□'

            if chamdiem.noidung_huongdan_caitien_4:
                context['xn_0049_D'] = chamdiem.noidung_huongdan_caitien_4.day
                context['xn_0049_M'] = chamdiem.noidung_huongdan_caitien_4.month
                context['xn_0049_Y'] = chamdiem.noidung_huongdan_caitien_4.year
            else:
                context['xn_0049_D'] = ''
                context['xn_0049_M'] = ''
                context['xn_0049_Y'] = ''

            if chamdiem.huongdan_quantri_4 == 'Có':
                context['xn_0051_1'] = u'□'
                context['xn_0051_2'] = u'■'
                if chamdiem.thuchien_huongdan_quantri_4 == 'Thực hiện cải tiến':
                    context['xn_0053_1'] = u'■'
                    context['xn_0053_2'] = u'□'
                elif chamdiem.thuchien_huongdan_quantri_4 == 'Không thực hiện cải tiến':
                    context['xn_0053_1'] = u'□'
                    context['xn_0053_2'] = u'■'
                else:
                    context['xn_0053_1'] = u'□'
                    context['xn_0053_2'] = u'□'
            elif chamdiem.huongdan_quantri_4 == 'Không':
                context['xn_0051_1'] = u'■'
                context['xn_0051_2'] = u'□'
            else:
                context['xn_0051_1'] = u'□'
                context['xn_0051_2'] = u'□'

            if chamdiem.noidung_huongdan_caitien_4:
                context[
                    'xn_0052_D'] = chamdiem.noidung_huongdan_quantri_4.day
                context[
                    'xn_0052_M'] = chamdiem.noidung_huongdan_quantri_4.month
                context[
                    'xn_0052_Y'] = chamdiem.noidung_huongdan_quantri_4.year
            else:
                context[
                    'xn_0052_D'] = ''
                context[
                    'xn_0052_M'] = ''
                context[
                    'xn_0052_Y'] = ''

            context['xn_0054'] = kiemtra(chamdiem.nguoi_mattich_4)
            context['xn_0055'] = kiemtra(chamdiem.nguoi_duocnhan_4)
            context['xn_0056'] = kiemtra(chamdiem.tyle_nguoi_4)

            if chamdiem.doloi_4 == 'Có':
                context['xn_0057_1'] = u'□'
                context['xn_0057_2'] = u'■'
            elif chamdiem.doloi_4 == 'Không':
                context['xn_0057_1'] = u'■'
                context['xn_0057_2'] = u'□'
            else:
                context['xn_0057_1'] = u'□'
                context['xn_0057_2'] = u'□'

            if chamdiem.nhansu_lienquan_5 == 'Có':
                context['xn_0058_1'] = u'■'
                context['xn_0058_2'] = u'□'
            elif chamdiem.nhansu_lienquan_5 == 'Không':
                context['xn_0058_1'] = u'□'
                context['xn_0058_2'] = u'■'
            else:
                context['xn_0058_1'] = u'□'
                context['xn_0058_2'] = u'□'

            if chamdiem.dambao_5 == 'Có':
                context['xn_0059_1'] = u'■'
                context['xn_0059_2'] = u'□'
            elif chamdiem.dambao_5 == 'Không':
                context['xn_0059_1'] = u'□'
                context['xn_0059_2'] = u'■'
            else:
                context['xn_0059_1'] = u'□'
                context['xn_0059_2'] = u'□'

            if chamdiem.thaydoi_daotao_5 == 'Có':
                context['xn_0060_1'] = u'□'
                context['xn_0060_2'] = u'■'
                if chamdiem.gioitinh_5 == 'Nam':
                    context['xn_0063_1'] = u'■'
                    context['xn_0063_2'] = u'□'
                elif chamdiem.gioitinh_5 == 'Nữ':
                    context['xn_0063_1'] = u'□'
                    context['xn_0063_2'] = u'■'
                else:
                    context['xn_0063_1'] = u'□'
                    context['xn_0063_2'] = u'□'
            elif chamdiem.thaydoi_daotao_5 == 'Không':
                context['xn_0060_1'] = u'■'
                context['xn_0060_2'] = u'□'
                context['xn_0063_1'] = u'□'
                context['xn_0063_2'] = u'□'
            else:
                context['xn_0060_1'] = u'□'
                context['xn_0060_2'] = u'□'
                context['xn_0063_1'] = u'□'
                context['xn_0063_2'] = u'□'

            context['xn_0061'] = kiemtra(chamdiem.nhanvien_kythuat_5)

            context['xn_0062'] = kiemtra(chamdiem.quoctich_5)

            context['xn_0063'] = kiemtra(chamdiem.gioitinh_5)

            if chamdiem.ngaysinh_5:
                context['xn_0064_D'] = chamdiem.ngaysinh_5.day
                context['xn_0064_M'] = chamdiem.ngaysinh_5.month
                context['xn_0064_Y'] = chamdiem.ngaysinh_5.year
            else:
                context['xn_0064_D'] = ''
                context['xn_0064_M'] = ''
                context['xn_0064_Y'] = ''

            if chamdiem.ngaysinh_5:
                context['xn_0065_D'] = chamdiem.ngaynhan_5.day
                context['xn_0065_M'] = chamdiem.ngaynhan_5.month
                context['xn_0065_Y'] = chamdiem.ngaynhan_5.year
            else:
                context['xn_0065_D'] = ''
                context['xn_0065_M'] = ''
                context['xn_0065_Y'] = ''

            context['xn_0066'] = kiemtra(chamdiem.souyquyen_5)
            context['xn_0067'] = kiemtra(chamdiem.noidung_decuong_6)
            context['xn_0068'] = kiemtra(chamdiem.noidung_tuongtac_6)
            context['xn_0069'] = kiemtra(chamdiem.noidung_vanhoa_6)
            context['xn_0070'] = kiemtra(chamdiem.diem_4_6_chamdiem)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None