# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception, content_disposition
from docx.enum.shape import WD_INLINE_SHAPE

from io import BytesIO, StringIO
from docxtpl import DocxTemplate, InlineImage
from docx.shared import Mm, Inches, Pt
from tempfile import NamedTemporaryFile
import os
import zipfile
import logging

_logger = logging.getLogger(__name__)
from odoo.addons.donhang.models import models


class Baocao(http.Controller):
    @http.route('/junkai/download/<string:id>', type='http', auth="public")
    def junkai(self, id=False, **kwargs):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        file_maps = {}
        junkai = request.env['baocao.junkai'].browse(int(id))
        invoice = request.env['baocao.junkai']
        doc_junkai = invoice.baocao_junkai(junkai)

        file_maps.update({u'技能実習日誌 (参考様式第4-2号).docx': doc_junkai.name})

        for key in file_maps:
            archive.write(file_maps[key], key)
            os.unlink(file_maps[key])

        archive.close()
        reponds.flush()
        ret_zip = reponds.getvalue()
        reponds.close()

        doc_junkai.name = '技能実習日誌 (参考様式第4-2号).zip'
        return request.make_response(ret_zip,
                                     [('Content-Type', 'application/zip'),
                                      ('Content-Disposition', content_disposition(doc_junkai.name))])

    @http.route('/kansa/download/<string:id>', type='http', auth="public")
    def kansa(self, id=False, **kwargs):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        file_maps = {}
        kansa = request.env['baocao.kansa'].browse(int(id))
        invoice = request.env['baocao.kansa']


        doc_kansa = invoice.baocao_kansa_39(kansa)
        file_maps.update({u'監査報告書 (省令様式第22号).docx': doc_kansa.name})

        doc_kansa_40 = invoice.baocao_kansa_40(kansa)
        file_maps.update({u'監査実施概要（参考様式第4-7号）.docx': doc_kansa_40.name})

        for key in file_maps:
            archive.write(file_maps[key], key)
            os.unlink(file_maps[key])

        archive.close()
        reponds.flush()
        ret_zip = reponds.getvalue()
        reponds.close()

        doc_kansa.name = '監査報告書.zip'
        return request.make_response(ret_zip,
                                     [('Content-Type', 'application/zip'),
                                      ('Content-Disposition', content_disposition(doc_kansa.name))])

    @http.route('/kinhdoanh/download/<string:id>', type='http', auth="public")
    def kinhdoanh(self, id=False, **kwargs):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        file_maps = {}
        kinhdoanh = request.env['baocao.kinhdoanh'].browse(int(id))
        invoice = request.env['baocao.kinhdoanh']
        doc_kinhdoanh = invoice.baocao_kinhdoanh_36(kinhdoanh)

        file_maps.update({u'訪問指導記録書 (参考様式第4-10号).docx': doc_kinhdoanh.name})

        for key in file_maps:
            archive.write(file_maps[key], key)
            os.unlink(file_maps[key])

        archive.close()
        reponds.flush()
        ret_zip = reponds.getvalue()
        reponds.close()

        doc_kinhdoanh.name = '訪問指導記録書 (参考様式第4-10号).zip'
        return request.make_response(ret_zip,
                                     [('Content-Type', 'application/zip'),
                                      ('Content-Disposition', content_disposition(doc_kinhdoanh.name))])

    @http.route('/chidao/download/<string:id>', type='http', auth="public")
    def chidao(self, id=False, **kwargs):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        file_maps = {}
        chidao = request.env['baocao.chidao'].browse(int(id))
        invoice = request.env['baocao.chidao']

        doc_chidao = invoice.baocao_chidao_41(chidao)
        file_maps.update({u'訪問指導記録書.docx': doc_chidao.name})

        for key in file_maps:
            archive.write(file_maps[key], key)
            os.unlink(file_maps[key])

        archive.close()
        reponds.flush()
        ret_zip = reponds.getvalue()
        reponds.close()

        doc_chidao.name = '訪問指導記録書.zip'
        return request.make_response(ret_zip,
                                     [('Content-Type', 'application/zip'),
                                      ('Content-Disposition', content_disposition(doc_chidao.name))])

    @http.route('/tinhtrang/download/<string:id>', type='http', auth="public")
    def tinhtrang(self, id=False, **kwargs):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        file_maps = {}
        tinhtrang = request.env['baocao.tinhtrang'].browse(int(id))
        invoice = request.env['baocao.tinhtrang']
        doc_tinhtrang = invoice.baocao_tinhtrang_37(tinhtrang)

        file_maps.update({u'実施状況報告書.docx': doc_tinhtrang.name})

        for key in file_maps:
            archive.write(file_maps[key], key)
            os.unlink(file_maps[key])

        archive.close()
        reponds.flush()
        ret_zip = reponds.getvalue()
        reponds.close()

        doc_tinhtrang.name = '実施状況報告書.zip'
        return request.make_response(ret_zip,
                                     [('Content-Type', 'application/zip'),
                                      ('Content-Disposition', content_disposition(doc_tinhtrang.name))])

    @http.route('/chamdiem/download/<string:id>', type='http', auth="public")
    def chamdiem(self, id=False, **kwargs):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        file_maps = {}
        tinhtrang = request.env['xinghiep.chamdiem'].browse(int(id))
        invoice = request.env['xinghiep.chamdiem']
        doc_chamdiem = invoice.baocao_chamdiem_32(tinhtrang)

        file_maps.update({u'優良要件適合申告書（実習実施者）.docx': doc_chamdiem.name})

        for key in file_maps:
            archive.write(file_maps[key], key)
            os.unlink(file_maps[key])

        archive.close()
        reponds.flush()
        ret_zip = reponds.getvalue()
        reponds.close()

        doc_chamdiem.name = '優良要件適合申告書（実習実施者）.zip'
        return request.make_response(ret_zip,
                                     [('Content-Type', 'application/zip'),
                                      ('Content-Disposition', content_disposition(doc_chamdiem.name))])

    @http.route('/chamdiemnghiepdoan/download/<string:id>', type='http', auth="public")
    def chamdiemnghiepdoan(self, id=False, **kwargs):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        file_maps = {}
        chamdiem = request.env['nghiepdoan.chamdiem'].browse(int(id))
        invoice = request.env['nghiepdoan.chamdiem']
        doc_chamdiem = invoice.baocao_chamdiemnghiepdoan(chamdiem)

        file_maps.update({u'優良要件適合申告書（監理団体）.docx': doc_chamdiem.name})

        for key in file_maps:
            archive.write(file_maps[key], key)
            os.unlink(file_maps[key])

        archive.close()
        reponds.flush()
        ret_zip = reponds.getvalue()
        reponds.close()

        doc_chamdiem.name = '優良要件適合申告書（監理団体）.zip'
        return request.make_response(ret_zip,
                                     [('Content-Type', 'application/zip'),
                                      ('Content-Disposition', content_disposition(doc_chamdiem.name))])
