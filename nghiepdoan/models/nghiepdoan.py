# -*- coding: utf-8 -*-

from odoo import models, fields, api


class CVNghiepDoan(models.Model):
    _name = 'congviec.nghiepdoan'
    _rec_name = 'ma_tonghop'
    _order = 'id desc'

    # congtyphaicu = fields.Many2one(comodel_name='congtyphaicu.congtyphaicu', string='Công ty phải cử')
    # xinghiep_tonghop = fields.Many2one(comodel_name='xinghiep.xinghiep')
    # xinghiep = fields.Many2one(comodel_name='hoso.xinghiep', string='Công ty phải cử')
    congviec_nghiepdoan = fields.Many2one(comodel_name='nghiepdoan.nghiepdoan', string='Nghiệp đoàn')

    ma_tonghop = fields.Char(string='Mã công việc')
    nganhnghe_tonghop = fields.Many2one(comodel_name='nganhnghe.nganhnghe', string='Ngành nghề')

    @api.onchange('nganhnghe_tonghop')
    def onchange_master_nganhnghe_tonghop(self):
        if self.nganhnghe_tonghop:
            return {'domain': {
                'loaicongviec_tonghop': [('nganhnghe_loaicongviec', '=', self.nganhnghe_tonghop.id)]}}

    loaicongviec_tonghop = fields.Many2one(comodel_name='loaicongviec.loaicongviec', string='Loại công việc')

    @api.onchange('loaicongviec_tonghop')
    def onchange_master_loaicongviec_tonghop(self):
        if self.loaicongviec_tonghop:
            return {'domain': {
                'congviec_tonghop': [('loaicongviec_congviec', '=', self.loaicongviec_tonghop.id)]}}

    congviec_tonghop = fields.Many2one(comodel_name='congviec.congviec', string='Công việc')

    @api.onchange('congviec_tonghop')
    def onchange_master_congviec_tonghop(self):
        if self.congviec_tonghop:
            self.ma_tonghop = self.congviec_tonghop.ma_congviec


class NDPhaiCu(models.Model):
    _name = 'nghiepdoan.phaicu'
    _rec_name = 'phaicu_nghiepdoan'

    phaicu_nghiepdoan = fields.Many2one(comodel_name='nghiepdoan.nghiepdoan', string='Nghiệp đoàn')
    phaicu_quoctich = fields.Many2one(comodel_name='quoctich.quoctich', string='Quốc tịch')
    phaicu_congtyphaicu = fields.Many2one(comodel_name='congtyphaicu.congtyphaicu', string='Công ty phái cử')

    @api.onchange('phaicu_quoctich')
    def onchange_master_phaicu_quoctich(self):
        if self.phaicu_quoctich:
            return {'domain': {
                'phaicu_congtyphaicu': [('quoctich_phaicu', '=', self.phaicu_quoctich.id)]}}


class nghiepdoan(models.Model):
    _name = 'nghiepdoan.nghiepdoan'
    _rec_name = 'ten_han_nghiepdoan'

    ten_phienam_nghiepdoan = fields.Char(string="Tiếng Furi")  # hhjp_0096
    ten_han_nghiepdoan = fields.Char(string="Tiếng Hán")  # hhjp_0097
    ten_viet_nghiepdoan = fields.Char(string="Tiếng Anh")  # hhjp_0923
    buudien_nghiepdoan = fields.Char(string='Số bưu điện')
    diachi_nghiepdoan = fields.Char(string="Tiếng Hán")  # hhjp_0098
    diachi_nghiepdoan_anh = fields.Char(string="Tiếng Anh")  # hhjp_0098

    @api.onchange('buudien_nghiepdoan')
    def _onchange_buudien_nghiepdoan(self):
        if self.buudien_nghiepdoan:
            buudien_nghiepdoan = self.env['post.office'].search([('post', '=', self.buudien_nghiepdoan)], limit=1)
            self.diachi_nghiepdoan = buudien_nghiepdoan.kanji_address
            self.diachi_nghiepdoan_anh = buudien_nghiepdoan.english_address
        else:
            self.diachi_nghiepdoan = ''
            self.diachi_nghiepdoan_anh = ''

    dienthoai_nghiepdoan = fields.Char(string="Điện thoại")  # hhjp_0099
    fax_nghiepdoan = fields.Char(string="Số fax")

    sovon_nghiepdoan = fields.Char(string="Số vốn")  # hhjp_0912
    ten_daidien_phienam_nghiepdoan = fields.Char(string="Tiếng Furi")  # hhjp_0102
    ten_daidien_han_nghiepdoan = fields.Char(string="Tiếng Hán")  # hhjp_0103
    ten_daidien_anh_nghiepdoan = fields.Char(string="Tiếng Anh")  # hhjp_0103
    chucvu_daidien_phienam_nghiepdoan = fields.Char(string="Chức vụ (Hán)")  # bc_0159
    ten_trachnhiem_phienam_nghiepdoan = fields.Char(string="Người chịu trách nhiệm (Hán)")  # hhjp_0104
    ten_trachnhiem_han_nghiepdoan = fields.Char(string="Người chịu trách nhiệm (Furi)")  # hhjp_0105
    so_nvien_chinhthuc_nghiepdoan = fields.Integer(string="Nhân viên chính thức")  # hhjp_0913
    nvien_thuongxuyen_nghiepdoan = fields.Integer(string="Nhân viên đi làm thường xuyên")
    nvien_khongthuongxuyen_nghiepdoan = fields.Integer(string="Nhân viên không đi làm thường")
    dientich_nghiepdoan = fields.Integer(string="Diện tích mặt bằng (m²)")

    loai_giayphep_nghiepdoan = fields.Selection(string="Loại giấy phép",
                                                selection=[('Tổng hợp', 'Tổng hợp'), ('Cụ thể', 'Cụ thể')])  # hhjp_0101
    so_giayphep_nghiepdoan = fields.Char(string="Số giấy phép kinh doanh")  # hhjp_0100
    ngay_capphep_nghiepdoan = fields.Date(string="Ngày cấp")  # hhjp_0914
    batdau_hieuluc_giayphep_nghiepdoan = fields.Date(string="Hiệu lực từ")  # hhjp_0915
    ketthuc_hieuluc_giayphep_nghiepdoan = fields.Date(string="~")  # hhjp_0916

    so_phapnhan_nghiepdoan = fields.Char(string="Số pháp nhân")  # hhjp_0912
    loai_nghiepdoan = fields.Selection(string='Loại nghiệp đoàn',
                                       selection=[('①商工会議所・商工会', '①商工会議所・商工会'), ('②中小企業団体', '②中小企業団体'),
                                                  ('③職業訓練法人', '③職業訓練法人'),
                                                  ('④農業協同組合', '④農業協同組合'), ('⑤漁業協同組合', '⑤漁業協同組合'),
                                                  ('⑥公益社団法人・公益財団法人', '⑥公益社団法人・公益財団法人'), ('⑦その他', '⑦その他')])  # bs_0123
    loai_khac_nghiepdoan = fields.Char(string='Loại nghiệp đoàn khác')

    chinhanh_nghiepdoan = fields.One2many(comodel_name="nghiepdoan.chinhanh", inverse_name="chinhanh_nghiepdoan",
                                          string="Danh sách chi nhánh nghiệp đoàn")  #

    @api.multi
    def laydulieu_button_1(self):
        if self.ten_han_nghiepdoan:
            chinhanh_chinh = self.env['nghiepdoan.chinhanh'].search(
                [('ten_han_chinhanh', '=', self.ten_han_nghiepdoan), ('truso_chinh', '=', True)], limit=1)
            print(chinhanh_chinh)
            if chinhanh_chinh:
                chinhanh_chinh.write(
                    {'ten_phienam_chinhanh': self.ten_phienam_nghiepdoan, 'ten_han_chinhanh': self.ten_han_nghiepdoan,
                     'dichi_chinhanh': self.diachi_nghiepdoan,
                     'dienthoai_chinhanh': self.dienthoai_nghiepdoan, 'fax_chinhanh': self.fax_nghiepdoan,
                     'trangthai_chinhanh': True,
                     'nvien_chinhthuc': self.so_nvien_chinhthuc_nghiepdoan,
                     'truso_chinh': True,
                     'buudien_chinhanh': self.buudien_nghiepdoan})

    nghiepdoan_daotao = fields.One2many(comodel_name="nghiepdoan.cosodaotao", inverse_name="nghiepdoan_daotao",
                                        string="Nghiệp đoàn đào tạo")  #

    danhsach_nhanvien_tts = fields.One2many(comodel_name="nghiepdoan.nhanvientts",
                                            inverse_name="danhsach_nhanvien_tts",
                                            string="Danh sách nhân viên liên quan đến TTS")  #
    danhsach_nhanvien_capcao = fields.One2many(comodel_name="nghiepdoan.nhanviencapcao",
                                               inverse_name="danhsach_nhanvien_capcao",
                                               string="Danh sách nhân viên cấp cao")  #
    cocau_tochuc = fields.Selection(string="Cơ cấu tổ chức", selection=[(' 単一業種団体', ' 単一業種団体'), (' 異業種団体', ' 異業種団体')],
                                    required=False, )
    ngay_thanhlap = fields.Date('Ngày thành lập')
    coquan_hanhchinh = fields.Char('Cơ quan hành chính ủy quyền')
    soluong_xinghiep = fields.Integer('Số lượng xí nghiệp tham gia')
    sobaohiem_laodong = fields.Char('Số bảo hiểm lao động')
    trang_chu = fields.Text('URL trang chủ')
    congty_phaicu = fields.One2many(comodel_name='nghiepdoan.phaicu', inverse_name='phaicu_nghiepdoan',
                                    string='Công ty phái cử')
    loaicongviec_nghiepdoan = fields.Many2many(comodel_name='congviec.congviec', string='Loại công việc',limit=100)
    nganhnghe_khongchuyentiep = fields.Boolean('Ngành nghề không chuyển tiếp')
    nd_nganhnghe_khongchuyentiep = fields.Text('Nội dung')
    mot_nganhnghe = fields.Boolean('Một ngành nghề')
    nhieu_nganhnghe = fields.Boolean('Nhiều ngành nghề')
    # Thực tập sinh tiếp nhận
    thuctapsinh_tiepnhan = fields.One2many(comodel_name='tiepnhan.tiepnhan', inverse_name='thuctapsinh_tiepnhan',
                                           string='Tiếp nhận thực tập sinh')
    # 2. Thực tập sinh đang tiếp nhận
    tts_tn_xn_soluong_1 = fields.Integer()
    tts_tn_xn_soluong_2 = fields.Integer()
    tts_tn_xn_soluong_3 = fields.Integer()

    tts_tn_xn_chedo_1 = fields.Integer()
    tts_tn_xn_chedo_2 = fields.Integer()

    tts_tn_ql_soluong_1 = fields.Integer()
    tts_tn_ql_soluong_2 = fields.Integer()
    tts_tn_ql_soluong_3 = fields.Integer()

    tts_tn_ql_chedo_1 = fields.Integer()
    tts_tn_ql_chedo_2 = fields.Integer()

    # 3. Thực tập sinh đã từng tiếp nhận 3 năm gần đây
    tts_tn_3nam_soluong_1 = fields.Integer()
    tts_tn_3nam_soluong_2 = fields.Integer()
    tts_tn_3nam_soluong_3 = fields.Integer()

    tts_tn_3nam_chedo_1 = fields.Integer()
    tts_tn_3nam_chedo_2 = fields.Integer()
    tts_tn_3nam_chedo_3 = fields.Integer()

    # 4. Thực tập sinh về nước trước hạn 3 năm gần đây
    tts_vn_3nam_1_soluong_1 = fields.Integer()
    tts_vn_3nam_1_soluong_2 = fields.Integer()
    tts_vn_3nam_1_soluong_3 = fields.Integer()

    tts_vn_3nam_1_chedo_1 = fields.Integer()
    tts_vn_3nam_1_chedo_2 = fields.Integer()

    tts_vn_3nam_2_soluong_1 = fields.Integer()
    tts_vn_3nam_2_soluong_2 = fields.Integer()
    tts_vn_3nam_2_soluong_3 = fields.Integer()

    tts_vn_3nam_2_chedo_1 = fields.Integer()
    tts_vn_3nam_2_chedo_2 = fields.Integer()

    tts_vn_3nam_3_soluong_1 = fields.Integer()
    tts_vn_3nam_3_soluong_2 = fields.Integer()
    tts_vn_3nam_3_soluong_3 = fields.Integer()

    tts_vn_3nam_3_chedo_1 = fields.Integer()
    tts_vn_3nam_3_chedo_2 = fields.Integer()

    # 5 Thực tập sinh bỏ trốn 3 năm gần đây
    tts_bt_3nam_1_soluong_1 = fields.Integer()
    tts_bt_3nam_1_soluong_2 = fields.Integer()
    tts_bt_3nam_1_soluong_3 = fields.Integer()

    tts_bt_3nam_1_chedo_1 = fields.Integer()
    tts_bt_3nam_1_chedo_2 = fields.Integer()

    tts_bt_3nam_2_soluong_1 = fields.Integer()
    tts_bt_3nam_2_soluong_2 = fields.Integer()
    tts_bt_3nam_2_soluong_3 = fields.Integer()

    tts_bt_3nam_2_chedo_1 = fields.Integer()
    tts_bt_3nam_2_chedo_2 = fields.Integer()

    tts_bt_3nam_3_soluong_1 = fields.Integer()
    tts_bt_3nam_3_soluong_2 = fields.Integer()
    tts_bt_3nam_3_soluong_3 = fields.Integer()

    tts_bt_3nam_3_chedo_1 = fields.Integer()
    tts_bt_3nam_3_chedo_2 = fields.Integer()

    ghichu_botron_3nam = fields.Text(string='Ghi chú (Ngày tháng năm bỏ trốn)')

    ghichu_tiepnhan = fields.Text(string='Ghi chú')

    # Kiểm toán bên ngoài
    ktra_coso_vatchat_nghiepdoan = fields.Selection(string="Kiểm tra cơ sở vật chất",
                                                    selection=[('1', '有（外部監査・法人'),
                                                               ('2', '有（外部監査・個人）'),
                                                               ('3', '無（指定外部役員）'), ],
                                                    default='1', required=False, )
    # là 1 công ty
    ten_cty_furi = fields.Char(string="Tên công ty (Furi)")
    ten_cty_han = fields.Char(string="Tên công ty (Hán)")
    ten_nguoidaidien_furi = fields.Char(string="Tên người đại diện (Furi)")
    ten_nguoidaidien_han = fields.Char(string="Tên người đại diện (Hán)")
    ten_ktra_cty_furi = fields.Char('Tên người kiểm tra (Furi)')
    ten_ktra_cty_han = fields.Char('Tên người kiểm tra (Hán)')
    chucvu_cty = fields.Char('Chức vụ')
    # so_buudien_cty = fields.Many2one(comodel_name='post.office', string='Số bưu điện')
    so_buudien_cty = fields.Char(string='Số bưu điện')
    diachi_cty = fields.Char('Địa chỉ')

    @api.onchange('ktra_coso_vatchat_nghiepdoan')
    def onchange_ktra_coso_vatchat_nghiepdoan(self):
        if self.ktra_coso_vatchat_nghiepdoan != '1':
            self.ten_cty_furi = ''
            self.ten_cty_han = ''
            self.so_buudien_cty = None
            self.ten_nguoidaidien_furi = ''
            self.ten_nguoidaidien_han = ''
            self.ten_ktra_cty_furi = ''
            self.ten_ktra_cty_han = ''
            self.chucvu_cty = ''
            self.diachi_cty = ''
            self.sdt_cty = ''
            self.kn_lienquan_kinang = ''
            self.thamgia_cacbuoi_ttkn = ''
            self.moi_qhe_nghiepdoan = ''
        if self.ktra_coso_vatchat_nghiepdoan != '2':
            self.ten_canhan_han = ''
            self.ten_canhan_furi = ''
            self.noi_lamviec = ''
            self.chucvu_canhan = ''
            self.quoc_tich_canhan = None
            self.bang_cap_canhan = ''
            self.so_buudien_nharieng = None
            self.diachi_canhan = ''
            self.sdt_canhan = ''
            self.so_buudien_noilviec = None
            self.diachi_noilviec = ''
            self.sdt_noilviec = ''
            self.kn_lienquan_kinang = ''
            self.thamgia_cacbuoi_ttkn = ''
            self.moi_qhe_nghiepdoan = ''
        if self.ktra_coso_vatchat_nghiepdoan != '3':
            self.ten_nguoi_cty_furi = ''
            self.ten_nguoi_cty_han = ''

    @api.onchange('so_buudien_cty')
    def _onchange_so_buudien_cty(self):
        if self.so_buudien_cty:
            so_buudien_cty = self.env['post.office'].search([('post', '=', self.so_buudien_cty)], limit=1)
            self.diachi_cty = so_buudien_cty.kanji_address
        else:
            self.diachi_cty = ''

    sdt_cty = fields.Char('Điện thoại')
    # là 1 cá nhân
    ten_canhan_furi = fields.Char('Họ và tên (Furi)')
    ten_canhan_han = fields.Char('Họ và tên (Hán)')
    noi_lamviec = fields.Char('Nơi làm việc')
    chucvu_canhan = fields.Char('Chức vụ')
    quoc_tich_canhan = fields.Many2one(comodel_name='quoctich.quoctich', string='Quốc  tịch')
    bang_cap_canhan = fields.Char('Bằng cấp')

    # so_buudien_nharieng = fields.Many2one(comodel_name='post.office', string='Số bưu điện')
    so_buudien_nharieng = fields.Char(string='Số bưu điện')
    diachi_canhan = fields.Char('Địa chỉ')

    @api.onchange('so_buudien_nharieng')
    def _onchange_so_buudien_nharieng(self):
        if self.so_buudien_nharieng:
            so_buudien_nharieng = self.env['post.office'].search([('post', '=', self.so_buudien_nharieng)], limit=1)
            self.diachi_canhan = so_buudien_nharieng.kanji_address
        else:
            self.diachi_canhan = ''

    sdt_canhan = fields.Char('Số điện thoại')

    # so_buudien_noilviec = fields.Many2one(comodel_name='post.office', string='Số bưu điện')
    so_buudien_noilviec = fields.Char(string='Số bưu điện')
    diachi_noilviec = fields.Char('Địa chỉ')

    @api.onchange('so_buudien_noilviec')
    def _onchange_so_buudien_noilviec(self):
        if self.so_buudien_noilviec:
            so_buudien_noilviec = self.env['post.office'].search([('post', '=', self.so_buudien_noilviec)], limit=1)
            self.diachi_noilviec = so_buudien_noilviec.kanji_address
        else:
            self.diachi_noilviec = ''

    sdt_noilviec = fields.Char('Số điện thoại')
    # Người trong công ty được chỉ định
    ten_nguoi_cty_furi = fields.Char('Họ và tên (Furi)')
    ten_nguoi_cty_han = fields.Char('Họ và tên (Hán)')
    kn_lienquan_kinang = fields.Char('Kinh nghiệm liên quan đến thực tập kỹ năng')
    thamgia_cacbuoi_ttkn = fields.Char('Tham gia các buổi nói về thực tập kỹ năng')
    moi_qhe_nghiepdoan = fields.Char('Mối quan hệ với nghiệp đoàn ngoài người nộp đơn')

    @api.multi
    def laydulieu_btn1(self):
        pass

    @api.multi
    def laydulieu_btn(self):
        pass

    ten_nguoinhap = fields.Char(string='Họ và tên')
    chucvu_nguoinhap = fields.Char(string='Chức vụ')
    ngaynhap = fields.Date(string='Ngày nhập', default=fields.Date.today())

class TiepNhan(models.Model):
    _name = 'tiepnhan.tiepnhan'
    _rec_name = 'thuctapsinh_tiepnhan'

    thuctapsinh_tiepnhan = fields.Many2one(comodel_name='nghiepdoan.nghiepdoan')
    quoctichtiepnhan = fields.Many2one(comodel_name="quoctich.quoctich", string="Quốc tịch", required=False, )
    soluong_tiepnhan = fields.Integer(string='Số lượng')
