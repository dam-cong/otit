# -*- coding: utf-8 -*-

from odoo import models, fields, api


class chinhanh(models.Model):
    _name = 'nghiepdoan.chinhanh'
    _rec_name = 'ten_han_chinhanh'
    _order = 'id asc'

    @api.model
    def _default_chinhanh_nghiepdoan(self):
        return self.env['nghiepdoan.nghiepdoan'].search([('id', '=', 1)],
                                                        limit=1)

    chinhanh_nghiepdoan = fields.Many2one('nghiepdoan.nghiepdoan', string="Nghiệp đoàn",
                                          default=_default_chinhanh_nghiepdoan)

    ten_phienam_chinhanh = fields.Char(string="Tên chi nhánh (Furi)")  # hhjp_0106
    ten_han_chinhanh = fields.Char(string="Tên chi nhánh (Hán)")  # hhjp_0107
    dichi_chinhanh = fields.Char(string="Địa chỉ")  # hhjp_0108
    dienthoai_chinhanh = fields.Char(string="Điện thoại")  # hhjp_0109
    fax_chinhanh = fields.Char(string="Fax")  #
    trangthai_chinhanh = fields.Boolean(string="Trạng thái hoạt động")
    nvien_chinhthuc = fields.Integer(string="Số lượng nhân viên chính thức")
    # nvien_toanthoigian = fields.Integer(string="Số lượng nhân viên toàn thời gian")
    # nvien_banthoigian = fields.Integer(string="Số lượng nhân viên bán thời gian", required=False, )
    truso_chinh = fields.Boolean(string="Trụ sở chính")

    # buudien_chinhanh = fields.Many2one(comodel_name='post.office', string='Số bưu điện')  # bs_0008
    buudien_chinhanh = fields.Char(string='Số bưu điện')  # bs_0008

    @api.onchange('buudien_chinhanh')
    def _onchange_buudien_chinhanh(self):
        if self.buudien_chinhanh:
            buudien_chinhanh = self.env['post.office'].search([('post', '=', self.buudien_chinhanh)], limit=1)
            self.dichi_chinhanh = buudien_chinhanh.kanji_address
        else:
            self.dichi_chinhanh = ''
