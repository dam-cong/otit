# -*- coding: utf-8 -*-

from odoo import models, fields, api


class nhanvientts(models.Model):
    _name = 'nghiepdoan.nhanvientts'
    _rec_name = 'ten_nvien_han_nghiepdoan'
    _order = 'id asc'

    danhsach_nhanvien_tts = fields.Many2one(comodel_name="nghiepdoan.nghiepdoan", string="Nhân viên")
    ten_nvien_furi_nghiepdoan = fields.Char(string="Họ và tên (Furi)")  #
    ten_nvien_han_nghiepdoan = fields.Char(string="Họ và tên (Hán)")  #
    gioitinh_nvien_nghiepdoan = fields.Selection(string="Giới tính",
                                                 selection=[('Nam', 'Nam'), ('Nữ', 'Nữ'), ])  #
    ngaysinh_nvien_nghiepdoan = fields.Date(string="Ngày sinh")  #
    quoctich_nvien_nghiepdoan = fields.Many2one(comodel_name='quoctich.quoctich', string="Quốc tịch")
    # buudien_nvien_nghiepdoan = fields.Many2one(comodel_name='post.office', string='Số bưu điện')
    buudien_nvien_nghiepdoan = fields.Char(string='Số bưu điện')
    diachi_nghiepdoan = fields.Char(string="Địa chỉ")  #

    @api.onchange('buudien_nvien_nghiepdoan')
    def _onchange_buudien_nvien_nghiepdoan(self):
        if self.buudien_nvien_nghiepdoan:
            buudien_nvien_nghiepdoan = self.env['post.office'].search([('post', '=', self.buudien_nvien_nghiepdoan)], limit=1)
            self.diachi_nghiepdoan = buudien_nvien_nghiepdoan.kanji_address
        else:
            self.diachi_nghiepdoan = ''

    dienthoai_nghiepdoan = fields.Char(string="Điện thoại")  #
    email_nvien_nghiepdoan = fields.Char(string="Email")  #
    ngayvao_nvien_nghiepdoan = fields.Date(string="Ngày vào công ty")  #
    chucvu_nghiepdoan = fields.Char(string="Chức vụ (Furi)")  #
    chucvu_nghiepdoan_han = fields.Char(string="Chức vụ (Hán)")  #
    chucvu_nghiepdoan_v = fields.Char(string="Chức vụ (Tiếng mẹ đẻ)")  #
    the_thuongtru_nvien_nghiepdoan = fields.Selection(string="Thẻ thường trú",
                                                      selection=[('Đã nộp', 'Đã nộp'),
                                                                 ('Chưa nộp', 'Chưa nộp'), ])
    vaitro_nghiepdoan = fields.Char(string='Vai trò')
    trachnhiem_vaitro_nghiepdoan = fields.Boolean(string='Người chịu trách nhiệm kansa')
    chidao_vaitro_nghiepdoan = fields.Boolean(string='Người chỉ đạo kế hoạch')
    quanly_vaitro_nghiepdoan = fields.Boolean(string='Người chịu trách nhiệm quản lý')
    chinhanh_nghiepdoan_nvien = fields.Many2one(comodel_name="nghiepdoan.chinhanh", string="Chi nhánh")


class nhanviencapcao(models.Model):
    _name = "nghiepdoan.nhanviencapcao"
    _inherit = 'nghiepdoan.nhanvientts'

    danhsach_nhanvien_capcao = fields.Many2one(comodel_name="nghiepdoan.nghiepdoan", string="Nhân viên")
    dilam_thuongxuyen_nhanviencapcao = fields.Selection(string="Đi làm thường xuyên",
                                                        selection=[('Có', 'Có'), ('Không', 'Không')])  #
    the_thuongtru_nhanviencapcao = fields.Selection(string="Thẻ thường trú",
                                                    selection=[('Đã nộp', 'Đã nộp'),
                                                               ('Chưa nộp', 'Chưa nộp')])  #
    canbo_chiutrachnhiem = fields.Boolean('Cán bộ chịu trách nhiệm đào tạo thực tập kỹ thuât')

# class nhanvien(models.Model):
#     _name = "nghiepdoan.nhanvien"
#     _rec_name = 'nghiepdoan_nhanvien'
#
#     @api.model
#     def _default_nghiepdoan_nhanvien(self):
#         return self.env['nghiepdoan.nghiepdoan'].search([('id', '=', 1)],
#                                                         limit=1)
#
#     nghiepdoan_nhanvien = fields.Many2one('nghiepdoan.nghiepdoan', string="Nghiệp đoàn",
#                                           default=_default_nghiepdoan_nhanvien)
#
#     so_nvien_nghiepdoan = fields.Integer('Nhân viên chính thức (tổng số)')  #
#     nvien_dilam_thuongxuyen_nghiepdoan = fields.Integer('Nhân viên đi làm thường xuyên')  #
#     nvien_khongdilam_thuongxuyen_nghiepdoan = fields.Integer('Nhân viên không đi àm thường xuyên')  #
#     danhsach_nhanvien_tts = fields.One2many(comodel_name="nghiepdoan.nhanvientts",
#                                             inverse_name="danhsach_nhanvien_tts",
#                                             string="Danh sách nhân viên liên quan đến TTS")  #
#     danhsach_nhanvien_capcao = fields.One2many(comodel_name="nghiepdoan.nhanviencapcao",
#                                                inverse_name="danhsach_nhanvien_capcao",
#                                                string="Danh sách nhân viên cấp cao")  #
