# -*- coding: utf-8 -*-

from odoo import models, fields, api

class loaihinhnha(models.Model):
    _name = 'nghiepdoan.loaihinhnhao'
    _rec_name = 'loaihinh_nhao'
    _order = 'id desc'

    loaihinh_nhao = fields.Char(string="Loại hình nhà ở")
