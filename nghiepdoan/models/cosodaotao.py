# -*- coding: utf-8 -*-

from odoo import models, fields, api


class cosodaotao(models.Model):
    _name = 'nghiepdoan.cosodaotao'
    _rec_name = 'ten_han_cosodaotao'
    _order = 'id asc'

    @api.model
    def _default_nghiepdoan_daotao(self):
        return self.env['nghiepdoan.nghiepdoan'].search([('id', '=', 1)], limit=1)

    nghiepdoan_daotao = fields.Many2one('nghiepdoan.nghiepdoan', string="Nghiệp đoàn",
                                        default=_default_nghiepdoan_daotao)
    ten_furi_cosodaotao = fields.Char(string="Tên cơ sở đào tào (Furi)")  #
    ten_han_cosodaotao = fields.Char(string="Tên cơ sở đào tạo (Hán)")  # hhjp_0174
    diachi_cosodaotao = fields.Char(string="Địa chỉ cụ thể")  # hhjp_0175
    dienthoai_cosodaotao = fields.Char(string="Điện thoại")  # hhjp_0176

    # buudien_cosodaotao = fields.Many2one(comodel_name='post.office', string='Số bưu điện')  #
    buudien_cosodaotao = fields.Char(string='Số bưu điện')  #

    @api.onchange('buudien_cosodaotao')
    def _onchange_buudien_cosodaotao(self):
        if self.buudien_cosodaotao:
            buudien_cosodaotao = self.env['post.office'].search([('post', '=', self.buudien_cosodaotao)], limit=1)
            self.diachi_cosodaotao = buudien_cosodaotao.kanji_address
        else:
            self.diachi_cosodaotao = ''
