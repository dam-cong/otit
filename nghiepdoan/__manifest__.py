# -*- coding: utf-8 -*-
{
    'name': "Nghiệp đoàn",
    'depends': ['base', 'congviec', 'congtyphaicu'],
    'data': [
        'security/group_user.xml',
        'security/ir.model.access.csv',
        'views/thongtincoban_nghiepdoan.xml',
        'views/chinhanh_nghiepdoan.xml',
        # 'views/loaihinh_nhao.xml',
        'views/nhanvien__tts_nghiepdoan.xml',
        'views/nhanvien_capcao_nghiepdoan.xml',
        'views/cosodaotao_nghiepdoan.xml',
        'data/nghiepdoan.xml',
    ],
}
