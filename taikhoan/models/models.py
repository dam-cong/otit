# -*- coding: utf-8 -*-

from odoo import models, fields, api


class taikhoan(models.Model):
    _name = 'taikhoan.taikhoan'
    _rec_name = 'name_taikhoan'

    anh_taikhoan = fields.Binary(string='Ảnh')
    name_taikhoan = fields.Char(string='Họ tên')
    users_taikhoan = fields.Many2one(comodel_name='res.users', string='Tài khoản')
    nhomquyen_taikhoan = fields.Selection(string='Nhóm quyền', selection=[('1', 'Nghiệp đoàn'), ('2', 'Xí nghiệp'),
                                                                          ('3', 'Công ty phái cử')])

    nghiepdoan_taikhoan = fields.Many2one(comodel_name='nghiepdoan.nghiepdoan', string='Nghiệp đoàn')
    xinghep_taikhoan = fields.Many2one(comodel_name='xinghiep.xinghiep', string='Xí nghiệp')
    phaicu_taikhoan = fields.Many2one(comodel_name='congtyphaicu.congtyphaicu', string='Công ty phái cử')

    @api.onchange('nhomquyen_taikhoan')
    def onchange_method_nhomquyen_taikhoan(self):
        self.nghiepdoan_taikhoan = ()
        self.xinghep_taikhoan = ()
        self.phaicu_taikhoan = ()

    gioitinh_taikhoan = fields.Selection(string='Giới tính', selection=[('Nam', 'Nam'), ('Nữ', 'Nữ')])
    ngaysinh_taikhoan = fields.Date(string='Ngày sinh')
