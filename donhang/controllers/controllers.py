# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
import json

class Donhang(http.Controller):
    @http.route('/donhang/donhang/<string:id>', auth='public')
    def index(self, id=False, **kw):
        query = "SELECT * FROM thuctapsinh_thuctapsinh WHERE donhang_tts = %d" % int(id)
        request.cr.execute(query)
        id_needed = request.cr.fetchall()
        data = []
        for item in range(len(id_needed)):
            data.append({
                "ma_tts": id_needed[item][4],
                "ten_lt_tts": id_needed[item][5],
                "gtinh_tts": id_needed[item][10],
                "quoctich_tts": id_needed[item][7],
            })
        return json.dumps({'demo': data})

# class Donhang(http.Controller):
#     @http.route('/donhang/donhang/<string:id>', auth='public')
#     def index(self, id=False, **kw):
#         query = "SELECT * FROM thuctapsinh_thuctapsinh WHERE donhang_tts = %d" % int(id)
#         request.cr.execute(query)
#         id_needed = request.cr.fetchall()
#         data = []
#         for item in range(len(id_needed)):
#             data.append({
#                 "ma_tts": id_needed[item][4],
#                 "ten_lt_tts": id_needed[item][5],
#                 "gtinh_tts": id_needed[item][10],
#                 "quoctich_tts": id_needed[item][7],
#             })
#         return json.dumps({'demo': data})
