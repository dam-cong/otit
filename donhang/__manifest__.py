# -*- coding: utf-8 -*-
{
    'name': "Đơn hàng",
    'depends': ['base', 'congviec', 'congtyphaicu', 'xinghiep', 'nghiepdoan'],

    # always loaded
    'data': [
        'security/group_user_donhang.xml',
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/donhang_congviec.xml',
        'views/daingo.xml',
        'views/thuctapsinh.xml',
        # 'views/ngaynghi_dinhki.xml',
        # 'views/baohiem.xml',
        # 'views/congtac.xml',
        # 'views/phanloaithuctap.xml',
        # 'views/hocvan.xml',
        # 'views/kithi.xml',
        # 'views/capdo.xml',
        # 'views/loaikiemtra.xml',
        'views/hopdong.xml',
        # 'views/danhsach.xml',
        'views/danhsach_new.xml',
        'views/daotaosau.xml',
        # 'views/giangvien.xml',
        # 'views/dtscoso.xml',
    ],
}
