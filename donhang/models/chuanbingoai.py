# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ChuanBiNgoai(models.Model):
    _name = 'daotao.chuanbingoai'
    _rec_name = 'tencoquan_chuanbingoai'
    _order = 'id desc'

    daotao_phaicu_daotaotruoc = fields.Many2one(comodel_name='daotao.daotaotruoc',string="Cơ quan chuẩn bị nước ngoài")
    id = fields.Integer(string='STT')
    noidung_chuanbingoai = fields.Char(string='Nội dung')
    tencoquan_chuanbingoai = fields.Char(string='Tên cơ quan')
    diachicoquan_chuanbingoai = fields.Char(string='Địa chỉ cơ quan')
    tencoso_chuanbingoai = fields.Char(string='Tên cơ sở')
    daichicoso_chuanbingoai = fields.Char(string='Địa chỉ cơ sở')
    # tochuc_chuanbingoai = fields.Char(string='Loại tổ chức')
    tochuc_chuanbingoai = fields.Selection(string='Loại tổ chức', selection=[('Tổ chức cộng động', 'Tổ chức cộng đồng'), ('Cơ sở giáo dục', 'Cơ sở giáo dục')])
    batdau_chuanbingoai = fields.Date(string='Bắt đầu')
    ketthuc_chuanbingoai = fields.Date(string='Kết thúc')
    sogioi_chuanbingoai = fields.Float(string='Số giờ')
