# -*- coding: utf-8 -*-

from odoo import models, fields, api

class baohiemxahoi(models.Model):
    _name = 'thuctapsinh.baohiemxahoi'
    _rec_name = 'tenbaohiemxahoi'

    tenbaohiemxahoi = fields.Char(string="Tên bảo hiểm xã hội")  #


class nguoithan(models.Model):
    _name = 'thuctapsinh.nguoithan'
    _rec_name = 'hoten_nguoithan'

    nguoithan_onhat_tts = fields.Many2one(comodel_name="thuctapsinh.thuctapsinh", string="Thực tập sinh")
    quanhe_nguoithan = fields.Char(string='Mối quan hệ')
    hoten_nguoithan = fields.Char(string="Họ và tên")  #
    ngaysinh_nguoithan = fields.Date(string="Ngày sinh")  #
    quoctich_nguoithan = fields.Many2one(comodel_name='quoctich.quoctich',string="Quốc tịch")  #
    kehoach_songcung = fields.Selection(string="Lên kế hoạch sống cùng nhau",
                                        selection=[('Có', 'Có'), ('Không', 'Không')])  #
    diadiem_truonghoc = fields.Char(string="Địa điểm làm việc / trường học")  #
    sothe_cutru = fields.Char(string="Số thẻ cứ trú. Số chứng nhận thường trú đặc biệt")  #


class khungca(models.Model):
    _name = 'thuctapsinh.khungca'

    khungcacodinh_tts = fields.Many2one(comodel_name="thuctapsinh.thuctapsinh", string="Khung ca cố định")  #
    batdau_tgian_viecca = fields.Float(string="Bắt đầu")  # hhjp_0386
    ketthuc_tgian_viecca = fields.Float(string="Kết thúc")  # hhjp_0387
    apdung_tgian_viecca = fields.Date(string='Ngày áp dụng')
    sogio_laodong_motngay = fields.Float(string="Số giờ lao động một ngày")  # hhjp_0383


class loaikiemtra(models.Model):
    _name = 'thuctapsinh.ngaynghidinhki'
    _rec_name = 'ten_ngaynghi'

    ten_ngaynghi = fields.Char(string="Ngày nghỉ định kì")  #


class kehoachdoatao(models.Model):
    _name = 'thuctapsinh.kehoachdoatao'
    _rec_name = 'name_kehoachdaotao'

    dapan_kehoachdaotao = fields.Char(string="Đáp án")
    name_kehoachdaotao = fields.Char(string="Kế hoạch đào tạo")


class thoigiannghi(models.Model):
    _name = 'thuctapsinh.thoigiannghi'

    thoigian_batdau_nghi = fields.Float(string="Thời gian bắt đầu nghỉ giải lao")  # hhjp_0706
    thoigian_ketthuc_nghi = fields.Float(string="Thời gian kết thúc nghỉ giải lao")  # hhjp_0707
    thoigian_giailao_ca = fields.Many2one(comodel_name="thuctapsinh.thuctapsinh", string="Ca")
