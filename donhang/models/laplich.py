# -*- coding: utf-8 -*-

from odoo import models, fields, api


class DTCoSo(models.Model):
    _name = 'daotao.coso'
    _rec_name = 'ten_han_cosodaotao'

    # hienthi_cosodaotaosau = fields.Char(string='Hiển thị')
    daotaosau_coso = fields.Many2one(comodel_name='daotao.daotaosau')
    ten_han_cosodaotao = fields.Many2one(comodel_name='nghiepdoan.cosodaotao', string='Tên cơ sở')  # hhjp_0174
    diachi_cosodaotao = fields.Char(string="Địa chỉ cụ thể")  # hhjp_0175
    dienthoai_cosodaotao = fields.Char(string="Điện thoại")  # hhjp_0176
    buudien_cosodaotao = fields.Char(string="Số bưu điện")  #

    @api.onchange('ten_han_cosodaotao')
    def onchange_method_ten_han_cosodaotao(self):
        if self.ten_han_cosodaotao:
            self.diachi_cosodaotao = self.ten_han_cosodaotao.diachi_cosodaotao
            self.dienthoai_cosodaotao = self.ten_han_cosodaotao.dienthoai_cosodaotao
            self.buudien_cosodaotao = self.ten_han_cosodaotao.buudien_cosodaotao

            # cosodaotao = self.env['daotao.coso'].search([('daotaosau_coso', '=', self.daotaosau_coso.id)])
            # print(cosodaotao)
            # for item in cosodaotao:
            #     print(item.hienthi_cosodaotaosau)
            #     if item.hienthi_cosodaotaosau == '①':
            #         self.hienthi_cosodaotaosau = '②'
            #     elif item.hienthi_cosodaotaosau == '②':
            #         self.hienthi_cosodaotaosau = '③'
            #     elif item.hienthi_cosodaotaosau == '③':
            #         self.hienthi_cosodaotaosau = '④'
            #     elif item.hienthi_cosodaotaosau == '④':
            #         self.hienthi_cosodaotaosau = '⑤'
            #     elif item.hienthi_cosodaotaosau == '⑤':
            #         self.hienthi_cosodaotaosau = '⑥'
            #     elif item.hienthi_cosodaotaosau == '⑥':
            #         self.hienthi_cosodaotaosau = '⑦'
            #     elif item.hienthi_cosodaotaosau == '⑦':
            #         self.hienthi_cosodaotaosau = '⑧'
            #     elif item.hienthi_cosodaotaosau == '⑧':
            #         self.hienthi_cosodaotaosau = '⑨'
            #     elif item.hienthi_cosodaotaosau == '⑨':
            #         self.hienthi_cosodaotaosau = '⑩'
            #     else:
            #         self.hienthi_cosodaotaosau = '①'

    # @api.model
    # def create(self, values):
    #     # Add code here
    #     demo = super(DTCoSo, self).create(values)
    #     cosodaotao = self.env['daotao.coso'].search([('daotaosau_coso', '=', demo.daotaosau_coso.id)])
    #
    #     for item in cosodaotao:
    #         print(item.hienthi_cosodaotaosau)
    #         if item.hienthi_cosodaotaosau == '①':
    #             self.env['daotao.coso'].create(
    #                 {'hienthi_cosodaotaosau': '②'})
    #         elif item.hienthi_cosodaotaosau == '②':
    #             self.env['daotao.coso'].create(
    #                 {'hienthi_cosodaotaosau': '③'})
    #         elif item.hienthi_cosodaotaosau == '③':
    #             self.env['daotao.coso'].create(
    #                 {'hienthi_cosodaotaosau': '④'})
    #         elif item.hienthi_cosodaotaosau == '④':
    #             self.env['daotao.coso'].create(
    #                 {'hienthi_cosodaotaosau': '⑤'})
    #         elif item.hienthi_cosodaotaosau == '⑤':
    #             self.env['daotao.coso'].create(
    #                 {'hienthi_cosodaotaosau': '⑥'})
    #         elif item.hienthi_cosodaotaosau == '⑥':
    #             self.env['daotao.coso'].create(
    #                 {'hienthi_cosodaotaosau': '⑦'})
    #         elif item.hienthi_cosodaotaosau == '⑦':
    #             self.env['daotao.coso'].create(
    #                 {'hienthi_cosodaotaosau': '⑧'})
    #         elif item.hienthi_cosodaotaosau == '⑧':
    #             self.env['daotao.coso'].create(
    #                 {'hienthi_cosodaotaosau': '⑨'})
    #         elif item.hienthi_cosodaotaosau == '⑨':
    #             self.env['daotao.coso'].create(
    #                 {'hienthi_cosodaotaosau': '⑩'})
    #         else:
    #             self.env['daotao.coso'].create(
    #                 {'hienthi_cosodaotaosau': '①'})


class LapLich(models.Model):
    _name = 'laplich.laplich'

    daotaosau = fields.Many2one(comodel_name='daotao.daotaosau')

    lich_laplich = fields.Date(string='Lịch')  # dt_0005
    ngaynghi_laplich = fields.Boolean(string='Ngày nghỉ')

    coso_sang_laplich = fields.Boolean(string='Mượn cơ sở')  # dt_0007

    # noidung_sang_laplich = fields.Selection(string="Nội dung ca sáng",
    #                                         selection=[('日本語基礎教育', '日本語基礎教育'),
    #                                                    ('本邦での生活一般に関する知識', '本邦での生活一般に関する知識'),
    #                                                    ('日本語基礎教育（生活習慣）', '日本語基礎教育（生活習慣）'),
    #                                                    ('日本の生活習慣・社会見学', '日本の生活習慣・社会見学'),
    #                                                    ('技能実習に関するガイダンス', '技能実習に関するガイダンス'),
    #                                                    ('日本語基礎教育', '日本語基礎教育）'),
    #                                                    ('法的保護等に必要な情報', '法的保護等に必要な情報'),
    #                                                    ('法的保護等に必要な情報（労働法令）', '法的保護等に必要な情報（労働法令）'),
    #                                                    ('法的保護等に必要な情報（入管法令・技能実習制度）', '法的保護等に必要な情報（入管法令・技能実習制度）'),
    #                                                    ('火災予防教育、防災訓練', '火災予防教育、防災訓練'),
    #                                                    ('見学（消防訓練）〇〇防災センター交通安全、防犯教育', '見学（消防訓練）〇〇防災センター交通安全、防犯教育'),
    #                                                    ('見学（交通安全）〇〇警察署社会見学', '見学（交通安全）〇〇警察署社会見学'),
    #                                                    ('（実習職種）に関する知識', '（実習職種）に関する知識'),
    #                                                    ('企業勤務の注意点、規律、心構え入校式・オリエンテーション', '企業勤務の注意点、規律、心構え入校式・オリエンテーション'),
    #                                                    ('閉校式（ｵﾘｴﾝﾃｰｼｮﾝ）企業配属', '閉校式（ｵﾘｴﾝﾃｰｼｮﾝ）企業配属'),
    #                                                    ('その他（　　　　　　）', 'その他（　　　　　　）'),
    #                                                    ('休日', '休日')], required=False, )
    # 
    # noidung_chieu_laplich = fields.Selection(string="Nội dung ca chiều",
    #                                         selection=[('日本語基礎教育', '日本語基礎教育'),
    #                                                    ('本邦での生活一般に関する知識', '本邦での生活一般に関する知識'),
    #                                                    ('日本語基礎教育（生活習慣）', '日本語基礎教育（生活習慣）'),
    #                                                    ('日本の生活習慣・社会見学', '日本の生活習慣・社会見学'),
    #                                                    ('技能実習に関するガイダンス', '技能実習に関するガイダンス'),
    #                                                    ('日本語基礎教育', '日本語基礎教育）'),
    #                                                    ('法的保護等に必要な情報', '法的保護等に必要な情報'),
    #                                                    ('法的保護等に必要な情報（労働法令）', '法的保護等に必要な情報（労働法令）'),
    #                                                    ('法的保護等に必要な情報（入管法令・技能実習制度）', '法的保護等に必要な情報（入管法令・技能実習制度）'),
    #                                                    ('火災予防教育、防災訓練', '火災予防教育、防災訓練'),
    #                                                    ('見学（消防訓練）〇〇防災センター交通安全、防犯教育', '見学（消防訓練）〇〇防災センター交通安全、防犯教育'),
    #                                                    ('見学（交通安全）〇〇警察署社会見学', '見学（交通安全）〇〇警察署社会見学'),
    #                                                    ('（実習職種）に関する知識', '（実習職種）に関する知識'),
    #                                                    ('企業勤務の注意点、規律、心構え入校式・オリエンテーション', '企業勤務の注意点、規律、心構え入校式・オリエンテーション'),
    #                                                    ('閉校式（ｵﾘｴﾝﾃｰｼｮﾝ）企業配属', '閉校式（ｵﾘｴﾝﾃｰｼｮﾝ）企業配属'),
    #                                                    ('その他（　　　　　　）', 'その他（　　　　　　）'),
    #                                                    ('休日', '休日')], required=False, )

    noidung_sang_laplich = fields.Many2one(comodel_name='daotaosau.noidungdaotao',string='Nội dung ca sáng')  # dt_0006
    noidung_chieu_laplich = fields.Many2one(comodel_name='daotaosau.noidungdaotao',string='Nội dung ca chiều')  # dt_0008

    coso_chieu_laplich = fields.Boolean(string='Mượn cơ sở')  # dt_0009
    cosao_daotao_laplich = fields.Many2one(comodel_name='daotao.coso', string='Cơ sở đào tạo')  # dt_0010
    thoigian_laplich = fields.Integer(string='Thời gian')  # dt_0011
    # giaovien_laplich = fields.Char(string='Giáo viên')  # dt_0012
    giaovien_laplich = fields.Many2one(comodel_name='giangvien.giangvien', string='Giáo viên')  # dt_0012

    @api.onchange('ngaynghi_laplich')
    def onchange_method_ngaynghi_laplich(self):
        noidungdaotao = self.env['daotaosau.noidungdaotao'].search([('noidungdaotao', '=', '休日')], limit=1)
        if self.ngaynghi_laplich == True:
            self.noidung_sang_laplich = noidungdaotao.id
            self.noidung_chieu_laplich = noidungdaotao.id
            self.cosao_daotao_laplich = ''
            self.thoigian_laplich = 0
            self.giaovien_laplich = ''
        elif self.ngaynghi_laplich == False:
            self.noidung_sang_laplich = ''
            self.noidung_chieu_laplich = ''
            self.cosao_daotao_laplich = ''
            self.thoigian_laplich = 0
            self.giaovien_laplich = ''

    @api.onchange('noidung_sang_laplich', 'noidung_chieu_laplich')
    def onchange_method_thoigian_laplich(self):
        noidungdaotao = self.env['daotaosau.noidungdaotao'].search([('noidungdaotao', '=', '休日')], limit=1)
        if self.noidung_sang_laplich and self.noidung_chieu_laplich:
            if self.noidung_sang_laplich != noidungdaotao.id and self.noidung_chieu_laplich != noidungdaotao.id:
                self.thoigian_laplich = 8
        elif self.noidung_sang_laplich or self.noidung_chieu_laplich:
            if self.noidung_sang_laplich != noidungdaotao.id or self.noidung_chieu_laplich != noidungdaotao.id:
                self.thoigian_laplich = 4
        else:
            self.thoigian_laplich = 0
