# -*- coding: utf-8 -*-
from odoo import models, fields, api
import logging
from datetime import datetime
import datetime
from datetime import timedelta
from datetime import date
import re

_logger = logging.getLogger(__name__)


def addYears(d, years):
    try:
        # Return same day of the current year
        return d.replace(year=d.year + years)
    except ValueError:
        # If not same day, it will return other, i.e.  February 29 to March 1 etc.
        return d + (date(d.year + years, 1, 1) - date(d.year, 1, 1))


class thuctapsinh(models.Model):
    _name = 'thuctapsinh.thuctapsinh'
    _rec_name = 'ten_lt_tts'
    _order = 'id asc'

    avatar = fields.Binary("Ảnh")  # hh_18
    # Sơ yếu lý lịch
    donhang_tts = fields.Many2one(comodel_name="donhang.donhang", string="Đơn hàng", required=True, )
    xinghiep_thuctapsinh = fields.Many2one(comodel_name="xinghiep.xinghiep", string="Xí nghiệp",
                                           related="donhang_tts.xinghiep_donhang")  #
    congtyphaicu_thuctapsinh = fields.Many2one(comodel_name="congtyphaicu.congtyphaicu", string="Công ty phái cử",
                                               related="donhang_tts.congtyphaicu_donhang")  #
    daotaotruoc_tts = fields.Many2one(comodel_name='daotao.daotaotruoc', string='Đào tạo')

    @api.onchange('donhang_tts')
    def onchange_donhang_tts(self):
        self.daotaotruoc_tts = self.donhang_tts.id

    def no_accent_vietnamese(s):
        s = re.sub(r'[àáạảãâầấậẩẫăằắặẳẵ]', 'a', s)
        s = re.sub(r'[ÀÁẠẢÃĂẰẮẶẲẴÂẦẤẬẨẪ]', 'A', s)
        s = re.sub(r'[èéẹẻẽêềếệểễ]', 'e', s)
        s = re.sub(r'[ÈÉẸẺẼÊỀẾỆỂỄ]', 'E', s)
        s = re.sub(r'[òóọỏõôồốộổỗơờớợởỡ]', 'o', s)
        s = re.sub(r'[ÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠ]', 'O', s)
        s = re.sub(r'[ìíịỉĩ]', 'i', s)
        s = re.sub(r'[ÌÍỊỈĨ]', 'I', s)
        s = re.sub(r'[ùúụủũưừứựửữ]', 'u', s)
        s = re.sub(r'[ƯỪỨỰỬỮÙÚỤỦŨ]', 'U', s)
        s = re.sub(r'[ỳýỵỷỹ]', 'y', s)
        s = re.sub(r'[ỲÝỴỶỸ]', 'Y', s)
        s = re.sub(r'[Đ]', 'D', s)
        s = re.sub(r'[đ]', 'd', s)
        return s

    @api.onchange('ten_tv_tts')
    def con(self):
        if self.ten_tv_tts:
            name_bg = thuctapsinh.no_accent_vietnamese(str(self.ten_tv_tts))
            name_bg_upper = str(name_bg).upper()
            self.ten_lt_tts = str(name_bg_upper)
            name_split = str(name_bg_upper).split()

            convert_jp = ""
            for i in name_split:
                convert_jp_lib = self.env['library.jp'].sudo().search([('name', '=', i)], limit=1).name_jp
                if convert_jp == '':
                    convert_jp = str(convert_jp_lib)
                else:
                    convert_jp = str(convert_jp) + "・" + str(convert_jp_lib)
            self.ten_katakana_tts = convert_jp

    ma_tts = fields.Char(string="Mã thực tập sinh", required=True,
                         help="Nhập mã thực tập sinh không trùng với những thực tập sinh đã nhập trước")  #

    custom_id = fields.Char('Mã số')
    _sql_constraints = [('unique_id', 'UNIQUE(ma_tts)', "Đã tồn tại TTS có mã số này")]

    ten_lt_tts = fields.Char(string="Họ và tên (Latinh)")  # hhjp_0001
    ten_tv_tts = fields.Char(string="Họ và tên (Tiếng mẹ đẻ)")  # hhjp_0001_v
    ten_han_tts = fields.Char(string="Họ và tên (Hán)")  # hhjp_0002
    ten_katakana_tts = fields.Char(string='Họ và tên (Katakana)')
    quoctich_tts = fields.Many2one(comodel_name='quoctich.quoctich', string='Quốc tịch')  # hhjp_0003
    quoctich_tts_v = fields.Char(string="Quốc tịch(Tiếng mẹ đẻ)")  # hhjp_0003_v

    @api.onchange('quoctich_tts')
    def onchange_method_quoctich_tts(self):
        if self.quoctich_tts:
            self.quoctich_tts_v = self.quoctich_tts.name_quoctich_viet

    ngaysinh_tts = fields.Date(string="Ngày sinh")  # hhjp_0004

    tuoi_tts = fields.Integer(string="Tuổi", compute='_tuoi_tts')  # hhjp_0005

    @api.one
    @api.depends('ngaysinh_tts')
    def _tuoi_tts(self):
        ngay_sinh = fields.Date.today()
        if self.ngaysinh_tts:
            tmp = ngay_sinh.year - self.ngaysinh_tts.year
            if ngay_sinh.month == self.ngaysinh_tts.month:
                if ngay_sinh.day < self.ngaysinh_tts.day:
                    tmp = tmp - 1
            elif ngay_sinh.month < self.ngaysinh_tts.month:
                tmp = tmp - 1
            self.tuoi_tts = int(tmp)

    gtinh_tts = fields.Selection(string="Giới tính", selection=[('Nam', 'Nam'), ('Nữ', 'Nữ')],
                                 default='Nam')  # hhjp_0006
    nguoikethon_tts = fields.Selection(string="Người kết hôn",
                                       selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')  # hhjp_0222
    tiengmede_tts = fields.Char(string="Tiếng mẹ đẻ")  # hhjp_0330
    tiengmede_tts_v = fields.Char(string="Tiếng mẹ đẻ")  # hhjp_0330_v
    diachi_tts = fields.Char(string="Địa chỉ hiện tại")  # hhjp_0331
    diachi_tts_v = fields.Char(string="Địa chỉ hiện tại(Tiếng mẹ đẻ)")  # hhjp_0331_v

    @api.onchange('diachi_tts_v')
    def onchange_method_diachi_tts_v(self):
        if self.diachi_tts_v:
            name_bg = thuctapsinh.no_accent_vietnamese(str(self.diachi_tts_v))
            name_bg_upper = str(name_bg).upper()
            self.diachi_tts = str(name_bg_upper)

    # 2. Ngày nhập cảnh / trở về
    ngaynhapcanh_tts = fields.Date(string="Ngày nhập cảnh", store=True, related='donhang_tts.thoigian_donhang')  #
    ngaynhapcanh_thucte_tts = fields.Date()  #
    ngay_chuyennhuong_kehoach_tts = fields.Date(string='Ngày về xí nghiệp')  #
    ngay_chuyennhuong_thucte_tts = fields.Date()  #
    ngay_venuoc_tts = fields.Date(string="Ngày về nước")  # hhjp_0206
    ngay_venuoc_thucte_tts = fields.Date()  # hhjp_0206

    batdau_kynang_tts = fields.Date(string="Thực tập kĩ năng số 2")  # bs_0052
    ketthuc_kynang_tts = fields.Date()  # bs_0053
    batdau_xaydung_tts = fields.Date(string="Xây dựng - đóng tàu")  # bs_0054
    ketthuc_xaydung_tts = fields.Date()  # bs_0055
    batdau_ungvien_tts = fields.Date(string="Ứng viên điều dưỡng - phúc lợi")  # bs_0056

    batdau_kynang_tts3 = fields.Date(string="Thực tập kĩ năng số 2")  # bs_0052
    ketthuc_kynang_tts3 = fields.Date()  # bs_0053
    batdau_xaydung_tts3 = fields.Date(string="Xây dựng - đóng tàu")  # bs_0054
    ketthuc_xaydung_tts3 = fields.Date()  # bs_0055
    batdau_ungvien_tts3 = fields.Date(string="Ứng viên điều dưỡng - phúc lợi")  # bs_0056

    ketthuc_ungvien_tts = fields.Date()  # bs_0057
    ketthuc_ungvien_tts3 = fields.Date()  # bs_0057
    tonggio_khoahoc_saunhapcannh = fields.Integer(string="Tổng số giờ đào tạo sau nhập cảnh")  # hhjp_0250
    # Ngày về nước giữa chừng
    venuoc_giuachung_tts = fields.Char(string='Ngày về nước giữa chừng')
    giaidoan_venuoc_giuachung_tts = fields.Many2one(comodel_name='giaidoan.giaidoan', string='Giai đoạn')
    ngay_venuoc_giuachung_tts = fields.Date(string='Ngày tháng')
    # Bỏ trốn
    botron_tts = fields.Char(string='Bỏ trốn')
    giaidoan_botron_tts = fields.Many2one(comodel_name='giaidoan.giaidoan', string='Giai đoạn')
    ngay_botron_tts = fields.Date(string='Ngày tháng')
    lydo_tuvong_tts = fields.Char(string='Lý do tử vong')
    ngay_tuvong_tts = fields.Date(string='Ngày tử vong')
    # 3. Nội dung thực tập kỹ năng
    daotao_hientai = fields.Many2one(comodel_name="thuctapsinh.kehoachdoatao",
                                     string="Phân loại đào tạo hiện tại")  # hhjp_0113
    kehoach_doatao = fields.Many2one(comodel_name="thuctapsinh.kehoachdoatao",
                                     string="Phân loại kế hoạch đào tạo")  # hhjp_0113
    # BẢNG 1
    sochungnhan_namnhat = fields.Char(string="Số chứng nhận")  # hhjp_0911
    ngaychungnhan_namnhat = fields.Date(string="Ngày chứng nhận")  #
    sochungnhan_namhai = fields.Char(string="Số chứng nhận")  #
    ngaychungnhan_namhai = fields.Date(string="Ngày chứng nhận")  #
    sochungnhan_namba = fields.Char(string="Số chứng nhận")  #
    ngaychungnhan_namba = fields.Date(string="Ngày chứng nhận")  #

    # Nghề nghiệp phải chuyển nhượng
    ma_congviec_chuyennhuong = fields.Char(string="Mã công việc", store=True,
                                           related='donhang_tts.ma_congviec_donhang')  # hhjp_0114
    nganhnghe_loainghe_chuyennhuong = fields.Many2one(comodel_name="nganhnghe.nganhnghe",
                                                      string="Ngành nghề - Loại nghề", store=True,
                                                      related='donhang_tts.nganhnghe_donhang')
    # related='donhang_tts.loainghe_donhang')  # hhjp_0115
    congviec_chuyennhuong = fields.Many2one(comodel_name="congviec.congviec", string="Công việc", store=True,
                                            related='donhang_tts.congviec_donhang')  # hhjp_0116

    kiemtra_chuyennhuong_namnhat = fields.Many2one(comodel_name="thuctapsinh.loaikiemtra",
                                                   string="Loại kiểm tra")  # hhjp_0131_kiemtra

    nganhnghe_tts = fields.Many2one(comodel_name="loaicongviec.loaicongviec", string="Ngành nghề xin thư tiến cử",
                                    related="donhang_tts.loainghe_donhang")
    nganhnghe_tts_a = fields.Char(string="Ngành nghề xin thư tiến cử(Tiếng anh)",
                                  related="donhang_tts.loainghe_donhang_a")
    nganhnghe_tts_v = fields.Char(string="Ngành nghề xin thư tiến cử(Tiếng mẹ đẻ)",
                                  related="donhang_tts.loainghe_donhang_v")

    kithi_chuyennhuong_namnhat = fields.Many2one(comodel_name='thuctapsinh.kithi',
                                                 string="Tên kì thi")  # hhjp_0131_kithi
    capdo_chuyennhuong_namnhat = fields.Many2one(comodel_name='thuctapsinh.capdo', string="Cấp độ")  # hhjp_0131_capdo

    kiemtra_chuyennhuong_namhai = fields.Many2one(comodel_name="thuctapsinh.loaikiemtra",  #
                                                  string="Loại kiểm tra")  #

    kithi_chuyennhuong_namhai = fields.Many2one(comodel_name='thuctapsinh.kithi', string="Tên kì thi")  #
    capdo_chuyennhuong_namhai = fields.Many2one(comodel_name='thuctapsinh.capdo', string="Cấp độ")  #

    kiemtra_chuyennhuong_namba = fields.Many2one(comodel_name="thuctapsinh.loaikiemtra",
                                                 string="Loại kiểm tra")  #

    kithi_chuyennhuong_namba = fields.Many2one(comodel_name='thuctapsinh.kithi', string="Tên kì thi")  #
    capdo_chuyennhuong_namba = fields.Many2one(comodel_name='thuctapsinh.capdo', string="Cấp độ")  #

    # Nghề nghiệp nhiều công việc
    ma_congviec_nhieucongviec = fields.Char(string="Mã công việc", store=True,
                                            related='donhang_tts.ma_congviec_nhieunganhnghe')
    # related='macongviec_congviec')  # hhjp_0117
    nganhnghe_loainghe_nhieucongviec = fields.Many2one(comodel_name="nganhnghe.nganhnghe",
                                                       string="Ngành nghề - Loại nghề", store=True,
                                                       related='donhang_tts.nganhnghe_nhieunganhnghe')
    # related='ten_nganhnghe_congviec')  # hhjp_0118
    congviec_nhieucongviec = fields.Many2one(comodel_name="congviec.congviec", string="Công việc", store=True
                                             , related='donhang_tts.congviec_nhieunganhnghe')

    kiemtra_nhieucongviec_namnhat = fields.Many2one(comodel_name="thuctapsinh.loaikiemtra",
                                                    string="Loại kiểm tra")  # hhjp_0132_kiemtra
    kithi_nhieucongviec_namnhat = fields.Many2one(comodel_name='thuctapsinh.kithi',
                                                  string="Tên kì thi")  # hhjp_0132_kithi
    capdo_nhieucongviec_namnhat = fields.Many2one(comodel_name='thuctapsinh.capdo', string="Cấp độ")  # hhjp_0132_capdo
    kiemtra_nhieucongviec_namhai = fields.Many2one(comodel_name="thuctapsinh.loaikiemtra", string="Loại kiểm tra")
    kithi_nhieucongviec_namhai = fields.Many2one(comodel_name='thuctapsinh.kithi', string="Tên kì thi")  #
    capdo_nhieucongviec_namhai = fields.Many2one(comodel_name='thuctapsinh.capdo', string="Cấp độ")  #
    kiemtra_nhieucongviec_namba = fields.Many2one(comodel_name="thuctapsinh.loaikiemtra", string="Loại kiểm tra")
    kithi_nhieucongviec_namba = fields.Many2one(comodel_name='thuctapsinh.kithi', string="Tên kì thi")  #
    capdo_nhieucongviec_namba = fields.Many2one(comodel_name='thuctapsinh.capdo', string="Cấp độ")  #

    # Ngành nghề khác
    ghichu_namnhat = fields.Char(string="Ghi chú")  # hhjp_0215
    ghichu_namhai = fields.Char(string="Ghi chú")  #
    ghichu_namba = fields.Char(string="Ghi chú")  #

    # BẢNG 2
    ngaythi_lythuyet_motcongviec_namnhat = fields.Date()  #
    ketqua_lythuyet_motcongviec_namnhat = fields.Selection(string="Kết quả",
                                                           selection=[('Qua', 'Qua'), ('Không qua', 'Không qua')])
    ngayqua_lythuyet_motcongviec_namnhat = fields.Date()  #
    ngaythi_lythuyet_motcongviec_namhai = fields.Date()  #
    ketqua_lythuyet_motcongviec_namhai = fields.Selection(string="Kết quả",
                                                          selection=[('Qua', 'Qua'), ('Không qua', 'Không qua')])
    ngayqua_lythuyet_motcongviec_namhai = fields.Date()  #
    ngaythi_lythuyet_motcongviec_namba = fields.Date()  #
    ketqua_lythuyet_motcongviec_namba = fields.Selection(string="Kết quả",
                                                         selection=[('Qua', 'Qua'), ('Không qua', 'Không qua')])
    ngayqua_lythuyet_motcongviec_namba = fields.Date()  #
    ngaythi_thuchanh_motcongviec_namnhat = fields.Date()  #
    ketqua_thuchanh_motcongviec_namnhat = fields.Selection(string="Kết quả",
                                                           selection=[('Qua', 'Qua'), ('Không qua', 'Không qua')])
    ngayqua_thuchanh_motcongviec_namnhat = fields.Date()  #
    ngaythi_thuchanh_motcongviec_namhai = fields.Date()  #
    ketqua_thuchanh_motcongviec_namhai = fields.Selection(string="Kết quả",
                                                          selection=[('Qua', 'Qua'), ('Không qua', 'Không qua')])
    ngayqua_thuchanh_motcongviec_namhai = fields.Date()  #
    ngaythi_thuchanh_motcongviec_namba = fields.Date()  #
    ketqua_thuchanh_motcongviec_namba = fields.Selection(string="Kết quả",
                                                         selection=[('Qua', 'Qua'), ('Không qua', 'Không qua')])
    ngayqua_thuchanh_motcongviec_namba = fields.Date()  #

    ngaythi_lythuyet_nhieucongviec_namnhat = fields.Date()  #
    ketqua_lythuyet_nhieucongviec_namnhat = fields.Selection(string="Kết quả",
                                                             selection=[('Qua', 'Qua'), ('Không qua', 'Không qua')])
    ngayqua_lythuyet_nhieucongviec_namnhat = fields.Date()  #
    ngaythi_lythuyet_nhieucongviec_namhai = fields.Date()  #
    ketqua_lythuyet_nhieucongviec_namhai = fields.Selection(string="Kết quả",
                                                            selection=[('Qua', 'Qua'), ('Không qua', 'Không qua')])
    ngayqua_lythuyet_nhieucongviec_namhai = fields.Date()  #
    ngaythi_lythuyet_nhieucongviec_namba = fields.Date()  #
    ketqua_lythuyet_nhieucongviec_namba = fields.Selection(string="Kết quả",
                                                           selection=[('Qua', 'Qua'), ('Không qua', 'Không qua')])
    ngayqua_lythuyet_nhieucongviec_namba = fields.Date()  #
    ngaythi_thuchanh_nhieucongviec_namnhat = fields.Date()  #
    ketqua_thuchanh_nhieucongviec_namnhat = fields.Selection(string="Kết quả",
                                                             selection=[('Qua', 'Qua'), ('Không qua', 'Không qua')])
    ngayqua_thuchanh_nhieucongviec_namnhat = fields.Date()  #
    ngaythi_thuchanh_nhieucongviec_namhai = fields.Date()  #
    ketqua_thuchanh_nhieucongviec_namhai = fields.Selection(string="Kết quả",
                                                            selection=[('Qua', 'Qua'), ('Không qua', 'Không qua')])
    ngayqua_thuchanh_nhieucongviec_namhai = fields.Date()  #
    ngaythi_thuchanh_nhieucongviec_namba = fields.Date()  #
    ketqua_thuchanh_nhieucongviec_namba = fields.Selection(string="Kết quả",
                                                           selection=[('Qua', 'Qua'), ('Không qua', 'Không qua')])
    ngayqua_thuchanh_nhieucongviec_namba = fields.Date()  #

    # BẢNG 3: THỜI GIAN THỤC TẬP
    batdau_kehoach_namnhat = fields.Date(string="Bắt đầu")  # hhjp_0313
    ketthuc_kehoach_namnhat = fields.Date(string="Kết thúc", compute='_ketthuc_kehoach_namnhat',
                                          store='True')  # hhjp_0314
    tongthoigian_kehoach_namnhat = fields.Char(string="Tổng thời gian")  # hhjp_0149

    @api.one
    @api.depends('batdau_kehoach_namnhat')
    def _ketthuc_kehoach_namnhat(self):
        if self.batdau_kehoach_namnhat:
            ketthuc = addYears(datetime.date(self.batdau_kehoach_namnhat.year, self.batdau_kehoach_namnhat.month,
                                             self.batdau_kehoach_namnhat.day), 1)
            self.ketthuc_kehoach_namnhat = ketthuc - timedelta(days=1)
            self.tongthoigian_kehoach_namnhat = '1年0月0日間'
        else:
            self.tongthoigian_kehoach_namnhat = '0年0月0日間'

    sogio_thuctap_kehoach_namnhat = fields.Char()

    batdau_kehoach_namhai = fields.Date(string="Bắt đầu")  # hhjp_0313
    ketthuc_kehoach_namhai = fields.Date(string="Kết thúc", compute='_ketthuc_kehoach_namhai',
                                         store='True')  # hhjp_0314
    tongthoigian_kehoach_namhai = fields.Char(string="Tổng thời gian")  # hhjp_0149
    sogio_thuctap_kehoach_namhai = fields.Char()
    ketthuc_kehoach_namhai_go1 = fields.Date(string="Kết thúc", compute='_ketthuc_kehoach_namhai_go1',
                                             store='True')
    ketthuc_kehoach_namhai_go2 = fields.Date(string="Kết thúc", compute='_ketthuc_kehoach_namhai_go2',
                                             store='True')

    @api.one
    @api.depends('batdau_kehoach_namhai')
    def _ketthuc_kehoach_namhai(self):
        if self.batdau_kehoach_namhai:
            ketthuc = addYears(datetime.date(self.batdau_kehoach_namhai.year, self.batdau_kehoach_namhai.month,
                                             self.batdau_kehoach_namhai.day), 2)
            self.ketthuc_kehoach_namhai = ketthuc - timedelta(days=1)
            self.tongthoigian_kehoach_namhai = '2年0月0日間'
        else:
            self.tongthoigian_kehoach_namhai = '0年0月0日間'

    @api.one
    @api.depends('batdau_kehoach_namhai')
    def _ketthuc_kehoach_namhai_go1(self):
        if self.batdau_kehoach_namhai:
            ketthuc = addYears(datetime.date(self.batdau_kehoach_namhai.year, self.batdau_kehoach_namhai.month,
                                             self.batdau_kehoach_namhai.day), 1)
            self.ketthuc_kehoach_namhai_go1 = ketthuc - timedelta(days=1)

    @api.one
    @api.depends('ketthuc_kehoach_namhai_go1')
    def _ketthuc_kehoach_namhai_go2(self):
        if self.ketthuc_kehoach_namhai_go1:
            ketthuc = addYears(
                datetime.date(self.ketthuc_kehoach_namhai_go1.year, self.ketthuc_kehoach_namhai_go1.month,
                              self.ketthuc_kehoach_namhai_go1.day), 1)
            self.ketthuc_kehoach_namhai_go2 = ketthuc - timedelta(days=1)

    batdau_kehoach_namba = fields.Date(string="Bắt đầu")  # hhjp_0313
    ketthuc_kehoach_namba = fields.Date(string="Kết thúc", compute='_ketthuc_kehoach_namba',
                                        store='true')  # hhjp_0314
    tongthoigian_kehoach_namba = fields.Char(string="Tổng thời gian")  #
    sogio_thuctap_kehoach_namba = fields.Char()
    ketthuc_kehoach_namba_go1 = fields.Date(string="Kết thúc", compute='_ketthuc_kehoach_namba_go1',
                                            store='true')  # hhjp_0314_go1
    ketthuc_kehoach_namba_go2 = fields.Date(string="Kết thúc", compute='_ketthuc_kehoach_namba_go2',
                                            store='true')  # hhjp_0314_go2

    @api.one
    @api.depends('batdau_kehoach_namba')
    def _ketthuc_kehoach_namba(self):
        if self.batdau_kehoach_namba:
            ketthuc = addYears(datetime.date(self.batdau_kehoach_namba.year, self.batdau_kehoach_namba.month,
                                             self.batdau_kehoach_namba.day), 2)
            self.ketthuc_kehoach_namba = ketthuc - timedelta(days=1)
            self.tongthoigian_kehoach_namba = '2年0月0日間'
        else:
            self.tongthoigian_kehoach_namba = '0年0月0日間'

    @api.one
    @api.depends('batdau_kehoach_namba')
    def _ketthuc_kehoach_namba_go1(self):
        if self.batdau_kehoach_namba:
            ketthuc = addYears(datetime.date(self.batdau_kehoach_namba.year, self.batdau_kehoach_namba.month,
                                             self.batdau_kehoach_namba.day), 1)
            self.ketthuc_kehoach_namba_go1 = ketthuc - timedelta(days=1)

    @api.one
    @api.depends('ketthuc_kehoach_namba_go1')
    def _ketthuc_kehoach_namba_go2(self):
        if self.ketthuc_kehoach_namba_go1:
            ketthuc = addYears(
                datetime.date(self.ketthuc_kehoach_namba_go1.year, self.ketthuc_kehoach_namba_go1.month,
                              self.ketthuc_kehoach_namba_go1.day), 1)
            self.ketthuc_kehoach_namba_go2 = ketthuc - timedelta(days=1)

    batdau_thucte_namnhat = fields.Date(string="Bắt đầu")
    ketthuc_thucte_namnhat = fields.Date(string="Kết thúc")
    batdau_thucte_namhai = fields.Date(string="Bắt đầu")
    ketthuc_thucte_namhai = fields.Date(string="Kết thúc")
    batdau_thucte_namba = fields.Date(string="Bắt đầu")
    ketthuc_thucte_namba = fields.Date(string="Kết thúc")
    tucach_luutru_1_so_namnhat = fields.Char()
    thoigian_luutru_1_so_namnhat = fields.Char()
    hieuluc_luutru_1_so_namnhat = fields.Date()
    hieuluc_kethuc_luutru_1_so_namnhat = fields.Date()
    tucach_luutru_1_so_namhai = fields.Char()
    thoigian_luutru_1_so_namhai = fields.Char()
    hieuluc_luutru_1_so_namhai = fields.Date()
    hieuluc_kethuc_luutru_1_so_namhai = fields.Date()
    tucach_luutru_1_so_namba = fields.Char()
    thoigian_luutru_1_so_namba = fields.Char()
    hieuluc_luutru_1_so_namba = fields.Date()
    hieuluc_kethuc_luutru_1_so_namba = fields.Date()

    tucach_luutru_2_so_namnhat = fields.Char()
    thoigian_luutru_2_so_namnhat = fields.Char()
    hieuluc_luutru_2_so_namnhat = fields.Date()
    hieuluc_kethuc_luutru_2_so_namnhat = fields.Date()
    tucach_luutru_2_so_namhai = fields.Char()
    thoigian_luutru_2_so_namhai = fields.Char()
    hieuluc_luutru_2_so_namhai = fields.Date()
    hieuluc_kethuc_luutru_2_so_namhai = fields.Date()
    tucach_luutru_2_so_namba = fields.Char()
    thoigian_luutru_2_so_namba = fields.Char()
    hieuluc_luutru_2_so_namba = fields.Date()
    hieuluc_kethuc_luutru_2_so_namba = fields.Date()

    baocao_tiepnhan_namnhat = fields.Char()
    baocao_tiepnhan_namhai = fields.Char()
    baocao_tiepnhan_namba = fields.Char()

    daotao_truoc_nhapcanh = fields.Boolean(string="Đào tạo trước nhập cảnh")  # hhjp_0120
    boicanh_daotao_kythuat = fields.Text(string="Bối cảnh đào tạo kỹ thuật")  # hhjp_0580
    thuctapkinang_canthiet = fields.Text(string="Tính cần thiết của thực tập kỹ năng")  # hhjp_0581

    # Bảng nội dung xác minh
    noidung_xacminh_a_donhang_xinghiep = fields.Boolean(string='A. 本邦において従事しようとする業務と同種の業務に外国において従事した経験を有する場合')

    noidung_xacminh_b_donhang_xinghiep = fields.Boolean(string='B. 団体監理型技能実習に従事することを必要とする特別な事情がある場合')
    noidung_xacminh_b_a_donhang_xinghiep = fields.Boolean(string='a. 申請者又は監理団体と送出国との間の技術協力上特に必要があると認められる場合')
    noidung_xacminh_b_b_donhang_xinghiep = fields.Boolean(string='b. 教育機関において、同種の業務に関連する教育課程を修了している場合（修了見込みの場合も含む。）')
    noidung_xacminh_b_c_donhang_xinghiep = fields.Boolean(
        string='c. 申請者が当該技能実習を行わせる必要性を具体的に説明でき、かつ、技能実習生が当該技能実習を本邦で行うために必要な最低限の訓練を受けている場合')

    @api.onchange('noidung_xacminh_b_donhang_xinghiep')
    def onchange_method_noidung_xacminh_b_donhang_xinghiep(self):
        if self.noidung_xacminh_b_donhang_xinghiep == False:
            self.noidung_xacminh_b_a_donhang_xinghiep = False
            self.noidung_xacminh_b_b_donhang_xinghiep = False
            self.noidung_xacminh_b_c_donhang_xinghiep = False

    @api.onchange('noidung_xacminh_b_a_donhang_xinghiep')
    def onchange_method_noidung_xacminh_b_a_donhang_xinghiep(self):
        if self.noidung_xacminh_b_a_donhang_xinghiep == True:
            self.noidung_xacminh_b_donhang_xinghiep = True
        elif self.noidung_xacminh_b_a_donhang_xinghiep == False and self.noidung_xacminh_b_b_donhang_xinghiep == False and self.noidung_xacminh_b_c_donhang_xinghiep == False:
            self.noidung_xacminh_b_donhang_xinghiep = False

    @api.onchange('noidung_xacminh_b_b_donhang_xinghiep')
    def onchange_method_noidung_xacminh_b_b_donhang_xinghiep(self):
        if self.noidung_xacminh_b_b_donhang_xinghiep == True:
            self.noidung_xacminh_b_donhang_xinghiep = True
        elif self.noidung_xacminh_b_a_donhang_xinghiep == False and self.noidung_xacminh_b_b_donhang_xinghiep == False and self.noidung_xacminh_b_c_donhang_xinghiep == False:
            self.noidung_xacminh_b_donhang_xinghiep = False

    @api.onchange('noidung_xacminh_b_c_donhang_xinghiep')
    def onchange_method_noidung_xacminh_b_c_donhang_xinghiep(self):
        if self.noidung_xacminh_b_c_donhang_xinghiep == True:
            self.noidung_xacminh_b_donhang_xinghiep = True
        elif self.noidung_xacminh_b_a_donhang_xinghiep == False and self.noidung_xacminh_b_b_donhang_xinghiep == False and self.noidung_xacminh_b_c_donhang_xinghiep == False:
            self.noidung_xacminh_b_donhang_xinghiep = False

    ghichu_congviec_kinang_lienquan = fields.Text(string='Ghi chú công việc liên quan kỹ năng')  # bs_0050
    ghichu_daotao_kithuat_nganhnghe = fields.Text(
        string='Ghi chú đào tạo thực tập kỹ thuật liên quan nhiều ngành nghề')  # bs_0051

    # --------------------Học vấn-----------------------------------------------------------------------------------
    batdau_hoctap_nam_1 = fields.Char(string="Năm")  # hhjp_0342
    batdau_hoctap_thang_1 = fields.Selection(string="Tháng",
                                             selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
                                                        ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'), ('10', '10'),
                                                        ('11', '11'), ('12', '12')])  # hhjp_0342_v
    ketthuc_hoctap_nam_1 = fields.Char(string="Năm")  # hhjp_0343
    ketthuc_hoctap_thang_1 = fields.Selection(string="Tháng",
                                              selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
                                                         ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'), ('10', '10'),
                                                         ('11', '11'), ('12', '12')])  # hhjp_0343

    truong_tts_1 = fields.Char(string="Trường(Tiếng Nhật)")  # hhjp_0344
    truong_tts_v_1 = fields.Char(string="Trường(Tiếng Việt)")  # hhjp_0344

    batdau_hoctap_nam_2 = fields.Char(string="Năm")  # hhjp_0342
    batdau_hoctap_thang_2 = fields.Selection(string="Tháng",
                                             selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
                                                        ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'), ('10', '10'),
                                                        ('11', '11'), ('12', '12')])  # hhjp_0342_v
    ketthuc_hoctap_nam_2 = fields.Char(string="Năm")  # hhjp_0343
    ketthuc_hoctap_thang_2 = fields.Selection(string="Tháng",
                                              selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
                                                         ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'), ('10', '10'),
                                                         ('11', '11'), ('12', '12')])  # hhjp_0343

    truong_tts_2 = fields.Char(string="Trường(Tiếng Nhật)")  # hhjp_0344
    truong_tts_v_2 = fields.Char(string="Trường(Tiếng Việt)")  # hhjp_0344

    batdau_hoctap_nam_3 = fields.Char(string="Năm")  # hhjp_0342
    batdau_hoctap_thang_3 = fields.Selection(string="Tháng",
                                             selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
                                                        ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'), ('10', '10'),
                                                        ('11', '11'), ('12', '12')])  # hhjp_0342_v
    ketthuc_hoctap_nam_3 = fields.Char(string="Năm")  # hhjp_0343
    ketthuc_hoctap_thang_3 = fields.Selection(string="Tháng",
                                              selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
                                                         ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'), ('10', '10'),
                                                         ('11', '11'), ('12', '12')])  # hhjp_0343

    truong_tts_3 = fields.Char(string="Trường(Tiếng Nhật)")  # hhjp_0344
    truong_tts_v_3 = fields.Char(string="Trường(Tiếng Việt)")  # hhjp_0344

    batdau_hoctap_nam_4 = fields.Char(string="Năm")  # hhjp_0342
    batdau_hoctap_thang_4 = fields.Selection(string="Tháng",
                                             selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
                                                        ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'), ('10', '10'),
                                                        ('11', '11'), ('12', '12')])  # hhjp_0342_v
    ketthuc_hoctap_nam_4 = fields.Char(string="Năm")  # hhjp_0343
    ketthuc_hoctap_thang_4 = fields.Selection(string="Tháng",
                                              selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
                                                         ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'), ('10', '10'),
                                                         ('11', '11'), ('12', '12')])  # hhjp_0343

    truong_tts_4 = fields.Char(string="Trường(Tiếng Nhật)")  # hhjp_0344
    truong_tts_v_4 = fields.Char(string="Trường(Tiếng Việt)")  # hhjp_0344

    batdau_hoctap_nam_5 = fields.Char(string="Năm")  # hhjp_0342
    batdau_hoctap_thang_5 = fields.Selection(string="Tháng",
                                             selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
                                                        ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'), ('10', '10'),
                                                        ('11', '11'), ('12', '12')])  # hhjp_0342_v
    ketthuc_hoctap_nam_5 = fields.Char(string="Năm")  # hhjp_0343
    ketthuc_hoctap_thang_5 = fields.Selection(string="Tháng",
                                              selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
                                                         ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'), ('10', '10'),
                                                         ('11', '11'), ('12', '12')])  # hhjp_0343

    truong_tts_5 = fields.Char(string="Trường(Tiếng Nhật)")  # hhjp_0344
    truong_tts_v_5 = fields.Char(string="Trường(Tiếng Việt)")  # hhjp_0344
    # ------------------------------Công tác--------------------------------------------------------

    batdau_congtac_tts_nam_1 = fields.Char(string="Năm")  # hhjp_0347
    batdau_congtac_tts_thang_1 = fields.Selection(string="Tháng",
                                                  selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
                                                             ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                             ('10', '10'),
                                                             ('11', '11'), ('12', '12')])  # hhjp_0347_v

    ketthuc_congtac_tts_nam_1 = fields.Char(string="Năm")  # hhjp_0348
    ketthuc_congtac_tts_thang_1 = fields.Selection(string="Tháng",
                                                   selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                                                              ('5', '5'),
                                                              ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                              ('10', '10'),
                                                              ('11', '11'), ('12', '12')])  # hhjp_0348_v
    den_nay_1 = fields.Boolean(string="Hiện nay")

    @api.onchange('den_nay_1')
    def onchange_method_den_nay_1(self):
        if self.den_nay_1 == False:
            self.ketthuc_congtac_tts_nam_1 = ''
            self.ketthuc_congtac_tts_thang_1 = ''

            self.donvi_congtac_tts_1 = ''
            self.donvi_congtac_tts_v_1 = ''

        if self.den_nay_1 == True:
            self.donvi_congtac_tts_1 = self.donhang_tts.congtypc2.name
            self.donvi_congtac_tts_v_1 = self.donhang_tts.congtypc2.name_vn

    donvi_congtac_tts_1 = fields.Char(string="Công ty(Tiếng Nhật)")  # hhjp_0349
    donvi_congtac_tts_v_1 = fields.Char(string="Công ty(Tiếng Việt)")  # hhjp_0349_v
    congviec_congtac_tts_1 = fields.Many2one(comodel_name="loaicongviec.loaicongviec", string="Công việc(Tiếng Nhật)")
    congviec_congtac_tts_v_1 = fields.Char(string="Công việc(Tiếng Việt)")  # hhjp_0350_v
    congviec_lienquan_1 = fields.Boolean(string="Công việc liên quan đến kỹ năng định học")
    thoigian_lamviec_nam_1 = fields.Float()

    @api.onchange('congviec_congtac_tts_1')
    def onchange_method_congviec_congtac_tts_1(self):
        if self.congviec_congtac_tts_1:
            self.congviec_congtac_tts_v_1 = self.congviec_congtac_tts_1.name_loaicongviec_viet
        else:
            pass

    @api.onchange('congviec_lienquan_1')
    def onchange_method_congviec_lienquan_1(self):
        if self.congviec_lienquan_1 == False:
            self.thoigian_lamviec_nam_1 = ''
            self.congviec_congtac_tts_1 = ''
            self.congviec_congtac_tts_v_1 = ''
        else:
            self.congviec_congtac_tts_1 = self.nganhnghe_tts
            self.congviec_congtac_tts_v_1 = self.nganhnghe_tts_v

    # ---------------------------------------------------------
    batdau_congtac_tts_nam_2 = fields.Char(string="Năm")  # hhjp_0347
    batdau_congtac_tts_thang_2 = fields.Selection(string="Tháng",
                                                  selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
                                                             ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                             ('10', '10'),
                                                             ('11', '11'), ('12', '12')])  # hhjp_0347_v
    ketthuc_congtac_tts_nam_2 = fields.Char(string="Năm")  # hhjp_0348
    ketthuc_congtac_tts_thang_2 = fields.Selection(string="Tháng",
                                                   selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                                                              ('5', '5'),
                                                              ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                              ('10', '10'),
                                                              ('11', '11'), ('12', '12')])  # hhjp_0348_v
    den_nay_2 = fields.Boolean(string="Hiện nay")

    @api.onchange('den_nay_2')
    def onchange_method_den_nay_2(self):
        if self.den_nay_2 == False:
            self.ketthuc_congtac_tts_nam_2 = ''
            self.ketthuc_congtac_tts_thang_2 = ''

            self.donvi_congtac_tts_2 = ''
            self.donvi_congtac_tts_v_2 = ''

        if self.den_nay_2 == True:
            self.donvi_congtac_tts_2 = self.donhang_tts.congtypc2.name
            self.donvi_congtac_tts_v_2 = self.donhang_tts.congtypc2.name_vn

    donvi_congtac_tts_2 = fields.Char(string="Công ty(Tiếng Nhật)")  # hhjp_0349
    donvi_congtac_tts_v_2 = fields.Char(string="Công ty(Tiếng Việt)")  # hhjp_0349_v
    congviec_congtac_tts_2 = fields.Many2one(comodel_name="loaicongviec.loaicongviec",
                                             string="Công việc(Tiếng Nhật)")
    congviec_congtac_tts_v_2 = fields.Char(string="Công việc(Tiếng Việt)")  # hhjp_0350_v
    congviec_lienquan_2 = fields.Boolean(string="Công việc liên quan đến kỹ năng định học")
    thoigian_lamviec_nam_2 = fields.Float()

    @api.onchange('congviec_congtac_tts_2')
    def onchange_method_congviec_congtac_tts_2(self):
        if self.congviec_congtac_tts_2:
            self.congviec_congtac_tts_v_2 = self.congviec_congtac_tts_2.name_loaicongviec_viet
        else:
            pass

    @api.onchange('congviec_lienquan_2')
    def onchange_method_congviec_lienquan_2(self):
        if self.congviec_lienquan_2 == False:
            self.thoigian_lamviec_nam_2 =''
            self.congviec_congtac_tts_2 = ''
            self.congviec_congtac_tts_v_2 = ''
        else:
            self.congviec_congtac_tts_2 = self.nganhnghe_tts
            self.congviec_congtac_tts_v_2 = self.nganhnghe_tts_v

    # ---------------------------------------------------------------------
    batdau_congtac_tts_nam_3 = fields.Char(string="Năm")  # hhjp_0347
    batdau_congtac_tts_thang_3 = fields.Selection(string="Tháng",
                                                  selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
                                                             ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                             ('10', '10'),
                                                             ('11', '11'), ('12', '12')])  # hhjp_0347_v
    ketthuc_congtac_tts_nam_3 = fields.Char(string="Năm")  # hhjp_0348
    ketthuc_congtac_tts_thang_3 = fields.Selection(string="Tháng",
                                                   selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                                                              ('5', '5'),
                                                              ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                              ('10', '10'),
                                                              ('11', '11'), ('12', '12')])  # hhjp_0348_v
    den_nay_3 = fields.Boolean(string="Hiện nay")

    @api.onchange('den_nay_3')
    def onchange_method_den_nay_3(self):
        if self.den_nay_3 == False:
            self.ketthuc_congtac_tts_nam_3 = ''
            self.ketthuc_congtac_tts_thang_3 = ''

            self.donvi_congtac_tts_3 = ''
            self.donvi_congtac_tts_v_3 = ''

        if self.den_nay_3 == True:
            self.donvi_congtac_tts_3 = self.donhang_tts.congtypc2.name
            self.donvi_congtac_tts_v_3 = self.donhang_tts.congtypc2.name_vn

    donvi_congtac_tts_3 = fields.Char(string="Công ty(Tiếng Nhật)")  # hhjp_0349
    donvi_congtac_tts_v_3 = fields.Char(string="Công ty(Tiếng Việt)")  # hhjp_0349_v
    congviec_congtac_tts_3 = fields.Many2one(comodel_name="loaicongviec.loaicongviec",
                                             string="Công việc(Tiếng Nhật)")
    congviec_congtac_tts_v_3 = fields.Char(string="Công việc(Tiếng Việt)")  # hhjp_0350_v
    congviec_lienquan_3 = fields.Boolean(string="Công việc liên quan đến kỹ năng định học")
    thoigian_lamviec_nam_3 = fields.Float()

    @api.onchange('congviec_congtac_tts_3')
    def onchange_method_congviec_congtac_tts_3(self):
        if self.congviec_congtac_tts_3:
            self.congviec_congtac_tts_v_3 = self.congviec_congtac_tts_3.name_loaicongviec_viet
        else:
            pass

    @api.onchange('congviec_lienquan_3')
    def onchange_method_congviec_lienquan_3(self):
        if self.congviec_lienquan_3 == False:
            self.thoigian_lamviec_nam_3 = ''
            self.congviec_congtac_tts_3 = ''
            self.congviec_congtac_tts_v_3 = ''
        else:
            self.congviec_congtac_tts_3 = self.nganhnghe_tts
            self.congviec_congtac_tts_v_3 = self.nganhnghe_tts_v

    # ------------------------------------------------------------------------
    batdau_congtac_tts_nam_4 = fields.Char(string="Năm")  # hhjp_0347
    batdau_congtac_tts_thang_4 = fields.Selection(string="Tháng",
                                                  selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
                                                             ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                             ('10', '10'),
                                                             ('11', '11'), ('12', '12')])  # hhjp_0347_v
    ketthuc_congtac_tts_nam_4 = fields.Char(string="Năm")  # hhjp_0348
    ketthuc_congtac_tts_thang_4 = fields.Selection(string="Tháng",
                                                   selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                                                              ('5', '5'),
                                                              ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                              ('10', '10'),
                                                              ('11', '11'), ('12', '12')])  # hhjp_0348_v
    den_nay_4 = fields.Boolean(string="Hiện nay")

    @api.onchange('den_nay_4')
    def onchange_method_den_nay_4(self):
        if self.den_nay_4 == False:
            self.ketthuc_congtac_tts_nam_4 = ''
            self.ketthuc_congtac_tts_thang_4 = ''

            self.donvi_congtac_tts_4 = ''
            self.donvi_congtac_tts_v_4 = ''

        if self.den_nay_4 == True:
            self.donvi_congtac_tts_4 = self.donhang_tts.congtypc2.name
            self.donvi_congtac_tts_v_4 = self.donhang_tts.congtypc2.name_vn

    donvi_congtac_tts_4 = fields.Char(string="Công ty(Tiếng Nhật)")  # hhjp_0349
    donvi_congtac_tts_v_4 = fields.Char(string="Công ty(Tiếng Việt)")  # hhjp_0349_v
    congviec_congtac_tts_4 = fields.Many2one(comodel_name="loaicongviec.loaicongviec",
                                             string="Công việc(Tiếng Nhật)")
    congviec_congtac_tts_v_4 = fields.Char(string="Công việc(Tiếng Việt)")  # hhjp_0350_v
    congviec_lienquan_4 = fields.Boolean(string="Công việc liên quan đến kỹ năng định học")
    thoigian_lamviec_nam_4 = fields.Float()

    @api.onchange('congviec_congtac_tts_4')
    def onchange_method_congviec_congtac_tts_4(self):
        if self.congviec_congtac_tts_4:
            self.congviec_congtac_tts_v_4 = self.congviec_congtac_tts_4.name_loaicongviec_viet
        else:
            pass

    @api.onchange('congviec_lienquan_4')
    def onchange_method_congviec_lienquan_4(self):
        if self.congviec_lienquan_4 == False:
            self.thoigian_lamviec_nam_4 = ''
            self.congviec_congtac_tts_4 = ''
            self.congviec_congtac_tts_v_4 = ''
        else:
            self.congviec_congtac_tts_4 = self.nganhnghe_tts
            self.congviec_congtac_tts_v_4 = self.nganhnghe_tts_v

    # -----------------------------------------------------------------------
    batdau_congtac_tts_nam_5 = fields.Char(string="Năm")  # hhjp_0347
    batdau_congtac_tts_thang_5 = fields.Selection(string="Tháng",
                                                  selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
                                                             ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                             ('10', '10'),
                                                             ('11', '11'), ('12', '12')])  # hhjp_0347_v
    ketthuc_congtac_tts_nam_5 = fields.Char(string="Năm")  # hhjp_0348
    ketthuc_congtac_tts_thang_5 = fields.Selection(string="Tháng",
                                                   selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                                                              ('5', '5'),
                                                              ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                              ('10', '10'),
                                                              ('11', '11'), ('12', '12')])  # hhjp_0348_v
    den_nay_5 = fields.Boolean(string="Hiện nay")

    @api.onchange('den_nay_5')
    def onchange_method_den_nay_5(self):
        if self.den_nay_5 == False:
            self.ketthuc_congtac_tts_nam_5 = ''
            self.ketthuc_congtac_tts_thang_5 = ''

            self.donvi_congtac_tts_5 = ''
            self.donvi_congtac_tts_v_5 = ''

        if self.den_nay_5 == True:
            self.donvi_congtac_tts_5 = self.donhang_tts.congtypc2.name
            self.donvi_congtac_tts_v_5 = self.donhang_tts.congtypc2.name_vn

    donvi_congtac_tts_5 = fields.Char(string="Công ty(Tiếng Nhật)")  # hhjp_0349
    donvi_congtac_tts_v_5 = fields.Char(string="Công ty(Tiếng Việt)")  # hhjp_0349_v
    congviec_congtac_tts_5 = fields.Many2one(comodel_name="loaicongviec.loaicongviec",
                                             string="Công việc(Tiếng Nhật)")
    congviec_congtac_tts_v_5 = fields.Char(string="Công việc(Tiếng Việt)")  # hhjp_0350_v
    congviec_lienquan_5 = fields.Boolean(string="Công việc liên quan đến kỹ năng định học")
    thoigian_lamviec_nam_5 = fields.Float()

    #------------------------------------------------------------------------------------
    batdau_congtac_tts_nam_6 = fields.Char(string="Năm")  # hhjp_0347
    batdau_congtac_tts_thang_6 = fields.Selection(string="Tháng",
                                                  selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
                                                             ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                             ('10', '10'),
                                                             ('11', '11'), ('12', '12')])  # hhjp_0347_v
    ketthuc_congtac_tts_nam_6 = fields.Char(string="Năm")  # hhjp_0348
    ketthuc_congtac_tts_thang_6 = fields.Selection(string="Tháng",
                                                   selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                                                              ('5', '5'),
                                                              ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                              ('10', '10'),
                                                              ('11', '11'), ('12', '12')])  # hhjp_0348_v
    den_nay_6 = fields.Boolean(string="Hiện nay")

    @api.onchange('den_nay_6')
    def onchange_method_den_nay_6(self):
        if self.den_nay_6 == False:
            self.ketthuc_congtac_tts_nam_6 = ''
            self.ketthuc_congtac_tts_thang_6 = ''

            self.donvi_congtac_tts_6 = ''
            self.donvi_congtac_tts_v_6 = ''

        if self.den_nay_6 == True:
            self.donvi_congtac_tts_6 = self.donhang_tts.congtypc2.name
            self.donvi_congtac_tts_v_6 = self.donhang_tts.congtypc2.name_vn

    donvi_congtac_tts_6 = fields.Char(string="Công ty(Tiếng Nhật)")  # hhjp_0349
    donvi_congtac_tts_v_6 = fields.Char(string="Công ty(Tiếng Việt)")  # hhjp_0349_v
    congviec_congtac_tts_6 = fields.Many2one(comodel_name="loaicongviec.loaicongviec",
                                             string="Công việc(Tiếng Nhật)")
    congviec_congtac_tts_v_6 = fields.Char(string="Công việc(Tiếng Việt)")  # hhjp_0350_v
    congviec_lienquan_6 = fields.Boolean(string="Công việc liên quan đến kỹ năng định học")
    thoigian_lamviec_nam_6 = fields.Float()
    #------------------------------------------------------------------------------------
    batdau_congtac_tts_nam_7 = fields.Char(string="Năm")  # hhjp_0347
    batdau_congtac_tts_thang_7 = fields.Selection(string="Tháng",
                                                  selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
                                                             ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                             ('10', '10'),
                                                             ('11', '11'), ('12', '12')])  # hhjp_0347_v
    ketthuc_congtac_tts_nam_7 = fields.Char(string="Năm")  # hhjp_0348
    ketthuc_congtac_tts_thang_7 = fields.Selection(string="Tháng",
                                                   selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                                                              ('5', '5'),
                                                              ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                              ('10', '10'),
                                                              ('11', '11'), ('12', '12')])  # hhjp_0348_v
    den_nay_7 = fields.Boolean(string="Hiện nay")

    @api.onchange('den_nay_7')
    def onchange_method_den_nay_7(self):
        if self.den_nay_7 == False:
            self.ketthuc_congtac_tts_nam_7 = ''
            self.ketthuc_congtac_tts_thang_7 = ''

            self.donvi_congtac_tts_7 = ''
            self.donvi_congtac_tts_v_7 = ''

        if self.den_nay_7 == True:
            self.donvi_congtac_tts_7 = self.donhang_tts.congtypc2.name
            self.donvi_congtac_tts_v_7 = self.donhang_tts.congtypc2.name_vn

    donvi_congtac_tts_7 = fields.Char(string="Công ty(Tiếng Nhật)")  # hhjp_0349
    donvi_congtac_tts_v_7 = fields.Char(string="Công ty(Tiếng Việt)")  # hhjp_0349_v
    congviec_congtac_tts_7 = fields.Many2one(comodel_name="loaicongviec.loaicongviec",
                                             string="Công việc(Tiếng Nhật)")
    congviec_congtac_tts_v_7 = fields.Char(string="Công việc(Tiếng Việt)")  # hhjp_0350_v
    congviec_lienquan_7 = fields.Boolean(string="Công việc liên quan đến kỹ năng định học")
    thoigian_lamviec_nam_7 = fields.Float()

    fill = fields.Float("fill", digits=(42, 1))
    # compute = 'onchange_FILL'
    # @api.multi
    # @api.depends('congviec_lienquan_1', 'congviec_lienquan_2', 'congviec_lienquan_3', 'congviec_lienquan_4',
    #              'congviec_lienquan_5','congviec_lienquan_6','congviec_lienquan_7')
    # def onchange_FILL(self):
    #     if self.congviec_lienquan_1 or self.congviec_lienquan_2 or self.congviec_lienquan_3 or self.congviec_lienquan_4 or self.congviec_lienquan_5 or self.congviec_lienquan_6 or self.congviec_lienquan_7:
    #         tong = self.thoigian_lamviec_nam_1 + self.thoigian_lamviec_nam_2 + self.thoigian_lamviec_nam_3 + self.thoigian_lamviec_nam_4 + self.thoigian_lamviec_nam_5 + self.thoigian_lamviec_nam_6 + self.thoigian_lamviec_nam_7
    #         self.fill = round((tong / 12), 1)
    #         print(self.thoigian_lamviec_nam_1)
    #         print(self.thoigian_lamviec_nam_2)
    #         print(self.thoigian_lamviec_nam_3)
    #         print(self.thoigian_lamviec_nam_4)
    #         print(self.thoigian_lamviec_nam_5)
    #         print(self.thoigian_lamviec_nam_6)
    #         print(self.thoigian_lamviec_nam_7)
    #         print(self.fill)
    #     else:
    #         self.fill = 0

    @api.onchange('congviec_congtac_tts_5')
    def onchange_method_congviec_congtac_tts_5(self):
        if self.congviec_congtac_tts_5:
            self.congviec_congtac_tts_v_5 = self.congviec_congtac_tts_5.name_loaicongviec_viet
        else:
            pass

    @api.onchange('congviec_lienquan_5')
    def onchange_method_congviec_lienquan_5(self):
        if self.congviec_lienquan_5 == False:
            self.thoigian_lamviec_nam_5 = ''
            self.congviec_congtac_tts_5 = ''
            self.congviec_congtac_tts_v_5 = ''
        else:
            self.congviec_congtac_tts_5 = self.nganhnghe_tts
            self.congviec_congtac_tts_v_5 = self.nganhnghe_tts_v

    @api.onchange('congviec_congtac_tts_6')
    def onchange_method_congviec_congtac_tts_6(self):
        if self.congviec_congtac_tts_6:
            self.congviec_congtac_tts_v_6 = self.congviec_congtac_tts_6.name_loaicongviec_viet
        else:
            pass

    @api.onchange('congviec_lienquan_6')
    def onchange_method_congviec_lienquan_6(self):
        if self.congviec_lienquan_6 == False:
            self.thoigian_lamviec_nam_6 = ''
            self.congviec_congtac_tts_6 = ''
            self.congviec_congtac_tts_v_6 = ''
        else:
            self.congviec_congtac_tts_6 = self.nganhnghe_tts
            self.congviec_congtac_tts_v_6 = self.nganhnghe_tts_v

    @api.onchange('congviec_congtac_tts_7')
    def onchange_method_congviec_congtac_tts_7(self):
        if self.congviec_congtac_tts_7:
            self.congviec_congtac_tts_v_7 = self.congviec_congtac_tts_7.name_loaicongviec_viet
        else:
            pass

    @api.onchange('congviec_lienquan_7')
    def onchange_method_congviec_lienquan_7(self):
        if self.congviec_lienquan_7 == False:
            self.thoigian_lamviec_nam_7 = ''
            self.congviec_congtac_tts_7 = ''
            self.congviec_congtac_tts_v_7 = ''
        else:
            self.congviec_congtac_tts_7 = self.nganhnghe_tts
            self.congviec_congtac_tts_v_7 = self.nganhnghe_tts_v
    # Codinggggg
    # @api.multi
    # @api.onchange('congviec_lienquan_1', 'congviec_lienquan_2', 'congviec_lienquan_3', 'congviec_lienquan_4',
    #               'congviec_lienquan_5','congviec_lienquan_6','congviec_lienquan_7')
    # def onchange_method_congviec_lienquan(self):
    #     if self.congviec_lienquan_1 == True and self.den_nay_1 == False:
    #         print('Đặng 1.1')
    #         a = int(self.ketthuc_congtac_tts_nam_1) - int(self.batdau_congtac_tts_nam_1)
    #         b = (a * 12 + int(self.ketthuc_congtac_tts_thang_1) - int(self.batdau_congtac_tts_thang_1) + 1)
    #         self.thoigian_lamviec_nam_1 = b
    #     if self.congviec_lienquan_2 == True and self.den_nay_2 == False:
    #         print('Đặng 2.2')
    #         a = int(self.ketthuc_congtac_tts_nam_2) - int(self.batdau_congtac_tts_nam_2)
    #         b = (a * 12 + int(self.ketthuc_congtac_tts_thang_2) - int(self.batdau_congtac_tts_thang_2) + 1)
    #         self.thoigian_lamviec_nam_2 = b
    #     if self.congviec_lienquan_3 == True and self.den_nay_3 == False:
    #         print('Đặng 3.3')
    #         a = int(self.ketthuc_congtac_tts_nam_3) - int(self.batdau_congtac_tts_nam_3)
    #         b = (a * 12 + int(self.ketthuc_congtac_tts_thang_3) - int(self.batdau_congtac_tts_thang_3) + 1)
    #         self.thoigian_lamviec_nam_3 = b
    #     if self.congviec_lienquan_4 == True and self.den_nay_4 == False:
    #         print('Đặng 4.4')
    #         a = int(self.ketthuc_congtac_tts_nam_4) - int(self.batdau_congtac_tts_nam_4)
    #         b = (a * 12 + int(self.ketthuc_congtac_tts_thang_4) - int(self.batdau_congtac_tts_thang_4) + 1)
    #         self.thoigian_lamviec_nam_4 = b
    #     if self.congviec_lienquan_5 == True and self.den_nay_5 == False:
    #         print('Đặng 5.5')
    #         a = int(self.ketthuc_congtac_tts_nam_5) - int(self.batdau_congtac_tts_nam_5)
    #         b = (a * 12 + int(self.ketthuc_congtac_tts_thang_5) - int(self.batdau_congtac_tts_thang_5) + 1)
    #         self.thoigian_lamviec_nam_5 = b
    #     if self.congviec_lienquan_6 == True and self.den_nay_6 == False:
    #         print('Đặng 6.6')
    #         a = int(self.ketthuc_congtac_tts_nam_6) - int(self.batdau_congtac_tts_nam_6)
    #         b = (a * 12 + int(self.ketthuc_congtac_tts_thang_6) - int(self.batdau_congtac_tts_thang_6) + 1)
    #         self.thoigian_lamviec_nam_6 = b
    #     if self.congviec_lienquan_7 == True and self.den_nay_7 == False:
    #         print('Đặng 7.7')
    #         a = int(self.ketthuc_congtac_tts_nam_7) - int(self.batdau_congtac_tts_nam_7)
    #         b = (a * 12 + int(self.ketthuc_congtac_tts_thang_7) - int(self.batdau_congtac_tts_thang_7) + 1)
    #         self.thoigian_lamviec_nam_7 = b
    #     if self.donhang_tts.date_create_letter_promotion:
    #         print('Đặng nguyễn')
    #         tinh_gio = self.donhang_tts.date_create_letter_promotion
    #         cuoi_thang = last_day_of_month(tinh_gio)
    #         print(cuoi_thang)
    #         if self.congviec_lienquan_1 == True and self.den_nay_1 == True:
    #             print('Đặng 1')
    #             if cuoi_thang == tinh_gio:
    #                 print('Đặng 1.1')
    #                 a = (tinh_gio.year) - int(self.batdau_congtac_tts_nam_1)
    #                 b = (a * 12 + tinh_gio.month - int(self.batdau_congtac_tts_thang_1) + 1)
    #                 self.thoigian_lamviec_nam_1 = b
    #             else:
    #                 print('1.1.1')
    #                 a = (tinh_gio.year) - int(self.batdau_congtac_tts_nam_1)
    #                 b = (a * 12 + tinh_gio.month - int(self.batdau_congtac_tts_thang_1))
    #                 self.thoigian_lamviec_nam_1 = b
    #         if self.congviec_lienquan_2 == True and self.den_nay_2 == True:
    #             print('Đặng 2')
    #             if cuoi_thang == tinh_gio :
    #                 a = (tinh_gio.year) - int(self.batdau_congtac_tts_nam_2)
    #                 b = (a * 12 + tinh_gio.month - int(self.batdau_congtac_tts_thang_2) + 1)
    #                 self.thoigian_lamviec_nam_2 = b
    #             else:
    #                 a = (tinh_gio.year) - int(self.batdau_congtac_tts_nam_2)
    #                 b = (a * 12 + tinh_gio.month - int(self.batdau_congtac_tts_thang_2))
    #                 self.thoigian_lamviec_nam_2 = b
    #         if self.congviec_lienquan_3 == True and self.den_nay_3 == True:
    #             print('Đặng 3')
    #             if cuoi_thang == tinh_gio:
    #                 a = (tinh_gio.year) - int(self.batdau_congtac_tts_nam_3)
    #                 b = (a * 12 + tinh_gio.month - int(self.batdau_congtac_tts_thang_3) + 1)
    #                 self.thoigian_lamviec_nam_3 = b
    #             else:
    #                 a = (tinh_gio.year) - int(self.batdau_congtac_tts_nam_3)
    #                 b = (a * 12 + tinh_gio.month - int(self.batdau_congtac_tts_thang_3))
    #                 self.thoigian_lamviec_nam_3 = b
    #         if self.congviec_lienquan_4 == True and self.den_nay_4 == True:
    #             print('Đặng 4')
    #             if cuoi_thang== tinh_gio :
    #                 a = (tinh_gio.year) - int(self.batdau_congtac_tts_nam_4)
    #                 b = (a * 12 + tinh_gio.month - int(self.batdau_congtac_tts_thang_4) + 1)
    #                 self.thoigian_lamviec_nam_4 = b
    #             else:
    #                 a = (tinh_gio.year) - int(self.batdau_congtac_tts_nam_4)
    #                 b = (a * 12 + tinh_gio.month - int(self.batdau_congtac_tts_thang_4))
    #                 self.thoigian_lamviec_nam_4 = b
    #         if self.congviec_lienquan_5 == True and self.den_nay_5 == True:
    #             print('Đặng 5')
    #             if cuoi_thang == tinh_gio:
    #                 a = (tinh_gio.year) - int(self.batdau_congtac_tts_nam_5)
    #                 b = (a * 12 + tinh_gio.month - int(self.batdau_congtac_tts_thang_5) + 1)
    #                 self.thoigian_lamviec_nam_5 = b
    #             else:
    #                 a = (tinh_gio.year) - int(self.batdau_congtac_tts_nam_5)
    #                 b = (a * 12 + tinh_gio.month - int(self.batdau_congtac_tts_thang_5))
    #                 self.thoigian_lamviec_nam_5 = b
    #         if self.congviec_lienquan_6 == True and self.den_nay_6 == True:
    #             print('Đặng 6')
    #             if cuoi_thang == tinh_gio:
    #                 a = (tinh_gio.year) - int(self.batdau_congtac_tts_nam_6)
    #                 b = (a * 12 + tinh_gio.month - int(self.batdau_congtac_tts_thang_6) + 1)
    #                 self.thoigian_lamviec_nam_6 = b
    #             else:
    #                 a = (tinh_gio.year) - int(self.batdau_congtac_tts_nam_6)
    #                 b = (a * 12 + tinh_gio.month - int(self.batdau_congtac_tts_thang_6))
    #                 self.thoigian_lamviec_nam_6 = b
    #         if self.congviec_lienquan_7 == True and self.den_nay_7 == True:
    #             print('Đặng 7')
    #             if cuoi_thang == tinh_gio:
    #                 a = (tinh_gio.year) - int(self.batdau_congtac_tts_nam_7)
    #                 b = (a * 12 + tinh_gio.month - int(self.batdau_congtac_tts_thang_7) + 1)
    #                 self.thoigian_lamviec_nam_7 = b
    #             else:
    #                 a = (tinh_gio.year) - int(self.batdau_congtac_tts_nam_7)
    #                 b = (a * 12 + tinh_gio.month - int(self.batdau_congtac_tts_thang_7))
    #                 self.thoigian_lamviec_nam_7 = b

    # Năng lực ngôn ngữ (ngoài tiếng mẹ đẻ)
    ngonngu_nhat_tts = fields.Char(string="Trình độ tiếng Nhật")  # hhjp_0355
    ngonngu_nhat_tts_v = fields.Char(string="Trình độ tiếng Nhật(Tiếng Việt)")  # hhjp_0355_v
    ngonngu_anh_tts = fields.Char(string="Trình độ tiếng Anh")  # hhjp_0357
    ngonngu_anh_tts_v = fields.Char(string="Trình độ tiếng Anh(Tiếng Việt)")  # hhjp_0357_v
    ngonngu_khac_tts = fields.Char(string="Ngôn ngữ khác")  # hhjp_0358
    ngonngu_khac_tts_v = fields.Char(string="Ngôn ngữ khác(Tiếng mẹ đẻ)")  # hhjp_0358_v
    trinhdo_khac_tts = fields.Char(string="Trình độ")  # hhjp_0359
    trinhdo_khac_tts_v = fields.Char(string="Trình độ(Tiếng Việt)")  # hhjp_0359

    # Từng đến Nhật
    # tungdennhat_tts = fields.Boolean(string="Từng đến nhật")  # hhjp_0360
    # Kinh nghiệm KKTN
    lichsunhapcu_tts = fields.Selection(string="Lịch sử nhập cư trong quá khứ",
                                        selection=[('Có', 'Có'), ('Không', 'Không'), ],
                                        default="Không")  # hhjp_0360
    tucach_luutru_tts = fields.Selection(string="Tư cách lưu trú",
                                         selection=[('Thực tập sinh kỹ năng', 'Thực tập sinh kỹ năng'),
                                                    ('Không phải thực tập sinh kỹ năng',
                                                     'Không phải thực tập sinh kỹ năng'), ])  # hhjp_0360

    solan_lichsunhapcu_tts = fields.Integer(string='Số lần')
    ngaydinhat_tts = fields.Date(string="Bắt đầu")  # hhjp_0361
    onhatden_tts = fields.Date(string='Kết thúc')  # hhjp_0362

    @api.onchange('lichsunhapcu_tts')
    def onchange_method_lichsunhapcu_tts(self):
        if self.lichsunhapcu_tts:
            self.solan_lichsunhapcu_tts = ''
            self.ngaydinhat_tts = ''
            self.onhatden_tts = ''
            self.batdau_kynang_tts = ''
            self.ketthuc_kynang_tts = ''
            self.batdau_xaydung_tts = ''
            self.ketthuc_xaydung_tts = ''
            self.batdau_ungvien_tts = ''
            self.ketthuc_ungvien_tts = ''

    kinhnghiem_tts = fields.Selection(string="Kinh nghiệm TTKN",
                                      selection=[('Có', 'Có'), ('Không', 'Không'), ],
                                      default="Không")  # hhjp_0363
    knttkn_tu_tts = fields.Date(string="Bắt đầu")  # hhjp_0364
    knttkn_den_tts = fields.Date(string="Kết thúc")  # hhjp_0365


    @api.onchange('kinhnghiem_tts')
    def _onchange_kinhnghiem_tts(self):
        for tts in self:
            if tts.kinhnghiem_tts == 'Có':
                pass
                # if tts.donhang_tts.giaidoan_phi == '1 go':
                #     self.knttkn_tu_tts = tts.batdau_thucte_namnhat
                #     self.knttkn_den_tts = tts.ketthuc_thucte_namhai
                #
                #     print(self.knttkn_tu_tts, 'Bắt đầu')
                #     print(self.knttkn_den_tts, 'Kết thúc')
                #
                # elif tts.donhang_tts.giaidoan_phi == '3 go':
                #
                #     tts.knttkn_tu_tts = tts.batdau_thucte_namba
                #     tts.knttkn_den_tts = tts.ketthuc_thucte_namba

            if tts.kinhnghiem_tts == 'Không':
                tts.knttkn_tu_tts = ''
                tts.knttkn_den_tts = ''
                tts.phanloai_kehoach_thuctap_tts_1 = False
                tts.phanloai_kehoach_thuctap_tts_2 = False
                tts.phanloai_kehoach_thuctap_tts_3 = False
                tts.phanloai_kehoach_thuctap_tts_4 = False
                tts.phanloai_kehoach_thuctap_tts_5 = False
                tts.phanloai_kehoach_thuctap_tts_6 = False

    phanloai_kehoach_thuctap_tts = fields.Many2one(comodel_name="thuctapsinh.kehoachdoatao",
                                                   string='Kế hoạch thực tập')  #
    phanloai_kehoach_thuctap_tts_1 = fields.Boolean('Thực tập kỹ năng tại công ty độc lập số 1')
    phanloai_kehoach_thuctap_tts_2 = fields.Boolean('Thực tập kỹ năng tại công ty độc lập số 2')
    phanloai_kehoach_thuctap_tts_3 = fields.Boolean('Thực tập kỹ năng tại công ty độc lập số 3')
    phanloai_kehoach_thuctap_tts_4 = fields.Boolean('Thực tập kỹ năng tại tổ chức quản lý số 1')
    phanloai_kehoach_thuctap_tts_5 = fields.Boolean('Thực tập kỹ năng tại tổ chức quản lý số 2')
    phanloai_kehoach_thuctap_tts_6 = fields.Boolean('Thực tập kỹ năng tại tổ chức quản lý số 3')

    loai_ttkn_tts = fields.Selection(string="Phân loại", selection=[('Có', 'Có'), ('Không', 'Không')],
                                     default='Không')  # hhjp_0366
    thongtinkhac_tts = fields.Text(string="Thông tin khác")  # hhjp_0373
    thongtinkhac_tts_v = fields.Text(string="Thông tin khác(Tiếng Việt)")  # hhjp_0373
    buocloaibo_tts = fields.Selection(string="Từng bị từ chối tư cách nhập cư",
                                      selection=[('Có', 'Có'), ('Không', 'Không'), ],
                                      default="Không")  # hhjp_0372 truot_luutru_tts
    lydo_buocloaibo_tts = fields.Text(string="Lý do")  #
    lydo_buocloaibo_tts_v = fields.Text(string="Lý do (Tiếng việt)")  #

    @api.onchange('buocloaibo_tts')
    def onchange_method_buocloaibo_tts(self):
        if self.buocloaibo_tts:
            self.lydo_buocloaibo_tts = ''
            self.lydo_buocloaibo_tts_v = ''

    # Khai báo nhập cư
    noisinh_tts = fields.Char(string="Nơi sinh")  # bs_0058
    quehuong_tts = fields.Char(string="Hộ khẩu thường trú")  # hhjp_0521
    passport_so_tts = fields.Char(string="Số")  # hhjp_0522
    passport_phathanh_tts = fields.Date(string="Ngày cấp Passport")  # hhjp_0523
    passport_type = fields.Selection([('0', 'Ngoại giao'), ('1', 'Công vụ'), ('2', 'Phổ thông'), ('3', 'Khác')],
                                     string='Loại passport', default='0')  # hh_392
    passport_hethan_tts = fields.Date(string="Ngày hết hạn")  # bs_0061
    passport_noicap = fields.Char(string="Nơi cấp")  # hhjp_0524
    passport_issuing_authority = fields.Char('Cơ quan cấp', default=u"出入国管理局")  # hh_395
    date_duration_previous_in_jp = fields.Char(u'Ngày và thời gian ở Nhật lần trước', default='NO')  # hh_397
    nghenghiep_tts = fields.Char(string="Nghề nghiệp")  #
    soban_tts = fields.Char(string="Thực tập sinh")  # bs_0059
    soban_xinghiep = fields.Char(string="Xí nghiệp")  #
    dienthoai_tts = fields.Char(string="Thực tập sinh")  #
    dienthoai_xinghiep = fields.Char(string="Xí nghiệp")  #
    toipham_tts = fields.Selection(string="Phạm tội hay chưa",
                                   selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')  #
    noidung_toipham = fields.Char(string="Nội dung cụ thể")  #

    @api.onchange('toipham_tts')
    def onchange_method_toipham_tts(self):
        if self.toipham_tts:
            self.noidung_toipham = ''

    nopdonxuatnhapcanh_tts = fields.Char(string="Văn phòng xuất nhập cảnh nơi nộp đơn")  #

    mucdichnhapcu_tts = fields.Selection(string="Mục đích nhập cư",
                                         selection=[('Thực tập kỹ thuật số 1', 'Thực tập kỹ thuật số 1'),
                                                    ('Đào tạo kỹ thuật số 2', 'Đào tạo kỹ thuật số 2'),
                                                    ('Đào tạo kỹ thuật số 3', ('Đào tạo kỹ thuật số 3'))],
                                         default="Thực tập kỹ thuật số 1")  # hhjp_0113
    hacanh_tts = fields.Char(string="Cảng dự kiến hạ cánh")  #
    luutru_tts = fields.Char(string="Thời gian lưu trú dự kiến")  #
    nguoidicung_tts = fields.Selection(string="Người đi cùng thực tập sinh",
                                       selection=[('Có', 'Có'), ('Không', 'Không'), ], default='Không')  #
    visa_tts = fields.Char(string="Nơi xin visa")  #

    # lichsuganday_tts = fields.Char(String="Lịch sử nhập cư gần đây")  #
    cokhong_tungbi_trucxuat_tts = fields.Selection(string="Từng bị trục xuất",
                                                   selection=[('Có', 'Có'), ('Không', 'Không'), ], default="Không")  #
    tungbi_trucxuat_tts = fields.Date(string='Ngày bị trục xuất')
    solan_tungbi_trucxuat_tts = fields.Char(string='Số lần bị trục xuất')

    @api.onchange('cokhong_tungbi_trucxuat_tts')
    def onchange_method_cokhong_tungbi_trucxuat_tts(self):
        if self.cokhong_tungbi_trucxuat_tts:
            self.tungbi_trucxuat_tts = ''
            self.solan_tungbi_trucxuat_tts = ''

    nguoithan_tts = fields.Selection(string="Người thân ở Nhật bản",
                                     selection=[('Có', 'Có'), ('Không', 'Không')], default="Không")  # hhjp_0527
    nguoithan_onhat_tts = fields.One2many(comodel_name="thuctapsinh.nguoithan", inverse_name="nguoithan_onhat_tts",
                                          string="Người thân ở Nhật Bản")  #

    @api.onchange('nguoithan_tts')
    def onchange_method_nguoithan_tts(self):
        if self.nguoithan_tts:
            self.nguoithan_onhat_tts = None

    ngay_quaylai_tts = fields.Date(string="Ngày quay lại")  # hhjp_0207
    khoang_thoigian_venuoc = fields.Char(string="Khoảng thời gian về nước", store=True,
                                         compute='_khoangthoigianvn')  # hhjp_0208
    khoang_thoigian_venuoc_nam = fields.Char(string="Năm")  # hhjp_0208_nam
    khoang_thoigian_venuoc_thang = fields.Selection([('01', '01'), ('02', '02'), ('03', '03'), ('04', '04'),
                                                     ('05', '05'), ('06', '06'), ('07', '07'), ('08', '08'),
                                                     ('09', '09'), ('10', '10'), ('11', '11'), ('12', '12'), ],
                                                    "Tháng")  # hhjp_0208_thang

    @api.one
    @api.depends('khoang_thoigian_venuoc_thang', 'khoang_thoigian_venuoc_nam')
    def _khoangthoigianvn(self):
        if self.khoang_thoigian_venuoc_thang and self.khoang_thoigian_venuoc_nam:
            self.khoang_thoigian_venuoc = u" %s 年 %s 月" % (
                self.khoang_thoigian_venuoc_nam, self.khoang_thoigian_venuoc_thang)
        elif self.khoang_thoigian_venuoc_nam:
            self.khoang_thoigian_venuoc = u'%s 月' % self.khoang_thoigian_venuoc_nam
        else:
            self.khoang_thoigian_venuoc = ""

    # Người chuẩn bị nộp đơn 1 (Gia hạn)
    sothe_luutru_giahan = fields.Char(string='Số thẻ/ Thời gian lưu trú')
    tinhtrang_cutru_giahan = fields.Char(string='Tư cách lưu trú')
    tinhtrang_luutru_giahan = fields.Char(string='Thời gian lưu trú')
    hethan_luutru_giahan = fields.Date(string='Ngày hết hạn của thời gian lưu trú')
    xuatnhapcanh_hoso_giahan = fields.Char(string='Cục xuất nhập cảnh nộp hồ sơ')
    thoigan_luutru_giahan = fields.Char(string='Thời gian lưu trú mong muốn')
    lydo_doimoi_giahan = fields.Char(string='Lý do gia hạn', default='技能等の習熟のため')

    # Người chuẩn bị nộp đơn 1 (Thay đổi)
    sothe_luutru_thaydoi = fields.Char(string='Số thẻ')
    tinhtrang_cutru_thaydoi_1 = fields.Char(string='Tư cách lưu trú')
    thoigian_luutru_thaydoi_1 = fields.Char(string='Thời gian lưu trú')
    hethan_luutru_thaydoi = fields.Date(string='Ngày hết hạn của thời gian lưu trú')
    xuatnhapcanh_hoso_thaydoi = fields.Char(string='Cục xuất nhập cảnh nộp hồ sơ')
    tinhtrang_cutru_thaydoi_2 = fields.Char(string='Tư cách lưu trú')
    thoigan_luutru_thaydoi_2 = fields.Char(string='Thời gian lưu trú')
    lydo_thaydoi_thaydoi = fields.Char(string='Lý do thay đổi', default='技能実習継続のため')
    # Chuẩn bị của ứng viên 2
    nam_vaocty_1 = fields.Char(size=4)
    thang_vaocty_1 = fields.Selection(string="Tháng",
                                                   selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                                                              ('5', '5'),
                                                              ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                              ('10', '10'),
                                                              ('11', '11'), ('12', '12')])
    nam_thoiviec_1 = fields.Char(size=4)
    thang_thoiviec_1 = fields.Selection(string="Tháng",
                                                   selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                                                              ('5', '5'),
                                                              ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                              ('10', '10'),
                                                              ('11', '11'), ('12', '12')])
    quatrinh_congtac_1 = fields.Char()
    nam_vaocty_2 = fields.Char(size=4)
    thang_vaocty_2 = fields.Selection(string="Tháng",
                                                   selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                                                              ('5', '5'),
                                                              ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                              ('10', '10'),
                                                              ('11', '11'), ('12', '12')])
    nam_thoiviec_2 = fields.Char(size=4)
    thang_thoiviec_2 = fields.Selection(string="Tháng",
                                                   selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                                                              ('5', '5'),
                                                              ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                              ('10', '10'),
                                                              ('11', '11'), ('12', '12')])
    quatrinh_congtac_2 = fields.Char()
    nam_vaocty_3 = fields.Char(size=4)
    thang_vaocty_3 = fields.Selection(string="Tháng",
                                                   selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                                                              ('5', '5'),
                                                              ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                              ('10', '10'),
                                                              ('11', '11'), ('12', '12')])
    nam_thoiviec_3 = fields.Char(size=4)
    thang_thoiviec_3 = fields.Selection(string="Tháng",
                                                   selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                                                              ('5', '5'),
                                                              ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                              ('10', '10'),
                                                              ('11', '11'), ('12', '12')])
    quatrinh_congtac_3 = fields.Char()
    nam_vaocty_4 = fields.Char(size=4)
    thang_vaocty_4 = fields.Selection(string="Tháng",
                                                   selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                                                              ('5', '5'),
                                                              ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                              ('10', '10'),
                                                              ('11', '11'), ('12', '12')])
    nam_thoiviec_4 = fields.Char(size=4)
    thang_thoiviec_4 = fields.Selection(string="Tháng",
                                                   selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                                                              ('5', '5'),
                                                              ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                              ('10', '10'),
                                                              ('11', '11'), ('12', '12')])
    quatrinh_congtac_4 = fields.Char()
    nam_vaocty_5 = fields.Char(size=4)
    thang_vaocty_5 = fields.Selection(string="Tháng",
                                                   selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                                                              ('5', '5'),
                                                              ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                              ('10', '10'),
                                                              ('11', '11'), ('12', '12')])
    nam_thoiviec_5 = fields.Char(size=4)
    thang_thoiviec_5 = fields.Selection(string="Tháng",
                                                   selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                                                              ('5', '5'),
                                                              ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                              ('10', '10'),
                                                              ('11', '11'), ('12', '12')])
    quatrinh_congtac_5 = fields.Char()
    nam_vaocty_6 = fields.Char(size=4)
    thang_vaocty_6 = fields.Selection(string="Tháng",
                                                   selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                                                              ('5', '5'),
                                                              ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                              ('10', '10'),
                                                              ('11', '11'), ('12', '12')])
    nam_thoiviec_6 = fields.Char(size=4)
    thang_thoiviec_6 = fields.Selection(string="Tháng",
                                                   selection=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                                                              ('5', '5'),
                                                              ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
                                                              ('10', '10'),
                                                              ('11', '11'), ('12', '12')])
    quatrinh_congtac_6 = fields.Char()
    # Người nộp đơn
    nguoinopdon_nghiepdoan = fields.Many2one(comodel_name='nghiepdoan.nhanviencapcao', string='Người nộp đơn')
    thongtin_nguoinopdon_nghiepdoan = fields.Boolean(string='Thông tin người nộp đơn')

    @api.onchange('thongtin_nguoinopdon_nghiepdoan')
    def onchange_method_thongtin_nguoinopdon_nghiepdoan(self):
        if self.thongtin_nguoinopdon_nghiepdoan == True:
            if self.nguoinopdon_nghiepdoan:
                self.hoten_nguoinopdon = self.nguoinopdon_nghiepdoan.ten_nvien_han_nghiepdoan
                self.diachi_nguoinopdon = self.nguoinopdon_nghiepdoan.diachi_nghiepdoan
                self.dienthoai_nguoinopdon = self.nguoinopdon_nghiepdoan.dienthoai_nghiepdoan
                self.quanhe_thuctapsinh_nguoinopdon = '監理団体の職員'
                self.khong_didong_nguoinopdon = True
        else:
            self.nguoinopdon_nghiepdoan = ''
            self.hoten_nguoinopdon = ''
            self.diachi_nguoinopdon = ''
            self.dienthoai_nguoinopdon = ''
            self.quanhe_thuctapsinh_nguoinopdon = ''
            self.khong_didong_nguoinopdon = True

    hoten_nguoinopdon = fields.Char(string='Họ và tên')
    quanhe_thuctapsinh_nguoinopdon = fields.Char(string='Quan hệ với thực tập sinh')
    diachi_nguoinopdon = fields.Char(string='Địa chỉ')
    dienthoai_nguoinopdon = fields.Char(string='Số điện thoại')
    didong_nguoinopdon = fields.Char(string='Di động')
    khong_didong_nguoinopdon = fields.Boolean(string='Không có số điện thoại')

    @api.onchange('khong_didong_nguoinopdon')
    def onchange_method_khong_didong_nguoinopdon(self):
        if self.khong_didong_nguoinopdon:
            self.didong_nguoinopdon = ''

    # Chứng nhận nghề tại mẫu quốc
    xinghiep_phaicu = fields.Char(string="Tên xí nghiệp phái cử")  # hhjp_0204
    daidien_chucvu_phaicu = fields.Char(string="Chức vụ")  # hhjp_0207_1
    daidien_phaicu = fields.Char(string="Đại diện công ty phái cử")  # hhjp_0207

    bophan_xinghiep_phaicu = fields.Char(string="Bộ phận")  # hhjp_0900
    nganhnghe_xinghiep_phaicu = fields.Char(string="Ngành nghề")  # hhjp_0901

    quatrinh_danden_thuctap_kynang = fields.Selection(string="Quá trình dẫn đến kĩ năng", selection=[
        ('Xí nghiệp phái cử tiến cử', 'Xí nghiệp phái cử tiến cử'),
        ('Nguyện vọng thực tập sinh', 'Nguyện vọng thực tập sinh'),
        ('Lý do khác', 'Lý do khác')], default='Xí nghiệp phái cử tiến cử')  # hhjp_0332
    quatrinh_danden_thuctap_kynang_khac_lydo = fields.Char(string="Lý do khác")  # bs_0010
    quatrinh_danden_thuctap_kynang_tiencu_lydo = fields.Char(string="Lý do tiến cử")  # bs_0011

    daingo_thuctap_xinghiep = fields.Selection(string="Đãi ngộ trong thời gian đi thực tập", selection=[
        ('Tiếp tục duy trì đãi ngộ với thực tập sinh', 'Tiếp tục duy trì đãi ngộ với thực tập sinh'),
        ('Nghỉ việc', 'Nghỉ việc'),
        ('Khác', 'Khác')], default='Tiếp tục duy trì đãi ngộ với thực tập sinh')  # hhjp_0333
    daingo_khac_thuctap_xinghiep = fields.Char(string="Đãi ngộ khác")  # bs_0012

    kehoach_ketthuc_thuctap_xinghiep = fields.Selection(string="Kế hoạch sau khi kết thúc thực tập kỹ năng", selection=[
        ('Quay lại xí nghiệp phái cử', 'Quay lại xí nghiệp phái cử'),
        ('Không quay lại xí nghiệp phái cử', 'Không quay lại xí nghiệp phái cử'),
        ('Chưa xác định', 'Chưa xác định')], default='Quay lại xí nghiệp phái cử')  # hhjp_0334
    kehoach_ketthuc_venuoc = fields.Text(string="Kế hoạch sau khi kết thúc thực tập sinh về nước")  # bs_0020
    kehoach_ketthuc_venuoc_v = fields.Text(string="Kế hoạch sau khi kết thúc thực tập sinh về nước(Việt)")  # bs_0020_v
