# -*- coding: utf-8 -*-

from odoo import models, fields, api


class NoiDay(models.Model):
    _name = 'noiday.noiday'
    _rec_name = 'ten_noiday'

    daotaosau_noiday = fields.Many2one(comodel_name='daotao.daotaosau')
    id = fields.Integer(string='STT')
    ten_noiday = fields.Char(string='Tên')
    diadiem_noiday = fields.Char(string='Địa điểm')
    dienthoai_noiday = fields.Char(string='Điện thoại')
