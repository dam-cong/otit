# -*- coding: utf-8 -*-

from odoo import models, fields, api
import math
from datetime import date


class ChiTiet(models.Model):
    _name = 'chitiet.chitiet'
    _rec_name = 'donhang_chitiet'
    _order = 'id desc'

    donhang_chitiet = fields.Many2one(comodel_name='donhang.donhang', string='Đơn hàng', required=True,
                                      ondelete='cascade')
    xinghiep_chitiet = fields.Many2one(comodel_name='xinghiep.xinghiep', string='Xí nghiệp',
                                       related='donhang_chitiet.xinghiep_donhang')
    chinnhanh_chitiet = fields.Many2one(comodel_name='xinghiep.chinhanh', string='① Chi nhánh',
                                        related='donhang_chitiet.chinhanh_xinghiep')
    chinnhanh_chitiet_2 = fields.Many2one(comodel_name='xinghiep.chinhanh', string='② Chi nhánh',
                                          related='donhang_chitiet.chinhanh_xinghiep_2')
    chinnhanh_chitiet_3 = fields.Many2one(comodel_name='xinghiep.chinhanh', string='③ Chi nhánh',
                                          related='donhang_chitiet.chinhanh_xinghiep_3')

    thoigian_nhapcanh = fields.Date(string="Thời gian nhập quốc", related='donhang_chitiet.thoigian_donhang',
                                    required=False, )

    giaidoan = fields.Many2one(comodel_name='giaidoan.giaidoan', string='Giai đoạn', required=True)

    # ma_congviec = fields.Char(string='Mã công việc', related='congviec.ma_congviec')
    # nganhnghe = fields.Many2one(comodel_name='nganhnghe.nganhnghe', string='Ngành nghề')
    #
    # loaicongviec = fields.Many2one(comodel_name='loaicongviec.loaicongviec', string='Loại công việc')
    #
    # congviec = fields.Many2one(comodel_name='congviec.congviec', string='Công việc')

    ma_congviec = fields.Char(string='Mã công việc', related='donhang_chitiet.ma_congviec_donhang')
    nganhnghe = fields.Many2one(comodel_name='nganhnghe.nganhnghe', string='Ngành nghề',
                                related='donhang_chitiet.nganhnghe_donhang')
    loaicongviec = fields.Many2one(comodel_name='loaicongviec.loaicongviec', related='donhang_chitiet.loainghe_donhang',
                                   string='Loại công việc')
    congviec = fields.Many2one(comodel_name='congviec.congviec', string='Công việc',
                               related='donhang_chitiet.congviec_donhang')

    nam_thoigian = fields.Float(string="Thời gian năm", required=True, default=0)

    noidung_congviec_mot = fields.Text(string='Nội dung')  # hhjp_0121
    coso_congviec_mot = fields.Text(string='Cơ sở thực hiện')
    tong_thoigian_mot = fields.Float(string='Tổng thời gian')
    thangmot_thoigian_mot = fields.Float(string='Tháng 1')
    thanghai_thoigian_mot = fields.Float(string='Tháng 2')
    thangba_thoigian_mot = fields.Float(string='Tháng 3')
    thangbon_thoigian_mot = fields.Float(string='Tháng 4')
    thangnam_thoigian_mot = fields.Float(string='Tháng 5')
    thangsau_thoigian_mot = fields.Float(string='Tháng 6')
    thangbay_thoigian_mot = fields.Float(string='Tháng 7')
    thangtam_thoigian_mot = fields.Float(string='Tháng 8')
    thangchin_thoigian_mot = fields.Float(string='Tháng 9')
    thangmuoi_thoigian_mot = fields.Float(string='Tháng 10')
    thangmuoimot_thoigian_mot = fields.Float(string='Tháng 11')
    thangmuoihai_thoigian_mot = fields.Float(string='Tháng 12')

    noidung_congviec_hai = fields.Text(string='Nội dung')  # hhjp_0122
    coso_congviec_hai = fields.Text(string='Cơ sở thực hiện')
    tong_thoigian_hai = fields.Float(string='Tổng thời gian')
    thangmot_thoigian_hai = fields.Float(string='Tháng 1')
    thanghai_thoigian_hai = fields.Float(string='Tháng 2')
    thangba_thoigian_hai = fields.Float(string='Tháng 3')
    thangbon_thoigian_hai = fields.Float(string='Tháng 4')
    thangnam_thoigian_hai = fields.Float(string='Tháng 5')
    thangsau_thoigian_hai = fields.Float(string='Tháng 6')
    thangbay_thoigian_hai = fields.Float(string='Tháng 7')
    thangtam_thoigian_hai = fields.Float(string='Tháng 8')
    thangchin_thoigian_hai = fields.Float(string='Tháng 9')
    thangmuoi_thoigian_hai = fields.Float(string='Tháng 10')
    thangmuoimot_thoigian_hai = fields.Float(string='Tháng 11')
    thangmuoihai_thoigian_hai = fields.Float(string='Tháng 12')

    noidung_congviec_ba = fields.Text(string='Nội dung')  # hhjp_0123
    coso_congviec_ba = fields.Text(string='Cơ sở thực hiện')
    tong_thoigian_ba = fields.Float(string='Tổng thời gian')
    thangmot_thoigian_ba = fields.Float(string='Tháng 1')
    thanghai_thoigian_ba = fields.Float(string='Tháng 2')
    thangba_thoigian_ba = fields.Float(string='Tháng 3')
    thangbon_thoigian_ba = fields.Float(string='Tháng 4')
    thangnam_thoigian_ba = fields.Float(string='Tháng 5')
    thangsau_thoigian_ba = fields.Float(string='Tháng 6')
    thangbay_thoigian_ba = fields.Float(string='Tháng 7')
    thangtam_thoigian_ba = fields.Float(string='Tháng 8')
    thangchin_thoigian_ba = fields.Float(string='Tháng 9')
    thangmuoi_thoigian_ba = fields.Float(string='Tháng 10')
    thangmuoimot_thoigian_ba = fields.Float(string='Tháng 11')
    thangmuoihai_thoigian_ba = fields.Float(string='Tháng 12')

    noidung_congviec_bon = fields.Text(string='Nội dung')  # hhjp_0124
    coso_congviec_bon = fields.Text(string='Cơ sở thực hiện')
    tong_thoigian_bon = fields.Float(string='Tổng thời gian')
    thangmot_thoigian_bon = fields.Float(string='Tháng 1')
    thanghai_thoigian_bon = fields.Float(string='Tháng 2')
    thangba_thoigian_bon = fields.Float(string='Tháng 3')
    thangbon_thoigian_bon = fields.Float(string='Tháng 4')
    thangnam_thoigian_bon = fields.Float(string='Tháng 5')
    thangsau_thoigian_bon = fields.Float(string='Tháng 6')
    thangbay_thoigian_bon = fields.Float(string='Tháng 7')
    thangtam_thoigian_bon = fields.Float(string='Tháng 8')
    thangchin_thoigian_bon = fields.Float(string='Tháng 9')
    thangmuoi_thoigian_bon = fields.Float(string='Tháng 10')
    thangmuoimot_thoigian_bon = fields.Float(string='Tháng 11')
    thangmuoihai_thoigian_bon = fields.Float(string='Tháng 12')

    noidung_congviec_nam = fields.Text(string='Nội dung')  # hhjp_0125
    coso_congviec_nam = fields.Text(string='Cơ sở thực hiện')
    tong_thoigian_nam = fields.Float(string='Tổng thời gian')
    thangmot_thoigian_nam = fields.Float(string='Tháng 1')
    thanghai_thoigian_nam = fields.Float(string='Tháng 2')
    thangba_thoigian_nam = fields.Float(string='Tháng 3')
    thangbon_thoigian_nam = fields.Float(string='Tháng 4')
    thangnam_thoigian_nam = fields.Float(string='Tháng 5')
    thangsau_thoigian_nam = fields.Float(string='Tháng 6')
    thangbay_thoigian_nam = fields.Float(string='Tháng 7')
    thangtam_thoigian_nam = fields.Float(string='Tháng 8')
    thangchin_thoigian_nam = fields.Float(string='Tháng 9')
    thangmuoi_thoigian_nam = fields.Float(string='Tháng 10')
    thangmuoimot_thoigian_nam = fields.Float(string='Tháng 11')
    thangmuoihai_thoigian_nam = fields.Float(string='Tháng 12')

    noidung_congviec_sau = fields.Text(string='Nội dung')  # hhjp_0126
    coso_congviec_sau = fields.Text(string='Cơ sở thực hiện')
    tong_thoigian_sau = fields.Float(string='Tổng thời gian')
    thangmot_thoigian_sau = fields.Float(string='Tháng 1')
    thanghai_thoigian_sau = fields.Float(string='Tháng 2')
    thangba_thoigian_sau = fields.Float(string='Tháng 3')
    thangbon_thoigian_sau = fields.Float(string='Tháng 4')
    thangnam_thoigian_sau = fields.Float(string='Tháng 5')
    thangsau_thoigian_sau = fields.Float(string='Tháng 6')
    thangbay_thoigian_sau = fields.Float(string='Tháng 7')
    thangtam_thoigian_sau = fields.Float(string='Tháng 8')
    thangchin_thoigian_sau = fields.Float(string='Tháng 9')
    thangmuoi_thoigian_sau = fields.Float(string='Tháng 10')
    thangmuoimot_thoigian_sau = fields.Float(string='Tháng 11')
    thangmuoihai_thoigian_sau = fields.Float(string='Tháng 12')

    tong_thoigian = fields.Float(string="Tổng")

    tong_thoigian_thangmot = fields.Float(string="Tháng 1")
    tong_thoigian_thanghai = fields.Float(string="Tháng 2")
    tong_thoigian_thangba = fields.Float(string="Tháng 3")
    tong_thoigian_thangbon = fields.Float(string="Tháng 4")
    tong_thoigian_thangnam = fields.Float(string="Tháng 5")
    tong_thoigian_thangsau = fields.Float(string="Tháng 6")
    tong_thoigian_thangbay = fields.Float(string="Tháng 7")
    tong_thoigian_thangtam = fields.Float(string="Tháng 8")
    tong_thoigian_thangchin = fields.Float(string="Tháng 9")
    tong_thoigian_thangmuoi = fields.Float(string="Tháng 10")
    tong_thoigian_thangmuoimot = fields.Float(string="Tháng 11")
    tong_thoigian_thangmuoihai = fields.Float(string="Tháng 12")

    nguyenluyen_chitiet = fields.Text(string='Nguyên liệu công việc')
    congcu_maymoc_chitiet = fields.Text(string='Công cụ máy móc')
    sanpham_chitiet = fields.Text(string='Sản phẩm')

    thoigian_batbuoc = fields.Float(string="Thời gian bắt buộc", compute='_kiemtra')
    thoigian_lienquan = fields.Float(string="Thời gian liên quan", compute='_kiemtra')
    thoigian_xungquanh = fields.Float(string="Thời gian xung quanh", compute='_kiemtra')
    thoigian_toanbo = fields.Float(string="Thời gian toàn bộ", compute='_kiemtra')

    antoan_batbuoc = fields.Float(string="An toàn bắt buộc", compute='_kiemtra')
    antoan_lienquan = fields.Float(string="An toàn liên quan", compute='_kiemtra')
    antoan_xungquanh = fields.Float(string="An toàn xưng quanh", compute='_kiemtra')

    # @api.onchange('nam_thoigian')
    # def onchange_method_nam_thoigian(self):
    #     if self.nam_thoigian:

    def nam_thoigian_button(self):

        # Cơ sở công việc
        if self.chinnhanh_chitiet and self.chinnhanh_chitiet_2 and self.chinnhanh_chitiet_3:
            coso_chinhanh = u'① ② ③'
        elif self.chinnhanh_chitiet and self.chinnhanh_chitiet_2:
            coso_chinhanh = u'① ②'
        elif self.chinnhanh_chitiet and self.chinnhanh_chitiet_3:
            coso_chinhanh = u'① ③'
        elif self.chinnhanh_chitiet_2 and self.chinnhanh_chitiet_3:
            coso_chinhanh = u'② ③'
        elif self.chinnhanh_chitiet:
            coso_chinhanh = u'①'
        elif self.chinnhanh_chitiet_2:
            coso_chinhanh = u'②'
        elif self.chinnhanh_chitiet_3:
            coso_chinhanh = u'③'
        else:
            coso_chinhanh = u''

        self.coso_congviec_mot = coso_chinhanh
        self.coso_congviec_hai = coso_chinhanh
        self.coso_congviec_ba = coso_chinhanh
        self.coso_congviec_bon = coso_chinhanh
        self.coso_congviec_nam = coso_chinhanh
        self.coso_congviec_sau = coso_chinhanh

        # Nội dung công việc
        if self.congviec and self.giaidoan:
            congviec = self.env['congviec.chitietcongviec'].search(
                [('name_chitiet_congviec', '=', self.congviec.id),
                 ('giaidoan_chitiet_congviec', '=', self.giaidoan.id)])

            self.noidung_congviec_mot = congviec.noidung_chitiet_congviec_mot
            self.noidung_congviec_hai = congviec.noidung_chitiet_congviec_hai
            self.noidung_congviec_ba = congviec.noidung_chitiet_congviec_ba
            self.noidung_congviec_bon = congviec.noidung_chitiet_congviec_bon
            self.noidung_congviec_nam = congviec.noidung_chitiet_congviec_nam
            self.noidung_congviec_sau = congviec.noidung_chitiet_congviec_sau

            self.nguyenluyen_chitiet = congviec.nguyenluyen_chitiet_congviec
            self.congcu_maymoc_chitiet = congviec.congcu_maymoc_chitiet_congviec
            self.sanpham_chitiet = congviec.sanpham_chitiet_congviec

        # Thời gian tháng
        if self.nam_thoigian:
            if self.giaidoan.ten_giaidoan == '１号':
                phanba = (self.nam_thoigian // 6)
                self.tong_thoigian_sau = math.ceil((phanba * 10) / 100.0)
                self.tong_thoigian_nam = phanba - self.tong_thoigian_sau

                phanhai = (self.nam_thoigian // 6) * 2
                self.tong_thoigian_bon = math.ceil((phanhai * 10) / 100.0)
                self.tong_thoigian_ba = phanhai - self.tong_thoigian_bon

                phanmot = (self.nam_thoigian - (phanhai + phanba))

                self.tong_thoigian_hai = math.ceil((phanmot * 10) / 100.0)
                self.tong_thoigian_mot = phanmot - self.tong_thoigian_hai

                du_mot = self.tong_thoigian_mot % 11
                if du_mot < 5.5:
                    them_mot = 0
                    themcuoi_mot = (du_mot / 2)
                elif du_mot >= 5.5:
                    them_mot = 0.5
                    themcuoi_mot = (du_mot - 0.5 * 9) / 2

                self.thangmot_thoigian_mot = int(0)
                self.thanghai_thoigian_mot = (self.tong_thoigian_mot // 11) + them_mot
                self.thangba_thoigian_mot = (self.tong_thoigian_mot // 11) + them_mot
                self.thangbon_thoigian_mot = (self.tong_thoigian_mot // 11) + them_mot
                self.thangnam_thoigian_mot = (self.tong_thoigian_mot // 11) + them_mot
                self.thangsau_thoigian_mot = (self.tong_thoigian_mot // 11) + them_mot
                self.thangbay_thoigian_mot = (self.tong_thoigian_mot // 11) + them_mot
                self.thangtam_thoigian_mot = (self.tong_thoigian_mot // 11) + them_mot
                self.thangchin_thoigian_mot = (self.tong_thoigian_mot // 11) + them_mot
                self.thangmuoi_thoigian_mot = (self.tong_thoigian_mot // 11) + them_mot
                self.thangmuoimot_thoigian_mot = (self.tong_thoigian_mot // 11) + themcuoi_mot
                self.thangmuoihai_thoigian_mot = (self.tong_thoigian_mot // 11) + themcuoi_mot

                du_hai = self.tong_thoigian_hai % 11
                if du_hai < 5.5:
                    them_hai = 0
                    themcuoi_hai = (du_hai / 2)
                elif du_hai >= 5.5:
                    them_hai = 0.5
                    themcuoi_hai = (du_hai - 0.5 * 9) / 2

                self.thangmot_thoigian_hai = int(0)
                self.thanghai_thoigian_hai = (self.tong_thoigian_hai // 11) + them_hai
                self.thangba_thoigian_hai = (self.tong_thoigian_hai // 11) + them_hai
                self.thangbon_thoigian_hai = (self.tong_thoigian_hai // 11) + them_hai
                self.thangnam_thoigian_hai = (self.tong_thoigian_hai // 11) + them_hai
                self.thangsau_thoigian_hai = (self.tong_thoigian_hai // 11) + them_hai
                self.thangbay_thoigian_hai = (self.tong_thoigian_hai // 11) + them_hai
                self.thangtam_thoigian_hai = (self.tong_thoigian_hai // 11) + them_hai
                self.thangchin_thoigian_hai = (self.tong_thoigian_hai // 11) + them_hai
                self.thangmuoi_thoigian_hai = (self.tong_thoigian_hai // 11) + them_hai
                self.thangmuoimot_thoigian_hai = (self.tong_thoigian_hai // 11) + themcuoi_hai
                self.thangmuoihai_thoigian_hai = (self.tong_thoigian_hai // 11) + themcuoi_hai

                du_ba = self.tong_thoigian_ba % 11
                if du_ba < 5.5:
                    them_ba = 0
                    themcuoi_ba = (du_ba / 2)
                elif du_ba >= 5.5:
                    them_ba = 0.5
                    themcuoi_ba = (du_ba - 0.5 * 9) / 2

                self.thangmot_thoigian_ba = int(0)
                self.thanghai_thoigian_ba = (self.tong_thoigian_ba // 11) + them_ba
                self.thangba_thoigian_ba = (self.tong_thoigian_ba // 11) + them_ba
                self.thangbon_thoigian_ba = (self.tong_thoigian_ba // 11) + them_ba
                self.thangnam_thoigian_ba = (self.tong_thoigian_ba // 11) + them_ba
                self.thangsau_thoigian_ba = (self.tong_thoigian_ba // 11) + them_ba
                self.thangbay_thoigian_ba = (self.tong_thoigian_ba // 11) + them_ba
                self.thangtam_thoigian_ba = (self.tong_thoigian_ba // 11) + them_ba
                self.thangchin_thoigian_ba = (self.tong_thoigian_ba // 11) + them_ba
                self.thangmuoi_thoigian_ba = (self.tong_thoigian_ba // 11) + them_ba
                self.thangmuoimot_thoigian_ba = (self.tong_thoigian_ba // 11) + themcuoi_ba
                self.thangmuoihai_thoigian_ba = (self.tong_thoigian_ba // 11) + themcuoi_ba

                du_bon = self.tong_thoigian_bon % 11
                if du_bon < 5.5:
                    them_bon = 0
                    themcuoi_bon = (du_bon / 2)
                elif du_bon >= 5.5:
                    them_bon = 0.5
                    themcuoi_bon = (du_bon - 0.5 * 9) / 2

                self.thangmot_thoigian_bon = int(0)
                self.thanghai_thoigian_bon = (self.tong_thoigian_bon // 11) + them_bon
                self.thangba_thoigian_bon = (self.tong_thoigian_bon // 11) + them_bon
                self.thangbon_thoigian_bon = (self.tong_thoigian_bon // 11) + them_bon
                self.thangnam_thoigian_bon = (self.tong_thoigian_bon // 11) + them_bon
                self.thangsau_thoigian_bon = (self.tong_thoigian_bon // 11) + them_bon
                self.thangbay_thoigian_bon = (self.tong_thoigian_bon // 11) + them_bon
                self.thangtam_thoigian_bon = (self.tong_thoigian_bon // 11) + them_bon
                self.thangchin_thoigian_bon = (self.tong_thoigian_bon // 11) + them_bon
                self.thangmuoi_thoigian_bon = (self.tong_thoigian_bon // 11) + them_bon
                self.thangmuoimot_thoigian_bon = (self.tong_thoigian_bon // 11) + themcuoi_bon
                self.thangmuoihai_thoigian_bon = (self.tong_thoigian_bon // 11) + themcuoi_bon

                du_nam = self.tong_thoigian_nam % 11
                if du_nam < 5.5:
                    them_nam = 0
                    themcuoi_nam = (du_nam / 2)
                elif du_nam >= 5.5:
                    them_nam = 0.5
                    themcuoi_nam = (du_nam - 0.5 * 9) / 2

                self.thangmot_thoigian_nam = int(0)
                self.thanghai_thoigian_nam = (self.tong_thoigian_nam // 11) + them_nam
                self.thangba_thoigian_nam = (self.tong_thoigian_nam // 11) + them_nam
                self.thangbon_thoigian_nam = (self.tong_thoigian_nam // 11) + them_nam
                self.thangnam_thoigian_nam = (self.tong_thoigian_nam // 11) + them_nam
                self.thangsau_thoigian_nam = (self.tong_thoigian_nam // 11) + them_nam
                self.thangbay_thoigian_nam = (self.tong_thoigian_nam // 11) + them_nam
                self.thangtam_thoigian_nam = (self.tong_thoigian_nam // 11) + them_nam
                self.thangchin_thoigian_nam = (self.tong_thoigian_nam // 11) + them_nam
                self.thangmuoi_thoigian_nam = (self.tong_thoigian_nam // 11) + them_nam
                self.thangmuoimot_thoigian_nam = (self.tong_thoigian_nam // 11) + themcuoi_nam
                self.thangmuoihai_thoigian_nam = (self.tong_thoigian_nam // 11) + themcuoi_nam

                du_sau = self.tong_thoigian_sau % 11
                if du_sau < 5.5:
                    them_sau = 0
                    themcuoi_sau = (du_sau / 2)
                elif du_sau >= 5.5:
                    them_sau = 0.5
                    themcuoi_sau = (du_sau - 0.5 * 9) / 2

                self.thangmot_thoigian_sau = int(0)
                self.thanghai_thoigian_sau = (self.tong_thoigian_sau // 11) + them_sau
                self.thangba_thoigian_sau = (self.tong_thoigian_sau // 11) + them_sau
                self.thangbon_thoigian_sau = (self.tong_thoigian_sau // 11) + them_sau
                self.thangnam_thoigian_sau = (self.tong_thoigian_sau // 11) + them_sau
                self.thangsau_thoigian_sau = (self.tong_thoigian_sau // 11) + them_sau
                self.thangbay_thoigian_sau = (self.tong_thoigian_sau // 11) + them_sau
                self.thangtam_thoigian_sau = (self.tong_thoigian_sau // 11) + them_sau
                self.thangchin_thoigian_sau = (self.tong_thoigian_sau // 11) + them_sau
                self.thangmuoi_thoigian_sau = (self.tong_thoigian_sau // 11) + them_sau
                self.thangmuoimot_thoigian_sau = (self.tong_thoigian_sau // 11) + themcuoi_sau
                self.thangmuoihai_thoigian_sau = (self.tong_thoigian_sau // 11) + themcuoi_sau

                self.tong_thoigian_thangmot = self.thangmot_thoigian_mot + self.thangmot_thoigian_hai + self.thangmot_thoigian_ba + self.thangmot_thoigian_bon + self.thangmot_thoigian_nam + self.thangmot_thoigian_sau
                self.tong_thoigian_thanghai = self.thanghai_thoigian_mot + self.thanghai_thoigian_hai + self.thanghai_thoigian_ba + self.thanghai_thoigian_bon + self.thanghai_thoigian_nam + self.thanghai_thoigian_sau
                self.tong_thoigian_thangba = self.thangba_thoigian_mot + self.thangba_thoigian_hai + self.thangba_thoigian_ba + self.thangba_thoigian_bon + self.thangba_thoigian_nam + self.thangba_thoigian_sau
                self.tong_thoigian_thangbon = self.thangbon_thoigian_mot + self.thangbon_thoigian_hai + self.thangbon_thoigian_ba + self.thangbon_thoigian_bon + self.thangbon_thoigian_nam + self.thangbon_thoigian_sau
                self.tong_thoigian_thangnam = self.thangnam_thoigian_mot + self.thangnam_thoigian_hai + self.thangnam_thoigian_ba + self.thangnam_thoigian_bon + self.thangnam_thoigian_nam + self.thangnam_thoigian_sau
                self.tong_thoigian_thangsau = self.thangsau_thoigian_mot + self.thangsau_thoigian_hai + self.thangsau_thoigian_ba + self.thangsau_thoigian_bon + self.thangsau_thoigian_nam + self.thangsau_thoigian_sau
                self.tong_thoigian_thangbay = self.thangbay_thoigian_mot + self.thangbay_thoigian_hai + self.thangbay_thoigian_ba + self.thangbay_thoigian_bon + self.thangbay_thoigian_nam + self.thangbay_thoigian_sau
                self.tong_thoigian_thangtam = self.thangtam_thoigian_mot + self.thangtam_thoigian_hai + self.thangtam_thoigian_ba + self.thangtam_thoigian_bon + self.thangtam_thoigian_nam + self.thangtam_thoigian_sau
                self.tong_thoigian_thangchin = self.thangchin_thoigian_mot + self.thangchin_thoigian_hai + self.thangchin_thoigian_ba + self.thangchin_thoigian_bon + self.thangchin_thoigian_nam + self.thangchin_thoigian_sau
                self.tong_thoigian_thangmuoi = self.thangmuoi_thoigian_mot + self.thangmuoi_thoigian_hai + self.thangmuoi_thoigian_ba + self.thangmuoi_thoigian_bon + self.thangmuoi_thoigian_nam + self.thangmuoi_thoigian_sau
                self.tong_thoigian_thangmuoimot = self.thangmuoimot_thoigian_mot + self.thangmuoimot_thoigian_hai + self.thangmuoimot_thoigian_ba + self.thangmuoimot_thoigian_bon + self.thangmuoimot_thoigian_nam + self.thangmuoimot_thoigian_sau
                self.tong_thoigian_thangmuoihai = self.thangmuoihai_thoigian_mot + self.thangmuoihai_thoigian_hai + self.thangmuoihai_thoigian_ba + self.thangmuoihai_thoigian_bon + self.thangmuoihai_thoigian_nam + self.thangmuoihai_thoigian_sau
            else:
                phanba = (self.nam_thoigian // 6)
                self.tong_thoigian_sau = math.ceil((phanba * 10) / 100.0)
                self.tong_thoigian_nam = phanba - self.tong_thoigian_sau

                phanhai = (self.nam_thoigian // 6) * 2
                self.tong_thoigian_bon = math.ceil((phanhai * 10) / 100.0)
                self.tong_thoigian_ba = phanhai - self.tong_thoigian_bon

                phanmot = (self.nam_thoigian - (phanhai + phanba))
                self.tong_thoigian_hai = math.ceil((phanmot * 10) / 100.0)
                self.tong_thoigian_mot = phanmot - self.tong_thoigian_hai

                du_mot = self.tong_thoigian_mot % 12
                if du_mot < 6.0:
                    them_mot = 0
                    themcuoi_mot = (du_mot / 2)
                elif du_mot >= 6.0:
                    them_mot = 0.5
                    themcuoi_mot = (du_mot - 0.5 * 10) / 2

                self.thangmot_thoigian_mot = (self.tong_thoigian_mot // 12) + them_mot
                self.thanghai_thoigian_mot = (self.tong_thoigian_mot // 12) + them_mot
                self.thangba_thoigian_mot = (self.tong_thoigian_mot // 12) + them_mot
                self.thangbon_thoigian_mot = (self.tong_thoigian_mot // 12) + them_mot
                self.thangnam_thoigian_mot = (self.tong_thoigian_mot // 12) + them_mot
                self.thangsau_thoigian_mot = (self.tong_thoigian_mot // 12) + them_mot
                self.thangbay_thoigian_mot = (self.tong_thoigian_mot // 12) + them_mot
                self.thangtam_thoigian_mot = (self.tong_thoigian_mot // 12) + them_mot
                self.thangchin_thoigian_mot = (self.tong_thoigian_mot // 12) + them_mot
                self.thangmuoi_thoigian_mot = (self.tong_thoigian_mot // 12) + them_mot
                self.thangmuoimot_thoigian_mot = (self.tong_thoigian_mot // 12) + themcuoi_mot
                self.thangmuoihai_thoigian_mot = (self.tong_thoigian_mot // 12) + themcuoi_mot

                du_hai = self.tong_thoigian_hai % 12
                if du_hai < 6.0:
                    them_hai = 0
                    themcuoi_hai = (du_hai / 2)
                elif du_hai >= 6.0:
                    them_hai = 0.5
                    themcuoi_hai = (du_hai - 0.5 * 10) / 2

                self.thangmot_thoigian_hai = (self.tong_thoigian_hai // 12) + them_hai
                self.thanghai_thoigian_hai = (self.tong_thoigian_hai // 12) + them_hai
                self.thangba_thoigian_hai = (self.tong_thoigian_hai // 12) + them_hai
                self.thangbon_thoigian_hai = (self.tong_thoigian_hai // 12) + them_hai
                self.thangnam_thoigian_hai = (self.tong_thoigian_hai // 12) + them_hai
                self.thangsau_thoigian_hai = (self.tong_thoigian_hai // 12) + them_hai
                self.thangbay_thoigian_hai = (self.tong_thoigian_hai // 12) + them_hai
                self.thangtam_thoigian_hai = (self.tong_thoigian_hai // 12) + them_hai
                self.thangchin_thoigian_hai = (self.tong_thoigian_hai // 12) + them_hai
                self.thangmuoi_thoigian_hai = (self.tong_thoigian_hai // 12) + them_hai
                self.thangmuoimot_thoigian_hai = (self.tong_thoigian_hai // 12) + themcuoi_hai
                self.thangmuoihai_thoigian_hai = (self.tong_thoigian_hai // 12) + themcuoi_hai

                du_ba = self.tong_thoigian_ba % 12
                if du_ba < 6.0:
                    them_ba = 0
                    themcuoi_ba = (du_ba / 2)
                elif du_ba >= 6.0:
                    them_ba = 0.5
                    themcuoi_ba = (du_ba - 0.5 * 10) / 2

                self.thangmot_thoigian_ba = (self.tong_thoigian_ba // 12) + them_ba
                self.thanghai_thoigian_ba = (self.tong_thoigian_ba // 12) + them_ba
                self.thangba_thoigian_ba = (self.tong_thoigian_ba // 12) + them_ba
                self.thangbon_thoigian_ba = (self.tong_thoigian_ba // 12) + them_ba
                self.thangnam_thoigian_ba = (self.tong_thoigian_ba // 12) + them_ba
                self.thangsau_thoigian_ba = (self.tong_thoigian_ba // 12) + them_ba
                self.thangbay_thoigian_ba = (self.tong_thoigian_ba // 12) + them_ba
                self.thangtam_thoigian_ba = (self.tong_thoigian_ba // 12) + them_ba
                self.thangchin_thoigian_ba = (self.tong_thoigian_ba // 12) + them_ba
                self.thangmuoi_thoigian_ba = (self.tong_thoigian_ba // 12) + them_ba
                self.thangmuoimot_thoigian_ba = (self.tong_thoigian_ba // 12) + themcuoi_ba
                self.thangmuoihai_thoigian_ba = (self.tong_thoigian_ba // 12) + themcuoi_ba

                du_bon = self.tong_thoigian_bon % 12
                if du_bon < 6.0:
                    them_bon = 0
                    themcuoi_bon = (du_bon / 2)
                elif du_bon >= 6.0:
                    them_bon = 0.5
                    themcuoi_bon = (du_bon - 0.5 * 10) / 2

                self.thangmot_thoigian_bon = (self.tong_thoigian_bon // 12) + them_bon
                self.thanghai_thoigian_bon = (self.tong_thoigian_bon // 12) + them_bon
                self.thangba_thoigian_bon = (self.tong_thoigian_bon // 12) + them_bon
                self.thangbon_thoigian_bon = (self.tong_thoigian_bon // 12) + them_bon
                self.thangnam_thoigian_bon = (self.tong_thoigian_bon // 12) + them_bon
                self.thangsau_thoigian_bon = (self.tong_thoigian_bon // 12) + them_bon
                self.thangbay_thoigian_bon = (self.tong_thoigian_bon // 12) + them_bon
                self.thangtam_thoigian_bon = (self.tong_thoigian_bon // 12) + them_bon
                self.thangchin_thoigian_bon = (self.tong_thoigian_bon // 12) + them_bon
                self.thangmuoi_thoigian_bon = (self.tong_thoigian_bon // 12) + them_bon
                self.thangmuoimot_thoigian_bon = (self.tong_thoigian_bon // 12) + themcuoi_bon
                self.thangmuoihai_thoigian_bon = (self.tong_thoigian_bon // 12) + themcuoi_bon

                du_nam = self.tong_thoigian_nam % 12
                if du_nam < 6.0:
                    them_nam = 0
                    themcuoi_nam = (du_nam / 2)
                elif du_nam >= 6.0:
                    them_nam = 0.5
                    themcuoi_nam = (du_nam - 0.5 * 10) / 2

                self.thangmot_thoigian_nam = (self.tong_thoigian_nam // 12) + them_nam
                self.thanghai_thoigian_nam = (self.tong_thoigian_nam // 12) + them_nam
                self.thangba_thoigian_nam = (self.tong_thoigian_nam // 12) + them_nam
                self.thangbon_thoigian_nam = (self.tong_thoigian_nam // 12) + them_nam
                self.thangnam_thoigian_nam = (self.tong_thoigian_nam // 12) + them_nam
                self.thangsau_thoigian_nam = (self.tong_thoigian_nam // 12) + them_nam
                self.thangbay_thoigian_nam = (self.tong_thoigian_nam // 12) + them_nam
                self.thangtam_thoigian_nam = (self.tong_thoigian_nam // 12) + them_nam
                self.thangchin_thoigian_nam = (self.tong_thoigian_nam // 12) + them_nam
                self.thangmuoi_thoigian_nam = (self.tong_thoigian_nam // 12) + them_nam
                self.thangmuoimot_thoigian_nam = (self.tong_thoigian_nam // 12) + themcuoi_nam
                self.thangmuoihai_thoigian_nam = (self.tong_thoigian_nam // 12) + themcuoi_nam

                du_sau = self.tong_thoigian_sau % 12
                if du_sau < 6.0:
                    them_sau = 0
                    themcuoi_sau = (du_sau / 2)
                elif du_sau >= 6.0:
                    them_sau = 0.5
                    themcuoi_sau = (du_sau - 0.5 * 10) / 2

                self.thangmot_thoigian_sau = (self.tong_thoigian_sau // 12) + them_sau
                self.thanghai_thoigian_sau = (self.tong_thoigian_sau // 12) + them_sau
                self.thangba_thoigian_sau = (self.tong_thoigian_sau // 12) + them_sau
                self.thangbon_thoigian_sau = (self.tong_thoigian_sau // 12) + them_sau
                self.thangnam_thoigian_sau = (self.tong_thoigian_sau // 12) + them_sau
                self.thangsau_thoigian_sau = (self.tong_thoigian_sau // 12) + them_sau
                self.thangbay_thoigian_sau = (self.tong_thoigian_sau // 12) + them_sau
                self.thangtam_thoigian_sau = (self.tong_thoigian_sau // 12) + them_sau
                self.thangchin_thoigian_sau = (self.tong_thoigian_sau // 12) + them_sau
                self.thangmuoi_thoigian_sau = (self.tong_thoigian_sau // 12) + them_sau
                self.thangmuoimot_thoigian_sau = (self.tong_thoigian_sau // 12) + themcuoi_sau
                self.thangmuoihai_thoigian_sau = (self.tong_thoigian_sau // 12) + themcuoi_sau

                self.tong_thoigian_thangmot = self.thangmot_thoigian_mot + self.thangmot_thoigian_hai + self.thangmot_thoigian_ba + self.thangmot_thoigian_bon + self.thangmot_thoigian_nam + self.thangmot_thoigian_sau
                self.tong_thoigian_thanghai = self.thanghai_thoigian_mot + self.thanghai_thoigian_hai + self.thanghai_thoigian_ba + self.thanghai_thoigian_bon + self.thanghai_thoigian_nam + self.thanghai_thoigian_sau
                self.tong_thoigian_thangba = self.thangba_thoigian_mot + self.thangba_thoigian_hai + self.thangba_thoigian_ba + self.thangba_thoigian_bon + self.thangba_thoigian_nam + self.thangba_thoigian_sau
                self.tong_thoigian_thangbon = self.thangbon_thoigian_mot + self.thangbon_thoigian_hai + self.thangbon_thoigian_ba + self.thangbon_thoigian_bon + self.thangbon_thoigian_nam + self.thangbon_thoigian_sau
                self.tong_thoigian_thangnam = self.thangnam_thoigian_mot + self.thangnam_thoigian_hai + self.thangnam_thoigian_ba + self.thangnam_thoigian_bon + self.thangnam_thoigian_nam + self.thangnam_thoigian_sau
                self.tong_thoigian_thangsau = self.thangsau_thoigian_mot + self.thangsau_thoigian_hai + self.thangsau_thoigian_ba + self.thangsau_thoigian_bon + self.thangsau_thoigian_nam + self.thangsau_thoigian_sau
                self.tong_thoigian_thangbay = self.thangbay_thoigian_mot + self.thangbay_thoigian_hai + self.thangbay_thoigian_ba + self.thangbay_thoigian_bon + self.thangbay_thoigian_nam + self.thangbay_thoigian_sau
                self.tong_thoigian_thangtam = self.thangtam_thoigian_mot + self.thangtam_thoigian_hai + self.thangtam_thoigian_ba + self.thangtam_thoigian_bon + self.thangtam_thoigian_nam + self.thangtam_thoigian_sau
                self.tong_thoigian_thangchin = self.thangchin_thoigian_mot + self.thangchin_thoigian_hai + self.thangchin_thoigian_ba + self.thangchin_thoigian_bon + self.thangchin_thoigian_nam + self.thangchin_thoigian_sau
                self.tong_thoigian_thangmuoi = self.thangmuoi_thoigian_mot + self.thangmuoi_thoigian_hai + self.thangmuoi_thoigian_ba + self.thangmuoi_thoigian_bon + self.thangmuoi_thoigian_nam + self.thangmuoi_thoigian_sau
                self.tong_thoigian_thangmuoimot = self.thangmuoimot_thoigian_mot + self.thangmuoimot_thoigian_hai + self.thangmuoimot_thoigian_ba + self.thangmuoimot_thoigian_bon + self.thangmuoimot_thoigian_nam + self.thangmuoimot_thoigian_sau
                self.tong_thoigian_thangmuoihai = self.thangmuoihai_thoigian_mot + self.thangmuoihai_thoigian_hai + self.thangmuoihai_thoigian_ba + self.thangmuoihai_thoigian_bon + self.thangmuoihai_thoigian_nam + self.thangmuoihai_thoigian_sau

            self.tong_thoigian = self.tong_thoigian_mot + self.tong_thoigian_hai + self.tong_thoigian_ba + self.tong_thoigian_bon + self.tong_thoigian_nam + self.tong_thoigian_sau

    @api.depends('tong_thoigian_mot', 'tong_thoigian_hai', 'tong_thoigian_ba', 'tong_thoigian_bon',
                 'tong_thoigian_nam', 'tong_thoigian_sau')
    def _kiemtra(self):
        if self.tong_thoigian_mot and self.tong_thoigian_hai and self.tong_thoigian_ba and self.tong_thoigian_bon and self.tong_thoigian_nam and self.tong_thoigian_sau:
            self.thoigian_batbuoc = round(
                (((self.tong_thoigian_mot + self.tong_thoigian_hai) / self.nam_thoigian) * 100), 2)
            self.thoigian_lienquan = round(
                (((self.tong_thoigian_ba + self.tong_thoigian_bon) / self.nam_thoigian) * 100), 2)
            self.thoigian_xungquanh = round(
                (((self.tong_thoigian_nam + self.tong_thoigian_sau) / self.nam_thoigian) * 100), 2)

            # self.thoigian_xungquanh = 100 - (self.thoigian_batbuoc + self.thoigian_lienquan)

            self.thoigian_toanbo = self.thoigian_batbuoc + self.thoigian_lienquan + self.thoigian_xungquanh

            self.antoan_batbuoc = \
                round((((self.tong_thoigian_hai) / (self.tong_thoigian_mot + self.tong_thoigian_hai)) * 100), 2)

            self.antoan_lienquan = \
                round((((self.tong_thoigian_bon) / (self.tong_thoigian_ba + self.tong_thoigian_bon)) * 100), 2)

            self.antoan_xungquanh = \
                round((((self.tong_thoigian_sau) / (self.tong_thoigian_nam + self.tong_thoigian_sau)) * 100), 2)

            # self.antoan_toanbo = self.antoan_batbuoc + self.antoan_lienquan + self.antoan_xungquanh

    @api.onchange('thangmot_thoigian_mot', 'thanghai_thoigian_mot', 'thangba_thoigian_mot', 'thangbon_thoigian_mot',
                  'thangnam_thoigian_mot', 'thangsau_thoigian_mot', 'thangbay_thoigian_mot', 'thangtam_thoigian_mot',
                  'thangchin_thoigian_mot', 'thangmuoi_thoigian_mot', 'thangmuoimot_thoigian_mot',
                  'thangmuoihai_thoigian_mot',
                  'thangmot_thoigian_hai', 'thanghai_thoigian_hai', 'thangba_thoigian_hai', 'thangbon_thoigian_hai',
                  'thangnam_thoigian_hai', 'thangsau_thoigian_hai', 'thangbay_thoigian_hai', 'thangtam_thoigian_hai',
                  'thangchin_thoigian_hai', 'thangmuoi_thoigian_hai', 'thangmuoimot_thoigian_hai',
                  'thangmuoihai_thoigian_hai',
                  'thangmot_thoigian_ba', 'thanghai_thoigian_ba', 'thangba_thoigian_ba', 'thangbon_thoigian_ba',
                  'thangnam_thoigian_ba', 'thangsau_thoigian_ba', 'thangbay_thoigian_ba', 'thangtam_thoigian_ba',
                  'thangchin_thoigian_ba', 'thangmuoi_thoigian_ba', 'thangmuoimot_thoigian_ba',
                  'thangmuoihai_thoigian_ba',
                  'thangmot_thoigian_bon', 'thanghai_thoigian_bon', 'thangba_thoigian_bon', 'thangbon_thoigian_bon',
                  'thangnam_thoigian_bon', 'thangsau_thoigian_bon', 'thangbay_thoigian_bon', 'thangtam_thoigian_bon',
                  'thangchin_thoigian_bon', 'thangmuoi_thoigian_bon', 'thangmuoimot_thoigian_bon',
                  'thangmuoihai_thoigian_bon',
                  'thangmot_thoigian_nam', 'thanghai_thoigian_nam', 'thangba_thoigian_nam', 'thangbon_thoigian_nam',
                  'thangnam_thoigian_nam', 'thangsau_thoigian_nam', 'thangbay_thoigian_nam', 'thangtam_thoigian_nam',
                  'thangchin_thoigian_nam', 'thangmuoi_thoigian_nam', 'thangmuoimot_thoigian_nam',
                  'thangmuoihai_thoigian_nam',
                  'thangmot_thoigian_sau', 'thanghai_thoigian_sau', 'thangba_thoigian_sau', 'thangbon_thoigian_sau',
                  'thangnam_thoigian_sau', 'thangsau_thoigian_sau', 'thangbay_thoigian_sau', 'thangtam_thoigian_sau',
                  'thangchin_thoigian_sau', 'thangmuoi_thoigian_sau', 'thangmuoimot_thoigian_sau',
                  'thangmuoihai_thoigian_sau',
                  'tong_thoigian_mot', 'tong_thoigian_hai', 'tong_thoigian_ba', 'tong_thoigian_bon',
                  'tong_thoigian_nam', 'tong_thoigian_sau'
                  )
    def onchange_method_tong_thoigian(self):
        self.tong_thoigian_mot = self.thangmot_thoigian_mot + self.thanghai_thoigian_mot + self.thangba_thoigian_mot + \
                                 self.thangbon_thoigian_mot + self.thangnam_thoigian_mot + self.thangsau_thoigian_mot + \
                                 self.thangbay_thoigian_mot + self.thangtam_thoigian_mot + self.thangchin_thoigian_mot + \
                                 self.thangmuoi_thoigian_mot + self.thangmuoimot_thoigian_mot + self.thangmuoihai_thoigian_mot

        self.tong_thoigian_hai = self.thangmot_thoigian_hai + self.thanghai_thoigian_hai + self.thangba_thoigian_hai + \
                                 self.thangbon_thoigian_hai + self.thangnam_thoigian_hai + self.thangsau_thoigian_hai + \
                                 self.thangbay_thoigian_hai + self.thangtam_thoigian_hai + self.thangchin_thoigian_hai + \
                                 self.thangmuoi_thoigian_hai + self.thangmuoimot_thoigian_hai + self.thangmuoihai_thoigian_hai

        self.tong_thoigian_ba = self.thangmot_thoigian_ba + self.thanghai_thoigian_ba + self.thangba_thoigian_ba + \
                                self.thangbon_thoigian_ba + self.thangnam_thoigian_ba + self.thangsau_thoigian_ba + \
                                self.thangbay_thoigian_ba + self.thangtam_thoigian_ba + self.thangchin_thoigian_ba + \
                                self.thangmuoi_thoigian_ba + self.thangmuoimot_thoigian_ba + self.thangmuoihai_thoigian_ba

        self.tong_thoigian_bon = self.thangmot_thoigian_bon + self.thanghai_thoigian_bon + self.thangba_thoigian_bon + \
                                 self.thangbon_thoigian_bon + self.thangnam_thoigian_bon + self.thangsau_thoigian_bon + \
                                 self.thangbay_thoigian_bon + self.thangtam_thoigian_bon + self.thangchin_thoigian_bon + \
                                 self.thangmuoi_thoigian_bon + self.thangmuoimot_thoigian_bon + self.thangmuoihai_thoigian_bon

        self.tong_thoigian_nam = self.thangmot_thoigian_nam + self.thanghai_thoigian_nam + self.thangba_thoigian_nam + \
                                 self.thangbon_thoigian_nam + self.thangnam_thoigian_nam + self.thangsau_thoigian_nam + \
                                 self.thangbay_thoigian_nam + self.thangtam_thoigian_nam + self.thangchin_thoigian_nam + \
                                 self.thangmuoi_thoigian_nam + self.thangmuoimot_thoigian_nam + self.thangmuoihai_thoigian_nam

        self.tong_thoigian_sau = self.thangmot_thoigian_sau + self.thanghai_thoigian_sau + self.thangba_thoigian_sau + \
                                 self.thangbon_thoigian_sau + self.thangnam_thoigian_sau + self.thangsau_thoigian_sau + \
                                 self.thangbay_thoigian_sau + self.thangtam_thoigian_sau + self.thangchin_thoigian_sau + \
                                 self.thangmuoi_thoigian_sau + self.thangmuoimot_thoigian_sau + self.thangmuoihai_thoigian_sau

        self.tong_thoigian_thangmot = self.thangmot_thoigian_mot + self.thangmot_thoigian_hai + self.thangmot_thoigian_ba + self.thangmot_thoigian_bon + self.thangmot_thoigian_nam + self.thangmot_thoigian_sau
        self.tong_thoigian_thanghai = self.thanghai_thoigian_mot + self.thanghai_thoigian_hai + self.thanghai_thoigian_ba + self.thanghai_thoigian_bon + self.thanghai_thoigian_nam + self.thanghai_thoigian_sau
        self.tong_thoigian_thangba = self.thangba_thoigian_mot + self.thangba_thoigian_hai + self.thangba_thoigian_ba + self.thangba_thoigian_bon + self.thangba_thoigian_nam + self.thangba_thoigian_sau
        self.tong_thoigian_thangbon = self.thangbon_thoigian_mot + self.thangbon_thoigian_hai + self.thangbon_thoigian_ba + self.thangbon_thoigian_bon + self.thangbon_thoigian_nam + self.thangbon_thoigian_sau
        self.tong_thoigian_thangnam = self.thangnam_thoigian_mot + self.thangnam_thoigian_hai + self.thangnam_thoigian_ba + self.thangnam_thoigian_bon + self.thangnam_thoigian_nam + self.thangnam_thoigian_sau
        self.tong_thoigian_thangsau = self.thangsau_thoigian_mot + self.thangsau_thoigian_hai + self.thangsau_thoigian_ba + self.thangsau_thoigian_bon + self.thangsau_thoigian_nam + self.thangsau_thoigian_sau
        self.tong_thoigian_thangbay = self.thangbay_thoigian_mot + self.thangbay_thoigian_hai + self.thangbay_thoigian_ba + self.thangbay_thoigian_bon + self.thangbay_thoigian_nam + self.thangbay_thoigian_sau
        self.tong_thoigian_thangtam = self.thangtam_thoigian_mot + self.thangtam_thoigian_hai + self.thangtam_thoigian_ba + self.thangtam_thoigian_bon + self.thangtam_thoigian_nam + self.thangtam_thoigian_sau
        self.tong_thoigian_thangchin = self.thangchin_thoigian_mot + self.thangchin_thoigian_hai + self.thangchin_thoigian_ba + self.thangchin_thoigian_bon + self.thangchin_thoigian_nam + self.thangchin_thoigian_sau
        self.tong_thoigian_thangmuoi = self.thangmuoi_thoigian_mot + self.thangmuoi_thoigian_hai + self.thangmuoi_thoigian_ba + self.thangmuoi_thoigian_bon + self.thangmuoi_thoigian_nam + self.thangmuoi_thoigian_sau
        self.tong_thoigian_thangmuoimot = self.thangmuoimot_thoigian_mot + self.thangmuoimot_thoigian_hai + self.thangmuoimot_thoigian_ba + self.thangmuoimot_thoigian_bon + self.thangmuoimot_thoigian_nam + self.thangmuoimot_thoigian_sau
        self.tong_thoigian_thangmuoihai = self.thangmuoihai_thoigian_mot + self.thangmuoihai_thoigian_hai + self.thangmuoihai_thoigian_ba + self.thangmuoihai_thoigian_bon + self.thangmuoihai_thoigian_nam + self.thangmuoihai_thoigian_sau

        self.tong_thoigian = self.tong_thoigian_mot + self.tong_thoigian_hai + self.tong_thoigian_ba + self.tong_thoigian_bon + self.tong_thoigian_nam + self.tong_thoigian_sau
