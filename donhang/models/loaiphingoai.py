# -*- coding: utf-8 -*-

from odoo import models, fields, api


class coquanchuanbinuocngoai(models.Model):
    _name = 'thuctapsinh.coquanchuanbinuocngoai'
    _rec_name = 'ten_cquan_cbi_nuocngoai'
    _order = 'id desc'

    tencoquan_chiphinuocngoaithu = fields.One2many(comodel_name="donhang.donhang",
                                                   inverse_name="tencoquan_ngoai_1",
                                                   string="Danh sách chi phí")
    ten_cquan_cbi_nuocngoai = fields.Char(string="Tên cơ quan")  # hhjp_0585
    nguoidaidien_cquan_cbi_nuocngoai = fields.Char(string="Tên người đại diện")  # hhjp_0586
    diachi_cquan_cbi_nuocngoai = fields.Char(string="Địa chỉ")  # hhjp_0587
    dienthoai_cquan_cbi_nuocngoai = fields.Char(string="Điện thoại")  # bs_0045
    email_cquan_cbi_nuocngoai = fields.Char(string="Email")  # bs_0046
    thanhlap_cquan_cbi_nuocngoai = fields.Date(string="Ngày thành lập")  # hhjp_0588
    # lienquan_tts_cquan_cbi_nuocngoai = fields.Char(string="Liên quan đến thực tập sinh")  # hhjp_0589
    lienquan_tts_cquan_cbi_nuocngoai = fields.Selection(string="Liên quan đến thực tập sinh", selection=[
        ('Liên quan đến thực tập sinh', 'Liên quan đến thực tập sinh'), ('Khác', 'Khác')])  # hhjp_0589
    khac_lienquan_tts_cquan_cbi_nuocngoai = fields.Char(string="Liên quan khác")  # bs_0044
    congviec_cquan_cbi_nuocngoai = fields.Char(string="Công việc cơ quan")  # hhjp_0590
    von_cquan_cbi_nuocngoai = fields.Integer(string="Số vốn")  # hhjp_0591
    doanhso_ganday_cquan_cbi_nuocngoai = fields.Integer(string="Doanh số")  # hhjp_0592
    nhanvien_cquan_cbi_nuocngoai = fields.Integer(string="Số nhân viên")  # hhjp_0593


# class chiphinuocngoaithu(models.Model):
#     _name = 'thuctapsinh.chiphinuocngoaithu'
#     _rec_name = 'loaiphi_chiphinuocngoaithu'
#
#     thuctapsinh_chiphinuocngoaithu = fields.Many2one(comodel_name="thuctapsinh.thuctapsinh", string="Thực tập sinh")
#     id = fields.Integer(string="STT")  #
#     # tencoquan_chiphinuocngoaithu = fields.Char(string="Tên cơ quan đã thu phí")  # bs_0023
#     tencoquan_chiphinuocngoaithu = fields.Many2one(comodel_name="thuctapsinh.coquanchuanbinuocngoai",
#                                                    string="Tên cơ quan đã thu phí(Tiếng Nhật)")  # bs_0023
#     tencoquan_chiphinuocngoaithu_v = fields.Char(string="Tên cơ quan đã thu phí(Tiếng Việt)")
#                                                  # related='tencoquan_chiphinuocngoaithu.ten_cquan_cbi_nuocngoai')  # bs_0023_v
#
#
#
#     vaitro_phaicu_chiphinuocngoaithu = fields.Char(string="Có vai trò trong việc phái cử(Tiếng Nhật)")  # bs_0024
#     vaitro_phaicu_chiphinuocngoaithu_v = fields.Char(string="Có vai trò trong việc phái cử(Tiếng Việt)")  # bs_0024_v
#     loaiphi_chiphinuocngoaithu = fields.Char(string="Loại phí(Tiếng Nhật)")  # bs_0025
#     loaiphi_chiphinuocngoaithu_v = fields.Char(string="Loại phí(Tiếng Việt)")  # bs_0025_v
#     ngaythu_chiphinuocngoaithu = fields.Date(string="Ngày thu")  # bs_0026
#     sotien_viet_chiphinuocngoaithu = fields.Integer(string="Số tiền (Việt)")  # bs_0027
#     sotien_nhat_chiphinuocngoaithu = fields.Integer(string="Số tiền (Nhật)")  # bs_0028
#
#
# class chiphiphaicuthu(models.Model):
#     _name = 'thuctapsinh.chiphiphaicuthu'
#     _rec_name = 'loaiphi_chiphiphaicuthu'
#
#     thuctapsinh_chiphiphaicuthu = fields.Many2one(comodel_name="thuctapsinh.thuctapsinh", string="Thực tập sinh")
#     id = fields.Integer(string="STT")  #
#     loaiphi_chiphiphaicuthu = fields.Char(string="Loại phí(Tiếng Nhật)")  # hhjp_0494
#     loaiphi_chiphiphaicuthu_v = fields.Char(string="Loại phí(Tiếng Việt)")  # hhjp_0494_v
#     ngaythu_chiphiphaicuthu = fields.Date(string="Ngày thu")  # hhjp_0495
#     sotien_viet_chiphiphaicuthu = fields.Integer(string="Số tiền (Việt)")  # hhjp_0496
#     sotien_nhat_chiphiphaicuthu = fields.Integer(string="Số tiền (Nhật)")  # bs_0021
