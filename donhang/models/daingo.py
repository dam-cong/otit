# -*- coding: utf-8 -*-

from odoo import models, fields, api


class DaiNgo(models.Model):
    _name = 'daingo.daingo'
    _rec_name = 'donhang_daingo_xinghiep'
    _order = 'id desc'

    @api.model
    def _default_nghiepdoan_xinghiep(self):
        return self.env['nghiepdoan.nghiepdoan'].search([('id', '=', 1)], limit=1)

    nghiepdoan_xinghiep_daingo = fields.Many2one('nghiepdoan.nghiepdoan', string="Nghiệp đoàn",
                                                 default=_default_nghiepdoan_xinghiep)
    donhang_daingo_xinghiep = fields.Many2one(comodel_name="donhang.donhang", string="Đơn hàng", required=True,ondelete='cascade')

    xinghiep_daingo = fields.Many2one(comodel_name="xinghiep.xinghiep", string="Xí nghiệp",
                                      related='donhang_daingo_xinghiep.xinghiep_donhang')
    ngaynhapcuoc_daingo_xinghiep = fields.Date(string="Ngày nhập cuộc", store=True,
                                               related='donhang_daingo_xinghiep.thoigian_donhang')

    madh = fields.Char(string="Mã đơn hàng", required=False, related='donhang_daingo_xinghiep.madonhang_donhang')
    congtyphaicu_daingo = fields.Many2one(comodel_name="congtyphaicu.congtyphaicu", string="Công ty phái cử",
                                      related='donhang_daingo_xinghiep.congtyphaicu_donhang')

    nhiemvu_trachnhiem_tts_daingo = fields.Char(string='Nhiệm vụ và trách nhiệm')
    kinhnghiem_tts_daingo = fields.Char(string='Nhiệm vụ và trách nhiệm')
    luongthang_phanthuong_mot_daingo = fields.Integer(string='Phần thưởng khóa đầu')
    luonggio_phanthuong_mot_daingo = fields.Integer()
    luongthang_phanthuong_hai_daingo = fields.Integer(string='Phần thưởng khóa hai')
    luonggio_phanthuong_hai_daingo = fields.Integer()
    luonggio_phanthuong_khac_daingo = fields.Char(string='Phần thưởng khác')
    laodong_nguoinhat_sosanh = fields.Selection(string="Lao động người Nhật so sánh",
                                                selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')

    @api.onchange('laodong_nguoinhat_sosanh')
    def onchange_method_laodong_nguoinhat_sosanh(self):
        if self.laodong_nguoinhat_sosanh != 'Có':
            self.mucdo_trachnhiem_ss = ''
            self.tuoi_ss = ''
            self.gioitinh_ss = ''
            self.namkinhnghiem_ss = ''
            self.luong_thang = ''
            self.luong_gio = ''
            self.lydo_xemxet_thulao = ''
            self.nhanxet_ss = True
        if self.laodong_nguoinhat_sosanh != 'Không':
            self.mucdo_trachnhiem_ss_ko = ''
            self.tuoi_ss_ko = ''
            self.gioitinh_ss_ko = ''
            self.namkinhnghiem_ss_ko = ''
            self.luong_quydinh_thang_ko = ''
            self.luong_quydinh_gio_ko = ''
            self.quydinhtienluong = 'Có'
            self.luong_thang_ko = ''
            self.luong_gio_ko = ''
            self.lydo_xemxet_thulao_ko = ''
            self.muckhac_ko = True

    mucdo_trachnhiem_ss = fields.Text('Mức độ trách nhiệm và nội dung công việc')  # hhjp_0178
    tuoi_ss = fields.Integer('Công nhân Nhật so sánh')  # hhjp_0904
    gioitinh_ss = fields.Selection(string="Giới tính", selection=[('Nam', 'Nam'), ('Nữ', 'Nữ')],
                                   default='Nam')  # hhjp_0445
    namkinhnghiem_ss = fields.Integer('Năm kinh nghiệm')  # hhjp_0446
    luong_thang = fields.Integer('Phần thưởng')  # hhjp_0447
    luong_gio = fields.Integer('')  # hhjp_0448
    lydo_xemxet_thulao = fields.Text('Lý do mức lương TTS lớn hơn hoặc bằng người Nhật này')  # hhjp_0183
    nhanxet_ss = fields.Boolean(string='Không có nhận xét')

    @api.onchange('nhanxet_ss')
    def onchange_method_nhanxet_ss(self):
        if self.nhanxet_ss:
            self.muckhac_ss = ''

    muckhac_ss = fields.Text('Mục khác')  # hhjp_0449

    mucdo_trachnhiem_ss_ko = fields.Text('Mức độ trách nhiệm và nội dung công việc')  # hhjp_0450
    tuoi_ss_ko = fields.Integer('Người Nhật gần nhất')  # hhjp_0452
    gioitinh_ss_ko = fields.Selection(string="Giới tính", selection=[('Nam', 'Nam'), ('Nữ', 'Nữ')],
                                      default='Nam')  # hhjp_0453
    namkinhnghiem_ss_ko = fields.Integer('Kinh nghiệm')  # hhjp_0454
    # ghichu_luong_ko = fields.Text(string="Ghi chú")  # bs_0045
    luong_quydinh_thang_ko = fields.Integer(string="Phần thưởng")  # hhjp_0460
    luong_quydinh_gio_ko = fields.Integer()  # hhjp_0461
    quydinhtienluong = fields.Selection(string="Quy định tiền lương",
                                        selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')  # hhjp_0459
    luong_thang_ko = fields.Integer(
        'Thù lao được trả cho người lao động Nhật Bản với nhiệm vụ và trách nhiệm tương tự như thực tập sinh kỹ thuật')  # hhjp_0456
    luong_gio_ko = fields.Integer()  # hhjp_0457
    lydo_xemxet_thulao_ko = fields.Text('Lý do mức lương TTS lớn hơn hoặc bằng người Nhật này')  # hhjp_0462
    muckhac_ko = fields.Boolean(string='Không có nhận xét')
    muckhac_ss_ko = fields.Text('Mục khác')  # hhjp_0463

    @api.onchange('muckhac_ko')
    def onchange_method_muckhac_ko(self):
        if self.muckhac_ko:
            self.muckhac_ss_ko = ''

    thulao_phaitra = fields.Integer(string='Thù lao phải trả')  # hhjp_0147
    cc_tp = fields.Selection(string="Được cung cấp thực phẩm", selection=[('Có', 'Có'), ('Không', 'Không')],
                             default='Không')  # bs_0043
    chiphi_tuongduong_thucpham = fields.Integer(string='Chi phí như chi phí thực phẩm')  # hhjp_0162
    cungcap_thucpham_chitiet = fields.Text('Nội dung chi tiết (cung cấp bữa ăn, nguyên liệu)')  # hhjp_0163
    giaithich_tienan = fields.Text('Giải thích phí thu')  # hhjp_0164

    chiphi_tuongduong_sinhhoat = fields.Integer(string='Chi phí thu được như chi phí sinh hoạt')  # hhjp_0165

    cungcap_coso_vatchat = fields.Selection(string="Cung cấp cơ sở vật chất",
                                            selection=[('Tài sản riêng', 'Tài sản riêng'), (
                                                'Tài sản vay mượn', 'Tài sản vay mượn')],
                                            default='Tài sản riêng')  # hhjp_0166
    giaithich_tiennha = fields.Text('Giải thích phí thu')  # hjjp_0167
    tp_ga = fields.Selection(string="Thu phí ga, điện, nước", selection=[('Có', 'Có'), ('Không', 'Không')],
                             default='Không')
    thuphi_ga = fields.Integer('Thu phí hàng tháng')  # hhjp_312
    phithu_khac = fields.Integer('Phí khác')  # hhjp_312
    thuphi_ga_chiphithucte = fields.Boolean(string='Chi phí thực tế')

    # @api.onchange('thuphi_ga_chiphithucte')
    # def onchange_method(self):
    #     self.field_name = ''

    phikhac = fields.Selection(string="Những phí khác", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    noidung_phi_mot = fields.Char()
    tienthu_mot = fields.Integer()
    noidung_phi_hai = fields.Char()
    tienthu_hai = fields.Integer()
    noidung_phi_ba = fields.Char()
    tienthu_ba = fields.Integer()
    noidung_loiich_traphi = fields.Text('Nội dung những lợi ích mà tts nhận được khi trả phí định kì')  # hhjp_0172
    giaithich_phithu = fields.Text('Giải thích phí thu')  # hhjp_0173
    trocap_daotao_thangdau = fields.Selection(string="Trợ cấp",
                                              selection=[('Có', 'Có'), ('Không', 'Không')],
                                              default='Không')  # hhjp_0138
    noidung_trocap_daotao_thangdau = fields.Char(string="Số tiền - nội dung")  # hhjp_0467
    sotien_trocap_daotao_thangdau = fields.Float(string="Số tiền")  # hhjp_0467
    noidung_trocap_daotao_thangdau_v = fields.Char(string="Tiếng mẹ đẻ")  # hhjp_0467
    sotien_trocap_daotao_thangdau_v = fields.Float(string="Tiếng mẹ đẻ")  # hhjp_0467

    @api.onchange('trocap_daotao_thangdau')
    def onchange_trocap_daotao_thangdau(self):
        if self.trocap_daotao_thangdau == 'Không':
            self.noidung_trocap_daotao_thangdau = ''
            self.sotien_trocap_daotao_thangdau = ''
            self.sotien_trocap_daotao_thangdau_v = ''
            self.noidung_trocap_daotao_thangdau_v = ''

    ghichu_trocap_daotao_thangdau = fields.Char(string="Ghi chú")  # hhjp_0464
    ghichu_trocap_daotao_thangdau_v = fields.Char(string="Ghi chú(Tiếng mẹ đẻ)")  # hhjp_0464_v

    trocap_daingo_tienan = fields.Selection(string="Trợ cấp",
                                            selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')  # hhjp_0139
    noidung_trocap_daingo_tienan = fields.Char(string="Nội dung")  # hhjp_0466
    noidung_trocap_daingo_tienan_v = fields.Char(string="Tiếng mẹ đẻ")  # hhjp_0466

    @api.onchange('trocap_daingo_tienan')
    def onchange_trocap_daingo_tienan(self):
        if self.trocap_daingo_tienan == 'Không':
            self.noidung_trocap_daingo_tienan = ''
            self.noidung_trocap_daingo_tienan_v = ''

    sotien_trocap_daingo_tienan = fields.Integer(string="Số tiền")  # bs_0014
    tts_tra_daingo_tienan = fields.Selection(string="Thực tập sinh trả",
                                             selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')  # hhjp_0140
    noidung_tts_tra_daingo_tienan = fields.Char(string="Nội dung")  # hhjp_0468
    noidung_tts_tra_daingo_tienan_v = fields.Char(string="Nội dung(Tiếng mẹ đẻ)")  # hhjp_0468

    @api.onchange('tts_tra_daingo_tienan')
    def onchange_tts_tra_daingo_tienan(self):
        if self.tts_tra_daingo_tienan == 'Không':
            self.noidung_tts_tra_daingo_tienan = ''
            self.noidung_tts_tra_daingo_tienan_v = ''

    # sotien_tts_tra_daingo_tienan = fields.Integer(string="Số tiền")  # bs_0015
    ghichu_daingo_tienan = fields.Char(string="Ghi chú")  # hhjp_0465
    ghichu_daingo_tienan_v = fields.Char(string="Ghi chú(Tiếng mẹ đẻ)")  # hhjp_0465

    trocap_daingo_nhao = fields.Selection(string="Trợ cấp",
                                          selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')  # hhjp_0141
    noidung_trocap_daingo_nhao = fields.Char(string="Nội dung")  # hhjp_0469
    noidung_trocap_daingo_nhao_v = fields.Char(string="Nội dung(Tiếng mẹ đẻ)")  # hhjp_0469

    @api.onchange('trocap_daingo_nhao')
    def onchange_trocap_daingo_nhao(self):
        if self.trocap_daingo_nhao == 'Không':
            self.noidung_trocap_daingo_nhao = ''
            self.noidung_trocap_daingo_nhao_v = ''

    tts_tra_daingo_nhao = fields.Selection(string="Thực tập sinh trả",
                                           selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')  # hhjp_0142
    noidung_tts_tra_daingo_nhao = fields.Char(string="Nội dung")  # hhjp_0470
    noidung_tts_tra_daingo_nhao_v = fields.Char(string="Nội dung(Tiếng mẹ đẻ)")  # hhjp_0470

    @api.onchange('tts_tra_daingo_nhao')
    def onchange_tts_tra_daingo_nhao(self):
        if self.tts_tra_daingo_nhao == 'Không':
            self.noidung_tts_tra_daingo_nhao = ''
            self.noidung_tts_tra_daingo_nhao_v = ''

    ghichu_daingo_nhao = fields.Char(string="Ghi chú")  #
    ghichu_daingo_nhao_v = fields.Char(string="Ghi chú(Tiếng mẹ đẻ)")  #
    loainha_daingo_nhao = fields.Selection(string="Loại hình nhà ở",
                                           selection=[('Kí túc xá', 'Kí túc xá'), ('Nhà thuê', 'Nhà thuê'),
                                                      ('Khác', 'Khác')], default='Kí túc xá')  # hhjp_0143
    khac_loainha_daingo_nhao = fields.Char(string="Loại hình nhà ở khác")  # hhjp_0473
    khac_loainha_daingo_nhao_v = fields.Char(string="Loại hình nhà ở khác(Tiếng mẹ đẻ)")  # hhjp_0473

    @api.onchange('loainha_daingo_nhao')
    def onchange_loainha_daingo_nhao(self):
        if self.loainha_daingo_nhao != 'Khác' or self.loainha_daingo_nhao == 'Khác':
            self.khac_loainha_daingo_nhao = ''
            self.khac_loainha_daingo_nhao_v = ''

    tennha_daingo_nhao = fields.Char(string="Tên nhà")  # hhjp_0144
    tennha_daingo_nhao_v = fields.Char(string="Tiếng mẹ đẻ")  # hhjp_0144_v
    diachi_daingo_nhao = fields.Char(string="Địa chỉ")  # hhjp_0145
    diachi_daingo_nhao_v = fields.Char(string="Địa chỉ(Tiếng mẹ đẻ)")  # hhjp_0145_v
    # sobuudien_daingo_nhao = fields.Many2one(comodel_name='post.office', string='Số bưu điện')  # bs_0018
    sobuudien_daingo_nhao = fields.Char( string='Số bưu điện')  # bs_0018

    @api.onchange('sobuudien_daingo_nhao')
    def _onchange_sobuudien_daingo_nhao(self):
        if self.sobuudien_daingo_nhao:
            sobuudien_daingo_nhao = self.env['post.office'].search([('post', '=', self.sobuudien_daingo_nhao)], limit=1)
            self.diachi_daingo_nhao = sobuudien_daingo_nhao.kanji_address
            self.diachi_daingo_nhao_v = str(sobuudien_daingo_nhao.english_address).upper()
        else:
            self.diachi_daingo_nhao = ''
            self.diachi_daingo_nhao_v = ''

    dienthoai_daingo_nhao = fields.Char(string="Số điện thoại")  # hhjp_0474
    matbang_daingo_nhao = fields.Float(string="Mặt bằng (m²)")  # hhjp_0475
    songuoi_daingo_nhao = fields.Integer(string="Số người 1 phòng (người)")  # hhjp_0476
    dientich_nguoi_daingo_nhao = fields.Float(string="Diện tích 1 người (m²)")  # hhjp_0477
    muckhac_daingo_khac = fields.Text(string="Mục khác")  # hhjp_0478
    muckhac_daingo_khac_v = fields.Text(string="Mục khác(Tiếng mẹ đẻ)")  # hhjp_0478_v

    loainha_daingo_daotao_nhao = fields.Selection(string="Loại hình nhà ở",
                                                  selection=[('Kí túc xá', 'Kí túc xá'), ('Nhà thuê', 'Nhà thuê'),
                                                             ('Khác', 'Khác')], default='Kí túc xá')
    khac_loainha_daingo_daotao_nhao = fields.Char(string="Loại hình nhà ở khác")
    khac_loainha_daingo_daotao_nhao_v = fields.Char(string="Loại hình nhà ở khác(Tiếng mẹ đẻ)")

    @api.onchange('loainha_daingo_daotao_nhao')
    def onchange_loainha_daingo_daotao_nhao(self):
        if self.loainha_daingo_daotao_nhao != 'Khác':
            self.khac_loainha_daingo_daotao_nhao=''
            self.khac_loainha_daingo_daotao_nhao_v =''

    tennha_daingo_daotao_nhao = fields.Char(string="Tên nhà")
    tennha_daingo_daotao_nhao_v = fields.Char(string="Tiếng mẹ đẻ")
    diachi_daingo_daotao_nhao = fields.Char(string="Địa chỉ")
    diachi_daingo_daotao_nhao_v = fields.Char(string="Địa chỉ(Tiếng mẹ đẻ)")
    # sobuudien_daingo_daotao_nhao = fields.Many2one(comodel_name='post.office', string='Số bưu điện')
    sobuudien_daingo_daotao_nhao = fields.Char(string='Số bưu điện')

    @api.onchange('sobuudien_daingo_daotao_nhao')
    def _onchange_sobuudien_daingo_daotao_nhao(self):
        if self.sobuudien_daingo_daotao_nhao:
            sobuudien_daingo_daotao_nhao = self.env['post.office'].search([('post', '=', self.sobuudien_daingo_daotao_nhao)], limit=1)
            self.diachi_daingo_daotao_nhao = sobuudien_daingo_daotao_nhao.kanji_address
            self.diachi_daingo_daotao_nhao_v = str(sobuudien_daingo_daotao_nhao.english_address).upper()
        else:
            self.diachi_daingo_daotao_nhao = ''
            self.diachi_daingo_daotao_nhao_v = ''

    dienthoai_daingo_daotao_nhao = fields.Char(string="Số điện thoại")
    matbang_daingo_daotao_nhao = fields.Float(string="Mặt bằng (m²)")
    songuoi_daingo_daotao_nhao = fields.Integer(string="Số người 1 phòng (người)")
    dientich_nguoi_daingo_daotao_nhao = fields.Float(string="Diện tích 1 người (m²)")
    nhacungcap_daingo_daotao_nhao = fields.Selection(string='Nhà cung cấp',
                                                     selection=[('Tổ chức giám sát', 'Tổ chức giám sát'),
                                                                ('Thực tập sinh', 'Thực tập sinh')],
                                                     default='Tổ chức giám sát')
    thuctapsinhtra_daingo_daotao_nhao = fields.Text(string='Thực tập sinh trả')

    thuctapsinhtra_daingo_daotao_nhao_viet = fields.Text(string='Tiếng mẹ đẻ')
    muckhac_daingo_daotao_khac = fields.Text(string="Mục khác")
    muckhac_daingo_daotao_khac_v = fields.Text(string="Mục khác(Tiếng mẹ đẻ)")
    # Bảng chỗ ở

    bienphap_1_donhang_xinghiep = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không'), ],
                                                   default="Không")  # nhao_0001
    ghichu_1_donhang_xinghiep = fields.Text()  # nhao_0009
    bienphap_2_donhang_xinghiep = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không'), ],
                                                   default="Không")  # nhao_0002
    ghichu_2_donhang_xinghiep = fields.Text()  # nhao_0010
    bienphap_3_donhang_xinghiep = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không'), ],
                                                   default="Không")  # nhao_0003
    ghichu_3_donhang_xinghiep = fields.Text()  # nhao_0011
    bienphap_4_donhang_xinghiep = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không'), ],
                                                   default="Không")  # nhao_0004
    ghichu_4_donhang_xinghiep = fields.Text()  # nhao_0012
    bienphap_5_donhang_xinghiep = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không'), ],
                                                   default="Không")  # nhao_0005
    ghichu_5_donhang_xinghiep = fields.Text()  # nhao_0013
    bienphap_6_donhang_xinghiep = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không'), ],
                                                   default="Không")  # nhao_0006
    ghichu_6_donhang_xinghiep = fields.Text()  # nhao_0014
    bienphap_7_donhang_xinghiep = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không'), ],
                                                   default="Không")  # nhao_0007
    ghichu_7_donhang_xinghiep = fields.Text()  # nhao_0015
    bienphap_8_donhang_xinghiep = fields.Selection(selection=[('Có', 'Có'), ('Không', 'Không'), ],
                                                   default="Không")  # nhao_0008
    ghichu_8_donhang_xinghiep = fields.Text()  # nhao_0016
