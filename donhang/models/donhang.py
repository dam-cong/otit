# -*- coding: utf-8 -*-

from odoo import models, fields, api


class donhang(models.Model):
    _name = 'donhang.donhang'
    _rec_name = 'tendonhang_donhang'
    _order = 'id desc'

    donhang_dilai = fields.Boolean(string='Đơn hàng đi lại')
    madonhang_donhang = fields.Char("Mã đơn hàng", required=True)  #

    custom_id = fields.Char('Mã số')
    _sql_constraints = [('unique_id', 'UNIQUE(madonhang_donhang)', "Đã tồn tại Đơn hàng có mã số này")]

    tendonhang_donhang = fields.Char("Tên đơn hàng", required=True)  #
    congtyphaicu_donhang = fields.Many2one("congtyphaicu.congtyphaicu", string="Công ty phái cử",
                                           required=True)  #

    def _list_thuctapsinh(self):
        if self.id:
            self.danhsachTTS = self.env['thuctapsinh.thuctapsinh'].search([('donhang_tts', '=', self.id)])
        else:
            pass

    @api.model
    def _default_chinhanh_nghiepdoan(self):
        return self.env['nghiepdoan.chinhanh'].search([('chinhanh_nghiepdoan', '=', 1), ('truso_chinh', '=', True)],
                                                      limit=1)

    chinhanh_nghiepdoan = fields.Many2one("nghiepdoan.chinhanh", string="Chi nhánh nghiệp đoàn", required=True,
                                          default=_default_chinhanh_nghiepdoan)

    danhsachTTS = fields.Many2many(comodel_name='thuctapsinh.thuctapsinh', string='Danh sách thực tập sinh',
                                   compute="list_thuctapsinh")

    @api.multi
    def list_thuctapsinh(self):
        if self.id:
            self.danhsachTTS = self.env['thuctapsinh.thuctapsinh'].search([('donhang_tts', '=', self.id)])
        else:
            pass

    @api.model
    def _default_nghiepdoan_daingo(self):
        return self.env['nghiepdoan.nghiepdoan'].search([('id', '=', 1)], limit=1)

    nghiepdoan_donhang = fields.Many2one('nghiepdoan.nghiepdoan', string="Nghiệp đoàn",
                                         default=_default_nghiepdoan_daingo)

    daotaotruoc_donhang = fields.One2many(comodel_name="daotao.daotaotruoc", inverse_name="donhang_daotaotruoc")

    # Nghề nghiệp phải chuyển nhượng
    nganhnghe_donhang = fields.Many2one(comodel_name='nganhnghe.nganhnghe', string='Ngành nghề')

    loainghe_donhang = fields.Many2one(comodel_name='loaicongviec.loaicongviec', string='Loại công việc')
    loainghe_donhang_v = fields.Char(string="Ngành nghề xin thư tiến cử")
    loainghe_donhang_a = fields.Char(string="Ngành nghề xin thư tiến cử (Tiếng anh)")

    @api.onchange('loainghe_donhang')
    def onchange_method_loainghe_donhang(self):
        if self.loainghe_donhang:
            self.loainghe_donhang_v = self.loainghe_donhang.name_loaicongviec_viet
            self.loainghe_donhang_a = self.loainghe_donhang.name_loaicongviec_anh

    congviec_donhang = fields.Many2one(comodel_name='congviec.congviec', string='Công việc')
    ma_congviec_donhang = fields.Char(string='Mã công việc', related='congviec_donhang.ma_congviec')

    @api.onchange('nganhnghe_donhang')
    def onchange_nganhnghe_donhang(self):
        if self.nganhnghe_donhang.name_nganhnghe == False:
            self.loainghe_donhang = ()
            self.congviec_donhang = ()
            self.ma_congviec_donhang = ''

    # Công việc nhiều ngành nghề
    ma_congviec_nhieunganhnghe = fields.Char(string='Mã công việc', compute='_ma_congviec_nhieunganhnghe')
    nganhnghe_nhieunganhnghe = fields.Many2one(comodel_name='nganhnghe.nganhnghe', string='Ngành nghề')

    loainghe_nhieunganhnghe = fields.Many2one(comodel_name='loaicongviec.loaicongviec', string='Loại công việc')

    congviec_nhieunganhnghe = fields.Many2one(comodel_name='congviec.congviec', string='Công việc')

    @api.depends('congviec_nhieunganhnghe')
    def _ma_congviec_nhieunganhnghe(self):
        if self.congviec_nhieunganhnghe:
            self.ma_congviec_nhieunganhnghe = self.congviec_nhieunganhnghe.ma_congviec

    @api.onchange('nganhnghe_nhieunganhnghe')
    def onchange_nganhnghe_donhang(self):
        if self.nganhnghe_nhieunganhnghe.name_nganhnghe == False:
            self.loainghe_nhieunganhnghe = ()
            self.congviec_nhieunganhnghe = ()
            self.ma_congviec_nhieunganhnghe = ''

    tts_nam_donhang = fields.Integer("Số thực tập sinh Nam")  #
    tts_nu_donhang = fields.Integer("Số thực tập sinh Nữ")  #
    tong_tts_donhang = fields.Integer("Tổng thực tập sinh", compute='_tong_tts_donhang')  #

    thoigian_donhang = fields.Date("Thời gian nhập quốc")  # hhjp_0902

    @api.one
    @api.depends('tts_nam_donhang', 'tts_nu_donhang')
    def _tong_tts_donhang(self):
        if self.tts_nam_donhang and self.tts_nu_donhang:
            self.tong_tts_donhang = (self.tts_nam_donhang + self.tts_nu_donhang)
        elif self.tts_nam_donhang:
            self.tong_tts_donhang = self.tts_nam_donhang
        elif self.tts_nu_donhang:
            self.tong_tts_donhang = self.tts_nu_donhang
        else:
            self.tong_tts_donhang = 0
            required = True

    donhang_daingo = fields.One2many(comodel_name="daingo.daingo", inverse_name="donhang_daingo_xinghiep")
    donhang_chitiet = fields.One2many(comodel_name="chitiet.chitiet", inverse_name="donhang_chitiet")

    # Nơi tổ chức thực tập kĩ năng

    xinghiep_donhang = fields.Many2one("xinghiep.xinghiep", string="Xí nghiệp", required=True)
    chinhanh_xinghiep = fields.Many2one("xinghiep.chinhanh", string="Chi nhánh xí nghiệp")
    nhanvien_trachnhiem_chinhanh_xinghiep_1 = fields.Many2one(comodel_name='nhanvien.nhanvien',
                                                              string='Chịu trách nhiệm thực tập')
    nhanvien_thuctap_chinhanh_xinghiep_11 = fields.Many2one(comodel_name='nhanvien.nhanvien', string='Chỉ đạo thực tập')
    nhanvien_thuctap_chinhanh_xinghiep_12 = fields.Many2one(comodel_name='nhanvien.nhanvien')
    nhanvien_thuctap_chinhanh_xinghiep_13 = fields.Many2one(comodel_name='nhanvien.nhanvien')
    nhanvien_thuctap_chinhanh_xinghiep_14 = fields.Many2one(comodel_name='nhanvien.nhanvien')
    nhanvien_thuctap_chinhanh_xinghiep_15 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    nhanvien_doisonh_chinhanh_xinghiep_11 = fields.Many2one(comodel_name='nhanvien.nhanvien', string='Chỉ đạo đời sống')
    nhanvien_doisonh_chinhanh_xinghiep_12 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    chinhanh_xinghiep_2 = fields.Many2one("xinghiep.chinhanh", string="Chi nhánh xí nghiệp")
    nhanvien_trachnhiem_chinhanh_xinghiep_2 = fields.Many2one(comodel_name='nhanvien.nhanvien',
                                                              string='Chịu trách nhiệm thực tập')
    nhanvien_thuctap_chinhanh_xinghiep_21 = fields.Many2one(comodel_name='nhanvien.nhanvien', string='Chỉ đạo thực tập')
    nhanvien_thuctap_chinhanh_xinghiep_22 = fields.Many2one(comodel_name='nhanvien.nhanvien')
    nhanvien_thuctap_chinhanh_xinghiep_23 = fields.Many2one(comodel_name='nhanvien.nhanvien')
    nhanvien_thuctap_chinhanh_xinghiep_24 = fields.Many2one(comodel_name='nhanvien.nhanvien')
    nhanvien_thuctap_chinhanh_xinghiep_25 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    nhanvien_doisonh_chinhanh_xinghiep_21 = fields.Many2one(comodel_name='nhanvien.nhanvien', string='Chỉ đạo đời sống')
    nhanvien_doisonh_chinhanh_xinghiep_22 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    chinhanh_xinghiep_3 = fields.Many2one("xinghiep.chinhanh", string="Chi nhánh xí nghiệp")
    nhanvien_trachnhiem_chinhanh_xinghiep_3 = fields.Many2one(comodel_name='nhanvien.nhanvien',
                                                              string='Chịu trách nhiệm thực tập')
    nhanvien_thuctap_chinhanh_xinghiep_31 = fields.Many2one(comodel_name='nhanvien.nhanvien', string='Chỉ đạo thực tập')
    nhanvien_thuctap_chinhanh_xinghiep_32 = fields.Many2one(comodel_name='nhanvien.nhanvien')
    nhanvien_thuctap_chinhanh_xinghiep_33 = fields.Many2one(comodel_name='nhanvien.nhanvien')
    nhanvien_thuctap_chinhanh_xinghiep_34 = fields.Many2one(comodel_name='nhanvien.nhanvien')
    nhanvien_thuctap_chinhanh_xinghiep_35 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    nhanvien_doisonh_chinhanh_xinghiep_31 = fields.Many2one(comodel_name='nhanvien.nhanvien', string='Chỉ đạo đời sống')
    nhanvien_doisonh_chinhanh_xinghiep_32 = fields.Many2one(comodel_name='nhanvien.nhanvien')

    @api.onchange('xinghiep_donhang')
    def onchange_xinghiep_donhang(self):
        print(self.xinghiep_donhang.ten_han_xnghiep)
        if self.xinghiep_donhang.ten_han_xnghiep == False:
            self.chinhanh_xinghiep = ()
            self.nhanvien_trachnhiem_chinhanh_xinghiep_1 = ()
            self.nhanvien_thuctap_chinhanh_xinghiep_11 = ()
            self.nhanvien_thuctap_chinhanh_xinghiep_12 = ()
            self.nhanvien_thuctap_chinhanh_xinghiep_13 = ()
            self.nhanvien_thuctap_chinhanh_xinghiep_14 = ()
            self.nhanvien_thuctap_chinhanh_xinghiep_15 = ()
            self.nhanvien_doisonh_chinhanh_xinghiep_11 = ()
            self.nhanvien_doisonh_chinhanh_xinghiep_12 = ()

            self.chinhanh_xinghiep_2 = ()
            self.nhanvien_trachnhiem_chinhanh_xinghiep_2 = ()
            self.nhanvien_thuctap_chinhanh_xinghiep_21 = ()
            self.nhanvien_thuctap_chinhanh_xinghiep_22 = ()
            self.nhanvien_thuctap_chinhanh_xinghiep_23 = ()
            self.nhanvien_thuctap_chinhanh_xinghiep_24 = ()
            self.nhanvien_thuctap_chinhanh_xinghiep_25 = ()
            self.nhanvien_doisonh_chinhanh_xinghiep_21 = ()
            self.nhanvien_doisonh_chinhanh_xinghiep_22 = ()

            self.chinhanh_xinghiep_3 = ()
            self.nhanvien_trachnhiem_chinhanh_xinghiep_3 = ()
            self.nhanvien_thuctap_chinhanh_xinghiep_31 = ()
            self.nhanvien_thuctap_chinhanh_xinghiep_32 = ()
            self.nhanvien_thuctap_chinhanh_xinghiep_33 = ()
            self.nhanvien_thuctap_chinhanh_xinghiep_34 = ()
            self.nhanvien_thuctap_chinhanh_xinghiep_35 = ()
            self.nhanvien_doisonh_chinhanh_xinghiep_31 = ()
            self.nhanvien_doisonh_chinhanh_xinghiep_32 = ()

    # ------------------------ Các loại phí ngoài

    tencoquan_ngoai_1 = fields.Many2one(comodel_name="thuctapsinh.coquanchuanbinuocngoai",
                                        string="Tên cơ quan đã thu phí(Tiếng Nhật)")  # bs_0023
    vaitro_phaicu_ngoai_1 = fields.Char(string="Có vai trò trong việc phái cử(Tiếng Nhật)")  # bs_0024
    vaitro_phaicu_ngoai_v_1 = fields.Char(string="Có vai trò trong việc phái cử(Tiếng Việt)")  # bs_0024_v
    loaiphi_ngoai_1 = fields.Char(string="Loại phí(Tiếng Nhật)")  # bs_0025
    loaiphi_ngoai_v_1 = fields.Char(string="Loại phí(Tiếng Việt)")  # bs_0025_v
    ngaythu_ngoai_1 = fields.Date(string="Ngày thu")  # bs_0026
    sotien_viet_ngoai_1 = fields.Integer(string="Số tiền (Việt)")  # bs_0027
    sotien_nhat_ngoai_1 = fields.Integer(string="Số tiền (Nhật)")  # bs_0028
    # ----------------------------------------------------
    tencoquan_ngoai_2 = fields.Many2one(comodel_name="thuctapsinh.coquanchuanbinuocngoai",
                                        string="Tên cơ quan đã thu phí(Tiếng Nhật)")  # bs_0023
    vaitro_phaicu_ngoai_2 = fields.Char(string="Có vai trò trong việc phái cử(Tiếng Nhật)")  # bs_0024
    vaitro_phaicu_ngoai_v_2 = fields.Char(string="Có vai trò trong việc phái cử(Tiếng Việt)")  # bs_0024_v
    loaiphi_ngoai_2 = fields.Char(string="Loại phí(Tiếng Nhật)")  # bs_0025
    loaiphi_ngoai_v_2 = fields.Char(string="Loại phí(Tiếng Việt)")  # bs_0025_v
    ngaythu_ngoai_2 = fields.Date(string="Ngày thu")  # bs_0026
    sotien_viet_ngoai_2 = fields.Integer(string="Số tiền (Việt)")  # bs_0027
    sotien_nhat_ngoai_2 = fields.Integer(string="Số tiền (Nhật)")  # bs_0028
    # ------------------------------------------------------
    tencoquan_ngoai_3 = fields.Many2one(comodel_name="thuctapsinh.coquanchuanbinuocngoai",
                                        string="Tên cơ quan đã thu phí(Tiếng Nhật)")  # bs_0023
    vaitro_phaicu_ngoai_3 = fields.Char(string="Có vai trò trong việc phái cử(Tiếng Nhật)")  # bs_0024
    vaitro_phaicu_ngoai_v_3 = fields.Char(string="Có vai trò trong việc phái cử(Tiếng Việt)")  # bs_0024_v
    loaiphi_ngoai_3 = fields.Char(string="Loại phí(Tiếng Nhật)")  # bs_0025
    loaiphi_ngoai_v_3 = fields.Char(string="Loại phí(Tiếng Việt)")  # bs_0025_v
    ngaythu_ngoai_3 = fields.Date(string="Ngày thu")  # bs_0026
    sotien_viet_ngoai_3 = fields.Integer(string="Số tiền (Việt)")  # bs_0027
    sotien_nhat_ngoai_3 = fields.Integer(string="Số tiền (Nhật)")  # bs_0028
    # -------------------------------------------------------
    tencoquan_ngoai_4 = fields.Many2one(comodel_name="thuctapsinh.coquanchuanbinuocngoai",
                                        string="Tên cơ quan đã thu phí(Tiếng Nhật)")  # bs_0023
    vaitro_phaicu_ngoai_4 = fields.Char(string="Có vai trò trong việc phái cử(Tiếng Nhật)")  # bs_0024
    vaitro_phaicu_ngoai_v_4 = fields.Char(string="Có vai trò trong việc phái cử(Tiếng Việt)")  # bs_0024_v
    loaiphi_ngoai_4 = fields.Char(string="Loại phí(Tiếng Nhật)")  # bs_0025
    loaiphi_ngoai_v_4 = fields.Char(string="Loại phí(Tiếng Việt)")  # bs_0025_v
    ngaythu_ngoai_4 = fields.Date(string="Ngày thu")  # bs_0026
    sotien_viet_ngoai_4 = fields.Integer(string="Số tiền (Việt)")  # bs_0027
    sotien_nhat_ngoai_4 = fields.Integer(string="Số tiền (Nhật)")  # bs_0028
    # -------------------------------------------------------
    tencoquan_ngoai_5 = fields.Many2one(comodel_name="thuctapsinh.coquanchuanbinuocngoai",
                                        string="Tên cơ quan đã thu phí")  # bs_0023
    vaitro_phaicu_ngoai_5 = fields.Char(string="Có vai trò trong việc phái cử(Tiếng Nhật)")  # bs_0024
    vaitro_phaicu_ngoai_v_5 = fields.Char(string="Có vai trò trong việc phái cử(Tiếng Việt)")  # bs_0024_v
    loaiphi_ngoai_5 = fields.Char(string="Loại phí(Tiếng Nhật)")  # bs_0025
    loaiphi_ngoai_v_5 = fields.Char(string="Loại phí(Tiếng Việt)")  # bs_0025_v
    ngaythu_ngoai_5 = fields.Date(string="Ngày thu")  # bs_0026
    sotien_viet_ngoai_5 = fields.Integer(string="Số tiền (Việt)")  # bs_0027
    sotien_nhat_ngoai_5 = fields.Integer(string="Số tiền (Nhật)")  # bs_0028
    # -------------------------------------------------------
    tong_ngoai_v = fields.Integer(string="Tổng cộng", compute="_tong_ngoai_v")
    tong_ngoai_n = fields.Integer(string="Tổng cộng", compute="_tong_ngoai_n")

    @api.multi
    @api.depends('sotien_viet_ngoai_1', 'sotien_viet_ngoai_2', 'sotien_viet_ngoai_3', 'sotien_viet_ngoai_4',
                 'sotien_viet_ngoai_5')
    def _tong_ngoai_v(self):
        if self.sotien_viet_ngoai_1 or self.sotien_viet_ngoai_2 or self.sotien_viet_ngoai_3 or self.sotien_viet_ngoai_4 or self.sotien_viet_ngoai_5:
            self.tong_ngoai_v = self.sotien_viet_ngoai_1 + self.sotien_viet_ngoai_2 + self.sotien_viet_ngoai_3 + self.sotien_viet_ngoai_4 + self.sotien_viet_ngoai_5
        else:
            self.tong_ngoai_v = 0

    @api.multi
    @api.depends('sotien_nhat_ngoai_1', 'sotien_nhat_ngoai_2', 'sotien_nhat_ngoai_3', 'sotien_nhat_ngoai_4',
                 'sotien_nhat_ngoai_5')
    def _tong_ngoai_n(self):
        if self.sotien_nhat_ngoai_1 or self.sotien_nhat_ngoai_2 or self.sotien_nhat_ngoai_3 or self.sotien_nhat_ngoai_4 or self.sotien_nhat_ngoai_5:
            self.tong_ngoai_n = self.sotien_nhat_ngoai_1 + self.sotien_nhat_ngoai_2 + self.sotien_nhat_ngoai_3 + self.sotien_nhat_ngoai_4 + self.sotien_nhat_ngoai_5
        else:
            self.tong_ngoai_n = 0

    coquan_chuanbi_ngoai = fields.Many2one(comodel_name="coquan.coquan", string="Cơ quan chuẩn bị của nước ngoài")

    def showpopup(self):
        view_id = self.env.ref('hoso.hosocongtyphaicu_form_view').id
        res_id_hs = self.env['hoso.hoso'].search([('donhang_hoso', '=', self.id)], limit=1)
        context = self._context.copy()
        context['form_view_initial_mode'] = 'edit'
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id,
            'res_model': 'hoso.hoso',
            'res_id': res_id_hs.id,
            'context': context,
            'target': 'new',
        }

    def showpopup_hoso(self):
        view_id = self.env.ref('hoso.hoso_form_view').id
        res_id_hs = self.env['hoso.hoso'].search([('donhang_hoso', '=', self.id)], limit=1)
        context = self._context.copy()
        context['form_view_initial_mode'] = 'edit'
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id,
            'res_model': 'hoso.hoso',
            'res_id': res_id_hs.id,
            'context': context,
            'target': 'new',
        }

    def showpopup_luutru(self):
        view_id = self.env.ref('hoso.luutru_form_view').id
        res_id_hs = self.env['luutru.luutru'].search([('donhang_luutru', '=', self.id)], limit=1)
        context = self._context.copy()
        context['form_view_initial_mode'] = 'edit'
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id,
            'res_model': 'luutru.luutru',
            'res_id': res_id_hs.id,
            'context': context,
            'target': 'new',
        }

    def showpopup_daingo(self):
        view_id = self.env.ref('donhang.daingo_form_view').id
        res_id_hs = self.env['daingo.daingo'].search([('donhang_daingo_xinghiep', '=', self.id)], limit=1)
        context = self._context.copy()
        context['form_view_initial_mode'] = 'edit'
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id,
            'res_model': 'daingo.daingo',
            'res_id': res_id_hs.id,
            'context': context,
            'target': 'new',
        }

    def showpopup_daotao(self):
        view_id = self.env.ref('donhang.daotaosau_form_view').id
        res_id_hs = self.env['daotao.daotaosau'].search([('donhang_daotaosau', '=', self.id)], limit=1)
        context = self._context.copy()
        context['form_view_initial_mode'] = 'edit'
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id,
            'res_model': 'daotao.daotaosau',
            'res_id': res_id_hs.id,
            'context': context,
            'target': 'new',
        }

    @api.model
    def create(self, vals):
        danhsach = super(donhang, self).create(vals)
        giaidoan = self.env['giaidoan.giaidoan'].search([])

        if danhsach.donhang_dilai == False:
            self.env['daotao.daotaotruoc'].create({'donhang_daotaotruoc': danhsach.id})
            self.env['hoso.hoso'].create({'donhang_hoso': danhsach.id, 'giaidoan_hoso': '1 go'})
            self.env['luutru.luutru'].create({'donhang_luutru': danhsach.id, 'giaidoan_luutru': '1 go'})
            self.env['daingo.daingo'].create({'donhang_daingo_xinghiep': danhsach.id})
            self.env['daotao.daotaosau'].create({'donhang_daotaosau': danhsach.id})
            item = 0
            for id_danhsach in giaidoan:
                if item < 3:
                    self.env['chitiet.chitiet'].create({'donhang_chitiet': danhsach.id, 'giaidoan': id_danhsach.id})
                    item += 1
                else:
                    break
        elif danhsach.donhang_dilai == True:
            self.env['hoso.hoso'].create({'donhang_hoso': danhsach.id, 'giaidoan_hoso': '3 go'})
            self.env['luutru.luutru'].create({'donhang_luutru': danhsach.id, 'giaidoan_luutru': '3 go'})
            self.env['daingo.daingo'].create({'donhang_daingo_xinghiep': danhsach.id})
            item = 0
            for id_danhsach in giaidoan:
                if item < 3:
                    item += 1
                else:
                    self.env['chitiet.chitiet'].create({'donhang_chitiet': danhsach.id, 'giaidoan': id_danhsach.id})

        return danhsach
