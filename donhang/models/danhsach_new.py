# -*- coding: utf-8 -*-
from odoo import models, fields, api


class DanhSach(models.Model):
    _name = 'thuctapsinh.danhsachnew'
    _rec_name = 'thuctapsinh_danhsach'

    thuctapsinh_danhsach = fields.Many2one(comodel_name='thuctapsinh.thuctapsinh', string='Thực tập sinh',
                                           required=True)
    donhang_danhsach = fields.Many2one(comodel_name="donhang.donhang", string="Đơn hàng",
                                       related='thuctapsinh_danhsach.donhang_tts')
    giaidoan_danhsach = fields.Selection(string='Giai đoạn', selection=[('1 go', '1 go'),
                                                                        ('2 go', '2 go'),
                                                                        ('3 go', '3 go')])

    file_1 = fields.Selection(string="file_1", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_1 = fields.Text()

    file_2 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_2 = fields.Text()

    file_3 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_3 = fields.Text()

    file_4 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')] , default='Không')
    ghichu_file_4 = fields.Text()

    file_5 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_5 = fields.Text()

    file_6 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_6 = fields.Text()

    file_7 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_7 = fields.Text()

    file_8 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_8 = fields.Text()

    file_9 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_9 = fields.Text()

    file_10 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_10 = fields.Text()

    file_11 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_11 = fields.Text()

    file_12 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_12 = fields.Text()

    file_13 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_13 = fields.Text()

    file_14 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_14 = fields.Text()

    file_15 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_15 = fields.Text()

    file_16 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_16 = fields.Text()

    file_17_1 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_17_1 = fields.Text()

    file_17_2 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_17_2 = fields.Text()

    file_18 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_18 = fields.Text()

    file_19 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_19 = fields.Text()

    file_20 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_20 = fields.Text()

    file_21 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_21 = fields.Text()

    file_22 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    file_23 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_22 = fields.Text()

    file_24 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_24 = fields.Text()

    file_25 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_25 = fields.Text()

    file_26 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_26 = fields.Text()

    file_27 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_27 = fields.Text()

    file_28 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_28 = fields.Text()

    file_29 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_29 = fields.Text()

    file_30 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_30 = fields.Text()

    file_31 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_31 = fields.Text()

    file_32 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_32 = fields.Text()

    file_33 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_33 = fields.Text()

    file_34 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_34 = fields.Text()

    file_35 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_35 = fields.Text()

    file_36 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_36 = fields.Text()

    file_37 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_37 = fields.Text()

    file_38 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_38 = fields.Text()

    file_39 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_39 = fields.Text()

    file_40 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_40 = fields.Text()

    file_41 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_41 = fields.Text()

    file_42 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_42 = fields.Text()

    file_43 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_43 = fields.Text()

    file_44 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_44 = fields.Text()

    file_45 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_45 = fields.Text()

    file_46 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_46 = fields.Text()

    file_47 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_47 = fields.Text()

    file_48 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_48 = fields.Text()

    file_49 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_49 = fields.Text()

    file_50 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_50 = fields.Text()

    file_51 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_51 = fields.Text()

    file_52 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_52 = fields.Text()

    file_53 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_53 = fields.Text()

    file_54 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    file_55 = fields.Selection(string="file_2", selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')
    ghichu_file_54 = fields.Text()
