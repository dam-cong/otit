# -*- coding: utf-8 -*-

from odoo import models, fields, api


class DaoTaoPhaiCu(models.Model):
    _name = 'daotao.daotaophaicu'
    _rec_name = 'tencoquan_daotaophaicu'
    _order = 'id desc'

    daotao_phaicu_daotaotruoc = fields.Many2one(comodel_name='daotao.daotaotruoc', string='Đào tạo phái cử')
    id = fields.Integer(string='STT')
    noidung_daotaophaicu = fields.Char(string='Nội dung')
    tencoquan_daotaophaicu = fields.Char(string='Tên cơ quan')
    diachicoquan_daotaophaicu = fields.Char(string='Địa chỉ cơ quan')
    tencoso_daotaophaicu = fields.Char(string='Tên cơ sở')
    daichicoso_daotaophaicu = fields.Char(string='Địa chỉ cơ sở')
    # uythac_daotaophaicu = fields.Char(string='Ủy thác ngoài')
    uythac_daotaophaicu = fields.Boolean(string='Ủy thác ngoài')
    batdau_daotaophaicu = fields.Date(string='Bắt đầu')
    ketthuc_daotaophaicu = fields.Date(string='Kết thúc')
    sogioi_daotaophaicu = fields.Float(string='Số giờ')