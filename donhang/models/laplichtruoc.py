# -*- coding: utf-8 -*-

from odoo import models, fields, api


class LapLichTruoc(models.Model):
    _name = 'laplich.laplichtruoc'

    daotaotruoc = fields.Many2one(comodel_name='daotao.daotaotruoc')
    ngay_laplichtruoc = fields.Date(string='Ngày')
    # noidung_laplichtruoc = fields.Char(string='Nội dung lập lịch trước')
    noidung_laplichtruoc = fields.Many2one(comodel_name='daotaotruoc.noidungdaotao', string='Nội dung lập lịch trước')
    ngaynghi_truoc = fields.Boolean(string="Ngày nghỉ")
    giangvien_laplichtruoc = fields.Many2one(comodel_name="daotaotruoc.giaovien", string='Giảng viên')
    congviec_giangvien_laplichtruoc = fields.Char(string='Công việc - Giảng viên',
                                                  related='giangvien_laplichtruoc.chucvu')

    coso_laplichtruoc = fields.Many2one(comodel_name="csdt.csdt", string='Cơ sở đào tạo')
    ghichu_laplichtruoc = fields.Char(string='Ghi chú')

    # @api.onchange('giangvien_laplichtruoc')
    # def _onchange_giangvien_laplichtruoc(self):
    #     self.congviec_giangvien_laplichtruoc = self.giangvien_laplichtruoc.chucvu

    @api.multi
    @api.onchange('ngaynghi_truoc')
    def _onchange_ngaynghi_truoc(self):
        noidungdaotaotruoc = self.env['daotaotruoc.noidungdaotao'].search([('noidungdaotao', '=', '休日')], limit=1)
        if self.ngaynghi_truoc == True:
            self.noidung_laplichtruoc = noidungdaotaotruoc.id
            self.coso_laplichtruoc = ''
            self.giangvien_laplichtruoc = ''
        elif self.ngaynghi_truoc == False:
            self.noidung_laplichtruoc = ''
            self.coso_laplichtruoc = ''
            self.giangvien_laplichtruoc = ''
