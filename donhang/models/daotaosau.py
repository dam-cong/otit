# -*- coding: utf-8 -*-
from odoo import models, fields, api
import datetime


def convert_th(self):
    ans = datetime.date(self.year, self.month, self.day)
    substr = ans.strftime("%A")
    if substr == 'Sunday':
        return '日'
    elif substr == 'Monday':
        return '月'
    elif substr == 'Tuesday':
        return '火'
    elif substr == 'Wednesday':
        return '水'
    elif substr == 'Thursday':
        return '木'
    elif substr == 'Friday':
        return '金'
    elif substr == 'Saturday':
        return '土'
    else:
        return ''


class DaoTaoSau(models.Model):
    _name = 'daotao.daotaosau'
    _rec_name = 'donhang_daotaosau'
    _order = 'id desc'

    donhang_daotaosau = fields.Many2one(comodel_name='donhang.donhang', string='Đơn hàng')

    @api.onchange('donhang_daotaosau')
    def onchange_method_donhang_daotaosau(self):
        self.xinghiep_daotaosau = self.donhang_daotaosau.xinghiep_donhang.id

    xinghiep_daotaosau = fields.Many2one(comodel_name='xinghiep.xinghiep', string='Xí nghiệp',
                                         related='donhang_daotaosau.xinghiep_donhang')
    coso_daotaosau_a = fields.Many2one(comodel_name="nghiepdoan.cosodaotao", string="① Cơ sở đào tạo")
    coso_daotaosau_b = fields.Many2one(comodel_name="nghiepdoan.cosodaotao", string="② Cơ sở đào tạo")
    coso_daotaosau_c = fields.Many2one(comodel_name="nghiepdoan.cosodaotao", string="③ Cơ sở đào tạo")
    giangvien_daotaosau = fields.One2many(comodel_name='giangvien.giangvien', inverse_name='giangvien_phapluat',
                                          string='Danh sách giảng viên')
    giangvien_phapluat_daotaosau = fields.Char(string='Tên giảng viên')
    nghenghiep_giangvien_daotaosau = fields.Char(string='Nghề nghiệp')
    coquan_giangvien_daotaosau = fields.Char(string='Cơ quan làm việc')
    kinhnghiem_giangvien_daotaosau = fields.Char(string='Kinh nghiệm')
    bangcap_giangvien_daotaosau = fields.Char(string='Bằng cấp')

    @api.multi
    def addgiangvien_button(self):
        daotaosau = self.search([('id', '=', self.id)])
        giangvien = self.env['giangvien.giangvien'].search(
            [('giangvien_phapluat', '=', self.id), ('ten_giangvien_phaply', '=', self.giangvien_phapluat_daotaosau)],
            limit=1)
        if giangvien:
            pass
        else:
            daotaosau.write({
                'giangvien_daotaosau': [(0, 0, {
                    'ten_giangvien_phaply': self.giangvien_phapluat_daotaosau,
                    'coquan_giangvien_phaply': self.coquan_giangvien_daotaosau,
                })]
            })

    # Khung giờ học
    batdau_sang = fields.Float(string='Bắt đầu - Sáng')  # dt_0001
    ketthuc_sang = fields.Float(string='Kế thúc - Sáng')  # dt_0002
    batdau_chieu = fields.Float(string='Bắt đầu - Chiều')  # dt_0003
    ketthuc_chieu = fields.Float(string='Kế thúc - Chiều')  # dt_0004
    # Thời gian đào tạo
    batdau_daotao = fields.Date(string='Ngày bắt đầu')
    ketthuc_daotao = fields.Date(string='Ngày kêt thúc')

    thu_hai = fields.Boolean(string='月')
    thu_ba = fields.Boolean(string='火')
    thu_bon = fields.Boolean(string='水')
    thu_nam = fields.Boolean(string='木')
    thu_sau = fields.Boolean(string='金')
    thu_bay = fields.Boolean(string='土')
    chu_nhat = fields.Boolean(string='日')
    ngay_le = fields.Boolean(string='祝')

    tonggio_daotao = fields.Integer(string='Tổng giờ đào tạo')

    laplich_daotaosau = fields.One2many(comodel_name='laplich.laplich', inverse_name='daotaosau', string='Lập lịch',
                                        copy=True)

    # Ngày _1
    # Ca sáng
    lich_laplich_1 = fields.Date()
    thang_1 = fields.Char(compute='_thang_1')
    ngay_1 = fields.Char(compute='_thang_1')
    thu_1 = fields.Char(compute='_thang_1')
    ngaynghi_1 = fields.Boolean()
    cangay_1 = fields.Boolean()
    noidung_sang_1 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_1 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_1 = fields.Char()
    noidung_chieu_char_1 = fields.Char()
    coso_sang_1 = fields.Boolean()
    coso_chieu_1 = fields.Boolean()
    cosao_daotao_a_1 = fields.Boolean(string='①')
    cosao_daotao_b_1 = fields.Boolean(string='②')
    cosao_daotao_c_1 = fields.Boolean(string='③')
    thoigian_1 = fields.Integer()
    giaovien_1 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_1 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_1 = fields.Boolean()
    coso_chieu_chieu_a_1 = fields.Boolean(string='①')
    coso_chieu_chieu_b_1 = fields.Boolean(string='②')
    coso_chieu_chieu_c_1 = fields.Boolean(string='③')
    thoigian_chieu_1 = fields.Integer()
    giaovien_chieu_1 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_1')
    def onchange_method_coso_chieu_chieu_a_1(self):
        if self.coso_chieu_chieu_a_1 == True:
            self.coso_chieu_chieu_b_1 = False
            self.coso_chieu_chieu_c_1 = False

    @api.onchange('coso_chieu_chieu_b_1')
    def onchange_method_coso_chieu_chieu_b_1(self):
        if self.coso_chieu_chieu_b_1 == True:
            self.coso_chieu_chieu_a_1 = False
            self.coso_chieu_chieu_c_1 = False

    @api.onchange('coso_chieu_chieu_c_1')
    def onchange_method_coso_chieu_chieu_c_1(self):
        if self.coso_chieu_chieu_c_1 == True:
            self.coso_chieu_chieu_a_1 = False
            self.coso_chieu_chieu_b_1 = False

    @api.onchange('cosao_daotao_a_1')
    def onchange_method_cosao_daotao_a_1(self):
        if self.cosao_daotao_a_1 == True:
            self.cosao_daotao_b_1 = False
            self.cosao_daotao_c_1 = False

    @api.onchange('cosao_daotao_b_1')
    def onchange_method_cosao_daotao_b_1(self):
        if self.cosao_daotao_b_1 == True:
            self.cosao_daotao_a_1 = False
            self.cosao_daotao_c_1 = False

    @api.onchange('cosao_daotao_c_1')
    def onchange_method_cosao_daotao_c_1(self):
        if self.cosao_daotao_c_1 == True:
            self.cosao_daotao_a_1 = False
            self.cosao_daotao_b_1 = False

    @api.one
    @api.depends('lich_laplich_1')
    def _thang_1(self):
        if self.lich_laplich_1:
            self.thang_1 = self.lich_laplich_1.month
            self.ngay_1 = self.lich_laplich_1.day
            self.thu_1 = convert_th(self.lich_laplich_1)
        else:
            self.thang_1 = ''
            self.ngay_1 = ''
            self.thu_1 = ''

    @api.onchange('cangay_1')
    def onchange_method_cangay_1(self):
        if self.cangay_1 == True:
            self.noidung_chieu_1 = None
            self.coso_chieu_1 = False
        else:
            self.coso_chieu_chieu_a_1 = False
            self.coso_chieu_chieu_b_1 = False
            self.coso_chieu_chieu_c_1 = False
            self.noidung_chieu_chieu_1 = None
            self.coso_chieu_chieu_1 = False
            self.thoigian_chieu_1 = 0
            self.giaovien_chieu_1 = None

    @api.onchange('ngaynghi_1')
    def onchange_method_ngaynghi_1(self):
        if self.ngaynghi_1 == True:
            self.cangay_1 = False
            self.noidung_sang_1 = None
            self.noidung_chieu_1 = None
            self.noidung_sang_char_1 = '休日'
            self.noidung_chieu_char_1 = '休日'
            self.coso_sang_1 = False
            self.coso_chieu_1 = False
            self.cosao_daotao_a_1 = False
            self.cosao_daotao_b_1 = False
            self.cosao_daotao_c_1 = False
            self.thoigian_1 = 0
            self.giaovien_1 = None
            self.noidung_chieu_chieu_1 = None
            self.coso_chieu_chieu_1 = False
            self.coso_chieu_chieu_a_1 = False
            self.coso_chieu_chieu_b_1 = False
            self.coso_chieu_chieu_c_1 = False
            self.thoigian_chieu_1 = 0
            self.giaovien_chieu_1 = None
        else:
            self.cangay_1 = False
            self.noidung_sang_1 = None
            self.noidung_chieu_1 = None
            self.noidung_sang_char_1 = ''
            self.noidung_chieu_char_1 = ''
            self.coso_sang_1 = False
            self.coso_chieu_1 = False
            self.cosao_daotao_a_1 = False
            self.cosao_daotao_b_1 = False
            self.cosao_daotao_c_1 = False
            self.thoigian_1 = 0
            self.giaovien_1 = None
            self.noidung_chieu_chieu_1 = None
            self.coso_chieu_chieu_1 = False
            self.coso_chieu_chieu_a_1 = False
            self.coso_chieu_chieu_b_1 = False
            self.coso_chieu_chieu_c_1 = False
            self.thoigian_chieu_1 = 0
            self.giaovien_chieu_1 = None

    @api.onchange('noidung_sang_1', 'noidung_chieu_1')
    def onchange_method_noidung_sang_1(self):
        if self.noidung_sang_1 and self.noidung_chieu_1:
            if self.noidung_sang_1.noidungdaotao != '休日' and self.noidung_chieu_1.noidungdaotao != '休日':
                self.thoigian_1 = 8
            elif self.noidung_sang_1.noidungdaotao != '休日' or self.noidung_chieu_1.noidungdaotao != '休日':
                self.thoigian_1 = 4
            else:
                self.thoigian_1 = 0
        elif self.noidung_sang_1 or self.noidung_chieu_1:
            if self.noidung_sang_1.noidungdaotao != '休日' and self.noidung_sang_1.noidungdaotao:
                self.thoigian_1 = 4
            elif self.noidung_chieu_1.noidungdaotao != '休日' and self.noidung_chieu_1.noidungdaotao:
                self.thoigian_1 = 4
            else:
                self.thoigian_1 = 0
        else:
            self.thoigian_1 = 0

    @api.onchange('noidung_chieu_chieu_1')
    def onchange_method_noidung_chieu_chieu_1(self):
        if self.noidung_chieu_chieu_1:
            if self.noidung_chieu_chieu_1.noidungdaotao != '休日':
                self.thoigian_chieu_1 = 4
        else:
            self.thoigian_chieu_1 = 0
    # Ngày _2
    # Ca sáng
    lich_laplich_2 = fields.Date()
    thang_2 = fields.Char(compute='_thang_2')
    ngay_2 = fields.Char(compute='_thang_2')
    thu_2 = fields.Char(compute='_thang_2')
    ngaynghi_2 = fields.Boolean()
    cangay_2 = fields.Boolean()
    noidung_sang_2 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_2 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_2 = fields.Char()
    noidung_chieu_char_2 = fields.Char()
    coso_sang_2 = fields.Boolean()
    coso_chieu_2 = fields.Boolean()
    cosao_daotao_a_2 = fields.Boolean(string='①')
    cosao_daotao_b_2 = fields.Boolean(string='②')
    cosao_daotao_c_2 = fields.Boolean(string='③')
    thoigian_2 = fields.Integer()
    giaovien_2 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_2 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_2 = fields.Boolean()
    coso_chieu_chieu_a_2 = fields.Boolean(string='①')
    coso_chieu_chieu_b_2 = fields.Boolean(string='②')
    coso_chieu_chieu_c_2 = fields.Boolean(string='③')
    thoigian_chieu_2 = fields.Integer()
    giaovien_chieu_2 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_2')
    def onchange_method_coso_chieu_chieu_a_2(self):
        if self.coso_chieu_chieu_a_2 == True:
            self.coso_chieu_chieu_b_2 = False
            self.coso_chieu_chieu_c_2 = False

    @api.onchange('coso_chieu_chieu_b_2')
    def onchange_method_coso_chieu_chieu_b_2(self):
        if self.coso_chieu_chieu_b_2 == True:
            self.coso_chieu_chieu_a_2 = False
            self.coso_chieu_chieu_c_2 = False

    @api.onchange('coso_chieu_chieu_c_2')
    def onchange_method_coso_chieu_chieu_c_2(self):
        if self.coso_chieu_chieu_c_2 == True:
            self.coso_chieu_chieu_a_2 = False
            self.coso_chieu_chieu_b_2 = False

    @api.onchange('cosao_daotao_a_2')
    def onchange_method_cosao_daotao_a_2(self):
        if self.cosao_daotao_a_2 == True:
            self.cosao_daotao_b_2 = False
            self.cosao_daotao_c_2 = False

    @api.onchange('cosao_daotao_b_2')
    def onchange_method_cosao_daotao_b_2(self):
        if self.cosao_daotao_b_2 == True:
            self.cosao_daotao_a_2 = False
            self.cosao_daotao_c_2 = False

    @api.onchange('cosao_daotao_c_2')
    def onchange_method_cosao_daotao_c_2(self):
        if self.cosao_daotao_c_2 == True:
            self.cosao_daotao_a_2 = False
            self.cosao_daotao_b_2 = False

    @api.one
    @api.depends('lich_laplich_2')
    def _thang_2(self):
        if self.lich_laplich_2:
            self.thang_2 = self.lich_laplich_2.month
            self.ngay_2 = self.lich_laplich_2.day
            self.thu_2 = convert_th(self.lich_laplich_2)
        else:
            self.thang_2 = ''
            self.ngay_2 = ''
            self.thu_2 = ''

    @api.onchange('cangay_2')
    def onchange_method_cangay_2(self):
        if self.cangay_2 == True:
            self.noidung_chieu_2 = None
            self.coso_chieu_2 = False
        else:
            self.coso_chieu_chieu_a_2 = False
            self.coso_chieu_chieu_b_2 = False
            self.coso_chieu_chieu_c_2 = False
            self.noidung_chieu_chieu_2 = None
            self.coso_chieu_chieu_2 = False
            self.thoigian_chieu_2 = 0
            self.giaovien_chieu_2 = None

    @api.onchange('ngaynghi_2')
    def onchange_method_ngaynghi_2(self):
        if self.ngaynghi_2 == True:
            self.cangay_2 = False
            self.noidung_sang_2 = None
            self.noidung_chieu_2 = None
            self.noidung_sang_char_2 = '休日'
            self.noidung_chieu_char_2 = '休日'
            self.coso_sang_2 = False
            self.coso_chieu_2 = False
            self.cosao_daotao_a_2 = False
            self.cosao_daotao_b_2 = False
            self.cosao_daotao_c_2 = False
            self.thoigian_2 = 0
            self.giaovien_2 = None
            self.noidung_chieu_chieu_2 = None
            self.coso_chieu_chieu_2 = False
            self.coso_chieu_chieu_a_2 = False
            self.coso_chieu_chieu_b_2 = False
            self.coso_chieu_chieu_c_2 = False
            self.thoigian_chieu_2 = 0
            self.giaovien_chieu_2 = None
        else:
            self.cangay_2 = False
            self.noidung_sang_2 = None
            self.noidung_chieu_2 = None
            self.noidung_sang_char_2 = ''
            self.noidung_chieu_char_2 = ''
            self.coso_sang_2 = False
            self.coso_chieu_2 = False
            self.cosao_daotao_a_2 = False
            self.cosao_daotao_b_2 = False
            self.cosao_daotao_c_2 = False
            self.thoigian_2 = 0
            self.giaovien_2 = None
            self.noidung_chieu_chieu_2 = None
            self.coso_chieu_chieu_2 = False
            self.coso_chieu_chieu_a_2 = False
            self.coso_chieu_chieu_b_2 = False
            self.coso_chieu_chieu_c_2 = False
            self.thoigian_chieu_2 = 0
            self.giaovien_chieu_2 = None

    @api.onchange('noidung_sang_2', 'noidung_chieu_2')
    def onchange_method_noidung_sang_2(self):
        if self.noidung_sang_2 and self.noidung_chieu_2:
            if self.noidung_sang_2.noidungdaotao != '休日' and self.noidung_chieu_2.noidungdaotao != '休日':
                self.thoigian_2 = 8
            elif self.noidung_sang_2.noidungdaotao != '休日' or self.noidung_chieu_2.noidungdaotao != '休日':
                self.thoigian_2 = 4
            else:
                self.thoigian_2 = 0
        elif self.noidung_sang_2 or self.noidung_chieu_2:
            if self.noidung_sang_2.noidungdaotao != '休日' and self.noidung_sang_2.noidungdaotao:
                self.thoigian_2 = 4
            elif self.noidung_chieu_2.noidungdaotao != '休日' and self.noidung_chieu_2.noidungdaotao:
                self.thoigian_2 = 4
            else:
                self.thoigian_2 = 0
        else:
            self.thoigian_2 = 0

    @api.onchange('noidung_chieu_chieu_2')
    def onchange_method_noidung_chieu_chieu_2(self):
        if self.noidung_chieu_chieu_2:
            if self.noidung_chieu_chieu_2.noidungdaotao != '休日':
                self.thoigian_chieu_2 = 4
        else:
            self.thoigian_chieu_2 = 0

    # Ngày _3
    # Ca sáng
    lich_laplich_3 = fields.Date()
    thang_3 = fields.Char(compute='_thang_3')
    ngay_3 = fields.Char(compute='_thang_3')
    thu_3 = fields.Char(compute='_thang_3')
    ngaynghi_3 = fields.Boolean()
    cangay_3 = fields.Boolean()
    noidung_sang_3 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_3 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_3 = fields.Char()
    noidung_chieu_char_3 = fields.Char()
    coso_sang_3 = fields.Boolean()
    coso_chieu_3 = fields.Boolean()
    cosao_daotao_a_3 = fields.Boolean(string='①')
    cosao_daotao_b_3 = fields.Boolean(string='②')
    cosao_daotao_c_3 = fields.Boolean(string='③')
    thoigian_3 = fields.Integer()
    giaovien_3 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_3 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_3 = fields.Boolean()
    coso_chieu_chieu_a_3 = fields.Boolean(string='①')
    coso_chieu_chieu_b_3 = fields.Boolean(string='②')
    coso_chieu_chieu_c_3 = fields.Boolean(string='③')
    thoigian_chieu_3 = fields.Integer()
    giaovien_chieu_3 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_3')
    def onchange_method_coso_chieu_chieu_a_3(self):
        if self.coso_chieu_chieu_a_3 == True:
            self.coso_chieu_chieu_b_3 = False
            self.coso_chieu_chieu_c_3 = False

    @api.onchange('coso_chieu_chieu_b_3')
    def onchange_method_coso_chieu_chieu_b_3(self):
        if self.coso_chieu_chieu_b_3 == True:
            self.coso_chieu_chieu_a_3 = False
            self.coso_chieu_chieu_c_3 = False

    @api.onchange('coso_chieu_chieu_c_3')
    def onchange_method_coso_chieu_chieu_c_3(self):
        if self.coso_chieu_chieu_c_3 == True:
            self.coso_chieu_chieu_a_3 = False
            self.coso_chieu_chieu_b_3 = False

    @api.onchange('cosao_daotao_a_3')
    def onchange_method_cosao_daotao_a_3(self):
        if self.cosao_daotao_a_3 == True:
            self.cosao_daotao_b_3 = False
            self.cosao_daotao_c_3 = False

    @api.onchange('cosao_daotao_b_3')
    def onchange_method_cosao_daotao_b_3(self):
        if self.cosao_daotao_b_3 == True:
            self.cosao_daotao_a_3 = False
            self.cosao_daotao_c_3 = False

    @api.onchange('cosao_daotao_c_3')
    def onchange_method_cosao_daotao_c_3(self):
        if self.cosao_daotao_c_3 == True:
            self.cosao_daotao_a_3 = False
            self.cosao_daotao_b_3 = False

    @api.one
    @api.depends('lich_laplich_3')
    def _thang_3(self):
        if self.lich_laplich_3:
            self.thang_3 = self.lich_laplich_3.month
            self.ngay_3 = self.lich_laplich_3.day
            self.thu_3 = convert_th(self.lich_laplich_3)
        else:
            self.thang_3 = ''
            self.ngay_3 = ''
            self.thu_3 = ''

    @api.onchange('cangay_3')
    def onchange_method_cangay_3(self):
        if self.cangay_3 == True:
            self.noidung_chieu_3 = None
            self.coso_chieu_3 = False
        else:
            self.coso_chieu_chieu_a_3 = False
            self.coso_chieu_chieu_b_3 = False
            self.coso_chieu_chieu_c_3 = False
            self.noidung_chieu_chieu_3 = None
            self.coso_chieu_chieu_3 = False
            self.thoigian_chieu_3 = 0
            self.giaovien_chieu_3 = None

    @api.onchange('ngaynghi_3')
    def onchange_method_ngaynghi_3(self):
        if self.ngaynghi_3 == True:
            self.cangay_3 = False
            self.noidung_sang_3 = None
            self.noidung_chieu_3 = None
            self.noidung_sang_char_3 = '休日'
            self.noidung_chieu_char_3 = '休日'
            self.coso_sang_3 = False
            self.coso_chieu_3 = False
            self.cosao_daotao_a_3 = False
            self.cosao_daotao_b_3 = False
            self.cosao_daotao_c_3 = False
            self.thoigian_3 = 0
            self.giaovien_3 = None
            self.noidung_chieu_chieu_3 = None
            self.coso_chieu_chieu_3 = False
            self.coso_chieu_chieu_a_3 = False
            self.coso_chieu_chieu_b_3 = False
            self.coso_chieu_chieu_c_3 = False
            self.thoigian_chieu_3 = 0
            self.giaovien_chieu_3 = None
        else:
            self.cangay_3 = False
            self.noidung_sang_3 = None
            self.noidung_chieu_3 = None
            self.noidung_sang_char_3 = ''
            self.noidung_chieu_char_3 = ''
            self.coso_sang_3 = False
            self.coso_chieu_3 = False
            self.cosao_daotao_a_3 = False
            self.cosao_daotao_b_3 = False
            self.cosao_daotao_c_3 = False
            self.thoigian_3 = 0
            self.giaovien_3 = None
            self.noidung_chieu_chieu_3 = None
            self.coso_chieu_chieu_3 = False
            self.coso_chieu_chieu_a_3 = False
            self.coso_chieu_chieu_b_3 = False
            self.coso_chieu_chieu_c_3 = False
            self.thoigian_chieu_3 = 0
            self.giaovien_chieu_3 = None

    @api.onchange('noidung_sang_3', 'noidung_chieu_3')
    def onchange_method_noidung_sang_3(self):
        if self.noidung_sang_3 and self.noidung_chieu_3:
            if self.noidung_sang_3.noidungdaotao != '休日' and self.noidung_chieu_3.noidungdaotao != '休日':
                self.thoigian_3 = 8
            elif self.noidung_sang_3.noidungdaotao != '休日' or self.noidung_chieu_3.noidungdaotao != '休日':
                self.thoigian_3 = 4
            else:
                self.thoigian_3 = 0
        elif self.noidung_sang_3 or self.noidung_chieu_3:
            if self.noidung_sang_3.noidungdaotao != '休日' and self.noidung_sang_3.noidungdaotao:
                self.thoigian_3 = 4
            elif self.noidung_chieu_3.noidungdaotao != '休日' and self.noidung_chieu_3.noidungdaotao:
                self.thoigian_3 = 4
            else:
                self.thoigian_3 = 0
        else:
            self.thoigian_3 = 0

    @api.onchange('noidung_chieu_chieu_3')
    def onchange_method_noidung_chieu_chieu_3(self):
        if self.noidung_chieu_chieu_3:
            if self.noidung_chieu_chieu_3.noidungdaotao != '休日':
                self.thoigian_chieu_3 = 4
        else:
            self.thoigian_chieu_3 = 0

    # Ngày _4
    # Ca sáng
    lich_laplich_4 = fields.Date()
    thang_4 = fields.Char(compute='_thang_4')
    ngay_4 = fields.Char(compute='_thang_4')
    thu_4 = fields.Char(compute='_thang_4')
    ngaynghi_4 = fields.Boolean()
    cangay_4 = fields.Boolean()
    noidung_sang_4 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_4 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_4 = fields.Char()
    noidung_chieu_char_4 = fields.Char()
    coso_sang_4 = fields.Boolean()
    coso_chieu_4 = fields.Boolean()
    cosao_daotao_a_4 = fields.Boolean(string='①')
    cosao_daotao_b_4 = fields.Boolean(string='②')
    cosao_daotao_c_4 = fields.Boolean(string='③')
    thoigian_4 = fields.Integer()
    giaovien_4 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_4 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_4 = fields.Boolean()
    coso_chieu_chieu_a_4 = fields.Boolean(string='①')
    coso_chieu_chieu_b_4 = fields.Boolean(string='②')
    coso_chieu_chieu_c_4 = fields.Boolean(string='③')
    thoigian_chieu_4 = fields.Integer()
    giaovien_chieu_4 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_4')
    def onchange_method_coso_chieu_chieu_a_4(self):
        if self.coso_chieu_chieu_a_4 == True:
            self.coso_chieu_chieu_b_4 = False
            self.coso_chieu_chieu_c_4 = False

    @api.onchange('coso_chieu_chieu_b_4')
    def onchange_method_coso_chieu_chieu_b_4(self):
        if self.coso_chieu_chieu_b_4 == True:
            self.coso_chieu_chieu_a_4 = False
            self.coso_chieu_chieu_c_4 = False

    @api.onchange('coso_chieu_chieu_c_4')
    def onchange_method_coso_chieu_chieu_c_4(self):
        if self.coso_chieu_chieu_c_4 == True:
            self.coso_chieu_chieu_a_4 = False
            self.coso_chieu_chieu_b_4 = False

    @api.onchange('cosao_daotao_a_4')
    def onchange_method_cosao_daotao_a_4(self):
        if self.cosao_daotao_a_4 == True:
            self.cosao_daotao_b_4 = False
            self.cosao_daotao_c_4 = False

    @api.onchange('cosao_daotao_b_4')
    def onchange_method_cosao_daotao_b_4(self):
        if self.cosao_daotao_b_4 == True:
            self.cosao_daotao_a_4 = False
            self.cosao_daotao_c_4 = False

    @api.onchange('cosao_daotao_c_4')
    def onchange_method_cosao_daotao_c_4(self):
        if self.cosao_daotao_c_4 == True:
            self.cosao_daotao_a_4 = False
            self.cosao_daotao_b_4 = False

    @api.one
    @api.depends('lich_laplich_4')
    def _thang_4(self):
        if self.lich_laplich_4:
            self.thang_4 = self.lich_laplich_4.month
            self.ngay_4 = self.lich_laplich_4.day
            self.thu_4 = convert_th(self.lich_laplich_4)
        else:
            self.thang_4 = ''
            self.ngay_4 = ''
            self.thu_4 = ''

    @api.onchange('cangay_4')
    def onchange_method_cangay_4(self):
        if self.cangay_4 == True:
            self.noidung_chieu_4 = None
            self.coso_chieu_4 = False
        else:
            self.coso_chieu_chieu_a_4 = False
            self.coso_chieu_chieu_b_4 = False
            self.coso_chieu_chieu_c_4 = False
            self.noidung_chieu_chieu_4 = None
            self.coso_chieu_chieu_4 = False
            self.thoigian_chieu_4 = 0
            self.giaovien_chieu_4 = None

    @api.onchange('ngaynghi_4')
    def onchange_method_ngaynghi_4(self):
        if self.ngaynghi_4 == True:
            self.cangay_4 = False
            self.noidung_sang_4 = None
            self.noidung_chieu_4 = None
            self.noidung_sang_char_4 = '休日'
            self.noidung_chieu_char_4 = '休日'
            self.coso_sang_4 = False
            self.coso_chieu_4 = False
            self.cosao_daotao_a_4 = False
            self.cosao_daotao_b_4 = False
            self.cosao_daotao_c_4 = False
            self.thoigian_4 = 0
            self.giaovien_4 = None
            self.noidung_chieu_chieu_4 = None
            self.coso_chieu_chieu_4 = False
            self.coso_chieu_chieu_a_4 = False
            self.coso_chieu_chieu_b_4 = False
            self.coso_chieu_chieu_c_4 = False
            self.thoigian_chieu_4 = 0
            self.giaovien_chieu_4 = None
        else:
            self.cangay_4 = False
            self.noidung_sang_4 = None
            self.noidung_chieu_4 = None
            self.noidung_sang_char_4 = ''
            self.noidung_chieu_char_4 = ''
            self.coso_sang_4 = False
            self.coso_chieu_4 = False
            self.cosao_daotao_a_4 = False
            self.cosao_daotao_b_4 = False
            self.cosao_daotao_c_4 = False
            self.thoigian_4 = 0
            self.giaovien_4 = None
            self.noidung_chieu_chieu_4 = None
            self.coso_chieu_chieu_4 = False
            self.coso_chieu_chieu_a_4 = False
            self.coso_chieu_chieu_b_4 = False
            self.coso_chieu_chieu_c_4 = False
            self.thoigian_chieu_4 = 0
            self.giaovien_chieu_4 = None

    @api.onchange('noidung_sang_4', 'noidung_chieu_4')
    def onchange_method_noidung_sang_4(self):
        if self.noidung_sang_4 and self.noidung_chieu_4:
            if self.noidung_sang_4.noidungdaotao != '休日' and self.noidung_chieu_4.noidungdaotao != '休日':
                self.thoigian_4 = 8
            elif self.noidung_sang_4.noidungdaotao != '休日' or self.noidung_chieu_4.noidungdaotao != '休日':
                self.thoigian_4 = 4
            else:
                self.thoigian_4 = 0
        elif self.noidung_sang_4 or self.noidung_chieu_4:
            if self.noidung_sang_4.noidungdaotao != '休日' and self.noidung_sang_4.noidungdaotao:
                self.thoigian_4 = 4
            elif self.noidung_chieu_4.noidungdaotao != '休日' and self.noidung_chieu_4.noidungdaotao:
                self.thoigian_4 = 4
            else:
                self.thoigian_4 = 0
        else:
            self.thoigian_4 = 0

    @api.onchange('noidung_chieu_chieu_4')
    def onchange_method_noidung_chieu_chieu_4(self):
        if self.noidung_chieu_chieu_4:
            if self.noidung_chieu_chieu_4.noidungdaotao != '休日':
                self.thoigian_chieu_4 = 4
        else:
            self.thoigian_chieu_4 = 0

    # Ngày _5
    # Ca sáng
    lich_laplich_5 = fields.Date()
    thang_5 = fields.Char(compute='_thang_5')
    ngay_5 = fields.Char(compute='_thang_5')
    thu_5 = fields.Char(compute='_thang_5')
    ngaynghi_5 = fields.Boolean()
    cangay_5 = fields.Boolean()
    noidung_sang_5 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_5 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_5 = fields.Char()
    noidung_chieu_char_5 = fields.Char()
    coso_sang_5 = fields.Boolean()
    coso_chieu_5 = fields.Boolean()
    cosao_daotao_a_5 = fields.Boolean(string='①')
    cosao_daotao_b_5 = fields.Boolean(string='②')
    cosao_daotao_c_5 = fields.Boolean(string='③')
    thoigian_5 = fields.Integer()
    giaovien_5 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_5 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_5 = fields.Boolean()
    coso_chieu_chieu_a_5 = fields.Boolean(string='①')
    coso_chieu_chieu_b_5 = fields.Boolean(string='②')
    coso_chieu_chieu_c_5 = fields.Boolean(string='③')
    thoigian_chieu_5 = fields.Integer()
    giaovien_chieu_5 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_5')
    def onchange_method_coso_chieu_chieu_a_5(self):
        if self.coso_chieu_chieu_a_5 == True:
            self.coso_chieu_chieu_b_5 = False
            self.coso_chieu_chieu_c_5 = False

    @api.onchange('coso_chieu_chieu_b_5')
    def onchange_method_coso_chieu_chieu_b_5(self):
        if self.coso_chieu_chieu_b_5 == True:
            self.coso_chieu_chieu_a_5 = False
            self.coso_chieu_chieu_c_5 = False

    @api.onchange('coso_chieu_chieu_c_5')
    def onchange_method_coso_chieu_chieu_c_5(self):
        if self.coso_chieu_chieu_c_5 == True:
            self.coso_chieu_chieu_a_5 = False
            self.coso_chieu_chieu_b_5 = False

    @api.onchange('cosao_daotao_a_5')
    def onchange_method_cosao_daotao_a_5(self):
        if self.cosao_daotao_a_5 == True:
            self.cosao_daotao_b_5 = False
            self.cosao_daotao_c_5 = False

    @api.onchange('cosao_daotao_b_5')
    def onchange_method_cosao_daotao_b_5(self):
        if self.cosao_daotao_b_5 == True:
            self.cosao_daotao_a_5 = False
            self.cosao_daotao_c_5 = False

    @api.onchange('cosao_daotao_c_5')
    def onchange_method_cosao_daotao_c_5(self):
        if self.cosao_daotao_c_5 == True:
            self.cosao_daotao_a_5 = False
            self.cosao_daotao_b_5 = False

    @api.one
    @api.depends('lich_laplich_5')
    def _thang_5(self):
        if self.lich_laplich_5:
            self.thang_5 = self.lich_laplich_5.month
            self.ngay_5 = self.lich_laplich_5.day
            self.thu_5 = convert_th(self.lich_laplich_5)
        else:
            self.thang_5 = ''
            self.ngay_5 = ''
            self.thu_5 = ''

    @api.onchange('cangay_5')
    def onchange_method_cangay_5(self):
        if self.cangay_5 == True:
            self.noidung_chieu_5 = None
            self.coso_chieu_5 = False
        else:
            self.coso_chieu_chieu_a_5 = False
            self.coso_chieu_chieu_b_5 = False
            self.coso_chieu_chieu_c_5 = False
            self.noidung_chieu_chieu_5 = None
            self.coso_chieu_chieu_5 = False
            self.thoigian_chieu_5 = 0
            self.giaovien_chieu_5 = None

    @api.onchange('ngaynghi_5')
    def onchange_method_ngaynghi_5(self):
        if self.ngaynghi_5 == True:
            self.cangay_5 = False
            self.noidung_sang_5 = None
            self.noidung_chieu_5 = None
            self.noidung_sang_char_5 = '休日'
            self.noidung_chieu_char_5 = '休日'
            self.coso_sang_5 = False
            self.coso_chieu_5 = False
            self.cosao_daotao_a_5 = False
            self.cosao_daotao_b_5 = False
            self.cosao_daotao_c_5 = False
            self.thoigian_5 = 0
            self.giaovien_5 = None
            self.noidung_chieu_chieu_5 = None
            self.coso_chieu_chieu_5 = False
            self.coso_chieu_chieu_a_5 = False
            self.coso_chieu_chieu_b_5 = False
            self.coso_chieu_chieu_c_5 = False
            self.thoigian_chieu_5 = 0
            self.giaovien_chieu_5 = None
        else:
            self.cangay_5 = False
            self.noidung_sang_5 = None
            self.noidung_chieu_5 = None
            self.noidung_sang_char_5 = ''
            self.noidung_chieu_char_5 = ''
            self.coso_sang_5 = False
            self.coso_chieu_5 = False
            self.cosao_daotao_a_5 = False
            self.cosao_daotao_b_5 = False
            self.cosao_daotao_c_5 = False
            self.thoigian_5 = 0
            self.giaovien_5 = None
            self.noidung_chieu_chieu_5 = None
            self.coso_chieu_chieu_5 = False
            self.coso_chieu_chieu_a_5 = False
            self.coso_chieu_chieu_b_5 = False
            self.coso_chieu_chieu_c_5 = False
            self.thoigian_chieu_5 = 0
            self.giaovien_chieu_5 = None

    @api.onchange('noidung_sang_5', 'noidung_chieu_5')
    def onchange_method_noidung_sang_5(self):
        if self.noidung_sang_5 and self.noidung_chieu_5:
            if self.noidung_sang_5.noidungdaotao != '休日' and self.noidung_chieu_5.noidungdaotao != '休日':
                self.thoigian_5 = 8
            elif self.noidung_sang_5.noidungdaotao != '休日' or self.noidung_chieu_5.noidungdaotao != '休日':
                self.thoigian_5 = 4
            else:
                self.thoigian_5 = 0
        elif self.noidung_sang_5 or self.noidung_chieu_5:
            if self.noidung_sang_5.noidungdaotao != '休日' and self.noidung_sang_5.noidungdaotao:
                self.thoigian_5 = 4
            elif self.noidung_chieu_5.noidungdaotao != '休日' and self.noidung_chieu_5.noidungdaotao:
                self.thoigian_5 = 4
            else:
                self.thoigian_5 = 0
        else:
            self.thoigian_5 = 0

    @api.onchange('noidung_chieu_chieu_5')
    def onchange_method_noidung_chieu_chieu_5(self):
        if self.noidung_chieu_chieu_5:
            if self.noidung_chieu_chieu_5.noidungdaotao != '休日':
                self.thoigian_chieu_5 = 4
        else:
            self.thoigian_chieu_5 = 0

    # Ngày _6
    # Ca sáng
    lich_laplich_6 = fields.Date()
    thang_6 = fields.Char(compute='_thang_6')
    ngay_6 = fields.Char(compute='_thang_6')
    thu_6 = fields.Char(compute='_thang_6')
    ngaynghi_6 = fields.Boolean()
    cangay_6 = fields.Boolean()
    noidung_sang_6 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_6 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_6 = fields.Char()
    noidung_chieu_char_6 = fields.Char()
    coso_sang_6 = fields.Boolean()
    coso_chieu_6 = fields.Boolean()
    cosao_daotao_a_6 = fields.Boolean(string='①')
    cosao_daotao_b_6 = fields.Boolean(string='②')
    cosao_daotao_c_6 = fields.Boolean(string='③')
    thoigian_6 = fields.Integer()
    giaovien_6 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_6 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_6 = fields.Boolean()
    coso_chieu_chieu_a_6 = fields.Boolean(string='①')
    coso_chieu_chieu_b_6 = fields.Boolean(string='②')
    coso_chieu_chieu_c_6 = fields.Boolean(string='③')
    thoigian_chieu_6 = fields.Integer()
    giaovien_chieu_6 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_6')
    def onchange_method_coso_chieu_chieu_a_6(self):
        if self.coso_chieu_chieu_a_6 == True:
            self.coso_chieu_chieu_b_6 = False
            self.coso_chieu_chieu_c_6 = False

    @api.onchange('coso_chieu_chieu_b_6')
    def onchange_method_coso_chieu_chieu_b_6(self):
        if self.coso_chieu_chieu_b_6 == True:
            self.coso_chieu_chieu_a_6 = False
            self.coso_chieu_chieu_c_6 = False

    @api.onchange('coso_chieu_chieu_c_6')
    def onchange_method_coso_chieu_chieu_c_6(self):
        if self.coso_chieu_chieu_c_6 == True:
            self.coso_chieu_chieu_a_6 = False
            self.coso_chieu_chieu_b_6 = False

    @api.onchange('cosao_daotao_a_6')
    def onchange_method_cosao_daotao_a_6(self):
        if self.cosao_daotao_a_6 == True:
            self.cosao_daotao_b_6 = False
            self.cosao_daotao_c_6 = False

    @api.onchange('cosao_daotao_b_6')
    def onchange_method_cosao_daotao_b_6(self):
        if self.cosao_daotao_b_6 == True:
            self.cosao_daotao_a_6 = False
            self.cosao_daotao_c_6 = False

    @api.onchange('cosao_daotao_c_6')
    def onchange_method_cosao_daotao_c_6(self):
        if self.cosao_daotao_c_6 == True:
            self.cosao_daotao_a_6 = False
            self.cosao_daotao_b_6 = False

    @api.one
    @api.depends('lich_laplich_6')
    def _thang_6(self):
        if self.lich_laplich_6:
            self.thang_6 = self.lich_laplich_6.month
            self.ngay_6 = self.lich_laplich_6.day
            self.thu_6 = convert_th(self.lich_laplich_6)
        else:
            self.thang_6 = ''
            self.ngay_6 = ''
            self.thu_6 = ''

    @api.onchange('cangay_6')
    def onchange_method_cangay_6(self):
        if self.cangay_6 == True:
            self.noidung_chieu_6 = None
            self.coso_chieu_6 = False
        else:
            self.coso_chieu_chieu_a_6 = False
            self.coso_chieu_chieu_b_6 = False
            self.coso_chieu_chieu_c_6 = False
            self.noidung_chieu_chieu_6 = None
            self.coso_chieu_chieu_6 = False
            self.thoigian_chieu_6 = 0
            self.giaovien_chieu_6 = None

    @api.onchange('ngaynghi_6')
    def onchange_method_ngaynghi_6(self):
        if self.ngaynghi_6 == True:
            self.cangay_6 = False
            self.noidung_sang_6 = None
            self.noidung_chieu_6 = None
            self.noidung_sang_char_6 = '休日'
            self.noidung_chieu_char_6 = '休日'
            self.coso_sang_6 = False
            self.coso_chieu_6 = False
            self.cosao_daotao_a_6 = False
            self.cosao_daotao_b_6 = False
            self.cosao_daotao_c_6 = False
            self.thoigian_6 = 0
            self.giaovien_6 = None
            self.noidung_chieu_chieu_6 = None
            self.coso_chieu_chieu_6 = False
            self.coso_chieu_chieu_a_6 = False
            self.coso_chieu_chieu_b_6 = False
            self.coso_chieu_chieu_c_6 = False
            self.thoigian_chieu_6 = 0
            self.giaovien_chieu_6 = None
        else:
            self.cangay_6 = False
            self.noidung_sang_6 = None
            self.noidung_chieu_6 = None
            self.noidung_sang_char_6 = ''
            self.noidung_chieu_char_6 = ''
            self.coso_sang_6 = False
            self.coso_chieu_6 = False
            self.cosao_daotao_a_6 = False
            self.cosao_daotao_b_6 = False
            self.cosao_daotao_c_6 = False
            self.thoigian_6 = 0
            self.giaovien_6 = None
            self.noidung_chieu_chieu_6 = None
            self.coso_chieu_chieu_6 = False
            self.coso_chieu_chieu_a_6 = False
            self.coso_chieu_chieu_b_6 = False
            self.coso_chieu_chieu_c_6 = False
            self.thoigian_chieu_6 = 0
            self.giaovien_chieu_6 = None

    @api.onchange('noidung_sang_6', 'noidung_chieu_6')
    def onchange_method_noidung_sang_6(self):
        if self.noidung_sang_6 and self.noidung_chieu_6:
            if self.noidung_sang_6.noidungdaotao != '休日' and self.noidung_chieu_6.noidungdaotao != '休日':
                self.thoigian_6 = 8
            elif self.noidung_sang_6.noidungdaotao != '休日' or self.noidung_chieu_6.noidungdaotao != '休日':
                self.thoigian_6 = 4
            else:
                self.thoigian_6 = 0
        elif self.noidung_sang_6 or self.noidung_chieu_6:
            if self.noidung_sang_6.noidungdaotao != '休日' and self.noidung_sang_6.noidungdaotao:
                self.thoigian_6 = 4
            elif self.noidung_chieu_6.noidungdaotao != '休日' and self.noidung_chieu_6.noidungdaotao:
                self.thoigian_6 = 4
            else:
                self.thoigian_6 = 0
        else:
            self.thoigian_6 = 0

    @api.onchange('noidung_chieu_chieu_6')
    def onchange_method_noidung_chieu_chieu_6(self):
        if self.noidung_chieu_chieu_6:
            if self.noidung_chieu_chieu_6.noidungdaotao != '休日':
                self.thoigian_chieu_6 = 4
        else:
            self.thoigian_chieu_6 = 0

    # Ngày _7
    # Ca sáng
    lich_laplich_7 = fields.Date()
    thang_7 = fields.Char(compute='_thang_7')
    ngay_7 = fields.Char(compute='_thang_7')
    thu_7 = fields.Char(compute='_thang_7')
    ngaynghi_7 = fields.Boolean()
    cangay_7 = fields.Boolean()
    noidung_sang_7 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_7 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_7 = fields.Char()
    noidung_chieu_char_7 = fields.Char()
    coso_sang_7 = fields.Boolean()
    coso_chieu_7 = fields.Boolean()
    cosao_daotao_a_7 = fields.Boolean(string='①')
    cosao_daotao_b_7 = fields.Boolean(string='②')
    cosao_daotao_c_7 = fields.Boolean(string='③')
    thoigian_7 = fields.Integer()
    giaovien_7 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_7 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_7 = fields.Boolean()
    coso_chieu_chieu_a_7 = fields.Boolean(string='①')
    coso_chieu_chieu_b_7 = fields.Boolean(string='②')
    coso_chieu_chieu_c_7 = fields.Boolean(string='③')
    thoigian_chieu_7 = fields.Integer()
    giaovien_chieu_7 = fields.Many2one(comodel_name='giangvien.giangvien')
    @api.onchange('coso_chieu_chieu_a_7')
    def onchange_method_coso_chieu_chieu_a_7(self):
        if self.coso_chieu_chieu_a_7 == True:
            self.coso_chieu_chieu_b_7 = False
            self.coso_chieu_chieu_c_7 = False
    @api.onchange('coso_chieu_chieu_b_7')
    def onchange_method_coso_chieu_chieu_b_7(self):
        if self.coso_chieu_chieu_b_7 == True:
            self.coso_chieu_chieu_a_7 = False
            self.coso_chieu_chieu_c_7 = False
    @api.onchange('coso_chieu_chieu_c_7')
    def onchange_method_coso_chieu_chieu_c_7(self):
        if self.coso_chieu_chieu_c_7 == True:
            self.coso_chieu_chieu_a_7 = False
            self.coso_chieu_chieu_b_7 = False
    @api.onchange('cosao_daotao_a_7')
    def onchange_method_cosao_daotao_a_7(self):
        if self.cosao_daotao_a_7 == True:
            self.cosao_daotao_b_7 = False
            self.cosao_daotao_c_7 = False
    @api.onchange('cosao_daotao_b_7')
    def onchange_method_cosao_daotao_b_7(self):
        if self.cosao_daotao_b_7 == True:
            self.cosao_daotao_a_7 = False
            self.cosao_daotao_c_7 = False
    @api.onchange('cosao_daotao_c_7')
    def onchange_method_cosao_daotao_c_7(self):
        if self.cosao_daotao_c_7 == True:
            self.cosao_daotao_a_7 = False
            self.cosao_daotao_b_7 = False
    @api.one
    @api.depends('lich_laplich_7')
    def _thang_7(self):
        if self.lich_laplich_7:
            self.thang_7 = self.lich_laplich_7.month
            self.ngay_7 = self.lich_laplich_7.day
            self.thu_7 = convert_th(self.lich_laplich_7)
        else:
            self.thang_7 = ''
            self.ngay_7 = ''
            self.thu_7 = ''
    @api.onchange('cangay_7')
    def onchange_method_cangay_7(self):
        if self.cangay_7 == True:
            self.noidung_chieu_7 = None
            self.coso_chieu_7 = False
        else:
            self.coso_chieu_chieu_a_7 = False
            self.coso_chieu_chieu_b_7 = False
            self.coso_chieu_chieu_c_7 = False
            self.noidung_chieu_chieu_7 = None
            self.coso_chieu_chieu_7 = False
            self.thoigian_chieu_7 = 0
            self.giaovien_chieu_7 = None
    @api.onchange('ngaynghi_7')
    def onchange_method_ngaynghi_7(self):
        if self.ngaynghi_7 == True:
            self.cangay_7 = False
            self.noidung_sang_7 = None
            self.noidung_chieu_7 = None
            self.noidung_sang_char_7 = '休日'
            self.noidung_chieu_char_7 = '休日'
            self.coso_sang_7 = False
            self.coso_chieu_7 = False
            self.cosao_daotao_a_7 = False
            self.cosao_daotao_b_7 = False
            self.cosao_daotao_c_7 = False
            self.thoigian_7 = 0
            self.giaovien_7 = None
            self.noidung_chieu_chieu_7 = None
            self.coso_chieu_chieu_7 = False
            self.coso_chieu_chieu_a_7 = False
            self.coso_chieu_chieu_b_7 = False
            self.coso_chieu_chieu_c_7 = False
            self.thoigian_chieu_7 = 0
            self.giaovien_chieu_7 = None
        else:
            self.cangay_7 = False
            self.noidung_sang_7 = None
            self.noidung_chieu_7 = None
            self.noidung_sang_char_7 = ''
            self.noidung_chieu_char_7 = ''
            self.coso_sang_7 = False
            self.coso_chieu_7 = False
            self.cosao_daotao_a_7 = False
            self.cosao_daotao_b_7 = False
            self.cosao_daotao_c_7 = False
            self.thoigian_7 = 0
            self.giaovien_7 = None
            self.noidung_chieu_chieu_7 = None
            self.coso_chieu_chieu_7 = False
            self.coso_chieu_chieu_a_7 = False
            self.coso_chieu_chieu_b_7 = False
            self.coso_chieu_chieu_c_7 = False
            self.thoigian_chieu_7 = 0
            self.giaovien_chieu_7 = None
    @api.onchange('noidung_sang_7', 'noidung_chieu_7')
    def onchange_method_noidung_sang_7(self):
        if self.noidung_sang_7 and self.noidung_chieu_7:
            if self.noidung_sang_7.noidungdaotao != '休日' and self.noidung_chieu_7.noidungdaotao != '休日':
                self.thoigian_7 = 8
            elif self.noidung_sang_7.noidungdaotao != '休日' or self.noidung_chieu_7.noidungdaotao != '休日':
                self.thoigian_7 = 4
            else:
                self.thoigian_7 = 0
        elif self.noidung_sang_7 or self.noidung_chieu_7:
            if self.noidung_sang_7.noidungdaotao != '休日' and self.noidung_sang_7.noidungdaotao:
                self.thoigian_7 = 4
            elif self.noidung_chieu_7.noidungdaotao != '休日' and self.noidung_chieu_7.noidungdaotao:
                self.thoigian_7 = 4
            else:
                self.thoigian_7 = 0
        else:
            self.thoigian_7 = 0
    @api.onchange('noidung_chieu_chieu_7')
    def onchange_method_noidung_chieu_chieu_7(self):
        if self.noidung_chieu_chieu_7:
            if self.noidung_chieu_chieu_7.noidungdaotao != '休日':
                self.thoigian_chieu_7 = 4
        else:
            self.thoigian_chieu_7 = 0

    # Ngày _8
    # Ca sáng
    lich_laplich_8 = fields.Date()
    thang_8 = fields.Char(compute='_thang_8')
    ngay_8 = fields.Char(compute='_thang_8')
    thu_8 = fields.Char(compute='_thang_8')
    ngaynghi_8 = fields.Boolean()
    cangay_8 = fields.Boolean()
    noidung_sang_8 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_8 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_8 = fields.Char()
    noidung_chieu_char_8 = fields.Char()
    coso_sang_8 = fields.Boolean()
    coso_chieu_8 = fields.Boolean()
    cosao_daotao_a_8 = fields.Boolean(string='①')
    cosao_daotao_b_8 = fields.Boolean(string='②')
    cosao_daotao_c_8 = fields.Boolean(string='③')
    thoigian_8 = fields.Integer()
    giaovien_8 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_8 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_8 = fields.Boolean()
    coso_chieu_chieu_a_8 = fields.Boolean(string='①')
    coso_chieu_chieu_b_8 = fields.Boolean(string='②')
    coso_chieu_chieu_c_8 = fields.Boolean(string='③')
    thoigian_chieu_8 = fields.Integer()
    giaovien_chieu_8 = fields.Many2one(comodel_name='giangvien.giangvien')
    @api.onchange('coso_chieu_chieu_a_8')
    def onchange_method_coso_chieu_chieu_a_8(self):
        if self.coso_chieu_chieu_a_8 == True:
            self.coso_chieu_chieu_b_8 = False
            self.coso_chieu_chieu_c_8 = False
    @api.onchange('coso_chieu_chieu_b_8')
    def onchange_method_coso_chieu_chieu_b_8(self):
        if self.coso_chieu_chieu_b_8 == True:
            self.coso_chieu_chieu_a_8 = False
            self.coso_chieu_chieu_c_8 = False
    @api.onchange('coso_chieu_chieu_c_8')
    def onchange_method_coso_chieu_chieu_c_8(self):
        if self.coso_chieu_chieu_c_8 == True:
            self.coso_chieu_chieu_a_8 = False
            self.coso_chieu_chieu_b_8 = False
    @api.onchange('cosao_daotao_a_8')
    def onchange_method_cosao_daotao_a_8(self):
        if self.cosao_daotao_a_8 == True:
            self.cosao_daotao_b_8 = False
            self.cosao_daotao_c_8 = False
    @api.onchange('cosao_daotao_b_8')
    def onchange_method_cosao_daotao_b_8(self):
        if self.cosao_daotao_b_8 == True:
            self.cosao_daotao_a_8 = False
            self.cosao_daotao_c_8 = False
    @api.onchange('cosao_daotao_c_8')
    def onchange_method_cosao_daotao_c_8(self):
        if self.cosao_daotao_c_8 == True:
            self.cosao_daotao_a_8 = False
            self.cosao_daotao_b_8 = False
    @api.one
    @api.depends('lich_laplich_8')
    def _thang_8(self):
        if self.lich_laplich_8:
            self.thang_8 = self.lich_laplich_8.month
            self.ngay_8 = self.lich_laplich_8.day
            self.thu_8 = convert_th(self.lich_laplich_8)
        else:
            self.thang_8 = ''
            self.ngay_8 = ''
            self.thu_8 = ''
    @api.onchange('cangay_8')
    def onchange_method_cangay_8(self):
        if self.cangay_8 == True:
            self.noidung_chieu_8 = None
            self.coso_chieu_8 = False
        else:
            self.coso_chieu_chieu_a_8 = False
            self.coso_chieu_chieu_b_8 = False
            self.coso_chieu_chieu_c_8 = False
            self.noidung_chieu_chieu_8 = None
            self.coso_chieu_chieu_8 = False
            self.thoigian_chieu_8 = 0
            self.giaovien_chieu_8 = None
    @api.onchange('ngaynghi_8')
    def onchange_method_ngaynghi_8(self):
        if self.ngaynghi_8 == True:
            self.cangay_8 = False
            self.noidung_sang_8 = None
            self.noidung_chieu_8 = None
            self.noidung_sang_char_8 = '休日'
            self.noidung_chieu_char_8 = '休日'
            self.coso_sang_8 = False
            self.coso_chieu_8 = False
            self.cosao_daotao_a_8 = False
            self.cosao_daotao_b_8 = False
            self.cosao_daotao_c_8 = False
            self.thoigian_8 = 0
            self.giaovien_8 = None
            self.noidung_chieu_chieu_8 = None
            self.coso_chieu_chieu_8 = False
            self.coso_chieu_chieu_a_8 = False
            self.coso_chieu_chieu_b_8 = False
            self.coso_chieu_chieu_c_8 = False
            self.thoigian_chieu_8 = 0
            self.giaovien_chieu_8 = None
        else:
            self.cangay_8 = False
            self.noidung_sang_8 = None
            self.noidung_chieu_8 = None
            self.noidung_sang_char_8 = ''
            self.noidung_chieu_char_8 = ''
            self.coso_sang_8 = False
            self.coso_chieu_8 = False
            self.cosao_daotao_a_8 = False
            self.cosao_daotao_b_8 = False
            self.cosao_daotao_c_8 = False
            self.thoigian_8 = 0
            self.giaovien_8 = None
            self.noidung_chieu_chieu_8 = None
            self.coso_chieu_chieu_8 = False
            self.coso_chieu_chieu_a_8 = False
            self.coso_chieu_chieu_b_8 = False
            self.coso_chieu_chieu_c_8 = False
            self.thoigian_chieu_8 = 0
            self.giaovien_chieu_8 = None
    @api.onchange('noidung_sang_8', 'noidung_chieu_8')
    def onchange_method_noidung_sang_8(self):
        if self.noidung_sang_8 and self.noidung_chieu_8:
            if self.noidung_sang_8.noidungdaotao != '休日' and self.noidung_chieu_8.noidungdaotao != '休日':
                self.thoigian_8 = 8
            elif self.noidung_sang_8.noidungdaotao != '休日' or self.noidung_chieu_8.noidungdaotao != '休日':
                self.thoigian_8 = 4
            else:
                self.thoigian_8 = 0
        elif self.noidung_sang_8 or self.noidung_chieu_8:
            if self.noidung_sang_8.noidungdaotao != '休日' and self.noidung_sang_8.noidungdaotao:
                self.thoigian_8 = 4
            elif self.noidung_chieu_8.noidungdaotao != '休日' and self.noidung_chieu_8.noidungdaotao:
                self.thoigian_8 = 4
            else:
                self.thoigian_8 = 0
        else:
            self.thoigian_8 = 0
    @api.onchange('noidung_chieu_chieu_8')
    def onchange_method_noidung_chieu_chieu_8(self):
        if self.noidung_chieu_chieu_8:
            if self.noidung_chieu_chieu_8.noidungdaotao != '休日':
                self.thoigian_chieu_8 = 4
        else:
            self.thoigian_chieu_8 = 0

    # Ngày _9
    # Ca sáng
    lich_laplich_9 = fields.Date()
    thang_9 = fields.Char(compute='_thang_9')
    ngay_9 = fields.Char(compute='_thang_9')
    thu_9 = fields.Char(compute='_thang_9')
    ngaynghi_9 = fields.Boolean()
    cangay_9 = fields.Boolean()
    noidung_sang_9 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_9 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_9 = fields.Char()
    noidung_chieu_char_9 = fields.Char()
    coso_sang_9 = fields.Boolean()
    coso_chieu_9 = fields.Boolean()
    cosao_daotao_a_9 = fields.Boolean(string='①')
    cosao_daotao_b_9 = fields.Boolean(string='②')
    cosao_daotao_c_9 = fields.Boolean(string='③')
    thoigian_9 = fields.Integer()
    giaovien_9 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_9 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_9 = fields.Boolean()
    coso_chieu_chieu_a_9 = fields.Boolean(string='①')
    coso_chieu_chieu_b_9 = fields.Boolean(string='②')
    coso_chieu_chieu_c_9 = fields.Boolean(string='③')
    thoigian_chieu_9 = fields.Integer()
    giaovien_chieu_9 = fields.Many2one(comodel_name='giangvien.giangvien')
    @api.onchange('coso_chieu_chieu_a_9')
    def onchange_method_coso_chieu_chieu_a_9(self):
        if self.coso_chieu_chieu_a_9 == True:
            self.coso_chieu_chieu_b_9 = False
            self.coso_chieu_chieu_c_9 = False
    @api.onchange('coso_chieu_chieu_b_9')
    def onchange_method_coso_chieu_chieu_b_9(self):
        if self.coso_chieu_chieu_b_9 == True:
            self.coso_chieu_chieu_a_9 = False
            self.coso_chieu_chieu_c_9 = False
    @api.onchange('coso_chieu_chieu_c_9')
    def onchange_method_coso_chieu_chieu_c_9(self):
        if self.coso_chieu_chieu_c_9 == True:
            self.coso_chieu_chieu_a_9 = False
            self.coso_chieu_chieu_b_9 = False
    @api.onchange('cosao_daotao_a_9')
    def onchange_method_cosao_daotao_a_9(self):
        if self.cosao_daotao_a_9 == True:
            self.cosao_daotao_b_9 = False
            self.cosao_daotao_c_9 = False
    @api.onchange('cosao_daotao_b_9')
    def onchange_method_cosao_daotao_b_9(self):
        if self.cosao_daotao_b_9 == True:
            self.cosao_daotao_a_9 = False
            self.cosao_daotao_c_9 = False
    @api.onchange('cosao_daotao_c_9')
    def onchange_method_cosao_daotao_c_9(self):
        if self.cosao_daotao_c_9 == True:
            self.cosao_daotao_a_9 = False
            self.cosao_daotao_b_9 = False
    @api.one
    @api.depends('lich_laplich_9')
    def _thang_9(self):
        if self.lich_laplich_9:
            self.thang_9 = self.lich_laplich_9.month
            self.ngay_9 = self.lich_laplich_9.day
            self.thu_9 = convert_th(self.lich_laplich_9)
        else:
            self.thang_9 = ''
            self.ngay_9 = ''
            self.thu_9 = ''
    @api.onchange('cangay_9')
    def onchange_method_cangay_9(self):
        if self.cangay_9 == True:
            self.noidung_chieu_9 = None
            self.coso_chieu_9 = False
        else:
            self.coso_chieu_chieu_a_9 = False
            self.coso_chieu_chieu_b_9 = False
            self.coso_chieu_chieu_c_9 = False
            self.noidung_chieu_chieu_9 = None
            self.coso_chieu_chieu_9 = False
            self.thoigian_chieu_9 = 0
            self.giaovien_chieu_9 = None
    @api.onchange('ngaynghi_9')
    def onchange_method_ngaynghi_9(self):
        if self.ngaynghi_9 == True:
            self.cangay_9 = False
            self.noidung_sang_9 = None
            self.noidung_chieu_9 = None
            self.noidung_sang_char_9 = '休日'
            self.noidung_chieu_char_9 = '休日'
            self.coso_sang_9 = False
            self.coso_chieu_9 = False
            self.cosao_daotao_a_9 = False
            self.cosao_daotao_b_9 = False
            self.cosao_daotao_c_9 = False
            self.thoigian_9 = 0
            self.giaovien_9 = None
            self.noidung_chieu_chieu_9 = None
            self.coso_chieu_chieu_9 = False
            self.coso_chieu_chieu_a_9 = False
            self.coso_chieu_chieu_b_9 = False
            self.coso_chieu_chieu_c_9 = False
            self.thoigian_chieu_9 = 0
            self.giaovien_chieu_9 = None
        else:
            self.cangay_9 = False
            self.noidung_sang_9 = None
            self.noidung_chieu_9 = None
            self.noidung_sang_char_9 = ''
            self.noidung_chieu_char_9 = ''
            self.coso_sang_9 = False
            self.coso_chieu_9 = False
            self.cosao_daotao_a_9 = False
            self.cosao_daotao_b_9 = False
            self.cosao_daotao_c_9 = False
            self.thoigian_9 = 0
            self.giaovien_9 = None
            self.noidung_chieu_chieu_9 = None
            self.coso_chieu_chieu_9 = False
            self.coso_chieu_chieu_a_9 = False
            self.coso_chieu_chieu_b_9 = False
            self.coso_chieu_chieu_c_9 = False
            self.thoigian_chieu_9 = 0
            self.giaovien_chieu_9 = None
    @api.onchange('noidung_sang_9', 'noidung_chieu_9')
    def onchange_method_noidung_sang_9(self):
        if self.noidung_sang_9 and self.noidung_chieu_9:
            if self.noidung_sang_9.noidungdaotao != '休日' and self.noidung_chieu_9.noidungdaotao != '休日':
                self.thoigian_9 = 8
            elif self.noidung_sang_9.noidungdaotao != '休日' or self.noidung_chieu_9.noidungdaotao != '休日':
                self.thoigian_9 = 4
            else:
                self.thoigian_9 = 0
        elif self.noidung_sang_9 or self.noidung_chieu_9:
            if self.noidung_sang_9.noidungdaotao != '休日' and self.noidung_sang_9.noidungdaotao:
                self.thoigian_9 = 4
            elif self.noidung_chieu_9.noidungdaotao != '休日' and self.noidung_chieu_9.noidungdaotao:
                self.thoigian_9 = 4
            else:
                self.thoigian_9 = 0
        else:
            self.thoigian_9 = 0
    @api.onchange('noidung_chieu_chieu_9')
    def onchange_method_noidung_chieu_chieu_9(self):
        if self.noidung_chieu_chieu_9:
            if self.noidung_chieu_chieu_9.noidungdaotao != '休日':
                self.thoigian_chieu_9 = 4
        else:
            self.thoigian_chieu_9 = 0

    # Ngày _10
    # Ca sáng
    lich_laplich_10 = fields.Date()
    thang_10 = fields.Char(compute='_thang_10')
    ngay_10 = fields.Char(compute='_thang_10')
    thu_10 = fields.Char(compute='_thang_10')
    ngaynghi_10 = fields.Boolean()
    cangay_10 = fields.Boolean()
    noidung_sang_10 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_10 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_10 = fields.Char()
    noidung_chieu_char_10 = fields.Char()
    coso_sang_10 = fields.Boolean()
    coso_chieu_10 = fields.Boolean()
    cosao_daotao_a_10 = fields.Boolean(string='①')
    cosao_daotao_b_10 = fields.Boolean(string='②')
    cosao_daotao_c_10 = fields.Boolean(string='③')
    thoigian_10 = fields.Integer()
    giaovien_10 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_10 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_10 = fields.Boolean()
    coso_chieu_chieu_a_10 = fields.Boolean(string='①')
    coso_chieu_chieu_b_10 = fields.Boolean(string='②')
    coso_chieu_chieu_c_10 = fields.Boolean(string='③')
    thoigian_chieu_10 = fields.Integer()
    giaovien_chieu_10 = fields.Many2one(comodel_name='giangvien.giangvien')
    @api.onchange('coso_chieu_chieu_a_10')
    def onchange_method_coso_chieu_chieu_a_10(self):
        if self.coso_chieu_chieu_a_10 == True:
            self.coso_chieu_chieu_b_10 = False
            self.coso_chieu_chieu_c_10 = False
    @api.onchange('coso_chieu_chieu_b_10')
    def onchange_method_coso_chieu_chieu_b_10(self):
        if self.coso_chieu_chieu_b_10 == True:
            self.coso_chieu_chieu_a_10 = False
            self.coso_chieu_chieu_c_10 = False
    @api.onchange('coso_chieu_chieu_c_10')
    def onchange_method_coso_chieu_chieu_c_10(self):
        if self.coso_chieu_chieu_c_10 == True:
            self.coso_chieu_chieu_a_10 = False
            self.coso_chieu_chieu_b_10 = False
    @api.onchange('cosao_daotao_a_10')
    def onchange_method_cosao_daotao_a_10(self):
        if self.cosao_daotao_a_10 == True:
            self.cosao_daotao_b_10 = False
            self.cosao_daotao_c_10 = False
    @api.onchange('cosao_daotao_b_10')
    def onchange_method_cosao_daotao_b_10(self):
        if self.cosao_daotao_b_10 == True:
            self.cosao_daotao_a_10 = False
            self.cosao_daotao_c_10 = False
    @api.onchange('cosao_daotao_c_10')
    def onchange_method_cosao_daotao_c_10(self):
        if self.cosao_daotao_c_10 == True:
            self.cosao_daotao_a_10 = False
            self.cosao_daotao_b_10 = False
    @api.one
    @api.depends('lich_laplich_10')
    def _thang_10(self):
        if self.lich_laplich_10:
            self.thang_10 = self.lich_laplich_10.month
            self.ngay_10 = self.lich_laplich_10.day
            self.thu_10 = convert_th(self.lich_laplich_10)
        else:
            self.thang_10 = ''
            self.ngay_10 = ''
            self.thu_10 = ''
    @api.onchange('cangay_10')
    def onchange_method_cangay_10(self):
        if self.cangay_10 == True:
            self.noidung_chieu_10 = None
            self.coso_chieu_10 = False
        else:
            self.coso_chieu_chieu_a_10 = False
            self.coso_chieu_chieu_b_10 = False
            self.coso_chieu_chieu_c_10 = False
            self.noidung_chieu_chieu_10 = None
            self.coso_chieu_chieu_10 = False
            self.thoigian_chieu_10 = 0
            self.giaovien_chieu_10 = None
    @api.onchange('ngaynghi_10')
    def onchange_method_ngaynghi_10(self):
        if self.ngaynghi_10 == True:
            self.cangay_10 = False
            self.noidung_sang_10 = None
            self.noidung_chieu_10 = None
            self.noidung_sang_char_10 = '休日'
            self.noidung_chieu_char_10 = '休日'
            self.coso_sang_10 = False
            self.coso_chieu_10 = False
            self.cosao_daotao_a_10 = False
            self.cosao_daotao_b_10 = False
            self.cosao_daotao_c_10 = False
            self.thoigian_10 = 0
            self.giaovien_10 = None
            self.noidung_chieu_chieu_10 = None
            self.coso_chieu_chieu_10 = False
            self.coso_chieu_chieu_a_10 = False
            self.coso_chieu_chieu_b_10 = False
            self.coso_chieu_chieu_c_10 = False
            self.thoigian_chieu_10 = 0
            self.giaovien_chieu_10 = None
        else:
            self.cangay_10 = False
            self.noidung_sang_10 = None
            self.noidung_chieu_10 = None
            self.noidung_sang_char_10 = ''
            self.noidung_chieu_char_10 = ''
            self.coso_sang_10 = False
            self.coso_chieu_10 = False
            self.cosao_daotao_a_10 = False
            self.cosao_daotao_b_10 = False
            self.cosao_daotao_c_10 = False
            self.thoigian_10 = 0
            self.giaovien_10 = None
            self.noidung_chieu_chieu_10 = None
            self.coso_chieu_chieu_10 = False
            self.coso_chieu_chieu_a_10 = False
            self.coso_chieu_chieu_b_10 = False
            self.coso_chieu_chieu_c_10 = False
            self.thoigian_chieu_10 = 0
            self.giaovien_chieu_10 = None
    @api.onchange('noidung_sang_10', 'noidung_chieu_10')
    def onchange_method_noidung_sang_10(self):
        if self.noidung_sang_10 and self.noidung_chieu_10:
            if self.noidung_sang_10.noidungdaotao != '休日' and self.noidung_chieu_10.noidungdaotao != '休日':
                self.thoigian_10 = 8
            elif self.noidung_sang_10.noidungdaotao != '休日' or self.noidung_chieu_10.noidungdaotao != '休日':
                self.thoigian_10 = 4
            else:
                self.thoigian_10 = 0
        elif self.noidung_sang_10 or self.noidung_chieu_10:
            if self.noidung_sang_10.noidungdaotao != '休日' and self.noidung_sang_10.noidungdaotao:
                self.thoigian_10 = 4
            elif self.noidung_chieu_10.noidungdaotao != '休日' and self.noidung_chieu_10.noidungdaotao:
                self.thoigian_10 = 4
            else:
                self.thoigian_10 = 0
        else:
            self.thoigian_10 = 0
    @api.onchange('noidung_chieu_chieu_10')
    def onchange_method_noidung_chieu_chieu_10(self):
        if self.noidung_chieu_chieu_10:
            if self.noidung_chieu_chieu_10.noidungdaotao != '休日':
                self.thoigian_chieu_10 = 4
        else:
            self.thoigian_chieu_10 = 0

    # Ngày _11
    # Ca sáng
    lich_laplich_11 = fields.Date()
    thang_11 = fields.Char(compute='_thang_11')
    ngay_11 = fields.Char(compute='_thang_11')
    thu_11 = fields.Char(compute='_thang_11')
    ngaynghi_11 = fields.Boolean()
    cangay_11 = fields.Boolean()
    noidung_sang_11 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_11 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_11 = fields.Char()
    noidung_chieu_char_11 = fields.Char()
    coso_sang_11 = fields.Boolean()
    coso_chieu_11 = fields.Boolean()
    cosao_daotao_a_11 = fields.Boolean(string='①')
    cosao_daotao_b_11 = fields.Boolean(string='②')
    cosao_daotao_c_11 = fields.Boolean(string='③')
    thoigian_11 = fields.Integer()
    giaovien_11 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_11 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_11 = fields.Boolean()
    coso_chieu_chieu_a_11 = fields.Boolean(string='①')
    coso_chieu_chieu_b_11 = fields.Boolean(string='②')
    coso_chieu_chieu_c_11 = fields.Boolean(string='③')
    thoigian_chieu_11 = fields.Integer()
    giaovien_chieu_11 = fields.Many2one(comodel_name='giangvien.giangvien')
    @api.onchange('coso_chieu_chieu_a_11')
    def onchange_method_coso_chieu_chieu_a_11(self):
        if self.coso_chieu_chieu_a_11 == True:
            self.coso_chieu_chieu_b_11 = False
            self.coso_chieu_chieu_c_11 = False
    @api.onchange('coso_chieu_chieu_b_11')
    def onchange_method_coso_chieu_chieu_b_11(self):
        if self.coso_chieu_chieu_b_11 == True:
            self.coso_chieu_chieu_a_11 = False
            self.coso_chieu_chieu_c_11 = False
    @api.onchange('coso_chieu_chieu_c_11')
    def onchange_method_coso_chieu_chieu_c_11(self):
        if self.coso_chieu_chieu_c_11 == True:
            self.coso_chieu_chieu_a_11 = False
            self.coso_chieu_chieu_b_11 = False
    @api.onchange('cosao_daotao_a_11')
    def onchange_method_cosao_daotao_a_11(self):
        if self.cosao_daotao_a_11 == True:
            self.cosao_daotao_b_11 = False
            self.cosao_daotao_c_11 = False
    @api.onchange('cosao_daotao_b_11')
    def onchange_method_cosao_daotao_b_11(self):
        if self.cosao_daotao_b_11 == True:
            self.cosao_daotao_a_11 = False
            self.cosao_daotao_c_11 = False
    @api.onchange('cosao_daotao_c_11')
    def onchange_method_cosao_daotao_c_11(self):
        if self.cosao_daotao_c_11 == True:
            self.cosao_daotao_a_11 = False
            self.cosao_daotao_b_11 = False
    @api.one
    @api.depends('lich_laplich_11')
    def _thang_11(self):
        if self.lich_laplich_11:
            self.thang_11 = self.lich_laplich_11.month
            self.ngay_11 = self.lich_laplich_11.day
            self.thu_11 = convert_th(self.lich_laplich_11)
        else:
            self.thang_11 = ''
            self.ngay_11 = ''
            self.thu_11 = ''
    @api.onchange('cangay_11')
    def onchange_method_cangay_11(self):
        if self.cangay_11 == True:
            self.noidung_chieu_11 = None
            self.coso_chieu_11 = False
        else:
            self.coso_chieu_chieu_a_11 = False
            self.coso_chieu_chieu_b_11 = False
            self.coso_chieu_chieu_c_11 = False
            self.noidung_chieu_chieu_11 = None
            self.coso_chieu_chieu_11 = False
            self.thoigian_chieu_11 = 0
            self.giaovien_chieu_11 = None
    @api.onchange('ngaynghi_11')
    def onchange_method_ngaynghi_11(self):
        if self.ngaynghi_11 == True:
            self.cangay_11 = False
            self.noidung_sang_11 = None
            self.noidung_chieu_11 = None
            self.noidung_sang_char_11 = '休日'
            self.noidung_chieu_char_11 = '休日'
            self.coso_sang_11 = False
            self.coso_chieu_11 = False
            self.cosao_daotao_a_11 = False
            self.cosao_daotao_b_11 = False
            self.cosao_daotao_c_11 = False
            self.thoigian_11 = 0
            self.giaovien_11 = None
            self.noidung_chieu_chieu_11 = None
            self.coso_chieu_chieu_11 = False
            self.coso_chieu_chieu_a_11 = False
            self.coso_chieu_chieu_b_11 = False
            self.coso_chieu_chieu_c_11 = False
            self.thoigian_chieu_11 = 0
            self.giaovien_chieu_11 = None
        else:
            self.cangay_11 = False
            self.noidung_sang_11 = None
            self.noidung_chieu_11 = None
            self.noidung_sang_char_11 = ''
            self.noidung_chieu_char_11 = ''
            self.coso_sang_11 = False
            self.coso_chieu_11 = False
            self.cosao_daotao_a_11 = False
            self.cosao_daotao_b_11 = False
            self.cosao_daotao_c_11 = False
            self.thoigian_11 = 0
            self.giaovien_11 = None
            self.noidung_chieu_chieu_11 = None
            self.coso_chieu_chieu_11 = False
            self.coso_chieu_chieu_a_11 = False
            self.coso_chieu_chieu_b_11 = False
            self.coso_chieu_chieu_c_11 = False
            self.thoigian_chieu_11 = 0
            self.giaovien_chieu_11 = None
    @api.onchange('noidung_sang_11', 'noidung_chieu_11')
    def onchange_method_noidung_sang_11(self):
        if self.noidung_sang_11 and self.noidung_chieu_11:
            if self.noidung_sang_11.noidungdaotao != '休日' and self.noidung_chieu_11.noidungdaotao != '休日':
                self.thoigian_11 = 8
            elif self.noidung_sang_11.noidungdaotao != '休日' or self.noidung_chieu_11.noidungdaotao != '休日':
                self.thoigian_11 = 4
            else:
                self.thoigian_11 = 0
        elif self.noidung_sang_11 or self.noidung_chieu_11:
            if self.noidung_sang_11.noidungdaotao != '休日' and self.noidung_sang_11.noidungdaotao:
                self.thoigian_11 = 4
            elif self.noidung_chieu_11.noidungdaotao != '休日' and self.noidung_chieu_11.noidungdaotao:
                self.thoigian_11 = 4
            else:
                self.thoigian_11 = 0
        else:
            self.thoigian_11 = 0
    @api.onchange('noidung_chieu_chieu_11')
    def onchange_method_noidung_chieu_chieu_11(self):
        if self.noidung_chieu_chieu_11:
            if self.noidung_chieu_chieu_11.noidungdaotao != '休日':
                self.thoigian_chieu_11 = 4
        else:
            self.thoigian_chieu_11 = 0

    # Ngày _12
    # Ca sáng
    lich_laplich_12 = fields.Date()
    thang_12 = fields.Char(compute='_thang_12')
    ngay_12 = fields.Char(compute='_thang_12')
    thu_12 = fields.Char(compute='_thang_12')
    ngaynghi_12 = fields.Boolean()
    cangay_12 = fields.Boolean()
    noidung_sang_12 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_12 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_12 = fields.Char()
    noidung_chieu_char_12 = fields.Char()
    coso_sang_12 = fields.Boolean()
    coso_chieu_12 = fields.Boolean()
    cosao_daotao_a_12 = fields.Boolean(string='①')
    cosao_daotao_b_12 = fields.Boolean(string='②')
    cosao_daotao_c_12 = fields.Boolean(string='③')
    thoigian_12 = fields.Integer()
    giaovien_12 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_12 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_12 = fields.Boolean()
    coso_chieu_chieu_a_12 = fields.Boolean(string='①')
    coso_chieu_chieu_b_12 = fields.Boolean(string='②')
    coso_chieu_chieu_c_12 = fields.Boolean(string='③')
    thoigian_chieu_12 = fields.Integer()
    giaovien_chieu_12 = fields.Many2one(comodel_name='giangvien.giangvien')
    @api.onchange('coso_chieu_chieu_a_12')
    def onchange_method_coso_chieu_chieu_a_12(self):
        if self.coso_chieu_chieu_a_12 == True:
            self.coso_chieu_chieu_b_12 = False
            self.coso_chieu_chieu_c_12 = False
    @api.onchange('coso_chieu_chieu_b_12')
    def onchange_method_coso_chieu_chieu_b_12(self):
        if self.coso_chieu_chieu_b_12 == True:
            self.coso_chieu_chieu_a_12 = False
            self.coso_chieu_chieu_c_12 = False
    @api.onchange('coso_chieu_chieu_c_12')
    def onchange_method_coso_chieu_chieu_c_12(self):
        if self.coso_chieu_chieu_c_12 == True:
            self.coso_chieu_chieu_a_12 = False
            self.coso_chieu_chieu_b_12 = False
    @api.onchange('cosao_daotao_a_12')
    def onchange_method_cosao_daotao_a_12(self):
        if self.cosao_daotao_a_12 == True:
            self.cosao_daotao_b_12 = False
            self.cosao_daotao_c_12 = False
    @api.onchange('cosao_daotao_b_12')
    def onchange_method_cosao_daotao_b_12(self):
        if self.cosao_daotao_b_12 == True:
            self.cosao_daotao_a_12 = False
            self.cosao_daotao_c_12 = False
    @api.onchange('cosao_daotao_c_12')
    def onchange_method_cosao_daotao_c_12(self):
        if self.cosao_daotao_c_12 == True:
            self.cosao_daotao_a_12 = False
            self.cosao_daotao_b_12 = False
    @api.one
    @api.depends('lich_laplich_12')
    def _thang_12(self):
        if self.lich_laplich_12:
            self.thang_12 = self.lich_laplich_12.month
            self.ngay_12 = self.lich_laplich_12.day
            self.thu_12 = convert_th(self.lich_laplich_12)
        else:
            self.thang_12 = ''
            self.ngay_12 = ''
            self.thu_12 = ''
    @api.onchange('cangay_12')
    def onchange_method_cangay_12(self):
        if self.cangay_12 == True:
            self.noidung_chieu_12 = None
            self.coso_chieu_12 = False
        else:
            self.coso_chieu_chieu_a_12 = False
            self.coso_chieu_chieu_b_12 = False
            self.coso_chieu_chieu_c_12 = False
            self.noidung_chieu_chieu_12 = None
            self.coso_chieu_chieu_12 = False
            self.thoigian_chieu_12 = 0
            self.giaovien_chieu_12 = None
    @api.onchange('ngaynghi_12')
    def onchange_method_ngaynghi_12(self):
        if self.ngaynghi_12 == True:
            self.cangay_12 = False
            self.noidung_sang_12 = None
            self.noidung_chieu_12 = None
            self.noidung_sang_char_12 = '休日'
            self.noidung_chieu_char_12 = '休日'
            self.coso_sang_12 = False
            self.coso_chieu_12 = False
            self.cosao_daotao_a_12 = False
            self.cosao_daotao_b_12 = False
            self.cosao_daotao_c_12 = False
            self.thoigian_12 = 0
            self.giaovien_12 = None
            self.noidung_chieu_chieu_12 = None
            self.coso_chieu_chieu_12 = False
            self.coso_chieu_chieu_a_12 = False
            self.coso_chieu_chieu_b_12 = False
            self.coso_chieu_chieu_c_12 = False
            self.thoigian_chieu_12 = 0
            self.giaovien_chieu_12 = None
        else:
            self.cangay_12 = False
            self.noidung_sang_12 = None
            self.noidung_chieu_12 = None
            self.noidung_sang_char_12 = ''
            self.noidung_chieu_char_12 = ''
            self.coso_sang_12 = False
            self.coso_chieu_12 = False
            self.cosao_daotao_a_12 = False
            self.cosao_daotao_b_12 = False
            self.cosao_daotao_c_12 = False
            self.thoigian_12 = 0
            self.giaovien_12 = None
            self.noidung_chieu_chieu_12 = None
            self.coso_chieu_chieu_12 = False
            self.coso_chieu_chieu_a_12 = False
            self.coso_chieu_chieu_b_12 = False
            self.coso_chieu_chieu_c_12 = False
            self.thoigian_chieu_12 = 0
            self.giaovien_chieu_12 = None
    @api.onchange('noidung_sang_12', 'noidung_chieu_12')
    def onchange_method_noidung_sang_12(self):
        if self.noidung_sang_12 and self.noidung_chieu_12:
            if self.noidung_sang_12.noidungdaotao != '休日' and self.noidung_chieu_12.noidungdaotao != '休日':
                self.thoigian_12 = 8
            elif self.noidung_sang_12.noidungdaotao != '休日' or self.noidung_chieu_12.noidungdaotao != '休日':
                self.thoigian_12 = 4
            else:
                self.thoigian_12 = 0
        elif self.noidung_sang_12 or self.noidung_chieu_12:
            if self.noidung_sang_12.noidungdaotao != '休日' and self.noidung_sang_12.noidungdaotao:
                self.thoigian_12 = 4
            elif self.noidung_chieu_12.noidungdaotao != '休日' and self.noidung_chieu_12.noidungdaotao:
                self.thoigian_12 = 4
            else:
                self.thoigian_12 = 0
        else:
            self.thoigian_12 = 0
    @api.onchange('noidung_chieu_chieu_12')
    def onchange_method_noidung_chieu_chieu_12(self):
        if self.noidung_chieu_chieu_12:
            if self.noidung_chieu_chieu_12.noidungdaotao != '休日':
                self.thoigian_chieu_12 = 4
        else:
            self.thoigian_chieu_12 = 0


    # Ngày _13
    # Ca sáng
    lich_laplich_13 = fields.Date()
    thang_13 = fields.Char(compute='_thang_13')
    ngay_13 = fields.Char(compute='_thang_13')
    thu_13 = fields.Char(compute='_thang_13')
    ngaynghi_13 = fields.Boolean()
    cangay_13 = fields.Boolean()
    noidung_sang_13 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_13 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_13 = fields.Char()
    noidung_chieu_char_13 = fields.Char()
    coso_sang_13 = fields.Boolean()
    coso_chieu_13 = fields.Boolean()
    cosao_daotao_a_13 = fields.Boolean(string='①')
    cosao_daotao_b_13 = fields.Boolean(string='②')
    cosao_daotao_c_13 = fields.Boolean(string='③')
    thoigian_13 = fields.Integer()
    giaovien_13 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_13 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_13 = fields.Boolean()
    coso_chieu_chieu_a_13 = fields.Boolean(string='①')
    coso_chieu_chieu_b_13 = fields.Boolean(string='②')
    coso_chieu_chieu_c_13 = fields.Boolean(string='③')
    thoigian_chieu_13 = fields.Integer()
    giaovien_chieu_13 = fields.Many2one(comodel_name='giangvien.giangvien')
    @api.onchange('coso_chieu_chieu_a_13')
    def onchange_method_coso_chieu_chieu_a_13(self):
        if self.coso_chieu_chieu_a_13 == True:
            self.coso_chieu_chieu_b_13 = False
            self.coso_chieu_chieu_c_13 = False
    @api.onchange('coso_chieu_chieu_b_13')
    def onchange_method_coso_chieu_chieu_b_13(self):
        if self.coso_chieu_chieu_b_13 == True:
            self.coso_chieu_chieu_a_13 = False
            self.coso_chieu_chieu_c_13 = False
    @api.onchange('coso_chieu_chieu_c_13')
    def onchange_method_coso_chieu_chieu_c_13(self):
        if self.coso_chieu_chieu_c_13 == True:
            self.coso_chieu_chieu_a_13 = False
            self.coso_chieu_chieu_b_13 = False
    @api.onchange('cosao_daotao_a_13')
    def onchange_method_cosao_daotao_a_13(self):
        if self.cosao_daotao_a_13 == True:
            self.cosao_daotao_b_13 = False
            self.cosao_daotao_c_13 = False
    @api.onchange('cosao_daotao_b_13')
    def onchange_method_cosao_daotao_b_13(self):
        if self.cosao_daotao_b_13 == True:
            self.cosao_daotao_a_13 = False
            self.cosao_daotao_c_13 = False
    @api.onchange('cosao_daotao_c_13')
    def onchange_method_cosao_daotao_c_13(self):
        if self.cosao_daotao_c_13 == True:
            self.cosao_daotao_a_13 = False
            self.cosao_daotao_b_13 = False
    @api.one
    @api.depends('lich_laplich_13')
    def _thang_13(self):
        if self.lich_laplich_13:
            self.thang_13 = self.lich_laplich_13.month
            self.ngay_13 = self.lich_laplich_13.day
            self.thu_13 = convert_th(self.lich_laplich_13)
        else:
            self.thang_13 = ''
            self.ngay_13 = ''
            self.thu_13 = ''
    @api.onchange('cangay_13')
    def onchange_method_cangay_13(self):
        if self.cangay_13 == True:
            self.noidung_chieu_13 = None
            self.coso_chieu_13 = False
        else:
            self.coso_chieu_chieu_a_13 = False
            self.coso_chieu_chieu_b_13 = False
            self.coso_chieu_chieu_c_13 = False
            self.noidung_chieu_chieu_13 = None
            self.coso_chieu_chieu_13 = False
            self.thoigian_chieu_13 = 0
            self.giaovien_chieu_13 = None
    @api.onchange('ngaynghi_13')
    def onchange_method_ngaynghi_13(self):
        if self.ngaynghi_13 == True:
            self.cangay_13 = False
            self.noidung_sang_13 = None
            self.noidung_chieu_13 = None
            self.noidung_sang_char_13 = '休日'
            self.noidung_chieu_char_13 = '休日'
            self.coso_sang_13 = False
            self.coso_chieu_13 = False
            self.cosao_daotao_a_13 = False
            self.cosao_daotao_b_13 = False
            self.cosao_daotao_c_13 = False
            self.thoigian_13 = 0
            self.giaovien_13 = None
            self.noidung_chieu_chieu_13 = None
            self.coso_chieu_chieu_13 = False
            self.coso_chieu_chieu_a_13 = False
            self.coso_chieu_chieu_b_13 = False
            self.coso_chieu_chieu_c_13 = False
            self.thoigian_chieu_13 = 0
            self.giaovien_chieu_13 = None
        else:
            self.cangay_13 = False
            self.noidung_sang_13 = None
            self.noidung_chieu_13 = None
            self.noidung_sang_char_13 = ''
            self.noidung_chieu_char_13 = ''
            self.coso_sang_13 = False
            self.coso_chieu_13 = False
            self.cosao_daotao_a_13 = False
            self.cosao_daotao_b_13 = False
            self.cosao_daotao_c_13 = False
            self.thoigian_13 = 0
            self.giaovien_13 = None
            self.noidung_chieu_chieu_13 = None
            self.coso_chieu_chieu_13 = False
            self.coso_chieu_chieu_a_13 = False
            self.coso_chieu_chieu_b_13 = False
            self.coso_chieu_chieu_c_13 = False
            self.thoigian_chieu_13 = 0
            self.giaovien_chieu_13 = None
    @api.onchange('noidung_sang_13', 'noidung_chieu_13')
    def onchange_method_noidung_sang_13(self):
        if self.noidung_sang_13 and self.noidung_chieu_13:
            if self.noidung_sang_13.noidungdaotao != '休日' and self.noidung_chieu_13.noidungdaotao != '休日':
                self.thoigian_13 = 8
            elif self.noidung_sang_13.noidungdaotao != '休日' or self.noidung_chieu_13.noidungdaotao != '休日':
                self.thoigian_13 = 4
            else:
                self.thoigian_13 = 0
        elif self.noidung_sang_13 or self.noidung_chieu_13:
            if self.noidung_sang_13.noidungdaotao != '休日' and self.noidung_sang_13.noidungdaotao:
                self.thoigian_13 = 4
            elif self.noidung_chieu_13.noidungdaotao != '休日' and self.noidung_chieu_13.noidungdaotao:
                self.thoigian_13 = 4
            else:
                self.thoigian_13 = 0
        else:
            self.thoigian_13 = 0
    @api.onchange('noidung_chieu_chieu_13')
    def onchange_method_noidung_chieu_chieu_13(self):
        if self.noidung_chieu_chieu_13:
            if self.noidung_chieu_chieu_13.noidungdaotao != '休日':
                self.thoigian_chieu_13 = 4
        else:
            self.thoigian_chieu_13 = 0

    # Ngày _14
    # Ca sáng
    lich_laplich_14 = fields.Date()
    thang_14 = fields.Char(compute='_thang_14')
    ngay_14 = fields.Char(compute='_thang_14')
    thu_14 = fields.Char(compute='_thang_14')
    ngaynghi_14 = fields.Boolean()
    cangay_14 = fields.Boolean()
    noidung_sang_14 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_14 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_14 = fields.Char()
    noidung_chieu_char_14 = fields.Char()
    coso_sang_14 = fields.Boolean()
    coso_chieu_14 = fields.Boolean()
    cosao_daotao_a_14 = fields.Boolean(string='①')
    cosao_daotao_b_14 = fields.Boolean(string='②')
    cosao_daotao_c_14 = fields.Boolean(string='③')
    thoigian_14 = fields.Integer()
    giaovien_14 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_14 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_14 = fields.Boolean()
    coso_chieu_chieu_a_14 = fields.Boolean(string='①')
    coso_chieu_chieu_b_14 = fields.Boolean(string='②')
    coso_chieu_chieu_c_14 = fields.Boolean(string='③')
    thoigian_chieu_14 = fields.Integer()
    giaovien_chieu_14 = fields.Many2one(comodel_name='giangvien.giangvien')
    @api.onchange('coso_chieu_chieu_a_14')
    def onchange_method_coso_chieu_chieu_a_14(self):
        if self.coso_chieu_chieu_a_14 == True:
            self.coso_chieu_chieu_b_14 = False
            self.coso_chieu_chieu_c_14 = False
    @api.onchange('coso_chieu_chieu_b_14')
    def onchange_method_coso_chieu_chieu_b_14(self):
        if self.coso_chieu_chieu_b_14 == True:
            self.coso_chieu_chieu_a_14 = False
            self.coso_chieu_chieu_c_14 = False
    @api.onchange('coso_chieu_chieu_c_14')
    def onchange_method_coso_chieu_chieu_c_14(self):
        if self.coso_chieu_chieu_c_14 == True:
            self.coso_chieu_chieu_a_14 = False
            self.coso_chieu_chieu_b_14 = False
    @api.onchange('cosao_daotao_a_14')
    def onchange_method_cosao_daotao_a_14(self):
        if self.cosao_daotao_a_14 == True:
            self.cosao_daotao_b_14 = False
            self.cosao_daotao_c_14 = False
    @api.onchange('cosao_daotao_b_14')
    def onchange_method_cosao_daotao_b_14(self):
        if self.cosao_daotao_b_14 == True:
            self.cosao_daotao_a_14 = False
            self.cosao_daotao_c_14 = False
    @api.onchange('cosao_daotao_c_14')
    def onchange_method_cosao_daotao_c_14(self):
        if self.cosao_daotao_c_14 == True:
            self.cosao_daotao_a_14 = False
            self.cosao_daotao_b_14 = False
    @api.one
    @api.depends('lich_laplich_14')
    def _thang_14(self):
        if self.lich_laplich_14:
            self.thang_14 = self.lich_laplich_14.month
            self.ngay_14 = self.lich_laplich_14.day
            self.thu_14 = convert_th(self.lich_laplich_14)
        else:
            self.thang_14 = ''
            self.ngay_14 = ''
            self.thu_14 = ''
    @api.onchange('cangay_14')
    def onchange_method_cangay_14(self):
        if self.cangay_14 == True:
            self.noidung_chieu_14 = None
            self.coso_chieu_14 = False
        else:
            self.coso_chieu_chieu_a_14 = False
            self.coso_chieu_chieu_b_14 = False
            self.coso_chieu_chieu_c_14 = False
            self.noidung_chieu_chieu_14 = None
            self.coso_chieu_chieu_14 = False
            self.thoigian_chieu_14 = 0
            self.giaovien_chieu_14 = None
    @api.onchange('ngaynghi_14')
    def onchange_method_ngaynghi_14(self):
        if self.ngaynghi_14 == True:
            self.cangay_14 = False
            self.noidung_sang_14 = None
            self.noidung_chieu_14 = None
            self.noidung_sang_char_14 = '休日'
            self.noidung_chieu_char_14 = '休日'
            self.coso_sang_14 = False
            self.coso_chieu_14 = False
            self.cosao_daotao_a_14 = False
            self.cosao_daotao_b_14 = False
            self.cosao_daotao_c_14 = False
            self.thoigian_14 = 0
            self.giaovien_14 = None
            self.noidung_chieu_chieu_14 = None
            self.coso_chieu_chieu_14 = False
            self.coso_chieu_chieu_a_14 = False
            self.coso_chieu_chieu_b_14 = False
            self.coso_chieu_chieu_c_14 = False
            self.thoigian_chieu_14 = 0
            self.giaovien_chieu_14 = None
        else:
            self.cangay_14 = False
            self.noidung_sang_14 = None
            self.noidung_chieu_14 = None
            self.noidung_sang_char_14 = ''
            self.noidung_chieu_char_14 = ''
            self.coso_sang_14 = False
            self.coso_chieu_14 = False
            self.cosao_daotao_a_14 = False
            self.cosao_daotao_b_14 = False
            self.cosao_daotao_c_14 = False
            self.thoigian_14 = 0
            self.giaovien_14 = None
            self.noidung_chieu_chieu_14 = None
            self.coso_chieu_chieu_14 = False
            self.coso_chieu_chieu_a_14 = False
            self.coso_chieu_chieu_b_14 = False
            self.coso_chieu_chieu_c_14 = False
            self.thoigian_chieu_14 = 0
            self.giaovien_chieu_14 = None
    @api.onchange('noidung_sang_14', 'noidung_chieu_14')
    def onchange_method_noidung_sang_14(self):
        if self.noidung_sang_14 and self.noidung_chieu_14:
            if self.noidung_sang_14.noidungdaotao != '休日' and self.noidung_chieu_14.noidungdaotao != '休日':
                self.thoigian_14 = 8
            elif self.noidung_sang_14.noidungdaotao != '休日' or self.noidung_chieu_14.noidungdaotao != '休日':
                self.thoigian_14 = 4
            else:
                self.thoigian_14 = 0
        elif self.noidung_sang_14 or self.noidung_chieu_14:
            if self.noidung_sang_14.noidungdaotao != '休日' and self.noidung_sang_14.noidungdaotao:
                self.thoigian_14 = 4
            elif self.noidung_chieu_14.noidungdaotao != '休日' and self.noidung_chieu_14.noidungdaotao:
                self.thoigian_14 = 4
            else:
                self.thoigian_14 = 0
        else:
            self.thoigian_14 = 0
    @api.onchange('noidung_chieu_chieu_14')
    def onchange_method_noidung_chieu_chieu_14(self):
        if self.noidung_chieu_chieu_14:
            if self.noidung_chieu_chieu_14.noidungdaotao != '休日':
                self.thoigian_chieu_14 = 4
        else:
            self.thoigian_chieu_14 = 0

    # Ngày _15
    # Ca sáng
    lich_laplich_15 = fields.Date()
    thang_15 = fields.Char(compute='_thang_15')
    ngay_15 = fields.Char(compute='_thang_15')
    thu_15 = fields.Char(compute='_thang_15')
    ngaynghi_15 = fields.Boolean()
    cangay_15 = fields.Boolean()
    noidung_sang_15 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_15 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_15 = fields.Char()
    noidung_chieu_char_15 = fields.Char()
    coso_sang_15 = fields.Boolean()
    coso_chieu_15 = fields.Boolean()
    cosao_daotao_a_15 = fields.Boolean(string='①')
    cosao_daotao_b_15 = fields.Boolean(string='②')
    cosao_daotao_c_15 = fields.Boolean(string='③')
    thoigian_15 = fields.Integer()
    giaovien_15 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_15 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_15 = fields.Boolean()
    coso_chieu_chieu_a_15 = fields.Boolean(string='①')
    coso_chieu_chieu_b_15 = fields.Boolean(string='②')
    coso_chieu_chieu_c_15 = fields.Boolean(string='③')
    thoigian_chieu_15 = fields.Integer()
    giaovien_chieu_15 = fields.Many2one(comodel_name='giangvien.giangvien')
    @api.onchange('coso_chieu_chieu_a_15')
    def onchange_method_coso_chieu_chieu_a_15(self):
        if self.coso_chieu_chieu_a_15 == True:
            self.coso_chieu_chieu_b_15 = False
            self.coso_chieu_chieu_c_15 = False
    @api.onchange('coso_chieu_chieu_b_15')
    def onchange_method_coso_chieu_chieu_b_15(self):
        if self.coso_chieu_chieu_b_15 == True:
            self.coso_chieu_chieu_a_15 = False
            self.coso_chieu_chieu_c_15 = False
    @api.onchange('coso_chieu_chieu_c_15')
    def onchange_method_coso_chieu_chieu_c_15(self):
        if self.coso_chieu_chieu_c_15 == True:
            self.coso_chieu_chieu_a_15 = False
            self.coso_chieu_chieu_b_15 = False
    @api.onchange('cosao_daotao_a_15')
    def onchange_method_cosao_daotao_a_15(self):
        if self.cosao_daotao_a_15 == True:
            self.cosao_daotao_b_15 = False
            self.cosao_daotao_c_15 = False
    @api.onchange('cosao_daotao_b_15')
    def onchange_method_cosao_daotao_b_15(self):
        if self.cosao_daotao_b_15 == True:
            self.cosao_daotao_a_15 = False
            self.cosao_daotao_c_15 = False
    @api.onchange('cosao_daotao_c_15')
    def onchange_method_cosao_daotao_c_15(self):
        if self.cosao_daotao_c_15 == True:
            self.cosao_daotao_a_15 = False
            self.cosao_daotao_b_15 = False
    @api.one
    @api.depends('lich_laplich_15')
    def _thang_15(self):
        if self.lich_laplich_15:
            self.thang_15 = self.lich_laplich_15.month
            self.ngay_15 = self.lich_laplich_15.day
            self.thu_15 = convert_th(self.lich_laplich_15)
        else:
            self.thang_15 = ''
            self.ngay_15 = ''
            self.thu_15 = ''
    @api.onchange('cangay_15')
    def onchange_method_cangay_15(self):
        if self.cangay_15 == True:
            self.noidung_chieu_15 = None
            self.coso_chieu_15 = False
        else:
            self.coso_chieu_chieu_a_15 = False
            self.coso_chieu_chieu_b_15 = False
            self.coso_chieu_chieu_c_15 = False
            self.noidung_chieu_chieu_15 = None
            self.coso_chieu_chieu_15 = False
            self.thoigian_chieu_15 = 0
            self.giaovien_chieu_15 = None
    @api.onchange('ngaynghi_15')
    def onchange_method_ngaynghi_15(self):
        if self.ngaynghi_15 == True:
            self.cangay_15 = False
            self.noidung_sang_15 = None
            self.noidung_chieu_15 = None
            self.noidung_sang_char_15 = '休日'
            self.noidung_chieu_char_15 = '休日'
            self.coso_sang_15 = False
            self.coso_chieu_15 = False
            self.cosao_daotao_a_15 = False
            self.cosao_daotao_b_15 = False
            self.cosao_daotao_c_15 = False
            self.thoigian_15 = 0
            self.giaovien_15 = None
            self.noidung_chieu_chieu_15 = None
            self.coso_chieu_chieu_15 = False
            self.coso_chieu_chieu_a_15 = False
            self.coso_chieu_chieu_b_15 = False
            self.coso_chieu_chieu_c_15 = False
            self.thoigian_chieu_15 = 0
            self.giaovien_chieu_15 = None
        else:
            self.cangay_15 = False
            self.noidung_sang_15 = None
            self.noidung_chieu_15 = None
            self.noidung_sang_char_15 = ''
            self.noidung_chieu_char_15 = ''
            self.coso_sang_15 = False
            self.coso_chieu_15 = False
            self.cosao_daotao_a_15 = False
            self.cosao_daotao_b_15 = False
            self.cosao_daotao_c_15 = False
            self.thoigian_15 = 0
            self.giaovien_15 = None
            self.noidung_chieu_chieu_15 = None
            self.coso_chieu_chieu_15 = False
            self.coso_chieu_chieu_a_15 = False
            self.coso_chieu_chieu_b_15 = False
            self.coso_chieu_chieu_c_15 = False
            self.thoigian_chieu_15 = 0
            self.giaovien_chieu_15 = None
    @api.onchange('noidung_sang_15', 'noidung_chieu_15')
    def onchange_method_noidung_sang_15(self):
        if self.noidung_sang_15 and self.noidung_chieu_15:
            if self.noidung_sang_15.noidungdaotao != '休日' and self.noidung_chieu_15.noidungdaotao != '休日':
                self.thoigian_15 = 8
            elif self.noidung_sang_15.noidungdaotao != '休日' or self.noidung_chieu_15.noidungdaotao != '休日':
                self.thoigian_15 = 4
            else:
                self.thoigian_15 = 0
        elif self.noidung_sang_15 or self.noidung_chieu_15:
            if self.noidung_sang_15.noidungdaotao != '休日' and self.noidung_sang_15.noidungdaotao:
                self.thoigian_15 = 4
            elif self.noidung_chieu_15.noidungdaotao != '休日' and self.noidung_chieu_15.noidungdaotao:
                self.thoigian_15 = 4
            else:
                self.thoigian_15 = 0
        else:
            self.thoigian_15 = 0
    @api.onchange('noidung_chieu_chieu_15')
    def onchange_method_noidung_chieu_chieu_15(self):
        if self.noidung_chieu_chieu_15:
            if self.noidung_chieu_chieu_15.noidungdaotao != '休日':
                self.thoigian_chieu_15 = 4
        else:
            self.thoigian_chieu_15 = 0

    # Ngày _16
    # Ca sáng
    lich_laplich_16 = fields.Date()
    thang_16 = fields.Char(compute='_thang_16')
    ngay_16 = fields.Char(compute='_thang_16')
    thu_16 = fields.Char(compute='_thang_16')
    ngaynghi_16 = fields.Boolean()
    cangay_16 = fields.Boolean()
    noidung_sang_16 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_16 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_16 = fields.Char()
    noidung_chieu_char_16 = fields.Char()
    coso_sang_16 = fields.Boolean()
    coso_chieu_16 = fields.Boolean()
    cosao_daotao_a_16 = fields.Boolean(string='①')
    cosao_daotao_b_16 = fields.Boolean(string='②')
    cosao_daotao_c_16 = fields.Boolean(string='③')
    thoigian_16 = fields.Integer()
    giaovien_16 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_16 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_16 = fields.Boolean()
    coso_chieu_chieu_a_16 = fields.Boolean(string='①')
    coso_chieu_chieu_b_16 = fields.Boolean(string='②')
    coso_chieu_chieu_c_16 = fields.Boolean(string='③')
    thoigian_chieu_16 = fields.Integer()
    giaovien_chieu_16 = fields.Many2one(comodel_name='giangvien.giangvien')
    @api.onchange('coso_chieu_chieu_a_16')
    def onchange_method_coso_chieu_chieu_a_16(self):
        if self.coso_chieu_chieu_a_16 == True:
            self.coso_chieu_chieu_b_16 = False
            self.coso_chieu_chieu_c_16 = False
    @api.onchange('coso_chieu_chieu_b_16')
    def onchange_method_coso_chieu_chieu_b_16(self):
        if self.coso_chieu_chieu_b_16 == True:
            self.coso_chieu_chieu_a_16 = False
            self.coso_chieu_chieu_c_16 = False
    @api.onchange('coso_chieu_chieu_c_16')
    def onchange_method_coso_chieu_chieu_c_16(self):
        if self.coso_chieu_chieu_c_16 == True:
            self.coso_chieu_chieu_a_16 = False
            self.coso_chieu_chieu_b_16 = False
    @api.onchange('cosao_daotao_a_16')
    def onchange_method_cosao_daotao_a_16(self):
        if self.cosao_daotao_a_16 == True:
            self.cosao_daotao_b_16 = False
            self.cosao_daotao_c_16 = False
    @api.onchange('cosao_daotao_b_16')
    def onchange_method_cosao_daotao_b_16(self):
        if self.cosao_daotao_b_16 == True:
            self.cosao_daotao_a_16 = False
            self.cosao_daotao_c_16 = False
    @api.onchange('cosao_daotao_c_16')
    def onchange_method_cosao_daotao_c_16(self):
        if self.cosao_daotao_c_16 == True:
            self.cosao_daotao_a_16 = False
            self.cosao_daotao_b_16 = False
    @api.one
    @api.depends('lich_laplich_16')
    def _thang_16(self):
        if self.lich_laplich_16:
            self.thang_16 = self.lich_laplich_16.month
            self.ngay_16 = self.lich_laplich_16.day
            self.thu_16 = convert_th(self.lich_laplich_16)
        else:
            self.thang_16 = ''
            self.ngay_16 = ''
            self.thu_16 = ''
    @api.onchange('cangay_16')
    def onchange_method_cangay_16(self):
        if self.cangay_16 == True:
            self.noidung_chieu_16 = None
            self.coso_chieu_16 = False
        else:
            self.coso_chieu_chieu_a_16 = False
            self.coso_chieu_chieu_b_16 = False
            self.coso_chieu_chieu_c_16 = False
            self.noidung_chieu_chieu_16 = None
            self.coso_chieu_chieu_16 = False
            self.thoigian_chieu_16 = 0
            self.giaovien_chieu_16 = None
    @api.onchange('ngaynghi_16')
    def onchange_method_ngaynghi_16(self):
        if self.ngaynghi_16 == True:
            self.cangay_16 = False
            self.noidung_sang_16 = None
            self.noidung_chieu_16 = None
            self.noidung_sang_char_16 = '休日'
            self.noidung_chieu_char_16 = '休日'
            self.coso_sang_16 = False
            self.coso_chieu_16 = False
            self.cosao_daotao_a_16 = False
            self.cosao_daotao_b_16 = False
            self.cosao_daotao_c_16 = False
            self.thoigian_16 = 0
            self.giaovien_16 = None
            self.noidung_chieu_chieu_16 = None
            self.coso_chieu_chieu_16 = False
            self.coso_chieu_chieu_a_16 = False
            self.coso_chieu_chieu_b_16 = False
            self.coso_chieu_chieu_c_16 = False
            self.thoigian_chieu_16 = 0
            self.giaovien_chieu_16 = None
        else:
            self.cangay_16 = False
            self.noidung_sang_16 = None
            self.noidung_chieu_16 = None
            self.noidung_sang_char_16 = ''
            self.noidung_chieu_char_16 = ''
            self.coso_sang_16 = False
            self.coso_chieu_16 = False
            self.cosao_daotao_a_16 = False
            self.cosao_daotao_b_16 = False
            self.cosao_daotao_c_16 = False
            self.thoigian_16 = 0
            self.giaovien_16 = None
            self.noidung_chieu_chieu_16 = None
            self.coso_chieu_chieu_16 = False
            self.coso_chieu_chieu_a_16 = False
            self.coso_chieu_chieu_b_16 = False
            self.coso_chieu_chieu_c_16 = False
            self.thoigian_chieu_16 = 0
            self.giaovien_chieu_16 = None
    @api.onchange('noidung_sang_16', 'noidung_chieu_16')
    def onchange_method_noidung_sang_16(self):
        if self.noidung_sang_16 and self.noidung_chieu_16:
            if self.noidung_sang_16.noidungdaotao != '休日' and self.noidung_chieu_16.noidungdaotao != '休日':
                self.thoigian_16 = 8
            elif self.noidung_sang_16.noidungdaotao != '休日' or self.noidung_chieu_16.noidungdaotao != '休日':
                self.thoigian_16 = 4
            else:
                self.thoigian_16 = 0
        elif self.noidung_sang_16 or self.noidung_chieu_16:
            if self.noidung_sang_16.noidungdaotao != '休日' and self.noidung_sang_16.noidungdaotao:
                self.thoigian_16 = 4
            elif self.noidung_chieu_16.noidungdaotao != '休日' and self.noidung_chieu_16.noidungdaotao:
                self.thoigian_16 = 4
            else:
                self.thoigian_16 = 0
        else:
            self.thoigian_16 = 0
    @api.onchange('noidung_chieu_chieu_16')
    def onchange_method_noidung_chieu_chieu_16(self):
        if self.noidung_chieu_chieu_16:
            if self.noidung_chieu_chieu_16.noidungdaotao != '休日':
                self.thoigian_chieu_16 = 4
        else:
            self.thoigian_chieu_16 = 0

    # Ngày _17
    # Ca sáng
    lich_laplich_17 = fields.Date()
    thang_17 = fields.Char(compute='_thang_17')
    ngay_17 = fields.Char(compute='_thang_17')
    thu_17 = fields.Char(compute='_thang_17')
    ngaynghi_17 = fields.Boolean()
    cangay_17 = fields.Boolean()
    noidung_sang_17 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_17 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_17 = fields.Char()
    noidung_chieu_char_17 = fields.Char()
    coso_sang_17 = fields.Boolean()
    coso_chieu_17 = fields.Boolean()
    cosao_daotao_a_17 = fields.Boolean(string='①')
    cosao_daotao_b_17 = fields.Boolean(string='②')
    cosao_daotao_c_17 = fields.Boolean(string='③')
    thoigian_17 = fields.Integer()
    giaovien_17 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_17 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_17 = fields.Boolean()
    coso_chieu_chieu_a_17 = fields.Boolean(string='①')
    coso_chieu_chieu_b_17 = fields.Boolean(string='②')
    coso_chieu_chieu_c_17 = fields.Boolean(string='③')
    thoigian_chieu_17 = fields.Integer()
    giaovien_chieu_17 = fields.Many2one(comodel_name='giangvien.giangvien')
    @api.onchange('coso_chieu_chieu_a_17')
    def onchange_method_coso_chieu_chieu_a_17(self):
        if self.coso_chieu_chieu_a_17 == True:
            self.coso_chieu_chieu_b_17 = False
            self.coso_chieu_chieu_c_17 = False
    @api.onchange('coso_chieu_chieu_b_17')
    def onchange_method_coso_chieu_chieu_b_17(self):
        if self.coso_chieu_chieu_b_17 == True:
            self.coso_chieu_chieu_a_17 = False
            self.coso_chieu_chieu_c_17 = False
    @api.onchange('coso_chieu_chieu_c_17')
    def onchange_method_coso_chieu_chieu_c_17(self):
        if self.coso_chieu_chieu_c_17 == True:
            self.coso_chieu_chieu_a_17 = False
            self.coso_chieu_chieu_b_17 = False
    @api.onchange('cosao_daotao_a_17')
    def onchange_method_cosao_daotao_a_17(self):
        if self.cosao_daotao_a_17 == True:
            self.cosao_daotao_b_17 = False
            self.cosao_daotao_c_17 = False
    @api.onchange('cosao_daotao_b_17')
    def onchange_method_cosao_daotao_b_17(self):
        if self.cosao_daotao_b_17 == True:
            self.cosao_daotao_a_17 = False
            self.cosao_daotao_c_17 = False
    @api.onchange('cosao_daotao_c_17')
    def onchange_method_cosao_daotao_c_17(self):
        if self.cosao_daotao_c_17 == True:
            self.cosao_daotao_a_17 = False
            self.cosao_daotao_b_17 = False
    @api.one
    @api.depends('lich_laplich_17')
    def _thang_17(self):
        if self.lich_laplich_17:
            self.thang_17 = self.lich_laplich_17.month
            self.ngay_17 = self.lich_laplich_17.day
            self.thu_17 = convert_th(self.lich_laplich_17)
        else:
            self.thang_17 = ''
            self.ngay_17 = ''
            self.thu_17 = ''
    @api.onchange('cangay_17')
    def onchange_method_cangay_17(self):
        if self.cangay_17 == True:
            self.noidung_chieu_17 = None
            self.coso_chieu_17 = False
        else:
            self.coso_chieu_chieu_a_17 = False
            self.coso_chieu_chieu_b_17 = False
            self.coso_chieu_chieu_c_17 = False
            self.noidung_chieu_chieu_17 = None
            self.coso_chieu_chieu_17 = False
            self.thoigian_chieu_17 = 0
            self.giaovien_chieu_17 = None
    @api.onchange('ngaynghi_17')
    def onchange_method_ngaynghi_17(self):
        if self.ngaynghi_17 == True:
            self.cangay_17 = False
            self.noidung_sang_17 = None
            self.noidung_chieu_17 = None
            self.noidung_sang_char_17 = '休日'
            self.noidung_chieu_char_17 = '休日'
            self.coso_sang_17 = False
            self.coso_chieu_17 = False
            self.cosao_daotao_a_17 = False
            self.cosao_daotao_b_17 = False
            self.cosao_daotao_c_17 = False
            self.thoigian_17 = 0
            self.giaovien_17 = None
            self.noidung_chieu_chieu_17 = None
            self.coso_chieu_chieu_17 = False
            self.coso_chieu_chieu_a_17 = False
            self.coso_chieu_chieu_b_17 = False
            self.coso_chieu_chieu_c_17 = False
            self.thoigian_chieu_17 = 0
            self.giaovien_chieu_17 = None
        else:
            self.cangay_17 = False
            self.noidung_sang_17 = None
            self.noidung_chieu_17 = None
            self.noidung_sang_char_17 = ''
            self.noidung_chieu_char_17 = ''
            self.coso_sang_17 = False
            self.coso_chieu_17 = False
            self.cosao_daotao_a_17 = False
            self.cosao_daotao_b_17 = False
            self.cosao_daotao_c_17 = False
            self.thoigian_17 = 0
            self.giaovien_17 = None
            self.noidung_chieu_chieu_17 = None
            self.coso_chieu_chieu_17 = False
            self.coso_chieu_chieu_a_17 = False
            self.coso_chieu_chieu_b_17 = False
            self.coso_chieu_chieu_c_17 = False
            self.thoigian_chieu_17 = 0
            self.giaovien_chieu_17 = None
    @api.onchange('noidung_sang_17', 'noidung_chieu_17')
    def onchange_method_noidung_sang_17(self):
        if self.noidung_sang_17 and self.noidung_chieu_17:
            if self.noidung_sang_17.noidungdaotao != '休日' and self.noidung_chieu_17.noidungdaotao != '休日':
                self.thoigian_17 = 8
            elif self.noidung_sang_17.noidungdaotao != '休日' or self.noidung_chieu_17.noidungdaotao != '休日':
                self.thoigian_17 = 4
            else:
                self.thoigian_17 = 0
        elif self.noidung_sang_17 or self.noidung_chieu_17:
            if self.noidung_sang_17.noidungdaotao != '休日' and self.noidung_sang_17.noidungdaotao:
                self.thoigian_17 = 4
            elif self.noidung_chieu_17.noidungdaotao != '休日' and self.noidung_chieu_17.noidungdaotao:
                self.thoigian_17 = 4
            else:
                self.thoigian_17 = 0
        else:
            self.thoigian_17 = 0
    @api.onchange('noidung_chieu_chieu_17')
    def onchange_method_noidung_chieu_chieu_17(self):
        if self.noidung_chieu_chieu_17:
            if self.noidung_chieu_chieu_17.noidungdaotao != '休日':
                self.thoigian_chieu_17 = 4
        else:
            self.thoigian_chieu_17 = 0

    # Ngày _18
    # Ca sáng
    lich_laplich_18 = fields.Date()
    thang_18 = fields.Char(compute='_thang_18')
    ngay_18 = fields.Char(compute='_thang_18')
    thu_18 = fields.Char(compute='_thang_18')
    ngaynghi_18 = fields.Boolean()
    cangay_18 = fields.Boolean()
    noidung_sang_18 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_18 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_18 = fields.Char()
    noidung_chieu_char_18 = fields.Char()
    coso_sang_18 = fields.Boolean()
    coso_chieu_18 = fields.Boolean()
    cosao_daotao_a_18 = fields.Boolean(string='①')
    cosao_daotao_b_18 = fields.Boolean(string='②')
    cosao_daotao_c_18 = fields.Boolean(string='③')
    thoigian_18 = fields.Integer()
    giaovien_18 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_18 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_18 = fields.Boolean()
    coso_chieu_chieu_a_18 = fields.Boolean(string='①')
    coso_chieu_chieu_b_18 = fields.Boolean(string='②')
    coso_chieu_chieu_c_18 = fields.Boolean(string='③')
    thoigian_chieu_18 = fields.Integer()
    giaovien_chieu_18 = fields.Many2one(comodel_name='giangvien.giangvien')
    @api.onchange('coso_chieu_chieu_a_18')
    def onchange_method_coso_chieu_chieu_a_18(self):
        if self.coso_chieu_chieu_a_18 == True:
            self.coso_chieu_chieu_b_18 = False
            self.coso_chieu_chieu_c_18 = False
    @api.onchange('coso_chieu_chieu_b_18')
    def onchange_method_coso_chieu_chieu_b_18(self):
        if self.coso_chieu_chieu_b_18 == True:
            self.coso_chieu_chieu_a_18 = False
            self.coso_chieu_chieu_c_18 = False
    @api.onchange('coso_chieu_chieu_c_18')
    def onchange_method_coso_chieu_chieu_c_18(self):
        if self.coso_chieu_chieu_c_18 == True:
            self.coso_chieu_chieu_a_18 = False
            self.coso_chieu_chieu_b_18 = False
    @api.onchange('cosao_daotao_a_18')
    def onchange_method_cosao_daotao_a_18(self):
        if self.cosao_daotao_a_18 == True:
            self.cosao_daotao_b_18 = False
            self.cosao_daotao_c_18 = False
    @api.onchange('cosao_daotao_b_18')
    def onchange_method_cosao_daotao_b_18(self):
        if self.cosao_daotao_b_18 == True:
            self.cosao_daotao_a_18 = False
            self.cosao_daotao_c_18 = False
    @api.onchange('cosao_daotao_c_18')
    def onchange_method_cosao_daotao_c_18(self):
        if self.cosao_daotao_c_18 == True:
            self.cosao_daotao_a_18 = False
            self.cosao_daotao_b_18 = False
    @api.one
    @api.depends('lich_laplich_18')
    def _thang_18(self):
        if self.lich_laplich_18:
            self.thang_18 = self.lich_laplich_18.month
            self.ngay_18 = self.lich_laplich_18.day
            self.thu_18 = convert_th(self.lich_laplich_18)
        else:
            self.thang_18 = ''
            self.ngay_18 = ''
            self.thu_18 = ''
    @api.onchange('cangay_18')
    def onchange_method_cangay_18(self):
        if self.cangay_18 == True:
            self.noidung_chieu_18 = None
            self.coso_chieu_18 = False
        else:
            self.coso_chieu_chieu_a_18 = False
            self.coso_chieu_chieu_b_18 = False
            self.coso_chieu_chieu_c_18 = False
            self.noidung_chieu_chieu_18 = None
            self.coso_chieu_chieu_18 = False
            self.thoigian_chieu_18 = 0
            self.giaovien_chieu_18 = None
    @api.onchange('ngaynghi_18')
    def onchange_method_ngaynghi_18(self):
        if self.ngaynghi_18 == True:
            self.cangay_18 = False
            self.noidung_sang_18 = None
            self.noidung_chieu_18 = None
            self.noidung_sang_char_18 = '休日'
            self.noidung_chieu_char_18 = '休日'
            self.coso_sang_18 = False
            self.coso_chieu_18 = False
            self.cosao_daotao_a_18 = False
            self.cosao_daotao_b_18 = False
            self.cosao_daotao_c_18 = False
            self.thoigian_18 = 0
            self.giaovien_18 = None
            self.noidung_chieu_chieu_18 = None
            self.coso_chieu_chieu_18 = False
            self.coso_chieu_chieu_a_18 = False
            self.coso_chieu_chieu_b_18 = False
            self.coso_chieu_chieu_c_18 = False
            self.thoigian_chieu_18 = 0
            self.giaovien_chieu_18 = None
        else:
            self.cangay_18 = False
            self.noidung_sang_18 = None
            self.noidung_chieu_18 = None
            self.noidung_sang_char_18 = ''
            self.noidung_chieu_char_18 = ''
            self.coso_sang_18 = False
            self.coso_chieu_18 = False
            self.cosao_daotao_a_18 = False
            self.cosao_daotao_b_18 = False
            self.cosao_daotao_c_18 = False
            self.thoigian_18 = 0
            self.giaovien_18 = None
            self.noidung_chieu_chieu_18 = None
            self.coso_chieu_chieu_18 = False
            self.coso_chieu_chieu_a_18 = False
            self.coso_chieu_chieu_b_18 = False
            self.coso_chieu_chieu_c_18 = False
            self.thoigian_chieu_18 = 0
            self.giaovien_chieu_18 = None
    @api.onchange('noidung_sang_18', 'noidung_chieu_18')
    def onchange_method_noidung_sang_18(self):
        if self.noidung_sang_18 and self.noidung_chieu_18:
            if self.noidung_sang_18.noidungdaotao != '休日' and self.noidung_chieu_18.noidungdaotao != '休日':
                self.thoigian_18 = 8
            elif self.noidung_sang_18.noidungdaotao != '休日' or self.noidung_chieu_18.noidungdaotao != '休日':
                self.thoigian_18 = 4
            else:
                self.thoigian_18 = 0
        elif self.noidung_sang_18 or self.noidung_chieu_18:
            if self.noidung_sang_18.noidungdaotao != '休日' and self.noidung_sang_18.noidungdaotao:
                self.thoigian_18 = 4
            elif self.noidung_chieu_18.noidungdaotao != '休日' and self.noidung_chieu_18.noidungdaotao:
                self.thoigian_18 = 4
            else:
                self.thoigian_18 = 0
        else:
            self.thoigian_18 = 0
    @api.onchange('noidung_chieu_chieu_18')
    def onchange_method_noidung_chieu_chieu_18(self):
        if self.noidung_chieu_chieu_18:
            if self.noidung_chieu_chieu_18.noidungdaotao != '休日':
                self.thoigian_chieu_18 = 4
        else:
            self.thoigian_chieu_18 = 0

    # Ngày _19
    # Ca sáng
    lich_laplich_19 = fields.Date()
    thang_19 = fields.Char(compute='_thang_19')
    ngay_19 = fields.Char(compute='_thang_19')
    thu_19 = fields.Char(compute='_thang_19')
    ngaynghi_19 = fields.Boolean()
    cangay_19 = fields.Boolean()
    noidung_sang_19 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_19 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_19 = fields.Char()
    noidung_chieu_char_19 = fields.Char()
    coso_sang_19 = fields.Boolean()
    coso_chieu_19 = fields.Boolean()
    cosao_daotao_a_19 = fields.Boolean(string='①')
    cosao_daotao_b_19 = fields.Boolean(string='②')
    cosao_daotao_c_19 = fields.Boolean(string='③')
    thoigian_19 = fields.Integer()
    giaovien_19 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_19 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_19 = fields.Boolean()
    coso_chieu_chieu_a_19 = fields.Boolean(string='①')
    coso_chieu_chieu_b_19 = fields.Boolean(string='②')
    coso_chieu_chieu_c_19 = fields.Boolean(string='③')
    thoigian_chieu_19 = fields.Integer()
    giaovien_chieu_19 = fields.Many2one(comodel_name='giangvien.giangvien')
    @api.onchange('coso_chieu_chieu_a_19')
    def onchange_method_coso_chieu_chieu_a_19(self):
        if self.coso_chieu_chieu_a_19 == True:
            self.coso_chieu_chieu_b_19 = False
            self.coso_chieu_chieu_c_19 = False
    @api.onchange('coso_chieu_chieu_b_19')
    def onchange_method_coso_chieu_chieu_b_19(self):
        if self.coso_chieu_chieu_b_19 == True:
            self.coso_chieu_chieu_a_19 = False
            self.coso_chieu_chieu_c_19 = False
    @api.onchange('coso_chieu_chieu_c_19')
    def onchange_method_coso_chieu_chieu_c_19(self):
        if self.coso_chieu_chieu_c_19 == True:
            self.coso_chieu_chieu_a_19 = False
            self.coso_chieu_chieu_b_19 = False
    @api.onchange('cosao_daotao_a_19')
    def onchange_method_cosao_daotao_a_19(self):
        if self.cosao_daotao_a_19 == True:
            self.cosao_daotao_b_19 = False
            self.cosao_daotao_c_19 = False
    @api.onchange('cosao_daotao_b_19')
    def onchange_method_cosao_daotao_b_19(self):
        if self.cosao_daotao_b_19 == True:
            self.cosao_daotao_a_19 = False
            self.cosao_daotao_c_19 = False
    @api.onchange('cosao_daotao_c_19')
    def onchange_method_cosao_daotao_c_19(self):
        if self.cosao_daotao_c_19 == True:
            self.cosao_daotao_a_19 = False
            self.cosao_daotao_b_19 = False
    @api.one
    @api.depends('lich_laplich_19')
    def _thang_19(self):
        if self.lich_laplich_19:
            self.thang_19 = self.lich_laplich_19.month
            self.ngay_19 = self.lich_laplich_19.day
            self.thu_19 = convert_th(self.lich_laplich_19)
        else:
            self.thang_19 = ''
            self.ngay_19 = ''
            self.thu_19 = ''
    @api.onchange('cangay_19')
    def onchange_method_cangay_19(self):
        if self.cangay_19 == True:
            self.noidung_chieu_19 = None
            self.coso_chieu_19 = False
        else:
            self.coso_chieu_chieu_a_19 = False
            self.coso_chieu_chieu_b_19 = False
            self.coso_chieu_chieu_c_19 = False
            self.noidung_chieu_chieu_19 = None
            self.coso_chieu_chieu_19 = False
            self.thoigian_chieu_19 = 0
            self.giaovien_chieu_19 = None
    @api.onchange('ngaynghi_19')
    def onchange_method_ngaynghi_19(self):
        if self.ngaynghi_19 == True:
            self.cangay_19 = False
            self.noidung_sang_19 = None
            self.noidung_chieu_19 = None
            self.noidung_sang_char_19 = '休日'
            self.noidung_chieu_char_19 = '休日'
            self.coso_sang_19 = False
            self.coso_chieu_19 = False
            self.cosao_daotao_a_19 = False
            self.cosao_daotao_b_19 = False
            self.cosao_daotao_c_19 = False
            self.thoigian_19 = 0
            self.giaovien_19 = None
            self.noidung_chieu_chieu_19 = None
            self.coso_chieu_chieu_19 = False
            self.coso_chieu_chieu_a_19 = False
            self.coso_chieu_chieu_b_19 = False
            self.coso_chieu_chieu_c_19 = False
            self.thoigian_chieu_19 = 0
            self.giaovien_chieu_19 = None
        else:
            self.cangay_19 = False
            self.noidung_sang_19 = None
            self.noidung_chieu_19 = None
            self.noidung_sang_char_19 = ''
            self.noidung_chieu_char_19 = ''
            self.coso_sang_19 = False
            self.coso_chieu_19 = False
            self.cosao_daotao_a_19 = False
            self.cosao_daotao_b_19 = False
            self.cosao_daotao_c_19 = False
            self.thoigian_19 = 0
            self.giaovien_19 = None
            self.noidung_chieu_chieu_19 = None
            self.coso_chieu_chieu_19 = False
            self.coso_chieu_chieu_a_19 = False
            self.coso_chieu_chieu_b_19 = False
            self.coso_chieu_chieu_c_19 = False
            self.thoigian_chieu_19 = 0
            self.giaovien_chieu_19 = None
    @api.onchange('noidung_sang_19', 'noidung_chieu_19')
    def onchange_method_noidung_sang_19(self):
        if self.noidung_sang_19 and self.noidung_chieu_19:
            if self.noidung_sang_19.noidungdaotao != '休日' and self.noidung_chieu_19.noidungdaotao != '休日':
                self.thoigian_19 = 8
            elif self.noidung_sang_19.noidungdaotao != '休日' or self.noidung_chieu_19.noidungdaotao != '休日':
                self.thoigian_19 = 4
            else:
                self.thoigian_19 = 0
        elif self.noidung_sang_19 or self.noidung_chieu_19:
            if self.noidung_sang_19.noidungdaotao != '休日' and self.noidung_sang_19.noidungdaotao:
                self.thoigian_19 = 4
            elif self.noidung_chieu_19.noidungdaotao != '休日' and self.noidung_chieu_19.noidungdaotao:
                self.thoigian_19 = 4
            else:
                self.thoigian_19 = 0
        else:
            self.thoigian_19 = 0
    @api.onchange('noidung_chieu_chieu_19')
    def onchange_method_noidung_chieu_chieu_19(self):
        if self.noidung_chieu_chieu_19:
            if self.noidung_chieu_chieu_19.noidungdaotao != '休日':
                self.thoigian_chieu_19 = 4
        else:
            self.thoigian_chieu_19 = 0

    # Ngày _20
    # Ca sáng
    lich_laplich_20 = fields.Date()
    thang_20 = fields.Char(compute='_thang_20')
    ngay_20 = fields.Char(compute='_thang_20')
    thu_20 = fields.Char(compute='_thang_20')
    ngaynghi_20 = fields.Boolean()
    cangay_20 = fields.Boolean()
    noidung_sang_20 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_20 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_20 = fields.Char()
    noidung_chieu_char_20 = fields.Char()
    coso_sang_20 = fields.Boolean()
    coso_chieu_20 = fields.Boolean()
    cosao_daotao_a_20 = fields.Boolean(string='①')
    cosao_daotao_b_20 = fields.Boolean(string='②')
    cosao_daotao_c_20 = fields.Boolean(string='③')
    thoigian_20 = fields.Integer()
    giaovien_20 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_20 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_20 = fields.Boolean()
    coso_chieu_chieu_a_20 = fields.Boolean(string='①')
    coso_chieu_chieu_b_20 = fields.Boolean(string='②')
    coso_chieu_chieu_c_20 = fields.Boolean(string='③')
    thoigian_chieu_20 = fields.Integer()
    giaovien_chieu_20 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_20')
    def onchange_method_coso_chieu_chieu_a_20(self):
        if self.coso_chieu_chieu_a_20 == True:
            self.coso_chieu_chieu_b_20 = False
            self.coso_chieu_chieu_c_20 = False

    @api.onchange('coso_chieu_chieu_b_20')
    def onchange_method_coso_chieu_chieu_b_20(self):
        if self.coso_chieu_chieu_b_20 == True:
            self.coso_chieu_chieu_a_20 = False
            self.coso_chieu_chieu_c_20 = False

    @api.onchange('coso_chieu_chieu_c_20')
    def onchange_method_coso_chieu_chieu_c_20(self):
        if self.coso_chieu_chieu_c_20 == True:
            self.coso_chieu_chieu_a_20 = False
            self.coso_chieu_chieu_b_20 = False

    @api.onchange('cosao_daotao_a_20')
    def onchange_method_cosao_daotao_a_20(self):
        if self.cosao_daotao_a_20 == True:
            self.cosao_daotao_b_20 = False
            self.cosao_daotao_c_20 = False

    @api.onchange('cosao_daotao_b_20')
    def onchange_method_cosao_daotao_b_20(self):
        if self.cosao_daotao_b_20 == True:
            self.cosao_daotao_a_20 = False
            self.cosao_daotao_c_20 = False

    @api.onchange('cosao_daotao_c_20')
    def onchange_method_cosao_daotao_c_20(self):
        if self.cosao_daotao_c_20 == True:
            self.cosao_daotao_a_20 = False
            self.cosao_daotao_b_20 = False

    @api.one
    @api.depends('lich_laplich_20')
    def _thang_20(self):
        if self.lich_laplich_20:
            self.thang_20 = self.lich_laplich_20.month
            self.ngay_20 = self.lich_laplich_20.day
            self.thu_20 = convert_th(self.lich_laplich_20)
        else:
            self.thang_20 = ''
            self.ngay_20 = ''
            self.thu_20 = ''

    @api.onchange('cangay_20')
    def onchange_method_cangay_20(self):
        if self.cangay_20 == True:
            self.noidung_chieu_20 = None
            self.coso_chieu_20 = False
        else:
            self.coso_chieu_chieu_a_20 = False
            self.coso_chieu_chieu_b_20 = False
            self.coso_chieu_chieu_c_20 = False
            self.noidung_chieu_chieu_20 = None
            self.coso_chieu_chieu_20 = False
            self.thoigian_chieu_20 = 0
            self.giaovien_chieu_20 = None

    @api.onchange('ngaynghi_20')
    def onchange_method_ngaynghi_20(self):
        if self.ngaynghi_20 == True:
            self.cangay_20 = False
            self.noidung_sang_20 = None
            self.noidung_chieu_20 = None
            self.noidung_sang_char_20 = '休日'
            self.noidung_chieu_char_20 = '休日'
            self.coso_sang_20 = False
            self.coso_chieu_20 = False
            self.cosao_daotao_a_20 = False
            self.cosao_daotao_b_20 = False
            self.cosao_daotao_c_20 = False
            self.thoigian_20 = 0
            self.giaovien_20 = None
            self.noidung_chieu_chieu_20 = None
            self.coso_chieu_chieu_20 = False
            self.coso_chieu_chieu_a_20 = False
            self.coso_chieu_chieu_b_20 = False
            self.coso_chieu_chieu_c_20 = False
            self.thoigian_chieu_20 = 0
            self.giaovien_chieu_20 = None
        else:
            self.cangay_20 = False
            self.noidung_sang_20 = None
            self.noidung_chieu_20 = None
            self.noidung_sang_char_20 = ''
            self.noidung_chieu_char_20 = ''
            self.coso_sang_20 = False
            self.coso_chieu_20 = False
            self.cosao_daotao_a_20 = False
            self.cosao_daotao_b_20 = False
            self.cosao_daotao_c_20 = False
            self.thoigian_20 = 0
            self.giaovien_20 = None
            self.noidung_chieu_chieu_20 = None
            self.coso_chieu_chieu_20 = False
            self.coso_chieu_chieu_a_20 = False
            self.coso_chieu_chieu_b_20 = False
            self.coso_chieu_chieu_c_20 = False
            self.thoigian_chieu_20 = 0
            self.giaovien_chieu_20 = None

    @api.onchange('noidung_sang_20', 'noidung_chieu_20')
    def onchange_method_noidung_sang_20(self):
        if self.noidung_sang_20 and self.noidung_chieu_20:
            if self.noidung_sang_20.noidungdaotao != '休日' and self.noidung_chieu_20.noidungdaotao != '休日':
                self.thoigian_20 = 8
            elif self.noidung_sang_20.noidungdaotao != '休日' or self.noidung_chieu_20.noidungdaotao != '休日':
                self.thoigian_20 = 4
            else:
                self.thoigian_20 = 0
        elif self.noidung_sang_20 or self.noidung_chieu_20:
            if self.noidung_sang_20.noidungdaotao != '休日' and self.noidung_sang_20.noidungdaotao:
                self.thoigian_20 = 4
            elif self.noidung_chieu_20.noidungdaotao != '休日' and self.noidung_chieu_20.noidungdaotao:
                self.thoigian_20 = 4
            else:
                self.thoigian_20 = 0
        else:
            self.thoigian_20 = 0

    @api.onchange('noidung_chieu_chieu_20')
    def onchange_method_noidung_chieu_chieu_20(self):
        if self.noidung_chieu_chieu_20:
            if self.noidung_chieu_chieu_20.noidungdaotao != '休日':
                self.thoigian_chieu_20 = 4
        else:
            self.thoigian_chieu_20 = 0

    # Ngày _21
    # Ca sáng
    lich_laplich_21 = fields.Date()
    thang_21 = fields.Char(compute='_thang_21')
    ngay_21 = fields.Char(compute='_thang_21')
    thu_21 = fields.Char(compute='_thang_21')
    ngaynghi_21 = fields.Boolean()
    cangay_21 = fields.Boolean()
    noidung_sang_21 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_21 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_21 = fields.Char()
    noidung_chieu_char_21 = fields.Char()
    coso_sang_21 = fields.Boolean()
    coso_chieu_21 = fields.Boolean()
    cosao_daotao_a_21 = fields.Boolean(string='①')
    cosao_daotao_b_21 = fields.Boolean(string='②')
    cosao_daotao_c_21 = fields.Boolean(string='③')
    thoigian_21 = fields.Integer()
    giaovien_21 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_21 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_21 = fields.Boolean()
    coso_chieu_chieu_a_21 = fields.Boolean(string='①')
    coso_chieu_chieu_b_21 = fields.Boolean(string='②')
    coso_chieu_chieu_c_21 = fields.Boolean(string='③')
    thoigian_chieu_21 = fields.Integer()
    giaovien_chieu_21 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_21')
    def onchange_method_coso_chieu_chieu_a_21(self):
        if self.coso_chieu_chieu_a_21 == True:
            self.coso_chieu_chieu_b_21 = False
            self.coso_chieu_chieu_c_21 = False

    @api.onchange('coso_chieu_chieu_b_21')
    def onchange_method_coso_chieu_chieu_b_21(self):
        if self.coso_chieu_chieu_b_21 == True:
            self.coso_chieu_chieu_a_21 = False
            self.coso_chieu_chieu_c_21 = False

    @api.onchange('coso_chieu_chieu_c_21')
    def onchange_method_coso_chieu_chieu_c_21(self):
        if self.coso_chieu_chieu_c_21 == True:
            self.coso_chieu_chieu_a_21 = False
            self.coso_chieu_chieu_b_21 = False

    @api.onchange('cosao_daotao_a_21')
    def onchange_method_cosao_daotao_a_21(self):
        if self.cosao_daotao_a_21 == True:
            self.cosao_daotao_b_21 = False
            self.cosao_daotao_c_21 = False

    @api.onchange('cosao_daotao_b_21')
    def onchange_method_cosao_daotao_b_21(self):
        if self.cosao_daotao_b_21 == True:
            self.cosao_daotao_a_21 = False
            self.cosao_daotao_c_21 = False

    @api.onchange('cosao_daotao_c_21')
    def onchange_method_cosao_daotao_c_21(self):
        if self.cosao_daotao_c_21 == True:
            self.cosao_daotao_a_21 = False
            self.cosao_daotao_b_21 = False

    @api.one
    @api.depends('lich_laplich_21')
    def _thang_21(self):
        if self.lich_laplich_21:
            self.thang_21 = self.lich_laplich_21.month
            self.ngay_21 = self.lich_laplich_21.day
            self.thu_21 = convert_th(self.lich_laplich_21)
        else:
            self.thang_21 = ''
            self.ngay_21 = ''
            self.thu_21 = ''

    @api.onchange('cangay_21')
    def onchange_method_cangay_21(self):
        if self.cangay_21 == True:
            self.noidung_chieu_21 = None
            self.coso_chieu_21 = False
        else:
            self.coso_chieu_chieu_a_21 = False
            self.coso_chieu_chieu_b_21 = False
            self.coso_chieu_chieu_c_21 = False
            self.noidung_chieu_chieu_21 = None
            self.coso_chieu_chieu_21 = False
            self.thoigian_chieu_21 = 0
            self.giaovien_chieu_21 = None

    @api.onchange('ngaynghi_21')
    def onchange_method_ngaynghi_21(self):
        if self.ngaynghi_21 == True:
            self.cangay_21 = False
            self.noidung_sang_21 = None
            self.noidung_chieu_21 = None
            self.noidung_sang_char_21 = '休日'
            self.noidung_chieu_char_21 = '休日'
            self.coso_sang_21 = False
            self.coso_chieu_21 = False
            self.cosao_daotao_a_21 = False
            self.cosao_daotao_b_21 = False
            self.cosao_daotao_c_21 = False
            self.thoigian_21 = 0
            self.giaovien_21 = None
            self.noidung_chieu_chieu_21 = None
            self.coso_chieu_chieu_21 = False
            self.coso_chieu_chieu_a_21 = False
            self.coso_chieu_chieu_b_21 = False
            self.coso_chieu_chieu_c_21 = False
            self.thoigian_chieu_21 = 0
            self.giaovien_chieu_21 = None
        else:
            self.cangay_21 = False
            self.noidung_sang_21 = None
            self.noidung_chieu_21 = None
            self.noidung_sang_char_21 = ''
            self.noidung_chieu_char_21 = ''
            self.coso_sang_21 = False
            self.coso_chieu_21 = False
            self.cosao_daotao_a_21 = False
            self.cosao_daotao_b_21 = False
            self.cosao_daotao_c_21 = False
            self.thoigian_21 = 0
            self.giaovien_21 = None
            self.noidung_chieu_chieu_21 = None
            self.coso_chieu_chieu_21 = False
            self.coso_chieu_chieu_a_21 = False
            self.coso_chieu_chieu_b_21 = False
            self.coso_chieu_chieu_c_21 = False
            self.thoigian_chieu_21 = 0
            self.giaovien_chieu_21 = None

    @api.onchange('noidung_sang_21', 'noidung_chieu_21')
    def onchange_method_noidung_sang_21(self):
        if self.noidung_sang_21 and self.noidung_chieu_21:
            if self.noidung_sang_21.noidungdaotao != '休日' and self.noidung_chieu_21.noidungdaotao != '休日':
                self.thoigian_21 = 8
            elif self.noidung_sang_21.noidungdaotao != '休日' or self.noidung_chieu_21.noidungdaotao != '休日':
                self.thoigian_21 = 4
            else:
                self.thoigian_21 = 0
        elif self.noidung_sang_21 or self.noidung_chieu_21:
            if self.noidung_sang_21.noidungdaotao != '休日' and self.noidung_sang_21.noidungdaotao:
                self.thoigian_21 = 4
            elif self.noidung_chieu_21.noidungdaotao != '休日' and self.noidung_chieu_21.noidungdaotao:
                self.thoigian_21 = 4
            else:
                self.thoigian_21 = 0
        else:
            self.thoigian_21 = 0

    @api.onchange('noidung_chieu_chieu_21')
    def onchange_method_noidung_chieu_chieu_21(self):
        if self.noidung_chieu_chieu_21:
            if self.noidung_chieu_chieu_21.noidungdaotao != '休日':
                self.thoigian_chieu_21 = 4
        else:
            self.thoigian_chieu_21 = 0

    # Ngày _22
    # Ca sáng
    lich_laplich_22 = fields.Date()
    thang_22 = fields.Char(compute='_thang_22')
    ngay_22 = fields.Char(compute='_thang_22')
    thu_22 = fields.Char(compute='_thang_22')
    ngaynghi_22 = fields.Boolean()
    cangay_22 = fields.Boolean()
    noidung_sang_22 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_22 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_22 = fields.Char()
    noidung_chieu_char_22 = fields.Char()
    coso_sang_22 = fields.Boolean()
    coso_chieu_22 = fields.Boolean()
    cosao_daotao_a_22 = fields.Boolean(string='①')
    cosao_daotao_b_22 = fields.Boolean(string='②')
    cosao_daotao_c_22 = fields.Boolean(string='③')
    thoigian_22 = fields.Integer()
    giaovien_22 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_22 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_22 = fields.Boolean()
    coso_chieu_chieu_a_22 = fields.Boolean(string='①')
    coso_chieu_chieu_b_22 = fields.Boolean(string='②')
    coso_chieu_chieu_c_22 = fields.Boolean(string='③')
    thoigian_chieu_22 = fields.Integer()
    giaovien_chieu_22 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_22')
    def onchange_method_coso_chieu_chieu_a_22(self):
        if self.coso_chieu_chieu_a_22 == True:
            self.coso_chieu_chieu_b_22 = False
            self.coso_chieu_chieu_c_22 = False

    @api.onchange('coso_chieu_chieu_b_22')
    def onchange_method_coso_chieu_chieu_b_22(self):
        if self.coso_chieu_chieu_b_22 == True:
            self.coso_chieu_chieu_a_22 = False
            self.coso_chieu_chieu_c_22 = False

    @api.onchange('coso_chieu_chieu_c_22')
    def onchange_method_coso_chieu_chieu_c_22(self):
        if self.coso_chieu_chieu_c_22 == True:
            self.coso_chieu_chieu_a_22 = False
            self.coso_chieu_chieu_b_22 = False

    @api.onchange('cosao_daotao_a_22')
    def onchange_method_cosao_daotao_a_22(self):
        if self.cosao_daotao_a_22 == True:
            self.cosao_daotao_b_22 = False
            self.cosao_daotao_c_22 = False

    @api.onchange('cosao_daotao_b_22')
    def onchange_method_cosao_daotao_b_22(self):
        if self.cosao_daotao_b_22 == True:
            self.cosao_daotao_a_22 = False
            self.cosao_daotao_c_22 = False

    @api.onchange('cosao_daotao_c_22')
    def onchange_method_cosao_daotao_c_22(self):
        if self.cosao_daotao_c_22 == True:
            self.cosao_daotao_a_22 = False
            self.cosao_daotao_b_22 = False

    @api.one
    @api.depends('lich_laplich_22')
    def _thang_22(self):
        if self.lich_laplich_22:
            self.thang_22 = self.lich_laplich_22.month
            self.ngay_22 = self.lich_laplich_22.day
            self.thu_22 = convert_th(self.lich_laplich_22)
        else:
            self.thang_22 = ''
            self.ngay_22 = ''
            self.thu_22 = ''

    @api.onchange('cangay_22')
    def onchange_method_cangay_22(self):
        if self.cangay_22 == True:
            self.noidung_chieu_22 = None
            self.coso_chieu_22 = False
        else:
            self.coso_chieu_chieu_a_22 = False
            self.coso_chieu_chieu_b_22 = False
            self.coso_chieu_chieu_c_22 = False
            self.noidung_chieu_chieu_22 = None
            self.coso_chieu_chieu_22 = False
            self.thoigian_chieu_22 = 0
            self.giaovien_chieu_22 = None

    @api.onchange('ngaynghi_22')
    def onchange_method_ngaynghi_22(self):
        if self.ngaynghi_22 == True:
            self.cangay_22 = False
            self.noidung_sang_22 = None
            self.noidung_chieu_22 = None
            self.noidung_sang_char_22 = '休日'
            self.noidung_chieu_char_22 = '休日'
            self.coso_sang_22 = False
            self.coso_chieu_22 = False
            self.cosao_daotao_a_22 = False
            self.cosao_daotao_b_22 = False
            self.cosao_daotao_c_22 = False
            self.thoigian_22 = 0
            self.giaovien_22 = None
            self.noidung_chieu_chieu_22 = None
            self.coso_chieu_chieu_22 = False
            self.coso_chieu_chieu_a_22 = False
            self.coso_chieu_chieu_b_22 = False
            self.coso_chieu_chieu_c_22 = False
            self.thoigian_chieu_22 = 0
            self.giaovien_chieu_22 = None
        else:
            self.cangay_22 = False
            self.noidung_sang_22 = None
            self.noidung_chieu_22 = None
            self.noidung_sang_char_22 = ''
            self.noidung_chieu_char_22 = ''
            self.coso_sang_22 = False
            self.coso_chieu_22 = False
            self.cosao_daotao_a_22 = False
            self.cosao_daotao_b_22 = False
            self.cosao_daotao_c_22 = False
            self.thoigian_22 = 0
            self.giaovien_22 = None
            self.noidung_chieu_chieu_22 = None
            self.coso_chieu_chieu_22 = False
            self.coso_chieu_chieu_a_22 = False
            self.coso_chieu_chieu_b_22 = False
            self.coso_chieu_chieu_c_22 = False
            self.thoigian_chieu_22 = 0
            self.giaovien_chieu_22 = None

    @api.onchange('noidung_sang_22', 'noidung_chieu_22')
    def onchange_method_noidung_sang_22(self):
        if self.noidung_sang_22 and self.noidung_chieu_22:
            if self.noidung_sang_22.noidungdaotao != '休日' and self.noidung_chieu_22.noidungdaotao != '休日':
                self.thoigian_22 = 8
            elif self.noidung_sang_22.noidungdaotao != '休日' or self.noidung_chieu_22.noidungdaotao != '休日':
                self.thoigian_22 = 4
            else:
                self.thoigian_22 = 0
        elif self.noidung_sang_22 or self.noidung_chieu_22:
            if self.noidung_sang_22.noidungdaotao != '休日' and self.noidung_sang_22.noidungdaotao:
                self.thoigian_22 = 4
            elif self.noidung_chieu_22.noidungdaotao != '休日' and self.noidung_chieu_22.noidungdaotao:
                self.thoigian_22 = 4
            else:
                self.thoigian_22 = 0
        else:
            self.thoigian_22 = 0

    @api.onchange('noidung_chieu_chieu_22')
    def onchange_method_noidung_chieu_chieu_22(self):
        if self.noidung_chieu_chieu_22:
            if self.noidung_chieu_chieu_22.noidungdaotao != '休日':
                self.thoigian_chieu_22 = 4
        else:
            self.thoigian_chieu_22 = 0

    # Ngày _23
    # Ca sáng
    lich_laplich_23 = fields.Date()
    thang_23 = fields.Char(compute='_thang_23')
    ngay_23 = fields.Char(compute='_thang_23')
    thu_23 = fields.Char(compute='_thang_23')
    ngaynghi_23 = fields.Boolean()
    cangay_23 = fields.Boolean()
    noidung_sang_23 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_23 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_23 = fields.Char()
    noidung_chieu_char_23 = fields.Char()
    coso_sang_23 = fields.Boolean()
    coso_chieu_23 = fields.Boolean()
    cosao_daotao_a_23 = fields.Boolean(string='①')
    cosao_daotao_b_23 = fields.Boolean(string='②')
    cosao_daotao_c_23 = fields.Boolean(string='③')
    thoigian_23 = fields.Integer()
    giaovien_23 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_23 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_23 = fields.Boolean()
    coso_chieu_chieu_a_23 = fields.Boolean(string='①')
    coso_chieu_chieu_b_23 = fields.Boolean(string='②')
    coso_chieu_chieu_c_23 = fields.Boolean(string='③')
    thoigian_chieu_23 = fields.Integer()
    giaovien_chieu_23 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_23')
    def onchange_method_coso_chieu_chieu_a_23(self):
        if self.coso_chieu_chieu_a_23 == True:
            self.coso_chieu_chieu_b_23 = False
            self.coso_chieu_chieu_c_23 = False

    @api.onchange('coso_chieu_chieu_b_23')
    def onchange_method_coso_chieu_chieu_b_23(self):
        if self.coso_chieu_chieu_b_23 == True:
            self.coso_chieu_chieu_a_23 = False
            self.coso_chieu_chieu_c_23 = False

    @api.onchange('coso_chieu_chieu_c_23')
    def onchange_method_coso_chieu_chieu_c_23(self):
        if self.coso_chieu_chieu_c_23 == True:
            self.coso_chieu_chieu_a_23 = False
            self.coso_chieu_chieu_b_23 = False

    @api.onchange('cosao_daotao_a_23')
    def onchange_method_cosao_daotao_a_23(self):
        if self.cosao_daotao_a_23 == True:
            self.cosao_daotao_b_23 = False
            self.cosao_daotao_c_23 = False

    @api.onchange('cosao_daotao_b_23')
    def onchange_method_cosao_daotao_b_23(self):
        if self.cosao_daotao_b_23 == True:
            self.cosao_daotao_a_23 = False
            self.cosao_daotao_c_23 = False

    @api.onchange('cosao_daotao_c_23')
    def onchange_method_cosao_daotao_c_23(self):
        if self.cosao_daotao_c_23 == True:
            self.cosao_daotao_a_23 = False
            self.cosao_daotao_b_23 = False

    @api.one
    @api.depends('lich_laplich_23')
    def _thang_23(self):
        if self.lich_laplich_23:
            self.thang_23 = self.lich_laplich_23.month
            self.ngay_23 = self.lich_laplich_23.day
            self.thu_23 = convert_th(self.lich_laplich_23)
        else:
            self.thang_23 = ''
            self.ngay_23 = ''
            self.thu_23 = ''

    @api.onchange('cangay_23')
    def onchange_method_cangay_23(self):
        if self.cangay_23 == True:
            self.noidung_chieu_23 = None
            self.coso_chieu_23 = False
        else:
            self.coso_chieu_chieu_a_23 = False
            self.coso_chieu_chieu_b_23 = False
            self.coso_chieu_chieu_c_23 = False
            self.noidung_chieu_chieu_23 = None
            self.coso_chieu_chieu_23 = False
            self.thoigian_chieu_23 = 0
            self.giaovien_chieu_23 = None

    @api.onchange('ngaynghi_23')
    def onchange_method_ngaynghi_23(self):
        if self.ngaynghi_23 == True:
            self.cangay_23 = False
            self.noidung_sang_23 = None
            self.noidung_chieu_23 = None
            self.noidung_sang_char_23 = '休日'
            self.noidung_chieu_char_23 = '休日'
            self.coso_sang_23 = False
            self.coso_chieu_23 = False
            self.cosao_daotao_a_23 = False
            self.cosao_daotao_b_23 = False
            self.cosao_daotao_c_23 = False
            self.thoigian_23 = 0
            self.giaovien_23 = None
            self.noidung_chieu_chieu_23 = None
            self.coso_chieu_chieu_23 = False
            self.coso_chieu_chieu_a_23 = False
            self.coso_chieu_chieu_b_23 = False
            self.coso_chieu_chieu_c_23 = False
            self.thoigian_chieu_23 = 0
            self.giaovien_chieu_23 = None
        else:
            self.cangay_23 = False
            self.noidung_sang_23 = None
            self.noidung_chieu_23 = None
            self.noidung_sang_char_23 = ''
            self.noidung_chieu_char_23 = ''
            self.coso_sang_23 = False
            self.coso_chieu_23 = False
            self.cosao_daotao_a_23 = False
            self.cosao_daotao_b_23 = False
            self.cosao_daotao_c_23 = False
            self.thoigian_23 = 0
            self.giaovien_23 = None
            self.noidung_chieu_chieu_23 = None
            self.coso_chieu_chieu_23 = False
            self.coso_chieu_chieu_a_23 = False
            self.coso_chieu_chieu_b_23 = False
            self.coso_chieu_chieu_c_23 = False
            self.thoigian_chieu_23 = 0
            self.giaovien_chieu_23 = None

    @api.onchange('noidung_sang_23', 'noidung_chieu_23')
    def onchange_method_noidung_sang_23(self):
        if self.noidung_sang_23 and self.noidung_chieu_23:
            if self.noidung_sang_23.noidungdaotao != '休日' and self.noidung_chieu_23.noidungdaotao != '休日':
                self.thoigian_23 = 8
            elif self.noidung_sang_23.noidungdaotao != '休日' or self.noidung_chieu_23.noidungdaotao != '休日':
                self.thoigian_23 = 4
            else:
                self.thoigian_23 = 0
        elif self.noidung_sang_23 or self.noidung_chieu_23:
            if self.noidung_sang_23.noidungdaotao != '休日' and self.noidung_sang_23.noidungdaotao:
                self.thoigian_23 = 4
            elif self.noidung_chieu_23.noidungdaotao != '休日' and self.noidung_chieu_23.noidungdaotao:
                self.thoigian_23 = 4
            else:
                self.thoigian_23 = 0
        else:
            self.thoigian_23 = 0

    @api.onchange('noidung_chieu_chieu_23')
    def onchange_method_noidung_chieu_chieu_23(self):
        if self.noidung_chieu_chieu_23:
            if self.noidung_chieu_chieu_23.noidungdaotao != '休日':
                self.thoigian_chieu_23 = 4
        else:
            self.thoigian_chieu_23 = 0

    # Ngày _24
    # Ca sáng
    lich_laplich_24 = fields.Date()
    thang_24 = fields.Char(compute='_thang_24')
    ngay_24 = fields.Char(compute='_thang_24')
    thu_24 = fields.Char(compute='_thang_24')
    ngaynghi_24 = fields.Boolean()
    cangay_24 = fields.Boolean()
    noidung_sang_24 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_24 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_24 = fields.Char()
    noidung_chieu_char_24 = fields.Char()
    coso_sang_24 = fields.Boolean()
    coso_chieu_24 = fields.Boolean()
    cosao_daotao_a_24 = fields.Boolean(string='①')
    cosao_daotao_b_24 = fields.Boolean(string='②')
    cosao_daotao_c_24 = fields.Boolean(string='③')
    thoigian_24 = fields.Integer()
    giaovien_24 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_24 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_24 = fields.Boolean()
    coso_chieu_chieu_a_24 = fields.Boolean(string='①')
    coso_chieu_chieu_b_24 = fields.Boolean(string='②')
    coso_chieu_chieu_c_24 = fields.Boolean(string='③')
    thoigian_chieu_24 = fields.Integer()
    giaovien_chieu_24 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_24')
    def onchange_method_coso_chieu_chieu_a_24(self):
        if self.coso_chieu_chieu_a_24 == True:
            self.coso_chieu_chieu_b_24 = False
            self.coso_chieu_chieu_c_24 = False

    @api.onchange('coso_chieu_chieu_b_24')
    def onchange_method_coso_chieu_chieu_b_24(self):
        if self.coso_chieu_chieu_b_24 == True:
            self.coso_chieu_chieu_a_24 = False
            self.coso_chieu_chieu_c_24 = False

    @api.onchange('coso_chieu_chieu_c_24')
    def onchange_method_coso_chieu_chieu_c_24(self):
        if self.coso_chieu_chieu_c_24 == True:
            self.coso_chieu_chieu_a_24 = False
            self.coso_chieu_chieu_b_24 = False

    @api.onchange('cosao_daotao_a_24')
    def onchange_method_cosao_daotao_a_24(self):
        if self.cosao_daotao_a_24 == True:
            self.cosao_daotao_b_24 = False
            self.cosao_daotao_c_24 = False

    @api.onchange('cosao_daotao_b_24')
    def onchange_method_cosao_daotao_b_24(self):
        if self.cosao_daotao_b_24 == True:
            self.cosao_daotao_a_24 = False
            self.cosao_daotao_c_24 = False

    @api.onchange('cosao_daotao_c_24')
    def onchange_method_cosao_daotao_c_24(self):
        if self.cosao_daotao_c_24 == True:
            self.cosao_daotao_a_24 = False
            self.cosao_daotao_b_24 = False

    @api.one
    @api.depends('lich_laplich_24')
    def _thang_24(self):
        if self.lich_laplich_24:
            self.thang_24 = self.lich_laplich_24.month
            self.ngay_24 = self.lich_laplich_24.day
            self.thu_24 = convert_th(self.lich_laplich_24)
        else:
            self.thang_24 = ''
            self.ngay_24 = ''
            self.thu_24 = ''

    @api.onchange('cangay_24')
    def onchange_method_cangay_24(self):
        if self.cangay_24 == True:
            self.noidung_chieu_24 = None
            self.coso_chieu_24 = False
        else:
            self.coso_chieu_chieu_a_24 = False
            self.coso_chieu_chieu_b_24 = False
            self.coso_chieu_chieu_c_24 = False
            self.noidung_chieu_chieu_24 = None
            self.coso_chieu_chieu_24 = False
            self.thoigian_chieu_24 = 0
            self.giaovien_chieu_24 = None

    @api.onchange('ngaynghi_24')
    def onchange_method_ngaynghi_24(self):
        if self.ngaynghi_24 == True:
            self.cangay_24 = False
            self.noidung_sang_24 = None
            self.noidung_chieu_24 = None
            self.noidung_sang_char_24 = '休日'
            self.noidung_chieu_char_24 = '休日'
            self.coso_sang_24 = False
            self.coso_chieu_24 = False
            self.cosao_daotao_a_24 = False
            self.cosao_daotao_b_24 = False
            self.cosao_daotao_c_24 = False
            self.thoigian_24 = 0
            self.giaovien_24 = None
            self.noidung_chieu_chieu_24 = None
            self.coso_chieu_chieu_24 = False
            self.coso_chieu_chieu_a_24 = False
            self.coso_chieu_chieu_b_24 = False
            self.coso_chieu_chieu_c_24 = False
            self.thoigian_chieu_24 = 0
            self.giaovien_chieu_24 = None
        else:
            self.cangay_24 = False
            self.noidung_sang_24 = None
            self.noidung_chieu_24 = None
            self.noidung_sang_char_24 = ''
            self.noidung_chieu_char_24 = ''
            self.coso_sang_24 = False
            self.coso_chieu_24 = False
            self.cosao_daotao_a_24 = False
            self.cosao_daotao_b_24 = False
            self.cosao_daotao_c_24 = False
            self.thoigian_24 = 0
            self.giaovien_24 = None
            self.noidung_chieu_chieu_24 = None
            self.coso_chieu_chieu_24 = False
            self.coso_chieu_chieu_a_24 = False
            self.coso_chieu_chieu_b_24 = False
            self.coso_chieu_chieu_c_24 = False
            self.thoigian_chieu_24 = 0
            self.giaovien_chieu_24 = None

    @api.onchange('noidung_sang_24', 'noidung_chieu_24')
    def onchange_method_noidung_sang_24(self):
        if self.noidung_sang_24 and self.noidung_chieu_24:
            if self.noidung_sang_24.noidungdaotao != '休日' and self.noidung_chieu_24.noidungdaotao != '休日':
                self.thoigian_24 = 8
            elif self.noidung_sang_24.noidungdaotao != '休日' or self.noidung_chieu_24.noidungdaotao != '休日':
                self.thoigian_24 = 4
            else:
                self.thoigian_24 = 0
        elif self.noidung_sang_24 or self.noidung_chieu_24:
            if self.noidung_sang_24.noidungdaotao != '休日' and self.noidung_sang_24.noidungdaotao:
                self.thoigian_24 = 4
            elif self.noidung_chieu_24.noidungdaotao != '休日' and self.noidung_chieu_24.noidungdaotao:
                self.thoigian_24 = 4
            else:
                self.thoigian_24 = 0
        else:
            self.thoigian_24 = 0

    @api.onchange('noidung_chieu_chieu_24')
    def onchange_method_noidung_chieu_chieu_24(self):
        if self.noidung_chieu_chieu_24:
            if self.noidung_chieu_chieu_24.noidungdaotao != '休日':
                self.thoigian_chieu_24 = 4
        else:
            self.thoigian_chieu_24 = 0

    # Ngày _25
    # Ca sáng
    lich_laplich_25 = fields.Date()
    thang_25 = fields.Char(compute='_thang_25')
    ngay_25 = fields.Char(compute='_thang_25')
    thu_25 = fields.Char(compute='_thang_25')
    ngaynghi_25 = fields.Boolean()
    cangay_25 = fields.Boolean()
    noidung_sang_25 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_25 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_25 = fields.Char()
    noidung_chieu_char_25 = fields.Char()
    coso_sang_25 = fields.Boolean()
    coso_chieu_25 = fields.Boolean()
    cosao_daotao_a_25 = fields.Boolean(string='①')
    cosao_daotao_b_25 = fields.Boolean(string='②')
    cosao_daotao_c_25 = fields.Boolean(string='③')
    thoigian_25 = fields.Integer()
    giaovien_25 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_25 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_25 = fields.Boolean()
    coso_chieu_chieu_a_25 = fields.Boolean(string='①')
    coso_chieu_chieu_b_25 = fields.Boolean(string='②')
    coso_chieu_chieu_c_25 = fields.Boolean(string='③')
    thoigian_chieu_25 = fields.Integer()
    giaovien_chieu_25 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_25')
    def onchange_method_coso_chieu_chieu_a_25(self):
        if self.coso_chieu_chieu_a_25 == True:
            self.coso_chieu_chieu_b_25 = False
            self.coso_chieu_chieu_c_25 = False

    @api.onchange('coso_chieu_chieu_b_25')
    def onchange_method_coso_chieu_chieu_b_25(self):
        if self.coso_chieu_chieu_b_25 == True:
            self.coso_chieu_chieu_a_25 = False
            self.coso_chieu_chieu_c_25 = False

    @api.onchange('coso_chieu_chieu_c_25')
    def onchange_method_coso_chieu_chieu_c_25(self):
        if self.coso_chieu_chieu_c_25 == True:
            self.coso_chieu_chieu_a_25 = False
            self.coso_chieu_chieu_b_25 = False

    @api.onchange('cosao_daotao_a_25')
    def onchange_method_cosao_daotao_a_25(self):
        if self.cosao_daotao_a_25 == True:
            self.cosao_daotao_b_25 = False
            self.cosao_daotao_c_25 = False

    @api.onchange('cosao_daotao_b_25')
    def onchange_method_cosao_daotao_b_25(self):
        if self.cosao_daotao_b_25 == True:
            self.cosao_daotao_a_25 = False
            self.cosao_daotao_c_25 = False

    @api.onchange('cosao_daotao_c_25')
    def onchange_method_cosao_daotao_c_25(self):
        if self.cosao_daotao_c_25 == True:
            self.cosao_daotao_a_25 = False
            self.cosao_daotao_b_25 = False

    @api.one
    @api.depends('lich_laplich_25')
    def _thang_25(self):
        if self.lich_laplich_25:
            self.thang_25 = self.lich_laplich_25.month
            self.ngay_25 = self.lich_laplich_25.day
            self.thu_25 = convert_th(self.lich_laplich_25)
        else:
            self.thang_25 = ''
            self.ngay_25 = ''
            self.thu_25 = ''

    @api.onchange('cangay_25')
    def onchange_method_cangay_25(self):
        if self.cangay_25 == True:
            self.noidung_chieu_25 = None
            self.coso_chieu_25 = False
        else:
            self.coso_chieu_chieu_a_25 = False
            self.coso_chieu_chieu_b_25 = False
            self.coso_chieu_chieu_c_25 = False
            self.noidung_chieu_chieu_25 = None
            self.coso_chieu_chieu_25 = False
            self.thoigian_chieu_25 = 0
            self.giaovien_chieu_25 = None

    @api.onchange('ngaynghi_25')
    def onchange_method_ngaynghi_25(self):
        if self.ngaynghi_25 == True:
            self.cangay_25 = False
            self.noidung_sang_25 = None
            self.noidung_chieu_25 = None
            self.noidung_sang_char_25 = '休日'
            self.noidung_chieu_char_25 = '休日'
            self.coso_sang_25 = False
            self.coso_chieu_25 = False
            self.cosao_daotao_a_25 = False
            self.cosao_daotao_b_25 = False
            self.cosao_daotao_c_25 = False
            self.thoigian_25 = 0
            self.giaovien_25 = None
            self.noidung_chieu_chieu_25 = None
            self.coso_chieu_chieu_25 = False
            self.coso_chieu_chieu_a_25 = False
            self.coso_chieu_chieu_b_25 = False
            self.coso_chieu_chieu_c_25 = False
            self.thoigian_chieu_25 = 0
            self.giaovien_chieu_25 = None
        else:
            self.cangay_25 = False
            self.noidung_sang_25 = None
            self.noidung_chieu_25 = None
            self.noidung_sang_char_25 = ''
            self.noidung_chieu_char_25 = ''
            self.coso_sang_25 = False
            self.coso_chieu_25 = False
            self.cosao_daotao_a_25 = False
            self.cosao_daotao_b_25 = False
            self.cosao_daotao_c_25 = False
            self.thoigian_25 = 0
            self.giaovien_25 = None
            self.noidung_chieu_chieu_25 = None
            self.coso_chieu_chieu_25 = False
            self.coso_chieu_chieu_a_25 = False
            self.coso_chieu_chieu_b_25 = False
            self.coso_chieu_chieu_c_25 = False
            self.thoigian_chieu_25 = 0
            self.giaovien_chieu_25 = None

    @api.onchange('noidung_sang_25', 'noidung_chieu_25')
    def onchange_method_noidung_sang_25(self):
        if self.noidung_sang_25 and self.noidung_chieu_25:
            if self.noidung_sang_25.noidungdaotao != '休日' and self.noidung_chieu_25.noidungdaotao != '休日':
                self.thoigian_25 = 8
            elif self.noidung_sang_25.noidungdaotao != '休日' or self.noidung_chieu_25.noidungdaotao != '休日':
                self.thoigian_25 = 4
            else:
                self.thoigian_25 = 0
        elif self.noidung_sang_25 or self.noidung_chieu_25:
            if self.noidung_sang_25.noidungdaotao != '休日' and self.noidung_sang_25.noidungdaotao:
                self.thoigian_25 = 4
            elif self.noidung_chieu_25.noidungdaotao != '休日' and self.noidung_chieu_25.noidungdaotao:
                self.thoigian_25 = 4
            else:
                self.thoigian_25 = 0
        else:
            self.thoigian_25 = 0

    @api.onchange('noidung_chieu_chieu_25')
    def onchange_method_noidung_chieu_chieu_25(self):
        if self.noidung_chieu_chieu_25:
            if self.noidung_chieu_chieu_25.noidungdaotao != '休日':
                self.thoigian_chieu_25 = 4
        else:
            self.thoigian_chieu_25 = 0

    # Ngày _26
    # Ca sáng
    lich_laplich_26 = fields.Date()
    thang_26 = fields.Char(compute='_thang_26')
    ngay_26 = fields.Char(compute='_thang_26')
    thu_26 = fields.Char(compute='_thang_26')
    ngaynghi_26 = fields.Boolean()
    cangay_26 = fields.Boolean()
    noidung_sang_26 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_26 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_26 = fields.Char()
    noidung_chieu_char_26 = fields.Char()
    coso_sang_26 = fields.Boolean()
    coso_chieu_26 = fields.Boolean()
    cosao_daotao_a_26 = fields.Boolean(string='①')
    cosao_daotao_b_26 = fields.Boolean(string='②')
    cosao_daotao_c_26 = fields.Boolean(string='③')
    thoigian_26 = fields.Integer()
    giaovien_26 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_26 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_26 = fields.Boolean()
    coso_chieu_chieu_a_26 = fields.Boolean(string='①')
    coso_chieu_chieu_b_26 = fields.Boolean(string='②')
    coso_chieu_chieu_c_26 = fields.Boolean(string='③')
    thoigian_chieu_26 = fields.Integer()
    giaovien_chieu_26 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_26')
    def onchange_method_coso_chieu_chieu_a_26(self):
        if self.coso_chieu_chieu_a_26 == True:
            self.coso_chieu_chieu_b_26 = False
            self.coso_chieu_chieu_c_26 = False

    @api.onchange('coso_chieu_chieu_b_26')
    def onchange_method_coso_chieu_chieu_b_26(self):
        if self.coso_chieu_chieu_b_26 == True:
            self.coso_chieu_chieu_a_26 = False
            self.coso_chieu_chieu_c_26 = False

    @api.onchange('coso_chieu_chieu_c_26')
    def onchange_method_coso_chieu_chieu_c_26(self):
        if self.coso_chieu_chieu_c_26 == True:
            self.coso_chieu_chieu_a_26 = False
            self.coso_chieu_chieu_b_26 = False

    @api.onchange('cosao_daotao_a_26')
    def onchange_method_cosao_daotao_a_26(self):
        if self.cosao_daotao_a_26 == True:
            self.cosao_daotao_b_26 = False
            self.cosao_daotao_c_26 = False

    @api.onchange('cosao_daotao_b_26')
    def onchange_method_cosao_daotao_b_26(self):
        if self.cosao_daotao_b_26 == True:
            self.cosao_daotao_a_26 = False
            self.cosao_daotao_c_26 = False

    @api.onchange('cosao_daotao_c_26')
    def onchange_method_cosao_daotao_c_26(self):
        if self.cosao_daotao_c_26 == True:
            self.cosao_daotao_a_26 = False
            self.cosao_daotao_b_26 = False

    @api.one
    @api.depends('lich_laplich_26')
    def _thang_26(self):
        if self.lich_laplich_26:
            self.thang_26 = self.lich_laplich_26.month
            self.ngay_26 = self.lich_laplich_26.day
            self.thu_26 = convert_th(self.lich_laplich_26)
        else:
            self.thang_26 = ''
            self.ngay_26 = ''
            self.thu_26 = ''

    @api.onchange('cangay_26')
    def onchange_method_cangay_26(self):
        if self.cangay_26 == True:
            self.noidung_chieu_26 = None
            self.coso_chieu_26 = False
        else:
            self.coso_chieu_chieu_a_26 = False
            self.coso_chieu_chieu_b_26 = False
            self.coso_chieu_chieu_c_26 = False
            self.noidung_chieu_chieu_26 = None
            self.coso_chieu_chieu_26 = False
            self.thoigian_chieu_26 = 0
            self.giaovien_chieu_26 = None

    @api.onchange('ngaynghi_26')
    def onchange_method_ngaynghi_26(self):
        if self.ngaynghi_26 == True:
            self.cangay_26 = False
            self.noidung_sang_26 = None
            self.noidung_chieu_26 = None
            self.noidung_sang_char_26 = '休日'
            self.noidung_chieu_char_26 = '休日'
            self.coso_sang_26 = False
            self.coso_chieu_26 = False
            self.cosao_daotao_a_26 = False
            self.cosao_daotao_b_26 = False
            self.cosao_daotao_c_26 = False
            self.thoigian_26 = 0
            self.giaovien_26 = None
            self.noidung_chieu_chieu_26 = None
            self.coso_chieu_chieu_26 = False
            self.coso_chieu_chieu_a_26 = False
            self.coso_chieu_chieu_b_26 = False
            self.coso_chieu_chieu_c_26 = False
            self.thoigian_chieu_26 = 0
            self.giaovien_chieu_26 = None
        else:
            self.cangay_26 = False
            self.noidung_sang_26 = None
            self.noidung_chieu_26 = None
            self.noidung_sang_char_26 = ''
            self.noidung_chieu_char_26 = ''
            self.coso_sang_26 = False
            self.coso_chieu_26 = False
            self.cosao_daotao_a_26 = False
            self.cosao_daotao_b_26 = False
            self.cosao_daotao_c_26 = False
            self.thoigian_26 = 0
            self.giaovien_26 = None
            self.noidung_chieu_chieu_26 = None
            self.coso_chieu_chieu_26 = False
            self.coso_chieu_chieu_a_26 = False
            self.coso_chieu_chieu_b_26 = False
            self.coso_chieu_chieu_c_26 = False
            self.thoigian_chieu_26 = 0
            self.giaovien_chieu_26 = None

    @api.onchange('noidung_sang_26', 'noidung_chieu_26')
    def onchange_method_noidung_sang_26(self):
        if self.noidung_sang_26 and self.noidung_chieu_26:
            if self.noidung_sang_26.noidungdaotao != '休日' and self.noidung_chieu_26.noidungdaotao != '休日':
                self.thoigian_26 = 8
            elif self.noidung_sang_26.noidungdaotao != '休日' or self.noidung_chieu_26.noidungdaotao != '休日':
                self.thoigian_26 = 4
            else:
                self.thoigian_26 = 0
        elif self.noidung_sang_26 or self.noidung_chieu_26:
            if self.noidung_sang_26.noidungdaotao != '休日' and self.noidung_sang_26.noidungdaotao:
                self.thoigian_26 = 4
            elif self.noidung_chieu_26.noidungdaotao != '休日' and self.noidung_chieu_26.noidungdaotao:
                self.thoigian_26 = 4
            else:
                self.thoigian_26 = 0
        else:
            self.thoigian_26 = 0

    @api.onchange('noidung_chieu_chieu_26')
    def onchange_method_noidung_chieu_chieu_26(self):
        if self.noidung_chieu_chieu_26:
            if self.noidung_chieu_chieu_26.noidungdaotao != '休日':
                self.thoigian_chieu_26 = 4
        else:
            self.thoigian_chieu_26 = 0

    # Ngày _27
    # Ca sáng
    lich_laplich_27 = fields.Date()
    thang_27 = fields.Char(compute='_thang_27')
    ngay_27 = fields.Char(compute='_thang_27')
    thu_27 = fields.Char(compute='_thang_27')
    ngaynghi_27 = fields.Boolean()
    cangay_27 = fields.Boolean()
    noidung_sang_27 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_27 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_27 = fields.Char()
    noidung_chieu_char_27 = fields.Char()
    coso_sang_27 = fields.Boolean()
    coso_chieu_27 = fields.Boolean()
    cosao_daotao_a_27 = fields.Boolean(string='①')
    cosao_daotao_b_27 = fields.Boolean(string='②')
    cosao_daotao_c_27 = fields.Boolean(string='③')
    thoigian_27 = fields.Integer()
    giaovien_27 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_27 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_27 = fields.Boolean()
    coso_chieu_chieu_a_27 = fields.Boolean(string='①')
    coso_chieu_chieu_b_27 = fields.Boolean(string='②')
    coso_chieu_chieu_c_27 = fields.Boolean(string='③')
    thoigian_chieu_27 = fields.Integer()
    giaovien_chieu_27 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_27')
    def onchange_method_coso_chieu_chieu_a_27(self):
        if self.coso_chieu_chieu_a_27 == True:
            self.coso_chieu_chieu_b_27 = False
            self.coso_chieu_chieu_c_27 = False

    @api.onchange('coso_chieu_chieu_b_27')
    def onchange_method_coso_chieu_chieu_b_27(self):
        if self.coso_chieu_chieu_b_27 == True:
            self.coso_chieu_chieu_a_27 = False
            self.coso_chieu_chieu_c_27 = False

    @api.onchange('coso_chieu_chieu_c_27')
    def onchange_method_coso_chieu_chieu_c_27(self):
        if self.coso_chieu_chieu_c_27 == True:
            self.coso_chieu_chieu_a_27 = False
            self.coso_chieu_chieu_b_27 = False

    @api.onchange('cosao_daotao_a_27')
    def onchange_method_cosao_daotao_a_27(self):
        if self.cosao_daotao_a_27 == True:
            self.cosao_daotao_b_27 = False
            self.cosao_daotao_c_27 = False

    @api.onchange('cosao_daotao_b_27')
    def onchange_method_cosao_daotao_b_27(self):
        if self.cosao_daotao_b_27 == True:
            self.cosao_daotao_a_27 = False
            self.cosao_daotao_c_27 = False

    @api.onchange('cosao_daotao_c_27')
    def onchange_method_cosao_daotao_c_27(self):
        if self.cosao_daotao_c_27 == True:
            self.cosao_daotao_a_27 = False
            self.cosao_daotao_b_27 = False

    @api.one
    @api.depends('lich_laplich_27')
    def _thang_27(self):
        if self.lich_laplich_27:
            self.thang_27 = self.lich_laplich_27.month
            self.ngay_27 = self.lich_laplich_27.day
            self.thu_27 = convert_th(self.lich_laplich_27)
        else:
            self.thang_27 = ''
            self.ngay_27 = ''
            self.thu_27 = ''

    @api.onchange('cangay_27')
    def onchange_method_cangay_27(self):
        if self.cangay_27 == True:
            self.noidung_chieu_27 = None
            self.coso_chieu_27 = False
        else:
            self.coso_chieu_chieu_a_27 = False
            self.coso_chieu_chieu_b_27 = False
            self.coso_chieu_chieu_c_27 = False
            self.noidung_chieu_chieu_27 = None
            self.coso_chieu_chieu_27 = False
            self.thoigian_chieu_27 = 0
            self.giaovien_chieu_27 = None

    @api.onchange('ngaynghi_27')
    def onchange_method_ngaynghi_27(self):
        if self.ngaynghi_27 == True:
            self.cangay_27 = False
            self.noidung_sang_27 = None
            self.noidung_chieu_27 = None
            self.noidung_sang_char_27 = '休日'
            self.noidung_chieu_char_27 = '休日'
            self.coso_sang_27 = False
            self.coso_chieu_27 = False
            self.cosao_daotao_a_27 = False
            self.cosao_daotao_b_27 = False
            self.cosao_daotao_c_27 = False
            self.thoigian_27 = 0
            self.giaovien_27 = None
            self.noidung_chieu_chieu_27 = None
            self.coso_chieu_chieu_27 = False
            self.coso_chieu_chieu_a_27 = False
            self.coso_chieu_chieu_b_27 = False
            self.coso_chieu_chieu_c_27 = False
            self.thoigian_chieu_27 = 0
            self.giaovien_chieu_27 = None
        else:
            self.cangay_27 = False
            self.noidung_sang_27 = None
            self.noidung_chieu_27 = None
            self.noidung_sang_char_27 = ''
            self.noidung_chieu_char_27 = ''
            self.coso_sang_27 = False
            self.coso_chieu_27 = False
            self.cosao_daotao_a_27 = False
            self.cosao_daotao_b_27 = False
            self.cosao_daotao_c_27 = False
            self.thoigian_27 = 0
            self.giaovien_27 = None
            self.noidung_chieu_chieu_27 = None
            self.coso_chieu_chieu_27 = False
            self.coso_chieu_chieu_a_27 = False
            self.coso_chieu_chieu_b_27 = False
            self.coso_chieu_chieu_c_27 = False
            self.thoigian_chieu_27 = 0
            self.giaovien_chieu_27 = None

    @api.onchange('noidung_sang_27', 'noidung_chieu_27')
    def onchange_method_noidung_sang_27(self):
        if self.noidung_sang_27 and self.noidung_chieu_27:
            if self.noidung_sang_27.noidungdaotao != '休日' and self.noidung_chieu_27.noidungdaotao != '休日':
                self.thoigian_27 = 8
            elif self.noidung_sang_27.noidungdaotao != '休日' or self.noidung_chieu_27.noidungdaotao != '休日':
                self.thoigian_27 = 4
            else:
                self.thoigian_27 = 0
        elif self.noidung_sang_27 or self.noidung_chieu_27:
            if self.noidung_sang_27.noidungdaotao != '休日' and self.noidung_sang_27.noidungdaotao:
                self.thoigian_27 = 4
            elif self.noidung_chieu_27.noidungdaotao != '休日' and self.noidung_chieu_27.noidungdaotao:
                self.thoigian_27 = 4
            else:
                self.thoigian_27 = 0
        else:
            self.thoigian_27 = 0

    @api.onchange('noidung_chieu_chieu_27')
    def onchange_method_noidung_chieu_chieu_27(self):
        if self.noidung_chieu_chieu_27:
            if self.noidung_chieu_chieu_27.noidungdaotao != '休日':
                self.thoigian_chieu_27 = 4
        else:
            self.thoigian_chieu_27 = 0

    # Ngày _28
    # Ca sáng
    lich_laplich_28 = fields.Date()
    thang_28 = fields.Char(compute='_thang_28')
    ngay_28 = fields.Char(compute='_thang_28')
    thu_28 = fields.Char(compute='_thang_28')
    ngaynghi_28 = fields.Boolean()
    cangay_28 = fields.Boolean()
    noidung_sang_28 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_28 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_28 = fields.Char()
    noidung_chieu_char_28 = fields.Char()
    coso_sang_28 = fields.Boolean()
    coso_chieu_28 = fields.Boolean()
    cosao_daotao_a_28 = fields.Boolean(string='①')
    cosao_daotao_b_28 = fields.Boolean(string='②')
    cosao_daotao_c_28 = fields.Boolean(string='③')
    thoigian_28 = fields.Integer()
    giaovien_28 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_28 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_28 = fields.Boolean()
    coso_chieu_chieu_a_28 = fields.Boolean(string='①')
    coso_chieu_chieu_b_28 = fields.Boolean(string='②')
    coso_chieu_chieu_c_28 = fields.Boolean(string='③')
    thoigian_chieu_28 = fields.Integer()
    giaovien_chieu_28 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_28')
    def onchange_method_coso_chieu_chieu_a_28(self):
        if self.coso_chieu_chieu_a_28 == True:
            self.coso_chieu_chieu_b_28 = False
            self.coso_chieu_chieu_c_28 = False

    @api.onchange('coso_chieu_chieu_b_28')
    def onchange_method_coso_chieu_chieu_b_28(self):
        if self.coso_chieu_chieu_b_28 == True:
            self.coso_chieu_chieu_a_28 = False
            self.coso_chieu_chieu_c_28 = False

    @api.onchange('coso_chieu_chieu_c_28')
    def onchange_method_coso_chieu_chieu_c_28(self):
        if self.coso_chieu_chieu_c_28 == True:
            self.coso_chieu_chieu_a_28 = False
            self.coso_chieu_chieu_b_28 = False

    @api.onchange('cosao_daotao_a_28')
    def onchange_method_cosao_daotao_a_28(self):
        if self.cosao_daotao_a_28 == True:
            self.cosao_daotao_b_28 = False
            self.cosao_daotao_c_28 = False

    @api.onchange('cosao_daotao_b_28')
    def onchange_method_cosao_daotao_b_28(self):
        if self.cosao_daotao_b_28 == True:
            self.cosao_daotao_a_28 = False
            self.cosao_daotao_c_28 = False

    @api.onchange('cosao_daotao_c_28')
    def onchange_method_cosao_daotao_c_28(self):
        if self.cosao_daotao_c_28 == True:
            self.cosao_daotao_a_28 = False
            self.cosao_daotao_b_28 = False

    @api.one
    @api.depends('lich_laplich_28')
    def _thang_28(self):
        if self.lich_laplich_28:
            self.thang_28 = self.lich_laplich_28.month
            self.ngay_28 = self.lich_laplich_28.day
            self.thu_28 = convert_th(self.lich_laplich_28)
        else:
            self.thang_28 = ''
            self.ngay_28 = ''
            self.thu_28 = ''

    @api.onchange('cangay_28')
    def onchange_method_cangay_28(self):
        if self.cangay_28 == True:
            self.noidung_chieu_28 = None
            self.coso_chieu_28 = False
        else:
            self.coso_chieu_chieu_a_28 = False
            self.coso_chieu_chieu_b_28 = False
            self.coso_chieu_chieu_c_28 = False
            self.noidung_chieu_chieu_28 = None
            self.coso_chieu_chieu_28 = False
            self.thoigian_chieu_28 = 0
            self.giaovien_chieu_28 = None

    @api.onchange('ngaynghi_28')
    def onchange_method_ngaynghi_28(self):
        if self.ngaynghi_28 == True:
            self.cangay_28 = False
            self.noidung_sang_28 = None
            self.noidung_chieu_28 = None
            self.noidung_sang_char_28 = '休日'
            self.noidung_chieu_char_28 = '休日'
            self.coso_sang_28 = False
            self.coso_chieu_28 = False
            self.cosao_daotao_a_28 = False
            self.cosao_daotao_b_28 = False
            self.cosao_daotao_c_28 = False
            self.thoigian_28 = 0
            self.giaovien_28 = None
            self.noidung_chieu_chieu_28 = None
            self.coso_chieu_chieu_28 = False
            self.coso_chieu_chieu_a_28 = False
            self.coso_chieu_chieu_b_28 = False
            self.coso_chieu_chieu_c_28 = False
            self.thoigian_chieu_28 = 0
            self.giaovien_chieu_28 = None
        else:
            self.cangay_28 = False
            self.noidung_sang_28 = None
            self.noidung_chieu_28 = None
            self.noidung_sang_char_28 = ''
            self.noidung_chieu_char_28 = ''
            self.coso_sang_28 = False
            self.coso_chieu_28 = False
            self.cosao_daotao_a_28 = False
            self.cosao_daotao_b_28 = False
            self.cosao_daotao_c_28 = False
            self.thoigian_28 = 0
            self.giaovien_28 = None
            self.noidung_chieu_chieu_28 = None
            self.coso_chieu_chieu_28 = False
            self.coso_chieu_chieu_a_28 = False
            self.coso_chieu_chieu_b_28 = False
            self.coso_chieu_chieu_c_28 = False
            self.thoigian_chieu_28 = 0
            self.giaovien_chieu_28 = None

    @api.onchange('noidung_sang_28', 'noidung_chieu_28')
    def onchange_method_noidung_sang_28(self):
        if self.noidung_sang_28 and self.noidung_chieu_28:
            if self.noidung_sang_28.noidungdaotao != '休日' and self.noidung_chieu_28.noidungdaotao != '休日':
                self.thoigian_28 = 8
            elif self.noidung_sang_28.noidungdaotao != '休日' or self.noidung_chieu_28.noidungdaotao != '休日':
                self.thoigian_28 = 4
            else:
                self.thoigian_28 = 0
        elif self.noidung_sang_28 or self.noidung_chieu_28:
            if self.noidung_sang_28.noidungdaotao != '休日' and self.noidung_sang_28.noidungdaotao:
                self.thoigian_28 = 4
            elif self.noidung_chieu_28.noidungdaotao != '休日' and self.noidung_chieu_28.noidungdaotao:
                self.thoigian_28 = 4
            else:
                self.thoigian_28 = 0
        else:
            self.thoigian_28 = 0

    @api.onchange('noidung_chieu_chieu_28')
    def onchange_method_noidung_chieu_chieu_28(self):
        if self.noidung_chieu_chieu_28:
            if self.noidung_chieu_chieu_28.noidungdaotao != '休日':
                self.thoigian_chieu_28 = 4
        else:
            self.thoigian_chieu_28 = 0

    # Ngày _29
    # Ca sáng
    lich_laplich_29 = fields.Date()
    thang_29 = fields.Char(compute='_thang_29')
    ngay_29 = fields.Char(compute='_thang_29')
    thu_29 = fields.Char(compute='_thang_29')
    ngaynghi_29 = fields.Boolean()
    cangay_29 = fields.Boolean()
    noidung_sang_29 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_29 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_29 = fields.Char()
    noidung_chieu_char_29 = fields.Char()
    coso_sang_29 = fields.Boolean()
    coso_chieu_29 = fields.Boolean()
    cosao_daotao_a_29 = fields.Boolean(string='①')
    cosao_daotao_b_29 = fields.Boolean(string='②')
    cosao_daotao_c_29 = fields.Boolean(string='③')
    thoigian_29 = fields.Integer()
    giaovien_29 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_29 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_29 = fields.Boolean()
    coso_chieu_chieu_a_29 = fields.Boolean(string='①')
    coso_chieu_chieu_b_29 = fields.Boolean(string='②')
    coso_chieu_chieu_c_29 = fields.Boolean(string='③')
    thoigian_chieu_29 = fields.Integer()
    giaovien_chieu_29 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_29')
    def onchange_method_coso_chieu_chieu_a_29(self):
        if self.coso_chieu_chieu_a_29 == True:
            self.coso_chieu_chieu_b_29 = False
            self.coso_chieu_chieu_c_29 = False

    @api.onchange('coso_chieu_chieu_b_29')
    def onchange_method_coso_chieu_chieu_b_29(self):
        if self.coso_chieu_chieu_b_29 == True:
            self.coso_chieu_chieu_a_29 = False
            self.coso_chieu_chieu_c_29 = False

    @api.onchange('coso_chieu_chieu_c_29')
    def onchange_method_coso_chieu_chieu_c_29(self):
        if self.coso_chieu_chieu_c_29 == True:
            self.coso_chieu_chieu_a_29 = False
            self.coso_chieu_chieu_b_29 = False

    @api.onchange('cosao_daotao_a_29')
    def onchange_method_cosao_daotao_a_29(self):
        if self.cosao_daotao_a_29 == True:
            self.cosao_daotao_b_29 = False
            self.cosao_daotao_c_29 = False

    @api.onchange('cosao_daotao_b_29')
    def onchange_method_cosao_daotao_b_29(self):
        if self.cosao_daotao_b_29 == True:
            self.cosao_daotao_a_29 = False
            self.cosao_daotao_c_29 = False

    @api.onchange('cosao_daotao_c_29')
    def onchange_method_cosao_daotao_c_29(self):
        if self.cosao_daotao_c_29 == True:
            self.cosao_daotao_a_29 = False
            self.cosao_daotao_b_29 = False

    @api.one
    @api.depends('lich_laplich_29')
    def _thang_29(self):
        if self.lich_laplich_29:
            self.thang_29 = self.lich_laplich_29.month
            self.ngay_29 = self.lich_laplich_29.day
            self.thu_29 = convert_th(self.lich_laplich_29)
        else:
            self.thang_29 = ''
            self.ngay_29 = ''
            self.thu_29 = ''

    @api.onchange('cangay_29')
    def onchange_method_cangay_29(self):
        if self.cangay_29 == True:
            self.noidung_chieu_29 = None
            self.coso_chieu_29 = False
        else:
            self.coso_chieu_chieu_a_29 = False
            self.coso_chieu_chieu_b_29 = False
            self.coso_chieu_chieu_c_29 = False
            self.noidung_chieu_chieu_29 = None
            self.coso_chieu_chieu_29 = False
            self.thoigian_chieu_29 = 0
            self.giaovien_chieu_29 = None

    @api.onchange('ngaynghi_29')
    def onchange_method_ngaynghi_29(self):
        if self.ngaynghi_29 == True:
            self.cangay_29 = False
            self.noidung_sang_29 = None
            self.noidung_chieu_29 = None
            self.noidung_sang_char_29 = '休日'
            self.noidung_chieu_char_29 = '休日'
            self.coso_sang_29 = False
            self.coso_chieu_29 = False
            self.cosao_daotao_a_29 = False
            self.cosao_daotao_b_29 = False
            self.cosao_daotao_c_29 = False
            self.thoigian_29 = 0
            self.giaovien_29 = None
            self.noidung_chieu_chieu_29 = None
            self.coso_chieu_chieu_29 = False
            self.coso_chieu_chieu_a_29 = False
            self.coso_chieu_chieu_b_29 = False
            self.coso_chieu_chieu_c_29 = False
            self.thoigian_chieu_29 = 0
            self.giaovien_chieu_29 = None
        else:
            self.cangay_29 = False
            self.noidung_sang_29 = None
            self.noidung_chieu_29 = None
            self.noidung_sang_char_29 = ''
            self.noidung_chieu_char_29 = ''
            self.coso_sang_29 = False
            self.coso_chieu_29 = False
            self.cosao_daotao_a_29 = False
            self.cosao_daotao_b_29 = False
            self.cosao_daotao_c_29 = False
            self.thoigian_29 = 0
            self.giaovien_29 = None
            self.noidung_chieu_chieu_29 = None
            self.coso_chieu_chieu_29 = False
            self.coso_chieu_chieu_a_29 = False
            self.coso_chieu_chieu_b_29 = False
            self.coso_chieu_chieu_c_29 = False
            self.thoigian_chieu_29 = 0
            self.giaovien_chieu_29 = None

    @api.onchange('noidung_sang_29', 'noidung_chieu_29')
    def onchange_method_noidung_sang_29(self):
        if self.noidung_sang_29 and self.noidung_chieu_29:
            if self.noidung_sang_29.noidungdaotao != '休日' and self.noidung_chieu_29.noidungdaotao != '休日':
                self.thoigian_29 = 8
            elif self.noidung_sang_29.noidungdaotao != '休日' or self.noidung_chieu_29.noidungdaotao != '休日':
                self.thoigian_29 = 4
            else:
                self.thoigian_29 = 0
        elif self.noidung_sang_29 or self.noidung_chieu_29:
            if self.noidung_sang_29.noidungdaotao != '休日' and self.noidung_sang_29.noidungdaotao:
                self.thoigian_29 = 4
            elif self.noidung_chieu_29.noidungdaotao != '休日' and self.noidung_chieu_29.noidungdaotao:
                self.thoigian_29 = 4
            else:
                self.thoigian_29 = 0
        else:
            self.thoigian_29 = 0

    @api.onchange('noidung_chieu_chieu_29')
    def onchange_method_noidung_chieu_chieu_29(self):
        if self.noidung_chieu_chieu_29:
            if self.noidung_chieu_chieu_29.noidungdaotao != '休日':
                self.thoigian_chieu_29 = 4
        else:
            self.thoigian_chieu_29 = 0

    # Ngày _30
    # Ca sáng
    lich_laplich_30 = fields.Date()
    thang_30 = fields.Char(compute='_thang_30')
    ngay_30 = fields.Char(compute='_thang_30')
    thu_30 = fields.Char(compute='_thang_30')
    ngaynghi_30 = fields.Boolean()
    cangay_30 = fields.Boolean()
    noidung_sang_30 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_30 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_30 = fields.Char()
    noidung_chieu_char_30 = fields.Char()
    coso_sang_30 = fields.Boolean()
    coso_chieu_30 = fields.Boolean()
    cosao_daotao_a_30 = fields.Boolean(string='①')
    cosao_daotao_b_30 = fields.Boolean(string='②')
    cosao_daotao_c_30 = fields.Boolean(string='③')
    thoigian_30 = fields.Integer()
    giaovien_30 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_30 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_30 = fields.Boolean()
    coso_chieu_chieu_a_30 = fields.Boolean(string='①')
    coso_chieu_chieu_b_30 = fields.Boolean(string='②')
    coso_chieu_chieu_c_30 = fields.Boolean(string='③')
    thoigian_chieu_30 = fields.Integer()
    giaovien_chieu_30 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_30')
    def onchange_method_coso_chieu_chieu_a_30(self):
        if self.coso_chieu_chieu_a_30 == True:
            self.coso_chieu_chieu_b_30 = False
            self.coso_chieu_chieu_c_30 = False

    @api.onchange('coso_chieu_chieu_b_30')
    def onchange_method_coso_chieu_chieu_b_30(self):
        if self.coso_chieu_chieu_b_30 == True:
            self.coso_chieu_chieu_a_30 = False
            self.coso_chieu_chieu_c_30 = False

    @api.onchange('coso_chieu_chieu_c_30')
    def onchange_method_coso_chieu_chieu_c_30(self):
        if self.coso_chieu_chieu_c_30 == True:
            self.coso_chieu_chieu_a_30 = False
            self.coso_chieu_chieu_b_30 = False

    @api.onchange('cosao_daotao_a_30')
    def onchange_method_cosao_daotao_a_30(self):
        if self.cosao_daotao_a_30 == True:
            self.cosao_daotao_b_30 = False
            self.cosao_daotao_c_30 = False

    @api.onchange('cosao_daotao_b_30')
    def onchange_method_cosao_daotao_b_30(self):
        if self.cosao_daotao_b_30 == True:
            self.cosao_daotao_a_30 = False
            self.cosao_daotao_c_30 = False

    @api.onchange('cosao_daotao_c_30')
    def onchange_method_cosao_daotao_c_30(self):
        if self.cosao_daotao_c_30 == True:
            self.cosao_daotao_a_30 = False
            self.cosao_daotao_b_30 = False

    @api.one
    @api.depends('lich_laplich_30')
    def _thang_30(self):
        if self.lich_laplich_30:
            self.thang_30 = self.lich_laplich_30.month
            self.ngay_30 = self.lich_laplich_30.day
            self.thu_30 = convert_th(self.lich_laplich_30)
        else:
            self.thang_30 = ''
            self.ngay_30 = ''
            self.thu_30 = ''

    @api.onchange('cangay_30')
    def onchange_method_cangay_30(self):
        if self.cangay_30 == True:
            self.noidung_chieu_30 = None
            self.coso_chieu_30 = False
        else:
            self.coso_chieu_chieu_a_30 = False
            self.coso_chieu_chieu_b_30 = False
            self.coso_chieu_chieu_c_30 = False
            self.noidung_chieu_chieu_30 = None
            self.coso_chieu_chieu_30 = False
            self.thoigian_chieu_30 = 0
            self.giaovien_chieu_30 = None

    @api.onchange('ngaynghi_30')
    def onchange_method_ngaynghi_30(self):
        if self.ngaynghi_30 == True:
            self.cangay_30 = False
            self.noidung_sang_30 = None
            self.noidung_chieu_30 = None
            self.noidung_sang_char_30 = '休日'
            self.noidung_chieu_char_30 = '休日'
            self.coso_sang_30 = False
            self.coso_chieu_30 = False
            self.cosao_daotao_a_30 = False
            self.cosao_daotao_b_30 = False
            self.cosao_daotao_c_30 = False
            self.thoigian_30 = 0
            self.giaovien_30 = None
            self.noidung_chieu_chieu_30 = None
            self.coso_chieu_chieu_30 = False
            self.coso_chieu_chieu_a_30 = False
            self.coso_chieu_chieu_b_30 = False
            self.coso_chieu_chieu_c_30 = False
            self.thoigian_chieu_30 = 0
            self.giaovien_chieu_30 = None
        else:
            self.cangay_30 = False
            self.noidung_sang_30 = None
            self.noidung_chieu_30 = None
            self.noidung_sang_char_30 = ''
            self.noidung_chieu_char_30 = ''
            self.coso_sang_30 = False
            self.coso_chieu_30 = False
            self.cosao_daotao_a_30 = False
            self.cosao_daotao_b_30 = False
            self.cosao_daotao_c_30 = False
            self.thoigian_30 = 0
            self.giaovien_30 = None
            self.noidung_chieu_chieu_30 = None
            self.coso_chieu_chieu_30 = False
            self.coso_chieu_chieu_a_30 = False
            self.coso_chieu_chieu_b_30 = False
            self.coso_chieu_chieu_c_30 = False
            self.thoigian_chieu_30 = 0
            self.giaovien_chieu_30 = None

    @api.onchange('noidung_sang_30', 'noidung_chieu_30')
    def onchange_method_noidung_sang_30(self):
        if self.noidung_sang_30 and self.noidung_chieu_30:
            if self.noidung_sang_30.noidungdaotao != '休日' and self.noidung_chieu_30.noidungdaotao != '休日':
                self.thoigian_30 = 8
            elif self.noidung_sang_30.noidungdaotao != '休日' or self.noidung_chieu_30.noidungdaotao != '休日':
                self.thoigian_30 = 4
            else:
                self.thoigian_30 = 0
        elif self.noidung_sang_30 or self.noidung_chieu_30:
            if self.noidung_sang_30.noidungdaotao != '休日' and self.noidung_sang_30.noidungdaotao:
                self.thoigian_30 = 4
            elif self.noidung_chieu_30.noidungdaotao != '休日' and self.noidung_chieu_30.noidungdaotao:
                self.thoigian_30 = 4
            else:
                self.thoigian_30 = 0
        else:
            self.thoigian_30 = 0

    @api.onchange('noidung_chieu_chieu_30')
    def onchange_method_noidung_chieu_chieu_30(self):
        if self.noidung_chieu_chieu_30:
            if self.noidung_chieu_chieu_30.noidungdaotao != '休日':
                self.thoigian_chieu_30 = 4
        else:
            self.thoigian_chieu_30 = 0

    # Ngày _31
    # Ca sáng
    lich_laplich_31 = fields.Date()
    thang_31 = fields.Char(compute='_thang_31')
    ngay_31 = fields.Char(compute='_thang_31')
    thu_31 = fields.Char(compute='_thang_31')
    ngaynghi_31 = fields.Boolean()
    cangay_31 = fields.Boolean()
    noidung_sang_31 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_31 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_31 = fields.Char()
    noidung_chieu_char_31 = fields.Char()
    coso_sang_31 = fields.Boolean()
    coso_chieu_31 = fields.Boolean()
    cosao_daotao_a_31 = fields.Boolean(string='①')
    cosao_daotao_b_31 = fields.Boolean(string='②')
    cosao_daotao_c_31 = fields.Boolean(string='③')
    thoigian_31 = fields.Integer()
    giaovien_31 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_31 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_31 = fields.Boolean()
    coso_chieu_chieu_a_31 = fields.Boolean(string='①')
    coso_chieu_chieu_b_31 = fields.Boolean(string='②')
    coso_chieu_chieu_c_31 = fields.Boolean(string='③')
    thoigian_chieu_31 = fields.Integer()
    giaovien_chieu_31 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_31')
    def onchange_method_coso_chieu_chieu_a_31(self):
        if self.coso_chieu_chieu_a_31 == True:
            self.coso_chieu_chieu_b_31 = False
            self.coso_chieu_chieu_c_31 = False

    @api.onchange('coso_chieu_chieu_b_31')
    def onchange_method_coso_chieu_chieu_b_31(self):
        if self.coso_chieu_chieu_b_31 == True:
            self.coso_chieu_chieu_a_31 = False
            self.coso_chieu_chieu_c_31 = False

    @api.onchange('coso_chieu_chieu_c_31')
    def onchange_method_coso_chieu_chieu_c_31(self):
        if self.coso_chieu_chieu_c_31 == True:
            self.coso_chieu_chieu_a_31 = False
            self.coso_chieu_chieu_b_31 = False

    @api.onchange('cosao_daotao_a_31')
    def onchange_method_cosao_daotao_a_31(self):
        if self.cosao_daotao_a_31 == True:
            self.cosao_daotao_b_31 = False
            self.cosao_daotao_c_31 = False

    @api.onchange('cosao_daotao_b_31')
    def onchange_method_cosao_daotao_b_31(self):
        if self.cosao_daotao_b_31 == True:
            self.cosao_daotao_a_31 = False
            self.cosao_daotao_c_31 = False

    @api.onchange('cosao_daotao_c_31')
    def onchange_method_cosao_daotao_c_31(self):
        if self.cosao_daotao_c_31 == True:
            self.cosao_daotao_a_31 = False
            self.cosao_daotao_b_31 = False

    @api.one
    @api.depends('lich_laplich_31')
    def _thang_31(self):
        if self.lich_laplich_31:
            self.thang_31 = self.lich_laplich_31.month
            self.ngay_31 = self.lich_laplich_31.day
            self.thu_31 = convert_th(self.lich_laplich_31)
        else:
            self.thang_31 = ''
            self.ngay_31 = ''
            self.thu_31 = ''

    @api.onchange('cangay_31')
    def onchange_method_cangay_31(self):
        if self.cangay_31 == True:
            self.noidung_chieu_31 = None
            self.coso_chieu_31 = False
        else:
            self.coso_chieu_chieu_a_31 = False
            self.coso_chieu_chieu_b_31 = False
            self.coso_chieu_chieu_c_31 = False
            self.noidung_chieu_chieu_31 = None
            self.coso_chieu_chieu_31 = False
            self.thoigian_chieu_31 = 0
            self.giaovien_chieu_31 = None

    @api.onchange('ngaynghi_31')
    def onchange_method_ngaynghi_31(self):
        if self.ngaynghi_31 == True:
            self.cangay_31 = False
            self.noidung_sang_31 = None
            self.noidung_chieu_31 = None
            self.noidung_sang_char_31 = '休日'
            self.noidung_chieu_char_31 = '休日'
            self.coso_sang_31 = False
            self.coso_chieu_31 = False
            self.cosao_daotao_a_31 = False
            self.cosao_daotao_b_31 = False
            self.cosao_daotao_c_31 = False
            self.thoigian_31 = 0
            self.giaovien_31 = None
            self.noidung_chieu_chieu_31 = None
            self.coso_chieu_chieu_31 = False
            self.coso_chieu_chieu_a_31 = False
            self.coso_chieu_chieu_b_31 = False
            self.coso_chieu_chieu_c_31 = False
            self.thoigian_chieu_31 = 0
            self.giaovien_chieu_31 = None
        else:
            self.cangay_31 = False
            self.noidung_sang_31 = None
            self.noidung_chieu_31 = None
            self.noidung_sang_char_31 = ''
            self.noidung_chieu_char_31 = ''
            self.coso_sang_31 = False
            self.coso_chieu_31 = False
            self.cosao_daotao_a_31 = False
            self.cosao_daotao_b_31 = False
            self.cosao_daotao_c_31 = False
            self.thoigian_31 = 0
            self.giaovien_31 = None
            self.noidung_chieu_chieu_31 = None
            self.coso_chieu_chieu_31 = False
            self.coso_chieu_chieu_a_31 = False
            self.coso_chieu_chieu_b_31 = False
            self.coso_chieu_chieu_c_31 = False
            self.thoigian_chieu_31 = 0
            self.giaovien_chieu_31 = None

    @api.onchange('noidung_sang_31', 'noidung_chieu_31')
    def onchange_method_noidung_sang_31(self):
        if self.noidung_sang_31 and self.noidung_chieu_31:
            if self.noidung_sang_31.noidungdaotao != '休日' and self.noidung_chieu_31.noidungdaotao != '休日':
                self.thoigian_31 = 8
            elif self.noidung_sang_31.noidungdaotao != '休日' or self.noidung_chieu_31.noidungdaotao != '休日':
                self.thoigian_31 = 4
            else:
                self.thoigian_31 = 0
        elif self.noidung_sang_31 or self.noidung_chieu_31:
            if self.noidung_sang_31.noidungdaotao != '休日' and self.noidung_sang_31.noidungdaotao:
                self.thoigian_31 = 4
            elif self.noidung_chieu_31.noidungdaotao != '休日' and self.noidung_chieu_31.noidungdaotao:
                self.thoigian_31 = 4
            else:
                self.thoigian_31 = 0
        else:
            self.thoigian_31 = 0

    @api.onchange('noidung_chieu_chieu_31')
    def onchange_method_noidung_chieu_chieu_31(self):
        if self.noidung_chieu_chieu_31:
            if self.noidung_chieu_chieu_31.noidungdaotao != '休日':
                self.thoigian_chieu_31 = 4
        else:
            self.thoigian_chieu_31 = 0

    # Ngày _32
    # Ca sáng
    lich_laplich_32 = fields.Date()
    thang_32 = fields.Char(compute='_thang_32')
    ngay_32 = fields.Char(compute='_thang_32')
    thu_32 = fields.Char(compute='_thang_32')
    ngaynghi_32 = fields.Boolean()
    cangay_32 = fields.Boolean()
    noidung_sang_32 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_32 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_32 = fields.Char()
    noidung_chieu_char_32 = fields.Char()
    coso_sang_32 = fields.Boolean()
    coso_chieu_32 = fields.Boolean()
    cosao_daotao_a_32 = fields.Boolean(string='①')
    cosao_daotao_b_32 = fields.Boolean(string='②')
    cosao_daotao_c_32 = fields.Boolean(string='③')
    thoigian_32 = fields.Integer()
    giaovien_32 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_32 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_32 = fields.Boolean()
    coso_chieu_chieu_a_32 = fields.Boolean(string='①')
    coso_chieu_chieu_b_32 = fields.Boolean(string='②')
    coso_chieu_chieu_c_32 = fields.Boolean(string='③')
    thoigian_chieu_32 = fields.Integer()
    giaovien_chieu_32 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_32')
    def onchange_method_coso_chieu_chieu_a_32(self):
        if self.coso_chieu_chieu_a_32 == True:
            self.coso_chieu_chieu_b_32 = False
            self.coso_chieu_chieu_c_32 = False

    @api.onchange('coso_chieu_chieu_b_32')
    def onchange_method_coso_chieu_chieu_b_32(self):
        if self.coso_chieu_chieu_b_32 == True:
            self.coso_chieu_chieu_a_32 = False
            self.coso_chieu_chieu_c_32 = False

    @api.onchange('coso_chieu_chieu_c_32')
    def onchange_method_coso_chieu_chieu_c_32(self):
        if self.coso_chieu_chieu_c_32 == True:
            self.coso_chieu_chieu_a_32 = False
            self.coso_chieu_chieu_b_32 = False

    @api.onchange('cosao_daotao_a_32')
    def onchange_method_cosao_daotao_a_32(self):
        if self.cosao_daotao_a_32 == True:
            self.cosao_daotao_b_32 = False
            self.cosao_daotao_c_32 = False

    @api.onchange('cosao_daotao_b_32')
    def onchange_method_cosao_daotao_b_32(self):
        if self.cosao_daotao_b_32 == True:
            self.cosao_daotao_a_32 = False
            self.cosao_daotao_c_32 = False

    @api.onchange('cosao_daotao_c_32')
    def onchange_method_cosao_daotao_c_32(self):
        if self.cosao_daotao_c_32 == True:
            self.cosao_daotao_a_32 = False
            self.cosao_daotao_b_32 = False

    @api.one
    @api.depends('lich_laplich_32')
    def _thang_32(self):
        if self.lich_laplich_32:
            self.thang_32 = self.lich_laplich_32.month
            self.ngay_32 = self.lich_laplich_32.day
            self.thu_32 = convert_th(self.lich_laplich_32)
        else:
            self.thang_32 = ''
            self.ngay_32 = ''
            self.thu_32 = ''

    @api.onchange('cangay_32')
    def onchange_method_cangay_32(self):
        if self.cangay_32 == True:
            self.noidung_chieu_32 = None
            self.coso_chieu_32 = False
        else:
            self.coso_chieu_chieu_a_32 = False
            self.coso_chieu_chieu_b_32 = False
            self.coso_chieu_chieu_c_32 = False
            self.noidung_chieu_chieu_32 = None
            self.coso_chieu_chieu_32 = False
            self.thoigian_chieu_32 = 0
            self.giaovien_chieu_32 = None

    @api.onchange('ngaynghi_32')
    def onchange_method_ngaynghi_32(self):
        if self.ngaynghi_32 == True:
            self.cangay_32 = False
            self.noidung_sang_32 = None
            self.noidung_chieu_32 = None
            self.noidung_sang_char_32 = '休日'
            self.noidung_chieu_char_32 = '休日'
            self.coso_sang_32 = False
            self.coso_chieu_32 = False
            self.cosao_daotao_a_32 = False
            self.cosao_daotao_b_32 = False
            self.cosao_daotao_c_32 = False
            self.thoigian_32 = 0
            self.giaovien_32 = None
            self.noidung_chieu_chieu_32 = None
            self.coso_chieu_chieu_32 = False
            self.coso_chieu_chieu_a_32 = False
            self.coso_chieu_chieu_b_32 = False
            self.coso_chieu_chieu_c_32 = False
            self.thoigian_chieu_32 = 0
            self.giaovien_chieu_32 = None
        else:
            self.cangay_32 = False
            self.noidung_sang_32 = None
            self.noidung_chieu_32 = None
            self.noidung_sang_char_32 = ''
            self.noidung_chieu_char_32 = ''
            self.coso_sang_32 = False
            self.coso_chieu_32 = False
            self.cosao_daotao_a_32 = False
            self.cosao_daotao_b_32 = False
            self.cosao_daotao_c_32 = False
            self.thoigian_32 = 0
            self.giaovien_32 = None
            self.noidung_chieu_chieu_32 = None
            self.coso_chieu_chieu_32 = False
            self.coso_chieu_chieu_a_32 = False
            self.coso_chieu_chieu_b_32 = False
            self.coso_chieu_chieu_c_32 = False
            self.thoigian_chieu_32 = 0
            self.giaovien_chieu_32 = None

    @api.onchange('noidung_sang_32', 'noidung_chieu_32')
    def onchange_method_noidung_sang_32(self):
        if self.noidung_sang_32 and self.noidung_chieu_32:
            if self.noidung_sang_32.noidungdaotao != '休日' and self.noidung_chieu_32.noidungdaotao != '休日':
                self.thoigian_32 = 8
            elif self.noidung_sang_32.noidungdaotao != '休日' or self.noidung_chieu_32.noidungdaotao != '休日':
                self.thoigian_32 = 4
            else:
                self.thoigian_32 = 0
        elif self.noidung_sang_32 or self.noidung_chieu_32:
            if self.noidung_sang_32.noidungdaotao != '休日' and self.noidung_sang_32.noidungdaotao:
                self.thoigian_32 = 4
            elif self.noidung_chieu_32.noidungdaotao != '休日' and self.noidung_chieu_32.noidungdaotao:
                self.thoigian_32 = 4
            else:
                self.thoigian_32 = 0
        else:
            self.thoigian_32 = 0

    @api.onchange('noidung_chieu_chieu_32')
    def onchange_method_noidung_chieu_chieu_32(self):
        if self.noidung_chieu_chieu_32:
            if self.noidung_chieu_chieu_32.noidungdaotao != '休日':
                self.thoigian_chieu_32 = 4
        else:
            self.thoigian_chieu_32 = 0

    # Ngày _33
    # Ca sáng
    lich_laplich_33 = fields.Date()
    thang_33 = fields.Char(compute='_thang_33')
    ngay_33 = fields.Char(compute='_thang_33')
    thu_33 = fields.Char(compute='_thang_33')
    ngaynghi_33 = fields.Boolean()
    cangay_33 = fields.Boolean()
    noidung_sang_33 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_33 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_33 = fields.Char()
    noidung_chieu_char_33 = fields.Char()
    coso_sang_33 = fields.Boolean()
    coso_chieu_33 = fields.Boolean()
    cosao_daotao_a_33 = fields.Boolean(string='①')
    cosao_daotao_b_33 = fields.Boolean(string='②')
    cosao_daotao_c_33 = fields.Boolean(string='③')
    thoigian_33 = fields.Integer()
    giaovien_33 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_33 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_33 = fields.Boolean()
    coso_chieu_chieu_a_33 = fields.Boolean(string='①')
    coso_chieu_chieu_b_33 = fields.Boolean(string='②')
    coso_chieu_chieu_c_33 = fields.Boolean(string='③')
    thoigian_chieu_33 = fields.Integer()
    giaovien_chieu_33 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_33')
    def onchange_method_coso_chieu_chieu_a_33(self):
        if self.coso_chieu_chieu_a_33 == True:
            self.coso_chieu_chieu_b_33 = False
            self.coso_chieu_chieu_c_33 = False

    @api.onchange('coso_chieu_chieu_b_33')
    def onchange_method_coso_chieu_chieu_b_33(self):
        if self.coso_chieu_chieu_b_33 == True:
            self.coso_chieu_chieu_a_33 = False
            self.coso_chieu_chieu_c_33 = False

    @api.onchange('coso_chieu_chieu_c_33')
    def onchange_method_coso_chieu_chieu_c_33(self):
        if self.coso_chieu_chieu_c_33 == True:
            self.coso_chieu_chieu_a_33 = False
            self.coso_chieu_chieu_b_33 = False

    @api.onchange('cosao_daotao_a_33')
    def onchange_method_cosao_daotao_a_33(self):
        if self.cosao_daotao_a_33 == True:
            self.cosao_daotao_b_33 = False
            self.cosao_daotao_c_33 = False

    @api.onchange('cosao_daotao_b_33')
    def onchange_method_cosao_daotao_b_33(self):
        if self.cosao_daotao_b_33 == True:
            self.cosao_daotao_a_33 = False
            self.cosao_daotao_c_33 = False

    @api.onchange('cosao_daotao_c_33')
    def onchange_method_cosao_daotao_c_33(self):
        if self.cosao_daotao_c_33 == True:
            self.cosao_daotao_a_33 = False
            self.cosao_daotao_b_33 = False

    @api.one
    @api.depends('lich_laplich_33')
    def _thang_33(self):
        if self.lich_laplich_33:
            self.thang_33 = self.lich_laplich_33.month
            self.ngay_33 = self.lich_laplich_33.day
            self.thu_33 = convert_th(self.lich_laplich_33)
        else:
            self.thang_33 = ''
            self.ngay_33 = ''
            self.thu_33 = ''

    @api.onchange('cangay_33')
    def onchange_method_cangay_33(self):
        if self.cangay_33 == True:
            self.noidung_chieu_33 = None
            self.coso_chieu_33 = False
        else:
            self.coso_chieu_chieu_a_33 = False
            self.coso_chieu_chieu_b_33 = False
            self.coso_chieu_chieu_c_33 = False
            self.noidung_chieu_chieu_33 = None
            self.coso_chieu_chieu_33 = False
            self.thoigian_chieu_33 = 0
            self.giaovien_chieu_33 = None

    @api.onchange('ngaynghi_33')
    def onchange_method_ngaynghi_33(self):
        if self.ngaynghi_33 == True:
            self.cangay_33 = False
            self.noidung_sang_33 = None
            self.noidung_chieu_33 = None
            self.noidung_sang_char_33 = '休日'
            self.noidung_chieu_char_33 = '休日'
            self.coso_sang_33 = False
            self.coso_chieu_33 = False
            self.cosao_daotao_a_33 = False
            self.cosao_daotao_b_33 = False
            self.cosao_daotao_c_33 = False
            self.thoigian_33 = 0
            self.giaovien_33 = None
            self.noidung_chieu_chieu_33 = None
            self.coso_chieu_chieu_33 = False
            self.coso_chieu_chieu_a_33 = False
            self.coso_chieu_chieu_b_33 = False
            self.coso_chieu_chieu_c_33 = False
            self.thoigian_chieu_33 = 0
            self.giaovien_chieu_33 = None
        else:
            self.cangay_33 = False
            self.noidung_sang_33 = None
            self.noidung_chieu_33 = None
            self.noidung_sang_char_33 = ''
            self.noidung_chieu_char_33 = ''
            self.coso_sang_33 = False
            self.coso_chieu_33 = False
            self.cosao_daotao_a_33 = False
            self.cosao_daotao_b_33 = False
            self.cosao_daotao_c_33 = False
            self.thoigian_33 = 0
            self.giaovien_33 = None
            self.noidung_chieu_chieu_33 = None
            self.coso_chieu_chieu_33 = False
            self.coso_chieu_chieu_a_33 = False
            self.coso_chieu_chieu_b_33 = False
            self.coso_chieu_chieu_c_33 = False
            self.thoigian_chieu_33 = 0
            self.giaovien_chieu_33 = None

    @api.onchange('noidung_sang_33', 'noidung_chieu_33')
    def onchange_method_noidung_sang_33(self):
        if self.noidung_sang_33 and self.noidung_chieu_33:
            if self.noidung_sang_33.noidungdaotao != '休日' and self.noidung_chieu_33.noidungdaotao != '休日':
                self.thoigian_33 = 8
            elif self.noidung_sang_33.noidungdaotao != '休日' or self.noidung_chieu_33.noidungdaotao != '休日':
                self.thoigian_33 = 4
            else:
                self.thoigian_33 = 0
        elif self.noidung_sang_33 or self.noidung_chieu_33:
            if self.noidung_sang_33.noidungdaotao != '休日' and self.noidung_sang_33.noidungdaotao:
                self.thoigian_33 = 4
            elif self.noidung_chieu_33.noidungdaotao != '休日' and self.noidung_chieu_33.noidungdaotao:
                self.thoigian_33 = 4
            else:
                self.thoigian_33 = 0
        else:
            self.thoigian_33 = 0

    @api.onchange('noidung_chieu_chieu_33')
    def onchange_method_noidung_chieu_chieu_33(self):
        if self.noidung_chieu_chieu_33:
            if self.noidung_chieu_chieu_33.noidungdaotao != '休日':
                self.thoigian_chieu_33 = 4
        else:
            self.thoigian_chieu_33 = 0

    # Ngày _34
    # Ca sáng
    lich_laplich_34 = fields.Date()
    thang_34 = fields.Char(compute='_thang_34')
    ngay_34 = fields.Char(compute='_thang_34')
    thu_34 = fields.Char(compute='_thang_34')
    ngaynghi_34 = fields.Boolean()
    cangay_34 = fields.Boolean()
    noidung_sang_34 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_34 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_34 = fields.Char()
    noidung_chieu_char_34 = fields.Char()
    coso_sang_34 = fields.Boolean()
    coso_chieu_34 = fields.Boolean()
    cosao_daotao_a_34 = fields.Boolean(string='①')
    cosao_daotao_b_34 = fields.Boolean(string='②')
    cosao_daotao_c_34 = fields.Boolean(string='③')
    thoigian_34 = fields.Integer()
    giaovien_34 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_34 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_34 = fields.Boolean()
    coso_chieu_chieu_a_34 = fields.Boolean(string='①')
    coso_chieu_chieu_b_34 = fields.Boolean(string='②')
    coso_chieu_chieu_c_34 = fields.Boolean(string='③')
    thoigian_chieu_34 = fields.Integer()
    giaovien_chieu_34 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_34')
    def onchange_method_coso_chieu_chieu_a_34(self):
        if self.coso_chieu_chieu_a_34 == True:
            self.coso_chieu_chieu_b_34 = False
            self.coso_chieu_chieu_c_34 = False

    @api.onchange('coso_chieu_chieu_b_34')
    def onchange_method_coso_chieu_chieu_b_34(self):
        if self.coso_chieu_chieu_b_34 == True:
            self.coso_chieu_chieu_a_34 = False
            self.coso_chieu_chieu_c_34 = False

    @api.onchange('coso_chieu_chieu_c_34')
    def onchange_method_coso_chieu_chieu_c_34(self):
        if self.coso_chieu_chieu_c_34 == True:
            self.coso_chieu_chieu_a_34 = False
            self.coso_chieu_chieu_b_34 = False

    @api.onchange('cosao_daotao_a_34')
    def onchange_method_cosao_daotao_a_34(self):
        if self.cosao_daotao_a_34 == True:
            self.cosao_daotao_b_34 = False
            self.cosao_daotao_c_34 = False

    @api.onchange('cosao_daotao_b_34')
    def onchange_method_cosao_daotao_b_34(self):
        if self.cosao_daotao_b_34 == True:
            self.cosao_daotao_a_34 = False
            self.cosao_daotao_c_34 = False

    @api.onchange('cosao_daotao_c_34')
    def onchange_method_cosao_daotao_c_34(self):
        if self.cosao_daotao_c_34 == True:
            self.cosao_daotao_a_34 = False
            self.cosao_daotao_b_34 = False

    @api.one
    @api.depends('lich_laplich_34')
    def _thang_34(self):
        if self.lich_laplich_34:
            self.thang_34 = self.lich_laplich_34.month
            self.ngay_34 = self.lich_laplich_34.day
            self.thu_34 = convert_th(self.lich_laplich_34)
        else:
            self.thang_34 = ''
            self.ngay_34 = ''
            self.thu_34 = ''

    @api.onchange('cangay_34')
    def onchange_method_cangay_34(self):
        if self.cangay_34 == True:
            self.noidung_chieu_34 = None
            self.coso_chieu_34 = False
        else:
            self.coso_chieu_chieu_a_34 = False
            self.coso_chieu_chieu_b_34 = False
            self.coso_chieu_chieu_c_34 = False
            self.noidung_chieu_chieu_34 = None
            self.coso_chieu_chieu_34 = False
            self.thoigian_chieu_34 = 0
            self.giaovien_chieu_34 = None

    @api.onchange('ngaynghi_34')
    def onchange_method_ngaynghi_34(self):
        if self.ngaynghi_34 == True:
            self.cangay_34 = False
            self.noidung_sang_34 = None
            self.noidung_chieu_34 = None
            self.noidung_sang_char_34 = '休日'
            self.noidung_chieu_char_34 = '休日'
            self.coso_sang_34 = False
            self.coso_chieu_34 = False
            self.cosao_daotao_a_34 = False
            self.cosao_daotao_b_34 = False
            self.cosao_daotao_c_34 = False
            self.thoigian_34 = 0
            self.giaovien_34 = None
            self.noidung_chieu_chieu_34 = None
            self.coso_chieu_chieu_34 = False
            self.coso_chieu_chieu_a_34 = False
            self.coso_chieu_chieu_b_34 = False
            self.coso_chieu_chieu_c_34 = False
            self.thoigian_chieu_34 = 0
            self.giaovien_chieu_34 = None
        else:
            self.cangay_34 = False
            self.noidung_sang_34 = None
            self.noidung_chieu_34 = None
            self.noidung_sang_char_34 = ''
            self.noidung_chieu_char_34 = ''
            self.coso_sang_34 = False
            self.coso_chieu_34 = False
            self.cosao_daotao_a_34 = False
            self.cosao_daotao_b_34 = False
            self.cosao_daotao_c_34 = False
            self.thoigian_34 = 0
            self.giaovien_34 = None
            self.noidung_chieu_chieu_34 = None
            self.coso_chieu_chieu_34 = False
            self.coso_chieu_chieu_a_34 = False
            self.coso_chieu_chieu_b_34 = False
            self.coso_chieu_chieu_c_34 = False
            self.thoigian_chieu_34 = 0
            self.giaovien_chieu_34 = None

    @api.onchange('noidung_sang_34', 'noidung_chieu_34')
    def onchange_method_noidung_sang_34(self):
        if self.noidung_sang_34 and self.noidung_chieu_34:
            if self.noidung_sang_34.noidungdaotao != '休日' and self.noidung_chieu_34.noidungdaotao != '休日':
                self.thoigian_34 = 8
            elif self.noidung_sang_34.noidungdaotao != '休日' or self.noidung_chieu_34.noidungdaotao != '休日':
                self.thoigian_34 = 4
            else:
                self.thoigian_34 = 0
        elif self.noidung_sang_34 or self.noidung_chieu_34:
            if self.noidung_sang_34.noidungdaotao != '休日' and self.noidung_sang_34.noidungdaotao:
                self.thoigian_34 = 4
            elif self.noidung_chieu_34.noidungdaotao != '休日' and self.noidung_chieu_34.noidungdaotao:
                self.thoigian_34 = 4
            else:
                self.thoigian_34 = 0
        else:
            self.thoigian_34 = 0

    @api.onchange('noidung_chieu_chieu_34')
    def onchange_method_noidung_chieu_chieu_34(self):
        if self.noidung_chieu_chieu_34:
            if self.noidung_chieu_chieu_34.noidungdaotao != '休日':
                self.thoigian_chieu_34 = 4
        else:
            self.thoigian_chieu_34 = 0

    # Ngày _35
    # Ca sáng
    lich_laplich_35 = fields.Date()
    thang_35 = fields.Char(compute='_thang_35')
    ngay_35 = fields.Char(compute='_thang_35')
    thu_35 = fields.Char(compute='_thang_35')
    ngaynghi_35 = fields.Boolean()
    cangay_35 = fields.Boolean()
    noidung_sang_35 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_chieu_35 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    noidung_sang_char_35 = fields.Char()
    noidung_chieu_char_35 = fields.Char()
    coso_sang_35 = fields.Boolean()
    coso_chieu_35 = fields.Boolean()
    cosao_daotao_a_35 = fields.Boolean(string='①')
    cosao_daotao_b_35 = fields.Boolean(string='②')
    cosao_daotao_c_35 = fields.Boolean(string='③')
    thoigian_35 = fields.Integer()
    giaovien_35 = fields.Many2one(comodel_name='giangvien.giangvien')
    # Ca chiều
    noidung_chieu_chieu_35 = fields.Many2one(comodel_name='daotaosau.noidungdaotao')
    coso_chieu_chieu_35 = fields.Boolean()
    coso_chieu_chieu_a_35 = fields.Boolean(string='①')
    coso_chieu_chieu_b_35 = fields.Boolean(string='②')
    coso_chieu_chieu_c_35 = fields.Boolean(string='③')
    thoigian_chieu_35 = fields.Integer()
    giaovien_chieu_35 = fields.Many2one(comodel_name='giangvien.giangvien')

    @api.onchange('coso_chieu_chieu_a_35')
    def onchange_method_coso_chieu_chieu_a_35(self):
        if self.coso_chieu_chieu_a_35 == True:
            self.coso_chieu_chieu_b_35 = False
            self.coso_chieu_chieu_c_35 = False

    @api.onchange('coso_chieu_chieu_b_35')
    def onchange_method_coso_chieu_chieu_b_35(self):
        if self.coso_chieu_chieu_b_35 == True:
            self.coso_chieu_chieu_a_35 = False
            self.coso_chieu_chieu_c_35 = False

    @api.onchange('coso_chieu_chieu_c_35')
    def onchange_method_coso_chieu_chieu_c_35(self):
        if self.coso_chieu_chieu_c_35 == True:
            self.coso_chieu_chieu_a_35 = False
            self.coso_chieu_chieu_b_35 = False

    @api.onchange('cosao_daotao_a_35')
    def onchange_method_cosao_daotao_a_35(self):
        if self.cosao_daotao_a_35 == True:
            self.cosao_daotao_b_35 = False
            self.cosao_daotao_c_35 = False

    @api.onchange('cosao_daotao_b_35')
    def onchange_method_cosao_daotao_b_35(self):
        if self.cosao_daotao_b_35 == True:
            self.cosao_daotao_a_35 = False
            self.cosao_daotao_c_35 = False

    @api.onchange('cosao_daotao_c_35')
    def onchange_method_cosao_daotao_c_35(self):
        if self.cosao_daotao_c_35 == True:
            self.cosao_daotao_a_35 = False
            self.cosao_daotao_b_35 = False

    @api.one
    @api.depends('lich_laplich_35')
    def _thang_35(self):
        if self.lich_laplich_35:
            self.thang_35 = self.lich_laplich_35.month
            self.ngay_35 = self.lich_laplich_35.day
            self.thu_35 = convert_th(self.lich_laplich_35)
        else:
            self.thang_35 = ''
            self.ngay_35 = ''
            self.thu_35 = ''

    @api.onchange('cangay_35')
    def onchange_method_cangay_35(self):
        if self.cangay_35 == True:
            self.noidung_chieu_35 = None
            self.coso_chieu_35 = False
        else:
            self.coso_chieu_chieu_a_35 = False
            self.coso_chieu_chieu_b_35 = False
            self.coso_chieu_chieu_c_35 = False
            self.noidung_chieu_chieu_35 = None
            self.coso_chieu_chieu_35 = False
            self.thoigian_chieu_35 = 0
            self.giaovien_chieu_35 = None

    @api.onchange('ngaynghi_35')
    def onchange_method_ngaynghi_35(self):
        if self.ngaynghi_35 == True:
            self.cangay_35 = False
            self.noidung_sang_35 = None
            self.noidung_chieu_35 = None
            self.noidung_sang_char_35 = '休日'
            self.noidung_chieu_char_35 = '休日'
            self.coso_sang_35 = False
            self.coso_chieu_35 = False
            self.cosao_daotao_a_35 = False
            self.cosao_daotao_b_35 = False
            self.cosao_daotao_c_35 = False
            self.thoigian_35 = 0
            self.giaovien_35 = None
            self.noidung_chieu_chieu_35 = None
            self.coso_chieu_chieu_35 = False
            self.coso_chieu_chieu_a_35 = False
            self.coso_chieu_chieu_b_35 = False
            self.coso_chieu_chieu_c_35 = False
            self.thoigian_chieu_35 = 0
            self.giaovien_chieu_35 = None
        else:
            self.cangay_35 = False
            self.noidung_sang_35 = None
            self.noidung_chieu_35 = None
            self.noidung_sang_char_35 = ''
            self.noidung_chieu_char_35 = ''
            self.coso_sang_35 = False
            self.coso_chieu_35 = False
            self.cosao_daotao_a_35 = False
            self.cosao_daotao_b_35 = False
            self.cosao_daotao_c_35 = False
            self.thoigian_35 = 0
            self.giaovien_35 = None
            self.noidung_chieu_chieu_35 = None
            self.coso_chieu_chieu_35 = False
            self.coso_chieu_chieu_a_35 = False
            self.coso_chieu_chieu_b_35 = False
            self.coso_chieu_chieu_c_35 = False
            self.thoigian_chieu_35 = 0
            self.giaovien_chieu_35 = None

    @api.onchange('noidung_sang_35', 'noidung_chieu_35')
    def onchange_method_noidung_sang_35(self):
        if self.noidung_sang_35 and self.noidung_chieu_35:
            if self.noidung_sang_35.noidungdaotao != '休日' and self.noidung_chieu_35.noidungdaotao != '休日':
                self.thoigian_35 = 8
            elif self.noidung_sang_35.noidungdaotao != '休日' or self.noidung_chieu_35.noidungdaotao != '休日':
                self.thoigian_35 = 4
            else:
                self.thoigian_35 = 0
        elif self.noidung_sang_35 or self.noidung_chieu_35:
            if self.noidung_sang_35.noidungdaotao != '休日' and self.noidung_sang_35.noidungdaotao:
                self.thoigian_35 = 4
            elif self.noidung_chieu_35.noidungdaotao != '休日' and self.noidung_chieu_35.noidungdaotao:
                self.thoigian_35 = 4
            else:
                self.thoigian_35 = 0
        else:
            self.thoigian_35 = 0

    @api.onchange('noidung_chieu_chieu_35')
    def onchange_method_noidung_chieu_chieu_35(self):
        if self.noidung_chieu_chieu_35:
            if self.noidung_chieu_chieu_35.noidungdaotao != '休日':
                self.thoigian_chieu_35 = 4
        else:
            self.thoigian_chieu_35 = 0

    total_time = fields.Integer(string='Tổng thời gian', compute='_total_time')

    @api.one
    @api.depends('thoigian_1', 'thoigian_2', 'thoigian_3', 'thoigian_4', 'thoigian_5', 'thoigian_6', 'thoigian_7',
                 'thoigian_8', 'thoigian_9', 'thoigian_10', 'thoigian_11', 'thoigian_12', 'thoigian_13', 'thoigian_14',
                 'thoigian_15', 'thoigian_16', 'thoigian_17', 'thoigian_18', 'thoigian_19', 'thoigian_20',
                 'thoigian_21', 'thoigian_22', 'thoigian_23', 'thoigian_24', 'thoigian_25', 'thoigian_26',
                 'thoigian_27', 'thoigian_28', 'thoigian_29', 'thoigian_30', 'thoigian_31', 'thoigian_32',
                 'thoigian_33', 'thoigian_34', 'thoigian_35', 'thoigian_chieu_1', 'thoigian_chieu_2',
                 'thoigian_chieu_3', 'thoigian_chieu_4', 'thoigian_chieu_5', 'thoigian_chieu_6', 'thoigian_chieu_7',
                 'thoigian_chieu_8', 'thoigian_chieu_9', 'thoigian_chieu_10', 'thoigian_chieu_11', 'thoigian_chieu_12',
                 'thoigian_chieu_13', 'thoigian_chieu_14', 'thoigian_chieu_15', 'thoigian_chieu_16',
                 'thoigian_chieu_17', 'thoigian_chieu_18', 'thoigian_chieu_19', 'thoigian_chieu_20',
                 'thoigian_chieu_21', 'thoigian_chieu_22', 'thoigian_chieu_23', 'thoigian_chieu_24',
                 'thoigian_chieu_25', 'thoigian_chieu_26', 'thoigian_chieu_27', 'thoigian_chieu_28',
                 'thoigian_chieu_29', 'thoigian_chieu_30', 'thoigian_chieu_31', 'thoigian_chieu_32',
                 'thoigian_chieu_33', 'thoigian_chieu_34', 'thoigian_chieu_35')
    def _total_time(self):
        for item in self:
            item.total_time = item.thoigian_1 + item.thoigian_2 + item.thoigian_3 + item.thoigian_4 + item.thoigian_5 + item.thoigian_6 + item.thoigian_7 + item.thoigian_8 + item.thoigian_9 + item.thoigian_10 + item.thoigian_11 + item.thoigian_12 + item.thoigian_13 + item.thoigian_14 + item.thoigian_15 + item.thoigian_16 + item.thoigian_17 + item.thoigian_18 + item.thoigian_19 + item.thoigian_20 + item.thoigian_21 + item.thoigian_22 + item.thoigian_23 + item.thoigian_24 + item.thoigian_25 + item.thoigian_26 + item.thoigian_27 + item.thoigian_28 + item.thoigian_29 + item.thoigian_30 + item.thoigian_31 + item.thoigian_32 + item.thoigian_33 + item.thoigian_34 + item.thoigian_35 + item.thoigian_chieu_1 + item.thoigian_chieu_2 + item.thoigian_chieu_3 + item.thoigian_chieu_4 + item.thoigian_chieu_5 + item.thoigian_chieu_6 + item.thoigian_chieu_7 + item.thoigian_chieu_8 + item.thoigian_chieu_9 + item.thoigian_chieu_10 + item.thoigian_chieu_11 + item.thoigian_chieu_12 + item.thoigian_chieu_13 + item.thoigian_chieu_14 + item.thoigian_chieu_15 + item.thoigian_chieu_16 + item.thoigian_chieu_17 + item.thoigian_chieu_18 + item.thoigian_chieu_19 + item.thoigian_chieu_20 + item.thoigian_chieu_21 + item.thoigian_chieu_22 + item.thoigian_chieu_23 + item.thoigian_chieu_24 + item.thoigian_chieu_25 + item.thoigian_chieu_26 + item.thoigian_chieu_27 + item.thoigian_chieu_28 + item.thoigian_chieu_29 + item.thoigian_chieu_30 + item.thoigian_chieu_31 + item.thoigian_chieu_32 + item.thoigian_chieu_33 + item.thoigian_chieu_34 + item.thoigian_chieu_35

    @api.multi
    def laydulieu_button(self):
        # Clear data
        # Ngày _1
        self.lich_laplich_1 = None
        self.ngaynghi_1 = False
        self.cangay_1 = False
        self.noidung_sang_1 = None
        self.noidung_chieu_1 = None
        self.noidung_sang_char_1 = ''
        self.noidung_chieu_char_1 = ''
        self.coso_sang_1 = False
        self.coso_chieu_1 = False
        self.cosao_daotao_1_1 = False
        self.cosao_daotao_2_1 = False
        self.cosao_daotao_3_1 = False
        self.thoigian_1 = 0
        self.giaovien_1 = None
        self.noidung_chieu_chieu_1 = None
        self.coso_chieu_chieu_1 = False
        self.coso_chieu_chieu_1_1 = False
        self.coso_chieu_chieu_2_1 = False
        self.coso_chieu_chieu_3_1 = False
        self.thoigian_chieu_1 = 0
        self.giaovien_chieu_1 = None

        # Ngày _2
        self.lich_laplich_2 = None
        self.ngaynghi_2 = False
        self.cangay_2 = False
        self.noidung_sang_2 = None
        self.noidung_chieu_2 = None
        self.noidung_sang_char_2 = ''
        self.noidung_chieu_char_2 = ''
        self.coso_sang_2 = False
        self.coso_chieu_2 = False
        self.cosao_daotao_2_2 = False
        self.cosao_daotao_2_2 = False
        self.cosao_daotao_3_2 = False
        self.thoigian_2 = 0
        self.giaovien_2 = None
        self.noidung_chieu_chieu_2 = None
        self.coso_chieu_chieu_2 = False
        self.coso_chieu_chieu_2_2 = False
        self.coso_chieu_chieu_2_2 = False
        self.coso_chieu_chieu_3_2 = False
        self.thoigian_chieu_2 = 0
        self.giaovien_chieu_2 = None

        # Ngày _3
        self.lich_laplich_3 = None
        self.ngaynghi_3 = False
        self.cangay_3 = False
        self.noidung_sang_3 = None
        self.noidung_chieu_3 = None
        self.noidung_sang_char_3 = ''
        self.noidung_chieu_char_3 = ''
        self.coso_sang_3 = False
        self.coso_chieu_3 = False
        self.cosao_daotao_3_3 = False
        self.cosao_daotao_3_3 = False
        self.cosao_daotao_3_3 = False
        self.thoigian_3 = 0
        self.giaovien_3 = None
        self.noidung_chieu_chieu_3 = None
        self.coso_chieu_chieu_3 = False
        self.coso_chieu_chieu_3_3 = False
        self.coso_chieu_chieu_3_3 = False
        self.coso_chieu_chieu_3_3 = False
        self.thoigian_chieu_3 = 0
        self.giaovien_chieu_3 = None

        # Ngày _4
        self.lich_laplich_4 = None
        self.ngaynghi_4 = False
        self.cangay_4 = False
        self.noidung_sang_4 = None
        self.noidung_chieu_4 = None
        self.noidung_sang_char_4 = ''
        self.noidung_chieu_char_4 = ''
        self.coso_sang_4 = False
        self.coso_chieu_4 = False
        self.cosao_daotao_4_4 = False
        self.cosao_daotao_4_4 = False
        self.cosao_daotao_4_4 = False
        self.thoigian_4 = 0
        self.giaovien_4 = None
        self.noidung_chieu_chieu_4 = None
        self.coso_chieu_chieu_4 = False
        self.coso_chieu_chieu_4_4 = False
        self.coso_chieu_chieu_4_4 = False
        self.coso_chieu_chieu_4_4 = False
        self.thoigian_chieu_4 = 0
        self.giaovien_chieu_4 = None

        # Ngày _5
        self.lich_laplich_5 = None
        self.ngaynghi_5 = False
        self.cangay_5 = False
        self.noidung_sang_5 = None
        self.noidung_chieu_5 = None
        self.noidung_sang_char_5 = ''
        self.noidung_chieu_char_5 = ''
        self.coso_sang_5 = False
        self.coso_chieu_5 = False
        self.cosao_daotao_5_5 = False
        self.cosao_daotao_5_5 = False
        self.cosao_daotao_5_5 = False
        self.thoigian_5 = 0
        self.giaovien_5 = None
        self.noidung_chieu_chieu_5 = None
        self.coso_chieu_chieu_5 = False
        self.coso_chieu_chieu_5_5 = False
        self.coso_chieu_chieu_5_5 = False
        self.coso_chieu_chieu_5_5 = False
        self.thoigian_chieu_5 = 0
        self.giaovien_chieu_5 = None

        # Ngày _6
        self.lich_laplich_6 = None
        self.ngaynghi_6 = False
        self.cangay_6 = False
        self.noidung_sang_6 = None
        self.noidung_chieu_6 = None
        self.noidung_sang_char_6 = ''
        self.noidung_chieu_char_6 = ''
        self.coso_sang_6 = False
        self.coso_chieu_6 = False
        self.cosao_daotao_6_6 = False
        self.cosao_daotao_6_6 = False
        self.cosao_daotao_6_6 = False
        self.thoigian_6 = 0
        self.giaovien_6 = None
        self.noidung_chieu_chieu_6 = None
        self.coso_chieu_chieu_6 = False
        self.coso_chieu_chieu_6_6 = False
        self.coso_chieu_chieu_6_6 = False
        self.coso_chieu_chieu_6_6 = False
        self.thoigian_chieu_6 = 0
        self.giaovien_chieu_6 = None

        # Ngày _7
        self.lich_laplich_7 = None
        self.ngaynghi_7 = False
        self.cangay_7 = False
        self.noidung_sang_7 = None
        self.noidung_chieu_7 = None
        self.noidung_sang_char_7 = ''
        self.noidung_chieu_char_7 = ''
        self.coso_sang_7 = False
        self.coso_chieu_7 = False
        self.cosao_daotao_7_7 = False
        self.cosao_daotao_7_7 = False
        self.cosao_daotao_7_7 = False
        self.thoigian_7 = 0
        self.giaovien_7 = None
        self.noidung_chieu_chieu_7 = None
        self.coso_chieu_chieu_7 = False
        self.coso_chieu_chieu_7_7 = False
        self.coso_chieu_chieu_7_7 = False
        self.coso_chieu_chieu_7_7 = False
        self.thoigian_chieu_7 = 0
        self.giaovien_chieu_7 = None

        # Ngày _8
        self.lich_laplich_8 = None
        self.ngaynghi_8 = False
        self.cangay_8 = False
        self.noidung_sang_8 = None
        self.noidung_chieu_8 = None
        self.noidung_sang_char_8 = ''
        self.noidung_chieu_char_8 = ''
        self.coso_sang_8 = False
        self.coso_chieu_8 = False
        self.cosao_daotao_8_8 = False
        self.cosao_daotao_8_8 = False
        self.cosao_daotao_8_8 = False
        self.thoigian_8 = 0
        self.giaovien_8 = None
        self.noidung_chieu_chieu_8 = None
        self.coso_chieu_chieu_8 = False
        self.coso_chieu_chieu_8_8 = False
        self.coso_chieu_chieu_8_8 = False
        self.coso_chieu_chieu_8_8 = False
        self.thoigian_chieu_8 = 0
        self.giaovien_chieu_8 = None

        # Ngày _9
        self.lich_laplich_9 = None
        self.ngaynghi_9 = False
        self.cangay_9 = False
        self.noidung_sang_9 = None
        self.noidung_chieu_9 = None
        self.noidung_sang_char_9 = ''
        self.noidung_chieu_char_9 = ''
        self.coso_sang_9 = False
        self.coso_chieu_9 = False
        self.cosao_daotao_9_9 = False
        self.cosao_daotao_9_9 = False
        self.cosao_daotao_9_9 = False
        self.thoigian_9 = 0
        self.giaovien_9 = None
        self.noidung_chieu_chieu_9 = None
        self.coso_chieu_chieu_9 = False
        self.coso_chieu_chieu_9_9 = False
        self.coso_chieu_chieu_9_9 = False
        self.coso_chieu_chieu_9_9 = False
        self.thoigian_chieu_9 = 0
        self.giaovien_chieu_9 = None

        # Ngày _10
        self.lich_laplich_10 = None
        self.ngaynghi_10 = False
        self.cangay_10 = False
        self.noidung_sang_10 = None
        self.noidung_chieu_10 = None
        self.noidung_sang_char_10 = ''
        self.noidung_chieu_char_10 = ''
        self.coso_sang_10 = False
        self.coso_chieu_10 = False
        self.cosao_daotao_10_10 = False
        self.cosao_daotao_10_10 = False
        self.cosao_daotao_10_10 = False
        self.thoigian_10 = 0
        self.giaovien_10 = None
        self.noidung_chieu_chieu_10 = None
        self.coso_chieu_chieu_10 = False
        self.coso_chieu_chieu_10_10 = False
        self.coso_chieu_chieu_10_10 = False
        self.coso_chieu_chieu_10_10 = False
        self.thoigian_chieu_10 = 0
        self.giaovien_chieu_10 = None

        # Ngày _11
        self.lich_laplich_11 = None
        self.ngaynghi_11 = False
        self.cangay_11 = False
        self.noidung_sang_11 = None
        self.noidung_chieu_11 = None
        self.noidung_sang_char_11 = ''
        self.noidung_chieu_char_11 = ''
        self.coso_sang_11 = False
        self.coso_chieu_11 = False
        self.cosao_daotao_11_11 = False
        self.cosao_daotao_11_11 = False
        self.cosao_daotao_11_11 = False
        self.thoigian_11 = 0
        self.giaovien_11 = None
        self.noidung_chieu_chieu_11 = None
        self.coso_chieu_chieu_11 = False
        self.coso_chieu_chieu_11_11 = False
        self.coso_chieu_chieu_11_11 = False
        self.coso_chieu_chieu_11_11 = False
        self.thoigian_chieu_11 = 0
        self.giaovien_chieu_11 = None

        # Ngày _12
        self.lich_laplich_12 = None
        self.ngaynghi_12 = False
        self.cangay_12 = False
        self.noidung_sang_12 = None
        self.noidung_chieu_12 = None
        self.noidung_sang_char_12 = ''
        self.noidung_chieu_char_12 = ''
        self.coso_sang_12 = False
        self.coso_chieu_12 = False
        self.cosao_daotao_12_12 = False
        self.cosao_daotao_12_12 = False
        self.cosao_daotao_12_12 = False
        self.thoigian_12 = 0
        self.giaovien_12 = None
        self.noidung_chieu_chieu_12 = None
        self.coso_chieu_chieu_12 = False
        self.coso_chieu_chieu_12_12 = False
        self.coso_chieu_chieu_12_12 = False
        self.coso_chieu_chieu_12_12 = False
        self.thoigian_chieu_12 = 0
        self.giaovien_chieu_12 = None

        # Ngày _13
        self.lich_laplich_13 = None
        self.ngaynghi_13 = False
        self.cangay_13 = False
        self.noidung_sang_13 = None
        self.noidung_chieu_13 = None
        self.noidung_sang_char_13 = ''
        self.noidung_chieu_char_13 = ''
        self.coso_sang_13 = False
        self.coso_chieu_13 = False
        self.cosao_daotao_13_13 = False
        self.cosao_daotao_13_13 = False
        self.cosao_daotao_13_13 = False
        self.thoigian_13 = 0
        self.giaovien_13 = None
        self.noidung_chieu_chieu_13 = None
        self.coso_chieu_chieu_13 = False
        self.coso_chieu_chieu_13_13 = False
        self.coso_chieu_chieu_13_13 = False
        self.coso_chieu_chieu_13_13 = False
        self.thoigian_chieu_13 = 0
        self.giaovien_chieu_13 = None

        # Ngày _14
        self.lich_laplich_14 = None
        self.ngaynghi_14 = False
        self.cangay_14 = False
        self.noidung_sang_14 = None
        self.noidung_chieu_14 = None
        self.noidung_sang_char_14 = ''
        self.noidung_chieu_char_14 = ''
        self.coso_sang_14 = False
        self.coso_chieu_14 = False
        self.cosao_daotao_14_14 = False
        self.cosao_daotao_14_14 = False
        self.cosao_daotao_14_14 = False
        self.thoigian_14 = 0
        self.giaovien_14 = None
        self.noidung_chieu_chieu_14 = None
        self.coso_chieu_chieu_14 = False
        self.coso_chieu_chieu_14_14 = False
        self.coso_chieu_chieu_14_14 = False
        self.coso_chieu_chieu_14_14 = False
        self.thoigian_chieu_14 = 0
        self.giaovien_chieu_14 = None

        # Ngày _15
        self.lich_laplich_15 = None
        self.ngaynghi_15 = False
        self.cangay_15 = False
        self.noidung_sang_15 = None
        self.noidung_chieu_15 = None
        self.noidung_sang_char_15 = ''
        self.noidung_chieu_char_15 = ''
        self.coso_sang_15 = False
        self.coso_chieu_15 = False
        self.cosao_daotao_15_15 = False
        self.cosao_daotao_15_15 = False
        self.cosao_daotao_15_15 = False
        self.thoigian_15 = 0
        self.giaovien_15 = None
        self.noidung_chieu_chieu_15 = None
        self.coso_chieu_chieu_15 = False
        self.coso_chieu_chieu_15_15 = False
        self.coso_chieu_chieu_15_15 = False
        self.coso_chieu_chieu_15_15 = False
        self.thoigian_chieu_15 = 0
        self.giaovien_chieu_15 = None

        # Ngày _16
        self.lich_laplich_16 = None
        self.ngaynghi_16 = False
        self.cangay_16 = False
        self.noidung_sang_16 = None
        self.noidung_chieu_16 = None
        self.noidung_sang_char_16 = ''
        self.noidung_chieu_char_16 = ''
        self.coso_sang_16 = False
        self.coso_chieu_16 = False
        self.cosao_daotao_16_16 = False
        self.cosao_daotao_16_16 = False
        self.cosao_daotao_16_16 = False
        self.thoigian_16 = 0
        self.giaovien_16 = None
        self.noidung_chieu_chieu_16 = None
        self.coso_chieu_chieu_16 = False
        self.coso_chieu_chieu_16_16 = False
        self.coso_chieu_chieu_16_16 = False
        self.coso_chieu_chieu_16_16 = False
        self.thoigian_chieu_16 = 0
        self.giaovien_chieu_16 = None

        # Ngày _17
        self.lich_laplich_17 = None
        self.ngaynghi_17 = False
        self.cangay_17 = False
        self.noidung_sang_17 = None
        self.noidung_chieu_17 = None
        self.noidung_sang_char_17 = ''
        self.noidung_chieu_char_17 = ''
        self.coso_sang_17 = False
        self.coso_chieu_17 = False
        self.cosao_daotao_17_17 = False
        self.cosao_daotao_17_17 = False
        self.cosao_daotao_17_17 = False
        self.thoigian_17 = 0
        self.giaovien_17 = None
        self.noidung_chieu_chieu_17 = None
        self.coso_chieu_chieu_17 = False
        self.coso_chieu_chieu_17_17 = False
        self.coso_chieu_chieu_17_17 = False
        self.coso_chieu_chieu_17_17 = False
        self.thoigian_chieu_17 = 0
        self.giaovien_chieu_17 = None

        # Ngày _18
        self.lich_laplich_18 = None
        self.ngaynghi_18 = False
        self.cangay_18 = False
        self.noidung_sang_18 = None
        self.noidung_chieu_18 = None
        self.noidung_sang_char_18 = ''
        self.noidung_chieu_char_18 = ''
        self.coso_sang_18 = False
        self.coso_chieu_18 = False
        self.cosao_daotao_18_18 = False
        self.cosao_daotao_18_18 = False
        self.cosao_daotao_18_18 = False
        self.thoigian_18 = 0
        self.giaovien_18 = None
        self.noidung_chieu_chieu_18 = None
        self.coso_chieu_chieu_18 = False
        self.coso_chieu_chieu_18_18 = False
        self.coso_chieu_chieu_18_18 = False
        self.coso_chieu_chieu_18_18 = False
        self.thoigian_chieu_18 = 0
        self.giaovien_chieu_18 = None

        # Ngày _19
        self.lich_laplich_19 = None
        self.ngaynghi_19 = False
        self.cangay_19 = False
        self.noidung_sang_19 = None
        self.noidung_chieu_19 = None
        self.noidung_sang_char_19 = ''
        self.noidung_chieu_char_19 = ''
        self.coso_sang_19 = False
        self.coso_chieu_19 = False
        self.cosao_daotao_19_19 = False
        self.cosao_daotao_19_19 = False
        self.cosao_daotao_19_19 = False
        self.thoigian_19 = 0
        self.giaovien_19 = None
        self.noidung_chieu_chieu_19 = None
        self.coso_chieu_chieu_19 = False
        self.coso_chieu_chieu_19_19 = False
        self.coso_chieu_chieu_19_19 = False
        self.coso_chieu_chieu_19_19 = False
        self.thoigian_chieu_19 = 0
        self.giaovien_chieu_19 = None

        # Ngày _20
        self.lich_laplich_20 = None
        self.ngaynghi_20 = False
        self.cangay_20 = False
        self.noidung_sang_20 = None
        self.noidung_chieu_20 = None
        self.noidung_sang_char_20 = ''
        self.noidung_chieu_char_20 = ''
        self.coso_sang_20 = False
        self.coso_chieu_20 = False
        self.cosao_daotao_20_20 = False
        self.cosao_daotao_20_20 = False
        self.cosao_daotao_20_20 = False
        self.thoigian_20 = 0
        self.giaovien_20 = None
        self.noidung_chieu_chieu_20 = None
        self.coso_chieu_chieu_20 = False
        self.coso_chieu_chieu_20_20 = False
        self.coso_chieu_chieu_20_20 = False
        self.coso_chieu_chieu_20_20 = False
        self.thoigian_chieu_20 = 0
        self.giaovien_chieu_20 = None

        # Ngày _21
        self.lich_laplich_21 = None
        self.ngaynghi_21 = False
        self.cangay_21 = False
        self.noidung_sang_21 = None
        self.noidung_chieu_21 = None
        self.noidung_sang_char_21 = ''
        self.noidung_chieu_char_21 = ''
        self.coso_sang_21 = False
        self.coso_chieu_21 = False
        self.cosao_daotao_21_21 = False
        self.cosao_daotao_21_21 = False
        self.cosao_daotao_21_21 = False
        self.thoigian_21 = 0
        self.giaovien_21 = None
        self.noidung_chieu_chieu_21 = None
        self.coso_chieu_chieu_21 = False
        self.coso_chieu_chieu_21_21 = False
        self.coso_chieu_chieu_21_21 = False
        self.coso_chieu_chieu_21_21 = False
        self.thoigian_chieu_21 = 0
        self.giaovien_chieu_21 = None

        # Ngày _22
        self.lich_laplich_22 = None
        self.ngaynghi_22 = False
        self.cangay_22 = False
        self.noidung_sang_22 = None
        self.noidung_chieu_22 = None
        self.noidung_sang_char_22 = ''
        self.noidung_chieu_char_22 = ''
        self.coso_sang_22 = False
        self.coso_chieu_22 = False
        self.cosao_daotao_22_22 = False
        self.cosao_daotao_22_22 = False
        self.cosao_daotao_22_22 = False
        self.thoigian_22 = 0
        self.giaovien_22 = None
        self.noidung_chieu_chieu_22 = None
        self.coso_chieu_chieu_22 = False
        self.coso_chieu_chieu_22_22 = False
        self.coso_chieu_chieu_22_22 = False
        self.coso_chieu_chieu_22_22 = False
        self.thoigian_chieu_22 = 0
        self.giaovien_chieu_22 = None

        # Ngày _23
        self.lich_laplich_23 = None
        self.ngaynghi_23 = False
        self.cangay_23 = False
        self.noidung_sang_23 = None
        self.noidung_chieu_23 = None
        self.noidung_sang_char_23 = ''
        self.noidung_chieu_char_23 = ''
        self.coso_sang_23 = False
        self.coso_chieu_23 = False
        self.cosao_daotao_23_23 = False
        self.cosao_daotao_23_23 = False
        self.cosao_daotao_23_23 = False
        self.thoigian_23 = 0
        self.giaovien_23 = None
        self.noidung_chieu_chieu_23 = None
        self.coso_chieu_chieu_23 = False
        self.coso_chieu_chieu_23_23 = False
        self.coso_chieu_chieu_23_23 = False
        self.coso_chieu_chieu_23_23 = False
        self.thoigian_chieu_23 = 0
        self.giaovien_chieu_23 = None

        # Ngày _24
        self.lich_laplich_24 = None
        self.ngaynghi_24 = False
        self.cangay_24 = False
        self.noidung_sang_24 = None
        self.noidung_chieu_24 = None
        self.noidung_sang_char_24 = ''
        self.noidung_chieu_char_24 = ''
        self.coso_sang_24 = False
        self.coso_chieu_24 = False
        self.cosao_daotao_24_24 = False
        self.cosao_daotao_24_24 = False
        self.cosao_daotao_24_24 = False
        self.thoigian_24 = 0
        self.giaovien_24 = None
        self.noidung_chieu_chieu_24 = None
        self.coso_chieu_chieu_24 = False
        self.coso_chieu_chieu_24_24 = False
        self.coso_chieu_chieu_24_24 = False
        self.coso_chieu_chieu_24_24 = False
        self.thoigian_chieu_24 = 0
        self.giaovien_chieu_24 = None

        # Ngày _25
        self.lich_laplich_25 = None
        self.ngaynghi_25 = False
        self.cangay_25 = False
        self.noidung_sang_25 = None
        self.noidung_chieu_25 = None
        self.noidung_sang_char_25 = ''
        self.noidung_chieu_char_25 = ''
        self.coso_sang_25 = False
        self.coso_chieu_25 = False
        self.cosao_daotao_25_25 = False
        self.cosao_daotao_25_25 = False
        self.cosao_daotao_25_25 = False
        self.thoigian_25 = 0
        self.giaovien_25 = None
        self.noidung_chieu_chieu_25 = None
        self.coso_chieu_chieu_25 = False
        self.coso_chieu_chieu_25_25 = False
        self.coso_chieu_chieu_25_25 = False
        self.coso_chieu_chieu_25_25 = False
        self.thoigian_chieu_25 = 0
        self.giaovien_chieu_25 = None

        # Ngày _26
        self.lich_laplich_26 = None
        self.ngaynghi_26 = False
        self.cangay_26 = False
        self.noidung_sang_26 = None
        self.noidung_chieu_26 = None
        self.noidung_sang_char_26 = ''
        self.noidung_chieu_char_26 = ''
        self.coso_sang_26 = False
        self.coso_chieu_26 = False
        self.cosao_daotao_26_26 = False
        self.cosao_daotao_26_26 = False
        self.cosao_daotao_26_26 = False
        self.thoigian_26 = 0
        self.giaovien_26 = None
        self.noidung_chieu_chieu_26 = None
        self.coso_chieu_chieu_26 = False
        self.coso_chieu_chieu_26_26 = False
        self.coso_chieu_chieu_26_26 = False
        self.coso_chieu_chieu_26_26 = False
        self.thoigian_chieu_26 = 0
        self.giaovien_chieu_26 = None

        # Ngày _27
        self.lich_laplich_27 = None
        self.ngaynghi_27 = False
        self.cangay_27 = False
        self.noidung_sang_27 = None
        self.noidung_chieu_27 = None
        self.noidung_sang_char_27 = ''
        self.noidung_chieu_char_27 = ''
        self.coso_sang_27 = False
        self.coso_chieu_27 = False
        self.cosao_daotao_27_27 = False
        self.cosao_daotao_27_27 = False
        self.cosao_daotao_27_27 = False
        self.thoigian_27 = 0
        self.giaovien_27 = None
        self.noidung_chieu_chieu_27 = None
        self.coso_chieu_chieu_27 = False
        self.coso_chieu_chieu_27_27 = False
        self.coso_chieu_chieu_27_27 = False
        self.coso_chieu_chieu_27_27 = False
        self.thoigian_chieu_27 = 0
        self.giaovien_chieu_27 = None

        # Ngày _28
        self.lich_laplich_28 = None
        self.ngaynghi_28 = False
        self.cangay_28 = False
        self.noidung_sang_28 = None
        self.noidung_chieu_28 = None
        self.noidung_sang_char_28 = ''
        self.noidung_chieu_char_28 = ''
        self.coso_sang_28 = False
        self.coso_chieu_28 = False
        self.cosao_daotao_28_28 = False
        self.cosao_daotao_28_28 = False
        self.cosao_daotao_28_28 = False
        self.thoigian_28 = 0
        self.giaovien_28 = None
        self.noidung_chieu_chieu_28 = None
        self.coso_chieu_chieu_28 = False
        self.coso_chieu_chieu_28_28 = False
        self.coso_chieu_chieu_28_28 = False
        self.coso_chieu_chieu_28_28 = False
        self.thoigian_chieu_28 = 0
        self.giaovien_chieu_28 = None

        # Ngày _29
        self.lich_laplich_29 = None
        self.ngaynghi_29 = False
        self.cangay_29 = False
        self.noidung_sang_29 = None
        self.noidung_chieu_29 = None
        self.noidung_sang_char_29 = ''
        self.noidung_chieu_char_29 = ''
        self.coso_sang_29 = False
        self.coso_chieu_29 = False
        self.cosao_daotao_29_29 = False
        self.cosao_daotao_29_29 = False
        self.cosao_daotao_29_29 = False
        self.thoigian_29 = 0
        self.giaovien_29 = None
        self.noidung_chieu_chieu_29 = None
        self.coso_chieu_chieu_29 = False
        self.coso_chieu_chieu_29_29 = False
        self.coso_chieu_chieu_29_29 = False
        self.coso_chieu_chieu_29_29 = False
        self.thoigian_chieu_29 = 0
        self.giaovien_chieu_29 = None

        # Ngày _30
        self.lich_laplich_30 = None
        self.ngaynghi_30 = False
        self.cangay_30 = False
        self.noidung_sang_30 = None
        self.noidung_chieu_30 = None
        self.noidung_sang_char_30 = ''
        self.noidung_chieu_char_30 = ''
        self.coso_sang_30 = False
        self.coso_chieu_30 = False
        self.cosao_daotao_30_30 = False
        self.cosao_daotao_30_30 = False
        self.cosao_daotao_30_30 = False
        self.thoigian_30 = 0
        self.giaovien_30 = None
        self.noidung_chieu_chieu_30 = None
        self.coso_chieu_chieu_30 = False
        self.coso_chieu_chieu_30_30 = False
        self.coso_chieu_chieu_30_30 = False
        self.coso_chieu_chieu_30_30 = False
        self.thoigian_chieu_30 = 0
        self.giaovien_chieu_30 = None

        # Ngày _31
        self.lich_laplich_31 = None
        self.ngaynghi_31 = False
        self.cangay_31 = False
        self.noidung_sang_31 = None
        self.noidung_chieu_31 = None
        self.noidung_sang_char_31 = ''
        self.noidung_chieu_char_31 = ''
        self.coso_sang_31 = False
        self.coso_chieu_31 = False
        self.cosao_daotao_31_31 = False
        self.cosao_daotao_31_31 = False
        self.cosao_daotao_31_31 = False
        self.thoigian_31 = 0
        self.giaovien_31 = None
        self.noidung_chieu_chieu_31 = None
        self.coso_chieu_chieu_31 = False
        self.coso_chieu_chieu_31_31 = False
        self.coso_chieu_chieu_31_31 = False
        self.coso_chieu_chieu_31_31 = False
        self.thoigian_chieu_31 = 0
        self.giaovien_chieu_31 = None

        # Ngày _32
        self.lich_laplich_32 = None
        self.ngaynghi_32 = False
        self.cangay_32 = False
        self.noidung_sang_32 = None
        self.noidung_chieu_32 = None
        self.noidung_sang_char_32 = ''
        self.noidung_chieu_char_32 = ''
        self.coso_sang_32 = False
        self.coso_chieu_32 = False
        self.cosao_daotao_32_32 = False
        self.cosao_daotao_32_32 = False
        self.cosao_daotao_32_32 = False
        self.thoigian_32 = 0
        self.giaovien_32 = None
        self.noidung_chieu_chieu_32 = None
        self.coso_chieu_chieu_32 = False
        self.coso_chieu_chieu_32_32 = False
        self.coso_chieu_chieu_32_32 = False
        self.coso_chieu_chieu_32_32 = False
        self.thoigian_chieu_32 = 0
        self.giaovien_chieu_32 = None

        # Ngày _33
        self.lich_laplich_33 = None
        self.ngaynghi_33 = False
        self.cangay_33 = False
        self.noidung_sang_33 = None
        self.noidung_chieu_33 = None
        self.noidung_sang_char_33 = ''
        self.noidung_chieu_char_33 = ''
        self.coso_sang_33 = False
        self.coso_chieu_33 = False
        self.cosao_daotao_33_33 = False
        self.cosao_daotao_33_33 = False
        self.cosao_daotao_33_33 = False
        self.thoigian_33 = 0
        self.giaovien_33 = None
        self.noidung_chieu_chieu_33 = None
        self.coso_chieu_chieu_33 = False
        self.coso_chieu_chieu_33_33 = False
        self.coso_chieu_chieu_33_33 = False
        self.coso_chieu_chieu_33_33 = False
        self.thoigian_chieu_33 = 0
        self.giaovien_chieu_33 = None

        # Ngày _34
        self.lich_laplich_34 = None
        self.ngaynghi_34 = False
        self.cangay_34 = False
        self.noidung_sang_34 = None
        self.noidung_chieu_34 = None
        self.noidung_sang_char_34 = ''
        self.noidung_chieu_char_34 = ''
        self.coso_sang_34 = False
        self.coso_chieu_34 = False
        self.cosao_daotao_34_34 = False
        self.cosao_daotao_34_34 = False
        self.cosao_daotao_34_34 = False
        self.thoigian_34 = 0
        self.giaovien_34 = None
        self.noidung_chieu_chieu_34 = None
        self.coso_chieu_chieu_34 = False
        self.coso_chieu_chieu_34_34 = False
        self.coso_chieu_chieu_34_34 = False
        self.coso_chieu_chieu_34_34 = False
        self.thoigian_chieu_34 = 0
        self.giaovien_chieu_34 = None

        # Ngày _35
        self.lich_laplich_35 = None
        self.ngaynghi_35 = False
        self.cangay_35 = False
        self.noidung_sang_35 = None
        self.noidung_chieu_35 = None
        self.noidung_sang_char_35 = ''
        self.noidung_chieu_char_35 = ''
        self.coso_sang_35 = False
        self.coso_chieu_35 = False
        self.cosao_daotao_35_35 = False
        self.cosao_daotao_35_35 = False
        self.cosao_daotao_35_35 = False
        self.thoigian_35 = 0
        self.giaovien_35 = None
        self.noidung_chieu_chieu_35 = None
        self.coso_chieu_chieu_35 = False
        self.coso_chieu_chieu_35_35 = False
        self.coso_chieu_chieu_35_35 = False
        self.coso_chieu_chieu_35_35 = False
        self.thoigian_chieu_35 = 0
        self.giaovien_chieu_35 = None


        if self.ketthuc_daotao and self.batdau_daotao:
            start = datetime.datetime.strptime(str(self.batdau_daotao), "%Y-%m-%d")
            end = datetime.datetime.strptime(str(self.ketthuc_daotao), "%Y-%m-%d")
            the_end = end + datetime.timedelta(days=1)
            date_array = (start + datetime.timedelta(days=x) for x in range(0, (the_end - start).days))
            ngay_nghi = []
            if self.thu_hai == True:
                ngay_nghi.append(0)
            if self.thu_ba == True:
                ngay_nghi.append(1)
            if self.thu_bon == True:
                ngay_nghi.append(2)
            if self.thu_nam == True:
                ngay_nghi.append(3)
            if self.thu_sau == True:
                ngay_nghi.append(4)
            if self.thu_bay == True:
                ngay_nghi.append(5)
            if self.chu_nhat == True:
                ngay_nghi.append(6)
            item = 0
            for date_object in date_array:
                item += 1
                if item == 1:
                    self.lich_laplich_1 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_1 = True
                        self.noidung_sang_1 = None
                        self.noidung_chieu_1 = None
                        self.noidung_sang_char_1 = '休日'
                        self.noidung_chieu_char_1 = '休日'
                        self.coso_sang_1 = False
                        self.coso_chieu_1 = False
                        self.cosao_daotao_1_1 = False
                        self.cosao_daotao_2_1 = False
                        self.cosao_daotao_3_1 = False
                        self.thoigian_1 = 0
                        self.giaovien_1 = None
                        self.noidung_chieu_chieu_1 = None
                        self.coso_chieu_chieu_1 = False
                        self.coso_chieu_chieu_1_1 = False
                        self.coso_chieu_chieu_2_1 = False
                        self.coso_chieu_chieu_3_1 = False
                        self.thoigian_chieu_1 = 0
                        self.giaovien_chieu_1 = None
                    else:
                        self.ngaynghi_1 = False
                        self.noidung_sang_1 = None
                        self.noidung_chieu_1 = None
                        self.noidung_sang_char_1 = ''
                        self.noidung_chieu_char_1 = ''
                        self.coso_sang_1 = False
                        self.coso_chieu_1 = False
                        self.cosao_daotao_1_1 = True
                        self.cosao_daotao_2_1 = False
                        self.cosao_daotao_3_1 = False
                        self.thoigian_1 = 0
                        self.giaovien_1 = None
                        self.noidung_chieu_chieu_1 = None
                        self.coso_chieu_chieu_1 = False
                        self.coso_chieu_chieu_1_1 = False
                        self.coso_chieu_chieu_2_1 = False
                        self.coso_chieu_chieu_3_1 = False
                        self.thoigian_chieu_1 = 0
                        self.giaovien_chieu_1 = None

                if item == 2:
                    self.lich_laplich_2 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_2 = True
                        self.noidung_sang_2 = None
                        self.noidung_chieu_2 = None
                        self.noidung_sang_char_2 = '休日'
                        self.noidung_chieu_char_2 = '休日'
                        self.coso_sang_2 = False
                        self.coso_chieu_2 = False
                        self.cosao_daotao_2_2 = False
                        self.cosao_daotao_2_2 = False
                        self.cosao_daotao_3_2 = False
                        self.thoigian_2 = 0
                        self.giaovien_2 = None
                        self.noidung_chieu_chieu_2 = None
                        self.coso_chieu_chieu_2 = False
                        self.coso_chieu_chieu_2_2 = False
                        self.coso_chieu_chieu_2_2 = False
                        self.coso_chieu_chieu_3_2 = False
                        self.thoigian_chieu_2 = 0
                        self.giaovien_chieu_2 = None
                    else:
                        self.ngaynghi_2 = False
                        self.noidung_sang_2 = None
                        self.noidung_chieu_2 = None
                        self.noidung_sang_char_2 = ''
                        self.noidung_chieu_char_2 = ''
                        self.coso_sang_2 = False
                        self.coso_chieu_2 = False
                        self.cosao_daotao_2_2 = True
                        self.cosao_daotao_2_2 = False
                        self.cosao_daotao_3_2 = False
                        self.thoigian_2 = 0
                        self.giaovien_2 = None
                        self.noidung_chieu_chieu_2 = None
                        self.coso_chieu_chieu_2 = False
                        self.coso_chieu_chieu_2_2 = False
                        self.coso_chieu_chieu_2_2 = False
                        self.coso_chieu_chieu_3_2 = False
                        self.thoigian_chieu_2 = 0
                        self.giaovien_chieu_2 = None

                if item == 3:
                    self.lich_laplich_3 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_3 = True
                        self.noidung_sang_3 = None
                        self.noidung_chieu_3 = None
                        self.noidung_sang_char_3 = '休日'
                        self.noidung_chieu_char_3 = '休日'
                        self.coso_sang_3 = False
                        self.coso_chieu_3 = False
                        self.cosao_daotao_3_3 = False
                        self.cosao_daotao_2_3 = False
                        self.cosao_daotao_3_3 = False
                        self.thoigian_3 = 0
                        self.giaovien_3 = None
                        self.noidung_chieu_chieu_3 = None
                        self.coso_chieu_chieu_3 = False
                        self.coso_chieu_chieu_3_3 = False
                        self.coso_chieu_chieu_2_3 = False
                        self.coso_chieu_chieu_3_3 = False
                        self.thoigian_chieu_3 = 0
                        self.giaovien_chieu_3 = None
                    else:
                        self.ngaynghi_3 = False
                        self.noidung_sang_3 = None
                        self.noidung_chieu_3 = None
                        self.noidung_sang_char_3 = ''
                        self.noidung_chieu_char_3 = ''
                        self.coso_sang_3 = False
                        self.coso_chieu_3 = False
                        self.cosao_daotao_3_3 = True
                        self.cosao_daotao_2_3 = False
                        self.cosao_daotao_3_3 = False
                        self.thoigian_3 = 0
                        self.giaovien_3 = None
                        self.noidung_chieu_chieu_3 = None
                        self.coso_chieu_chieu_3 = False
                        self.coso_chieu_chieu_3_3 = False
                        self.coso_chieu_chieu_2_3 = False
                        self.coso_chieu_chieu_3_3 = False
                        self.thoigian_chieu_3 = 0
                        self.giaovien_chieu_3 = None

                if item == 4:
                    self.lich_laplich_4 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_4 = True
                        self.noidung_sang_4 = None
                        self.noidung_chieu_4 = None
                        self.noidung_sang_char_4 = '休日'
                        self.noidung_chieu_char_4 = '休日'
                        self.coso_sang_4 = False
                        self.coso_chieu_4 = False
                        self.cosao_daotao_4_4 = False
                        self.cosao_daotao_2_4 = False
                        self.cosao_daotao_3_4 = False
                        self.thoigian_4 = 0
                        self.giaovien_4 = None
                        self.noidung_chieu_chieu_4 = None
                        self.coso_chieu_chieu_4 = False
                        self.coso_chieu_chieu_4_4 = False
                        self.coso_chieu_chieu_2_4 = False
                        self.coso_chieu_chieu_3_4 = False
                        self.thoigian_chieu_4 = 0
                        self.giaovien_chieu_4 = None
                    else:
                        self.ngaynghi_4 = False
                        self.noidung_sang_4 = None
                        self.noidung_chieu_4 = None
                        self.noidung_sang_char_4 = ''
                        self.noidung_chieu_char_4 = ''
                        self.coso_sang_4 = False
                        self.coso_chieu_4 = False
                        self.cosao_daotao_4_4 = True
                        self.cosao_daotao_2_4 = False
                        self.cosao_daotao_3_4 = False
                        self.thoigian_4 = 0
                        self.giaovien_4 = None
                        self.noidung_chieu_chieu_4 = None
                        self.coso_chieu_chieu_4 = False
                        self.coso_chieu_chieu_4_4 = False
                        self.coso_chieu_chieu_2_4 = False
                        self.coso_chieu_chieu_3_4 = False
                        self.thoigian_chieu_4 = 0
                        self.giaovien_chieu_4 = None

                if item == 5:
                    self.lich_laplich_5 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_5 = True
                        self.noidung_sang_5 = None
                        self.noidung_chieu_5 = None
                        self.noidung_sang_char_5 = '休日'
                        self.noidung_chieu_char_5 = '休日'
                        self.coso_sang_5 = False
                        self.coso_chieu_5 = False
                        self.cosao_daotao_5_5 = False
                        self.cosao_daotao_2_5 = False
                        self.cosao_daotao_3_5 = False
                        self.thoigian_5 = 0
                        self.giaovien_5 = None
                        self.noidung_chieu_chieu_5 = None
                        self.coso_chieu_chieu_5 = False
                        self.coso_chieu_chieu_5_5 = False
                        self.coso_chieu_chieu_2_5 = False
                        self.coso_chieu_chieu_3_5 = False
                        self.thoigian_chieu_5 = 0
                        self.giaovien_chieu_5 = None
                    else:
                        self.ngaynghi_5 = False
                        self.noidung_sang_5 = None
                        self.noidung_chieu_5 = None
                        self.noidung_sang_char_5 = ''
                        self.noidung_chieu_char_5 = ''
                        self.coso_sang_5 = False
                        self.coso_chieu_5 = False
                        self.cosao_daotao_5_5 = True
                        self.cosao_daotao_2_5 = False
                        self.cosao_daotao_3_5 = False
                        self.thoigian_5 = 0
                        self.giaovien_5 = None
                        self.noidung_chieu_chieu_5 = None
                        self.coso_chieu_chieu_5 = False
                        self.coso_chieu_chieu_5_5 = False
                        self.coso_chieu_chieu_2_5 = False
                        self.coso_chieu_chieu_3_5 = False
                        self.thoigian_chieu_5 = 0
                        self.giaovien_chieu_5 = None

                if item == 6:
                    self.lich_laplich_6 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_6 = True
                        self.noidung_sang_6 = None
                        self.noidung_chieu_6 = None
                        self.noidung_sang_char_6 = '休日'
                        self.noidung_chieu_char_6 = '休日'
                        self.coso_sang_6 = False
                        self.coso_chieu_6 = False
                        self.cosao_daotao_6_6 = False
                        self.cosao_daotao_2_6 = False
                        self.cosao_daotao_3_6 = False
                        self.thoigian_6 = 0
                        self.giaovien_6 = None
                        self.noidung_chieu_chieu_6 = None
                        self.coso_chieu_chieu_6 = False
                        self.coso_chieu_chieu_6_6 = False
                        self.coso_chieu_chieu_2_6 = False
                        self.coso_chieu_chieu_3_6 = False
                        self.thoigian_chieu_6 = 0
                        self.giaovien_chieu_6 = None
                    else:
                        self.ngaynghi_6 = False
                        self.noidung_sang_6 = None
                        self.noidung_chieu_6 = None
                        self.noidung_sang_char_6 = ''
                        self.noidung_chieu_char_6 = ''
                        self.coso_sang_6 = False
                        self.coso_chieu_6 = False
                        self.cosao_daotao_6_6 = True
                        self.cosao_daotao_2_6 = False
                        self.cosao_daotao_3_6 = False
                        self.thoigian_6 = 0
                        self.giaovien_6 = None
                        self.noidung_chieu_chieu_6 = None
                        self.coso_chieu_chieu_6 = False
                        self.coso_chieu_chieu_6_6 = False
                        self.coso_chieu_chieu_2_6 = False
                        self.coso_chieu_chieu_3_6 = False
                        self.thoigian_chieu_6 = 0
                        self.giaovien_chieu_6 = None

                if item == 7:
                    self.lich_laplich_7 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_7 = True
                        self.noidung_sang_7 = None
                        self.noidung_chieu_7 = None
                        self.noidung_sang_char_7 = '休日'
                        self.noidung_chieu_char_7 = '休日'
                        self.coso_sang_7 = False
                        self.coso_chieu_7 = False
                        self.cosao_daotao_7_7 = False
                        self.cosao_daotao_2_7 = False
                        self.cosao_daotao_3_7 = False
                        self.thoigian_7 = 0
                        self.giaovien_7 = None
                        self.noidung_chieu_chieu_7 = None
                        self.coso_chieu_chieu_7 = False
                        self.coso_chieu_chieu_7_7 = False
                        self.coso_chieu_chieu_2_7 = False
                        self.coso_chieu_chieu_3_7 = False
                        self.thoigian_chieu_7 = 0
                        self.giaovien_chieu_7 = None
                    else:
                        self.ngaynghi_7 = False
                        self.noidung_sang_7 = None
                        self.noidung_chieu_7 = None
                        self.noidung_sang_char_7 = ''
                        self.noidung_chieu_char_7 = ''
                        self.coso_sang_7 = False
                        self.coso_chieu_7 = False
                        self.cosao_daotao_7_7 = True
                        self.cosao_daotao_2_7 = False
                        self.cosao_daotao_3_7 = False
                        self.thoigian_7 = 0
                        self.giaovien_7 = None
                        self.noidung_chieu_chieu_7 = None
                        self.coso_chieu_chieu_7 = False
                        self.coso_chieu_chieu_7_7 = False
                        self.coso_chieu_chieu_2_7 = False
                        self.coso_chieu_chieu_3_7 = False
                        self.thoigian_chieu_7 = 0
                        self.giaovien_chieu_7 = None

                if item == 8:
                    self.lich_laplich_8 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_8 = True
                        self.noidung_sang_8 = None
                        self.noidung_chieu_8 = None
                        self.noidung_sang_char_8 = '休日'
                        self.noidung_chieu_char_8 = '休日'
                        self.coso_sang_8 = False
                        self.coso_chieu_8 = False
                        self.cosao_daotao_8_8 = False
                        self.cosao_daotao_2_8 = False
                        self.cosao_daotao_3_8 = False
                        self.thoigian_8 = 0
                        self.giaovien_8 = None
                        self.noidung_chieu_chieu_8 = None
                        self.coso_chieu_chieu_8 = False
                        self.coso_chieu_chieu_8_8 = False
                        self.coso_chieu_chieu_2_8 = False
                        self.coso_chieu_chieu_3_8 = False
                        self.thoigian_chieu_8 = 0
                        self.giaovien_chieu_8 = None
                    else:
                        self.ngaynghi_8 = False
                        self.noidung_sang_8 = None
                        self.noidung_chieu_8 = None
                        self.noidung_sang_char_8 = ''
                        self.noidung_chieu_char_8 = ''
                        self.coso_sang_8 = False
                        self.coso_chieu_8 = False
                        self.cosao_daotao_8_8 = True
                        self.cosao_daotao_2_8 = False
                        self.cosao_daotao_3_8 = False
                        self.thoigian_8 = 0
                        self.giaovien_8 = None
                        self.noidung_chieu_chieu_8 = None
                        self.coso_chieu_chieu_8 = False
                        self.coso_chieu_chieu_8_8 = False
                        self.coso_chieu_chieu_2_8 = False
                        self.coso_chieu_chieu_3_8 = False
                        self.thoigian_chieu_8 = 0
                        self.giaovien_chieu_8 = None

                if item == 9:
                    self.lich_laplich_9 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_9 = True
                        self.noidung_sang_9 = None
                        self.noidung_chieu_9 = None
                        self.noidung_sang_char_9 = '休日'
                        self.noidung_chieu_char_9 = '休日'
                        self.coso_sang_9 = False
                        self.coso_chieu_9 = False
                        self.cosao_daotao_9_9 = False
                        self.cosao_daotao_2_9 = False
                        self.cosao_daotao_3_9 = False
                        self.thoigian_9 = 0
                        self.giaovien_9 = None
                        self.noidung_chieu_chieu_9 = None
                        self.coso_chieu_chieu_9 = False
                        self.coso_chieu_chieu_9_9 = False
                        self.coso_chieu_chieu_2_9 = False
                        self.coso_chieu_chieu_3_9 = False
                        self.thoigian_chieu_9 = 0
                        self.giaovien_chieu_9 = None
                    else:
                        self.ngaynghi_9 = False
                        self.noidung_sang_9 = None
                        self.noidung_chieu_9 = None
                        self.noidung_sang_char_9 = ''
                        self.noidung_chieu_char_9 = ''
                        self.coso_sang_9 = False
                        self.coso_chieu_9 = False
                        self.cosao_daotao_9_9 = True
                        self.cosao_daotao_2_9 = False
                        self.cosao_daotao_3_9 = False
                        self.thoigian_9 = 0
                        self.giaovien_9 = None
                        self.noidung_chieu_chieu_9 = None
                        self.coso_chieu_chieu_9 = False
                        self.coso_chieu_chieu_9_9 = False
                        self.coso_chieu_chieu_2_9 = False
                        self.coso_chieu_chieu_3_9 = False
                        self.thoigian_chieu_9 = 0
                        self.giaovien_chieu_9 = None

                if item == 10:
                    self.lich_laplich_10 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_10 = True
                        self.noidung_sang_10 = None
                        self.noidung_chieu_10 = None
                        self.noidung_sang_char_10 = '休日'
                        self.noidung_chieu_char_10 = '休日'
                        self.coso_sang_10 = False
                        self.coso_chieu_10 = False
                        self.cosao_daotao_10_10 = False
                        self.cosao_daotao_2_10 = False
                        self.cosao_daotao_3_10 = False
                        self.thoigian_10 = 0
                        self.giaovien_10 = None
                        self.noidung_chieu_chieu_10 = None
                        self.coso_chieu_chieu_10 = False
                        self.coso_chieu_chieu_10_10 = False
                        self.coso_chieu_chieu_2_10 = False
                        self.coso_chieu_chieu_3_10 = False
                        self.thoigian_chieu_10 = 0
                        self.giaovien_chieu_10 = None
                    else:
                        self.ngaynghi_10 = False
                        self.noidung_sang_10 = None
                        self.noidung_chieu_10 = None
                        self.noidung_sang_char_10 = ''
                        self.noidung_chieu_char_10 = ''
                        self.coso_sang_10 = False
                        self.coso_chieu_10 = False
                        self.cosao_daotao_10_10 = True
                        self.cosao_daotao_2_10 = False
                        self.cosao_daotao_3_10 = False
                        self.thoigian_10 = 0
                        self.giaovien_10 = None
                        self.noidung_chieu_chieu_10 = None
                        self.coso_chieu_chieu_10 = False
                        self.coso_chieu_chieu_10_10 = False
                        self.coso_chieu_chieu_2_10 = False
                        self.coso_chieu_chieu_3_10 = False
                        self.thoigian_chieu_10 = 0
                        self.giaovien_chieu_10 = None

                if item == 11:
                    self.lich_laplich_11 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_11 = True
                        self.noidung_sang_11 = None
                        self.noidung_chieu_11 = None
                        self.noidung_sang_char_11 = '休日'
                        self.noidung_chieu_char_11 = '休日'
                        self.coso_sang_11 = False
                        self.coso_chieu_11 = False
                        self.cosao_daotao_11_11 = False
                        self.cosao_daotao_2_11 = False
                        self.cosao_daotao_3_11 = False
                        self.thoigian_11 = 0
                        self.giaovien_11 = None
                        self.noidung_chieu_chieu_11 = None
                        self.coso_chieu_chieu_11 = False
                        self.coso_chieu_chieu_11_11 = False
                        self.coso_chieu_chieu_2_11 = False
                        self.coso_chieu_chieu_3_11 = False
                        self.thoigian_chieu_11 = 0
                        self.giaovien_chieu_11 = None
                    else:
                        self.ngaynghi_11 = False
                        self.noidung_sang_11 = None
                        self.noidung_chieu_11 = None
                        self.noidung_sang_char_11 = ''
                        self.noidung_chieu_char_11 = ''
                        self.coso_sang_11 = False
                        self.coso_chieu_11 = False
                        self.cosao_daotao_11_11 = True
                        self.cosao_daotao_2_11 = False
                        self.cosao_daotao_3_11 = False
                        self.thoigian_11 = 0
                        self.giaovien_11 = None
                        self.noidung_chieu_chieu_11 = None
                        self.coso_chieu_chieu_11 = False
                        self.coso_chieu_chieu_11_11 = False
                        self.coso_chieu_chieu_2_11 = False
                        self.coso_chieu_chieu_3_11 = False
                        self.thoigian_chieu_11 = 0
                        self.giaovien_chieu_11 = None

                if item == 12:
                    self.lich_laplich_12 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_12 = True
                        self.noidung_sang_12 = None
                        self.noidung_chieu_12 = None
                        self.noidung_sang_char_12 = '休日'
                        self.noidung_chieu_char_12 = '休日'
                        self.coso_sang_12 = False
                        self.coso_chieu_12 = False
                        self.cosao_daotao_12_12 = False
                        self.cosao_daotao_2_12 = False
                        self.cosao_daotao_3_12 = False
                        self.thoigian_12 = 0
                        self.giaovien_12 = None
                        self.noidung_chieu_chieu_12 = None
                        self.coso_chieu_chieu_12 = False
                        self.coso_chieu_chieu_12_12 = False
                        self.coso_chieu_chieu_2_12 = False
                        self.coso_chieu_chieu_3_12 = False
                        self.thoigian_chieu_12 = 0
                        self.giaovien_chieu_12 = None
                    else:
                        self.ngaynghi_12 = False
                        self.noidung_sang_12 = None
                        self.noidung_chieu_12 = None
                        self.noidung_sang_char_12 = ''
                        self.noidung_chieu_char_12 = ''
                        self.coso_sang_12 = False
                        self.coso_chieu_12 = False
                        self.cosao_daotao_12_12 = True
                        self.cosao_daotao_2_12 = False
                        self.cosao_daotao_3_12 = False
                        self.thoigian_12 = 0
                        self.giaovien_12 = None
                        self.noidung_chieu_chieu_12 = None
                        self.coso_chieu_chieu_12 = False
                        self.coso_chieu_chieu_12_12 = False
                        self.coso_chieu_chieu_2_12 = False
                        self.coso_chieu_chieu_3_12 = False
                        self.thoigian_chieu_12 = 0
                        self.giaovien_chieu_12 = None

                if item == 13:
                    self.lich_laplich_13 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_13 = True
                        self.noidung_sang_13 = None
                        self.noidung_chieu_13 = None
                        self.noidung_sang_char_13 = '休日'
                        self.noidung_chieu_char_13 = '休日'
                        self.coso_sang_13 = False
                        self.coso_chieu_13 = False
                        self.cosao_daotao_13_13 = False
                        self.cosao_daotao_2_13 = False
                        self.cosao_daotao_3_13 = False
                        self.thoigian_13 = 0
                        self.giaovien_13 = None
                        self.noidung_chieu_chieu_13 = None
                        self.coso_chieu_chieu_13 = False
                        self.coso_chieu_chieu_13_13 = False
                        self.coso_chieu_chieu_2_13 = False
                        self.coso_chieu_chieu_3_13 = False
                        self.thoigian_chieu_13 = 0
                        self.giaovien_chieu_13 = None
                    else:
                        self.ngaynghi_13 = False
                        self.noidung_sang_13 = None
                        self.noidung_chieu_13 = None
                        self.noidung_sang_char_13 = ''
                        self.noidung_chieu_char_13 = ''
                        self.coso_sang_13 = False
                        self.coso_chieu_13 = False
                        self.cosao_daotao_13_13 = True
                        self.cosao_daotao_2_13 = False
                        self.cosao_daotao_3_13 = False
                        self.thoigian_13 = 0
                        self.giaovien_13 = None
                        self.noidung_chieu_chieu_13 = None
                        self.coso_chieu_chieu_13 = False
                        self.coso_chieu_chieu_13_13 = False
                        self.coso_chieu_chieu_2_13 = False
                        self.coso_chieu_chieu_3_13 = False
                        self.thoigian_chieu_13 = 0
                        self.giaovien_chieu_13 = None

                if item == 14:
                    self.lich_laplich_14 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_14 = True
                        self.noidung_sang_14 = None
                        self.noidung_chieu_14 = None
                        self.noidung_sang_char_14 = '休日'
                        self.noidung_chieu_char_14 = '休日'
                        self.coso_sang_14 = False
                        self.coso_chieu_14 = False
                        self.cosao_daotao_14_14 = False
                        self.cosao_daotao_2_14 = False
                        self.cosao_daotao_3_14 = False
                        self.thoigian_14 = 0
                        self.giaovien_14 = None
                        self.noidung_chieu_chieu_14 = None
                        self.coso_chieu_chieu_14 = False
                        self.coso_chieu_chieu_14_14 = False
                        self.coso_chieu_chieu_2_14 = False
                        self.coso_chieu_chieu_3_14 = False
                        self.thoigian_chieu_14 = 0
                        self.giaovien_chieu_14 = None
                    else:
                        self.ngaynghi_14 = False
                        self.noidung_sang_14 = None
                        self.noidung_chieu_14 = None
                        self.noidung_sang_char_14 = ''
                        self.noidung_chieu_char_14 = ''
                        self.coso_sang_14 = False
                        self.coso_chieu_14 = False
                        self.cosao_daotao_14_14 = True
                        self.cosao_daotao_2_14 = False
                        self.cosao_daotao_3_14 = False
                        self.thoigian_14 = 0
                        self.giaovien_14 = None
                        self.noidung_chieu_chieu_14 = None
                        self.coso_chieu_chieu_14 = False
                        self.coso_chieu_chieu_14_14 = False
                        self.coso_chieu_chieu_2_14 = False
                        self.coso_chieu_chieu_3_14 = False
                        self.thoigian_chieu_14 = 0
                        self.giaovien_chieu_14 = None

                if item == 15:
                    self.lich_laplich_15 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_15 = True
                        self.noidung_sang_15 = None
                        self.noidung_chieu_15 = None
                        self.noidung_sang_char_15 = '休日'
                        self.noidung_chieu_char_15 = '休日'
                        self.coso_sang_15 = False
                        self.coso_chieu_15 = False
                        self.cosao_daotao_15_15 = False
                        self.cosao_daotao_2_15 = False
                        self.cosao_daotao_3_15 = False
                        self.thoigian_15 = 0
                        self.giaovien_15 = None
                        self.noidung_chieu_chieu_15 = None
                        self.coso_chieu_chieu_15 = False
                        self.coso_chieu_chieu_15_15 = False
                        self.coso_chieu_chieu_2_15 = False
                        self.coso_chieu_chieu_3_15 = False
                        self.thoigian_chieu_15 = 0
                        self.giaovien_chieu_15 = None
                    else:
                        self.ngaynghi_15 = False
                        self.noidung_sang_15 = None
                        self.noidung_chieu_15 = None
                        self.noidung_sang_char_15 = ''
                        self.noidung_chieu_char_15 = ''
                        self.coso_sang_15 = False
                        self.coso_chieu_15 = False
                        self.cosao_daotao_15_15 = True
                        self.cosao_daotao_2_15 = False
                        self.cosao_daotao_3_15 = False
                        self.thoigian_15 = 0
                        self.giaovien_15 = None
                        self.noidung_chieu_chieu_15 = None
                        self.coso_chieu_chieu_15 = False
                        self.coso_chieu_chieu_15_15 = False
                        self.coso_chieu_chieu_2_15 = False
                        self.coso_chieu_chieu_3_15 = False
                        self.thoigian_chieu_15 = 0
                        self.giaovien_chieu_15 = None

                if item == 16:
                    self.lich_laplich_16 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_16 = True
                        self.noidung_sang_16 = None
                        self.noidung_chieu_16 = None
                        self.noidung_sang_char_16 = '休日'
                        self.noidung_chieu_char_16 = '休日'
                        self.coso_sang_16 = False
                        self.coso_chieu_16 = False
                        self.cosao_daotao_16_16 = False
                        self.cosao_daotao_2_16 = False
                        self.cosao_daotao_3_16 = False
                        self.thoigian_16 = 0
                        self.giaovien_16 = None
                        self.noidung_chieu_chieu_16 = None
                        self.coso_chieu_chieu_16 = False
                        self.coso_chieu_chieu_16_16 = False
                        self.coso_chieu_chieu_2_16 = False
                        self.coso_chieu_chieu_3_16 = False
                        self.thoigian_chieu_16 = 0
                        self.giaovien_chieu_16 = None
                    else:
                        self.ngaynghi_16 = False
                        self.noidung_sang_16 = None
                        self.noidung_chieu_16 = None
                        self.noidung_sang_char_16 = ''
                        self.noidung_chieu_char_16 = ''
                        self.coso_sang_16 = False
                        self.coso_chieu_16 = False
                        self.cosao_daotao_16_16 = True
                        self.cosao_daotao_2_16 = False
                        self.cosao_daotao_3_16 = False
                        self.thoigian_16 = 0
                        self.giaovien_16 = None
                        self.noidung_chieu_chieu_16 = None
                        self.coso_chieu_chieu_16 = False
                        self.coso_chieu_chieu_16_16 = False
                        self.coso_chieu_chieu_2_16 = False
                        self.coso_chieu_chieu_3_16 = False
                        self.thoigian_chieu_16 = 0
                        self.giaovien_chieu_16 = None

                if item == 17:
                    self.lich_laplich_17 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_17 = True
                        self.noidung_sang_17 = None
                        self.noidung_chieu_17 = None
                        self.noidung_sang_char_17 = '休日'
                        self.noidung_chieu_char_17 = '休日'
                        self.coso_sang_17 = False
                        self.coso_chieu_17 = False
                        self.cosao_daotao_17_17 = False
                        self.cosao_daotao_2_17 = False
                        self.cosao_daotao_3_17 = False
                        self.thoigian_17 = 0
                        self.giaovien_17 = None
                        self.noidung_chieu_chieu_17 = None
                        self.coso_chieu_chieu_17 = False
                        self.coso_chieu_chieu_17_17 = False
                        self.coso_chieu_chieu_2_17 = False
                        self.coso_chieu_chieu_3_17 = False
                        self.thoigian_chieu_17 = 0
                        self.giaovien_chieu_17 = None
                    else:
                        self.ngaynghi_17 = False
                        self.noidung_sang_17 = None
                        self.noidung_chieu_17 = None
                        self.noidung_sang_char_17 = ''
                        self.noidung_chieu_char_17 = ''
                        self.coso_sang_17 = False
                        self.coso_chieu_17 = False
                        self.cosao_daotao_17_17 = True
                        self.cosao_daotao_2_17 = False
                        self.cosao_daotao_3_17 = False
                        self.thoigian_17 = 0
                        self.giaovien_17 = None
                        self.noidung_chieu_chieu_17 = None
                        self.coso_chieu_chieu_17 = False
                        self.coso_chieu_chieu_17_17 = False
                        self.coso_chieu_chieu_2_17 = False
                        self.coso_chieu_chieu_3_17 = False
                        self.thoigian_chieu_17 = 0
                        self.giaovien_chieu_17 = None

                if item == 18:
                    self.lich_laplich_18 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_18 = True
                        self.noidung_sang_18 = None
                        self.noidung_chieu_18 = None
                        self.noidung_sang_char_18 = '休日'
                        self.noidung_chieu_char_18 = '休日'
                        self.coso_sang_18 = False
                        self.coso_chieu_18 = False
                        self.cosao_daotao_18_18 = False
                        self.cosao_daotao_2_18 = False
                        self.cosao_daotao_3_18 = False
                        self.thoigian_18 = 0
                        self.giaovien_18 = None
                        self.noidung_chieu_chieu_18 = None
                        self.coso_chieu_chieu_18 = False
                        self.coso_chieu_chieu_18_18 = False
                        self.coso_chieu_chieu_2_18 = False
                        self.coso_chieu_chieu_3_18 = False
                        self.thoigian_chieu_18 = 0
                        self.giaovien_chieu_18 = None
                    else:
                        self.ngaynghi_18 = False
                        self.noidung_sang_18 = None
                        self.noidung_chieu_18 = None
                        self.noidung_sang_char_18 = ''
                        self.noidung_chieu_char_18 = ''
                        self.coso_sang_18 = False
                        self.coso_chieu_18 = False
                        self.cosao_daotao_18_18 = True
                        self.cosao_daotao_2_18 = False
                        self.cosao_daotao_3_18 = False
                        self.thoigian_18 = 0
                        self.giaovien_18 = None
                        self.noidung_chieu_chieu_18 = None
                        self.coso_chieu_chieu_18 = False
                        self.coso_chieu_chieu_18_18 = False
                        self.coso_chieu_chieu_2_18 = False
                        self.coso_chieu_chieu_3_18 = False
                        self.thoigian_chieu_18 = 0
                        self.giaovien_chieu_18 = None

                if item == 19:
                    self.lich_laplich_19 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_19 = True
                        self.noidung_sang_19 = None
                        self.noidung_chieu_19 = None
                        self.noidung_sang_char_19 = '休日'
                        self.noidung_chieu_char_19 = '休日'
                        self.coso_sang_19 = False
                        self.coso_chieu_19 = False
                        self.cosao_daotao_19_19 = False
                        self.cosao_daotao_2_19 = False
                        self.cosao_daotao_3_19 = False
                        self.thoigian_19 = 0
                        self.giaovien_19 = None
                        self.noidung_chieu_chieu_19 = None
                        self.coso_chieu_chieu_19 = False
                        self.coso_chieu_chieu_19_19 = False
                        self.coso_chieu_chieu_2_19 = False
                        self.coso_chieu_chieu_3_19 = False
                        self.thoigian_chieu_19 = 0
                        self.giaovien_chieu_19 = None
                    else:
                        self.ngaynghi_19 = False
                        self.noidung_sang_19 = None
                        self.noidung_chieu_19 = None
                        self.noidung_sang_char_19 = ''
                        self.noidung_chieu_char_19 = ''
                        self.coso_sang_19 = False
                        self.coso_chieu_19 = False
                        self.cosao_daotao_19_19 = True
                        self.cosao_daotao_2_19 = False
                        self.cosao_daotao_3_19 = False
                        self.thoigian_19 = 0
                        self.giaovien_19 = None
                        self.noidung_chieu_chieu_19 = None
                        self.coso_chieu_chieu_19 = False
                        self.coso_chieu_chieu_19_19 = False
                        self.coso_chieu_chieu_2_19 = False
                        self.coso_chieu_chieu_3_19 = False
                        self.thoigian_chieu_19 = 0
                        self.giaovien_chieu_19 = None

                if item == 20:
                    self.lich_laplich_20 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_20 = True
                        self.noidung_sang_20 = None
                        self.noidung_chieu_20 = None
                        self.noidung_sang_char_20 = '休日'
                        self.noidung_chieu_char_20 = '休日'
                        self.coso_sang_20 = False
                        self.coso_chieu_20 = False
                        self.cosao_daotao_20_20 = False
                        self.cosao_daotao_2_20 = False
                        self.cosao_daotao_3_20 = False
                        self.thoigian_20 = 0
                        self.giaovien_20 = None
                        self.noidung_chieu_chieu_20 = None
                        self.coso_chieu_chieu_20 = False
                        self.coso_chieu_chieu_20_20 = False
                        self.coso_chieu_chieu_2_20 = False
                        self.coso_chieu_chieu_3_20 = False
                        self.thoigian_chieu_20 = 0
                        self.giaovien_chieu_20 = None
                    else:
                        self.ngaynghi_20 = False
                        self.noidung_sang_20 = None
                        self.noidung_chieu_20 = None
                        self.noidung_sang_char_20 = ''
                        self.noidung_chieu_char_20 = ''
                        self.coso_sang_20 = False
                        self.coso_chieu_20 = False
                        self.cosao_daotao_20_20 = True
                        self.cosao_daotao_2_20 = False
                        self.cosao_daotao_3_20 = False
                        self.thoigian_20 = 0
                        self.giaovien_20 = None
                        self.noidung_chieu_chieu_20 = None
                        self.coso_chieu_chieu_20 = False
                        self.coso_chieu_chieu_20_20 = False
                        self.coso_chieu_chieu_2_20 = False
                        self.coso_chieu_chieu_3_20 = False
                        self.thoigian_chieu_20 = 0
                        self.giaovien_chieu_20 = None

                if item == 21:
                    self.lich_laplich_21 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_21 = True
                        self.noidung_sang_21 = None
                        self.noidung_chieu_21 = None
                        self.noidung_sang_char_21 = '休日'
                        self.noidung_chieu_char_21 = '休日'
                        self.coso_sang_21 = False
                        self.coso_chieu_21 = False
                        self.cosao_daotao_21_21 = False
                        self.cosao_daotao_2_21 = False
                        self.cosao_daotao_3_21 = False
                        self.thoigian_21 = 0
                        self.giaovien_21 = None
                        self.noidung_chieu_chieu_21 = None
                        self.coso_chieu_chieu_21 = False
                        self.coso_chieu_chieu_21_21 = False
                        self.coso_chieu_chieu_2_21 = False
                        self.coso_chieu_chieu_3_21 = False
                        self.thoigian_chieu_21 = 0
                        self.giaovien_chieu_21 = None
                    else:
                        self.ngaynghi_21 = False
                        self.noidung_sang_21 = None
                        self.noidung_chieu_21 = None
                        self.noidung_sang_char_21 = ''
                        self.noidung_chieu_char_21 = ''
                        self.coso_sang_21 = False
                        self.coso_chieu_21 = False
                        self.cosao_daotao_21_21 = True
                        self.cosao_daotao_2_21 = False
                        self.cosao_daotao_3_21 = False
                        self.thoigian_21 = 0
                        self.giaovien_21 = None
                        self.noidung_chieu_chieu_21 = None
                        self.coso_chieu_chieu_21 = False
                        self.coso_chieu_chieu_21_21 = False
                        self.coso_chieu_chieu_2_21 = False
                        self.coso_chieu_chieu_3_21 = False
                        self.thoigian_chieu_21 = 0
                        self.giaovien_chieu_21 = None

                if item == 22:
                    self.lich_laplich_22 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_22 = True
                        self.noidung_sang_22 = None
                        self.noidung_chieu_22 = None
                        self.noidung_sang_char_22 = '休日'
                        self.noidung_chieu_char_22 = '休日'
                        self.coso_sang_22 = False
                        self.coso_chieu_22 = False
                        self.cosao_daotao_22_22 = False
                        self.cosao_daotao_2_22 = False
                        self.cosao_daotao_3_22 = False
                        self.thoigian_22 = 0
                        self.giaovien_22 = None
                        self.noidung_chieu_chieu_22 = None
                        self.coso_chieu_chieu_22 = False
                        self.coso_chieu_chieu_22_22 = False
                        self.coso_chieu_chieu_2_22 = False
                        self.coso_chieu_chieu_3_22 = False
                        self.thoigian_chieu_22 = 0
                        self.giaovien_chieu_22 = None
                    else:
                        self.ngaynghi_22 = False
                        self.noidung_sang_22 = None
                        self.noidung_chieu_22 = None
                        self.noidung_sang_char_22 = ''
                        self.noidung_chieu_char_22 = ''
                        self.coso_sang_22 = False
                        self.coso_chieu_22 = False
                        self.cosao_daotao_22_22 = True
                        self.cosao_daotao_2_22 = False
                        self.cosao_daotao_3_22 = False
                        self.thoigian_22 = 0
                        self.giaovien_22 = None
                        self.noidung_chieu_chieu_22 = None
                        self.coso_chieu_chieu_22 = False
                        self.coso_chieu_chieu_22_22 = False
                        self.coso_chieu_chieu_2_22 = False
                        self.coso_chieu_chieu_3_22 = False
                        self.thoigian_chieu_22 = 0
                        self.giaovien_chieu_22 = None

                if item == 23:
                    self.lich_laplich_23 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_23 = True
                        self.noidung_sang_23 = None
                        self.noidung_chieu_23 = None
                        self.noidung_sang_char_23 = '休日'
                        self.noidung_chieu_char_23 = '休日'
                        self.coso_sang_23 = False
                        self.coso_chieu_23 = False
                        self.cosao_daotao_23_23 = False
                        self.cosao_daotao_2_23 = False
                        self.cosao_daotao_3_23 = False
                        self.thoigian_23 = 0
                        self.giaovien_23 = None
                        self.noidung_chieu_chieu_23 = None
                        self.coso_chieu_chieu_23 = False
                        self.coso_chieu_chieu_23_23 = False
                        self.coso_chieu_chieu_2_23 = False
                        self.coso_chieu_chieu_3_23 = False
                        self.thoigian_chieu_23 = 0
                        self.giaovien_chieu_23 = None
                    else:
                        self.ngaynghi_23 = False
                        self.noidung_sang_23 = None
                        self.noidung_chieu_23 = None
                        self.noidung_sang_char_23 = ''
                        self.noidung_chieu_char_23 = ''
                        self.coso_sang_23 = False
                        self.coso_chieu_23 = False
                        self.cosao_daotao_23_23 = True
                        self.cosao_daotao_2_23 = False
                        self.cosao_daotao_3_23 = False
                        self.thoigian_23 = 0
                        self.giaovien_23 = None
                        self.noidung_chieu_chieu_23 = None
                        self.coso_chieu_chieu_23 = False
                        self.coso_chieu_chieu_23_23 = False
                        self.coso_chieu_chieu_2_23 = False
                        self.coso_chieu_chieu_3_23 = False
                        self.thoigian_chieu_23 = 0
                        self.giaovien_chieu_23 = None

                if item == 24:
                    self.lich_laplich_24 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_24 = True
                        self.noidung_sang_24 = None
                        self.noidung_chieu_24 = None
                        self.noidung_sang_char_24 = '休日'
                        self.noidung_chieu_char_24 = '休日'
                        self.coso_sang_24 = False
                        self.coso_chieu_24 = False
                        self.cosao_daotao_24_24 = False
                        self.cosao_daotao_2_24 = False
                        self.cosao_daotao_3_24 = False
                        self.thoigian_24 = 0
                        self.giaovien_24 = None
                        self.noidung_chieu_chieu_24 = None
                        self.coso_chieu_chieu_24 = False
                        self.coso_chieu_chieu_24_24 = False
                        self.coso_chieu_chieu_2_24 = False
                        self.coso_chieu_chieu_3_24 = False
                        self.thoigian_chieu_24 = 0
                        self.giaovien_chieu_24 = None
                    else:
                        self.ngaynghi_24 = False
                        self.noidung_sang_24 = None
                        self.noidung_chieu_24 = None
                        self.noidung_sang_char_24 = ''
                        self.noidung_chieu_char_24 = ''
                        self.coso_sang_24 = False
                        self.coso_chieu_24 = False
                        self.cosao_daotao_24_24 = True
                        self.cosao_daotao_2_24 = False
                        self.cosao_daotao_3_24 = False
                        self.thoigian_24 = 0
                        self.giaovien_24 = None
                        self.noidung_chieu_chieu_24 = None
                        self.coso_chieu_chieu_24 = False
                        self.coso_chieu_chieu_24_24 = False
                        self.coso_chieu_chieu_2_24 = False
                        self.coso_chieu_chieu_3_24 = False
                        self.thoigian_chieu_24 = 0
                        self.giaovien_chieu_24 = None

                if item == 25:
                    self.lich_laplich_25 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_25 = True
                        self.noidung_sang_25 = None
                        self.noidung_chieu_25 = None
                        self.noidung_sang_char_25 = '休日'
                        self.noidung_chieu_char_25 = '休日'
                        self.coso_sang_25 = False
                        self.coso_chieu_25 = False
                        self.cosao_daotao_25_25 = False
                        self.cosao_daotao_2_25 = False
                        self.cosao_daotao_3_25 = False
                        self.thoigian_25 = 0
                        self.giaovien_25 = None
                        self.noidung_chieu_chieu_25 = None
                        self.coso_chieu_chieu_25 = False
                        self.coso_chieu_chieu_25_25 = False
                        self.coso_chieu_chieu_2_25 = False
                        self.coso_chieu_chieu_3_25 = False
                        self.thoigian_chieu_25 = 0
                        self.giaovien_chieu_25 = None
                    else:
                        self.ngaynghi_25 = False
                        self.noidung_sang_25 = None
                        self.noidung_chieu_25 = None
                        self.noidung_sang_char_25 = ''
                        self.noidung_chieu_char_25 = ''
                        self.coso_sang_25 = False
                        self.coso_chieu_25 = False
                        self.cosao_daotao_25_25 = True
                        self.cosao_daotao_2_25 = False
                        self.cosao_daotao_3_25 = False
                        self.thoigian_25 = 0
                        self.giaovien_25 = None
                        self.noidung_chieu_chieu_25 = None
                        self.coso_chieu_chieu_25 = False
                        self.coso_chieu_chieu_25_25 = False
                        self.coso_chieu_chieu_2_25 = False
                        self.coso_chieu_chieu_3_25 = False
                        self.thoigian_chieu_25 = 0
                        self.giaovien_chieu_25 = None

                if item == 26:
                    self.lich_laplich_26 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_26 = True
                        self.noidung_sang_26 = None
                        self.noidung_chieu_26 = None
                        self.noidung_sang_char_26 = '休日'
                        self.noidung_chieu_char_26 = '休日'
                        self.coso_sang_26 = False
                        self.coso_chieu_26 = False
                        self.cosao_daotao_26_26 = False
                        self.cosao_daotao_2_26 = False
                        self.cosao_daotao_3_26 = False
                        self.thoigian_26 = 0
                        self.giaovien_26 = None
                        self.noidung_chieu_chieu_26 = None
                        self.coso_chieu_chieu_26 = False
                        self.coso_chieu_chieu_26_26 = False
                        self.coso_chieu_chieu_2_26 = False
                        self.coso_chieu_chieu_3_26 = False
                        self.thoigian_chieu_26 = 0
                        self.giaovien_chieu_26 = None
                    else:
                        self.ngaynghi_26 = False
                        self.noidung_sang_26 = None
                        self.noidung_chieu_26 = None
                        self.noidung_sang_char_26 = ''
                        self.noidung_chieu_char_26 = ''
                        self.coso_sang_26 = False
                        self.coso_chieu_26 = False
                        self.cosao_daotao_26_26 = True
                        self.cosao_daotao_2_26 = False
                        self.cosao_daotao_3_26 = False
                        self.thoigian_26 = 0
                        self.giaovien_26 = None
                        self.noidung_chieu_chieu_26 = None
                        self.coso_chieu_chieu_26 = False
                        self.coso_chieu_chieu_26_26 = False
                        self.coso_chieu_chieu_2_26 = False
                        self.coso_chieu_chieu_3_26 = False
                        self.thoigian_chieu_26 = 0
                        self.giaovien_chieu_26 = None

                if item == 27:
                    self.lich_laplich_27 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_27 = True
                        self.noidung_sang_27 = None
                        self.noidung_chieu_27 = None
                        self.noidung_sang_char_27 = '休日'
                        self.noidung_chieu_char_27 = '休日'
                        self.coso_sang_27 = False
                        self.coso_chieu_27 = False
                        self.cosao_daotao_27_27 = False
                        self.cosao_daotao_2_27 = False
                        self.cosao_daotao_3_27 = False
                        self.thoigian_27 = 0
                        self.giaovien_27 = None
                        self.noidung_chieu_chieu_27 = None
                        self.coso_chieu_chieu_27 = False
                        self.coso_chieu_chieu_27_27 = False
                        self.coso_chieu_chieu_2_27 = False
                        self.coso_chieu_chieu_3_27 = False
                        self.thoigian_chieu_27 = 0
                        self.giaovien_chieu_27 = None
                    else:
                        self.ngaynghi_27 = False
                        self.noidung_sang_27 = None
                        self.noidung_chieu_27 = None
                        self.noidung_sang_char_27 = ''
                        self.noidung_chieu_char_27 = ''
                        self.coso_sang_27 = False
                        self.coso_chieu_27 = False
                        self.cosao_daotao_27_27 = True
                        self.cosao_daotao_2_27 = False
                        self.cosao_daotao_3_27 = False
                        self.thoigian_27 = 0
                        self.giaovien_27 = None
                        self.noidung_chieu_chieu_27 = None
                        self.coso_chieu_chieu_27 = False
                        self.coso_chieu_chieu_27_27 = False
                        self.coso_chieu_chieu_2_27 = False
                        self.coso_chieu_chieu_3_27 = False
                        self.thoigian_chieu_27 = 0
                        self.giaovien_chieu_27 = None

                if item == 28:
                    self.lich_laplich_28 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_28 = True
                        self.noidung_sang_28 = None
                        self.noidung_chieu_28 = None
                        self.noidung_sang_char_28 = '休日'
                        self.noidung_chieu_char_28 = '休日'
                        self.coso_sang_28 = False
                        self.coso_chieu_28 = False
                        self.cosao_daotao_28_28 = False
                        self.cosao_daotao_2_28 = False
                        self.cosao_daotao_3_28 = False
                        self.thoigian_28 = 0
                        self.giaovien_28 = None
                        self.noidung_chieu_chieu_28 = None
                        self.coso_chieu_chieu_28 = False
                        self.coso_chieu_chieu_28_28 = False
                        self.coso_chieu_chieu_2_28 = False
                        self.coso_chieu_chieu_3_28 = False
                        self.thoigian_chieu_28 = 0
                        self.giaovien_chieu_28 = None
                    else:
                        self.ngaynghi_28 = False
                        self.noidung_sang_28 = None
                        self.noidung_chieu_28 = None
                        self.noidung_sang_char_28 = ''
                        self.noidung_chieu_char_28 = ''
                        self.coso_sang_28 = False
                        self.coso_chieu_28 = False
                        self.cosao_daotao_28_28 = True
                        self.cosao_daotao_2_28 = False
                        self.cosao_daotao_3_28 = False
                        self.thoigian_28 = 0
                        self.giaovien_28 = None
                        self.noidung_chieu_chieu_28 = None
                        self.coso_chieu_chieu_28 = False
                        self.coso_chieu_chieu_28_28 = False
                        self.coso_chieu_chieu_2_28 = False
                        self.coso_chieu_chieu_3_28 = False
                        self.thoigian_chieu_28 = 0
                        self.giaovien_chieu_28 = None

                if item == 29:
                    self.lich_laplich_29 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_29 = True
                        self.noidung_sang_29 = None
                        self.noidung_chieu_29 = None
                        self.noidung_sang_char_29 = '休日'
                        self.noidung_chieu_char_29 = '休日'
                        self.coso_sang_29 = False
                        self.coso_chieu_29 = False
                        self.cosao_daotao_29_29 = False
                        self.cosao_daotao_2_29 = False
                        self.cosao_daotao_3_29 = False
                        self.thoigian_29 = 0
                        self.giaovien_29 = None
                        self.noidung_chieu_chieu_29 = None
                        self.coso_chieu_chieu_29 = False
                        self.coso_chieu_chieu_29_29 = False
                        self.coso_chieu_chieu_2_29 = False
                        self.coso_chieu_chieu_3_29 = False
                        self.thoigian_chieu_29 = 0
                        self.giaovien_chieu_29 = None
                    else:
                        self.ngaynghi_29 = False
                        self.noidung_sang_29 = None
                        self.noidung_chieu_29 = None
                        self.noidung_sang_char_29 = ''
                        self.noidung_chieu_char_29 = ''
                        self.coso_sang_29 = False
                        self.coso_chieu_29 = False
                        self.cosao_daotao_29_29 = True
                        self.cosao_daotao_2_29 = False
                        self.cosao_daotao_3_29 = False
                        self.thoigian_29 = 0
                        self.giaovien_29 = None
                        self.noidung_chieu_chieu_29 = None
                        self.coso_chieu_chieu_29 = False
                        self.coso_chieu_chieu_29_29 = False
                        self.coso_chieu_chieu_2_29 = False
                        self.coso_chieu_chieu_3_29 = False
                        self.thoigian_chieu_29 = 0
                        self.giaovien_chieu_29 = None

                if item == 30:
                    self.lich_laplich_30 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_30 = True
                        self.noidung_sang_30 = None
                        self.noidung_chieu_30 = None
                        self.noidung_sang_char_30 = '休日'
                        self.noidung_chieu_char_30 = '休日'
                        self.coso_sang_30 = False
                        self.coso_chieu_30 = False
                        self.cosao_daotao_30_30 = False
                        self.cosao_daotao_3_30 = False
                        self.cosao_daotao_3_30 = False
                        self.thoigian_30 = 0
                        self.giaovien_30 = None
                        self.noidung_chieu_chieu_30 = None
                        self.coso_chieu_chieu_30 = False
                        self.coso_chieu_chieu_30_30 = False
                        self.coso_chieu_chieu_3_30 = False
                        self.coso_chieu_chieu_3_30 = False
                        self.thoigian_chieu_30 = 0
                        self.giaovien_chieu_30 = None
                    else:
                        self.ngaynghi_30 = False
                        self.noidung_sang_30 = None
                        self.noidung_chieu_30 = None
                        self.noidung_sang_char_30 = ''
                        self.noidung_chieu_char_30 = ''
                        self.coso_sang_30 = False
                        self.coso_chieu_30 = False
                        self.cosao_daotao_30_30 = True
                        self.cosao_daotao_3_30 = False
                        self.cosao_daotao_3_30 = False
                        self.thoigian_30 = 0
                        self.giaovien_30 = None
                        self.noidung_chieu_chieu_30 = None
                        self.coso_chieu_chieu_30 = False
                        self.coso_chieu_chieu_30_30 = False
                        self.coso_chieu_chieu_3_30 = False
                        self.coso_chieu_chieu_3_30 = False
                        self.thoigian_chieu_30 = 0
                        self.giaovien_chieu_30 = None

                if item == 31:
                    self.lich_laplich_31 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_31 = True
                        self.noidung_sang_31 = None
                        self.noidung_chieu_31 = None
                        self.noidung_sang_char_31 = '休日'
                        self.noidung_chieu_char_31 = '休日'
                        self.coso_sang_31 = False
                        self.coso_chieu_31 = False
                        self.cosao_daotao_31_31 = False
                        self.cosao_daotao_3_31 = False
                        self.cosao_daotao_3_31 = False
                        self.thoigian_31 = 0
                        self.giaovien_31 = None
                        self.noidung_chieu_chieu_31 = None
                        self.coso_chieu_chieu_31 = False
                        self.coso_chieu_chieu_31_31 = False
                        self.coso_chieu_chieu_3_31 = False
                        self.coso_chieu_chieu_3_31 = False
                        self.thoigian_chieu_31 = 0
                        self.giaovien_chieu_31 = None
                    else:
                        self.ngaynghi_31 = False
                        self.noidung_sang_31 = None
                        self.noidung_chieu_31 = None
                        self.noidung_sang_char_31 = ''
                        self.noidung_chieu_char_31 = ''
                        self.coso_sang_31 = False
                        self.coso_chieu_31 = False
                        self.cosao_daotao_31_31 = True
                        self.cosao_daotao_3_31 = False
                        self.cosao_daotao_3_31 = False
                        self.thoigian_31 = 0
                        self.giaovien_31 = None
                        self.noidung_chieu_chieu_31 = None
                        self.coso_chieu_chieu_31 = False
                        self.coso_chieu_chieu_31_31 = False
                        self.coso_chieu_chieu_3_31 = False
                        self.coso_chieu_chieu_3_31 = False
                        self.thoigian_chieu_31 = 0
                        self.giaovien_chieu_31 = None

                if item == 32:
                    self.lich_laplich_32 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_32 = True
                        self.noidung_sang_32 = None
                        self.noidung_chieu_32 = None
                        self.noidung_sang_char_32 = '休日'
                        self.noidung_chieu_char_32 = '休日'
                        self.coso_sang_32 = False
                        self.coso_chieu_32 = False
                        self.cosao_daotao_32_32 = False
                        self.cosao_daotao_3_32 = False
                        self.cosao_daotao_3_32 = False
                        self.thoigian_32 = 0
                        self.giaovien_32 = None
                        self.noidung_chieu_chieu_32 = None
                        self.coso_chieu_chieu_32 = False
                        self.coso_chieu_chieu_32_32 = False
                        self.coso_chieu_chieu_3_32 = False
                        self.coso_chieu_chieu_3_32 = False
                        self.thoigian_chieu_32 = 0
                        self.giaovien_chieu_32 = None
                    else:
                        self.ngaynghi_32 = False
                        self.noidung_sang_32 = None
                        self.noidung_chieu_32 = None
                        self.noidung_sang_char_32 = ''
                        self.noidung_chieu_char_32 = ''
                        self.coso_sang_32 = False
                        self.coso_chieu_32 = False
                        self.cosao_daotao_32_32 = True
                        self.cosao_daotao_3_32 = False
                        self.cosao_daotao_3_32 = False
                        self.thoigian_32 = 0
                        self.giaovien_32 = None
                        self.noidung_chieu_chieu_32 = None
                        self.coso_chieu_chieu_32 = False
                        self.coso_chieu_chieu_32_32 = False
                        self.coso_chieu_chieu_3_32 = False
                        self.coso_chieu_chieu_3_32 = False
                        self.thoigian_chieu_32 = 0
                        self.giaovien_chieu_32 = None

                if item == 33:
                    self.lich_laplich_33 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_33 = True
                        self.noidung_sang_33 = None
                        self.noidung_chieu_33 = None
                        self.noidung_sang_char_33 = '休日'
                        self.noidung_chieu_char_33 = '休日'
                        self.coso_sang_33 = False
                        self.coso_chieu_33 = False
                        self.cosao_daotao_33_33 = False
                        self.cosao_daotao_3_33 = False
                        self.cosao_daotao_3_33 = False
                        self.thoigian_33 = 0
                        self.giaovien_33 = None
                        self.noidung_chieu_chieu_33 = None
                        self.coso_chieu_chieu_33 = False
                        self.coso_chieu_chieu_33_33 = False
                        self.coso_chieu_chieu_3_33 = False
                        self.coso_chieu_chieu_3_33 = False
                        self.thoigian_chieu_33 = 0
                        self.giaovien_chieu_33 = None
                    else:
                        self.ngaynghi_33 = False
                        self.noidung_sang_33 = None
                        self.noidung_chieu_33 = None
                        self.noidung_sang_char_33 = ''
                        self.noidung_chieu_char_33 = ''
                        self.coso_sang_33 = False
                        self.coso_chieu_33 = False
                        self.cosao_daotao_33_33 = True
                        self.cosao_daotao_3_33 = False
                        self.cosao_daotao_3_33 = False
                        self.thoigian_33 = 0
                        self.giaovien_33 = None
                        self.noidung_chieu_chieu_33 = None
                        self.coso_chieu_chieu_33 = False
                        self.coso_chieu_chieu_33_33 = False
                        self.coso_chieu_chieu_3_33 = False
                        self.coso_chieu_chieu_3_33 = False
                        self.thoigian_chieu_33 = 0
                        self.giaovien_chieu_33 = None

                if item == 34:
                    self.lich_laplich_34 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_34 = True
                        self.noidung_sang_34 = None
                        self.noidung_chieu_34 = None
                        self.noidung_sang_char_34 = '休日'
                        self.noidung_chieu_char_34 = '休日'
                        self.coso_sang_34 = False
                        self.coso_chieu_34 = False
                        self.cosao_daotao_34_34 = False
                        self.cosao_daotao_3_34 = False
                        self.cosao_daotao_3_34 = False
                        self.thoigian_34 = 0
                        self.giaovien_34 = None
                        self.noidung_chieu_chieu_34 = None
                        self.coso_chieu_chieu_34 = False
                        self.coso_chieu_chieu_34_34 = False
                        self.coso_chieu_chieu_3_34 = False
                        self.coso_chieu_chieu_3_34 = False
                        self.thoigian_chieu_34 = 0
                        self.giaovien_chieu_34 = None
                    else:
                        self.ngaynghi_34 = False
                        self.noidung_sang_34 = None
                        self.noidung_chieu_34 = None
                        self.noidung_sang_char_34 = ''
                        self.noidung_chieu_char_34 = ''
                        self.coso_sang_34 = False
                        self.coso_chieu_34 = False
                        self.cosao_daotao_34_34 = True
                        self.cosao_daotao_3_34 = False
                        self.cosao_daotao_3_34 = False
                        self.thoigian_34 = 0
                        self.giaovien_34 = None
                        self.noidung_chieu_chieu_34 = None
                        self.coso_chieu_chieu_34 = False
                        self.coso_chieu_chieu_34_34 = False
                        self.coso_chieu_chieu_3_34 = False
                        self.coso_chieu_chieu_3_34 = False
                        self.thoigian_chieu_34 = 0
                        self.giaovien_chieu_34 = None

                if item == 35:
                    self.lich_laplich_35 = date_object.date()
                    if date_object.weekday() in ngay_nghi:
                        self.ngaynghi_35 = True
                        self.noidung_sang_35 = None
                        self.noidung_chieu_35 = None
                        self.noidung_sang_char_35 = '休日'
                        self.noidung_chieu_char_35 = '休日'
                        self.coso_sang_35 = False
                        self.coso_chieu_35 = False
                        self.cosao_daotao_35_35 = False
                        self.cosao_daotao_3_35 = False
                        self.cosao_daotao_3_35 = False
                        self.thoigian_35 = 0
                        self.giaovien_35 = None
                        self.noidung_chieu_chieu_35 = None
                        self.coso_chieu_chieu_35 = False
                        self.coso_chieu_chieu_35_35 = False
                        self.coso_chieu_chieu_3_35 = False
                        self.coso_chieu_chieu_3_35 = False
                        self.thoigian_chieu_35 = 0
                        self.giaovien_chieu_35 = None
                    else:
                        self.ngaynghi_35 = False
                        self.noidung_sang_35 = None
                        self.noidung_chieu_35 = None
                        self.noidung_sang_char_35 = ''
                        self.noidung_chieu_char_35 = ''
                        self.coso_sang_35 = False
                        self.coso_chieu_35 = False
                        self.cosao_daotao_35_35 = True
                        self.cosao_daotao_3_35 = False
                        self.cosao_daotao_3_35 = False
                        self.thoigian_35 = 0
                        self.giaovien_35 = None
                        self.noidung_chieu_chieu_35 = None
                        self.coso_chieu_chieu_35 = False
                        self.coso_chieu_chieu_35_35 = False
                        self.coso_chieu_chieu_3_35 = False
                        self.coso_chieu_chieu_3_35 = False
                        self.thoigian_chieu_35 = 0
                        self.giaovien_chieu_35 = None

    @api.multi
    def duplicate_coso_daotaosau(self):
        ids_coso_daotaosau = self.coso_daotaosau.ids
        ids_giangvien_daotaosau = self.giangvien_daotaosau.ids
        ids_laplich_daotaosau = self.laplich_daotaosau.ids
        mass_mailing_copy = self.copy(default={
            'donhang_daotaosau': self.donhang_daotaosau.id,
            'xinghiep_daotaosau': self.xinghiep_daotaosau.id,
            'coso_daotaosau': [(6, False, ids_coso_daotaosau)],
            'giangvien_daotaosau': [(6, False, ids_giangvien_daotaosau)],
            'laplich_daotaosau': [(6, False, ids_laplich_daotaosau)],
            'giangvien_phapluat_daotaosau': self.giangvien_phapluat_daotaosau,
            'nghenghiep_giangvien_daotaosau': self.nghenghiep_giangvien_daotaosau,
            'coquan_giangvien_daotaosau': self.coquan_giangvien_daotaosau,
            'kinhnghiem_giangvien_daotaosau': self.kinhnghiem_giangvien_daotaosau,
            'batdau_sang': self.batdau_sang,
            'ketthuc_sang': self.ketthuc_sang,
            'batdau_chieu': self.batdau_chieu,
            'ketthuc_chieu': self.ketthuc_chieu,
            'batdau_daotao': self.batdau_daotao,
            'ketthuc_daotao': self.ketthuc_daotao,
            'tonggio_daotao': self.tonggio_daotao,
            'thu_hai': self.thu_hai,
            'thu_ba': self.thu_ba,
            'thu_bon': self.thu_bon,
            'thu_nam': self.thu_nam,
            'thu_sau': self.thu_sau,
            'thu_bay': self.thu_bay,
            'chu_nhat': self.chu_nhat,
            'ngay_le': self.ngay_le,
            'total_time': self.total_time,
        })
        if mass_mailing_copy:
            context = dict(self.env.context)
            context['form_view_initial_mode'] = 'edit'
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'daotao.daotaosau',
                'res_id': mass_mailing_copy.id,
                'context': context,
            }
        return False
