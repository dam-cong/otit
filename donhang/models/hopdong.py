# -*- coding: utf-8 -*-
from odoo import models, fields, api


class HopDong(models.Model):
    _name = 'hopdong.hopdong'
    _rec_name = 'thuctapsinh_hopdong'
    _order = 'id desc'

    thuctapsinh_hopdong = fields.Many2one(comodel_name='thuctapsinh.thuctapsinh', string='Thực tập sinh',
                                          required=True, )
    donhang_hopdong = fields.Many2one(comodel_name="donhang.donhang", string="Đơn hàng",
                                      related='thuctapsinh_hopdong.donhang_tts')
    giaidoan_hopdong = fields.Selection(string='Giai đoạn',
                                        selection=[('1 go', '1 go'),
                                                   ('2 go', '2 go'),
                                                   ('3 go', '3 go')])

    # Hợp đồng lao động
    quydinh_hopdong_thoihan = fields.Selection(string="Có quy định hợp đồng lao động về thời hạn hay không",
                                               selection=[('Có', 'Có'), ('Không', 'Không')],
                                               default='Có')  # hhjp_0703
    batdau_tgian_congviec = fields.Date(string="Bắt đầu")  # hhjp_0148
    kethuc_tgian_congviec = fields.Date(string="Kết thúc")  # hhjp_0380

    @api.onchange('quydinh_hopdong_thoihan')
    def onchange_method_quydinh_hopdong_thoihan(self):
        self.batdau_tgian_congviec = None
        self.kethuc_tgian_congviec = None

    dukien_nhapcanh = fields.Date(string="Ngày dự kiến nhập cảnh",
                                  related='donhang_hopdong.thoigian_donhang')  # hhjp_0902
    giahan_hopdong = fields.Selection(string="Có gia hạn hợp đồng hay không",
                                      selection=[('Có', 'Có'), ('Không', 'Không')])  # hhjp_0381
    tonggio_thuctap_kinang = fields.Float(string="Tổng số giờ thực tập kĩ năng theo giai đoạn")  # hhjp_0150
    ten_noilam_han = fields.Many2many(comodel_name="xinghiep.chinhanh", string="Tên chi nhánh xí nghiệp (Hán)")
    macongviec_tts = fields.Char(string='Mã công việc', related='donhang_hopdong.ma_congviec_donhang')
    ten_nganhnghe_tts = fields.Many2one(comodel_name='nganhnghe.nganhnghe', string='Ngành nghề',
                                        related='donhang_hopdong.nganhnghe_donhang')
    loai_nghe_nganhnghe_tts = fields.Many2one(comodel_name='loaicongviec.loaicongviec', string='Loại công việc',
                                              related='donhang_hopdong.loainghe_donhang')
    congviec_nganhnghe_tts = fields.Many2one(comodel_name='congviec.congviec', string='Công việc',
                                             related='donhang_hopdong.congviec_donhang')
    loai_nghe_nganhnghe_tts_viet = fields.Char(string='Loại công việc', related="donhang_hopdong.loainghe_donhang.name_loaicongviec_viet")
    congviec_nganhnghe_tts_viet = fields.Char(string='Công việc')
    macongviec_congviec = fields.Char(string='Mã công việc', store=True,
                                      related='donhang_hopdong.ma_congviec_nhieunganhnghe')
    ten_nganhnghe_congviec = fields.Many2one(comodel_name='nganhnghe.nganhnghe', string='Ngành nghề', store=True,
                                             related='donhang_hopdong.nganhnghe_nhieunganhnghe')
    loai_nghe_nganhnghe_congviec = fields.Many2one(comodel_name='loaicongviec.loaicongviec', string='Loại công việc',
                                                   store=True, related='donhang_hopdong.loainghe_nhieunganhnghe')
    congviec_nganhnghe_congviec = fields.Many2one(comodel_name='congviec.congviec', string='Công việc', store=True
                                                  , related='donhang_hopdong.congviec_nhieunganhnghe')
    loai_nghe_nganhnghe_congviec_viet = fields.Char(string='Loại công việc')  # tiếng Việt
    congviec_nganhnghe_congviec_viet = fields.Char(string='Công việc')  # tiếng Việt
    codinh_hopdong = fields.Boolean(string='Cố định')
    chedo_hopdong = fields.Boolean(string='Chế độ giờ lao động')
    khungca_hopdong = fields.Boolean(string='Khung ca')
    batdau_codinh_sang_hopdong = fields.Float(string='Sáng')  # hhjp_0903
    kethuc_codinh_sang_hopdong = fields.Float()  # hhjp_0382
    batdau_codinh_trua_hopdong = fields.Float(string='Nghỉ trưa')
    kethuc_codinh_trua_hopdong = fields.Float()
    batdau_codinh_chieu_hopdong = fields.Float(string='Chiều')
    kethuc_codinh_chieu_hopdong = fields.Float()
    batdau_codinh_chieu1_hopdong = fields.Float(string='Nghỉ chiều')
    kethuc_codinh_chieu1_hopdong = fields.Float()
    batdau_codinh_chieu2_hopdong = fields.Float(string='Nghỉ chiều')
    kethuc_codinh_chieu2_hopdong = fields.Float()
    laodong_gio_tts = fields.Float(string="Tổng giờ")  # hhjp_0383
    donvithaydoi_tts = fields.Char(string="Đơn vị thay đổi")  #
    donvithaydoi_tts_viet = fields.Char(string="Đơn vị thay đổi (Tiếng mẹ đẻ)")  # tiếng Việt
    batdau_tgian_viecca_mot = fields.Float(string="Bắt đầu")  # hhjp_0386
    ketthuc_tgian_viecca_mot = fields.Float(string="Kết thúc")  # hhjp_0387
    apdung_tgian_viecca_mot = fields.Date(string='Ngày áp dụng')
    sogio_laodong_motngay_mot = fields.Float(string="Số giờ lao động một ngày")  # hhjp_0383
    batdau_tgian_viecca_hai = fields.Float(string="Bắt đầu")  #
    ketthuc_tgian_viecca_hai = fields.Float(string="Kết thúc")  #
    apdung_tgian_viecca_hai = fields.Date(string='Ngày áp dụng')
    sogio_laodong_motngay_hai = fields.Float(string="Số giờ lao động một ngày")  #
    batdau_tgian_viecca_ba = fields.Float(string="Bắt đầu")  #
    ketthuc_tgian_viecca_ba = fields.Float(string="Kết thúc")  #
    apdung_tgian_viecca_ba = fields.Date(string='Ngày áp dụng')
    sogio_laodong_motngay_ba = fields.Float(string="Số giờ lao động một ngày")  #
    thoigian_giailao = fields.Integer(string="Thời gian nghỉ giải lao (phút)")  # hhjp_0151

    giolaodong_motthang = fields.Integer(string="Số giờ lao động quy đinh trong một tháng")  # hhjp_0388_h
    phutlaodong_motthang = fields.Integer()  # hhjp_0388_p

    giolaodong_mottuan = fields.Integer(string="Số giờ lao động quy đinh trong một tuần")  # hhjp_0153
    giolaodong_motnam = fields.Integer(string="Số giờ lao động quy đinh trong một năm")  # hhjp_0152
    loadong_ngoaigioi = fields.Selection(string="Lao động ngoài giờ quy định",
                                         selection=[('Có', 'Có'), ('Không', 'Không')])  # hhjp_0392
    namnhat_ngaylaodong = fields.Integer(string="１年目")  # hhjp_0389
    namhai_ngaylaodong = fields.Integer(string="２年目")  # hhjp_0390
    namba_ngaylaodong = fields.Integer(string="３年目")  # hhjp_0391
    tudieu_mot_hopdong = fields.Char(string='Nội quy lao động')  # hhjp_nq1
    dendieu_mot_hopdong = fields.Char()  # hhjp_nq2
    tudieu_hai_hopdong = fields.Char()
    dendieu_hai_hopdong = fields.Char()
    tudieu_ba_hopdong = fields.Char()
    dendieu_ba_hopdong = fields.Char()
    ngaynghi_hopdong = fields.Char(string='Ngày nghỉ')
    ngaynghi_dinhki_thuhai = fields.Boolean(string='月')
    ngaynghi_dinhki_thuba = fields.Boolean(string='火')
    ngaynghi_dinhki_thutu = fields.Boolean(string='水')
    ngaynghi_dinhki_thunam = fields.Boolean(string='木')
    ngaynghi_dinhki_thusau = fields.Boolean(string='金')
    ngaynghi_dinhki_thubay = fields.Boolean(string='土')
    ngaynghi_dinhki_chunhat = fields.Boolean(string='日')
    ngaynghi_dinhki_ngayle = fields.Boolean(string='祝')
    tongso_ngaynghi_nam = fields.Integer(string="Số ngày nghỉ trong năm (ngày)")  # hhjp_0393
    # ngaynghi_dinhki_v = fields.Char(string="Ngày nghỉ định kì(Tiếng mẹ đẻ)")  #
    ngaynghi_dinhki_khac = fields.Char(string="Ngày nghỉ định kì (Khác)")  # hhjp_0154_1
    ngaynghi_dinhki_khac_viet = fields.Char()  #
    tongso_ngaynghi_nam = fields.Integer(string="Số ngày nghỉ trong năm (ngày)")  # hhjp_0393
    ngaynghi_khac = fields.Selection(string='Những ngày nghỉ không định kì',
                                     selection=[('Tuần', 'Tuần'), ('Tháng', 'Tháng')], default='Tuần')  #
    so_ngaynghi_khac = fields.Char(string='Số ngày nghỉ khác')  # hhjp_0396
    ngaynghi_khac_nhat = fields.Char(string='Ngày nghỉ định kì (Khác)')
    ngaynghi_khac_viet = fields.Char()
    tudieu_mot_ngaynghi = fields.Char(string='Nội quy lao động')  #
    dendieu_mot_ngaynghi = fields.Char()  #
    tudieu_hai_ngaynghi = fields.Char()
    dendieu_hai_ngaynghi = fields.Char()
    ngaynghiphep_coluong_lientuc_sauthang = fields.Integer(string="Nghỉ phép có lương (ngày)")  # hhjp_0394
    duoi_sauthang_nghiphep_coluong = fields.Selection(string="Nghỉ phép có lương",
                                                      selection=[('Có', 'Có'), ('Không', 'Không')],
                                                      default='Không')  # hhjp_0395
    nghiphep_coluong_ngay = fields.Char()  #
    nghiphep_coluong_thang = fields.Char()  #
    nghiphep_nghicoluong = fields.Char(string="Có lương")  # hhjp_0397
    nghiphep_nghicoluong_viet = fields.Char()  #
    nghikhac_khongluong = fields.Char(string="Không lương")  # hhjp_0398
    nghikhac_khongluong_viet = fields.Char()  #
    ghichu_luong = fields.Text(string='Ghi chú')
    tudieu_mot_luong = fields.Char(string='Nội quy lao động')  #
    dendieu_mot_luong = fields.Char()  #
    tudieu_hai_luong = fields.Char()
    dendieu_hai_luong = fields.Char()

    hinhthuc_luongthang = fields.Boolean(string='月給')

    @api.onchange('hinhthuc_luongthang')
    def onchange_method_hinhthuc_luongthang(self):
        if self.hinhthuc_luongthang == True:
            self.hinhthuc_luongngay = False
            self.hinhthuc_luonggio = False

    sotien_luongthang = fields.Integer()

    sotien_luongthang_1 = fields.Integer(related='sotien_luongthang')


    sothang_luongthang = fields.Integer()
    sogio_luongthang = fields.Integer()
    sotien_luongthang_tong = fields.Integer(compute='_sotien_luongthang_tong')

    @api.multi
    @api.depends('hinhthuc_luongthang', 'sotien_luongthang', 'sotien_luongthang_1', 'sothang_luongthang',
                 'sogio_luongthang')
    def _sotien_luongthang_tong(self):
        if self.hinhthuc_luongthang == True:
            if self.sotien_luongthang_1 and self.sothang_luongthang and self.sogio_luongthang:
                self.sotien_luongthang_tong = round((self.sotien_luongthang_1 * self.sothang_luongthang) / self.sogio_luongthang)
        else:
            self.sotien_luongthang_1 = 0
            self.sothang_luongthang = 0
            self.sogio_luongthang = 0
            self.sotien_luongthang_tong = 0
            self.sotien_luongthang = 0

    hinhthuc_luongngay = fields.Boolean(string='日給')

    @api.onchange('hinhthuc_luongngay')
    def onchange_method_hinhthuc_luongngay(self):
        if self.hinhthuc_luongngay == True:
            self.hinhthuc_luongthang = False
            self.hinhthuc_luonggio = False

    sotien_luongngay = fields.Integer(string='Lương ngày')
    sotien_luongngay_1 = fields.Integer(related='sotien_luongngay')
    sogio_luongngay = fields.Float()
    tongtien_thang_luongngay_1 = fields.Integer(compute='_tongtien_thang_luongngay_1')

    @api.multi
    @api.depends('hinhthuc_luongngay', 'sotien_luongngay', 'sotien_luongngay_1', 'sogio_luongngay')
    def _tongtien_thang_luongngay_1(self):
        if self.hinhthuc_luongngay == True:
            if self.sotien_luongngay_1 and self.sogio_luongngay:
                self.tongtien_thang_luongngay_1 = round(self.sotien_luongngay_1 / self.sogio_luongngay)
        else:
            self.sotien_luongngay = 0
            self.sotien_luongngay_1 = 0
            self.tongtien_thang_luongngay_1 = 0
            self.sogio_luongngay = 0

    sotien_luongngay_2 = fields.Integer(related='sotien_luongngay')
    songay_nam_luongngay = fields.Integer()
    sothang_nam_luongngay = fields.Integer()
    tongtien_thang_luongngay_2 = fields.Float(compute='_tongtien_thang_luongngay_2')

    @api.multi
    @api.depends('sotien_luongngay_2', 'hinhthuc_luongngay', 'songay_nam_luongngay', 'sothang_nam_luongngay')
    def _tongtien_thang_luongngay_2(self):
        if self.hinhthuc_luongngay == True:
            if self.sotien_luongngay_2 and self.songay_nam_luongngay and self.sothang_nam_luongngay:
                self.tongtien_thang_luongngay_2 = round((self.sotien_luongngay_2 * self.songay_nam_luongngay) / self.sothang_nam_luongngay)
        else:
            self.tongtien_thang_luongngay_2 = 0
            self.sotien_luongngay_2 = 0
            self.songay_nam_luongngay = 0
            self.sothang_nam_luongngay = 0

    hinhthuc_luonggio = fields.Boolean(string='時給')

    @api.onchange('hinhthuc_luonggio')
    def onchange_method_hinhthuc_luonggio(self):
        if self.hinhthuc_luonggio == True:
            self.hinhthuc_luongthang = False
            self.hinhthuc_luongngay = False

    sotien_luonggio = fields.Integer(string='Lương giờ')
    sotien_luonggio_1 = fields.Integer(related='sotien_luonggio')
    sothang_luonggio = fields.Integer()
    sogio_luonggio = fields.Integer()
    sotien_luonggio_tong = fields.Integer(compute='_sotien_luonggio_tong')

    @api.multi
    @api.depends('hinhthuc_luonggio', 'sotien_luonggio_1', 'sotien_luonggio', 'sothang_luonggio', 'sogio_luonggio')
    def _sotien_luonggio_tong(self):
        if self.hinhthuc_luonggio:
            if self.sotien_luonggio_1 and self.sothang_luonggio and self.sogio_luonggio:
                self.sotien_luonggio_tong = round((self.sotien_luonggio_1 * self.sogio_luonggio) / self.sothang_luonggio)
        else:
            self.sotien_luonggio = 0
            self.sotien_luonggio_1 = 0
            self.sothang_luonggio = 0
            self.sogio_luonggio = 0
            self.sotien_luonggio_tong = 0

    khac_phucap_daotao = fields.Char(string="Phụ cấp")  # hhjp_0425
    sotien_phuccap_daotao = fields.Integer(string="Trợ cấp đào tạo")  # hhjp_0424
    cachtinh_phucap_daotao = fields.Char(string="Cách tính")  # hhjp_0426
    sotien_phuccap_nhao = fields.Integer(string="Trợ cấp nhà ở")
    cachtinh_phucap_nhao = fields.Char()
    khac_phucap_nhao_viet = fields.Char(string="Tiếng mẹ đẻ", default='nhà ở')
    cachtinh_phucap_nhao_viet = fields.Char(string="Tiếng mẹ đẻ")
    sotien_phuccap_an = fields.Integer(string="Trợ cấp ăn")
    cachtinh_phucap_an = fields.Char()
    khac_phucap_an_viet = fields.Char(string="Tiếng mẹ đẻ", default='tiền ăn')
    cachtinh_phucap_an_viet = fields.Char(string="Tiếng mẹ đẻ")
    sotien_phuccap_dilai = fields.Integer(string="Trợ cấp đi lại")
    cachtinh_phucap_dilai = fields.Char()
    khac_phucap_dilai_viet = fields.Char(string="Tiếng mẹ đẻ", default='đi lại')
    cachtinh_phucap_dilai_viet = fields.Char(string="Tiếng mẹ đẻ")
    sotien_phuccap_thue = fields.Integer(string="Trợ cấp thuế")
    cachtinh_phucap_thue = fields.Char()
    khac_phucap_thue_viet = fields.Char(string="Tiếng mẹ đẻ", default='thuế')
    cachtinh_phucap_thue_viet = fields.Char(string="Tiếng mẹ đẻ")
    phucap_them = fields.Boolean(string='Không có phụ cấp', default=True)
    phuccap_mot = fields.Char()
    sotien_phuccap_mot = fields.Integer()
    cachtinh_phucap_mot = fields.Char()
    khac_phucap_mot_viet = fields.Char(string="Tiếng mẹ đẻ")
    cachtinh_phucap_mot_viet = fields.Char(string="Tiếng mẹ đẻ")
    phuccap_hai = fields.Char()
    sotien_phuccap_hai = fields.Integer()
    cachtinh_phucap_hai = fields.Char()
    khac_phucap_hai_viet = fields.Char(string="Tiếng mẹ đẻ")
    cachtinh_phucap_hai_viet = fields.Char(string="Tiếng mẹ đẻ")
    phuccap_ba = fields.Char()
    sotien_phuccap_ba = fields.Integer()
    cachtinh_phucap_ba = fields.Char()
    khac_phucap_ba_viet = fields.Char(string="Tiếng mẹ đẻ")
    cachtinh_phucap_ba_viet = fields.Char(string="Tiếng mẹ đẻ")
    phuccap_bon = fields.Char()
    sotien_phuccap_bon = fields.Integer()
    cachtinh_phucap_bon = fields.Char()
    khac_phucap_bon_viet = fields.Char(string="Tiếng mẹ đẻ")
    cachtinh_phucap_bon_viet = fields.Char(string="Tiếng mẹ đẻ")

    @api.onchange('phucap_them')
    def onchange_method_phucap_them(self):
        if self.phucap_them:
            self.phuccap_mot = ''
            self.khac_phucap_mot_viet = ''
            self.sotien_phuccap_mot = ''
            self.cachtinh_phucap_mot = ''
            self.cachtinh_phucap_mot_viet = ''

            self.phuccap_hai = ''
            self.khac_phucap_hai_viet = ''
            self.sotien_phuccap_hai = ''
            self.cachtinh_phucap_hai = ''
            self.cachtinh_phucap_hai_viet = ''

            self.phuccap_ba = ''
            self.khac_phucap_ba_viet = ''
            self.sotien_phuccap_ba = ''
            self.cachtinh_phucap_ba = ''
            self.cachtinh_phucap_ba_viet = ''

            self.phuccap_bon = ''
            self.khac_phucap_bon_viet = ''
            self.sotien_phuccap_bon = ''
            self.cachtinh_phucap_bon = ''
            self.cachtinh_phucap_bon_viet = ''

    khoankhac_phucap_tts = fields.Integer(string="Dự tính thanh toán mỗi tháng (1+2)",
                                          compute='_tong_khoankhac_phucap_tts')  # hhjp_0701
    ngayle_theoquydinh = fields.Integer(string="Ngày lễ nghỉ theo quy định")  # hhjp_0406
    theo_quydinh = fields.Integer(string="Theo quy định")  # hhjp_0405
    lamthem_trongvong_giokhong = fields.Boolean(string="Áp dụng phí bảo hiểm")

    @api.onchange('lamthem_trongvong_giokhong')
    def onchange_method_lamthem_trongvong_giokhong(self):
        if self.lamthem_trongvong_giokhong:
            self.lamthem_trongvong_gio = ''

    ngayle_khong_theoquydinh = fields.Integer(string="Ngày lễ nghỉ không theo quy định")  # hhjp_0407
    lamthem_trongvong_gio = fields.Integer(string="Làm thêm trong vòng 60 giờ/tháng")  # hhjp_0404
    lamthem_vuotqua_gio = fields.Integer(string="Làm thêm vượt quá 60 giờ/tháng")  # hhjp_0403
    laodong_bandem = fields.Integer(string="Lao động vào ban đêm")  # hhjp_0408
    ngaytinh_luong = fields.Selection(string='Số lần tính lương',
                                      selection=[('Mỗi tháng', 'Mỗi tháng'), ('Mỗi tuần', 'Mỗi tuần'), ],
                                      default='Mỗi tháng')

    @api.onchange('ngaytinh_luong')
    def onchange_method_ngaytinh_luong(self):
        if self.ngaytinh_luong:
            self.tinhluong_cuoithang = True
            self.ngaytinh_luong_hai = ''
            self.ngaytinh_luong_hai_v = ''

    ngaytra_luong = fields.Selection(string='Số lần trả lương',
                                     selection=[('Mỗi tháng', 'Mỗi tháng'), ('Mỗi tuần', 'Mỗi tuần'), ],
                                     default='Mỗi tháng')

    @api.onchange('ngaytra_luong')
    def onchange_method_ngaytra_luong(self):
        if self.ngaytra_luong:
            self.traluong_cuoithang = True
            self.ngaytra_luong_hai = ''
            self.ngaytra_luong_hai_v = ''

    hinhthuc_thanhtoan = fields.Selection(string="Hình thức thanh toán",
                                          selection=[('Tiền mặt', 'Tiền mặt'),
                                                     ('Chuyển khoản', 'Chuyển khoản')], default='Tiền mặt')  # hhjp_0411
    ngaytinh_luong_mot = fields.Char(string='Ngày tình lương')
    ngaytinh_luong_mot_v = fields.Char(string='Ngày tình lương')
    tinhluong_cuoithang = fields.Boolean(string='Ngày cuối tháng')

    @api.onchange('tinhluong_cuoithang')
    def onchange_method_tinhluong_cuoithang(self):
        if self.tinhluong_cuoithang:
            self.ngaytinh_luong_mot = ''
            self.ngaytinh_luong_mot_v = ''

    ngaytinh_luong_hai = fields.Char(string='Ngày tình lương')
    ngaytinh_luong_hai_v = fields.Char(string='Ngày tình lương')

    ngaytra_luong_mot = fields.Char(string='Ngày trả lương')
    ngaytra_luong_mot_v = fields.Char(string='Ngày trả lương')
    traluong_cuoithang = fields.Boolean(string='Ngày cuối tháng')

    @api.onchange('traluong_cuoithang')
    def onchange_method_traluong_cuoithang(self):
        if self.traluong_cuoithang:
            self.ngaytra_luong_mot = ''
            self.ngaytra_luong_mot_v = ''

    ngaytra_luong_hai = fields.Char(string='Ngày trả lương')
    ngaytra_luong_hai_v = fields.Char(string='Ngày trả lương')

    khautru_luong = fields.Selection(string='⑦労使協定に基づく賃金支払時の控除',
                                     selection=[('Có', 'Có'), ('Không', 'Không')], default='Không')

    @api.onchange('khautru_luong')
    def onchange_method_khautru_luong(self):
        if self.khautru_luong:
            self.chiphi_khoanmuc_7 = False
            self.sotien_khoanmuc_7 = ''
            self.ghichu_khoanmuc_7 = ''

            self.chiphi_khoanmuc_8 = False
            self.sotien_khoanmuc_8 = ''
            self.ghichu_khoanmuc_8 = ''

            self.noidung_khoanmuc_viet_9 = ''
            self.chiphi_khoanmuc_9 = False
            self.sotien_khoanmuc_9 = ''
            self.ghichu_khoanmuc_9 = ''

            self.noidung_khoanmuc_10 = ''
            self.noidung_khoanmuc_viet_10 = ''
            self.chiphi_khoanmuc_10 = False
            self.sotien_khoanmuc_10 = ''
            self.ghichu_khoanmuc_10 = ''

            self.noidung_khoanmuc_11 = ''
            self.noidung_khoanmuc_viet_11 = ''
            self.chiphi_khoanmuc_11 = False
            self.sotien_khoanmuc_11 = ''
            self.ghichu_khoanmuc_11 = ''

            self.noidung_khoanmuc_12 = ''
            self.noidung_khoanmuc_viet_12 = ''
            self.chiphi_khoanmuc_12 = False
            self.sotien_khoanmuc_12 = ''
            self.ghichu_khoanmuc_12 = ''

            self.noidung_khoanmuc_13 = ''
            self.noidung_khoanmuc_viet_13 = ''
            self.chiphi_khoanmuc_13 = False
            self.sotien_khoanmuc_13 = ''
            self.ghichu_khoanmuc_13 = ''

    # noidung_khoanmuc_1 = fields.Char(string='Tiền thuế')
    # chiphi_khoanmuc_1 = fields.Boolean(string='Chi phí thực tế')
    sotien_khoanmuc_1 = fields.Integer(string='所得税')
    ghichu_khoanmuc_1 = fields.Char()

    sotien_khoanmuc_2 = fields.Integer(string='住民税')
    ghichu_khoanmuc_2 = fields.Char()

    sotien_khoanmuc_3 = fields.Integer(string='健康保険')
    ghichu_khoanmuc_3 = fields.Char()

    sotien_khoanmuc_4 = fields.Integer(string='介護保険')
    ghichu_khoanmuc_4 = fields.Char()

    sotien_khoanmuc_5 = fields.Integer(string='厚生年金保険')
    ghichu_khoanmuc_5 = fields.Char()

    sotien_khoanmuc_6 = fields.Integer(string='労働保険')
    ghichu_khoanmuc_6 = fields.Char()

    sotien_khoanmuc_7 = fields.Integer(string='食費')
    chiphi_khoanmuc_7 = fields.Boolean(string='実費')
    ghichu_khoanmuc_7 = fields.Char()

    sotien_khoanmuc_8 = fields.Integer(string='居住費')
    chiphi_khoanmuc_8 = fields.Boolean(string='実費')
    ghichu_khoanmuc_8 = fields.Char()

    noidung_khoanmuc_9 = fields.Char(default=u'水道光熱費')
    noidung_khoanmuc_viet_9 = fields.Char(string='母国語')
    sotien_khoanmuc_9 = fields.Integer(string='')
    chiphi_khoanmuc_9 = fields.Boolean(string='実費')
    ghichu_khoanmuc_9 = fields.Char()

    noidung_khoanmuc_10 = fields.Char()
    noidung_khoanmuc_viet_10 = fields.Char(string='母国語')
    sotien_khoanmuc_10 = fields.Integer(string='')
    chiphi_khoanmuc_10 = fields.Boolean(string='実費')
    ghichu_khoanmuc_10 = fields.Char()

    noidung_khoanmuc_11 = fields.Char()
    noidung_khoanmuc_viet_11 = fields.Char(string='母国語')
    sotien_khoanmuc_11 = fields.Integer(string='')
    chiphi_khoanmuc_11 = fields.Boolean(string='実費')
    ghichu_khoanmuc_11 = fields.Char()

    noidung_khoanmuc_12 = fields.Char()
    noidung_khoanmuc_viet_12 = fields.Char(string='母国語')
    sotien_khoanmuc_12 = fields.Integer(string='')
    chiphi_khoanmuc_12 = fields.Boolean(string='実費')
    ghichu_khoanmuc_12 = fields.Char()

    noidung_khoanmuc_13 = fields.Char()
    noidung_khoanmuc_viet_13 = fields.Char(string='母国語')
    sotien_khoanmuc_13 = fields.Integer(string='')
    chiphi_khoanmuc_13 = fields.Boolean(string='実費')
    ghichu_khoanmuc_13 = fields.Char()
    tong_sotien_khoanmuc = fields.Integer(compute='_tong_sotien_khoanmuc')

    @api.multi
    @api.depends('sotien_luongthang', 'tongtien_thang_luongngay_2', 'sotien_luonggio_tong', 'sotien_phuccap_daotao',
                 'sotien_phuccap_nhao', 'sotien_phuccap_an', 'sotien_phuccap_dilai', 'sotien_phuccap_thue',
                 'sotien_phuccap_mot', 'sotien_phuccap_hai', 'sotien_phuccap_ba', 'sotien_phuccap_bon')
    def _tong_khoankhac_phucap_tts(self):
        self.khoankhac_phucap_tts = int(
            self.sotien_luongthang + self.tongtien_thang_luongngay_2 + self.sotien_luonggio_tong + self.sotien_phuccap_daotao + self.sotien_phuccap_nhao + self.sotien_phuccap_an + self.sotien_phuccap_dilai + self.sotien_phuccap_thue + self.sotien_phuccap_mot + self.sotien_phuccap_hai + self.sotien_phuccap_ba + self.sotien_phuccap_bon)

    @api.multi
    @api.depends('sotien_khoanmuc_1', 'sotien_khoanmuc_2', 'sotien_khoanmuc_3', 'sotien_khoanmuc_4',
                 'sotien_khoanmuc_5', 'sotien_khoanmuc_6', 'sotien_khoanmuc_7', 'sotien_khoanmuc_8',
                 'sotien_khoanmuc_9', 'sotien_khoanmuc_10', 'sotien_khoanmuc_11', 'sotien_khoanmuc_12',
                 'sotien_khoanmuc_13')
    def _tong_sotien_khoanmuc(self):
        self.tong_sotien_khoanmuc = int(
            self.sotien_khoanmuc_1 + self.sotien_khoanmuc_2 + self.sotien_khoanmuc_3 + self.sotien_khoanmuc_4 + self.sotien_khoanmuc_5 + self.sotien_khoanmuc_6 + self.sotien_khoanmuc_7 + self.sotien_khoanmuc_8 + self.sotien_khoanmuc_9 + self.sotien_khoanmuc_10 + self.sotien_khoanmuc_11 + self.sotien_khoanmuc_12 + self.sotien_khoanmuc_13)

    thanhtoan_tienmat = fields.Integer(compute='_thanhtoan_tienmat')

    @api.multi
    @api.depends('khoankhac_phucap_tts', 'tong_sotien_khoanmuc')
    def _thanhtoan_tienmat(self):
        self.thanhtoan_tienmat = float(self.khoankhac_phucap_tts) - float(self.tong_sotien_khoanmuc)

    tangluong = fields.Selection(string="Tăng lương", selection=[('Có', 'Có'), ('Không', 'Không')],
                                 default='Không')  # hhjp_0413
    thoidiem_tangluong = fields.Char(string="Thời điểm")  #
    thoidiem_tangluong_viet = fields.Char(string="Tiếng mẹ đẻ")  # tiếng việt

    @api.onchange('tangluong')
    def onchange_method_tangluong(self):
        if self.tangluong:
            self.thoidiem_tangluong = ''
            self.thoidiem_tangluong_viet = ''

    thuong = fields.Selection(string="Thưởng", selection=[('Có', 'Có'), ('Không', 'Không')],
                              default='Không')  # hhjp_0414
    thoidiem_thuong = fields.Char(string="Thời điểm")  #
    thoidiem_thuong_viet = fields.Char(string="Tiếng mẹ đẻ")  #

    @api.onchange('thuong')
    def onchange_method_thuong(self):
        if self.thuong:
            self.thoidiem_thuong = ''
            self.thoidiem_thuong_viet = ''

    trocap_thoiviec = fields.Selection(string="Trợ cấp thôi việc", selection=[('Có', 'Có'), ('Không', 'Không')],
                                       default='Không')  # hhjp_0415
    thoidiem_trocap_thoiviec = fields.Char(string="Thời điểm")  #
    thoidiem_trocap_thoiviec_viet = fields.Char(string="Tiếng mẹ đẻ")  # việt

    @api.onchange('trocap_thoiviec')
    def onchange_method_trocap_thoiviec(self):
        if self.trocap_thoiviec:
            self.thoidiem_trocap_thoiviec = ''
            self.thoidiem_trocap_thoiviec_viet = ''

    phucap_ngung_kinhdoanh = fields.Selection(string="Phụ cấp ngừng kinh doanh",
                                              selection=[('Có', 'Có'), ('Không', 'Không')],
                                              default='Không')  # hhjp_0416
    tyle_ngung_kinhdoanh = fields.Char(string="Tỷ lệ")  #

    @api.onchange('phucap_ngung_kinhdoanh')
    def onchange_method_phucap_ngung_kinhdoanh(self):
        if self.phucap_ngung_kinhdoanh:
            self.tyle_ngung_kinhdoanh = ''

    huutri_baohiem = fields.Boolean(string='Nhân viên hưu trí')
    huwuquocgia_baohiem = fields.Boolean(string='Lương hưu quốc gia')
    yte_baohiem = fields.Boolean(string='Bảo hiểm y tế')
    yte_quocgia_baohiem = fields.Boolean(string='Bảo hiểm y tế quốc gia')
    vieclam_baohiem = fields.Boolean(string='Bảo hiểm việc làm')
    congnghiep_baohiem = fields.Boolean(string='Bảo hiểm tai nạn công nghiệp')
    khac_baohiem = fields.Boolean(string='Khác')
    noidung_khac_baohiem = fields.Char(string='Khác')
    noidung_khacviet_baohiem = fields.Char(string='Tiếng mẹ đẻ')

    @api.onchange('khac_baohiem')
    def onchange_method_khac_baohiem(self):
        if self.khac_baohiem:
            self.noidung_khac_baohiem = ''
            self.noidung_khacviet_baohiem = ''

    thang_suckhoe_congty = fields.Selection([('01', '01'), ('02', '02'), ('03', '03'), ('04', '04'),
                                             ('05', '05'), ('06', '06'), ('07', '07'), ('08', '08'),
                                             ('09', '09'), ('10', '10'), ('11', '11'), ('12', '12'), ],
                                            "月")  # hh_151
    nam_suckhoe_congty = fields.Char(string="年")  # hh_152
    thang_suckhoe_dinhky_landau = fields.Selection([('01', '01'), ('02', '02'), ('03', '03'), ('04', '04'),
                                                    ('05', '05'), ('06', '06'), ('07', '07'), ('08', '08'),
                                                    ('09', '09'), ('10', '10'), ('11', '11'), ('12', '12'), ],
                                                   "月")  # hh_151
    nam_suckhoe_dinhky_landau = fields.Char(string="年")  # hh_152
    tanso_khamlai = fields.Char(string="Tần số khám lại (/Lần)")  # hhjp_0421
    tanso_khamlai_thangtuan = fields.Selection(selection=[('Năm', 'Năm'), ('Tuần', 'Tuần')], default='Năm')
    diachi_cutru = fields.Char(string="Địa chỉ")
    ghichu_suckhoe = fields.Text(string="Ghi chú")
    noptruoc_thoiviec = fields.Integer(string='Nộp trước khi thôi việc (Ngày)')
    tudieu_mot_thoiviec = fields.Char(string='Nội quy lao động')  #
    dendieu_mot_thoiviec = fields.Char()  #
    tudieu_hai_thoiviec = fields.Char()
    dendieu_hai_thoiviec = fields.Char()
