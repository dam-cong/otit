# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime


class DaoTaoTruocDONHANG(models.Model):
    _name = 'daotao.daotaotruoc'
    _rec_name = 'donhang_daotaotruoc'
    _order = 'id desc'

    coso_daotaotruoc = fields.Many2many(comodel_name='csdt.csdt', string='Cơ sở đào tạo')
    giangvien_daotaotruoc = fields.One2many(comodel_name='giaovien', inverse_name='ten_daotaotruoc',
                                            string='Danh sách giáo viên')

    donhang_daotaotruoc = fields.Many2one(comodel_name='donhang.donhang', string='Đơn hàng',ondelete='cascade')
    congtyphaicu_daotaotruoc = fields.Many2one(comodel_name='congtyphaicu.congtyphaicu',
                                               related="donhang_daotaotruoc.congtyphaicu_donhang",
                                               string='Công ty phái cử')
    xinghiep_daotaotruoc = fields.Many2one(comodel_name='xinghiep.xinghiep',
                                           related="donhang_daotaotruoc.xinghiep_donhang", string='Xí nghiệp')
    congviec_daotaotruoc = fields.Many2one(comodel_name='congviec.congviec',
                                           related="donhang_daotaotruoc.congviec_donhang", string='Công việc')
    macongviec_daotaotruoc = fields.Char(related="congviec_daotaotruoc.ma_congviec", string='Mã công việc')
    nguoi_lamdon = fields.Date('Ngày lập kế hoạch')
    ngay_lamdon = fields.Date('Ngày lập báo cáo')

    # daotao_phaicu_daotaotruoc = fields.Many2many(comodel_name='daotao.daotaophaicu', string="Đào tạo phái cử")
    noidung_1 = fields.Text(string="Nội dung", default=u"日本語（会話、読み書き、文法）")
    tencoquan_diachi_1 = fields.Many2one(comodel_name="nghiepdoan.nghiepdoan", string="Tên cơ quan - Địa chỉ")
    diachi_coquan_1 = fields.Char(string="Địa chỉ", related="tencoquan_diachi_1.diachi_nghiepdoan")
    tencoso_diachi_1 = fields.Many2one(comodel_name="csdt.csdt", string="Tên cơ sở - Địa chỉ")
    diachi_coso_1 = fields.Char(string="Địa chỉ", related="tencoso_diachi_1.diachi_coso")
    khoangthoigian_batdau_1 = fields.Date(string="Khoảng thời gian")
    khoangthoigian_ketthuc_1 = fields.Date(string="Kết thúc")
    sogio_1 = fields.Integer(string="Số giờ")
    uythac_1 = fields.Selection(string="Ủy thác ngoài", selection=[('Có', 'Có'), ('Không', 'Không'), ],
                                required=False, default='Có')
    # ----------------------------------------------------
    noidung_2 = fields.Text(string="Nội dung", default=u"本邦での生活一般に関する知識（日本の歴史、文化、生活様式、職場ルール）")
    tencoquan_diachi_2 = fields.Many2one(comodel_name="nghiepdoan.nghiepdoan", string="Tên cơ quan - Địa chỉ",
                                         related="tencoquan_diachi_1")
    diachi_coquan_2 = fields.Char(string="Địa chỉ", related="tencoquan_diachi_2.diachi_nghiepdoan")
    tencoso_diachi_2 = fields.Many2one(comodel_name="csdt.csdt", string="Tên cơ sở - Địa chỉ",
                                       related="tencoso_diachi_1")
    diachi_coso_2 = fields.Char(string="Địa chỉ", related="tencoso_diachi_2.diachi_coso")
    khoangthoigian_batdau_2 = fields.Date(string="Khoảng thời gian")
    khoangthoigian_ketthuc_2 = fields.Date(string="Kết thúc")
    sogio_2 = fields.Integer(string="Số giờ")
    uythac_2 = fields.Selection(string="Ủy thác ngoài", selection=[('Có', 'Có'), ('Không', 'Không'), ],
                                required=False, default='Có')
    # ------------------------------------------------------
    noidung_3 = fields.Text(string="Nội dung", default=u"本邦での円滑な技能等の修得等に資する知識（修得技能の目標、内容、職場規律、心構え）")
    tencoquan_diachi_3 = fields.Many2one(comodel_name="nghiepdoan.nghiepdoan", string="Tên cơ quan - Địa chỉ",
                                         related="tencoquan_diachi_1")
    diachi_coquan_3 = fields.Char(string="Địa chỉ", related="tencoquan_diachi_3.diachi_nghiepdoan")
    tencoso_diachi_3 = fields.Many2one(comodel_name="csdt.csdt", string="Tên cơ sở - Địa chỉ",
                                       related="tencoso_diachi_1")
    diachi_coso_3 = fields.Char(string="Địa chỉ", related="tencoso_diachi_3.diachi_coso")
    khoangthoigian_batdau_3 = fields.Date(string="Khoảng thời gian")
    khoangthoigian_ketthuc_3 = fields.Date(string="Kết thúc")
    sogio_3 = fields.Integer(string="Số giờ")
    uythac_3 = fields.Selection(string="Ủy thác ngoài", selection=[('Có', 'Có'), ('Không', 'Không'), ],
                                required=False, default='Có')

    tongsogio = fields.Integer(string="Tổng số giờ", compute="_tong_sogio")

    @api.multi
    @api.depends('sogio_1', 'sogio_2', 'sogio_3')
    def _tong_sogio(self):
        if self.sogio_1 or self.sogio_2 or self.sogio_3:
            self.tongsogio = self.sogio_1 + self.sogio_2 + self.sogio_3

    @api.onchange('khoangthoigian_batdau_1', 'khoangthoigian_ketthuc_1')
    def onchange_method_khoangthoigian_batdau_1(self):
        if self.khoangthoigian_batdau_1:
            self.khoangthoigian_batdau_2 = self.khoangthoigian_batdau_1
            self.khoangthoigian_batdau_3 = self.khoangthoigian_batdau_1
            self.thoigian_batdau_daotaotruoc = self.khoangthoigian_batdau_1
        if self.khoangthoigian_ketthuc_1:
            self.khoangthoigian_ketthuc_2 = self.khoangthoigian_ketthuc_1
            self.khoangthoigian_ketthuc_3 = self.khoangthoigian_ketthuc_1
            self.thoigian_ketthuc_daotaotruoc = self.khoangthoigian_ketthuc_1

    # coquan_nuocngoai_daotaotruoc = fields.Many2many(comodel_name='daotao.chuanbingoai',
    #                                                 string="Cơ quan chuẩn bị nước ngoài")
    noidung_ngoai_1 = fields.Text(string="Nội dung")
    ten_diachi_ngoai_1 = fields.Many2one(comodel_name="nghiepdoan.nghiepdoan", string="Tên cơ quan - Địa chỉ")
    diachi_coquan_ngoai_1 = fields.Char(string="Địa chỉ", related="ten_diachi_ngoai_1.diachi_nghiepdoan")
    tencoso_diachi_ngoai_1 = fields.Many2one(comodel_name="coquan.coquan", string="Tên cơ sở - Địa chỉ",
                                             related="donhang_daotaotruoc.coquan_chuanbi_ngoai")
    diachi_coso_ngoai_1 = fields.Char(string="Địa chỉ", related="tencoso_diachi_ngoai_1.diachi_coquan")
    khoangthoigian_batdau_ngoai_1 = fields.Date(string="Khoảng thời gian")
    khoangthoigian_ketthuc_ngoai_1 = fields.Date(string="Kết thúc")
    sogio_ngoai_1 = fields.Integer(string="Số giờ")
    loai_tochuc_1 = fields.Selection(string="Loại tổ chức ngoài", selection=[('Cơ quan công cộng', 'Cơ quan công cộng'),
                                                                             ('Cơ sở giáo dục', 'Cơ sở giáo dục'),
                                                                             ('Tổ chức tư nhân nước ngoài',
                                                                              'Tổ chức tư nhân nước ngoài')],
                                     required=False, )
    # -------------------------------------------------------
    noidung_ngoai_2 = fields.Text(string="Nội dung")
    ten_diachi_ngoai_2 = fields.Many2one(comodel_name="nghiepdoan.nghiepdoan", string="Tên cơ quan - Địa chỉ")
    diachi_coquan_ngoai_2 = fields.Char(string="Địa chỉ", related="ten_diachi_ngoai_2.diachi_nghiepdoan")
    tencoso_diachi_ngoai_2 = fields.Many2one(comodel_name="coquan.coquan", string="Tên cơ sở - Địa chỉ",
                                             related="donhang_daotaotruoc.coquan_chuanbi_ngoai")
    diachi_coso_ngoai_2 = fields.Char(string="Địa chỉ", related="tencoso_diachi_ngoai_2.diachi_coquan")
    khoangthoigian_batdau_ngoai_2 = fields.Date(string="Khoảng thời gian")
    khoangthoigian_ketthuc_ngoai_2 = fields.Date(string="Kết thúc")
    sogio_ngoai_2 = fields.Integer(string="Số giờ")
    loai_tochuc_2 = fields.Selection(string="Loại tổ chức ngoài", selection=[('Cơ quan công cộng', 'Cơ quan công cộng'),
                                                                             ('Cơ sở giáo dục', 'Cơ sở giáo dục'),
                                                                             ('Tổ chức tư nhân nước ngoài',
                                                                              'Tổ chức tư nhân nước ngoài')],
                                     required=False, )
    # ----------------------------------------------------------
    noidung_ngoai_3 = fields.Text(string="Nội dung")
    ten_diachi_ngoai_3 = fields.Many2one(comodel_name="nghiepdoan.nghiepdoan", string="Tên cơ quan - Địa chỉ")
    diachi_coquan_ngoai_3 = fields.Char(string="Địa chỉ", related="ten_diachi_ngoai_3.diachi_nghiepdoan")
    tencoso_diachi_ngoai_3 = fields.Many2one(comodel_name="coquan.coquan", string="Tên cơ sở - Địa chỉ",
                                             related="donhang_daotaotruoc.coquan_chuanbi_ngoai")
    diachi_coso_ngoai_3 = fields.Char(string="Địa chỉ", related="tencoso_diachi_ngoai_3.diachi_coquan")
    khoangthoigian_batdau_ngoai_3 = fields.Date(string="Khoảng thời gian")
    khoangthoigian_ketthuc_ngoai_3 = fields.Date(string="Kết thúc")
    sogio_ngoai_3 = fields.Integer(string="Số giờ")
    loai_tochuc_3 = fields.Selection(string="Loại tổ chức ngoài", selection=[('Cơ quan công cộng', 'Cơ quan công cộng'),
                                                                             ('Cơ sở giáo dục', 'Cơ sở giáo dục'),
                                                                             ('Tổ chức tư nhân nước ngoài',
                                                                              'Tổ chức tư nhân nước ngoài')],
                                     required=False, )
    tongsogio_ngoai = fields.Integer(string="Tổng số giờ", compute="_tong_sogio_ngoai")

    @api.multi
    @api.depends('sogio_ngoai_1', 'sogio_ngoai_2', 'sogio_ngoai_3')
    def _tong_sogio_ngoai(self):
        if self.sogio_ngoai_1 or self.sogio_ngoai_2 or self.sogio_ngoai_3:
            self.tongsogio_ngoai = self.sogio_ngoai_1 + self.sogio_ngoai_2 + self.sogio_ngoai_3

    # coquan_nuocngoai_daotaotruoc = fields.Many2many(comodel_name='daotao.chuanbingoai',
    #                                                 string="Cơ quan chuẩn bị nước ngoài")
    # Khung giờ học

    nguoigiamsat_daotaotruoc = fields.Char(string='Người giám sát')
    thu_hai = fields.Boolean(string='月')
    thu_ba = fields.Boolean(string='火')
    thu_bon = fields.Boolean(string='水')
    thu_nam = fields.Boolean(string='木')
    thu_sau = fields.Boolean(string='金')
    thu_bay = fields.Boolean(string='土')
    chu_nhat = fields.Boolean(string='日')
    ngay_le = fields.Boolean(string='祝')

    laplich_daotaotruoc = fields.One2many(comodel_name='laplich.laplichtruoc', inverse_name='daotaotruoc',
                                          string='Lập lịch')

    giangvien_daotaotruoc_1 = fields.One2many(comodel_name='daotaotruoc.giaovien', inverse_name='daotao_daotaotruoc')

    @api.multi
    def laydulieu_button_1(self):

        if self.giangvien_daotaotruoc:
            vals_giangvien = []
            for giangvien in self.giangvien_daotaotruoc:
                vals_row = {}
                vals_row['ten_giaovien'] = giangvien.ten_giaovien
                vals_row['ten_cosodaotao'] = giangvien.ten_cosodaotao.id
                vals_row['chucvu'] = giangvien.chucvu
                giangvien_truoc = self.env['daotaotruoc.giaovien'].create(vals_row)
                vals_giangvien.append(giangvien_truoc.id)
            self.giangvien_daotaotruoc_1 = [(6, 0, vals_giangvien)]

    @api.multi
    def laydulieu_button_daotaotruoc(self):
        if self.thoigian_batdau_daotaotruoc and self.thoigian_ketthuc_daotaotruoc:

            start = datetime.datetime.strptime(str(self.thoigian_batdau_daotaotruoc), "%Y-%m-%d")
            end = datetime.datetime.strptime(str(self.thoigian_ketthuc_daotaotruoc), "%Y-%m-%d")
            the_end = end + datetime.timedelta(days=1)

            date_array = (start + datetime.timedelta(days=x) for x in range(0, (the_end - start).days))

            print(date_array)

            ngay_nghi = []
            if self.thu_hai == True:
                ngay_nghi.append(0)
            if self.thu_ba == True:
                ngay_nghi.append(1)
            if self.thu_bon == True:
                ngay_nghi.append(2)
            if self.thu_nam == True:
                ngay_nghi.append(3)
            if self.thu_sau == True:
                ngay_nghi.append(4)
            if self.thu_bay == True:
                ngay_nghi.append(5)
            if self.chu_nhat == True:
                ngay_nghi.append(6)

            noidungdaotaotruoc = self.env['daotaotruoc.noidungdaotao'].search([('noidungdaotao', '=', '休日')], limit=1)

            vals_lich = []
            for date_object in date_array:
                vals_lich_row = {}

                vals_lich_row['ngay_laplichtruoc'] = date_object
                vals_lich_row['coso_laplichtruoc'] = self.tencoso_diachi_1.id

                if date_object.weekday() in ngay_nghi:
                    vals_lich_row['noidung_laplichtruoc'] = noidungdaotaotruoc.id
                    vals_lich_row['ngaynghi_truoc'] = True
                    vals_lich_row['coso_laplichtruoc'] = ''

                laplichtruoc = self.env['laplich.laplichtruoc'].create(vals_lich_row)
                vals_lich.append(laplichtruoc.id)

            self.laplich_daotaotruoc = [(6, 0, vals_lich)]


class GV_DTTruoc(models.Model):
    _name = 'daotaotruoc.giaovien'
    _rec_name = 'ten_giaovien'
    _order = 'id desc'

    ten_giaovien = fields.Char(string='Họ và tên')
    daotao_daotaotruoc = fields.Many2one(comodel_name='daotao.daotaotruoc', string='Đào tạo trước', )
    ten_cosodaotao = fields.Many2one(comodel_name='csdt.csdt', string='Cơ quan làm việc')
    chucvu = fields.Char(string="Chức vụ")
