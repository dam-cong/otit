# -*- coding: utf-8 -*-

from odoo import models, fields, api


class hocvan(models.Model):
    _name = 'thuctapsinh.hocvan'
    _rec_name = 'truong_tts'

    hocvan_tts = fields.Many2one(comodel_name="thuctapsinh.thuctapsinh", string="Thực tập sinh")  #
    id = fields.Integer(string="STT")  #

    batdau_hoctap_tts = fields.Char(string="Từ(Tiếng Nhật)")  # hhjp_0342
    batdau_hoctap_tts_v = fields.Char(string="Từ(Tiếng Việt)")  # hhjp_0342_v
    ketthuc_hoctap_tts = fields.Char(string="Kết thúc(Tiếng Nhật)")  # hhjp_0343
    ketthuc_hoctap_tts_v = fields.Char(string="Kết thúc(Tiếng Việt)")  # hhjp_0343
    truong_tts = fields.Char(string="Trường(Tiếng Nhật)")  # hhjp_0344
    truong_tts_v = fields.Char(string="Trường(Tiếng Việt)")  # hhjp_0344

class hocvan_viet(models.Model):
    _name = 'thuctapsinh.hocvanviet'
    _rec_name = 'truong_tts_v'

    hocvan_tts_v = fields.Many2one(comodel_name="thuctapsinh.thuctapsinh", string="Thực tập sinh")  #
    id = fields.Integer(string="STT")  #

    batdau_hoctap_tts_v = fields.Char(string="Từ")  # hhjp_0342
    ketthuc_hoctap_tts_v = fields.Char(string="Kết thúc")  # hhjp_0343
    truong_tts_v = fields.Char(string="Trường")  # hhjp_0344

class congtac(models.Model):
    _name = 'thuctapsinh.congtac'
    _rec_name = 'congtac_tts'

    congtac_tts = fields.Many2one(comodel_name="thuctapsinh.thuctapsinh", string="Công tác")  #
    batdau_congtac_tts = fields.Char(string="Từ(Tiếng Nhật)")  # hhjp_0347
    batdau_congtac_tts_v = fields.Char(string="Từ(Tiếng Việt)")  # hhjp_0347_v
    ketthuc_congtac_tts = fields.Char(string="Kết thúc(Tiếng Nhật)")  # hhjp_0348
    ketthuc_congtac_tts_v = fields.Char(string="Kết thúc(Tiếng việt)")  # hhjp_0348_v
    donvi_congtac_tts = fields.Char(string="Công ty(Tiếng Nhật)")  # hhjp_0349
    donvi_congtac_tts_v = fields.Char(string="Công ty(Tiếng Việt)")  # hhjp_0349_v
    congviec_congtac_tts = fields.Char(string="Công việc(Tiếng Nhật)")  # hhjp_0350
    congviec_congtac_tts_v = fields.Char(string="Công việc(Tiếng Việt)")  # hhjp_0350_v



class loainghe(models.Model):
    _name = 'thuctapsinh.loainghe'

    sonam_tts = fields.Many2one(comodel_name="thuctapsinh.thuctapsinh", string="Loại nghề")  #
    loainghe_tts = fields.Char(string="Loại nghề(Tiếng Nhật)")  # hhjp_0351
    loainghe_tts_v = fields.Char(string="Loại nghề(Tiếng Việt)")  # hhjp_0351_v
    kinhnghiem_tts = fields.Char(string="Thời gian(Tiếng Nhật)")  # hhjp_0352
    kinhnghiem_tts_v = fields.Char(string="Thời gian(Tiếng Việt)")  # hhjp_0352_v

class chinhanhviet(models.Model):
    _name = 'chinhanh.viet'

    tenhan_chinhanh_viet = fields.Char(string="Tên")
    chinhanh_chinhanh_viet = fields.Char(string="Địa chỉ")
    chinhanh_viet = fields.Many2one(comodel_name='thuctapsinh.thuctapsinh')

class phucap(models.Model):
    _name = 'thuctapsinh.phucap'
    _rec_name = 'khac_phucap'

    tts_phucap = fields.Many2one(comodel_name='thuctapsinh.thuctapsinh')
    khac_phucap = fields.Char(string="Nội dung")  # hhjp_0425
    sotien_phuccap = fields.Integer(string="Số tiền (Yên)")  # hhjp_0424
    cachtinh_phucap = fields.Char(string="Cách tính")  # hhjp_0426

    khac_phucap_viet = fields.Char(string="Nội dung(Việt)")  # hhjp_0425
    sotien_phuccap_viet = fields.Integer(string="Số tiền (Việt)")  # hhjp_0424
    cachtinh_phucap_viet = fields.Char(string="Cách tính(Việt)")  # hhjp_0426
