# -*- coding: utf-8 -*-

from odoo import models, fields, api


class GiangVien(models.Model):
    _name = 'giangvien.giangvien'
    _rec_name = 'ten_giangvien_phaply'
    _order = 'id desc'

    giangvien_phapluat = fields.Many2one(comodel_name="daotao.daotaosau", string="Cơ sở đào tạo")
    ten_giangvien_phaply = fields.Char(string="Tên")  # hhjp_0320
    nghenghiep_giangvien_phaply = fields.Char(string="Nghề nghiệp")  # hhjp_0321
    coquan_giangvien_phaply = fields.Char(string="Cơ quan làm việc")  # hhjp_0322
    kinhnghiem_giangvien_phaply = fields.Char(string="Kinh nghiệm")  # hhjp_0323
    bangcap_giangvien_phaply = fields.Char(string="Bằng cấp")  # hhjp_0324
