# -*- coding: utf-8 -*-
{
    'name': "Hồ sơ",
    'depends': ['base', 'donhang', 'xinghiep','congviec'],
    'data': [
        # 'security/group_user.xml',
        'security/ir.model.access.csv',
        'views/hoso.xml',
        # 'views/kieu.xml',
        'views/luutru.xml',
        # 'views/tucach.xml',
        # 'views/hosodonhang.xml',
        # 'views/hosoCTPC.xml',
        # 'views/hosocongtyphaicu.xml',
        # 'views/daotaotruoc.xml',
    ],
}
