# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception, content_disposition
from io import BytesIO, StringIO
import os
import zipfile
import logging

_logger = logging.getLogger(__name__)

class Hoso(http.Controller):
    @http.route('/hoso/<string:id>', type='http', auth="public")
    def download(self, id=False, **kwargs):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        document = request.env['hoso.hoso'].browse(int(id))
        invoice = request.env['hoso.hoso']
        infor = document.thuctapsinh_hoso

        # Go 1
        if document.giaidoan_hoso == '1 go':
            item = 1
            for thuctapsinh in infor:
                # Thông tin file 1
                if document.file_1:
                    finalDoc = invoice.baocao_hoso_1(document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\第１号　第１面　技能実習計画　認定申請書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 2
                if document.file_2:
                    finalDoc = invoice.baocao_hoso_2(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\第１号　第２面　技能実習計画.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 3
                if document.file_3:
                    finalDoc = invoice.baocao_hoso_3(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\第１号　第３面　入国後講習実施予定表.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 4
                if document.file_4:
                    finalDoc = invoice.baocao_hoso_4(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\第１号　第４面　実習実施予定表.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 7
                if document.file_7:
                    finalDoc = invoice.baocao_hoso_5()
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\第１号　第７面　誓約事項.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 8
                if item == 1:
                    if document.file_8:
                        finalDoc = invoice.baocao_hoso_7(thuctapsinh.ma_tts, document)
                        demo = str('共通')
                        archive.write(finalDoc.name, r'%s\参考１ー１号、第１-25号 申請者の概要書.docx' % demo)
                        os.unlink(finalDoc.name)
                else:
                    pass

                # Thông tin file 9
                if item == 1:
                    if document.file_9:
                        finalDoc = invoice.baocao_hoso_8(thuctapsinh.ma_tts, document)
                        demo = str('共通')
                        archive.write(finalDoc.name, r'%s\参考１ー２号　申請者の誓約書.docx' % demo)
                        os.unlink(finalDoc.name)
                else:
                    pass

                # Thông tin file 10
                if document.file_10:
                    finalDoc = invoice.baocao_hoso_9(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１ー３号　技能実習生の履歴書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 11
                if document.file_11:
                    finalDoc = invoice.baocao_hoso_10(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１ー４号、第１ー６ 号 、第１ー８ 号　技能実習責任者・技能実習指導員・生活指導員 の履歴書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 12
                    if document.file_12:
                        finalDoc = invoice.baocao_hoso_11(thuctapsinh.ma_tts, document)
                        demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                        archive.write(finalDoc.name,
                                      r'%s\参考１ー５号、 第１ー７号、 第１－９号 技能実習責任者・技能実習指導員・生活指導員の就任承諾書及び誓約書.docx' % demo)
                        os.unlink(finalDoc.name)

                # Thông tin file 13
                if document.file_13:
                    finalDoc = invoice.baocao_hoso_16(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name,
                                  r'%s\参考1-16号、 第1-17号、 第1-18号 技能実習生の報酬・宿泊施設・徴収費用についての説明書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 14
                if document.file_14:
                    finalDoc = invoice.baocao_hoso_18(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１- 14号 技能実習のための雇用契約書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 15
                if document.file_15:
                    finalDoc = invoice.baocao_hoso_19(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１-19号(Ｄ) 技能実習の期間中の待遇に関する重要事項説明書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 17
                if document.file_17:
                    finalDoc = invoice.baocao_hoso_22(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１ー２２号　技能実習を行わせる理由書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 18
                if document.file_18:
                    finalDoc = invoice.baocao_hoso_32(document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１ー24号 優良要件適合申告書（実習実施者）.docx' % demo)
                    os.unlink(finalDoc.name)

                    finalDoc = invoice.baocao_hoso_32_2(document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考様式第１－24号 別紙受検・不受検　技能実習生名簿.docx' % (demo))
                    os.unlink(finalDoc.name)

                # Thông tin file 19
                if document.file_19:
                    finalDoc = invoice.baocao_hoso_25(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１ー２７号　同種業務従事経験等証明書-団体監理型技能実習.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 20
                if document.file_20:
                    finalDoc = invoice.baocao_hoso_28(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１ー３０号　複数の職種及び作業に係る技能実習を行わせる理由書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 21
                if item == 1:
                    if document.file_21:
                        finalDoc = invoice.baocao_hoso_29(thuctapsinh.ma_tts, document)
                        demo = str('共通')
                        archive.write(finalDoc.name, r'%s\申請する技能実習計画の対象となる技能実習生の名簿.docx' % demo)
                        os.unlink(finalDoc.name)
                else:
                    pass

                # Thông tin file 22
                if document.file_22:
                    finalDoc = invoice.baocao_hoso_30(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\技能実習計画認定申請に係る提出書類一覧・確認表.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 23
                if item == 1:
                    if document.file_23:
                        finalDoc = invoice.baocao_hoso_31(thuctapsinh.ma_tts, document)
                        demo = str('共通')
                        archive.write(finalDoc.name, r'%s\委任状.docx' % demo)
                        os.unlink(finalDoc.name)
                else:
                    pass

                # Thông tin file 24
                if item == 1:
                    if document.file_24:
                        finalDoc = invoice.baocao_hoso_36(thuctapsinh.ma_tts, document)
                        demo = str('共通')
                        archive.write(finalDoc.name, r'%s\認定申請手数料払込申告書.docx' % demo)
                        os.unlink(finalDoc.name)
                else:
                    pass

                # Thông tin file 25
                if item == 1:
                    if document.file_25:
                        finalDoc = invoice.baocao_hoso_6(thuctapsinh.ma_tts, document)
                        demo = str('共通')
                        archive.write(finalDoc.name, r'%s\別記様式第７号実習実施者届出書.docx' % demo)
                        os.unlink(finalDoc.name)
                else:
                    pass
                item += 1
        # Go 2
        elif document.giaidoan_hoso == '2 go':
            item = 1
            for thuctapsinh in infor:
                # Thông tin file 1
                if document.file_1:
                    finalDoc = invoice.baocao_hoso_1(document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\第１号　第１面　技能実習計画　認定申請書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 2
                if document.file_2:
                    finalDoc = invoice.baocao_hoso_2(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\第１号　第２面　技能実習計画.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 5
                if document.file_5:
                    finalDoc = invoice.baocao_hoso_34(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\第１号　第５面　実習実施予定表　１年目.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 6
                if document.file_6:
                    finalDoc = invoice.baocao_hoso_35(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\第１号　第６面　実習実施予定表　２年目.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 7
                if document.file_7:
                    finalDoc = invoice.baocao_hoso_5()
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\第１号　第７面　誓約事項.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 8
                if item == 1:
                    if document.file_8:
                        finalDoc = invoice.baocao_hoso_7(thuctapsinh.ma_tts, document)
                        demo = str('共通')
                        archive.write(finalDoc.name, r'%s\参考１ー１号、第１-25号 申請者の概要書.docx' % demo)
                        os.unlink(finalDoc.name)
                else:
                    pass

                # Thông tin file 9
                if item == 1:
                    if document.file_9:
                        finalDoc = invoice.baocao_hoso_8(thuctapsinh.ma_tts, document)
                        demo = str('共通')
                        archive.write(finalDoc.name, r'%s\参考１ー２号　申請者の誓約書.docx' % demo)
                        os.unlink(finalDoc.name)
                else:
                    pass

                # Thông tin file 10
                if document.file_10:
                    finalDoc = invoice.baocao_hoso_9(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１ー３号　技能実習生の履歴書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 11
                if document.file_11:
                    finalDoc = invoice.baocao_hoso_10(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１ー４号、第１ー６ 号 、第１ー８ 号　技能実習責任者・技能実習指導員・生活指導員 の履歴書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 12
                if document.file_12:
                    finalDoc = invoice.baocao_hoso_11(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name,
                                  r'%s\参考１ー５号、 第１ー７号、 第１－９号 技能実習責任者・技能実習指導員・生活指導員の就任承諾書及び誓約書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 13
                if document.file_13:
                    finalDoc = invoice.baocao_hoso_16(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name,
                                  r'%s\参考1-16号、 第1-17号、 第1-18号 技能実習生の報酬・宿泊施設・徴収費用についての説明書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 14
                if document.file_14:
                    finalDoc = invoice.baocao_hoso_18(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１- 14号 技能実習のための雇用契約書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 16
                if document.file_16:
                    finalDoc = invoice.baocao_hoso_33(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１ー19号(Ｅ・Ｆ) 技能実習の期間中の待遇に関する重要事項説明書.docx' % demo)
                    os.unlink(finalDoc.name)


                # Thông tin file 17
                if document.file_17:
                    finalDoc = invoice.baocao_hoso_22(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１ー２２号　技能実習を行わせる理由書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 18
                if document.file_18:
                    finalDoc = invoice.baocao_hoso_32(document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１ー24号 優良要件適合申告書（実習実施者）.docx' % demo)
                    os.unlink(finalDoc.name)

                    finalDoc = invoice.baocao_hoso_32_2(document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考様式第１－24号別紙受検・不受検　技能実習生名簿.docx' % (demo))
                    os.unlink(finalDoc.name)

                # Thông tin file 19
                if document.file_19:
                    finalDoc = invoice.baocao_hoso_25(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１ー２７号　同種業務従事経験等証明書-団体監理型技能実習.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 20
                if document.file_20:
                    finalDoc = invoice.baocao_hoso_28(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１ー３０号　複数の職種及び作業に係る技能実習を行わせる理由書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 21
                if item == 1:
                    if document.file_21:
                        finalDoc = invoice.baocao_hoso_29(thuctapsinh.ma_tts, document)
                        demo = str('共通')
                        archive.write(finalDoc.name, r'%s\申請する技能実習計画の対象となる技能実習生の名簿.docx' % demo)
                        os.unlink(finalDoc.name)
                else:
                    pass

                # Thông tin file 22
                if document.file_22:
                    finalDoc = invoice.baocao_hoso_30(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\技能実習計画認定申請に係る提出書類一覧・確認表.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 23
                if item == 1:
                    if document.file_23:
                        finalDoc = invoice.baocao_hoso_31(thuctapsinh.ma_tts, document)
                        demo = str('共通')
                        archive.write(finalDoc.name, r'%s\委任状.docx' % demo)
                        os.unlink(finalDoc.name)
                else:
                    pass

                # Thông tin file 24
                if item == 1:
                    if document.file_24:
                        finalDoc = invoice.baocao_hoso_36(thuctapsinh.ma_tts, document)
                        demo = str('共通')
                        archive.write(finalDoc.name, r'%s\認定申請手数料払込申告書.docx' % demo)
                        os.unlink(finalDoc.name)
                else:
                    pass

                # Thông tin file 25
                if item == 1:
                    if document.file_25:
                        finalDoc = invoice.baocao_hoso_6(thuctapsinh.ma_tts, document)
                        demo = str('共通')
                        archive.write(finalDoc.name, r'%s\別記様式第７号実習実施者届出書.docx' % demo)
                        os.unlink(finalDoc.name)
                else:
                    pass
        # Go 3
        elif document.giaidoan_hoso == '3 go':
            item = 1
            for thuctapsinh in infor:
                # Thông tin file 1
                if document.file_1:
                    finalDoc = invoice.baocao_hoso_1(document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\第１号　第１面　技能実習計画　認定申請書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 2
                if document.file_2:
                    finalDoc = invoice.baocao_hoso_2(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\第１号　第２面　技能実習計画.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 5
                if document.file_5:
                    finalDoc = invoice.baocao_hoso_34(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\第１号　第５面　実習実施予定表　１年目.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 6
                if document.file_6:
                    finalDoc = invoice.baocao_hoso_35(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\第１号　第６面　実習実施予定表　２年目.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 7
                if document.file_7:
                    finalDoc = invoice.baocao_hoso_5()
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\第１号　第７面　誓約事項.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 8
                if item == 1:
                    if document.file_8:
                        finalDoc = invoice.baocao_hoso_7(thuctapsinh.ma_tts, document)
                        demo = str('共通')
                        archive.write(finalDoc.name, r'%s\参考１ー１号、第１-25号 申請者の概要書.docx' % demo)
                        os.unlink(finalDoc.name)
                else:
                    pass

                # Thông tin file 9
                if item == 1:
                    if document.file_9:
                        finalDoc = invoice.baocao_hoso_8(thuctapsinh.ma_tts, document)
                        demo = str('共通')
                        archive.write(finalDoc.name, r'%s\参考１ー２号　申請者の誓約書.docx' % demo)
                        os.unlink(finalDoc.name)
                else:
                    pass

                # Thông tin file 10
                if document.file_10:
                    finalDoc = invoice.baocao_hoso_9(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１ー３号　技能実習生の履歴書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 11
                if document.file_11:
                    finalDoc = invoice.baocao_hoso_10(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１ー４号、第１ー６ 号 、第１ー８ 号　技能実習責任者・技能実習指導員・生活指導員 の履歴書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 12
                if document.file_12:
                    finalDoc = invoice.baocao_hoso_11(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name,
                                  r'%s\参考１ー５号、 第１ー７号、 第１－９号 技能実習責任者・技能実習指導員・生活指導員の就任承諾書及び誓約書.docx' % demo)
                    os.unlink(finalDoc.name)


                # Thông tin file 13
                if document.file_13:
                    finalDoc = invoice.baocao_hoso_16(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name,
                                  r'%s\参考1-16号、 第1-17号、 第1-18号 技能実習生の報酬・宿泊施設・徴収費用についての説明書.docx' % demo)
                    os.unlink(finalDoc.name)


                # Thông tin file 14
                if document.file_14:
                    finalDoc = invoice.baocao_hoso_18(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１- 14号 技能実習のための雇用契約書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 16
                if document.file_16:
                    finalDoc = invoice.baocao_hoso_33(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１ー19号(Ｅ・Ｆ) 技能実習の期間中の待遇に関する重要事項説明書.docx' % demo)
                    os.unlink(finalDoc.name)


                # Thông tin file 17
                if document.file_17:
                    finalDoc = invoice.baocao_hoso_22(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１ー２２号　技能実習を行わせる理由書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 18
                if document.file_18:
                    finalDoc = invoice.baocao_hoso_32(document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１ー24号 優良要件適合申告書（実習実施者）.docx' % demo)
                    os.unlink(finalDoc.name)

                    finalDoc = invoice.baocao_hoso_32_2(document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考様式第１－24号別紙受検・不受検　技能実習生名簿.docx' % (demo))
                    os.unlink(finalDoc.name)

                # Thông tin file 19
                if document.file_19:
                    finalDoc = invoice.baocao_hoso_25(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１ー２７号　同種業務従事経験等証明書-団体監理型技能実習.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 20
                if document.file_20:
                    finalDoc = invoice.baocao_hoso_28(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\参考１ー３０号　複数の職種及び作業に係る技能実習を行わせる理由書.docx' % demo)
                    os.unlink(finalDoc.name)

                # Thông tin file 21
                if item == 1:
                    if document.file_21:
                        finalDoc = invoice.baocao_hoso_29(thuctapsinh.ma_tts, document)
                        demo = str('共通')
                        archive.write(finalDoc.name, r'%s\申請する技能実習計画の対象となる技能実習生の名簿.docx' % demo)
                        os.unlink(finalDoc.name)
                else:
                    pass

                # Thông tin file 22
                if document.file_22:
                    finalDoc = invoice.baocao_hoso_30(thuctapsinh.ma_tts, document)
                    demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                    archive.write(finalDoc.name, r'%s\技能実習計画認定申請に係る提出書類一覧・確認表.docx' % demo)
                    os.unlink(finalDoc.name)
                # Thông tin file 23
                if item == 1:
                    if document.file_23:
                        finalDoc = invoice.baocao_hoso_31(thuctapsinh.ma_tts, document)
                        demo = str('共通')
                        archive.write(finalDoc.name, r'%s\委任状.docx' % demo)
                        os.unlink(finalDoc.name)
                else:
                    pass

                # Thông tin file 24
                if item == 1:
                    if document.file_24:
                        finalDoc = invoice.baocao_hoso_36(thuctapsinh.ma_tts, document)
                        demo = str('共通')
                        archive.write(finalDoc.name, r'%s\認定申請手数料払込申告書.docx' % demo)
                        os.unlink(finalDoc.name)
                else:
                    pass

                # Thông tin file 25
                if item == 1:
                    if document.file_25:
                        finalDoc = invoice.baocao_hoso_6(thuctapsinh.ma_tts, document)
                        demo = str('共通')
                        archive.write(finalDoc.name, r'%s\別記様式第７号実習実施者届出書.docx' % demo)
                        os.unlink(finalDoc.name)
                else:
                    pass

        archive.close()
        reponds.flush()
        ret_zip = reponds.getvalue()
        reponds.close()

        finalDoc.name = '%s.zip' % (str(document.donhang_hoso.tendonhang_donhang))
        return request.make_response(ret_zip,
                                     [('Content-Type', 'application/zip'),
                                      ('Content-Disposition', content_disposition(finalDoc.name))])

    @http.route('/luutru/<string:id>', type='http', auth="public")
    def downloadluutru(self, id=False, **kwargs):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        document = request.env['luutru.luutru'].browse(int(id))
        invoice = request.env['luutru.luutru']
        infor = document.thuctapsinh_luutru

        filename = ''
        if document.tucach_luutru.tucach_luutru == '在留資格認定証明書交付申請書':
            for thuctapsinh in infor:
                finalDoc = invoice.baocao_hoso_43(thuctapsinh.ma_tts, document)
                demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                archive.write(finalDoc.name, r'%s\在留資格認定証明書交付申請書.docx' % demo)
                os.unlink(finalDoc.name)
                filename = '在留資格認定証明書交付申請書'

        if document.tucach_luutru.tucach_luutru == '在留期間更新許可申請書':
            for thuctapsinh in infor:
                finalDoc = invoice.baocao_hoso_46(thuctapsinh.ma_tts, document)
                demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                archive.write(finalDoc.name, r'%s\在留期間更新許可申請書.docx' % demo)
                os.unlink(finalDoc.name)
                filename = '在留期間更新許可申請書'

        if document.tucach_luutru.tucach_luutru == '在留資格変更許可申請書':
            for thuctapsinh in infor:
                finalDoc = invoice.baocao_hoso_49(thuctapsinh.ma_tts, document)
                demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                archive.write(finalDoc.name, r'%s\在留資格変更許可申請書.docx' % demo)
                os.unlink(finalDoc.name)
                filename = '在留資格変更許可申請書'

        archive.close()
        reponds.flush()
        ret_zip = reponds.getvalue()
        reponds.close()

        finalDoc.name = '%s.zip' % filename
        return request.make_response(ret_zip,
                                     [('Content-Type', 'application/zip'),
                                      ('Content-Disposition', content_disposition(finalDoc.name))])

    @http.route('/phaicu/<string:id>', type='http', auth="public")
    def phaicu(self, id=False, **kwargs):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        document = request.env['hoso.hoso'].browse(int(id))
        invoice = request.env['hoso.hoso']

        infor = document.thuctapsinh_hoso
        # Go 1
        item = 1
        for thuctapsinh in infor:
            if document.file_26:
                finalDoc = invoice.baocao_hoso_50_MOI(thuctapsinh.ma_tts, document)
                demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                archive.write(finalDoc.name, r'%s\参考様式第1-3号　技能実習生の履歴書.docx' % demo)
                os.unlink(finalDoc.name)

            if document.file_30:
                finalDoc = invoice.baocao_hoso_58(thuctapsinh.ma_tts, document)
                demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                archive.write(finalDoc.name, r'%s\参考様式第１-21号 技能実習の準備に関し本国で支払った費用の明細書.docx' % demo)
                os.unlink(finalDoc.name)

            if document.file_33:
                finalDoc = invoice.baocao_hoso_64(thuctapsinh.ma_tts, document)
                demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                archive.write(finalDoc.name, r'%s\日本への技能実習生派遣契約書.docx' % demo)
                os.unlink(finalDoc.name)

                finalDoc = invoice.baocao_hoso_64_1(thuctapsinh.ma_tts, document)
                demo = str(thuctapsinh.ma_tts) + '-' + str(thuctapsinh.ten_lt_tts)
                archive.write(finalDoc.name, r'%s\Hợp_Đồng_Phái_Cử.docx' % demo)
                os.unlink(finalDoc.name)
            if item == 1:
                demo = str('共通')
                if document.file_29:
                    finalDoc = invoice.baocao_hoso_57(thuctapsinh.ma_tts, document)
                    archive.write(finalDoc.name, r'%s\参考様式第１-20 号 技能実習生の申告書.docx' % demo)
                    os.unlink(finalDoc.name)

                if document.file_35:
                    finalDoc = invoice.baocao_hoso_60(thuctapsinh.ma_tts, document)
                    archive.write(finalDoc.name, r'%s\参考様式第１-27号 同種業務従事経験等証明書.docx' % demo)
                    os.unlink(finalDoc.name)

                if document.file_27:
                    finalDoc = invoice.baocao_hoso_51(thuctapsinh.ma_tts, document)
                    archive.write(finalDoc.name, r'%s\参考様式第１-10号 日本への技能実習生派遣契約書.docx' % demo)
                    os.unlink(finalDoc.name)

                if document.file_32:
                    # finalDoc = invoice.baocao_hoso_61(thuctapsinh.ma_tts, document)
                    # archive.write(finalDoc.name, r'%s\参考１ー２8 外国の所属機関による証明書.docx' % demo)
                    # os.unlink(finalDoc.name)

                    finalDoc = invoice.baocao_hoso_61_MOI(thuctapsinh.ma_tts, document)
                    archive.write(finalDoc.name, r'%s\参考様式第1-28号　外国の所属機関による証明書（団体監理型技能実習）.docx' % demo)
                    os.unlink(finalDoc.name)

                if document.file_31:
                    finalDoc = invoice.baocao_hoso_59(thuctapsinh.ma_tts, document)
                    archive.write(finalDoc.name, r'%s\参考様式第１-23号 技能実習生の推薦状.docx' % demo)
                    os.unlink(finalDoc.name)

                    finalDoc = invoice.baocao_hoso_59_1(thuctapsinh.ma_tts, document)
                    archive.write(finalDoc.name, r'%s\参考様式第１-23号 技能実習生の推薦状_Anh.docx' % demo)
                    os.unlink(finalDoc.name)

                    finalDoc = invoice.baocao_hoso_dsld(thuctapsinh.ma_tts, document)
                    archive.write(finalDoc.name, r'%s\Danh_Sach_Lao_Dong.docx' % demo)
                    os.unlink(finalDoc.name)
                if document.file_28:
                    finalDoc = invoice.baocao_hoso_52(thuctapsinh.ma_tts, document)
                    archive.write(finalDoc.name, r'%s\参考様式第１-13号外国の準備機関の概要書及び誓約書.docx' % demo)
                    os.unlink(finalDoc.name)
                if document.file_36:
                    # finalDoc = invoice.baocao_hoso_62(document)
                    # archive.write(finalDoc.name, r'%s\参考様式第１-29号.docx' % demo)
                    # os.unlink(finalDoc.name)

                    finalDoc = invoice.baocao_hoso_62_MOI(document)
                    archive.write(finalDoc.name, r'%s\参考様式第1-29号　入国前講習実施（予定）表.docx' % demo)
                    os.unlink(finalDoc.name)

                if document.file_34:
                    finalDoc = invoice.baocao_hoso_63(document)
                    archive.write(finalDoc.name, r'%s\事前講習実施報告書.docx' % demo)
                    os.unlink(finalDoc.name)
                if document.file_37:
                    finalDoc = invoice.baocao_hoso_53(thuctapsinh.ma_tts, document)
                    archive.write(finalDoc.name, r'%s\参考様式第４-８号入国前講習実施記録.docx' % demo)
                    os.unlink(finalDoc.name)
                item += 1
            else:
                pass
        archive.close()
        reponds.flush()
        ret_zip = reponds.getvalue()
        reponds.close()
        finalDoc.name = u'%s_%s.zip' % (str(document.donhang_hoso.xinghiep_donhang.ten_viet_xnghiep),str(document.donhang_hoso.tendonhang_donhang))
        return request.make_response(ret_zip,
                                     [('Content-Type', 'application/zip'),
                                      ('Content-Disposition', content_disposition(finalDoc.name))])
