# -*- coding: utf-8 -*-
import codecs
import datetime
import logging
from builtins import print
from io import BytesIO
from tempfile import NamedTemporaryFile
from docxtpl import DocxTemplate, Listing
from odoo import models, fields, api


# from datetime import datetime, timedelta


def convert(self):
    if self:
        minutes = self * 60
        hours, minutes = divmod(minutes, 60)
        return "%02d %s %02d %s" % (hours, u'時', minutes, u'分')
    else:
        return "時     分"


def convert_hour(self):
    if self:
        minutes = self * 60
        hours, minutes = divmod(minutes, 60)
        return "%02d:%02d" % (hours, minutes)
    else:
        ""


def convert_viet(self):
    if self:
        minutes = self * 60
        hours, minutes = divmod(minutes, 60)
        return "%02d %s %02d %s" % (hours, u'giờ', minutes, u'phút')
    else:
        return 'Giờ   phút'


def place_value(number):
    if number:
        return ("{:,}".format(number))
    else:
        return ''


def gender_check(gender):
    if gender == u'Nam':
        return u'㊚ ・ 女'
    elif gender == u'Nữ':
        return u'男 ・ ㊛'
    else:
        return '男 ・ 女'


def convert_date_hocvan(year, month):
    if year and month:
        return str(year) + '年' + str(month) + '月'
    elif year:
        return str(year) + '年'
    else:
        return ' '


def convert_date_hocvan_v(year, month):
    if year and month:
        return 'Tháng ' + str(month) + ' năm ' + str(year)
    elif year:
        return 'Năm ' + str(year)
    else:
        return ' '


def convert_hocvan(a, b):
    if a != '' or b != '':
        return str(a) + ' ～ ' + str(b)
    else:
        return ' '


def convert_congtac(c, d):
    if c != '' or d != '':
        return str(c) + ' ( ' + str(d) + ' )'
    else:
        return ' '


def convert_date(self):
    if self:
        return str(self.year) + '年' + \
               str(self.month) + '月' + \
               str(self.day) + '日'
    else:
        return '年　月　日'


def convert_date2(year, month, day):
    if year and month and day:
        return str(year) + '年' + \
               str(month) + '月' + \
               str(day) + '日'
    else:
        return '年　月　日'


def convert_th(self):
    ans = datetime.date(self.year, self.month, self.day)
    substr = ans.strftime("%A")
    if substr == 'Sunday':
        return '日'
    elif substr == 'Monday':
        return '月'
    elif substr == 'Tuesday':
        return '火'
    elif substr == 'Wednesday':
        return '水'
    elif substr == 'Thursday':
        return '木'
    elif substr == 'Friday':
        return '金'
    elif substr == 'Saturday':
        return '土'
    else:
        return ''


def kiemtra(data):
    if data:
        return data
    else:
        return ''


def round_nam(sefl):
    if sefl:
        return round(sefl, 1)
    else:
        return 0


def kiemtra1(data):
    if data:
        demo = data.replace('.', ',')
        return demo
    else:
        return ''


def nganhnghe(self):
    if self:
        demo = self.find(':')
        print(demo)
        if demo != -1:
            return self.split(":")[1]
        else:
            return self
    else:
        return ''
