import codecs
from datetime import datetime
from odoo import models, fields, api


class HoSoDonHang(models.Model):
    _inherit = 'donhang.donhang'
    _order = 'id desc'

    @api.model
    def _get_current_year(self):
        return str(datetime.now().year)

    day_departure_doc = fields.Char("Ngày", size=2)
    month_departure_doc = fields.Selection([('01', '01'), ('02', '02'), ('03', '03'), ('04', '04'),
                                            ('05', '05'), ('06', '06'), ('07', '07'), ('08', '08'),
                                            ('09', '09'), ('10', '10'), ('11', '11'), ('12', '12'), ], "Tháng")

    year_departure_doc = fields.Char("Năm", size=4, default=lambda self: self._get_current_year())

    date_departure_doc = fields.Char("Ngày nhập cảnh Dự kiến", store=False, compute='_date_departure_doc')

    @api.multi
    @api.depends('day_departure_doc', 'month_departure_doc', 'year_departure_doc')
    def _date_departure_doc(self):
        for rec in self:
            if rec.day_departure_doc and rec.month_departure_doc and rec.year_departure_doc:
                rec.date_departure_doc = u"Ngày %s tháng %s năm %s" % (
                    rec.day_departure_doc, rec.month_departure_doc, rec.year_departure_doc)
            elif rec.month_departure_doc and rec.year_departure_doc:
                rec.date_departure_doc = u"Tháng %s năm %s" % (
                    rec.month_departure_doc, rec.year_departure_doc)
            elif rec.year_departure_doc:
                rec.date_departure_doc = u'Năm %s' % rec.year_departure_doc
            else:
                rec.date_departure_doc = ""

    year_expire = fields.Integer(string="Thời hạn hợp đồng (năm)")

    # Thong tin bo sung cho ho so noi

    date_create_letter_promotion = fields.Date("Ngày làm thư tiến cử")
    date_expected_send_to_customer = fields.Date("Ngày ký hợp đồng phái cử")

    danhsachTTS = fields.Many2many(comodel_name='thuctapsinh.thuctapsinh', string='Danh sách thực tập sinh',
                                   compute="list_thuctapsinh")

    @api.multi
    def list_thuctapsinh(self):
        if self.id:
            self.danhsachTTS = self.env['thuctapsinh.thuctapsinh'].search([('donhang_tts', '=', self.id)])
        else:
            pass

    daotao_truocnhapcanh = fields.Many2one(comodel_name='daotao.daotaotruoc', string='Đào tạo trước nhập cảnh',
                                           compute="_daotao_truocnhapcanh")

    @api.model
    def _daotao_truocnhapcanh(self):
        daotao_truocnhapcanh = self.env['daotao.daotaotruoc'].search([('donhang_daotaotruoc', '=', self.id)], limit=1)
        self.daotao_truocnhapcanh = daotao_truocnhapcanh.id

    coquan_nhanuoc_n = fields.Char("Cơ quan nhà nước - Tiếng nhật", default=u'海外労働管理局（DOLAB）')
    coquan_nhanuoc = fields.Char("Cơ quan nhà nước - Tiếng Anh", default=u'Department of Oversea Labour (DOLAB)')
    person_sign_proletter = fields.Char("Tên người ký thư tiến cử - Tiếng Anh", default=u'NGUYEN THI ANH HANG')
    person_sign_proletter_n = fields.Char("Tên người ký thư tiến cử - Tiếng Nhật", default=u'NGUYEN THI ANH HANG')
    position_person_sign = fields.Char("Chức danh người ký thư tiến cử - Tiếng Anh",
                                       default=u'Deputy Head of Division for Japan, Southeast Asia')
    position_person_sign_jp = fields.Char("Chức danh người ký thư tiến cử - Tiếng Nhật", default=u'日本、東南アジア副部長')

    giaidoan_phi = fields.Selection(string='Giai đoạn',
                                    selection=[('1 go', '1 go'),
                                               ('3 go', '3 go')])

    @api.onchange('giaidoan_phi')
    def onchange_giaidoan_phi(self):
        self.ngaythu_1 = ''
        self.ngaythu_2= ''
        self.ngaythu_3 = ''
        self.ngaythu_4 = ''
        self.ngaythu_5 = ''

        self.du_kien_1 = False
        self.du_kien_2 = False
        self.du_kien_3 = False
        self.du_kien_4 = False
        self.du_kien_5 = False
        if self.congtyphaicu_donhang and self.giaidoan_phi:
            phaicu_loaiphi = self.env['phaicu.loaiphi'].search([('congtyphaicu_phi', '=', self.congtyphaicu_donhang.id),
                                                                ('giaidoan_phi', '=', self.giaidoan_phi)], limit=1)
            self.loaiphi_1 = phaicu_loaiphi.loaiphi_1
            self.loaiphi_v_1 = phaicu_loaiphi.loaiphi_v_1
            self.sotien_viet_1 = phaicu_loaiphi.sotien_viet_1
            self.sotien_nhat_1 = phaicu_loaiphi.sotien_nhat_1

            self.loaiphi_2 = phaicu_loaiphi.loaiphi_2
            self.loaiphi_v_2 = phaicu_loaiphi.loaiphi_v_2
            self.sotien_viet_2 = phaicu_loaiphi.sotien_viet_2
            self.sotien_nhat_2 = phaicu_loaiphi.sotien_nhat_2

            self.loaiphi_3 = phaicu_loaiphi.loaiphi_3
            self.loaiphi_v_3 = phaicu_loaiphi.loaiphi_v_3
            self.sotien_viet_3 = phaicu_loaiphi.sotien_viet_3
            self.sotien_nhat_3 = phaicu_loaiphi.sotien_nhat_3

            self.loaiphi_4 = phaicu_loaiphi.loaiphi_4
            self.loaiphi_v_4 = phaicu_loaiphi.loaiphi_v_4
            self.sotien_viet_4 = phaicu_loaiphi.sotien_viet_4
            self.sotien_nhat_4 = phaicu_loaiphi.sotien_nhat_4

            self.loaiphi_5 = phaicu_loaiphi.loaiphi_5
            self.loaiphi_v_5 = phaicu_loaiphi.loaiphi_v_5
            self.sotien_viet_5 = phaicu_loaiphi.sotien_viet_5
            self.sotien_nhat_5 = phaicu_loaiphi.sotien_nhat_5

    loaiphi_1 = fields.Char(string="Loại phí(Tiếng nhật)")  # hhjp_0494
    loaiphi_v_1 = fields.Char(string="Loại phí(Tiếng Việt)")  # hhjp_0494_v
    ngaythu_1 = fields.Date(string="Ngày thu")  # hhjp_0495
    du_kien_1 = fields.Boolean(string="Dự kiến")
    sotien_viet_1 = fields.Integer(string="Số tiền(Tiếng việt)", default="5400000")  # hhjp_0496
    sotien_nhat_1 = fields.Integer(string="Số tiền (Nhật)", default="27000")  # bs_0021
    # -----------------------------------------------------------------
    loaiphi_2 = fields.Char(string="Loại phí(Tiếng Nhật)")  # hhjp_0494
    loaiphi_v_2 = fields.Char(string="Loại phí(Tiếng Việt)")  # hhjp_0494_v
    ngaythu_2 = fields.Date(string="Ngày thu")  # hhjp_0495
    du_kien_2 = fields.Boolean(string="Dự kiến")
    sotien_viet_2 = fields.Integer(string="Số tiền (Việt)", default="100000")  # hhjp_0496
    sotien_nhat_2 = fields.Integer(string="Số tiền (Nhật)", default="500")  # bs_0021
    # ------------------------------------------------------------------
    loaiphi_3 = fields.Char(string="Loại phí(Tiếng Nhật)")  # hhjp_0494
    loaiphi_v_3 = fields.Char(string="Loại phí(Tiếng Việt)")  # hhjp_0494_v
    ngaythu_3 = fields.Date(string="Ngày thu")  # hhjp_0495
    du_kien_3 = fields.Boolean(string="Dự kiến")
    sotien_viet_3 = fields.Integer(string="Số tiền (Việt)", default="630000")  # hhjp_0496
    sotien_nhat_3 = fields.Integer(string="Số tiền (Nhật)", default="3150")  # bs_0021
    # -----------------------------------------------------------------
    loaiphi_4 = fields.Char(string="Loại phí(Tiếng Nhật)")  # hhjp_0494
    loaiphi_v_4 = fields.Char(string="Loại phí(Tiếng Việt)")  # hhjp_0494_v
    ngaythu_4 = fields.Date(string="Ngày thu")  # hhjp_0495
    du_kien_4 = fields.Boolean(string="Dự kiến")
    sotien_viet_4 = fields.Integer(string="Số tiền (Việt)", default="500000")  # hhjp_0496
    sotien_nhat_4 = fields.Integer(string="Số tiền (Nhật)", default="2500")  # bs_0021
    # ------------------------------------------------------------------
    loaiphi_5 = fields.Char(string="Loại phí(Tiếng Nhật)")  # hhjp_0494
    loaiphi_v_5 = fields.Char(string="Loại phí(Tiếng Việt)")  # hhjp_0494_v
    ngaythu_5 = fields.Date(string="Ngày thu")  # hhjp_0495
    du_kien_5 = fields.Boolean(string="Dự kiến")
    sotien_viet_5 = fields.Integer(string="Số tiền (Việt)", default="54100000")  # hhjp_0496
    sotien_nhat_5 = fields.Integer(string="Số tiền (Nhật)", default="270500")  # bs_0021
    # -------------------------------------------------------------------
    tong_sotien_v = fields.Integer(string="Tổng cộng", compute="_tong_tien_v")
    tong_sotien_n = fields.Integer(string="Tổng cộng", compute="_tong_tien_n")

    @api.multi
    @api.depends('sotien_viet_1', 'sotien_viet_2', 'sotien_viet_3', 'sotien_viet_4', 'sotien_viet_5')
    def _tong_tien_v(self):
        if self.sotien_viet_1 or self.sotien_viet_2 or self.sotien_viet_3 or self.sotien_viet_4 or self.sotien_viet_5:
            self.tong_sotien_v = self.sotien_viet_1 + self.sotien_viet_2 + self.sotien_viet_3 + self.sotien_viet_4 + self.sotien_viet_5
        else:
            self.tong_sotien_v = 0

    @api.multi
    @api.depends('sotien_nhat_1', 'sotien_nhat_2', 'sotien_nhat_3', 'sotien_nhat_4', 'sotien_nhat_5')
    def _tong_tien_n(self):
        if self.sotien_nhat_1 or self.sotien_nhat_2 or self.sotien_nhat_3 or self.sotien_nhat_4 or self.sotien_nhat_5:
            self.tong_sotien_n = self.sotien_nhat_1 + self.sotien_nhat_2 + self.sotien_nhat_3 + self.sotien_nhat_4 + self.sotien_nhat_5
        else:
            self.tong_sotien_n = 0

    congtypc2 = fields.Many2one(comodel_name="ctpc2.ctpc2", string="Tên xí nghiệp phái cử")
    bophan = fields.Char(string="Bộ phận")
    nganhnghe_xnpc = fields.Many2one(comodel_name="loaicongviec.loaicongviec", string="Ngành nghề",
                                     related="loainghe_donhang")
    quatrinh_xnpc = fields.Boolean(string="Xí nghiệp phái cử")
    quatrinh_xnpc_lydo = fields.Char(string="Lý do")
    quatrinh_nguyenvong = fields.Boolean(string="Nguyện vọng của TTS")
    quatrinh_khac = fields.Boolean(string="Khác")
    quatrinh_khac_lydo = fields.Char()
    mqh_voi_tts = fields.Boolean(string="Tiếp tục duy trì mối quan hệ với TTS (giữ lại nơi làm việc,cho nghỉ việc)")
    mqh_tuthoiviec = fields.Boolean(string="Tự thôi việc")
    mqh_khac = fields.Boolean(string="Khác")
    mqh_khac_lydo = fields.Char(string="Lý do")
    dudinh_quaylai = fields.Boolean(string="Quay trở lại xí nghiệp phái cử")
    dudinh_quaylai_tencty = fields.Many2one(comodel_name="ctpc2.ctpc2", string="Tên công ty")
    dudinh_quaylai_bophan = fields.Char(string="Bộ phận")
    dudinh_quaylai_cv = fields.Many2one(comodel_name="loaicongviec.loaicongviec", string="Công việc", )
    dudinh_khong_quaylai = fields.Boolean(string="Không có ý định quay lại xí nghiệp phái cử")
    dudinh_khongco_ydinh = fields.Boolean(string="Chưa quyết định")

    @api.onchange('dudinh_quaylai')
    def _onchange_dudinh_quaylai(self):
        if self.dudinh_quaylai:
            self.dudinh_quaylai_tencty = self.congtypc2
            self.dudinh_quaylai_bophan = self.bophan
            self.dudinh_quaylai_cv = self.nganhnghe_xnpc

    hinhthuc_vieclam = fields.Selection(string="Hình thức việc làm", selection=[('Toàn thời gian', 'Toàn thời gian'),
                                                                                ('Bán thời gian', 'Bán thời gian'), ])


class job(models.Model):
    _name = 'intern.job'
    _description = u'Ngành nghề'
    _rec_name = 'name'

    name = fields.Char("Tiếng Việt")  # hh_107
    name_en = fields.Char("Tiếng Anh")  # hh_108
