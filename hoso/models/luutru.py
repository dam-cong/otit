# -*- coding: utf-8 -*-
import codecs
import datetime
import logging
from io import BytesIO
from tempfile import NamedTemporaryFile
from docxtpl import DocxTemplate, Listing
from odoo import models, fields, api
from . import format_hoso

_logger = logging.getLogger(__name__)

class TuCach(models.Model):
    _inherit = 'tucach.luutru'
    _rec_name = 'tucach_luutru'

class LuuTru(models.Model):
    _name = 'luutru.luutru'
    _rec_name = 'nhapquoc_luutru'
    _order = 'id desc'

    donhang_luutru = fields.Many2one(comodel_name='donhang.donhang', string='Đơn hàng', required=True,
                                     ondelete='cascade')
    nhapquoc_luutru = fields.Date(string="Ngày nhập quốc", related='donhang_luutru.thoigian_donhang')  #
    ngay_lapvanban_luutru = fields.Date(string='Ngày lập văn bản', required=True, default=fields.Date.today())
    xinghiep_luutru = fields.Many2one(comodel_name='xinghiep.xinghiep', string='Xí nghiệp',
                                      related='donhang_luutru.xinghiep_donhang')
    chinhanh_luutru = fields.Many2one(comodel_name='xinghiep.chinhanh', string='Chi nhánh')

    @api.onchange('donhang_luutru')
    def onchange_method(self):
        self.xinghiep_luutru = self.donhang_luutru.xinghiep_donhang

    tucach_luutru = fields.Many2one(comodel_name='tucach.luutru', string='Tư cách lưu trú')
    kieutts_luutru = fields.Boolean(string='Các kiểu thực tập sinh')
    thuctapsinh_luutru = fields.Many2many(comodel_name='thuctapsinh.thuctapsinh', string='Danh sách thực tập sinh',
                                          domain="[('donhang_tts', '=', donhang_luutru)]", compute='list_thuctapsinh')

    @api.multi
    def list_thuctapsinh(self):
        if self.donhang_luutru:
            self.thuctapsinh_luutru = self.env['thuctapsinh.thuctapsinh'].search([('donhang_tts', '=', self.donhang_luutru.id)])
        else:
            pass

    giaidoan_luutru = fields.Selection(string='Giai đoạn', required=True, selection=[('1 go', '1 go'),
                                                                                     ('2 go', '2 go'),
                                                                                     ('3 go', '3 go')],default='1 go')

    @api.onchange('giaidoan_luutru')
    def onchange_giaidoan_luutru(self):
        self.tucach_luutru = ()

    @api.multi
    def luutru_button(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/luutru/%s' % (self.id),
            'target': 'self', }

    @api.multi
    def luutru_button_save(self):
        pass

    @api.multi
    def baocao_hoso_43(self, ma_thuctapsinh, document):
        print('43')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_43")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            context = {}
            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            if document.giaidoan_luutru == '1 go':
                context['hhjp_0911'] = format_hoso.kiemtra(thuctapsinh.sochungnhan_namnhat)
                if thuctapsinh.batdau_kehoach_namnhat:
                    context['hhjp_0596_Y'] = thuctapsinh.batdau_kehoach_namnhat.year
                    context['hhjp_0596_M'] = thuctapsinh.batdau_kehoach_namnhat.month
                    context['hhjp_0596_D'] = thuctapsinh.batdau_kehoach_namnhat.day
                else:
                    context['hhjp_0596_Y'] = ''
                    context['hhjp_0596_M'] = ''
                    context['hhjp_0596_D'] = ''

                if thuctapsinh.ketthuc_kehoach_namnhat:
                    context['hhjp_0597_Y'] = thuctapsinh.ketthuc_kehoach_namnhat.year
                    context['hhjp_0597_M'] = thuctapsinh.ketthuc_kehoach_namnhat.month
                    context['hhjp_0597_D'] = thuctapsinh.ketthuc_kehoach_namnhat.day
                else:
                    context['hhjp_0597_Y'] = ''
                    context['hhjp_0597_M'] = ''
                    context['hhjp_0597_D'] = ''

                if thuctapsinh.ngaychungnhan_namnhat:
                    context['hhjp_0582_Y'] = thuctapsinh.ngaychungnhan_namnhat.year
                    context['hhjp_0582_M'] = thuctapsinh.ngaychungnhan_namnhat.month
                    context['hhjp_0582_D'] = thuctapsinh.ngaychungnhan_namnhat.day
                else:
                    context['hhjp_0582_Y'] = ''
                    context['hhjp_0582_M'] = ''
                    context['hhjp_0582_D'] = ''

                context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_luutru.ten_han_xnghiep)
                context['bs_0109'] = format_hoso.kiemtra(document.xinghiep_luutru.phapnhan_xinghiep)
                context['hhjp_0011'] = format_hoso.kiemtra(document.xinghiep_luutru.diachi_han_xnghiep)
                context['hhjp_0012'] = format_hoso.kiemtra(document.xinghiep_luutru.sdt_xnghiep)
                context['hhjp_0811'] = format_hoso.kiemtra(document.xinghiep_luutru.so_chapnhan_daotao)
                context['hhjp_0023'] = format_hoso.place_value(document.xinghiep_luutru.sovon_xnghiep)
                context['hhjp_0024'] = format_hoso.place_value(document.xinghiep_luutru.doanhthu_namtruoc_xnghiep)
                context['hhjp_0097'] = format_hoso.kiemtra(
                    document.donhang_luutru.nghiepdoan_donhang.ten_han_nghiepdoan)

                context['hhjp_0912'] = format_hoso.kiemtra(
                    document.donhang_luutru.nghiepdoan_donhang.so_phapnhan_nghiepdoan)

                context['bs_0111'] = format_hoso.place_value(document.xinghiep_luutru.so_nvien_xnghiep)
                context['bs_0113'] = format_hoso.place_value(
                    document.donhang_luutru.nghiepdoan_donhang.so_nvien_chinhthuc_nghiepdoan)
                context['bs_0112'] = format_hoso.place_value(document.xinghiep_luutru.nvien_nuocngoai_xnghiep)

                if document.donhang_luutru.nghiepdoan_donhang.ngay_capphep_nghiepdoan:
                    context[
                        'bs_0114_Y'] = document.donhang_luutru.nghiepdoan_donhang.ngay_capphep_nghiepdoan.year
                    context[
                        'bs_0114_M'] = document.donhang_luutru.nghiepdoan_donhang.ngay_capphep_nghiepdoan.month
                    context[
                        'bs_0114_D'] = document.donhang_luutru.nghiepdoan_donhang.ngay_capphep_nghiepdoan.day
                else:
                    context['bs_0114_Y'] = ''
                    context['bs_0114_M'] = ''
                    context['bs_0114_D'] = ''

                if document.donhang_luutru.nghiepdoan_donhang.batdau_hieuluc_giayphep_nghiepdoan:
                    context[
                        'bs_0115_Y'] = document.donhang_luutru.nghiepdoan_donhang.batdau_hieuluc_giayphep_nghiepdoan.year
                    context[
                        'bs_0115_M'] = document.donhang_luutru.nghiepdoan_donhang.batdau_hieuluc_giayphep_nghiepdoan.month
                    context[
                        'bs_0115_D'] = document.donhang_luutru.nghiepdoan_donhang.batdau_hieuluc_giayphep_nghiepdoan.day
                else:
                    context['bs_0115_Y'] = ''
                    context['bs_0115_M'] = ''
                    context['bs_0115_D'] = ''

                if document.donhang_luutru.nghiepdoan_donhang.ketthuc_hieuluc_giayphep_nghiepdoan:
                    context[
                        'bs_0116_Y'] = document.donhang_luutru.nghiepdoan_donhang.ketthuc_hieuluc_giayphep_nghiepdoan.year
                    context[
                        'bs_0116_M'] = document.donhang_luutru.nghiepdoan_donhang.ketthuc_hieuluc_giayphep_nghiepdoan.month
                    context[
                        'bs_0116_D'] = document.donhang_luutru.nghiepdoan_donhang.ketthuc_hieuluc_giayphep_nghiepdoan.day
                else:
                    context['bs_0116_Y'] = ''
                    context['bs_0116_M'] = ''
                    context['bs_0116_D'] = ''

                context[
                    'hhjp_0103'] = format_hoso.kiemtra(
                    document.donhang_luutru.nghiepdoan_donhang.ten_daidien_han_nghiepdoan)
                context[
                    'hhjp_0103_1'] = format_hoso.kiemtra(
                    document.donhang_luutru.nghiepdoan_donhang.chucvu_daidien_phienam_nghiepdoan)

                if document.ngay_lapvanban_luutru:
                    context['hhjp_0193_D'] = document.ngay_lapvanban_luutru.day
                    context['hhjp_0193_M'] = document.ngay_lapvanban_luutru.month
                    context['hhjp_0193_Y'] = document.ngay_lapvanban_luutru.year
                else:
                    context['hhjp_0193_D'] = ''
                    context['hhjp_0193_M'] = ''
                    context['hhjp_0193_Y'] = ''

                if document.donhang_luutru.nghiepdoan_donhang.loai_giayphep_nghiepdoan == 'Tổng hợp':
                    context['bs_0114_1'] = '■'
                    context['bs_0114_2'] = '□'
                elif document.donhang_luutru.nghiepdoan_donhang.loai_giayphep_nghiepdoan == 'Cụ thể':
                    context['bs_0114_1'] = '□'
                    context['bs_0114_2'] = '■'
                else:
                    context['bs_0114_1'] = '□'
                    context['bs_0114_2'] = '□'

                if document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == '1:農業関係':
                    context['bs_0110_1'] = '■'
                    context['bs_0110_2'] = '□'
                    context['bs_0110_3'] = '□'
                    context['bs_0110_4'] = '□'
                    context['bs_0110_5'] = '□'
                    context['bs_0110_6'] = '□'
                    context['bs_0110_7'] = '□'
                    context['bs_0110_8'] = ''
                elif document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == '2:漁業関係':
                    context['bs_0110_1'] = '□'
                    context['bs_0110_2'] = '■'
                    context['bs_0110_3'] = '□'
                    context['bs_0110_4'] = '□'
                    context['bs_0110_5'] = '□'
                    context['bs_0110_6'] = '□'
                    context['bs_0110_7'] = '□'
                    context['bs_0110_8'] = ''
                elif document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == '3:建設関係':
                    context['bs_0110_1'] = '□'
                    context['bs_0110_2'] = '□'
                    context['bs_0110_3'] = '■'
                    context['bs_0110_4'] = '□'
                    context['bs_0110_5'] = '□'
                    context['bs_0110_6'] = '□'
                    context['bs_0110_7'] = '□'
                    context['bs_0110_8'] = ''
                elif document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == '4:食品製造関係':
                    context['bs_0110_1'] = '□'
                    context['bs_0110_2'] = '□'
                    context['bs_0110_3'] = '□'
                    context['bs_0110_4'] = '■'
                    context['bs_0110_5'] = '□'
                    context['bs_0110_6'] = '□'
                    context['bs_0110_7'] = '□'
                    context['bs_0110_8'] = ''
                elif document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == '5:繊維・衣服関係':
                    context['bs_0110_1'] = '□'
                    context['bs_0110_2'] = '□'
                    context['bs_0110_3'] = '□'
                    context['bs_0110_4'] = '□'
                    context['bs_0110_5'] = '■'
                    context['bs_0110_6'] = '□'
                    context['bs_0110_7'] = '□'
                    context['bs_0110_8'] = ''
                elif document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == '6:機械・金属関係':
                    context['bs_0110_1'] = '□'
                    context['bs_0110_2'] = '□'
                    context['bs_0110_3'] = '□'
                    context['bs_0110_4'] = '□'
                    context['bs_0110_5'] = '□'
                    context['bs_0110_6'] = '■'
                    context['bs_0110_7'] = '□'
                    context['bs_0110_8'] = ''
                elif document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == '7:その他':
                    context['bs_0110_1'] = '□'
                    context['bs_0110_2'] = '□'
                    context['bs_0110_3'] = '□'
                    context['bs_0110_4'] = '□'
                    context['bs_0110_5'] = '□'
                    context['bs_0110_6'] = '□'
                    context['bs_0110_7'] = '■'
                    context['bs_0110_8'] = format_hoso.kiemtra(document.xinghiep_luutru.noidung_congviec_khac_xinghiep)
                else:
                    context['bs_0110_1'] = '□'
                    context['bs_0110_2'] = '□'
                    context['bs_0110_3'] = '□'
                    context['bs_0110_4'] = '□'
                    context['bs_0110_5'] = '□'
                    context['bs_0110_6'] = '□'
                    context['bs_0110_7'] = '□'
                    context['bs_0110_8'] = ''

                if document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '①商工会議所・商工会':
                    context['bs_0123_1'] = '■'
                    context['bs_0123_2'] = '□'
                    context['bs_0123_3'] = '□'
                    context['bs_0123_4'] = '□'
                    context['bs_0123_5'] = '□'
                    context['bs_0123_6'] = '□'
                    context['bs_0123_7'] = '□'
                elif document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '②中小企業団体':
                    context['bs_0123_1'] = '□'
                    context['bs_0123_2'] = '■'
                    context['bs_0123_3'] = '□'
                    context['bs_0123_4'] = '□'
                    context['bs_0123_5'] = '□'
                    context['bs_0123_6'] = '□'
                    context['bs_0123_7'] = '□'
                elif document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '③職業訓練法人':
                    context['bs_0123_1'] = '□'
                    context['bs_0123_2'] = '□'
                    context['bs_0123_3'] = '■'
                    context['bs_0123_4'] = '□'
                    context['bs_0123_5'] = '□'
                    context['bs_0123_6'] = '□'
                    context['bs_0123_7'] = '□'
                elif document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '④農業協同組合':
                    context['bs_0123_1'] = '□'
                    context['bs_0123_2'] = '□'
                    context['bs_0123_3'] = '□'
                    context['bs_0123_4'] = '■'
                    context['bs_0123_5'] = '□'
                    context['bs_0123_6'] = '□'
                    context['bs_0123_7'] = '□'
                elif document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '⑤漁業協同組合':
                    context['bs_0123_1'] = '□'
                    context['bs_0123_2'] = '□'
                    context['bs_0123_3'] = '□'
                    context['bs_0123_4'] = '□'
                    context['bs_0123_5'] = '■'
                    context['bs_0123_6'] = '□'
                    context['bs_0123_7'] = '□'
                elif document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '⑥公益社団法人・公益財団法人':
                    context['bs_0123_1'] = '□'
                    context['bs_0123_2'] = '□'
                    context['bs_0123_3'] = '□'
                    context['bs_0123_4'] = '□'
                    context['bs_0123_5'] = '□'
                    context['bs_0123_6'] = '■'
                    context['bs_0123_7'] = '□'
                elif document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '⑦その他':
                    context['bs_0123_1'] = '□'
                    context['bs_0123_2'] = '□'
                    context['bs_0123_3'] = '□'
                    context['bs_0123_4'] = '□'
                    context['bs_0123_5'] = '□'
                    context['bs_0123_6'] = '□'
                    context['bs_0123_7'] = '■'
                else:
                    context['bs_0123_1'] = '□'
                    context['bs_0123_2'] = '□'
                    context['bs_0123_3'] = '□'
                    context['bs_0123_4'] = '□'
                    context['bs_0123_5'] = '□'
                    context['bs_0123_6'] = '□'
                    context['bs_0123_7'] = '□'

                context['hhjp_0099_1'] = format_hoso.kiemtra(
                    document.donhang_luutru.nghiepdoan_donhang.so_giayphep_nghiepdoan)
                context['hhjp_0003'] = format_hoso.kiemtra(thuctapsinh.quoctich_tts.name_quoctich)
                if thuctapsinh.ngaysinh_tts:
                    context['hhjp_0004_D'] = thuctapsinh.ngaysinh_tts.day
                    context['hhjp_0004_M'] = thuctapsinh.ngaysinh_tts.month
                    context['hhjp_0004_Y'] = thuctapsinh.ngaysinh_tts.year
                else:
                    context['hhjp_0004_D'] = ''
                    context['hhjp_0004_M'] = ''
                    context['hhjp_0004_Y'] = ''

                context['bs_0058'] = format_hoso.kiemtra(thuctapsinh.noisinh_tts)
                context['hhjp_0521'] = format_hoso.kiemtra(thuctapsinh.quehuong_tts)
                context['hhjp_0345'] = thuctapsinh.nghenghiep_tts if thuctapsinh.nghenghiep_tts else ''
                if thuctapsinh.nguoikethon_tts == 'Có':
                    docdata.remove_shape(u'id="30" name="Oval 30"')
                elif thuctapsinh.nguoikethon_tts == 'Không':
                    docdata.remove_shape(u'id="29" name="Oval 29"')
                else:
                    docdata.remove_shape(u'id="30" name="Oval 30"')
                    docdata.remove_shape(u'id="29" name="Oval 29"')
                context['hhjp_0522'] = format_hoso.kiemtra(thuctapsinh.passport_so_tts)
                context['bs_0059'] = format_hoso.kiemtra(thuctapsinh.soban_xinghiep)
                context['bs_0060'] = format_hoso.kiemtra(thuctapsinh.dienthoai_xinghiep)
                context['bs_0061'] = format_hoso.convert_date(thuctapsinh.passport_hethan_tts)
                context['hhjp_0006'] = format_hoso.gender_check(thuctapsinh.gtinh_tts)
                if thuctapsinh.mucdichnhapcu_tts == 'Thực tập kỹ thuật số 1':
                    context['hhjp_0522_1'] = '■'
                    context['hhjp_0522_2'] = '□'
                    context['hhjp_0522_3'] = '□'
                elif thuctapsinh.mucdichnhapcu_tts == 'Đào tạo kỹ thuật số 2':
                    context['hhjp_0522_1'] = '□'
                    context['hhjp_0522_2'] = '■'
                    context['hhjp_0522_3'] = '□'
                elif thuctapsinh.mucdichnhapcu_tts == 'Đào tạo kỹ thuật số 3':
                    context['hhjp_0522_1'] = '□'
                    context['hhjp_0522_2'] = '□'
                    context['hhjp_0522_3'] = '■'
                else:
                    context['hhjp_0522_1'] = '□'
                    context['hhjp_0522_2'] = '□'
                    context['hhjp_0522_3'] = '□'
                context['hhjp_0902'] = format_hoso.convert_date(thuctapsinh.ngaynhapcanh_tts)
                context['bs_0062'] = format_hoso.kiemtra(thuctapsinh.hacanh_tts)
                context['bs_0063'] = format_hoso.kiemtra(thuctapsinh.luutru_tts)
                context['hhjp_0524'] = format_hoso.kiemtra(thuctapsinh.visa_tts)

                if thuctapsinh.nguoidicung_tts == 'Có':
                    docdata.remove_shape(u'id="9" name="Oval 9"')
                elif thuctapsinh.nguoidicung_tts == 'Không':
                    docdata.remove_shape(u'id="2" name="Oval 2"')
                else:
                    docdata.remove_shape(u'id="9" name="Oval 9"')
                    docdata.remove_shape(u'id="2" name="Oval 2"')

                if thuctapsinh.nguoidicung_tts == 'Có':
                    docdata.remove_shape(u'id="9" name="Oval 9"')
                elif thuctapsinh.nguoidicung_tts == 'Không':
                    docdata.remove_shape(u'id="2" name="Oval 2"')
                else:
                    docdata.remove_shape(u'id="9" name="Oval 9"')
                    docdata.remove_shape(u'id="2" name="Oval 2"')

                if thuctapsinh.lichsunhapcu_tts == 'Có':
                    docdata.remove_shape(u'id="46" name="Oval 46"')
                    context['bs_0065'] = format_hoso.kiemtra(thuctapsinh.solan_lichsunhapcu_tts)

                    if thuctapsinh.ngaydinhat_tts:
                        context['bs_0066_D'] = thuctapsinh.ngaydinhat_tts.day
                        context['bs_0066_M'] = thuctapsinh.ngaydinhat_tts.month
                        context['bs_0066_Y'] = thuctapsinh.ngaydinhat_tts.year
                    else:
                        context['bs_0066_D'] = ''
                        context['bs_0066_M'] = ''
                        context['bs_0066_Y'] = ''

                    if thuctapsinh.onhatden_tts:
                        context['bs_0067_D'] = thuctapsinh.onhatden_tts.day
                        context['bs_0067_M'] = thuctapsinh.onhatden_tts.month
                        context['bs_0067_Y'] = thuctapsinh.onhatden_tts.year
                    else:
                        context['bs_0067_D'] = ''
                        context['bs_0067_M'] = ''
                        context['bs_0067_Y'] = ''
                elif thuctapsinh.lichsunhapcu_tts == 'Không':
                    docdata.remove_shape(u'id="45" name="Oval 45"')

                if thuctapsinh.toipham_tts == 'Có':
                    context['bs_0068_1'] = '◼ 有'
                    context['bs_0068_2'] = '◻ 無'
                    context['bs_0069'] = format_hoso.kiemtra(thuctapsinh.noidung_toipham)
                elif thuctapsinh.toipham_tts == 'Không':
                    context['bs_0068_1'] = '◻ 有'
                    context['bs_0068_2'] = '◼ 無'
                    context['bs_0069'] = ''
                else:
                    context['bs_0068_1'] = '◻ 有'
                    context['bs_0068_2'] = '◻ 無'
                    context['bs_0069'] = ''

                if thuctapsinh.cokhong_tungbi_trucxuat_tts == 'Có':
                    docdata.remove_shape(u'id="42" name="Oval 42"')
                    context['bs_0071'] = format_hoso.kiemtra(thuctapsinh.solan_tungbi_trucxuat_tts)
                    context['bs_0072'] = format_hoso.convert_date(thuctapsinh.tungbi_trucxuat_tts)
                elif thuctapsinh.cokhong_tungbi_trucxuat_tts == 'Không':
                    docdata.remove_shape(u'id="41" name="Oval 41"')
                    context['bs_0071'] = ''
                    context['bs_0072'] = '年　月　日'
                else:
                    docdata.remove_shape(u'id="42" name="Oval 42"')
                    docdata.remove_shape(u'id="41" name="Oval 41"')
                    context['bs_0071'] = ''
                    context['bs_0072'] = '年　月　日'

                if thuctapsinh.nguoithan_tts == 'Có':
                    docdata.remove_shape(u'id="44" name="Oval 44"')
                    i = 1
                    for item in thuctapsinh.nguoithan_onhat_tts:
                        if i > len(thuctapsinh.nguoithan_onhat_tts) or i == 5:
                            break
                        else:
                            context['bs_0074_%d' % i] = format_hoso.kiemtra(item.quanhe_nguoithan)
                            context['bs_0075_%d' % i] = format_hoso.kiemtra(item.hoten_nguoithan)
                            context['bs_0076_%d' % i] = format_hoso.convert_date(item.ngaysinh_nguoithan)
                            context['bs_0077_%d' % i] = format_hoso.kiemtra(item.quoctich_nguoithan.name_quoctich)
                            if item.kehoach_songcung == 'Có':
                                context['bs_0078_%d' % i] = '有'
                            elif item.kehoach_songcung == 'Không':
                                context['bs_0078_%d' % i] = '無'
                            context['bs_0079_%d' % i] = format_hoso.kiemtra(item.diadiem_truonghoc)
                            context['bs_0080_%d' % i] = format_hoso.kiemtra(item.sothe_cutru)
                            i += 1
                elif thuctapsinh.nguoithan_tts == 'Không':
                    docdata.remove_shape(u'id="43" name="Oval 43"')
                else:
                    docdata.remove_shape(u'id="44" name="Oval 44"')
                    docdata.remove_shape(u'id="43" name="Oval 43"')

                if thuctapsinh.nguoithan_tts == 'Có':
                    docdata.remove_shape(u'id="44" name="Oval 44"')
                    i = 1
                    for item in thuctapsinh.nguoithan_onhat_tts:
                        if i > len(thuctapsinh.nguoithan_onhat_tts) or i == 6:
                            break
                        else:
                            if i == 1:
                                context['bs_0074_1'] = format_hoso.kiemtra(item.quanhe_nguoithan)
                                context['bs_0075_1'] = format_hoso.kiemtra(item.hoten_nguoithan)
                                context['bs_0076_1'] = format_hoso.convert_date(item.ngaysinh_nguoithan)
                                context['bs_0077_1'] = format_hoso.kiemtra(item.quoctich_nguoithan.name_quoctich)
                                if item.kehoach_songcung == 'Có':
                                    docdata.remove_shape(u'id="87" name="Oval 87"')
                                elif item.kehoach_songcung == 'Không':
                                    docdata.remove_shape(u'id="80" name="Oval 80"')
                                else:
                                    docdata.remove_shape(u'id="87" name="Oval 87"')
                                    docdata.remove_shape(u'id="80" name="Oval 80"')

                                if len(thuctapsinh.nguoithan_onhat_tts) == 1:
                                    docdata.remove_shape(u'id="88" name="Oval 88"')
                                    docdata.remove_shape(u'id="91" name="Oval 91"')
                                    docdata.remove_shape(u'id="89" name="Oval 89"')
                                    docdata.remove_shape(u'id="90" name="Oval 90"')
                                    docdata.remove_shape(u'id="92" name="Oval 92"')
                                    docdata.remove_shape(u'id="93" name="Oval 93"')
                                context['bs_0079_1'] = format_hoso.kiemtra(item.diadiem_truonghoc)
                                context['bs_0080_1'] = format_hoso.kiemtra(item.sothe_cutru)
                            elif i == 2:
                                context['bs_0074_2'] = format_hoso.kiemtra(item.quanhe_nguoithan)
                                context['bs_0075_2'] = format_hoso.kiemtra(item.hoten_nguoithan)
                                context['bs_0076_2'] = format_hoso.convert_date(item.ngaysinh_nguoithan)
                                context['bs_0077_2'] = format_hoso.kiemtra(item.quoctich_nguoithan.name_quoctich)
                                if item.kehoach_songcung == 'Có':
                                    docdata.remove_shape(u'id="91" name="Oval 91"')
                                elif item.kehoach_songcung == 'Không':
                                    docdata.remove_shape(u'id="88" name="Oval 88"')
                                else:
                                    docdata.remove_shape(u'id="88" name="Oval 88"')
                                    docdata.remove_shape(u'id="91" name="Oval 91"')
                                if len(thuctapsinh.nguoithan_onhat_tts) == 2:
                                    docdata.remove_shape(u'id="89" name="Oval 89"')
                                    docdata.remove_shape(u'id="90" name="Oval 90"')
                                    docdata.remove_shape(u'id="92" name="Oval 92"')
                                    docdata.remove_shape(u'id="93" name="Oval 93"')
                                context['bs_0079_2'] = format_hoso.kiemtra(item.diadiem_truonghoc)
                                context['bs_0080_2'] = format_hoso.kiemtra(item.sothe_cutru)
                            elif i == 3:
                                context['bs_0074_3'] = format_hoso.kiemtra(item.quanhe_nguoithan)
                                context['bs_0075_3'] = format_hoso.kiemtra(item.hoten_nguoithan)
                                context['bs_0076_3'] = format_hoso.convert_date(item.ngaysinh_nguoithan)
                                context['bs_0077_3'] = format_hoso.kiemtra(item.quoctich_nguoithan.name_quoctich)
                                if item.kehoach_songcung == 'Có':
                                    docdata.remove_shape(u'id="89" name="Oval 89"')
                                elif item.kehoach_songcung == 'Không':
                                    docdata.remove_shape(u'id="90" name="Oval 90"')
                                else:
                                    docdata.remove_shape(u'id="89" name="Oval 89"')
                                    docdata.remove_shape(u'id="90" name="Oval 90"')

                                if len(thuctapsinh.nguoithan_onhat_tts) == 3:
                                    docdata.remove_shape(u'id="92" name="Oval 92"')
                                    docdata.remove_shape(u'id="93" name="Oval 93"')
                                context['bs_0079_3'] = format_hoso.kiemtra(item.diadiem_truonghoc)
                                context['bs_0080_3'] = format_hoso.kiemtra(item.sothe_cutru)
                            elif i == 4:
                                context['bs_0074_4'] = format_hoso.kiemtra(item.quanhe_nguoithan)
                                context['bs_0075_4'] = format_hoso.kiemtra(item.hoten_nguoithan)
                                context['bs_0076_4'] = format_hoso.convert_date(item.ngaysinh_nguoithan)
                                context['bs_0077_4'] = format_hoso.kiemtra(item.quoctich_nguoithan.name_quoctich)
                                if item.kehoach_songcung == 'Có':
                                    docdata.remove_shape(u'id="93" name="Oval 93"')
                                elif item.kehoach_songcung == 'Không':
                                    docdata.remove_shape(u'id="92" name="Oval 92"')
                                else:
                                    docdata.remove_shape(u'id="92" name="Oval 92"')
                                    docdata.remove_shape(u'id="93" name="Oval 93"')
                                context['bs_0079_4'] = format_hoso.kiemtra(item.diadiem_truonghoc)
                                context['bs_0080_4'] = format_hoso.kiemtra(item.sothe_cutru)
                            else:
                                break
                            i += 1
                elif thuctapsinh.nguoithan_tts == 'Không':
                    docdata.remove_shape(u'id="43" name="Oval 43"')
                    docdata.remove_shape(u'id="87" name="Oval 87"')
                    docdata.remove_shape(u'id="80" name="Oval 80"')
                    docdata.remove_shape(u'id="88" name="Oval 88"')
                    docdata.remove_shape(u'id="91" name="Oval 91"')
                    docdata.remove_shape(u'id="89" name="Oval 89"')
                    docdata.remove_shape(u'id="90" name="Oval 90"')
                    docdata.remove_shape(u'id="92" name="Oval 92"')
                    docdata.remove_shape(u'id="93" name="Oval 93"')
                else:
                    docdata.remove_shape(u'id="44" name="Oval 44"')
                    docdata.remove_shape(u'id="43" name="Oval 43"')

                context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_luutru.ten_han_xnghiep)
                context['hhjp_0011_1'] = format_hoso.kiemtra(
                    document.donhang_luutru.nghiepdoan_donhang.diachi_nghiepdoan)
                context['hhjp_0012'] = format_hoso.kiemtra(document.xinghiep_luutru.sdt_xnghiep)
                context['hhjp_0097'] = format_hoso.kiemtra(
                    document.donhang_luutru.nghiepdoan_donhang.ten_han_nghiepdoan)
                context['hhjp_0099'] = format_hoso.kiemtra(
                    document.donhang_luutru.nghiepdoan_donhang.dienthoai_nghiepdoan)

                context['bs_0074'] = format_hoso.kiemtra(thuctapsinh.nam_vaocty_1)
                context['bs_0075'] = format_hoso.kiemtra(thuctapsinh.thang_vaocty_1)
                context['bs_0076'] = format_hoso.kiemtra(thuctapsinh.nam_thoiviec_1)
                context['bs_0077'] = format_hoso.kiemtra(thuctapsinh.thang_thoiviec_1)
                context['bs_0078'] = format_hoso.kiemtra(thuctapsinh.quatrinh_congtac_1)

                context['bs_0079'] = format_hoso.kiemtra(thuctapsinh.nam_vaocty_2)
                context['bs_0080'] = format_hoso.kiemtra(thuctapsinh.thang_vaocty_2)
                context['bs_0081'] = format_hoso.kiemtra(thuctapsinh.nam_thoiviec_2)
                context['bs_0082'] = format_hoso.kiemtra(thuctapsinh.thang_thoiviec_2)
                context['bs_0083'] = format_hoso.kiemtra(thuctapsinh.quatrinh_congtac_2)

                context['bs_0084'] = format_hoso.kiemtra(thuctapsinh.nam_vaocty_3)
                context['bs_0085'] = format_hoso.kiemtra(thuctapsinh.thang_vaocty_3)
                context['bs_0086'] = format_hoso.kiemtra(thuctapsinh.nam_thoiviec_3)
                context['bs_0087'] = format_hoso.kiemtra(thuctapsinh.thang_thoiviec_3)
                context['bs_0088'] = format_hoso.kiemtra(thuctapsinh.quatrinh_congtac_3)

                context['bs_0089'] = format_hoso.kiemtra(thuctapsinh.nam_vaocty_4)
                context['bs_0090'] = format_hoso.kiemtra(thuctapsinh.thang_vaocty_4)
                context['bs_0091'] = format_hoso.kiemtra(thuctapsinh.nam_thoiviec_4)
                context['bs_0092'] = format_hoso.kiemtra(thuctapsinh.thang_thoiviec_4)
                context['bs_0093'] = format_hoso.kiemtra(thuctapsinh.quatrinh_congtac_4)

                context['bs_0094'] = format_hoso.kiemtra(thuctapsinh.nam_vaocty_5)
                context['bs_0095'] = format_hoso.kiemtra(thuctapsinh.thang_vaocty_5)
                context['bs_0096'] = format_hoso.kiemtra(thuctapsinh.nam_thoiviec_5)
                context['bs_0097'] = format_hoso.kiemtra(thuctapsinh.thang_thoiviec_5)
                context['bs_0098'] = format_hoso.kiemtra(thuctapsinh.quatrinh_congtac_5)

                context['bs_0099'] = format_hoso.kiemtra(thuctapsinh.nam_vaocty_6)
                context['bs_0100'] = format_hoso.kiemtra(thuctapsinh.thang_vaocty_6)
                context['bs_0101'] = format_hoso.kiemtra(thuctapsinh.nam_thoiviec_6)
                context['bs_0102'] = format_hoso.kiemtra(thuctapsinh.thang_thoiviec_6)
                context['bs_0103'] = format_hoso.kiemtra(thuctapsinh.quatrinh_congtac_6)

                context['bs_0104'] = format_hoso.kiemtra(thuctapsinh.hoten_nguoinopdon)
                context['bs_0105'] = format_hoso.kiemtra(thuctapsinh.quanhe_thuctapsinh_nguoinopdon)
                context['bs_0106'] = format_hoso.kiemtra(thuctapsinh.diachi_nguoinopdon)
                context['bs_0107'] = format_hoso.kiemtra(thuctapsinh.dienthoai_nguoinopdon)
                context['bs_0108'] = format_hoso.kiemtra(thuctapsinh.didong_nguoinopdon)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_46(self, ma_thuctapsinh, document):
        print('46')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_46")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            context = {}
            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            if document.giaidoan_luutru == '2 go':
                context['hhjp_0911'] = format_hoso.kiemtra(thuctapsinh.sochungnhan_namhai)
                context['b1'] = '■'
                context['b2'] = '□'
                if thuctapsinh.ngaychungnhan_namhai:
                    context['hhjp_0582_Y'] = thuctapsinh.ngaychungnhan_namhai.year
                    context['hhjp_0582_M'] = thuctapsinh.ngaychungnhan_namhai.month
                    context['hhjp_0582_D'] = thuctapsinh.ngaychungnhan_namhai.day
                else:
                    context['hhjp_0582_Y'] = ''
                    context['hhjp_0582_M'] = ''
                    context['hhjp_0582_D'] = ''

                if thuctapsinh.batdau_kehoach_namhai:
                    context['hhjp_0596_Y'] = thuctapsinh.batdau_kehoach_namhai.year
                    context['hhjp_0596_M'] = thuctapsinh.batdau_kehoach_namhai.month
                    context['hhjp_0596_D'] = thuctapsinh.batdau_kehoach_namhai.day
                else:
                    context['hhjp_0596_Y'] = ''
                    context['hhjp_0596_M'] = ''
                    context['hhjp_0596_D'] = ''

                if thuctapsinh.ketthuc_kehoach_namhai:
                    context['hhjp_0597_Y'] = thuctapsinh.ketthuc_kehoach_namhai.year
                    context['hhjp_0597_M'] = thuctapsinh.ketthuc_kehoach_namhai.month
                    context['hhjp_0597_D'] = thuctapsinh.ketthuc_kehoach_namhai.day
                else:
                    context['hhjp_0597_Y'] = ''
                    context['hhjp_0597_M'] = ''
                    context['hhjp_0597_D'] = ''
            elif document.giaidoan_luutru == '3 go':
                context['b1'] = '□'
                context['b2'] = '■'
                context['hhjp_0911'] = format_hoso.kiemtra(thuctapsinh.sochungnhan_namba)
                if thuctapsinh.ngaychungnhan_namba:
                    context['hhjp_0582_Y'] = thuctapsinh.ngaychungnhan_namba.year
                    context['hhjp_0582_M'] = thuctapsinh.ngaychungnhan_namba.month
                    context['hhjp_0582_D'] = thuctapsinh.ngaychungnhan_namba.day
                else:
                    context['hhjp_0582_Y'] = ''
                    context['hhjp_0582_M'] = ''
                    context['hhjp_0582_D'] = ''

                if thuctapsinh.batdau_kehoach_namba:
                    context['hhjp_0596_Y'] = thuctapsinh.batdau_kehoach_namba.year
                    context['hhjp_0596_M'] = thuctapsinh.batdau_kehoach_namba.month
                    context['hhjp_0596_D'] = thuctapsinh.batdau_kehoach_namba.day
                else:
                    context['hhjp_0596_Y'] = ''
                    context['hhjp_0596_M'] = ''
                    context['hhjp_0596_D'] = ''

                if thuctapsinh.ketthuc_kehoach_namba:
                    context['hhjp_0597_Y'] = thuctapsinh.ketthuc_kehoach_namba.year
                    context['hhjp_0597_M'] = thuctapsinh.ketthuc_kehoach_namba.month
                    context['hhjp_0597_D'] = thuctapsinh.ketthuc_kehoach_namba.day
                else:
                    context['hhjp_0597_Y'] = ''
                    context['hhjp_0597_M'] = ''
                    context['hhjp_0597_D'] = ''
            else:
                context['b1'] = '□'
                context['b2'] = '□'
                context['hhjp_0596_Y'] = ''
                context['hhjp_0596_M'] = ''
                context['hhjp_0596_D'] = ''
                context['hhjp_0597_Y'] = ''
                context['hhjp_0597_M'] = ''
                context['hhjp_0597_D'] = ''
                context['hhjp_0911'] = ''
                context['hhjp_0582_Y'] = ''
                context['hhjp_0582_M'] = ''
                context['hhjp_0582_D'] = ''

            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_luutru.ten_han_xnghiep)
            context['bs_0109'] = format_hoso.kiemtra(document.xinghiep_luutru.phapnhan_xinghiep)
            context['hhjp_0012'] = format_hoso.kiemtra(document.xinghiep_luutru.sdt_xnghiep)
            context['hhjp_0811'] = format_hoso.kiemtra(document.xinghiep_luutru.so_chapnhan_daotao)
            context['hhjp_0023'] = format_hoso.place_value(document.xinghiep_luutru.sovon_xnghiep)
            context['hhjp_0024'] = format_hoso.place_value(document.xinghiep_luutru.doanhthu_namtruoc_xnghiep)
            context['hhjp_0097'] = format_hoso.kiemtra(document.donhang_luutru.nghiepdoan_donhang.ten_han_nghiepdoan)
            context['hhjp_0912'] = format_hoso.kiemtra(
                document.donhang_luutru.nghiepdoan_donhang.so_phapnhan_nghiepdoan)
            context['bs_0111'] = format_hoso.place_value(document.xinghiep_luutru.so_nvien_xnghiep)
            context['bs_0113'] = format_hoso.place_value(
                document.donhang_luutru.nghiepdoan_donhang.so_nvien_chinhthuc_nghiepdoan)
            context['bs_0112'] = format_hoso.place_value(document.xinghiep_luutru.nvien_nuocngoai_xnghiep)

            if document.donhang_luutru.nghiepdoan_donhang.ngay_capphep_nghiepdoan:
                context[
                    'bs_0114_Y'] = document.donhang_luutru.nghiepdoan_donhang.ngay_capphep_nghiepdoan.year
                context[
                    'bs_0114_M'] = document.donhang_luutru.nghiepdoan_donhang.ngay_capphep_nghiepdoan.month
                context[
                    'bs_0114_D'] = document.donhang_luutru.nghiepdoan_donhang.ngay_capphep_nghiepdoan.day
            else:
                context['bs_0114_Y'] = ''
                context['bs_0114_M'] = ''
                context['bs_0114_D'] = ''

            if document.donhang_luutru.nghiepdoan_donhang.batdau_hieuluc_giayphep_nghiepdoan:
                context[
                    'bs_0115_Y'] = document.donhang_luutru.nghiepdoan_donhang.batdau_hieuluc_giayphep_nghiepdoan.year
                context[
                    'bs_0115_M'] = document.donhang_luutru.nghiepdoan_donhang.batdau_hieuluc_giayphep_nghiepdoan.month
                context[
                    'bs_0115_D'] = document.donhang_luutru.nghiepdoan_donhang.batdau_hieuluc_giayphep_nghiepdoan.day
            else:
                context['bs_0115_Y'] = ''
                context['bs_0115_M'] = ''
                context['bs_0115_D'] = ''

            if document.donhang_luutru.nghiepdoan_donhang.ketthuc_hieuluc_giayphep_nghiepdoan:
                context[
                    'bs_0116_Y'] = document.donhang_luutru.nghiepdoan_donhang.ketthuc_hieuluc_giayphep_nghiepdoan.year
                context[
                    'bs_0116_M'] = document.donhang_luutru.nghiepdoan_donhang.ketthuc_hieuluc_giayphep_nghiepdoan.month
                context[
                    'bs_0116_D'] = document.donhang_luutru.nghiepdoan_donhang.ketthuc_hieuluc_giayphep_nghiepdoan.day
            else:
                context['bs_0116_Y'] = ''
                context['bs_0116_M'] = ''
                context['bs_0116_D'] = ''

            context[
                'hhjp_0103'] = format_hoso.kiemtra(
                document.donhang_luutru.nghiepdoan_donhang.ten_daidien_han_nghiepdoan)
            context[
                'hhjp_0103_1'] = format_hoso.kiemtra(
                document.donhang_luutru.nghiepdoan_donhang.chucvu_daidien_phienam_nghiepdoan)

            if document.donhang_luutru.nghiepdoan_donhang.loai_giayphep_nghiepdoan == 'Tổng hợp':
                context['bs_0114_1'] = '■'
                context['bs_0114_2'] = '□'
            elif document.donhang_luutru.nghiepdoan_donhang.loai_giayphep_nghiepdoan == 'Cụ thể':
                context['bs_0114_1'] = '□'
                context['bs_0114_2'] = '■'
            else:
                context['bs_0114_1'] = '□'
                context['bs_0114_2'] = '□'

            if document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == 1:
                context['bs_0110_1'] = '■'
                context['bs_0110_2'] = '□'
                context['bs_0110_3'] = '□'
                context['bs_0110_4'] = '□'
                context['bs_0110_5'] = '□'
                context['bs_0110_6'] = '□'
                context['bs_0110_7'] = '□'
                context['bs_0110_8'] = ''
            elif document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == 2:
                context['bs_0110_1'] = '□'
                context['bs_0110_2'] = '■'
                context['bs_0110_3'] = '□'
                context['bs_0110_4'] = '□'
                context['bs_0110_5'] = '□'
                context['bs_0110_6'] = '□'
                context['bs_0110_7'] = '□'
                context['bs_0110_8'] = ''
            elif document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == 3:
                context['bs_0110_1'] = '□'
                context['bs_0110_2'] = '□'
                context['bs_0110_3'] = '■'
                context['bs_0110_4'] = '□'
                context['bs_0110_5'] = '□'
                context['bs_0110_6'] = '□'
                context['bs_0110_7'] = '□'
                context['bs_0110_8'] = ''
            elif document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == 4:
                context['bs_0110_1'] = '□'
                context['bs_0110_2'] = '□'
                context['bs_0110_3'] = '□'
                context['bs_0110_4'] = '■'
                context['bs_0110_5'] = '□'
                context['bs_0110_6'] = '□'
                context['bs_0110_7'] = '□'
                context['bs_0110_8'] = ''
            elif document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == 5:
                context['bs_0110_1'] = '□'
                context['bs_0110_2'] = '□'
                context['bs_0110_3'] = '□'
                context['bs_0110_4'] = '□'
                context['bs_0110_5'] = '■'
                context['bs_0110_6'] = '□'
                context['bs_0110_7'] = '□'
                context['bs_0110_8'] = ''
            elif document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == 6:
                context['bs_0110_1'] = '□'
                context['bs_0110_2'] = '□'
                context['bs_0110_3'] = '□'
                context['bs_0110_4'] = '□'
                context['bs_0110_5'] = '□'
                context['bs_0110_6'] = '■'
                context['bs_0110_7'] = '□'
                context['bs_0110_8'] = ''
            elif document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == 7:
                context['bs_0110_1'] = '□'
                context['bs_0110_2'] = '□'
                context['bs_0110_3'] = '□'
                context['bs_0110_4'] = '□'
                context['bs_0110_5'] = '□'
                context['bs_0110_6'] = '□'
                context['bs_0110_7'] = '■'
                context['bs_0110_8'] = format_hoso.kiemtra(document.xinghiep_luutru.noidung_congviec_khac_xinghiep)
            else:
                context['bs_0110_1'] = '□'
                context['bs_0110_2'] = '□'
                context['bs_0110_3'] = '□'
                context['bs_0110_4'] = '□'
                context['bs_0110_5'] = '□'
                context['bs_0110_6'] = '□'
                context['bs_0110_7'] = '□'
                context['bs_0110_8'] = ''

            if document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '①商工会議所・商工会':
                context['bs_0123_1'] = '■'
                context['bs_0123_2'] = '□'
                context['bs_0123_3'] = '□'
                context['bs_0123_4'] = '□'
                context['bs_0123_5'] = '□'
                context['bs_0123_6'] = '□'
                context['bs_0123_7'] = '□'
            elif document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '②中小企業団体':
                context['bs_0123_1'] = '□'
                context['bs_0123_2'] = '■'
                context['bs_0123_3'] = '□'
                context['bs_0123_4'] = '□'
                context['bs_0123_5'] = '□'
                context['bs_0123_6'] = '□'
                context['bs_0123_7'] = '□'
            elif document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '③職業訓練法人':
                context['bs_0123_1'] = '□'
                context['bs_0123_2'] = '□'
                context['bs_0123_3'] = '■'
                context['bs_0123_4'] = '□'
                context['bs_0123_5'] = '□'
                context['bs_0123_6'] = '□'
                context['bs_0123_7'] = '□'
            elif document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '④農業協同組合':
                context['bs_0123_1'] = '□'
                context['bs_0123_2'] = '□'
                context['bs_0123_3'] = '□'
                context['bs_0123_4'] = '■'
                context['bs_0123_5'] = '□'
                context['bs_0123_6'] = '□'
                context['bs_0123_7'] = '□'
            elif document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '⑤漁業協同組合':
                context['bs_0123_1'] = '□'
                context['bs_0123_2'] = '□'
                context['bs_0123_3'] = '□'
                context['bs_0123_4'] = '□'
                context['bs_0123_5'] = '■'
                context['bs_0123_6'] = '□'
                context['bs_0123_7'] = '□'
            elif document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '⑥公益社団法人・公益財団法人':
                context['bs_0123_1'] = '□'
                context['bs_0123_2'] = '□'
                context['bs_0123_3'] = '□'
                context['bs_0123_4'] = '□'
                context['bs_0123_5'] = '□'
                context['bs_0123_6'] = '■'
                context['bs_0123_7'] = '□'
            elif document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '⑦その他':
                context['bs_0123_1'] = '□'
                context['bs_0123_2'] = '□'
                context['bs_0123_3'] = '□'
                context['bs_0123_4'] = '□'
                context['bs_0123_5'] = '□'
                context['bs_0123_6'] = '□'
                context['bs_0123_7'] = '■'
            else:
                context['bs_0123_1'] = '□'
                context['bs_0123_2'] = '□'
                context['bs_0123_3'] = '□'
                context['bs_0123_4'] = '□'
                context['bs_0123_5'] = '□'
                context['bs_0123_6'] = '□'
                context['bs_0123_7'] = '□'

            context['hhjp_0098'] = format_hoso.kiemtra(
                document.donhang_luutru.nghiepdoan_donhang.diachi_nghiepdoan)

            context['hhjp_0099'] = format_hoso.kiemtra(
                document.donhang_luutru.nghiepdoan_donhang.dienthoai_nghiepdoan)
            context['hhjp_0003'] = format_hoso.kiemtra(thuctapsinh.quoctich_tts.name_quoctich)
            if thuctapsinh.ngaysinh_tts:
                context['hhjp_0004_D'] = thuctapsinh.ngaysinh_tts.day
                context['hhjp_0004_M'] = thuctapsinh.ngaysinh_tts.month
                context['hhjp_0004_Y'] = thuctapsinh.ngaysinh_tts.year
            else:
                context['hhjp_0004_D'] = ''
                context['hhjp_0004_M'] = ''
                context['hhjp_0004_Y'] = ''

            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            context['bs_0058'] = format_hoso.kiemtra(thuctapsinh.noisinh_tts)
            context['hhjp_0521'] = format_hoso.kiemtra(thuctapsinh.quehuong_tts)
            context['hhjp_0345'] = thuctapsinh.nghenghiep_tts if thuctapsinh.nghenghiep_tts else ''
            context['hhjp_0011_2'] = format_hoso.kiemtra(
                document.donhang_luutru.donhang_daingo.tennha_daingo_daotao_nhao) + '     ' + format_hoso.kiemtra(
                document.donhang_luutru.donhang_daingo.diachi_daingo_daotao_nhao)
            context['hhjp_0011_1'] = format_hoso.kiemtra(document.xinghiep_luutru.diachi_han_xnghiep)
            if thuctapsinh.nguoikethon_tts == 'Có':
                docdata.remove_shape(u'id="20" name="Oval 20"')
            elif thuctapsinh.nguoikethon_tts == 'Không':
                docdata.remove_shape(u'id="9" name="Oval 9"')
            else:
                docdata.remove_shape(u'id="20" name="Oval 20"')
                docdata.remove_shape(u'id="9" name="Oval 9"')
            context['hhjp_0522'] = format_hoso.kiemtra(thuctapsinh.passport_so_tts)
            context['bs_0059'] = format_hoso.kiemtra(thuctapsinh.soban_tts)
            context['bs_0060'] = format_hoso.kiemtra(thuctapsinh.dienthoai_tts)

            if thuctapsinh.passport_hethan_tts:
                context['bs_0061_D'] = thuctapsinh.passport_hethan_tts.day
                context['bs_0061_M'] = thuctapsinh.passport_hethan_tts.month
                context['bs_0061_Y'] = thuctapsinh.passport_hethan_tts.year
            else:
                context['bs_0061_D'] = ''
                context['bs_0061_M'] = ''
                context['bs_0061_Y'] = ''

            context['hhjp_0006'] = format_hoso.gender_check(thuctapsinh.gtinh_tts)
            context['bs_0117'] = format_hoso.kiemtra(thuctapsinh.tinhtrang_cutru_giahan)
            context['bs_0118'] = format_hoso.kiemtra(thuctapsinh.tinhtrang_luutru_giahan)

            if thuctapsinh.hethan_luutru_giahan:
                context['bs_0119_D'] = thuctapsinh.hethan_luutru_giahan.day
                context['bs_0119_M'] = thuctapsinh.hethan_luutru_giahan.month
                context['bs_0119_Y'] = thuctapsinh.hethan_luutru_giahan.year
            else:
                context['bs_0119_D'] = ''
                context['bs_0119_M'] = ''
                context['bs_0119_Y'] = ''

            context['bs_0120'] = format_hoso.kiemtra(thuctapsinh.sothe_luutru_giahan)
            context['bs_0121'] = format_hoso.kiemtra(thuctapsinh.thoigan_luutru_giahan)
            context['bs_0122'] = format_hoso.kiemtra(thuctapsinh.lydo_doimoi_giahan)

            if thuctapsinh.toipham_tts == 'Có':
                docdata.remove_shape(u'id="22" name="Oval 22"')
                context['bs_0069'] = format_hoso.kiemtra(thuctapsinh.noidung_toipham)
            elif thuctapsinh.toipham_tts == 'Không':
                docdata.remove_shape(u'id="21" name="Oval 21"')
                context['bs_0069'] = ''
            else:
                docdata.remove_shape(u'id="22" name="Oval 22"')
                docdata.remove_shape(u'id="21" name="Oval 21"')
                context['bs_0069'] = ''

            if thuctapsinh.nguoithan_tts == 'Có':
                docdata.remove_shape(u'id="24" name="Oval 24"')
                i = 1
                for item in thuctapsinh.nguoithan_onhat_tts:
                    if i > len(thuctapsinh.nguoithan_onhat_tts) or i == 6:
                        break
                    else:
                        if i == 1:
                            context['bs_0074_1'] = format_hoso.kiemtra(item.quanhe_nguoithan)
                            context['bs_0075_1'] = format_hoso.kiemtra(item.hoten_nguoithan)
                            context['bs_0076_1'] = format_hoso.convert_date(item.ngaysinh_nguoithan)
                            context['bs_0077_1'] = format_hoso.kiemtra(item.quoctich_nguoithan.name_quoctich)
                            if item.kehoach_songcung == 'Có':
                                docdata.remove_shape(u'id="68" name="Oval 68"')
                            elif item.kehoach_songcung == 'Không':
                                docdata.remove_shape(u'id="67" name="Oval 67"')
                            else:
                                docdata.remove_shape(u'id="68" name="Oval 68"')
                                docdata.remove_shape(u'id="67" name="Oval 67"')

                            if len(thuctapsinh.nguoithan_onhat_tts) == 1:
                                docdata.remove_shape(u'id="69" name="Oval 69"')
                                docdata.remove_shape(u'id="70" name="Oval 70"')
                                docdata.remove_shape(u'id="72" name="Oval 72"')
                                docdata.remove_shape(u'id="73" name="Oval 73"')
                                docdata.remove_shape(u'id="74" name="Oval 74"')
                                docdata.remove_shape(u'id="71" name="Oval 71"')
                                docdata.remove_shape(u'id="75" name="Oval 75"')
                                docdata.remove_shape(u'id="76" name="Oval 76"')
                                docdata.remove_shape(u'id="77" name="Oval 77"')
                                docdata.remove_shape(u'id="78" name="Oval 78"')
                            context['bs_0079_1'] = format_hoso.kiemtra(item.diadiem_truonghoc)
                            context['bs_0080_1'] = format_hoso.kiemtra(item.sothe_cutru)
                        elif i == 2:
                            context['bs_0074_2'] = format_hoso.kiemtra(item.quanhe_nguoithan)
                            context['bs_0075_2'] = format_hoso.kiemtra(item.hoten_nguoithan)
                            context['bs_0076_2'] = format_hoso.convert_date(item.ngaysinh_nguoithan)
                            context['bs_0077_2'] = format_hoso.kiemtra(item.quoctich_nguoithan.name_quoctich)
                            if item.kehoach_songcung == 'Có':
                                docdata.remove_shape(u'id="70" name="Oval 70"')
                            elif item.kehoach_songcung == 'Không':
                                docdata.remove_shape(u'id="69" name="Oval 69"')
                            else:
                                docdata.remove_shape(u'id="69" name="Oval 69"')
                                docdata.remove_shape(u'id="70" name="Oval 70"')
                            if len(thuctapsinh.nguoithan_onhat_tts) == 2:
                                docdata.remove_shape(u'id="72" name="Oval 72"')
                                docdata.remove_shape(u'id="73" name="Oval 73"')
                                docdata.remove_shape(u'id="74" name="Oval 74"')
                                docdata.remove_shape(u'id="71" name="Oval 71"')
                                docdata.remove_shape(u'id="75" name="Oval 75"')
                                docdata.remove_shape(u'id="76" name="Oval 76"')
                                docdata.remove_shape(u'id="77" name="Oval 77"')
                                docdata.remove_shape(u'id="78" name="Oval 78"')
                            context['bs_0079_2'] = format_hoso.kiemtra(item.diadiem_truonghoc)
                            context['bs_0080_2'] = format_hoso.kiemtra(item.sothe_cutru)
                        elif i == 3:
                            context['bs_0074_3'] = format_hoso.kiemtra(item.quanhe_nguoithan)
                            context['bs_0075_3'] = format_hoso.kiemtra(item.hoten_nguoithan)
                            context['bs_0076_3'] = format_hoso.convert_date(item.ngaysinh_nguoithan)
                            context['bs_0077_3'] = format_hoso.kiemtra(item.quoctich_nguoithan.name_quoctich)
                            if item.kehoach_songcung == 'Có':
                                docdata.remove_shape(u'id="72" name="Oval 72"')
                            elif item.kehoach_songcung == 'Không':
                                docdata.remove_shape(u'id="71" name="Oval 71"')
                            else:
                                docdata.remove_shape(u'id="72" name="Oval 72"')
                                docdata.remove_shape(u'id="71" name="Oval 71"')

                            if len(thuctapsinh.nguoithan_onhat_tts) == 3:
                                docdata.remove_shape(u'id="73" name="Oval 73"')
                                docdata.remove_shape(u'id="74" name="Oval 74"')
                                docdata.remove_shape(u'id="75" name="Oval 75"')
                                docdata.remove_shape(u'id="76" name="Oval 76"')
                                docdata.remove_shape(u'id="77" name="Oval 77"')
                                docdata.remove_shape(u'id="78" name="Oval 78"')
                            context['bs_0079_3'] = format_hoso.kiemtra(item.diadiem_truonghoc)
                            context['bs_0080_3'] = format_hoso.kiemtra(item.sothe_cutru)
                        elif i == 4:
                            context['bs_0074_4'] = format_hoso.kiemtra(item.quanhe_nguoithan)
                            context['bs_0075_4'] = format_hoso.kiemtra(item.hoten_nguoithan)
                            context['bs_0076_4'] = format_hoso.convert_date(item.ngaysinh_nguoithan)
                            context['bs_0077_4'] = format_hoso.kiemtra(item.quoctich_nguoithan.name_quoctich)
                            if item.kehoach_songcung == 'Có':
                                docdata.remove_shape(u'id="74" name="Oval 74"')
                            elif item.kehoach_songcung == 'Không':
                                docdata.remove_shape(u'id="73" name="Oval 73"')
                            else:
                                docdata.remove_shape(u'id="73" name="Oval 73"')
                                docdata.remove_shape(u'id="74" name="Oval 74"')

                            if len(thuctapsinh.nguoithan_onhat_tts) == 4:
                                docdata.remove_shape(u'id="75" name="Oval 75"')
                                docdata.remove_shape(u'id="76" name="Oval 76"')
                                docdata.remove_shape(u'id="77" name="Oval 77"')
                                docdata.remove_shape(u'id="78" name="Oval 78"')
                            context['bs_0079_4'] = format_hoso.kiemtra(item.diadiem_truonghoc)
                            context['bs_0080_4'] = format_hoso.kiemtra(item.sothe_cutru)
                        elif i == 5:
                            context['bs_0074_5'] = format_hoso.kiemtra(item.quanhe_nguoithan)
                            context['bs_0075_5'] = format_hoso.kiemtra(item.hoten_nguoithan)
                            context['bs_0076_5'] = format_hoso.convert_date(item.ngaysinh_nguoithan)
                            context['bs_0077_5'] = format_hoso.kiemtra(item.quoctich_nguoithan.name_quoctich)
                            if item.kehoach_songcung == 'Có':
                                docdata.remove_shape(u'id="75" name="Oval 75"')
                            elif item.kehoach_songcung == 'Không':
                                docdata.remove_shape(u'id="76" name="Oval 76"')
                            else:
                                docdata.remove_shape(u'id="75" name="Oval 75"')
                                docdata.remove_shape(u'id="76" name="Oval 76"')

                            if len(thuctapsinh.nguoithan_onhat_tts) == 5:
                                docdata.remove_shape(u'id="77" name="Oval 77"')
                                docdata.remove_shape(u'id="78" name="Oval 78"')
                            context['bs_0079_5'] = format_hoso.kiemtra(item.diadiem_truonghoc)
                            context['bs_0080_5'] = format_hoso.kiemtra(item.sothe_cutru)
                        elif i == 6:
                            context['bs_0074_6'] = format_hoso.kiemtra(item.quanhe_nguoithan)
                            context['bs_0075_6'] = format_hoso.kiemtra(item.hoten_nguoithan)
                            context['bs_0076_6'] = format_hoso.convert_date(item.ngaysinh_nguoithan)
                            context['bs_0077_6'] = format_hoso.kiemtra(item.quoctich_nguoithan.name_quoctich)
                            if item.kehoach_songcung == 'Có':
                                docdata.remove_shape(u'id="77" name="Oval 77"')
                            elif item.kehoach_songcung == 'Không':
                                docdata.remove_shape(u'id="78" name="Oval 78"')
                            else:
                                docdata.remove_shape(u'id="77" name="Oval 77"')
                                docdata.remove_shape(u'id="78" name="Oval 78"')
                            context['bs_0079_6'] = format_hoso.kiemtra(item.diadiem_truonghoc)
                            context['bs_0080_6'] = format_hoso.kiemtra(item.sothe_cutru)
                        else:
                            break
                        i += 1
            elif thuctapsinh.nguoithan_tts == 'Không':
                docdata.remove_shape(u'id="23" name="Oval 23"')
                docdata.remove_shape(u'id="68" name="Oval 68"')
                docdata.remove_shape(u'id="67" name="Oval 67"')
                docdata.remove_shape(u'id="69" name="Oval 69"')
                docdata.remove_shape(u'id="70" name="Oval 70"')
                docdata.remove_shape(u'id="72" name="Oval 72"')
                docdata.remove_shape(u'id="73" name="Oval 73"')
                docdata.remove_shape(u'id="74" name="Oval 74"')
                docdata.remove_shape(u'id="71" name="Oval 71"')
                docdata.remove_shape(u'id="75" name="Oval 75"')
                docdata.remove_shape(u'id="76" name="Oval 76"')
                docdata.remove_shape(u'id="77" name="Oval 77"')
                docdata.remove_shape(u'id="78" name="Oval 78"')
            else:
                docdata.remove_shape(u'id="24" name="Oval 24"')
                docdata.remove_shape(u'id="23" name="Oval 23"')
                docdata.remove_shape(u'id="68" name="Oval 68"')
                docdata.remove_shape(u'id="67" name="Oval 67"')
                docdata.remove_shape(u'id="69" name="Oval 69"')
                docdata.remove_shape(u'id="70" name="Oval 70"')
                docdata.remove_shape(u'id="72" name="Oval 72"')
                docdata.remove_shape(u'id="73" name="Oval 73"')
                docdata.remove_shape(u'id="74" name="Oval 74"')
                docdata.remove_shape(u'id="71" name="Oval 71"')
                docdata.remove_shape(u'id="75" name="Oval 75"')
                docdata.remove_shape(u'id="76" name="Oval 76"')
                docdata.remove_shape(u'id="77" name="Oval 77"')
                docdata.remove_shape(u'id="78" name="Oval 78"')

            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_luutru.ten_han_xnghiep)
            context['hhjp_0012'] = format_hoso.kiemtra(document.xinghiep_luutru.sdt_xnghiep)
            context['hhjp_0097'] = format_hoso.kiemtra(
                document.donhang_luutru.nghiepdoan_donhang.ten_han_nghiepdoan)
            context['hhjp_0011_1'] = format_hoso.kiemtra(document.xinghiep_luutru.diachi_han_xnghiep)
            context['hhjp_0011'] = format_hoso.kiemtra(
                document.donhang_luutru.nghiepdoan_donhang.diachi_nghiepdoan)
            context['hhjp_0099'] = format_hoso.kiemtra(
                document.donhang_luutru.nghiepdoan_donhang.dienthoai_nghiepdoan)

            context['hhjp_0099_1'] = format_hoso.kiemtra(
                document.donhang_luutru.nghiepdoan_donhang.so_giayphep_nghiepdoan)
            context['bs_0074'] = format_hoso.kiemtra(thuctapsinh.nam_vaocty_1)
            context['bs_0075'] = format_hoso.kiemtra(thuctapsinh.thang_vaocty_1)
            context['bs_0076'] = format_hoso.kiemtra(thuctapsinh.nam_thoiviec_1)
            context['bs_0077'] = format_hoso.kiemtra(thuctapsinh.thang_thoiviec_1)
            context['bs_0078'] = format_hoso.kiemtra(thuctapsinh.quatrinh_congtac_1)

            context['bs_0079'] = format_hoso.kiemtra(thuctapsinh.nam_vaocty_2)
            context['bs_0080'] = format_hoso.kiemtra(thuctapsinh.thang_vaocty_2)
            context['bs_0081'] = format_hoso.kiemtra(thuctapsinh.nam_thoiviec_2)
            context['bs_0082'] = format_hoso.kiemtra(thuctapsinh.thang_thoiviec_2)
            context['bs_0083'] = format_hoso.kiemtra(thuctapsinh.quatrinh_congtac_2)

            context['bs_0084'] = format_hoso.kiemtra(thuctapsinh.nam_vaocty_3)
            context['bs_0085'] = format_hoso.kiemtra(thuctapsinh.thang_vaocty_3)
            context['bs_0086'] = format_hoso.kiemtra(thuctapsinh.nam_thoiviec_3)
            context['bs_0087'] = format_hoso.kiemtra(thuctapsinh.thang_thoiviec_3)
            context['bs_0088'] = format_hoso.kiemtra(thuctapsinh.quatrinh_congtac_3)

            context['bs_0089'] = format_hoso.kiemtra(thuctapsinh.nam_vaocty_4)
            context['bs_0090'] = format_hoso.kiemtra(thuctapsinh.thang_vaocty_4)
            context['bs_0091'] = format_hoso.kiemtra(thuctapsinh.nam_thoiviec_4)
            context['bs_0092'] = format_hoso.kiemtra(thuctapsinh.thang_thoiviec_4)
            context['bs_0093'] = format_hoso.kiemtra(thuctapsinh.quatrinh_congtac_4)

            context['bs_0094'] = format_hoso.kiemtra(thuctapsinh.nam_vaocty_5)
            context['bs_0095'] = format_hoso.kiemtra(thuctapsinh.thang_vaocty_5)
            context['bs_0096'] = format_hoso.kiemtra(thuctapsinh.nam_thoiviec_5)
            context['bs_0097'] = format_hoso.kiemtra(thuctapsinh.thang_thoiviec_5)
            context['bs_0098'] = format_hoso.kiemtra(thuctapsinh.quatrinh_congtac_5)

            context['bs_0099'] = format_hoso.kiemtra(thuctapsinh.nam_vaocty_6)
            context['bs_0100'] = format_hoso.kiemtra(thuctapsinh.thang_vaocty_6)
            context['bs_0101'] = format_hoso.kiemtra(thuctapsinh.nam_thoiviec_6)
            context['bs_0102'] = format_hoso.kiemtra(thuctapsinh.thang_thoiviec_6)
            context['bs_0103'] = format_hoso.kiemtra(thuctapsinh.quatrinh_congtac_6)

            if document.ngay_lapvanban_luutru:
                context['hhjp_0193_D'] = document.ngay_lapvanban_luutru.day
                context['hhjp_0193_M'] = document.ngay_lapvanban_luutru.month
                context['hhjp_0193_Y'] = document.ngay_lapvanban_luutru.year
            else:
                context['hhjp_0193_D'] = ''
                context['hhjp_0193_M'] = ''
                context['hhjp_0193_Y'] = ''

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_49(self, ma_thuctapsinh, document):
        print('49')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_49")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            context = {}
            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            if document.giaidoan_luutru == '2 go':
                context['hhjp_0911'] = format_hoso.kiemtra(thuctapsinh.sochungnhan_namhai)
                context['b1'] = '■'
                context['b2'] = '□'
                if thuctapsinh.ngaychungnhan_namhai:
                    context['hhjp_0582_Y'] = thuctapsinh.ngaychungnhan_namhai.year
                    context['hhjp_0582_M'] = thuctapsinh.ngaychungnhan_namhai.month
                    context['hhjp_0582_D'] = thuctapsinh.ngaychungnhan_namhai.day
                else:
                    context['hhjp_0582_Y'] = ''
                    context['hhjp_0582_M'] = ''
                    context['hhjp_0582_D'] = ''

                if thuctapsinh.batdau_kehoach_namhai:
                    context['hhjp_0596_Y'] = thuctapsinh.batdau_kehoach_namhai.year
                    context['hhjp_0596_M'] = thuctapsinh.batdau_kehoach_namhai.month
                    context['hhjp_0596_D'] = thuctapsinh.batdau_kehoach_namhai.day
                else:
                    context['hhjp_0596_Y'] = ''
                    context['hhjp_0596_M'] = ''
                    context['hhjp_0596_D'] = ''

                if thuctapsinh.ketthuc_kehoach_namhai:
                    context['hhjp_0597_Y'] = thuctapsinh.ketthuc_kehoach_namhai.year
                    context['hhjp_0597_M'] = thuctapsinh.ketthuc_kehoach_namhai.month
                    context['hhjp_0597_D'] = thuctapsinh.ketthuc_kehoach_namhai.day
                else:
                    context['hhjp_0597_Y'] = ''
                    context['hhjp_0597_M'] = ''
                    context['hhjp_0597_D'] = ''
            elif document.giaidoan_luutru == '3 go':
                context['b1'] = '□'
                context['b2'] = '■'
                context['hhjp_0911'] = format_hoso.kiemtra(thuctapsinh.sochungnhan_namba)
                if thuctapsinh.ngaychungnhan_namba:
                    context['hhjp_0582_Y'] = thuctapsinh.ngaychungnhan_namba.year
                    context['hhjp_0582_M'] = thuctapsinh.ngaychungnhan_namba.month
                    context['hhjp_0582_D'] = thuctapsinh.ngaychungnhan_namba.day
                else:
                    context['hhjp_0582_Y'] = ''
                    context['hhjp_0582_M'] = ''
                    context['hhjp_0582_D'] = ''

                if thuctapsinh.batdau_kehoach_namba:
                    context['hhjp_0596_Y'] = thuctapsinh.batdau_kehoach_namba.year
                    context['hhjp_0596_M'] = thuctapsinh.batdau_kehoach_namba.month
                    context['hhjp_0596_D'] = thuctapsinh.batdau_kehoach_namba.day
                else:
                    context['hhjp_0596_Y'] = ''
                    context['hhjp_0596_M'] = ''
                    context['hhjp_0596_D'] = ''

                if thuctapsinh.ketthuc_kehoach_namba:
                    context['hhjp_0597_Y'] = thuctapsinh.ketthuc_kehoach_namba.year
                    context['hhjp_0597_M'] = thuctapsinh.ketthuc_kehoach_namba.month
                    context['hhjp_0597_D'] = thuctapsinh.ketthuc_kehoach_namba.day
                else:
                    context['hhjp_0597_Y'] = ''
                    context['hhjp_0597_M'] = ''
                    context['hhjp_0597_D'] = ''
            else:
                context['b1'] = '□'
                context['b2'] = '□'
                context['hhjp_0596_Y'] = ''
                context['hhjp_0596_M'] = ''
                context['hhjp_0596_D'] = ''
                context['hhjp_0597_Y'] = ''
                context['hhjp_0597_M'] = ''
                context['hhjp_0597_D'] = ''
                context['hhjp_0911'] = ''
                context['hhjp_0582_Y'] = ''
                context['hhjp_0582_M'] = ''
                context['hhjp_0582_D'] = ''

            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_luutru.ten_han_xnghiep)
            context['bs_0109'] = format_hoso.kiemtra(document.xinghiep_luutru.phapnhan_xinghiep)
            context['hhjp_0011_2'] = format_hoso.kiemtra(
                document.donhang_luutru.donhang_daingo.tennha_daingo_daotao_nhao) + '     ' + format_hoso.kiemtra(
                document.donhang_luutru.donhang_daingo.diachi_daingo_daotao_nhao)
            context['hhjp_0011_1'] = format_hoso.kiemtra(document.xinghiep_luutru.diachi_han_xnghiep)
            context['hhjp_0011'] = format_hoso.kiemtra(document.donhang_luutru.nghiepdoan_donhang.diachi_nghiepdoan)
            context['hhjp_0012'] = format_hoso.kiemtra(document.xinghiep_luutru.sdt_xnghiep)
            context['hhjp_0811'] = format_hoso.kiemtra(document.xinghiep_luutru.so_chapnhan_daotao)
            context['hhjp_0023'] = format_hoso.place_value(document.xinghiep_luutru.sovon_xnghiep)
            context['hhjp_0024'] = format_hoso.place_value(document.xinghiep_luutru.doanhthu_namtruoc_xnghiep)
            context['hhjp_0097'] = format_hoso.kiemtra(document.donhang_luutru.nghiepdoan_donhang.ten_han_nghiepdoan)
            context['hhjp_0912'] = format_hoso.kiemtra(
                document.donhang_luutru.nghiepdoan_donhang.so_phapnhan_nghiepdoan)
            context['bs_0111'] = format_hoso.place_value(document.xinghiep_luutru.so_nvien_xnghiep)
            context['bs_0113'] = format_hoso.place_value(
                document.donhang_luutru.nghiepdoan_donhang.so_nvien_chinhthuc_nghiepdoan)
            context['bs_0112'] = format_hoso.place_value(document.xinghiep_luutru.nvien_nuocngoai_xnghiep)
            if document.donhang_luutru.nghiepdoan_donhang.ngay_capphep_nghiepdoan:
                context['bs_0114_Y'] = document.donhang_luutru.nghiepdoan_donhang.ngay_capphep_nghiepdoan.year
                context['bs_0114_M'] = document.donhang_luutru.nghiepdoan_donhang.ngay_capphep_nghiepdoan.month
                context['bs_0114_D'] = document.donhang_luutru.nghiepdoan_donhang.ngay_capphep_nghiepdoan.day
            else:
                context['bs_0114_Y'] = ''
                context['bs_0114_M'] = ''
                context['bs_0114_D'] = ''

            if document.donhang_luutru.nghiepdoan_donhang.batdau_hieuluc_giayphep_nghiepdoan:
                context[
                    'bs_0115_Y'] = document.donhang_luutru.nghiepdoan_donhang.batdau_hieuluc_giayphep_nghiepdoan.year
                context[
                    'bs_0115_M'] = document.donhang_luutru.nghiepdoan_donhang.batdau_hieuluc_giayphep_nghiepdoan.month
                context['bs_0115_D'] = document.donhang_luutru.nghiepdoan_donhang.batdau_hieuluc_giayphep_nghiepdoan.day
            else:
                context['bs_0115_Y'] = ''
                context['bs_0115_M'] = ''
                context['bs_0115_D'] = ''

            if document.donhang_luutru.nghiepdoan_donhang.ketthuc_hieuluc_giayphep_nghiepdoan:
                context[
                    'bs_0116_Y'] = document.donhang_luutru.nghiepdoan_donhang.ketthuc_hieuluc_giayphep_nghiepdoan.year
                context[
                    'bs_0116_M'] = document.donhang_luutru.nghiepdoan_donhang.ketthuc_hieuluc_giayphep_nghiepdoan.month
                context[
                    'bs_0116_D'] = document.donhang_luutru.nghiepdoan_donhang.ketthuc_hieuluc_giayphep_nghiepdoan.day
            else:
                context['bs_0116_Y'] = ''
                context['bs_0116_M'] = ''
                context['bs_0116_D'] = ''

            context['hhjp_0103'] = format_hoso.kiemtra(
                document.donhang_luutru.nghiepdoan_donhang.ten_daidien_han_nghiepdoan)
            context['hhjp_0103_1'] = format_hoso.kiemtra(
                document.donhang_luutru.nghiepdoan_donhang.chucvu_daidien_phienam_nghiepdoan)

            if document.ngay_lapvanban_luutru:
                context['hhjp_0193_D'] = document.ngay_lapvanban_luutru.day
                context['hhjp_0193_M'] = document.ngay_lapvanban_luutru.month
                context['hhjp_0193_Y'] = document.ngay_lapvanban_luutru.year
            else:
                context['hhjp_0193_D'] = ''
                context['hhjp_0193_M'] = ''
                context['hhjp_0193_Y'] = ''

            if document.donhang_luutru.nghiepdoan_donhang.loai_giayphep_nghiepdoan == 'Tổng hợp':
                context['bs_0114_1'] = '■'
                context['bs_0114_2'] = '□'
            elif document.donhang_luutru.nghiepdoan_donhang.loai_giayphep_nghiepdoan == 'Cụ thể':
                context['bs_0114_1'] = '□'
                context['bs_0114_2'] = '■'
            else:
                context['bs_0114_1'] = '□'
                context['bs_0114_2'] = '□'

            if document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == '1:農業関係':
                context['bs_0110_1'] = '■'
                context['bs_0110_2'] = '□'
                context['bs_0110_3'] = '□'
                context['bs_0110_4'] = '□'
                context['bs_0110_5'] = '□'
                context['bs_0110_6'] = '□'
                context['bs_0110_7'] = '□'
                context['bs_0110_8'] = ''
            elif document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == '2:漁業関係':
                context['bs_0110_1'] = '□'
                context['bs_0110_2'] = '■'
                context['bs_0110_3'] = '□'
                context['bs_0110_4'] = '□'
                context['bs_0110_5'] = '□'
                context['bs_0110_6'] = '□'
                context['bs_0110_7'] = '□'
                context['bs_0110_8'] = ''
            elif document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == '3:建設関係':
                context['bs_0110_1'] = '□'
                context['bs_0110_2'] = '□'
                context['bs_0110_3'] = '■'
                context['bs_0110_4'] = '□'
                context['bs_0110_5'] = '□'
                context['bs_0110_6'] = '□'
                context['bs_0110_7'] = '□'
                context['bs_0110_8'] = ''
            elif document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == '4:食品製造関係':
                context['bs_0110_1'] = '□'
                context['bs_0110_2'] = '□'
                context['bs_0110_3'] = '□'
                context['bs_0110_4'] = '■'
                context['bs_0110_5'] = '□'
                context['bs_0110_6'] = '□'
                context['bs_0110_7'] = '□'
                context['bs_0110_8'] = ''
            elif document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == '5:繊維・衣服関係':
                context['bs_0110_1'] = '□'
                context['bs_0110_2'] = '□'
                context['bs_0110_3'] = '□'
                context['bs_0110_4'] = '□'
                context['bs_0110_5'] = '■'
                context['bs_0110_6'] = '□'
                context['bs_0110_7'] = '□'
                context['bs_0110_8'] = ''
            elif document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == '6:機械・金属関係':
                context['bs_0110_1'] = '□'
                context['bs_0110_2'] = '□'
                context['bs_0110_3'] = '□'
                context['bs_0110_4'] = '□'
                context['bs_0110_5'] = '□'
                context['bs_0110_6'] = '■'
                context['bs_0110_7'] = '□'
                context['bs_0110_8'] = ''
            elif document.xinghiep_luutru.noidung_congviec_xinghiep.name_nganhnghe == '7:その他':
                context['bs_0110_1'] = '□'
                context['bs_0110_2'] = '□'
                context['bs_0110_3'] = '□'
                context['bs_0110_4'] = '□'
                context['bs_0110_5'] = '□'
                context['bs_0110_6'] = '□'
                context['bs_0110_7'] = '■'
                context['bs_0110_8'] = format_hoso.kiemtra(document.xinghiep_luutru.noidung_congviec_khac_xinghiep)
            else:
                context['bs_0110_1'] = '□'
                context['bs_0110_2'] = '□'
                context['bs_0110_3'] = '□'
                context['bs_0110_4'] = '□'
                context['bs_0110_5'] = '□'
                context['bs_0110_6'] = '□'
                context['bs_0110_7'] = '□'
                context['bs_0110_8'] = ''

            if document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '①商工会議所・商工会':
                context['bs_0123_1'] = '■'
                context['bs_0123_2'] = '□'
                context['bs_0123_3'] = '□'
                context['bs_0123_4'] = '□'
                context['bs_0123_5'] = '□'
                context['bs_0123_6'] = '□'
                context['bs_0123_7'] = '□'
            elif document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '②中小企業団体':
                context['bs_0123_1'] = '□'
                context['bs_0123_2'] = '■'
                context['bs_0123_3'] = '□'
                context['bs_0123_4'] = '□'
                context['bs_0123_5'] = '□'
                context['bs_0123_6'] = '□'
                context['bs_0123_7'] = '□'
            elif document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '③職業訓練法人':
                context['bs_0123_1'] = '□'
                context['bs_0123_2'] = '□'
                context['bs_0123_3'] = '■'
                context['bs_0123_4'] = '□'
                context['bs_0123_5'] = '□'
                context['bs_0123_6'] = '□'
                context['bs_0123_7'] = '□'
            elif document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '④農業協同組合':
                context['bs_0123_1'] = '□'
                context['bs_0123_2'] = '□'
                context['bs_0123_3'] = '□'
                context['bs_0123_4'] = '■'
                context['bs_0123_5'] = '□'
                context['bs_0123_6'] = '□'
                context['bs_0123_7'] = '□'
            elif document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '⑤漁業協同組合':
                context['bs_0123_1'] = '□'
                context['bs_0123_2'] = '□'
                context['bs_0123_3'] = '□'
                context['bs_0123_4'] = '□'
                context['bs_0123_5'] = '■'
                context['bs_0123_6'] = '□'
                context['bs_0123_7'] = '□'
            elif document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '⑥公益社団法人・公益財団法人':
                context['bs_0123_1'] = '□'
                context['bs_0123_2'] = '□'
                context['bs_0123_3'] = '□'
                context['bs_0123_4'] = '□'
                context['bs_0123_5'] = '□'
                context['bs_0123_6'] = '■'
                context['bs_0123_7'] = '□'
            elif document.donhang_luutru.nghiepdoan_donhang.loai_nghiepdoan == '⑦その他':
                context['bs_0123_1'] = '□'
                context['bs_0123_2'] = '□'
                context['bs_0123_3'] = '□'
                context['bs_0123_4'] = '□'
                context['bs_0123_5'] = '□'
                context['bs_0123_6'] = '□'
                context['bs_0123_7'] = '■'
            else:
                context['bs_0123_1'] = '□'
                context['bs_0123_2'] = '□'
                context['bs_0123_3'] = '□'
                context['bs_0123_4'] = '□'
                context['bs_0123_5'] = '□'
                context['bs_0123_6'] = '□'
                context['bs_0123_7'] = '□'

            context['hhjp_0098'] = format_hoso.kiemtra(document.donhang_luutru.nghiepdoan_donhang.diachi_nghiepdoan)

            context['hhjp_0099'] = format_hoso.kiemtra(document.donhang_luutru.nghiepdoan_donhang.dienthoai_nghiepdoan)
            context['hhjp_0003'] = format_hoso.kiemtra(thuctapsinh.quoctich_tts.name_quoctich)
            if thuctapsinh.ngaysinh_tts:
                context['hhjp_0004_D'] = thuctapsinh.ngaysinh_tts.day
                context['hhjp_0004_M'] = thuctapsinh.ngaysinh_tts.month
                context['hhjp_0004_Y'] = thuctapsinh.ngaysinh_tts.year
            else:
                context['hhjp_0004_D'] = ''
                context['hhjp_0004_M'] = ''
                context['hhjp_0004_Y'] = ''

            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            context['bs_0058'] = format_hoso.kiemtra(thuctapsinh.noisinh_tts)
            context['hhjp_0521'] = format_hoso.kiemtra(thuctapsinh.quehuong_tts)
            context['hhjp_0345'] = format_hoso.kiemtra(thuctapsinh.nghenghiep_tts)

            if thuctapsinh.nguoikethon_tts == 'Có':
                docdata.remove_shape(u'id="25" name="Oval 25"')
            elif thuctapsinh.nguoikethon_tts == 'Không':
                docdata.remove_shape(u'id="24" name="Oval 24"')
            else:
                docdata.remove_shape(u'id="25" name="Oval 25"')
                docdata.remove_shape(u'id="24" name="Oval 24"')
            context['hhjp_0522'] = format_hoso.kiemtra(thuctapsinh.passport_so_tts)
            context['bs_0059'] = format_hoso.kiemtra(thuctapsinh.soban_tts)
            context['bs_0060'] = format_hoso.kiemtra(thuctapsinh.dienthoai_tts)

            if thuctapsinh.passport_hethan_tts:
                context['bs_0061_D'] = thuctapsinh.passport_hethan_tts.day
                context['bs_0061_M'] = thuctapsinh.passport_hethan_tts.month
                context['bs_0061_Y'] = thuctapsinh.passport_hethan_tts.year
            else:
                context['bs_0061_D'] = ''
                context['bs_0061_M'] = ''
                context['bs_0061_Y'] = ''

            context['hhjp_0006'] = format_hoso.gender_check(thuctapsinh.gtinh_tts)
            context['bs_0117'] = format_hoso.kiemtra(thuctapsinh.tinhtrang_cutru_thaydoi_1)
            context['bs_0118'] = format_hoso.kiemtra(thuctapsinh.thoigian_luutru_thaydoi_1)

            if thuctapsinh.hethan_luutru_thaydoi:
                context['bs_0119_D'] = thuctapsinh.hethan_luutru_thaydoi.day
                context['bs_0119_M'] = thuctapsinh.hethan_luutru_thaydoi.month
                context['bs_0119_Y'] = thuctapsinh.hethan_luutru_thaydoi.year
            else:
                context['bs_0119_D'] = ''
                context['bs_0119_M'] = ''
                context['bs_0119_Y'] = ''

            context['bs_0120'] = format_hoso.kiemtra(thuctapsinh.sothe_luutru_thaydoi)
            context['bs_0121'] = format_hoso.kiemtra(thuctapsinh.tinhtrang_cutru_thaydoi_2)
            context['bs_0121_1'] = format_hoso.kiemtra(thuctapsinh.thoigan_luutru_thaydoi_2)
            context['bs_0122'] = format_hoso.kiemtra(thuctapsinh.lydo_thaydoi_thaydoi)

            if thuctapsinh.toipham_tts == 'Có':
                docdata.remove_shape(u'id="21" name="Oval 21"')
                context['bs_0069'] = format_hoso.kiemtra(thuctapsinh.noidung_toipham)
            elif thuctapsinh.toipham_tts == 'Không':
                docdata.remove_shape(u'id="20" name="Oval 20"')
                context['bs_0069'] = ''
            else:
                docdata.remove_shape(u'id="21" name="Oval 21"')
                docdata.remove_shape(u'id="20" name="Oval 20"')
                context['bs_0069'] = ''

            if thuctapsinh.nguoithan_tts == 'Có':
                docdata.remove_shape(u'id="23" name="Oval 23"')
                i = 1
                for item in thuctapsinh.nguoithan_onhat_tts:
                    if i > len(thuctapsinh.nguoithan_onhat_tts) or i == 6:
                        break
                    else:
                        if i == 1:
                            context['bs_0074_1'] = format_hoso.kiemtra(item.quanhe_nguoithan)
                            context['bs_0075_1'] = format_hoso.kiemtra(item.hoten_nguoithan)
                            context['bs_0076_1'] = format_hoso.convert_date(item.ngaysinh_nguoithan)
                            context['bs_0077_1'] = format_hoso.kiemtra(item.quoctich_nguoithan.name_quoctich)
                            if item.kehoach_songcung == 'Có':
                                docdata.remove_shape(u'id="70" name="Oval 70"')
                            elif item.kehoach_songcung == 'Không':
                                docdata.remove_shape(u'id="69" name="Oval 69"')
                            else:
                                docdata.remove_shape(u'id="70" name="Oval 70"')
                                docdata.remove_shape(u'id="69" name="Oval 69"')

                            if len(thuctapsinh.nguoithan_onhat_tts) == 1:
                                docdata.remove_shape(u'id="72" name="Oval 72"')
                                docdata.remove_shape(u'id="73" name="Oval 73"')
                                docdata.remove_shape(u'id="74" name="Oval 74"')
                                docdata.remove_shape(u'id="71" name="Oval 71"')
                                docdata.remove_shape(u'id="75" name="Oval 75"')
                                docdata.remove_shape(u'id="76" name="Oval 76"')
                                docdata.remove_shape(u'id="77" name="Oval 77"')
                                docdata.remove_shape(u'id="78" name="Oval 78"')
                                docdata.remove_shape(u'id="79" name="Oval 79"')
                                docdata.remove_shape(u'id="80" name="Oval 80"')
                            context['bs_0079_1'] = format_hoso.kiemtra(item.diadiem_truonghoc)
                            context['bs_0080_1'] = format_hoso.kiemtra(item.sothe_cutru)
                        elif i == 2:
                            context['bs_0074_2'] = format_hoso.kiemtra(item.quanhe_nguoithan)
                            context['bs_0075_2'] = format_hoso.kiemtra(item.hoten_nguoithan)
                            context['bs_0076_2'] = format_hoso.convert_date(item.ngaysinh_nguoithan)
                            context['bs_0077_2'] = format_hoso.kiemtra(item.quoctich_nguoithan.name_quoctich)
                            if item.kehoach_songcung == 'Có':
                                docdata.remove_shape(u'id="72" name="Oval 72"')
                            elif item.kehoach_songcung == 'Không':
                                docdata.remove_shape(u'id="71" name="Oval 71"')
                            else:
                                docdata.remove_shape(u'id="71" name="Oval 71"')
                                docdata.remove_shape(u'id="72" name="Oval 72"')
                            if len(thuctapsinh.nguoithan_onhat_tts) == 2:
                                docdata.remove_shape(u'id="73" name="Oval 73"')
                                docdata.remove_shape(u'id="74" name="Oval 74"')
                                docdata.remove_shape(u'id="75" name="Oval 75"')
                                docdata.remove_shape(u'id="76" name="Oval 76"')
                                docdata.remove_shape(u'id="77" name="Oval 77"')
                                docdata.remove_shape(u'id="78" name="Oval 78"')
                                docdata.remove_shape(u'id="79" name="Oval 79"')
                                docdata.remove_shape(u'id="80" name="Oval 80"')
                            context['bs_0079_2'] = format_hoso.kiemtra(item.diadiem_truonghoc)
                            context['bs_0080_2'] = format_hoso.kiemtra(item.sothe_cutru)
                        elif i == 3:
                            context['bs_0074_3'] = format_hoso.kiemtra(item.quanhe_nguoithan)
                            context['bs_0075_3'] = format_hoso.kiemtra(item.hoten_nguoithan)
                            context['bs_0076_3'] = format_hoso.convert_date(item.ngaysinh_nguoithan)
                            context['bs_0077_3'] = format_hoso.kiemtra(item.quoctich_nguoithan.name_quoctich)
                            if item.kehoach_songcung == 'Có':
                                docdata.remove_shape(u'id="74" name="Oval 74"')
                            elif item.kehoach_songcung == 'Không':
                                docdata.remove_shape(u'id="73" name="Oval 73"')
                            else:
                                docdata.remove_shape(u'id="74" name="Oval 74"')
                                docdata.remove_shape(u'id="73" name="Oval 73"')

                            if len(thuctapsinh.nguoithan_onhat_tts) == 3:
                                docdata.remove_shape(u'id="75" name="Oval 75"')
                                docdata.remove_shape(u'id="76" name="Oval 76"')
                                docdata.remove_shape(u'id="77" name="Oval 77"')
                                docdata.remove_shape(u'id="78" name="Oval 78"')
                                docdata.remove_shape(u'id="79" name="Oval 79"')
                                docdata.remove_shape(u'id="80" name="Oval 80"')
                            context['bs_0079_3'] = format_hoso.kiemtra(item.diadiem_truonghoc)
                            context['bs_0080_3'] = format_hoso.kiemtra(item.sothe_cutru)
                        elif i == 4:
                            context['bs_0074_4'] = format_hoso.kiemtra(item.quanhe_nguoithan)
                            context['bs_0075_4'] = format_hoso.kiemtra(item.hoten_nguoithan)
                            context['bs_0076_4'] = format_hoso.convert_date(item.ngaysinh_nguoithan)
                            context['bs_0077_4'] = format_hoso.kiemtra(item.quoctich_nguoithan.name_quoctich)
                            if item.kehoach_songcung == 'Có':
                                docdata.remove_shape(u'id="76" name="Oval 76"')
                            elif item.kehoach_songcung == 'Không':
                                docdata.remove_shape(u'id="75" name="Oval 75"')
                            else:
                                docdata.remove_shape(u'id="76" name="Oval 76"')
                                docdata.remove_shape(u'id="75" name="Oval 75"')

                            if len(thuctapsinh.nguoithan_onhat_tts) == 4:
                                docdata.remove_shape(u'id="77" name="Oval 77"')
                                docdata.remove_shape(u'id="78" name="Oval 78"')
                                docdata.remove_shape(u'id="79" name="Oval 79"')
                                docdata.remove_shape(u'id="80" name="Oval 80"')
                            context['bs_0079_4'] = format_hoso.kiemtra(item.diadiem_truonghoc)
                            context['bs_0080_4'] = format_hoso.kiemtra(item.sothe_cutru)
                        elif i == 5:
                            context['bs_0074_5'] = format_hoso.kiemtra(item.quanhe_nguoithan)
                            context['bs_0075_5'] = format_hoso.kiemtra(item.hoten_nguoithan)
                            context['bs_0076_5'] = format_hoso.convert_date(item.ngaysinh_nguoithan)
                            context['bs_0077_5'] = format_hoso.kiemtra(item.quoctich_nguoithan.name_quoctich)
                            if item.kehoach_songcung == 'Có':
                                docdata.remove_shape(u'id="78" name="Oval 78"')
                            elif item.kehoach_songcung == 'Không':
                                docdata.remove_shape(u'id="77" name="Oval 77"')
                            else:
                                docdata.remove_shape(u'id="78" name="Oval 78"')
                                docdata.remove_shape(u'id="77" name="Oval 77"')

                            if len(thuctapsinh.nguoithan_onhat_tts) == 5:
                                docdata.remove_shape(u'id="79" name="Oval 79"')
                                docdata.remove_shape(u'id="80" name="Oval 80"')
                            context['bs_0079_5'] = format_hoso.kiemtra(item.diadiem_truonghoc)
                            context['bs_0080_5'] = format_hoso.kiemtra(item.sothe_cutru)
                        elif i == 6:
                            context['bs_0074_6'] = format_hoso.kiemtra(item.quanhe_nguoithan)
                            context['bs_0075_6'] = format_hoso.kiemtra(item.hoten_nguoithan)
                            context['bs_0076_6'] = format_hoso.convert_date(item.ngaysinh_nguoithan)
                            context['bs_0077_6'] = format_hoso.kiemtra(item.quoctich_nguoithan.name_quoctich)
                            if item.kehoach_songcung == 'Có':
                                docdata.remove_shape(u'id="80" name="Oval 80"')
                            elif item.kehoach_songcung == 'Không':
                                docdata.remove_shape(u'id="79" name="Oval 79"')
                            else:
                                docdata.remove_shape(u'id="80" name="Oval 80"')
                                docdata.remove_shape(u'id="79" name="Oval 79"')
                            context['bs_0079_6'] = format_hoso.kiemtra(item.diadiem_truonghoc)
                            context['bs_0080_6'] = format_hoso.kiemtra(item.sothe_cutru)
                        else:
                            break
                        i += 1
            elif thuctapsinh.nguoithan_tts == 'Không':
                docdata.remove_shape(u'id="22" name="Oval 22"')
                docdata.remove_shape(u'id="70" name="Oval 70"')
                docdata.remove_shape(u'id="69" name="Oval 69"')
                docdata.remove_shape(u'id="72" name="Oval 72"')
                docdata.remove_shape(u'id="73" name="Oval 73"')
                docdata.remove_shape(u'id="74" name="Oval 74"')
                docdata.remove_shape(u'id="71" name="Oval 71"')
                docdata.remove_shape(u'id="75" name="Oval 75"')
                docdata.remove_shape(u'id="76" name="Oval 76"')
                docdata.remove_shape(u'id="77" name="Oval 77"')
                docdata.remove_shape(u'id="78" name="Oval 78"')
                docdata.remove_shape(u'id="79" name="Oval 79"')
                docdata.remove_shape(u'id="80" name="Oval 80"')
            else:
                docdata.remove_shape(u'id="23" name="Oval 23"')
                docdata.remove_shape(u'id="22" name="Oval 22"')
                docdata.remove_shape(u'id="70" name="Oval 70"')
                docdata.remove_shape(u'id="69" name="Oval 69"')
                docdata.remove_shape(u'id="72" name="Oval 72"')
                docdata.remove_shape(u'id="73" name="Oval 73"')
                docdata.remove_shape(u'id="74" name="Oval 74"')
                docdata.remove_shape(u'id="71" name="Oval 71"')
                docdata.remove_shape(u'id="75" name="Oval 75"')
                docdata.remove_shape(u'id="76" name="Oval 76"')
                docdata.remove_shape(u'id="77" name="Oval 77"')
                docdata.remove_shape(u'id="78" name="Oval 78"')
                docdata.remove_shape(u'id="79" name="Oval 79"')
                docdata.remove_shape(u'id="80" name="Oval 80"')

            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_luutru.ten_han_xnghiep)
            context['hhjp_0012'] = format_hoso.kiemtra(document.xinghiep_luutru.sdt_xnghiep)
            context['hhjp_0097'] = format_hoso.kiemtra(document.donhang_luutru.nghiepdoan_donhang.ten_han_nghiepdoan)
            context['hhjp_0098'] = format_hoso.kiemtra(document.donhang_luutru.nghiepdoan_donhang.diachi_nghiepdoan)
            context['hhjp_0099_1'] = format_hoso.kiemtra(
                document.donhang_luutru.nghiepdoan_donhang.so_giayphep_nghiepdoan)

            context['bs_0074'] = format_hoso.kiemtra(thuctapsinh.nam_vaocty_1)
            context['bs_0075'] = format_hoso.kiemtra(thuctapsinh.thang_vaocty_1)
            context['bs_0076'] = format_hoso.kiemtra(thuctapsinh.nam_thoiviec_1)
            context['bs_0077'] = format_hoso.kiemtra(thuctapsinh.thang_thoiviec_1)
            context['bs_0078'] = format_hoso.kiemtra(thuctapsinh.quatrinh_congtac_1)

            context['bs_0079'] = format_hoso.kiemtra(thuctapsinh.nam_vaocty_2)
            context['bs_0080'] = format_hoso.kiemtra(thuctapsinh.thang_vaocty_2)
            context['bs_0081'] = format_hoso.kiemtra(thuctapsinh.nam_thoiviec_2)
            context['bs_0082'] = format_hoso.kiemtra(thuctapsinh.thang_thoiviec_2)
            context['bs_0083'] = format_hoso.kiemtra(thuctapsinh.quatrinh_congtac_2)

            context['bs_0084'] = format_hoso.kiemtra(thuctapsinh.nam_vaocty_3)
            context['bs_0085'] = format_hoso.kiemtra(thuctapsinh.thang_vaocty_3)
            context['bs_0086'] = format_hoso.kiemtra(thuctapsinh.nam_thoiviec_3)
            context['bs_0087'] = format_hoso.kiemtra(thuctapsinh.thang_thoiviec_3)
            context['bs_0088'] = format_hoso.kiemtra(thuctapsinh.quatrinh_congtac_3)

            context['bs_0089'] = format_hoso.kiemtra(thuctapsinh.nam_vaocty_4)
            context['bs_0090'] = format_hoso.kiemtra(thuctapsinh.thang_vaocty_4)
            context['bs_0091'] = format_hoso.kiemtra(thuctapsinh.nam_thoiviec_4)
            context['bs_0092'] = format_hoso.kiemtra(thuctapsinh.thang_thoiviec_4)
            context['bs_0093'] = format_hoso.kiemtra(thuctapsinh.quatrinh_congtac_4)

            context['bs_0094'] = format_hoso.kiemtra(thuctapsinh.nam_vaocty_5)
            context['bs_0095'] = format_hoso.kiemtra(thuctapsinh.thang_vaocty_5)
            context['bs_0096'] = format_hoso.kiemtra(thuctapsinh.nam_thoiviec_5)
            context['bs_0097'] = format_hoso.kiemtra(thuctapsinh.thang_thoiviec_5)
            context['bs_0098'] = format_hoso.kiemtra(thuctapsinh.quatrinh_congtac_5)

            context['bs_0099'] = format_hoso.kiemtra(thuctapsinh.nam_vaocty_6)
            context['bs_0100'] = format_hoso.kiemtra(thuctapsinh.thang_vaocty_6)
            context['bs_0101'] = format_hoso.kiemtra(thuctapsinh.nam_thoiviec_6)
            context['bs_0102'] = format_hoso.kiemtra(thuctapsinh.thang_thoiviec_6)
            context['bs_0103'] = format_hoso.kiemtra(thuctapsinh.quatrinh_congtac_6)

            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None


class Hoso2(models.Model):
    _inherit = 'hoso.hoso'

    @api.multi
    def baocao_hoso_50_MOI(self, ma_thuctapsinh, document):
        print('50_Mới')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_50_MOI")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            context = {}

            if document.ngay_lapvanban1:
                context['hhjp_0193'] = str(document.ngay_lapvanban1.year) + '年' + \
                                       str(document.ngay_lapvanban1.month) + '月' + \
                                       str(document.ngay_lapvanban1.day) + '日'
                context['hhjp_0193_v'] = 'Ngày ' + str(document.ngay_lapvanban1.day) + \
                                         ' tháng ' + str(document.ngay_lapvanban1.month) + \
                                         ' năm ' + str(document.ngay_lapvanban1.year)
            else:
                context['hhjp_0193'] = '年　月　日'
                context['hhjp_0193_v'] = 'Ngày tháng năm'

            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            if thuctapsinh.ten_han_tts:
                context['hhjp_0002'] = format_hoso.kiemtra(thuctapsinh.ten_han_tts)
            else:
                context['hhjp_0002'] = format_hoso.kiemtra(thuctapsinh.ten_katakana_tts)
            context['hhjp_0003'] = format_hoso.kiemtra(thuctapsinh.quoctich_tts.name_quoctich)
            context['hhjp_0003_v'] = format_hoso.kiemtra(thuctapsinh.quoctich_tts_v)

            if thuctapsinh.gtinh_tts == u'Nam':
                context['a12_1'] = u'■'
                context['a12_2'] = u'□'
            elif thuctapsinh.gtinh_tts == u'Nữ':
                context['a12_1'] = u'□'
                context['a12_2'] = u'■'
            else:
                context['a12_1'] = u'□'
                context['a12_2'] = u'□'

            context['hhjp_0330'] = format_hoso.kiemtra(thuctapsinh.tiengmede_tts)
            context['hhjp_0330_v'] = format_hoso.kiemtra(thuctapsinh.tiengmede_tts_v)
            if thuctapsinh.ngaysinh_tts:
                context['hhjp_0004'] = str(thuctapsinh.ngaysinh_tts.year) + '年' + \
                                       str(thuctapsinh.ngaysinh_tts.month) + '月' + \
                                       str(thuctapsinh.ngaysinh_tts.day) + '日'

                context['hhjp_0004_v'] = 'Ngày ' + str(thuctapsinh.ngaysinh_tts.day) + \
                                         ' tháng ' + str(thuctapsinh.ngaysinh_tts.month) + \
                                         ' năm ' + str(thuctapsinh.ngaysinh_tts.year)

            else:
                context['hhjp_0004'] = '年　月　日'
                context['hhjp_0004_v'] = 'Ngày tháng năm'

            context['hhjp_0005'] = format_hoso.kiemtra(thuctapsinh.tuoi_tts_pc)
            context['hhjp_0005_v'] = format_hoso.kiemtra(thuctapsinh.tuoi_tts_pc)
            context['hhjp_0331'] = format_hoso.kiemtra(thuctapsinh.diachi_tts)
            context['hhjp_0331_v'] = format_hoso.kiemtra(thuctapsinh.diachi_tts_v).upper()

            hhjp_0342_1 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_hoctap_nam_1,
                                                              thuctapsinh.batdau_hoctap_thang_1)) if thuctapsinh.batdau_hoctap_nam_1 else ''
            hhjp_0343_1 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_hoctap_nam_1,
                                                              thuctapsinh.ketthuc_hoctap_thang_1)) if thuctapsinh.ketthuc_hoctap_nam_1 else ''
            context['hhjp_0342_1'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0342_1, hhjp_0343_1))

            context['hhjp_0344_1'] = format_hoso.kiemtra(thuctapsinh.truong_tts_1)

            hhjp_0342_v1 = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_hoctap_nam_1,
                                                                 thuctapsinh.batdau_hoctap_thang_1)) if thuctapsinh.batdau_hoctap_nam_1 else ''
            hhjp_0343_v1 = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_hoctap_nam_1,
                                                                 thuctapsinh.ketthuc_hoctap_thang_1)) if thuctapsinh.ketthuc_hoctap_nam_1 else ''
            context['hhjp_0342_v1'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0342_v1, hhjp_0343_v1))

            context['hhjp_0344_v1'] = format_hoso.kiemtra(thuctapsinh.truong_tts_v_1)

            hhjp_0342_2 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_hoctap_nam_2,
                                                              thuctapsinh.batdau_hoctap_thang_2)) if thuctapsinh.batdau_hoctap_nam_2 else ''
            hhjp_0343_2 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_hoctap_nam_2,
                                                              thuctapsinh.ketthuc_hoctap_thang_2)) if thuctapsinh.ketthuc_hoctap_nam_2 else ''
            context['hhjp_0342_2'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0342_2, hhjp_0343_2))
            context['hhjp_0344_2'] = format_hoso.kiemtra(thuctapsinh.truong_tts_2)
            hhjp_0342_v2 = format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_hoctap_nam_2,
                                                             thuctapsinh.batdau_hoctap_thang_2) if thuctapsinh.batdau_hoctap_nam_2 else ''
            hhjp_0343_v2 = format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_hoctap_nam_2,
                                                             thuctapsinh.ketthuc_hoctap_thang_2) if thuctapsinh.ketthuc_hoctap_nam_2 else ''
            context['hhjp_0342_v2'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0342_v2, hhjp_0343_v2))
            context['hhjp_0344_v2'] = format_hoso.kiemtra(thuctapsinh.truong_tts_v_2)

            hhjp_0342_3 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_hoctap_nam_3,
                                                              thuctapsinh.batdau_hoctap_thang_3)) if thuctapsinh.batdau_hoctap_nam_3 else ''
            hhjp_0343_3 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_hoctap_nam_3,
                                                              thuctapsinh.ketthuc_hoctap_thang_3)) if thuctapsinh.ketthuc_hoctap_nam_3 else ''

            context['hhjp_0342_3'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0342_3, hhjp_0343_3))
            context['hhjp_0344_3'] = format_hoso.kiemtra(thuctapsinh.truong_tts_3)
            hhjp_0342_v3 = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_hoctap_nam_3,
                                                                 thuctapsinh.batdau_hoctap_thang_3)) if thuctapsinh.batdau_hoctap_nam_3 else ''
            hhjp_0343_v3 = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_hoctap_nam_3,
                                                                 thuctapsinh.ketthuc_hoctap_thang_3)) if thuctapsinh.ketthuc_hoctap_nam_3 else ''
            context['hhjp_0344_v3'] = format_hoso.kiemtra(thuctapsinh.truong_tts_v_3)

            context['hhjp_0342_v3'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0342_v3, hhjp_0343_v3))

            ### Công tác 1
            hhjp_0347_1 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_congtac_tts_nam_1,
                                                              thuctapsinh.batdau_congtac_tts_thang_1)) if thuctapsinh.batdau_congtac_tts_nam_1 else ''

            if thuctapsinh.den_nay_1 == True:
                hhjp_0348_1 = '現在'
            else:
                hhjp_0348_1 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_congtac_tts_nam_1,
                                                                  thuctapsinh.ketthuc_congtac_tts_thang_1)) if thuctapsinh.ketthuc_congtac_tts_nam_1 else ''

            context['hhjp_0347_1'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_1, hhjp_0348_1))

            hhjp_0349_1 = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_1)
            hhjp_0350_1 = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_1.name_loaicongviec)
            context['hhjp_0349_1'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_1, hhjp_0350_1))

            hhjp_0347_1_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_congtac_tts_nam_1,
                                                                  thuctapsinh.batdau_congtac_tts_thang_1)) if thuctapsinh.batdau_congtac_tts_nam_1 else ''
            if thuctapsinh.den_nay_1 == True:
                hhjp_0348_1_v = 'Hiện nay'
            else:
                hhjp_0348_1_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_congtac_tts_nam_1,
                                                                      thuctapsinh.ketthuc_congtac_tts_thang_1)) if thuctapsinh.ketthuc_congtac_tts_nam_1 else ''
            context['hhjp_0347_1_v'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_1_v, hhjp_0348_1_v))

            hhjp_0349_1_v = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_v_1)
            hhjp_0350_1_v = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_1)
            context['hhjp_0349_1_v'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_1_v, hhjp_0350_1_v))

            ### Công tác 2
            hhjp_0347_2 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_congtac_tts_nam_2,
                                                              thuctapsinh.batdau_congtac_tts_thang_2)) if thuctapsinh.batdau_congtac_tts_nam_2 else ''

            if thuctapsinh.den_nay_2 == True:
                hhjp_0348_2 = '現在'
            else:
                hhjp_0348_2 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_congtac_tts_nam_2,
                                                                  thuctapsinh.ketthuc_congtac_tts_thang_2)) if thuctapsinh.ketthuc_congtac_tts_nam_2 else ''

            context['hhjp_0347_2'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_2, hhjp_0348_2))

            hhjp_0349_2 = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_2)
            hhjp_0350_2 = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_2.name_loaicongviec)
            context['hhjp_0349_2'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_2, hhjp_0350_2))

            hhjp_0347_2_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_congtac_tts_nam_2,
                                                                  thuctapsinh.batdau_congtac_tts_thang_2)) if thuctapsinh.batdau_congtac_tts_nam_2 else ''
            if thuctapsinh.den_nay_2 == True:
                hhjp_0348_2_v = 'Hiện nay'
            else:
                hhjp_0348_2_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_congtac_tts_nam_2,
                                                                      thuctapsinh.ketthuc_congtac_tts_thang_2)) if thuctapsinh.ketthuc_congtac_tts_nam_2 else ''

            context['hhjp_0347_2_v'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_2_v, hhjp_0348_2_v))

            hhjp_0349_2_v = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_v_2)
            hhjp_0350_2_v = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_2)
            context['hhjp_0349_2_v'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_2_v, hhjp_0350_2_v))

            ### Công tác 3
            hhjp_0347_3 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_congtac_tts_nam_3,
                                                              thuctapsinh.batdau_congtac_tts_thang_3)) if thuctapsinh.batdau_congtac_tts_nam_3 else ''

            if thuctapsinh.den_nay_3 == True:
                hhjp_0348_3 = '現在'
            else:
                hhjp_0348_3 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_congtac_tts_nam_3,
                                                                  thuctapsinh.ketthuc_congtac_tts_thang_3)) if thuctapsinh.ketthuc_congtac_tts_nam_3 else ''

            context['hhjp_0347_3'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_3, hhjp_0348_3))

            hhjp_0349_3 = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_3)
            hhjp_0350_3 = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_3.name_loaicongviec)
            context['hhjp_0349_3'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_3, hhjp_0350_3))

            hhjp_0347_3_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_congtac_tts_nam_3,
                                                                  thuctapsinh.batdau_congtac_tts_thang_3)) if thuctapsinh.batdau_congtac_tts_nam_3 else ''

            if thuctapsinh.den_nay_3 == True:
                hhjp_0348_3_v = 'Hiện nay'
            else:
                hhjp_0348_3_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_congtac_tts_nam_3,
                                                                      thuctapsinh.ketthuc_congtac_tts_thang_3)) if thuctapsinh.ketthuc_congtac_tts_nam_3 else ''

            context['hhjp_0347_3_v'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_3_v, hhjp_0348_3_v))

            hhjp_0349_3_v = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_v_3)
            hhjp_0350_3_v = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_3)
            context['hhjp_0349_3_v'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_3_v, hhjp_0350_3_v))

            ### Công tác 4
            hhjp_0347_4 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_congtac_tts_nam_4,
                                                              thuctapsinh.batdau_congtac_tts_thang_4)) if thuctapsinh.batdau_congtac_tts_nam_4 else ''

            if thuctapsinh.den_nay_4 == True:
                hhjp_0348_4 = '現在'
            else:
                hhjp_0348_4 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_congtac_tts_nam_4,
                                                                  thuctapsinh.ketthuc_congtac_tts_thang_4)) if thuctapsinh.ketthuc_congtac_tts_nam_4 else ''

            context['hhjp_0347_4'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_4, hhjp_0348_4))

            hhjp_0349_4 = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_4)
            hhjp_0350_4 = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_4.name_loaicongviec)
            context['hhjp_0349_4'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_4, hhjp_0350_4))
            hhjp_0347_4_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_congtac_tts_nam_4,
                                                                  thuctapsinh.batdau_congtac_tts_thang_4)) if thuctapsinh.batdau_congtac_tts_nam_4 else ''
            if thuctapsinh.den_nay_4 == True:
                hhjp_0348_4_v = 'Hiện nay'
            else:
                hhjp_0348_4_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_congtac_tts_nam_4,
                                                                      thuctapsinh.ketthuc_congtac_tts_thang_4)) if thuctapsinh.ketthuc_congtac_tts_nam_4 else ''
            context['hhjp_0347_4_v'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_4_v, hhjp_0348_4_v))

            hhjp_0349_4_v = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_v_4)
            hhjp_0350_4_v = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_4)
            context['hhjp_0349_4_v'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_4_v, hhjp_0350_4_v))

            ### Công tác 5
            hhjp_0347_5 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_congtac_tts_nam_5,
                                                              thuctapsinh.batdau_congtac_tts_thang_5)) if thuctapsinh.batdau_congtac_tts_nam_5 else ''

            if thuctapsinh.den_nay_5 == True:
                hhjp_0348_5 = '現在'
            else:
                hhjp_0348_5 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_congtac_tts_nam_5,
                                                                  thuctapsinh.ketthuc_congtac_tts_thang_5)) if thuctapsinh.ketthuc_congtac_tts_nam_5 else ''
            context['hhjp_0347_5'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_5, hhjp_0348_5))

            hhjp_0349_5 = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_5)
            hhjp_0350_5 = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_5.name_loaicongviec)
            context['hhjp_0349_5'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_5, hhjp_0350_5))

            hhjp_0347_5_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_congtac_tts_nam_5,
                                                                  thuctapsinh.batdau_congtac_tts_thang_5)) if thuctapsinh.batdau_congtac_tts_nam_5 else ''
            if thuctapsinh.den_nay_5 == True:
                hhjp_0348_5_v = 'Hiện nay'
            else:
                hhjp_0348_5_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_congtac_tts_nam_5,
                                                                      thuctapsinh.ketthuc_congtac_tts_thang_5)) if thuctapsinh.ketthuc_congtac_tts_nam_5 else ''

            context['hhjp_0347_5_v'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_5_v, hhjp_0348_5_v))

            hhjp_0349_5_v = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_v_5)
            hhjp_0350_5_v = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_5)
            context['hhjp_0349_5_v'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_5_v, hhjp_0350_5_v))

            ### Công tác 6
            hhjp_0347_6 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_congtac_tts_nam_6,
                                                              thuctapsinh.batdau_congtac_tts_thang_6)) if thuctapsinh.batdau_congtac_tts_nam_6 else ''

            if thuctapsinh.den_nay_6 == True:
                hhjp_0348_6 = '現在'
            else:
                hhjp_0348_6 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_congtac_tts_nam_6,
                                                                  thuctapsinh.ketthuc_congtac_tts_thang_6)) if thuctapsinh.ketthuc_congtac_tts_nam_6 else ''
            context['hhjp_0347_6'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_6, hhjp_0348_6))

            hhjp_0349_6 = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_6)
            hhjp_0350_6 = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_6.name_loaicongviec)
            context['hhjp_0349_6'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_6, hhjp_0350_6))

            hhjp_0347_6_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_congtac_tts_nam_6,
                                                                  thuctapsinh.batdau_congtac_tts_thang_6)) if thuctapsinh.batdau_congtac_tts_nam_6 else ''
            if thuctapsinh.den_nay_6 == True:
                hhjp_0348_6_v = 'Hiện nay'
            else:
                hhjp_0348_6_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_congtac_tts_nam_6,
                                                                      thuctapsinh.ketthuc_congtac_tts_thang_6)) if thuctapsinh.ketthuc_congtac_tts_nam_6 else ''

            context['hhjp_0347_6_v'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_6_v, hhjp_0348_6_v))

            hhjp_0349_6_v = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_v_6)
            hhjp_0350_6_v = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_6)
            context['hhjp_0349_6_v'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_6_v, hhjp_0350_6_v))

            ### Công tác 7
            hhjp_0347_7 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_congtac_tts_nam_7,
                                                              thuctapsinh.batdau_congtac_tts_thang_7)) if thuctapsinh.batdau_congtac_tts_nam_7 else ''

            if thuctapsinh.den_nay_7 == True:
                hhjp_0348_7 = '現在'
            else:
                hhjp_0348_7 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_congtac_tts_nam_7,
                                                                  thuctapsinh.ketthuc_congtac_tts_thang_7)) if thuctapsinh.ketthuc_congtac_tts_nam_7 else ''
            context['hhjp_0347_7'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_7, hhjp_0348_7))

            hhjp_0349_7 = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_7)
            hhjp_0350_7 = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_7.name_loaicongviec)
            context['hhjp_0349_7'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_7, hhjp_0350_7))

            hhjp_0347_7_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_congtac_tts_nam_7,
                                                                  thuctapsinh.batdau_congtac_tts_thang_7)) if thuctapsinh.batdau_congtac_tts_nam_7 else ''
            if thuctapsinh.den_nay_7 == True:
                hhjp_0348_7_v = 'Hiện nay'
            else:
                hhjp_0348_7_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_congtac_tts_nam_7,
                                                                      thuctapsinh.ketthuc_congtac_tts_thang_7)) if thuctapsinh.ketthuc_congtac_tts_nam_7 else ''

            context['hhjp_0347_7_v'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_7_v, hhjp_0348_7_v))

            hhjp_0349_7_v = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_v_7)
            hhjp_0350_7_v = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_7)
            context['hhjp_0349_7_v'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_7_v, hhjp_0350_7_v))

            if thuctapsinh.congviec_lienquan_1:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_1.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_1)
                context['ahh_0352'] = format_hoso.round_nam(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.round_nam(thuctapsinh.fill)
            elif thuctapsinh.congviec_lienquan_2:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_2.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_2)
                context['ahh_0352'] = format_hoso.round_nam(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.round_nam(thuctapsinh.fill)
            elif thuctapsinh.congviec_lienquan_3:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_3.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_3)
                context['ahh_0352'] = format_hoso.round_nam(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.round_nam(thuctapsinh.fill)
            elif thuctapsinh.congviec_lienquan_4:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_4.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_4)
                context['ahh_0352'] = format_hoso.round_nam(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.round_nam(thuctapsinh.fill)
            elif thuctapsinh.congviec_lienquan_5:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_5.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_5)
                context['ahh_0352'] = format_hoso.round_nam(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.round_nam(thuctapsinh.fill)
            elif thuctapsinh.congviec_lienquan_6:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_6.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_6)
                context['ahh_0352'] = format_hoso.round_nam(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.round_nam(thuctapsinh.fill)
            elif thuctapsinh.congviec_lienquan_7:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_7.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_7)
                context['ahh_0352'] = format_hoso.round_nam(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.round_nam(thuctapsinh.fill)

            if thuctapsinh.lichsunhapcu_tts == 'Có':
                context['hhjp_0360_1'] = u'■'
                context['hhjp_0360_4'] = u'□'
                if thuctapsinh.tucach_luutru_tts == 'Thực tập sinh kỹ năng':
                    context['hhjp_0360_2'] = u'■'
                    context['hhjp_0360_3'] = u'□'
                elif thuctapsinh.tucach_luutru_tts == 'Không phải thực tập sinh kỹ năng':
                    context['hhjp_0360_2'] = u'□'
                    context['hhjp_0360_3'] = u'■'
                else:
                    context['hhjp_0360_2'] = u'□'
                    context['hhjp_0360_3'] = u'□'

                if thuctapsinh.ngaydinhat_tts:
                    context['hhjp_0361'] = str(thuctapsinh.ngaydinhat_tts.year) + '.' + str(
                        thuctapsinh.ngaydinhat_tts.month) + '.' + str(thuctapsinh.ngaydinhat_tts.day)
                else:
                    context['hhjp_0361'] = ''
                if thuctapsinh.onhatden_tts:
                    context['hhjp_0362'] = str(thuctapsinh.onhatden_tts.year) + '.' + str(
                        thuctapsinh.onhatden_tts.month) + '.' + str(thuctapsinh.onhatden_tts.day)
                else:
                    context['hhjp_0362'] = ''

                if (thuctapsinh.batdau_kynang_tts and thuctapsinh.ketthuc_kynang_tts):
                    context['bs_0052'] = str(thuctapsinh.batdau_kynang_tts.year) + '年' + \
                                         str(thuctapsinh.batdau_kynang_tts.month) + '月' + \
                                         str(thuctapsinh.batdau_kynang_tts.day) + '日'
                    context['bs_0052_v'] = 'Ngày ' + str(thuctapsinh.batdau_kynang_tts.day) + \
                                           ' tháng ' + str(thuctapsinh.batdau_kynang_tts.month) + \
                                           ' năm ' + str(thuctapsinh.batdau_kynang_tts.year)
                    context['bs_0053'] = str(thuctapsinh.ketthuc_kynang_tts.year) + '年' + \
                                         str(thuctapsinh.ketthuc_kynang_tts.month) + '月' + \
                                         str(thuctapsinh.ketthuc_kynang_tts.day) + '日'
                    context['bs_0053_v'] = 'Ngày ' + str(thuctapsinh.ketthuc_kynang_tts.day) + \
                                           ' tháng ' + str(thuctapsinh.ketthuc_kynang_tts.month) + \
                                           ' năm ' + str(thuctapsinh.ketthuc_kynang_tts.year)
                    context['bs_0052_1'] = '■'
                else:
                    context['bs_0052'] = '年　月　日'
                    context['bs_0052_v'] = 'Ngày tháng năm'
                    context['bs_0053'] = '年　月　日'
                    context['bs_0053_v'] = 'Ngày tháng năm'
                    context['bs_0052_1'] = '□'

                if (thuctapsinh.batdau_xaydung_tts and thuctapsinh.ketthuc_xaydung_tts):
                    context['bs_0054'] = str(thuctapsinh.batdau_xaydung_tts.year) + '年' + \
                                         str(thuctapsinh.batdau_xaydung_tts.month) + '月' + \
                                         str(thuctapsinh.batdau_xaydung_tts.day) + '日'
                    context['bs_0054_v'] = 'Ngày ' + str(thuctapsinh.batdau_xaydung_tts.day) + \
                                           ' tháng ' + str(thuctapsinh.batdau_xaydung_tts.month) + \
                                           ' năm ' + str(thuctapsinh.batdau_xaydung_tts.year)
                    context['bs_0055'] = str(thuctapsinh.ketthuc_xaydung_tts.year) + '年' + \
                                         str(thuctapsinh.ketthuc_xaydung_tts.month) + '月' + \
                                         str(thuctapsinh.ketthuc_xaydung_tts.day) + '日'
                    context['bs_0055_v'] = 'Ngày ' + str(thuctapsinh.ketthuc_xaydung_tts.day) + \
                                           ' tháng ' + str(thuctapsinh.ketthuc_xaydung_tts.month) + \
                                           ' năm ' + str(thuctapsinh.ketthuc_xaydung_tts.year)
                    context['bs_0052_1'] = '■'

                else:
                    context['bs_0054'] = '年　月　日'
                    context['bs_0054_v'] = 'Ngày tháng năm'
                    context['bs_0055'] = '年　月　日'
                    context['bs_0055_v'] = 'Ngày tháng năm'
                    if (thuctapsinh.batdau_kynang_tts and thuctapsinh.ketthuc_kynang_tts):
                        context['bs_0052_1'] = '■'
                    else:
                        context['bs_0052_1'] = '□'

                if (thuctapsinh.batdau_ungvien_tts and thuctapsinh.ketthuc_ungvien_tts):
                    context['bs_0056'] = str(thuctapsinh.batdau_ungvien_tts.year) + '年' + \
                                         str(thuctapsinh.batdau_ungvien_tts.month) + '月' + \
                                         str(thuctapsinh.batdau_ungvien_tts.day) + '日'
                    context['bs_0056_v'] = 'Ngày ' + str(thuctapsinh.batdau_ungvien_tts.day) + \
                                           ' tháng ' + str(thuctapsinh.batdau_ungvien_tts.month) + \
                                           ' năm ' + str(thuctapsinh.batdau_ungvien_tts.year)
                    context['bs_0057'] = str(thuctapsinh.ketthuc_ungvien_tts.year) + '年' + \
                                         str(thuctapsinh.ketthuc_ungvien_tts.month) + '月' + \
                                         str(thuctapsinh.ketthuc_ungvien_tts.day) + '日'
                    context['bs_0057_v'] = 'Ngày ' + str(thuctapsinh.ketthuc_ungvien_tts.day) + \
                                           ' tháng ' + str(thuctapsinh.ketthuc_ungvien_tts.month) + \
                                           ' năm ' + str(thuctapsinh.ketthuc_ungvien_tts.year)
                    context['bs_0056_1'] = '■'
                else:
                    context['bs_0056'] = '年　月　日'
                    context['bs_0056_v'] = 'Ngày tháng năm'
                    context['bs_0057'] = '年　月　日'
                    context['bs_0057_v'] = 'Ngày tháng năm'
                    context['bs_0056_1'] = '□'
            else:
                context['hhjp_0360_4'] = u'■'
                context['hhjp_0360_1'] = u'□'

                context['hhjp_0360_2'] = u'□'
                context['hhjp_0360_3'] = u'□'

                context['hhjp_0361'] = ''
                context['hhjp_0362'] = ''
                context['bs_0052'] = '年　月　日'
                context['bs_0052_v'] = 'Ngày tháng năm'
                context['bs_0053'] = '年　月　日'
                context['bs_0053_v'] = 'Ngày tháng năm'
                context['bs_0054'] = '年　月　日'
                context['bs_0054_v'] = 'Ngày tháng năm'
                context['bs_0055'] = '年　月　日'
                context['bs_0055_v'] = 'Ngày tháng năm'
                context['bs_0056'] = '年　月　日'
                context['bs_0056_v'] = 'Ngày tháng năm'
                context['bs_0057'] = '年　月　日'
                context['bs_0057_v'] = 'Ngày tháng năm'
                context['bs_0056_1'] = '□'
                context['bs_0052_1'] = '□'

            if thuctapsinh.kinhnghiem_tts == 'Có':
                context['hhjp_0113_c'] = u'■'
                context['hhjp_0113_k'] = u'□'
            elif thuctapsinh.kinhnghiem_tts == 'Không':
                context['hhjp_0113_c'] = u'□'
                context['hhjp_0113_k'] = u'■'
            else:
                context['hhjp_0113_c'] = u'□'
                context['hhjp_0113_k'] = u'■'

            if thuctapsinh.phanloai_kehoach_thuctap_tts_1 == True:
                context['hhjp_0113_A'] = '■'
            else:
                context['hhjp_0113_A'] = '□'

            if thuctapsinh.phanloai_kehoach_thuctap_tts_2 == True:
                context['hhjp_0113_B'] = '■'
            else:
                context['hhjp_0113_B'] = '□'
            if thuctapsinh.phanloai_kehoach_thuctap_tts_3 == True:
                context['hhjp_0113_C'] = '■'
            else:
                context['hhjp_0113_C'] = '□'
            if thuctapsinh.phanloai_kehoach_thuctap_tts_4 == True:
                context['hhjp_0113_D'] = '■'
            else:
                context['hhjp_0113_D'] = '□'
            if thuctapsinh.phanloai_kehoach_thuctap_tts_5 == True:
                context['hhjp_0113_E'] = '■'
            else:
                context['hhjp_0113_E'] = '□'
            if thuctapsinh.phanloai_kehoach_thuctap_tts_6 == True:
                context['hhjp_0113_F'] = '■'
            else:
                context['hhjp_0113_F'] = '□'

            if thuctapsinh.knttkn_tu_tts:
                context['hhjp_0364'] = str(thuctapsinh.knttkn_tu_tts.year) + '.' + str(
                    thuctapsinh.knttkn_tu_tts.month) + '.' + str(thuctapsinh.knttkn_tu_tts.day)
            else:
                context['hhjp_0364'] = ''
            if thuctapsinh.knttkn_den_tts:
                context['hhjp_0365'] = str(thuctapsinh.knttkn_den_tts.year) + '.' + str(
                    thuctapsinh.knttkn_den_tts.month) + '.' + str(thuctapsinh.knttkn_den_tts.day)
            else:
                context['hhjp_0365'] = ''

            if thuctapsinh.buocloaibo_tts == u'Có':
                context['bs_0009_c'] = u'■'
                context['bs_0009_k'] = u'□'
                context['bs_0009'] = format_hoso.kiemtra(thuctapsinh.lydo_buocloaibo_tts)
                context['bs_0009_v'] = format_hoso.kiemtra(thuctapsinh.lydo_buocloaibo_tts_v)
            else:
                context['bs_0009_k'] = u'■'
                context['bs_0009_c'] = u'□'
                context['bs_0009'] = ''
                context['bs_0009_v'] = ''

            context['hhjp_0373'] = format_hoso.kiemtra(thuctapsinh.thongtinkhac_tts)
            context['hhjp_0373_v'] = format_hoso.kiemtra(thuctapsinh.thongtinkhac_tts_v)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_61_MOI(self, ma_thuctapsinh, document):
        thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
        thuctapsinh_danhsach = self.env['thuctapsinh.thuctapsinh'].search(
            [('donhang_tts', '=', thuctapsinh.donhang_tts.id)])
        if len(thuctapsinh_danhsach) < 4:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_61_MOI")], limit=1)
            print('61_MOI')
        else:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_61_MOI_1")], limit=1)
            print('61_1_MOI')
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)

            context = {}

            item = 1
            for tts in thuctapsinh_danhsach:
                if item == 1:
                    context['hhjp_0001'] = format_hoso.kiemtra(tts.ten_lt_tts)
                    context['hhjp_0204_1'] = format_hoso.kiemtra(tts.donhang_tts.congtypc2.name)
                    context['hhjp_0900_1'] = format_hoso.kiemtra(tts.donhang_tts.bophan)
                    context['hhjp_0901_1'] = format_hoso.nganhnghe(tts.donhang_tts.nganhnghe_xnpc.name_loaicongviec)
                    if tts.donhang_tts.hinhthuc_vieclam == 'Toàn thời gian':
                        context['hhjp_0332_1_1'] = '■'
                        context['hhjp_0332_2_1'] = '□'
                    elif tts.donhang_tts.hinhthuc_vieclam == 'Bán thời gian':
                        context['hhjp_0332_1_1'] = '□'
                        context['hhjp_0332_2_1'] = '■'
                    else:
                        context['hhjp_0332_1_1'] = '□'
                        context['hhjp_0332_2_1'] = '□'
                    if thuctapsinh.ten_han_tts:
                        context['hhjp_0001_h'] = format_hoso.kiemtra(tts.ten_han_tts)
                    else:
                        context['hhjp_0001_h'] = format_hoso.kiemtra(tts.ten_katakana_tts)
                    item += 1
                elif item == 2:
                    context['hhjp_0001_1'] = format_hoso.kiemtra(tts.ten_lt_tts)
                    context['hhjp_0204_2'] = format_hoso.kiemtra(tts.donhang_tts.congtypc2.name)
                    context['hhjp_0900_2'] = format_hoso.kiemtra(tts.donhang_tts.bophan)
                    context['hhjp_0901_2'] = format_hoso.nganhnghe(tts.donhang_tts.nganhnghe_xnpc.name_loaicongviec)

                    if tts.donhang_tts.hinhthuc_vieclam == 'Toàn thời gian':
                        context['hhjp_0332_1_2'] = '■'
                        context['hhjp_0332_2_2'] = '□'
                    elif tts.donhang_tts.hinhthuc_vieclam == 'Bán thời gian':
                        context['hhjp_0332_1_2'] = '□'
                        context['hhjp_0332_2_2'] = '■'
                    else:
                        context['hhjp_0332_1_2'] = '□'
                        context['hhjp_0332_2_2'] = '□'

                    if thuctapsinh.ten_han_tts:
                        context['hhjp_0001_h_1'] = format_hoso.kiemtra(tts.ten_han_tts)
                    else:
                        context['hhjp_0001_h_1'] = format_hoso.kiemtra(tts.ten_katakana_tts)
                    item += 1
                elif item == 3:
                    context['hhjp_0001_2'] = format_hoso.kiemtra(tts.ten_lt_tts)
                    context['hhjp_0204_3'] = format_hoso.kiemtra(tts.donhang_tts.congtypc2.name)
                    context['hhjp_0900_3'] = format_hoso.kiemtra(tts.donhang_tts.bophan)
                    context['hhjp_0901_3'] = format_hoso.nganhnghe(tts.donhang_tts.nganhnghe_xnpc.name_loaicongviec)

                    if tts.donhang_tts.hinhthuc_vieclam == 'Toàn thời gian':
                        context['hhjp_0332_1_3'] = '■'
                        context['hhjp_0332_2_3'] = '□'
                    elif tts.donhang_tts.hinhthuc_vieclam == 'Bán thời gian':
                        context['hhjp_0332_1_3'] = '□'
                        context['hhjp_0332_2_3'] = '■'
                    else:
                        context['hhjp_0332_1_3'] = '□'
                        context['hhjp_0332_2_3'] = '□'

                    if thuctapsinh.ten_han_tts:
                        context['hhjp_0001_h_2'] = format_hoso.kiemtra(tts.ten_han_tts)
                    else:
                        context['hhjp_0001_h_2'] = format_hoso.kiemtra(tts.ten_katakana_tts)
                    break

            context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban1)

            context['hhjp_0204'] = format_hoso.kiemtra(thuctapsinh.donhang_tts.congtypc2.name)
            context['hhjp_0900'] = format_hoso.kiemtra(thuctapsinh.donhang_tts.bophan)
            context['hhjp_0901'] = format_hoso.nganhnghe(thuctapsinh.donhang_tts.nganhnghe_xnpc.name_loaicongviec)

            context['hhjp_0207'] = format_hoso.kiemtra(thuctapsinh.donhang_tts.congtypc2.position_person_sign_n)
            context['hhjp_0207_1'] = format_hoso.kiemtra(thuctapsinh.donhang_tts.congtypc2.director_n)
            if len(thuctapsinh_danhsach) > 3:
                table_tts = []
                for thuctapsinh_danhsach in thuctapsinh_danhsach:
                    infor_tts = {}
                    infor_tts['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh_danhsach.ten_lt_tts)
                    infor_tts['hhjp_0204'] = format_hoso.kiemtra(thuctapsinh_danhsach.donhang_tts.congtypc2.name)

                    if tts.donhang_tts.hinhthuc_vieclam == 'Toàn thời gian':
                        infor_tts['hhjp_0332_1'] = '■'
                        infor_tts['hhjp_0332_2'] = '□'
                    elif tts.donhang_tts.hinhthuc_vieclam == 'Bán thời gian':
                        infor_tts['hhjp_0332_1'] = '□'
                        infor_tts['hhjp_0332_2'] = '■'
                    else:
                        infor_tts['hhjp_0332_1'] = '□'
                        infor_tts['hhjp_0332_2'] = '□'

                    infor_tts['hhjp_0900'] = format_hoso.kiemtra(thuctapsinh_danhsach.donhang_tts.bophan)
                    infor_tts['hhjp_0901'] = format_hoso.nganhnghe(
                        thuctapsinh_danhsach.donhang_tts.nganhnghe_xnpc.name_loaicongviec)
                    if thuctapsinh_danhsach.ten_han_tts:
                        infor_tts['hhjp_0001_h'] = format_hoso.kiemtra(thuctapsinh_danhsach.ten_han_tts)
                    else:
                        infor_tts['hhjp_0001_h'] = format_hoso.kiemtra(thuctapsinh_danhsach.ten_katakana_tts)
                    table_tts.append(infor_tts)
                context['tbl_thuctapsinh'] = table_tts

            if thuctapsinh.donhang_tts.mqh_voi_tts:
                context['hhjp_0333_1'] = '■'
                context['hhjp_0333_2'] = '□'
                context['hhjp_0333_3'] = '□'
            elif thuctapsinh.donhang_tts.mqh_tuthoiviec:
                context['hhjp_0333_1'] = '□'
                context['hhjp_0333_2'] = '■'
                context['hhjp_0333_3'] = '□'
            elif thuctapsinh.donhang_tts.mqh_khac:
                context['hhjp_0333_1'] = '□'
                context['hhjp_0333_2'] = '□'
                context['hhjp_0333_3'] = '■'
                context['bs_0010_a'] = format_hoso.kiemtra(thuctapsinh.donhang_tts.mqh_khac_lydo)
            else:
                context['hhjp_0333_3'] = '□'
                context['hhjp_0333_1'] = '□'
                context['hhjp_0333_2'] = '□'
                context['bs_0010_a'] = ''

            if thuctapsinh.donhang_tts.dudinh_quaylai:
                context['hhjp_0334_1'] = '■'
                context['hhjp_0334_2'] = '□'
                context['hhjp_0334_3'] = '□'
                context['hhjp_0204_kt'] = format_hoso.kiemtra(thuctapsinh.donhang_tts.dudinh_quaylai_tencty.name)
                context['hhjp_0900_kt'] = format_hoso.kiemtra(thuctapsinh.donhang_tts.dudinh_quaylai_bophan)
                context['hhjp_0901_kt'] = format_hoso.nganhnghe(
                    thuctapsinh.donhang_tts.dudinh_quaylai_cv.name_loaicongviec)
            elif thuctapsinh.donhang_tts.dudinh_khong_quaylai:
                context['hhjp_0334_1'] = '□'
                context['hhjp_0334_2'] = '■'
                context['hhjp_0334_3'] = '□'
                context['hhjp_0204_kt'] = ''
                context['hhjp_0900_kt'] = ''
                context['hhjp_0901_kt'] = ''
            elif thuctapsinh.donhang_tts.dudinh_khongco_ydinh:
                context['hhjp_0334_1'] = '□'
                context['hhjp_0334_2'] = '□'
                context['hhjp_0334_3'] = '■'
                context['hhjp_0204_kt'] = ''
                context['hhjp_0900_kt'] = ''
                context['hhjp_0901_kt'] = ''
            else:
                context['hhjp_0334_3'] = '□'
                context['hhjp_0334_1'] = '□'
                context['hhjp_0334_2'] = '□'
                context['hhjp_0204_kt'] = ''
                context['hhjp_0900_kt'] = ''
                context['hhjp_0901_kt'] = ''

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_62_MOI(self, document):
        print('62_MOI')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_62_MOI")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)

            context = {}
            daotaotruoc = self.env['daotao.daotaotruoc'].search(
                [('donhang_daotaotruoc', '=', document.donhang_hoso.id)], limit=1)
            context['ahhjp_0793_1'] = Listing(format_hoso.kiemtra(daotaotruoc.noidung_1))
            context['ahhjp_0794_1'] = Listing('%s \n %s' % (
                format_hoso.kiemtra(daotaotruoc.tencoquan_diachi_1.ten_han_nghiepdoan),
                format_hoso.kiemtra(daotaotruoc.diachi_coquan_1)))
            if daotaotruoc.uythac_1 == 'Có':
                context['ahhjp_0795_c_1'] = '■'
                context['ahhjp_0795_k_1'] = '□'
            elif daotaotruoc.uythac_1 == 'Không':
                context['ahhjp_0795_c_1'] = '□'
                context['ahhjp_0795_k_1'] = '■'
            else:
                context['ahhjp_0795_c_1'] = '□'
                context['ahhjp_0795_k_1'] = '□'
            context['ahhjp_0796_1'] = Listing(
                '%s \n %s' % (format_hoso.kiemtra(daotaotruoc.tencoso_diachi_1.ten_coso_jp),
                              format_hoso.kiemtra(daotaotruoc.diachi_coso_1)))
            context['ahhjp_0797_1'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_batdau_1)
            context['ahhjp_0798_1'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_ketthuc_1)
            context['ahhjp_0799_1'] = format_hoso.kiemtra(daotaotruoc.sogio_1)
            context['bhhjp_0793_2'] = Listing(format_hoso.kiemtra(daotaotruoc.noidung_2))
            context['bhhjp_0794_2'] = Listing('%s \n %s' % (
                format_hoso.kiemtra(daotaotruoc.tencoquan_diachi_2.ten_han_nghiepdoan),
                format_hoso.kiemtra(daotaotruoc.diachi_coquan_2)))
            if daotaotruoc.uythac_2 == 'Có':
                context['ahhjp_0795_c_2'] = '■'
                context['ahhjp_0795_k_2'] = '□'
            elif daotaotruoc.uythac_2 == 'Không':
                context['ahhjp_0795_c_2'] = '□'
                context['ahhjp_0795_k_2'] = '■'
            else:
                context['ahhjp_0795_c_2'] = '□'
                context['ahhjp_0795_k_2'] = '□'
            context['bhhjp_0796_2'] = Listing(
                '%s \n %s' % (format_hoso.kiemtra(daotaotruoc.tencoso_diachi_2.ten_coso_jp),
                              format_hoso.kiemtra(daotaotruoc.diachi_coso_2)))
            context['bhhjp_0797_2'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_batdau_2)
            context['bhhjp_0798_2'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_ketthuc_2)
            context['bhhjp_0799_2'] = format_hoso.kiemtra(daotaotruoc.sogio_2)
            context['chhjp_0793_3'] = Listing(format_hoso.kiemtra(daotaotruoc.noidung_3))
            context['chhjp_0794_3'] = Listing('%s \n %s' % (
                format_hoso.kiemtra(daotaotruoc.tencoquan_diachi_3.ten_han_nghiepdoan),
                format_hoso.kiemtra(daotaotruoc.diachi_coquan_3)))
            if daotaotruoc.uythac_3 == 'Có':
                context['ahhjp_0795_c_3'] = '■'
                context['ahhjp_0795_k_3'] = '□'
            elif daotaotruoc.uythac_3 == 'Không':
                context['ahhjp_0795_c_3'] = '□'
                context['ahhjp_0795_k_3'] = '■'
            else:
                context['ahhjp_0795_c_3'] = '□'
                context['ahhjp_0795_k_3'] = '□'

            context['chhjp_0796_3'] = Listing(
                '%s \n %s' % (format_hoso.kiemtra(daotaotruoc.tencoso_diachi_3.ten_coso_jp),
                              format_hoso.kiemtra(daotaotruoc.diachi_coso_3)))
            context['chhjp_0797_3'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_batdau_3)
            context['chhjp_0798_3'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_ketthuc_3)
            context['chhjp_0799_3'] = format_hoso.kiemtra(daotaotruoc.sogio_3)
            context['hhjp_0800'] = format_hoso.kiemtra(daotaotruoc.tongsogio)
            if daotaotruoc.noidung_ngoai_1:
                context['ahhjp_0801_1'] = Listing(format_hoso.kiemtra(daotaotruoc.noidung_ngoai_1))
                context['ahhjp_0802_1'] = Listing('%s \n %s' % (
                    format_hoso.kiemtra(daotaotruoc.ten_diachi_ngoai_1.ten_han_nghiepdoan),
                    format_hoso.kiemtra(daotaotruoc.diachi_coquan_ngoai_1)))
                context['ahhjp_0803_1'] = Listing(
                    '%s \n %s' % (format_hoso.kiemtra(daotaotruoc.tencoso_diachi_ngoai_1.ten_coquan_h),
                                  format_hoso.kiemtra(daotaotruoc.diachi_coso_ngoai_1)))
                context['ahhjp_0804_1'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_batdau_ngoai_1)
                context['ahhjp_0805_1'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_ketthuc_ngoai_1)
                context['ahhjp_0806_1'] = format_hoso.kiemtra(daotaotruoc.sogio_ngoai_1)
            if daotaotruoc.loai_tochuc_1 == 'Cơ quan công cộng':
                context['ahhjp_0808_1'] = '■'
                context['ahhjp_0802_2'] = '□'
                context['ahhjp_0802_3'] = '□'
            elif daotaotruoc.loai_tochuc_1 == 'Cơ sở giáo dục':
                context['ahhjp_0808_1'] = '□'
                context['ahhjp_0802_2'] = '■'
                context['ahhjp_0802_3'] = '□'
            elif daotaotruoc.loai_tochuc_1 == 'Tổ chức tư nhân nước ngoài':
                context['ahhjp_0808_1'] = '□'
                context['ahhjp_0802_2'] = '□'
                context['ahhjp_0802_3'] = '■'
            else:
                context['ahhjp_0808_1'] = '□'
                context['ahhjp_0802_2'] = '□'
                context['ahhjp_0802_3'] = '□'
            if daotaotruoc.noidung_ngoai_2:
                context['bhhjp_0801_2'] = Listing(format_hoso.kiemtra(daotaotruoc.noidung_ngoai_2))
                context['bhhjp_0802_2'] = Listing('%s \n %s' % (
                    format_hoso.kiemtra(daotaotruoc.ten_diachi_ngoai_2.ten_han_nghiepdoan),
                    format_hoso.kiemtra(daotaotruoc.diachi_coquan_ngoai_2)))

                context['bhhjp_0803_2'] = Listing(
                    '%s \n %s' % (format_hoso.kiemtra(daotaotruoc.tencoso_diachi_ngoai_2.ten_coquan_h),
                                  format_hoso.kiemtra(daotaotruoc.diachi_coso_ngoai_2)))
                context['bhhjp_0804_2'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_batdau_ngoai_2)
                context['bhhjp_0805_2'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_ketthuc_ngoai_2)
                context['bhhjp_0806_2'] = format_hoso.kiemtra(daotaotruoc.sogio_ngoai_2)

            if daotaotruoc.loai_tochuc_2 == 'Cơ quan công cộng':
                context['bhhjp_0808_1'] = '■'
                context['bhhjp_0802_4'] = '□'
                context['bhhjp_0802_3'] = '□'
            elif daotaotruoc.loai_tochuc_2 == 'Cơ sở giáo dục':
                context['bhhjp_0808_1'] = '□'
                context['bhhjp_0802_4'] = '■'
                context['bhhjp_0802_3'] = '□'
            elif daotaotruoc.loai_tochuc_2 == 'Tổ chức tư nhân nước ngoài':
                context['bhhjp_0808_1'] = '□'
                context['bhhjp_0802_4'] = '□'
                context['bhhjp_0802_3'] = '■'
            else:
                context['bhhjp_0808_1'] = '□'
                context['bhhjp_0802_4'] = '□'
                context['bhhjp_0802_3'] = '□'

            if daotaotruoc.noidung_ngoai_3:
                context['chhjp_0801_3'] = Listing(format_hoso.kiemtra(daotaotruoc.noidung_ngoai_3))
                context['chhjp_0802_3'] = Listing('%s \n %s' % (
                    format_hoso.kiemtra(daotaotruoc.ten_diachi_ngoai_3.ten_han_nghiepdoan),
                    format_hoso.kiemtra(daotaotruoc.diachi_coquan_ngoai_3)))
                context['chhjp_0803_3'] = Listing(
                    '%s \n %s' % (format_hoso.kiemtra(daotaotruoc.tencoso_diachi_ngoai_3.ten_coquan_h),
                                  format_hoso.kiemtra(daotaotruoc.diachi_coso_ngoai_3)))
                context['chhjp_0804_3'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_batdau_ngoai_3)
                context['chhjp_0805_3'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_ketthuc_ngoai_3)
                context['chhjp_0806_3'] = format_hoso.kiemtra(daotaotruoc.sogio_ngoai_3)

            if daotaotruoc.loai_tochuc_3 == 'Cơ quan công cộng':
                context['chhjp_0808_1'] = '■'
                context['chhjp_0802_2'] = '□'
                context['chhjp_0802_4'] = '□'
            elif daotaotruoc.loai_tochuc_3 == 'Cơ sở giáo dục':
                context['chhjp_0808_1'] = '□'
                context['chhjp_0802_2'] = '■'
                context['chhjp_0802_4'] = '□'
            elif daotaotruoc.loai_tochuc_3 == 'Tổ chức tư nhân nước ngoài':
                context['chhjp_0808_1'] = '□'
                context['chhjp_0802_2'] = '□'
                context['chhjp_0802_4'] = '■'
            else:
                context['chhjp_0808_1'] = '□'
                context['chhjp_0802_2'] = '□'
                context['chhjp_0802_4'] = '□'
            context['hhjp_0807'] = format_hoso.kiemtra(daotaotruoc.tongsogio_ngoai)
            table_tts = []
            for thuctapsinh_hoso in document.thuctapsinh_hoso:
                infor_tts = {}
                infor_tts['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh_hoso.ten_lt_tts)
                infor_tts['hhjp_0902'] = format_hoso.convert_date(thuctapsinh_hoso.ngaynhapcanh_tts)
                infor_tts['hhjp_0599'] = format_hoso.kiemtra(thuctapsinh_hoso.thongtinkhac_tts)
                table_tts.append(infor_tts)
            context['tbl_tts'] = table_tts
            context['hhjp_0596'] = format_hoso.convert_date(daotaotruoc.thoigian_batdau_daotaotruoc)
            context['hhjp_0597'] = format_hoso.convert_date(daotaotruoc.thoigian_ketthuc_daotaotruoc)
            context['hhjp_0600'] = format_hoso.convert_date(daotaotruoc.nguoi_lamdon)
            context['hhjp_0097'] = format_hoso.kiemtra(
                daotaotruoc.donhang_daotaotruoc.nghiepdoan_donhang.ten_han_nghiepdoan)
            context['hhjp_0103'] = '%s  %s' % (
                format_hoso.kiemtra(
                    daotaotruoc.donhang_daotaotruoc.nghiepdoan_donhang.chucvu_daidien_phienam_nghiepdoan),
                format_hoso.kiemtra(daotaotruoc.donhang_daotaotruoc.nghiepdoan_donhang.ten_daidien_han_nghiepdoan))

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None
