# -*- coding: utf-8 -*-
import codecs
import datetime
import logging
from builtins import print
from io import BytesIO
from tempfile import NamedTemporaryFile
from docxtpl import DocxTemplate, Listing
from odoo import models, fields, api

# from server.doc.conf import odoo_cover_default
from . import format_hoso
from . import intern_utils

_logger = logging.getLogger(__name__)


class HoSo(models.Model):
    _name = 'hoso.hoso'
    _rec_name = 'donhang_hoso'
    _order = 'id asc'

    state = fields.Selection(selection=[('state1', 'Năm thứ ba'),
                                        ('state2', 'Năm thứ hai'),
                                        ('state3', 'Năm thứ nhất'),
                                        ('state4', 'Trước khi nhập quốc'), ], default='state1',
                             track_visibility='onchange', )

    num_before_done = fields.Integer(default='2')
    num_before_late = fields.Integer(default='3')
    num_before_pre = fields.Integer(default='4')

    num_first_done = fields.Integer(default='6')
    num_first_late = fields.Integer(default='7')
    num_first_pre = fields.Integer(default='8')

    num_second_done = fields.Integer(default='10')
    num_second_late = fields.Integer(default='11')
    num_second_pre = fields.Integer(default='12')

    num_third_done = fields.Integer(default='14')
    num_third_late = fields.Integer(default='15')
    num_third_pre = fields.Integer(default='16')

    num_sum = fields.Integer(compute="calculator_sum")
    num_done = fields.Integer(compute="calculator_done")
    num_late = fields.Integer(compute="calculator_late")
    num_pre = fields.Integer(compute="calculator_pre")

    @api.onchange('state', 'num_done', 'num_late', 'num_pre')
    def calculator(self):
        if (self.state == 'state4'):
            self.num_done = self.num_before_done
            self.num_late = self.num_before_late
            self.num_pre = self.num_before_pre
        elif (self.state == 'state3'):
            self.num_done = self.num_first_done
            self.num_late = self.num_first_late
            self.num_pre = self.num_first_pre
        elif (self.state == 'state3'):
            self.num_done = self.num_second_done
            self.num_late = self.num_second_late
            self.num_pre = self.num_second_pre
        else:
            self.num_done = self.num_third_done
            self.num_late = self.num_third_late
            self.num_pre = self.num_third_pre
        self.num_sum = self.num_done + self.num_pre + self.num_late

    @api.onchange('state', 'num_done', 'num_late', 'num_pre')
    def calculator_sum(self):
        self.num_sum = self.num_done + self.num_pre + self.num_late

    @api.onchange('state', 'num_done', 'num_late', 'num_pre')
    def calculator_done(self):
        if (self.state == 'state4'):
            self.num_done = self.num_before_done
        elif (self.state == 'state3'):
            self.num_done = self.num_first_done
        elif (self.state == 'state2'):
            self.num_done = self.num_second_done
        else:
            self.num_done = self.num_third_done

    @api.onchange('state', 'num_done', 'num_late', 'num_pre')
    def calculator_late(self):
        if (self.state == 'state4'):
            self.num_late = self.num_before_late
        elif (self.state == 'state3'):
            self.num_late = self.num_first_late
        elif (self.state == 'state2'):
            self.num_late = self.num_second_late
        else:
            self.num_late = self.num_third_late

    @api.onchange('state', 'num_done', 'num_late', 'num_pre')
    def calculator_pre(self):

        if (self.state == 'state4'):

            self.num_pre = self.num_before_pre
        elif (self.state == 'state3'):

            self.num_pre = self.num_first_pre
        elif (self.state == 'state2'):

            self.num_pre = self.num_second_pre
        else:

            self.num_pre = self.num_third_pre

    nhapquoc = fields.Date(string="Ngày nhập quốc", related='donhang_hoso.thoigian_donhang')  #

    donhang_hoso = fields.Many2one(comodel_name='donhang.donhang', string='Đơn hàng', required=True, ondelete='cascade')
    xinghiep_hoso = fields.Many2one(comodel_name='xinghiep.xinghiep', string='Xí nghiệp',
                                    related='donhang_hoso.xinghiep_donhang')

    # Cong Viec Ho So
    ma_congviec = fields.Char(string='Mã công việc', related='donhang_hoso.ma_congviec_donhang')
    nganhnghe = fields.Many2one(comodel_name='nganhnghe.nganhnghe', string='Ngành nghề',
                                related='donhang_hoso.nganhnghe_donhang')
    loaicongviec = fields.Many2one(comodel_name='loaicongviec.loaicongviec', related='donhang_hoso.loainghe_donhang',
                                   string='Loại công việc')
    congviec = fields.Many2one(comodel_name='congviec.congviec', string='Công việc',
                               related='donhang_hoso.congviec_donhang')

    # Ten File Word
    file_1 = fields.Boolean()
    file_2 = fields.Boolean()
    file_3 = fields.Boolean()
    file_4 = fields.Boolean()
    file_5 = fields.Boolean()
    file_6 = fields.Boolean()
    file_7 = fields.Boolean()
    file_8 = fields.Boolean()
    file_9 = fields.Boolean()
    file_10 = fields.Boolean()
    file_11 = fields.Boolean()
    file_12 = fields.Boolean()
    file_13 = fields.Boolean()
    file_14 = fields.Boolean()
    file_15 = fields.Boolean()
    file_16 = fields.Boolean()
    file_17 = fields.Boolean()
    file_18 = fields.Boolean()
    file_19 = fields.Boolean()
    file_20 = fields.Boolean()
    file_21 = fields.Boolean()
    file_22 = fields.Boolean()
    file_23 = fields.Boolean()
    file_24 = fields.Boolean()
    file_25 = fields.Boolean()
    file_all = fields.Boolean()

    @api.onchange('giaidoan_hoso')
    def onchange_method_giaidoan_hoso(self):
        self.file_1 = False
        self.file_2 = False
        self.file_3 = False
        self.file_4 = False
        self.file_5 = False
        self.file_6 = False
        self.file_7 = False
        self.file_8 = False
        self.file_9 = False
        self.file_10 = False
        self.file_11 = False
        self.file_12 = False
        self.file_13 = False
        self.file_14 = False
        self.file_15 = False
        self.file_16 = False
        self.file_17 = False
        self.file_18 = False
        self.file_19 = False
        self.file_20 = False
        self.file_21 = False
        self.file_22 = False
        self.file_23 = False
        self.file_24 = False
        self.file_25 = False
        self.file_all = False

    @api.multi
    @api.onchange('file_all')
    def onchange_method_file_all(self):
        if self.file_all == True:
            if self.giaidoan_hoso == '1 go':
                self.file_1 = True
                self.file_2 = True
                self.file_3 = True
                self.file_4 = True
                self.file_5 = False
                self.file_6 = False
                self.file_7 = True
                self.file_8 = True
                self.file_9 = True
                self.file_10 = True
                self.file_11 = True
                self.file_12 = True
                self.file_13 = True
                self.file_14 = True
                self.file_15 = True
                self.file_16 = False
                self.file_17 = True
                self.file_18 = True
                self.file_19 = True
                self.file_20 = True
                self.file_21 = True
                self.file_22 = True
                self.file_23 = True
                self.file_24 = True
                self.file_25 = True
            else:
                self.file_1 = True
                self.file_2 = True
                self.file_3 = False
                self.file_4 = False
                self.file_5 = True
                self.file_6 = True
                self.file_7 = True
                self.file_8 = True
                self.file_9 = True
                self.file_10 = True
                self.file_11 = True
                self.file_12 = True
                self.file_13 = True
                self.file_14 = True
                self.file_15 = False
                self.file_16 = True
                self.file_17 = True
                self.file_18 = True
                self.file_19 = True
                self.file_20 = True
                self.file_21 = True
                self.file_22 = True
                self.file_23 = True
                self.file_24 = True
                self.file_25 = True
        else:
            self.file_1 = False
            self.file_2 = False
            self.file_3 = False
            self.file_4 = False
            self.file_5 = False
            self.file_6 = False
            self.file_7 = False
            self.file_8 = False
            self.file_9 = False
            self.file_10 = False
            self.file_11 = False
            self.file_12 = False
            self.file_13 = False
            self.file_14 = False
            self.file_15 = False
            self.file_16 = False
            self.file_17 = False
            self.file_18 = False
            self.file_19 = False
            self.file_20 = False
            self.file_21 = False
            self.file_22 = False
            self.file_23 = False
            self.file_24 = False
            self.file_25 = False

    file_26 = fields.Boolean()  # file_50
    file_27 = fields.Boolean()  # file_51
    file_28 = fields.Boolean()  # file_52
    file_29 = fields.Boolean()  # file_57
    file_30 = fields.Boolean()  # file_58
    file_31 = fields.Boolean()  # file_59
    file_32 = fields.Boolean()  # file_61
    file_33 = fields.Boolean()  # file_64
    file_34 = fields.Boolean()  # file_63
    file_35 = fields.Boolean()  # file_60
    file_36 = fields.Boolean()  # file_62
    file_37 = fields.Boolean()  # file_53
    file_all_pc = fields.Boolean()

    @api.onchange('file_all_pc')
    def onchange_method_file_all_pc(self):
        if self.file_all_pc == True:
            if self.giaidoan_pc == '1 go':
                self.file_26 = True
                self.file_27 = True
                self.file_28 = True
                self.file_29 = True
                self.file_30 = True
                self.file_31 = True
                self.file_32 = True
                self.file_33 = True
                self.file_34 = True
                self.file_35 = True
                self.file_36 = True
                self.file_37 = True
            elif self.giaidoan_pc == '3 go':
                self.file_26 = True
                self.file_27 = True
                self.file_28 = True
                self.file_29 = True
                self.file_30 = True
                self.file_31 = True
                self.file_32 = True
                self.file_33 = True
        else:
            self.file_26 = False
            self.file_27 = False
            self.file_28 = False
            self.file_29 = False
            self.file_30 = False
            self.file_31 = False
            self.file_32 = False
            self.file_33 = False
            self.file_34 = False
            self.file_35 = False
            self.file_36 = False
            self.file_37 = False

    chinhanh_hoso = fields.Many2one(comodel_name='xinghiep.chinhanh', string='Chi nhánh')

    @api.onchange('donhang_hoso')
    def onchange_method(self):
        self.xinghiep_hoso = self.donhang_hoso.xinghiep_donhang

    giaidoan_hoso = fields.Selection(string='Giai đoạn',
                                     selection=[('1 go', '1 go'),
                                                ('2 go', '2 go'),
                                                ('3 go', '3 go')], default='1 go')

    giaidoan_pc = fields.Selection(string='Phân loại thực tập sinh', selection=[('1 go', '1 go'),
                                                                                ('3 go', '3 go')],
                                   related="donhang_hoso.giaidoan_phi")

    kieutts_hoso = fields.Boolean(string='Kiểu thực tập sinh')
    ngay_lapvanban = fields.Date(string='Ngày lập văn bản', default=fields.Date.today())
    ngay_lapvanban1 = fields.Date(string='Ngày lập văn bản', default=fields.Date.today())

    thuctapsinh_hoso = fields.Many2many(comodel_name='thuctapsinh.thuctapsinh', string='Danh sách thực tập sinh',
                                        compute='_thuctapsinh_hoso')

    @api.multi
    @api.depends('donhang_hoso')
    def _thuctapsinh_hoso(self):
        related_ids = []
        if self.donhang_hoso:
            # Shearch danh sach thuc tap sinh trong thuctapsinh.thuctapsinh
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('donhang_tts', '=', self.donhang_hoso.id)],
                                                                     order="id asc")

            # Append danh sach thuc tap sinh dang theo hoc
            for tts in thuctapsinh:
                related_ids.append(tts.id)

            # Gan vao invoices_promoted
            self.thuctapsinh_hoso = self.env['thuctapsinh.thuctapsinh'].search([('id', 'in', related_ids)])

    # hoso_tts = fields.One2many(comodel_name='hoso.thuctapsinh', inverse_name='hoso_tts', string='Báo cáo thực tập sinh')
    #
    # @api.onchange('thuctapsinh_hoso')
    # def onchange_method_thuctapsinh_hoso(self):
    #     if self.thuctapsinh_hoso:
    #         vals_tts = []
    #         for thuctapsinh in self.thuctapsinh_hoso:
    #             vals_tts_row = {}
    #
    #             vals_tts_row['ma_tts'] = thuctapsinh.ma_tts
    #             vals_tts_row['ten_han_tts'] = thuctapsinh.id
    #             tts = self.env['hoso.thuctapsinh'].create(vals_tts_row)
    #             vals_tts.append(tts.id)
    #
    #         self.hoso_tts = [(6, 0, vals_tts)]
    #     else:
    #         self.hoso_tts = ''

    @api.multi
    def baocao_button(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/hoso/%s' % (self.id),
            'target': 'self', }

    @api.multi
    def baocao_button_save(self):
        pass

    @api.multi
    def luutru_button_save_pc(self):
        pass

    @api.multi
    def baocao_button_CTPC(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/phaicu/%s' % (self.id),
            'target': 'self', }

    @api.multi
    def baocao_hoso_1(self, document):
        print('1')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_1")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            context['hhjp_0014'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep)
            context['hhjp_0014_1'] = format_hoso.kiemtra(document.xinghiep_hoso.chucvu_daidien_han)
            context['hhjp_0097'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_han_nghiepdoan)
            context['hhjp_0103_1'] = format_hoso.kiemtra(
                document.donhang_hoso.nghiepdoan_donhang.chucvu_daidien_phienam_nghiepdoan)
            context['hhjp_0103'] = format_hoso.kiemtra(
                document.donhang_hoso.nghiepdoan_donhang.ten_daidien_han_nghiepdoan)
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_2(self, ma_thuctapsinh, document):
        print('2')
        nhanvien = len(document.donhang_hoso.xinghiep_donhang.danhsach_nhanvien_cc)
        chinhanh = 0
        if document.donhang_hoso.chinhanh_xinghiep:
            chinhanh += 1
        else:
            chinhanh += 0

        if document.donhang_hoso.chinhanh_xinghiep_2:
            chinhanh += 1
        else:
            chinhanh += 0

        if document.donhang_hoso.chinhanh_xinghiep_3:
            chinhanh += 1
        else:
            chinhanh += 0

        if nhanvien > 6 and chinhanh == 1:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_2_1")], limit=1)
            demo = 'file_2_1'
        elif nhanvien > 6 and chinhanh == 2:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_2_3")], limit=1)
            demo = 'file_2_3'
        elif nhanvien > 6 and chinhanh == 3:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_2_3_1")], limit=1)
            demo = 'file_2_3_1'
        elif nhanvien < 6 and chinhanh == 1:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_2")], limit=1)
            demo = 'file_2'
        elif nhanvien < 6 and chinhanh == 2:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_2_2")], limit=1)
            demo = 'file_2_2'
        elif nhanvien < 6 and chinhanh == 3:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_2_2_1")], limit=1)
            demo = 'file_2_2_1'
        else:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_2")], limit=1)
            demo = 'file_2'
        print(demo)

        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}
            giaidoan = self.env['giaidoan.giaidoan'].search([('ten_giaidoan', '=', '１号')], limit=1)
            donhang_congviec = self.env['chitiet.chitiet'].search([('donhang_chitiet', '=', document.donhang_hoso.id),
                                                                   ('giaidoan', '=', giaidoan.id)], limit=1)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)

            hopdonglaodong = self.env['hopdong.hopdong'].search([('thuctapsinh_hopdong', '=', thuctapsinh.id),
                                                                 ('donhang_hopdong', '=', document.donhang_hoso.id),
                                                                 ('giaidoan_hopdong', '=', document.giaidoan_hoso)],
                                                                limit=1)

            daotaosau = self.env['daotao.daotaosau'].search([('donhang_daotaosau', '=', thuctapsinh.donhang_tts.id),
                                                             ('xinghiep_daotaosau', '=',
                                                              thuctapsinh.xinghiep_thuctapsinh.id)], limit=1)

            kehoachdoatao = self.env['thuctapsinh.kehoachdoatao'].search([])
            context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
            context['hhjp_0811'] = format_hoso.kiemtra(
                hopdonglaodong.donhang_hopdong.xinghiep_donhang.so_chapnhan_daotao)
            context['hhjp_0009'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_furi_xnghiep)
            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)

            if document.xinghiep_hoso.sobuudien_xnghiep:
                context['bs_0006'] = document.xinghiep_hoso.sobuudien_xnghiep[
                                     0:3] + '-' + document.xinghiep_hoso.sobuudien_xnghiep[
                                                  3:]
            else:
                context['bs_0006'] = ''

            context['hhjp_0011'] = format_hoso.kiemtra(document.xinghiep_hoso.diachi_han_xnghiep)
            context['hhjp_0012'] = format_hoso.kiemtra(document.xinghiep_hoso.sdt_xnghiep)
            context['hhjp_0013'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_furi_xnghiep)
            context['hhjp_0014'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep)
            context['hhjp_0015'] = format_hoso.kiemtra(document.xinghiep_hoso.phapnhan_xinghiep)
            item = 1
            if nhanvien <= 6:
                for nhanvien_cc in document.xinghiep_hoso.danhsach_nhanvien_cc:
                    if item <= 6:
                        context['hhjp_0016_furi_%s' % item] = format_hoso.kiemtra(nhanvien_cc.ten_phienam_nhanvien_cc)
                        context['hhjp_0016_han_%s' % item] = format_hoso.kiemtra(nhanvien_cc.ten_han_nhanvien_cc)
                        context['bs_0001_%s' % item] = format_hoso.kiemtra(nhanvien_cc.chucdanh_han_nhanvien_cc)

                        if nhanvien_cc.sobuudien_nhanvien:
                            context['bs_0003_%s' % item] = nhanvien_cc.sobuudien_nhanvien[
                                                           0:3] + '-' + nhanvien_cc.sobuudien_nhanvien[
                                                                        3:]
                        else:
                            context['bs_0003_%s' % item] = ''

                        context['bs_0002_%s' % item] = format_hoso.kiemtra(nhanvien_cc.diachi_nhanvien_cc)
                        item += 1
                    else:
                        break
            else:
                table_nhanvien_cc = []
                for nhanvien_cc in document.xinghiep_hoso.danhsach_nhanvien_cc:
                    infor_nhanvien_cc = {}
                    infor_nhanvien_cc['hhjp_0016_furi'] = format_hoso.kiemtra(nhanvien_cc.ten_phienam_nhanvien_cc)
                    infor_nhanvien_cc['hhjp_0016_han'] = format_hoso.kiemtra(nhanvien_cc.ten_han_nhanvien_cc)
                    infor_nhanvien_cc['bs_0001'] = format_hoso.kiemtra(nhanvien_cc.chucdanh_han_nhanvien_cc)
                    infor_nhanvien_cc['bs_0002'] = format_hoso.kiemtra(nhanvien_cc.diachi_nhanvien_cc)
                    if nhanvien_cc.sobuudien_nhanvien:
                        infor_nhanvien_cc['bs_0003'] = nhanvien_cc.sobuudien_nhanvien[
                                                       0:3] + '-' + nhanvien_cc.sobuudien_nhanvien[
                                                                    3:]
                    else:
                        infor_nhanvien_cc['bs_0003'] = ''

                    table_nhanvien_cc.append(infor_nhanvien_cc)

                context['tbl_nhanvien_cc'] = table_nhanvien_cc

            context['hhjp_0017'] = format_hoso.kiemtra(document.xinghiep_hoso.nganhmax_xnghiep.name_phannganh)
            context['hhjp_0018'] = format_hoso.kiemtra(document.xinghiep_hoso.nganhmin_xnghiep.name_phannganhbe)

            if chinhanh == 1:

                for chinhanh in thuctapsinh.xinghiep_thuctapsinh.chinhanh_xinghiep:
                    context['hhjp_0040_1'] = format_hoso.kiemtra(chinhanh.ten_chinhanh_furl)
                    context['hhjp_0041_1'] = format_hoso.kiemtra(chinhanh.ten_chinhanh_han)

                    if chinhanh.sobuudien_chinhanh:
                        context['bs_0005'] = chinhanh.sobuudien_chinhanh[
                                             0:3] + '-' + chinhanh.sobuudien_chinhanh[
                                                          3:]
                    else:
                        context['bs_0005'] = ''

                    context['hhjp_0042'] = format_hoso.kiemtra(chinhanh.diachi_chinhanh)
                    context['hhjp_0043'] = format_hoso.kiemtra(chinhanh.sdt_chinhanh)
                    item += 1

                for danhsach_nhanvien in document.xinghiep_hoso.danhsach_nhanvien:
                    if danhsach_nhanvien.vaitro_three == True:
                        context['hhjp_0047'] = format_hoso.kiemtra(danhsach_nhanvien.ten_phienam_nhanvien)
                        context['hhjp_0048'] = format_hoso.kiemtra(danhsach_nhanvien.ten_han_nhanvien)
                        context['hhjp_0058'] = format_hoso.kiemtra(danhsach_nhanvien.chucdanh_han_nhanvien)
                        context['hhjp_0057'] = format_hoso.kiemtra(danhsach_nhanvien.chucdanh_furi_nhanvien)
                    if danhsach_nhanvien.vaitro_five == True:
                        context['hhjp_0080'] = format_hoso.kiemtra(danhsach_nhanvien.ten_phienam_nhanvien)
                        context['hhjp_0081'] = format_hoso.kiemtra(danhsach_nhanvien.ten_han_nhanvien)
                        context['hhjp_0091'] = format_hoso.kiemtra(danhsach_nhanvien.chucdanh_han_nhanvien)
                        context['hhjp_0090'] = format_hoso.kiemtra(danhsach_nhanvien.chucdanh_furi_nhanvien)
                    if danhsach_nhanvien.vaitro_six == True:
                        context['hhjp_0064'] = format_hoso.kiemtra(danhsach_nhanvien.ten_han_nhanvien)
                        context['hhjp_0063'] = format_hoso.kiemtra(danhsach_nhanvien.ten_phienam_nhanvien)
                        context['hhjp_0073'] = format_hoso.kiemtra(danhsach_nhanvien.chucdanh_furi_nhanvien)
                        context['hhjp_0074'] = format_hoso.kiemtra(danhsach_nhanvien.chucdanh_han_nhanvien)

            elif chinhanh == 2:

                context['ahhjp_0040'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_furl)
                context['ahhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)

                if document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh:
                    context['abs_0005'] = document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                          0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                                       3:]
                else:
                    context['abs_0005'] = ''

                context['ahhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                context['ahhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.sdt_chinhanh)

                context['ahhjp_0047'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.ten_phienam_nhanvien)
                context['ahhjp_0048'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.ten_han_nhanvien)
                context['ahhjp_0058'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.chucdanh_han_nhanvien)
                context['ahhjp_0057'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.chucdanh_furi_nhanvien)

                context['ahhjp_0063_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.ten_phienam_nhanvien)
                context['ahhjp_0064_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.ten_han_nhanvien)
                context['ahhjp_0073_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.chucdanh_furi_nhanvien)
                context['ahhjp_0074_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.chucdanh_han_nhanvien)

                context['ahhjp_0063_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.ten_phienam_nhanvien)
                context['ahhjp_0064_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.ten_han_nhanvien)
                context['ahhjp_0073_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.chucdanh_furi_nhanvien)
                context['ahhjp_0074_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.chucdanh_han_nhanvien)

                context['ahhjp_0063_3'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.ten_phienam_nhanvien)
                context['ahhjp_0064_3'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.ten_han_nhanvien)
                context['ahhjp_0073_3'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.chucdanh_furi_nhanvien)
                context['ahhjp_0074_3'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.chucdanh_han_nhanvien)

                context['ahhjp_0063_4'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.ten_phienam_nhanvien)
                context['ahhjp_0064_4'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.ten_han_nhanvien)
                context['ahhjp_0073_4'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.chucdanh_furi_nhanvien)
                context['ahhjp_0074_4'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.chucdanh_han_nhanvien)

                context['ahhjp_0063_5'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.ten_phienam_nhanvien)
                context['ahhjp_0064_5'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.ten_han_nhanvien)
                context['ahhjp_0073_5'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.chucdanh_furi_nhanvien)
                context['ahhjp_0074_5'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.chucdanh_han_nhanvien)

                context['ahhjp_0080_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.ten_phienam_nhanvien)
                context['ahhjp_0081_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.ten_han_nhanvien)
                context['ahhjp_0090_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.chucdanh_furi_nhanvien)
                context['ahhjp_0091_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.chucdanh_han_nhanvien)

                context['ahhjp_0080_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.ten_phienam_nhanvien)
                context['ahhjp_0081_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.ten_han_nhanvien)
                context['ahhjp_0090_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.chucdanh_furi_nhanvien)
                context['ahhjp_0091_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.chucdanh_han_nhanvien)

                context['bhhjp_0040'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_furl)
                context['bhhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)

                if document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh:
                    context['bbs_0005'] = document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                          0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                                       3:]
                else:
                    context['bbs_0005'] = ''

                context['bhhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                context['bhhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.sdt_chinhanh)

                context['bhhjp_0047'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.ten_phienam_nhanvien)
                context['bhhjp_0048'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.ten_han_nhanvien)
                context['bhhjp_0058'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.chucdanh_han_nhanvien)
                context['bhhjp_0057'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.chucdanh_furi_nhanvien)

                context['bhhjp_0063_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.ten_phienam_nhanvien)
                context['bhhjp_0064_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.ten_han_nhanvien)
                context['bhhjp_0073_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.chucdanh_furi_nhanvien)
                context['bhhjp_0074_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.chucdanh_han_nhanvien)

                context['bhhjp_0063_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.ten_phienam_nhanvien)
                context['bhhjp_0064_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.ten_han_nhanvien)

                context['bhhjp_0074_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.chucdanh_han_nhanvien)
                context['bhhjp_0073_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.chucdanh_furi_nhanvien)

                context['bhhjp_0063_3'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.ten_phienam_nhanvien)
                context['bhhjp_0064_3'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.ten_han_nhanvien)

                context['bhhjp_0074_3'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.chucdanh_han_nhanvien)
                context['bhhjp_0073_3'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.chucdanh_furi_nhanvien)

                context['bhhjp_0063_4'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.ten_phienam_nhanvien)
                context['bhhjp_0064_4'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.ten_han_nhanvien)

                context['bhhjp_0074_4'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.chucdanh_han_nhanvien)
                context['bhhjp_0073_4'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.chucdanh_furi_nhanvien)

                context['bhhjp_0063_5'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.ten_phienam_nhanvien)
                context['bhhjp_0064_5'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.ten_han_nhanvien)
                context['bhhjp_0074_5'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.chucdanh_han_nhanvien)
                context['bhhjp_0073_5'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.chucdanh_furi_nhanvien)

                context['bhhjp_0080_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.ten_phienam_nhanvien)
                context['bhhjp_0081_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.ten_han_nhanvien)
                context['bhhjp_0091_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.chucdanh_han_nhanvien)
                context['bhhjp_0090_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.chucdanh_furi_nhanvien)

                context['bhhjp_0080_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.ten_phienam_nhanvien)
                context['bhhjp_0081_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.ten_han_nhanvien)
                context['bhhjp_0091_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.chucdanh_han_nhanvien)
                context['bhhjp_0090_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.chucdanh_furi_nhanvien)

            elif chinhanh == 3:

                context['ahhjp_0040'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_furl)
                context['ahhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)

                if document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh:
                    context['abs_0005'] = document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                          0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                                       3:]
                else:
                    context['abs_0005'] = ''

                context['ahhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                context['ahhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.sdt_chinhanh)

                context['ahhjp_0047'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.ten_phienam_nhanvien)
                context['ahhjp_0048'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.ten_han_nhanvien)
                context['ahhjp_0058'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.chucdanh_han_nhanvien)
                context['ahhjp_0057'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.chucdanh_furi_nhanvien)

                context['ahhjp_0063_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.ten_phienam_nhanvien)
                context['ahhjp_0064_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.ten_han_nhanvien)
                context['ahhjp_0073_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.chucdanh_han_nhanvien)
                context['ahhjp_0074_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.chucdanh_furi_nhanvien)

                context['ahhjp_0063_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.ten_phienam_nhanvien)
                context['ahhjp_0064_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.ten_han_nhanvien)
                context['ahhjp_0073_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.chucdanh_han_nhanvien)
                context['ahhjp_0074_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.chucdanh_furi_nhanvien)

                context['ahhjp_0063_3'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.ten_phienam_nhanvien)
                context['ahhjp_0064_3'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.ten_han_nhanvien)
                context['ahhjp_0073_3'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.chucdanh_han_nhanvien)
                context['ahhjp_0074_3'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.chucdanh_furi_nhanvien)

                context['ahhjp_0063_4'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.ten_phienam_nhanvien)
                context['ahhjp_0064_4'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.ten_han_nhanvien)
                context['ahhjp_0073_4'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.chucdanh_han_nhanvien)
                context['ahhjp_0074_4'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.chucdanh_furi_nhanvien)

                context['ahhjp_0063_5'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.ten_phienam_nhanvien)
                context['ahhjp_0064_5'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.ten_han_nhanvien)
                context['ahhjp_0073_5'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.chucdanh_han_nhanvien)
                context['ahhjp_0074_5'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.chucdanh_furi_nhanvien)

                context['ahhjp_0080_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.ten_phienam_nhanvien)
                context['ahhjp_0081_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.ten_han_nhanvien)
                context['ahhjp_0091_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.chucdanh_han_nhanvien)
                context['ahhjp_0090_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.chucdanh_furi_nhanvien)

                context['ahhjp_0080_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.ten_phienam_nhanvien)
                context['ahhjp_0081_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.ten_han_nhanvien)
                context['ahhjp_0091_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.chucdanh_han_nhanvien)
                context['ahhjp_0090_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.chucdanh_furi_nhanvien)

                context['bhhjp_0040'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_furl)
                context['bhhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)

                if document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh:
                    context['bbs_0005'] = document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                          0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                                       3:]
                else:
                    context['bbs_0005'] = ''

                context['bhhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                context['bhhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.sdt_chinhanh)

                context['bhhjp_0047'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.ten_phienam_nhanvien)
                context['bhhjp_0048'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.ten_han_nhanvien)
                context['bhhjp_0058'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.chucdanh_han_nhanvien)
                context['bhhjp_0057'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.chucdanh_furi_nhanvien)

                context['bhhjp_0063_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.ten_phienam_nhanvien)
                context['bhhjp_0064_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.ten_han_nhanvien)
                context['bhhjp_0073_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.chucdanh_han_nhanvien)
                context['bhhjp_0074_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.chucdanh_furi_nhanvien)

                context['bhhjp_0063_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.ten_phienam_nhanvien)
                context['bhhjp_0064_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.ten_han_nhanvien)
                context['bhhjp_0073_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.chucdanh_han_nhanvien)
                context['bhhjp_0074_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.chucdanh_furi_nhanvien)

                context['bhhjp_0063_3'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.ten_phienam_nhanvien)
                context['bhhjp_0064_3'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.ten_han_nhanvien)
                context['bhhjp_0073_3'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.chucdanh_han_nhanvien)
                context['bhhjp_0074_3'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.chucdanh_furi_nhanvien)

                context['bhhjp_0063_4'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.ten_phienam_nhanvien)
                context['bhhjp_0064_4'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.ten_han_nhanvien)
                context['bhhjp_0073_4'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.chucdanh_han_nhanvien)
                context['bhhjp_0074_4'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.chucdanh_furi_nhanvien)

                context['bhhjp_0063_5'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.ten_phienam_nhanvien)
                context['bhhjp_0064_5'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.ten_han_nhanvien)
                context['bhhjp_0073_5'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.chucdanh_han_nhanvien)
                context['bhhjp_0074_5'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.chucdanh_furi_nhanvien)

                context['bhhjp_0080_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.ten_phienam_nhanvien)
                context['bhhjp_0081_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.ten_han_nhanvien)
                context['bhhjp_0091_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.chucdanh_han_nhanvien)
                context['bhhjp_0090_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.chucdanh_furi_nhanvien)

                context['bhhjp_0080_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.ten_phienam_nhanvien)
                context['bhhjp_0081_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.ten_han_nhanvien)
                context['bhhjp_0091_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.chucdanh_han_nhanvien)
                context['bhhjp_0090_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.chucdanh_furi_nhanvien)

                context['chhjp_0040'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_furl)
                context['chhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)

                if document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh:
                    context['cbs_0005'] = document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                          0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                                       3:]
                else:
                    context['cbs_0005'] = ''

                context['chhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                context['chhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.sdt_chinhanh)

                context['chhjp_0047'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.ten_phienam_nhanvien)
                context['chhjp_0048'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.ten_han_nhanvien)
                context['chhjp_0058'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.chucdanh_han_nhanvien)
                context['chhjp_0057'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.chucdanh_furi_nhanvien)

                context['chhjp_0063_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.ten_phienam_nhanvien)
                context['chhjp_0064_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.ten_han_nhanvien)
                context['chhjp_0073_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.chucdanh_han_nhanvien)
                context['chhjp_0074_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.chucdanh_furi_nhanvien)

                context['chhjp_0063_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.ten_phienam_nhanvien)
                context['chhjp_0064_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.ten_han_nhanvien)
                context['chhjp_0073_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.chucdanh_han_nhanvien)
                context['chhjp_0074_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.chucdanh_furi_nhanvien)

                context['chhjp_0063_3'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.ten_phienam_nhanvien)
                context['chhjp_0064_3'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.ten_han_nhanvien)
                context['chhjp_0073_3'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.chucdanh_han_nhanvien)
                context['chhjp_0074_3'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.chucdanh_furi_nhanvien)

                context['chhjp_0063_4'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.ten_phienam_nhanvien)
                context['chhjp_0064_4'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.ten_han_nhanvien)
                context['chhjp_0073_4'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.chucdanh_han_nhanvien)
                context['chhjp_0074_4'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.chucdanh_furi_nhanvien)

                context['chhjp_0063_5'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.ten_phienam_nhanvien)
                context['chhjp_0064_5'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.ten_han_nhanvien)
                context['chhjp_0073_5'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.chucdanh_han_nhanvien)
                context['chhjp_0074_5'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.chucdanh_furi_nhanvien)

                context['chhjp_0080_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.ten_phienam_nhanvien)
                context['chhjp_0081_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.ten_han_nhanvien)
                context['chhjp_0091_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.chucdanh_han_nhanvien)
                context['chhjp_0090_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.chucdanh_furi_nhanvien)

                context['chhjp_0080_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.ten_phienam_nhanvien)
                context['chhjp_0081_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.ten_han_nhanvien)
                context['chhjp_0091_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.chucdanh_han_nhanvien)
                context['chhjp_0090_2'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.chucdanh_furi_nhanvien)

            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            context['hhjp_0003'] = format_hoso.kiemtra(thuctapsinh.quoctich_tts.name_quoctich)
            context['hhjp_0004'] = format_hoso.convert_date(thuctapsinh.ngaysinh_tts)
            context['hhjp_0005'] = format_hoso.kiemtra(thuctapsinh.tuoi_tts)
            context['hhjp_0006'] = format_hoso.gender_check(thuctapsinh.gtinh_tts)

            context['hhjp_0113_A'] = '□ A 第１号企業単独型技能実習'
            context['hhjp_0113_B'] = '□ B 第２号企業単独型技能実習'
            context['hhjp_0113_C'] = '□ C 第３号企業単独型技能実習'

            if document.giaidoan_hoso == '1 go':
                context['hhjp_0113_D'] = '■ D 第１号団体監理型技能実習'
                context['hhjp_0113_E'] = '□ E 第２号団体監理型技能実習'
                context['hhjp_0113_F'] = '□ F 第３号団体監理型技能実習'
            elif document.giaidoan_hoso == '2 go':
                context['hhjp_0113_D'] = '□ D 第１号団体監理型技能実習'
                context['hhjp_0113_E'] = '■ E 第２号団体監理型技能実習'
                context['hhjp_0113_F'] = '□ F 第３号団体監理型技能実習'
            elif document.giaidoan_hoso == '3 go':
                context['hhjp_0113_D'] = '□ D 第１号団体監理型技能実習'
                context['hhjp_0113_E'] = '□ E 第２号団体監理型技能実習'
                context['hhjp_0113_F'] = '■ F 第３号団体監理型技能実習'
            else:
                context['hhjp_0113_D'] = '□ D 第１号団体監理型技能実習'
                context['hhjp_0113_E'] = '□ E 第２号団体監理型技能実習'
                context['hhjp_0113_F'] = '□ F 第３号団体監理型技能実習'

            # for kehoach in kehoachdoatao:
            #     dapan = 'hhjp_0113_'
            #     if thuctapsinh.kehoach_doatao.id == kehoach.id:
            #         dapan = dapan + kehoach.dapan_kehoachdaotao
            #         context[dapan] = '■ %s %s' % (
            #             thuctapsinh.kehoach_doatao.dapan_kehoachdaotao, thuctapsinh.kehoach_doatao.name_kehoachdaotao)
            #     else:
            #         dapan = dapan + kehoach.dapan_kehoachdaotao
            #         context[dapan] = '□ %s %s' % (kehoach.dapan_kehoachdaotao, kehoach.name_kehoachdaotao)

            if thuctapsinh.ketthuc_kynang_tts and thuctapsinh.batdau_kynang_tts:
                day = (thuctapsinh.ketthuc_kynang_tts - thuctapsinh.batdau_kynang_tts).days
            else:
                day = 0

            if day >= 365:
                year = int(day) // 365
                month = (int(day) % 365) // 30

                month_surplus = ((int(day) % 365) // 30) % 30

                if month_surplus >= 20:
                    month += 1
                else:
                    month += 0
                hhjp_0208 = '%s年 %s月' % (int(year), int(month))
                if hhjp_0208 == u'0年 0月':
                    context['hhjp_0208'] = ' 年  月'
                else:
                    context['hhjp_0208'] = hhjp_0208

                context['hhjp_0207'] = format_hoso.convert_date(thuctapsinh.batdau_kynang_tts)
                context['hhjp_0206'] = format_hoso.convert_date(thuctapsinh.ketthuc_kynang_tts)
            elif day < 365:
                month = int(day) // 30
                month_surplus = int(day) % 30

                if month_surplus >= 20:
                    month += 1
                else:
                    month += 0

                hjp_208 = '0年 %s月' % month
                if hjp_208 == '0年 0月':
                    context['hhjp_0208'] = ' 年  月'
                else:
                    context['hhjp_0208'] = hjp_208
                context['hhjp_0207'] = format_hoso.convert_date(thuctapsinh.batdau_kynang_tts)
                context['hhjp_0206'] = format_hoso.convert_date(thuctapsinh.ketthuc_kynang_tts)
            else:
                context['hhjp_0208'] = ' 年 　月'
                context['hhjp_0207'] = format_hoso.convert_date(thuctapsinh.batdau_kynang_tts)
                context['hhjp_0206'] = format_hoso.convert_date(thuctapsinh.ketthuc_kynang_tts)

            context['hhjp_0114'] = format_hoso.kiemtra(hopdonglaodong.macongviec_tts)
            context['hhjp_0115'] = format_hoso.nganhnghe(hopdonglaodong.loai_nghe_nganhnghe_tts.name_loaicongviec)
            context['hhjp_0116'] = format_hoso.nganhnghe(thuctapsinh.congviec_chuyennhuong.name_congviec)

            context['hhjp_0117'] = format_hoso.kiemtra(thuctapsinh.ma_congviec_nhieucongviec)
            context['hhjp_0118'] = format_hoso.nganhnghe(hopdonglaodong.loai_nghe_nganhnghe_congviec.name_loaicongviec)
            context['hhjp_0119'] = format_hoso.nganhnghe(thuctapsinh.congviec_nhieucongviec.name_congviec)

            if document.giaidoan_hoso == '1 go':
                context['hhjp_0215'] = format_hoso.kiemtra(thuctapsinh.ghichu_namnhat)
                context['hhjp_0216'] = '第３面「入国後講習実施予定表」のとおり'

                if thuctapsinh.daotao_truoc_nhapcanh == True:
                    context['hhjp_0120_1'] = u'■ 有'
                    context['hhjp_0120_2'] = u'□ 無'
                elif thuctapsinh.daotao_truoc_nhapcanh == False:
                    context['hhjp_0120_1'] = u'□ 有'
                    context['hhjp_0120_2'] = u'■ 無'
                else:
                    context['hhjp_0120_1'] = u'□ 有'
                    context['hhjp_0120_2'] = u'□ 無'

                context['hhjp_0217'] = '第4面「実習実施予定表」のとおり'

                if thuctapsinh.kiemtra_chuyennhuong_namnhat.loaikiemtra == '技能検定':
                    context['a132'] = '■'
                    context['a132kt'] = format_hoso.kiemtra(thuctapsinh.kithi_chuyennhuong_namnhat.ten_kithi)
                    context['a132cd'] = format_hoso.kiemtra(thuctapsinh.capdo_chuyennhuong_namnhat.ten_capdo)
                    context['b132'] = '□'
                    context['c132'] = '□'
                    context['b132kt'] = ''
                    context['b132cd'] = ''
                elif thuctapsinh.kiemtra_chuyennhuong_namnhat.loaikiemtra == '技能実習評価試験':
                    context['a132'] = '□'
                    context['b132kt'] = format_hoso.kiemtra(thuctapsinh.kithi_chuyennhuong_namnhat.ten_kithi)
                    context['b132cd'] = format_hoso.kiemtra(thuctapsinh.capdo_chuyennhuong_namnhat.ten_capdo)
                    context['b132'] = '■'
                    context['c132'] = '□'
                    context['a132kt'] = ''
                    context['a132cd'] = ''
                elif thuctapsinh.kiemtra_chuyennhuong_namnhat.loaikiemtra == 'その他':
                    context['a132'] = '□'
                    context['b132kt'] = ''
                    context['b132cd'] = ''
                    context['b132'] = '□'
                    context['c132'] = '■'
                    context['a132kt'] = ''
                    context['a132cd'] = ''
                else:
                    context['a132'] = '□'
                    context['b132kt'] = ''
                    context['b132cd'] = ''
                    context['b132'] = '□'
                    context['c132'] = '□'
                    context['a132kt'] = ''
                    context['a132cd'] = ''

                if thuctapsinh.kiemtra_nhieucongviec_namnhat.loaikiemtra == '技能検定':
                    context['a133'] = '■'
                    context['a133kt'] = format_hoso.kiemtra(thuctapsinh.kithi_nhieucongviec_namnhat.ten_kithi)
                    context['a133cd'] = format_hoso.kiemtra(thuctapsinh.capdo_nhieucongviec_namnhat.ten_capdo)
                    context['b133'] = '□'
                    context['c133'] = '□'
                    context['b133kt'] = ''
                    context['b133cd'] = ''
                elif thuctapsinh.kiemtra_nhieucongviec_namnhat.loaikiemtra == '技能実習評価試験':
                    context['a133'] = '□'
                    context['b133kt'] = format_hoso.kiemtra(thuctapsinh.kithi_nhieucongviec_namnhat.ten_kithi)
                    context['b133cd'] = format_hoso.kiemtra(thuctapsinh.capdo_nhieucongviec_namnhat.ten_capdo)
                    context['b133'] = '■'
                    context['c133'] = '□'
                    context['a133kt'] = ''
                    context['a133cd'] = ''
                elif thuctapsinh.kiemtra_nhieucongviec_namnhat.loaikiemtra == 'その他':
                    context['a133'] = '□'
                    context['b133kt'] = ''
                    context['b133cd'] = ''
                    context['b133'] = '□'
                    context['c133'] = '■'
                    context['a133kt'] = ''
                    context['a133cd'] = ''
                else:
                    context['a133'] = '□'
                    context['b133kt'] = ''
                    context['b133cd'] = ''
                    context['b133'] = '□'
                    context['c133'] = '□'
                    context['a133kt'] = ''
                    context['a133cd'] = ''

                context['a134'] = '□'
                context['a134kt'] = ''
                context['a134cd'] = ''
                context['b134'] = '□'
                context['b134kt'] = ''
                context['b134cd'] = ''
                context['a136'] = '□'
                context['a136kt'] = ''
                context['a136cd'] = ''
                context['b137'] = '□'
                context['b137kt'] = ''
                context['b137cd'] = ''
                context['c137'] = '□'

                context['hhjp_0135'] = ''
                context['hhjp_0149'] = format_hoso.kiemtra(thuctapsinh.tongthoigian_kehoach_namnhat)
                context['h313'] = format_hoso.convert_date(thuctapsinh.batdau_kehoach_namnhat)
                context['h314'] = format_hoso.convert_date(thuctapsinh.ketthuc_kehoach_namnhat)
                context['h152'] = format_hoso.kiemtra(thuctapsinh.sogio_thuctap_kehoach_namnhat)

                if daotaosau.tonggio_daotao:
                    context['h250'] = format_hoso.kiemtra(round(daotaosau.tonggio_daotao, 2))
                else:
                    context['h250'] = ''

                if donhang_congviec.nam_thoigian:
                    context['h251'] = format_hoso.kiemtra(round(donhang_congviec.nam_thoigian, 2))
                else:
                    context['h251'] = ''

            elif document.giaidoan_hoso == '2 go':
                context['hhjp_0215'] = format_hoso.kiemtra(thuctapsinh.ghichu_namhai)
                context['hhjp_0216'] = ''
                context['hhjp_0120_1'] = ''
                context['hhjp_0120_2'] = ''
                context['hhjp_0217'] = '第5面「実習実施予定表（１年目）」及び第６面「実習実施予定表（２年目）」のとおり'

                if thuctapsinh.kiemtra_chuyennhuong_namnhat.loaikiemtra == '技能検定':
                    if thuctapsinh.ketqua_thuchanh_motcongviec_namnhat == 'Qua':
                        context['a134'] = '■'
                        context['a134kt'] = format_hoso.kiemtra(thuctapsinh.kithi_chuyennhuong_namnhat.ten_kithi)
                        context['a134cd'] = format_hoso.kiemtra(thuctapsinh.capdo_chuyennhuong_namnhat.ten_capdo)
                    else:
                        context['a134'] = '□'
                        context['a134kt'] = ''
                        context['a134cd'] = ''
                else:
                    context['a134'] = '□'
                    context['a134kt'] = ''
                    context['a134cd'] = ''

                if thuctapsinh.kiemtra_chuyennhuong_namnhat.loaikiemtra == '技能実習評価試験':
                    if thuctapsinh.ketqua_thuchanh_motcongviec_namnhat == 'Qua':
                        context['b134'] = '■'
                        context['b134kt'] = format_hoso.kiemtra(thuctapsinh.kithi_chuyennhuong_namnhat.ten_kithi)
                        context['b134cd'] = format_hoso.kiemtra(thuctapsinh.capdo_chuyennhuong_namnhat.ten_capdo)
                    else:
                        context['b134'] = '□'
                        context['b134kt'] = ''
                        context['b134cd'] = ''
                else:
                    context['b134'] = '□'
                    context['b134kt'] = ''
                    context['b134cd'] = ''

                if thuctapsinh.kiemtra_chuyennhuong_namhai.loaikiemtra == '技能検定':
                    context['a132'] = '■'
                    context['a132kt'] = format_hoso.kiemtra(thuctapsinh.kithi_chuyennhuong_namhai.ten_kithi)
                    context['a132cd'] = format_hoso.kiemtra(thuctapsinh.capdo_chuyennhuong_namhai.ten_capdo)
                    context['b132'] = '□'
                    context['c132'] = '□'
                    context['b132kt'] = ''
                    context['b132cd'] = ''
                elif thuctapsinh.kiemtra_chuyennhuong_namhai.loaikiemtra == '技能実習評価試験':
                    context['a132'] = '□'
                    context['b132kt'] = format_hoso.kiemtra(thuctapsinh.kithi_chuyennhuong_namhai.ten_kithi)
                    context['b132cd'] = format_hoso.kiemtra(thuctapsinh.capdo_chuyennhuong_namhai.ten_capdo)
                    context['b132'] = '■'
                    context['c132'] = '□'
                    context['a132kt'] = ''
                    context['a132cd'] = ''
                elif thuctapsinh.kiemtra_chuyennhuong_namhai.loaikiemtra == 'その他':
                    context['a132'] = '□'
                    context['b132kt'] = ''
                    context['b132cd'] = ''
                    context['b132'] = '□'
                    context['c132'] = '■'
                    context['a132kt'] = ''
                    context['a132cd'] = ''
                else:
                    context['a132'] = '□'
                    context['b132kt'] = ''
                    context['b132cd'] = ''
                    context['b132'] = '□'
                    context['c132'] = '□'
                    context['a132kt'] = ''
                    context['a132cd'] = ''

                if thuctapsinh.kiemtra_nhieucongviec_namhai.loaikiemtra == '技能検定':
                    context['a133'] = '■'
                    context['a133kt'] = format_hoso.kiemtra(thuctapsinh.kithi_nhieucongviec_namhai.ten_kithi)
                    context['a133cd'] = format_hoso.kiemtra(thuctapsinh.capdo_nhieucongviec_namhai.ten_capdo)
                    context['b133'] = '□'
                    context['c133'] = '□'
                    context['b133kt'] = ''
                    context['b133cd'] = ''
                elif thuctapsinh.kiemtra_nhieucongviec_namhai.loaikiemtra == '技能実習評価試験':
                    context['a133'] = '□'
                    context['b133kt'] = format_hoso.kiemtra(thuctapsinh.kithi_nhieucongviec_namhai.ten_kithi)
                    context['b133cd'] = format_hoso.kiemtra(thuctapsinh.capdo_nhieucongviec_namhai.ten_capdo)
                    context['b133'] = '■'
                    context['c133'] = '□'
                    context['a133kt'] = ''
                    context['a133cd'] = ''
                elif thuctapsinh.kiemtra_nhieucongviec_namhai.loaikiemtra == 'その他':
                    context['a133'] = '□'
                    context['b133kt'] = ''
                    context['b133cd'] = ''
                    context['b133'] = '□'
                    context['c133'] = '■'
                    context['a133kt'] = ''
                    context['a133d'] = ''
                else:
                    context['a133'] = '□'
                    context['b133kt'] = ''
                    context['b133cd'] = ''
                    context['b133'] = '□'
                    context['c133'] = '□'
                    context['a133t'] = ''
                    context['a133cd'] = ''

                if thuctapsinh.kiemtra_nhieucongviec_namnhat.loaikiemtra == '技能検定':
                    if thuctapsinh.ketqua_thuchanh_nhieucongviec_namnhat == 'Qua':
                        context['a136'] = '■'
                        context['a136kt'] = format_hoso.kiemtra(thuctapsinh.kithi_nhieucongviec_namnhat.ten_kithi)
                        context['a136cd'] = format_hoso.kiemtra(thuctapsinh.capdo_nhieucongviec_namnhat.ten_capdo)
                    else:
                        context['a136'] = '□'
                        context['a136kt'] = ''
                        context['a136cd'] = ''
                else:
                    context['a136'] = '□'
                    context['a136kt'] = ''
                    context['a136cd'] = ''

                if thuctapsinh.kiemtra_nhieucongviec_namnhat.loaikiemtra == '技能実習評価試験':
                    if thuctapsinh.ketqua_thuchanh_nhieucongviec_namnhat == 'Qua':
                        context['b137'] = '■'
                        context['b137kt'] = format_hoso.kiemtra(thuctapsinh.kithi_nhieucongviec_namnhat.ten_kithi)
                        context['b137cd'] = format_hoso.kiemtra(thuctapsinh.capdo_nhieucongviec_namnhat.ten_capdo)
                    else:
                        context['b137'] = '□'
                        context['b137kt'] = ''
                        context['b137cd'] = ''
                else:
                    context['b137'] = '□'
                    context['b137kt'] = ''
                    context['b137cd'] = ''

                if thuctapsinh.kiemtra_nhieucongviec_namnhat.loaikiemtra == 'その他':
                    if thuctapsinh.ketqua_thuchanh_nhieucongviec_namnhat == 'Qua':
                        context['c137'] = '■'
                    else:
                        context['c137'] = '□'
                else:
                    context['c137'] = '□'

                context['hhjp_0135'] = ''
                context['hhjp_0149'] = format_hoso.kiemtra(thuctapsinh.tongthoigian_kehoach_namhai)
                context['h313'] = format_hoso.convert_date(thuctapsinh.batdau_kehoach_namhai)
                context['h314'] = format_hoso.convert_date(thuctapsinh.ketthuc_kehoach_namhai)
                context['h152'] = format_hoso.kiemtra(thuctapsinh.sogio_thuctap_kehoach_namhai)

                context['h250'] = '0'

                context['h251'] = format_hoso.kiemtra(thuctapsinh.tongthoigian_kehoach_namhai)

            elif document.giaidoan_hoso == '3 go':
                context['hhjp_0215'] = ''
                context['hhjp_0216'] = ''
                context['hhjp_0217'] = '第5面「実習実施予定表（１年目）」及び第６面「実習実施予定表（２年目）」のとおり'
                context['hhjp_0120_1'] = ''
                context['hhjp_0120_2'] = ''
                context[
                    'hhjp_0149'] = thuctapsinh.tongthoigian_kehoach_namba if thuctapsinh.tongthoigian_kehoach_namba else u'0年0月0日間'

                if thuctapsinh.kiemtra_chuyennhuong_namhai.loaikiemtra == '技能検定':
                    if thuctapsinh.ketqua_thuchanh_motcongviec_namhai == 'Qua':
                        context['a134'] = '■'
                        context['a134kt'] = format_hoso.kiemtra(thuctapsinh.kithi_nhieucongviec_namhai.ten_kithi)
                        context['a134cd'] = format_hoso.kiemtra(thuctapsinh.capdo_nhieucongviec_namhai.ten_capdo)
                    else:
                        context['a134'] = '□'
                        context['a134kt'] = ''
                        context['a134cd'] = ''
                else:
                    context['a134'] = '□'
                    context['a134kt'] = ''
                    context['a134cd'] = ''

                if thuctapsinh.kiemtra_chuyennhuong_namhai.loaikiemtra == '技能実習評価試験':
                    if thuctapsinh.ketqua_thuchanh_nhieucongviec_namhai == 'Qua':
                        context['b134'] = '■'
                        context['b134kt'] = format_hoso.kiemtra(thuctapsinh.kithi_nhieucongviec_namhai.ten_kithi)
                        context['b134cd'] = format_hoso.kiemtra(thuctapsinh.capdo_nhieucongviec_namhai.ten_capdo)
                    else:
                        context['b134'] = '□'
                        context['b134kt'] = ''
                        context['b134cd'] = ''
                else:
                    context['b134'] = '□'
                    context['b134kt'] = ''
                    context['b134cd'] = ''

                if thuctapsinh.kiemtra_chuyennhuong_namba.loaikiemtra == '技能検定':
                    context['a132'] = '■'
                    context['a132kt'] = format_hoso.kiemtra(thuctapsinh.kithi_chuyennhuong_namba.ten_kithi)
                    context['a132cd'] = format_hoso.kiemtra(thuctapsinh.capdo_chuyennhuong_namba.ten_capdo)
                    context['b132'] = '□'
                    context['c132'] = '□'
                    context['b132kt'] = ''
                    context['b132cd'] = ''
                elif thuctapsinh.kiemtra_chuyennhuong_namba.loaikiemtra == '技能実習評価試験':
                    context['a132'] = '□'
                    context['b132kt'] = format_hoso.kiemtra(thuctapsinh.kithi_chuyennhuong_namba.ten_kithi)
                    context['b132cd'] = format_hoso.kiemtra(thuctapsinh.capdo_chuyennhuong_namba.ten_capdo)
                    context['b132'] = '■'
                    context['c132'] = '□'
                    context['a132kt'] = ''
                    context['a132cd'] = ''
                elif thuctapsinh.kiemtra_chuyennhuong_namba.loaikiemtra == 'その他':
                    context['a132'] = '□'
                    context['b132kt'] = ''
                    context['b132cd'] = ''
                    context['b132'] = '□'
                    context['c132'] = '■'
                    context['a132kt'] = ''
                    context['a132cd'] = ''
                else:
                    context['a132'] = '□'
                    context['b132kt'] = ''
                    context['b132cd'] = ''
                    context['b132'] = '□'
                    context['c132'] = '□'
                    context['a132kt'] = ''
                    context['a132cd'] = ''

                if thuctapsinh.kiemtra_nhieucongviec_namba.loaikiemtra == '技能検定':
                    context['a133'] = '■'
                    context['a133kt'] = format_hoso.kiemtra(thuctapsinh.kithi_nhieucongviec_namba.ten_kithi)
                    context['a133cd'] = format_hoso.kiemtra(thuctapsinh.capdo_nhieucongviec_namba.ten_capdo)
                    context['b133'] = '□'
                    context['c133'] = '□'
                    context['b133kt'] = ''
                    context['b133cd'] = ''
                elif thuctapsinh.kiemtra_nhieucongviec_namba.loaikiemtra == '技能実習評価試験':
                    context['a133'] = '□'
                    context['b133kt'] = format_hoso.kiemtra(thuctapsinh.kithi_nhieucongviec_namba.ten_kithi)
                    context['b133cd'] = format_hoso.kiemtra(thuctapsinh.capdo_nhieucongviec_namba.ten_capdo)
                    context['b133'] = '■'
                    context['c133'] = '□'
                    context['a133kt'] = ''
                    context['a133cd'] = ''
                elif thuctapsinh.kiemtra_nhieucongviec_namba.loaikiemtra == 'その他':
                    context['a133'] = '□'
                    context['b133kt'] = ''
                    context['b133cd'] = ''
                    context['b133'] = '□'
                    context['c133'] = '■'
                    context['a133kt'] = ''
                    context['a133cd'] = ''
                else:
                    context['a133'] = '□'
                    context['b133kt'] = ''
                    context['b133cd'] = ''
                    context['b133'] = '□'
                    context['c133'] = '□'
                    context['a133kt'] = ''
                    context['a133cd'] = ''

                if thuctapsinh.kiemtra_nhieucongviec_namhai.loaikiemtra == '技能検定':
                    if thuctapsinh.ketqua_thuchanh_nhieucongviec_namhai == 'Qua':
                        context['a136'] = '■'
                        context['a136kt'] = format_hoso.kiemtra(thuctapsinh.kithi_nhieucongviec_namhai.ten_kithi)
                        context['a136cd'] = format_hoso.kiemtra(thuctapsinh.capdo_nhieucongviec_namhai.ten_capdo)
                    else:
                        context['a136'] = '□'
                        context['a136kt'] = ''
                        context['a136cd'] = ''
                else:
                    context['a136'] = '□'
                    context['a136kt'] = ''
                    context['a136cd'] = ''
                if thuctapsinh.kiemtra_nhieucongviec_namhai.loaikiemtra == '技能実習評価試験':
                    if thuctapsinh.ketqua_thuchanh_nhieucongviec_namhai == 'Qua':
                        context['b137'] = '■'
                        context['b137kt'] = format_hoso.kiemtra(thuctapsinh.kithi_nhieucongviec_namhai.ten_kithi)
                        context['b137cd'] = format_hoso.kiemtra(thuctapsinh.capdo_nhieucongviec_namhai.ten_capdo)
                    else:
                        context['b137'] = '□'
                        context['b137kt'] = ''
                        context['b137cd'] = ''
                else:
                    context['b137'] = '□'
                    context['b137kt'] = ''
                    context['b137cd'] = ''

                if thuctapsinh.kiemtra_nhieucongviec_namhai.loaikiemtra == 'その他':
                    if thuctapsinh.ketqua_lythuyet_nhieucongviec_namhai == 'Qua':
                        context['c137'] = '■'
                    else:
                        context['c137'] = '□'
                else:
                    context['c137'] = '□'

                context['hhjp_0135'] = ''
                context['hhjp_0149'] = format_hoso.kiemtra(thuctapsinh.tongthoigian_kehoach_namba)
                context['h313'] = format_hoso.convert_date(thuctapsinh.batdau_kehoach_namba)
                context['h314'] = format_hoso.convert_date(thuctapsinh.ketthuc_kehoach_namba)
                context['h152'] = format_hoso.kiemtra(thuctapsinh.sogio_thuctap_kehoach_namba)
                context['h251'] = format_hoso.kiemtra(thuctapsinh.tongthoigian_kehoach_namba)
                context['h250'] = '0'

            else:
                context['h250'] = ''
                context['hhjp_0215'] = ''
                context['hhjp_0216'] = '第３面「入国後講習実施予定表」のとおり'
                context['hhjp_0217'] = ''
                context['hhjp_0149'] = u'0年0月0日間'
                context['hhjp_0313_Y'] = ''
                context['hhjp_0313_M'] = ''
                context['hhjp_0313_D'] = ''

                context['hhjp_0120_1'] = u'□ 有'
                context['hhjp_0120_2'] = u'□ 無'

                context['a132'] = '□'
                context['a132kt'] = ''
                context['a132cd'] = ''
                context['b132'] = '□'
                context['c132'] = '□'
                context['b132kt'] = ''
                context['b132cd'] = ''

                context['a133'] = '□'
                context['a133kt'] = ''
                context['a133cd'] = ''
                context['b133'] = '□'
                context['c133'] = '□'
                context['b133kt'] = ''
                context['b133cd'] = ''

                context['hhjp_0135'] = ''
                context['hhjp_0149'] = ''
                context['h313'] = ''
                context['h314'] = ''
                context['h152'] = ''
                context['h251'] = ''

            context['hhjp_0100'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.so_giayphep_nghiepdoan)

            if document.donhang_hoso.nghiepdoan_donhang.loai_giayphep_nghiepdoan == 'Tổng hợp':
                context['hhjp_0101_1'] = u'■ 一般監理事業'
                context['hhjp_0101_2'] = u'□ 特定監理事業'
            elif document.donhang_hoso.nghiepdoan_donhang.loai_giayphep_nghiepdoan == 'Cụ thể':
                context['hhjp_0101_1'] = u'□ 一般監理事業'
                context['hhjp_0101_2'] = u'■ 特定監理事業'
            else:
                context['hhjp_0101_1'] = u'□ 一般監理事業'
                context['hhjp_0101_2'] = u'□ 特定監理事業'

            context['hhjp_0096'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_phienam_nghiepdoan)
            context['hhjp_0097'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_han_nghiepdoan)

            if document.donhang_hoso.nghiepdoan_donhang.buudien_nghiepdoan:
                context['bs_0007'] = document.donhang_hoso.nghiepdoan_donhang.buudien_nghiepdoan[
                                     0:3] + '-' + document.donhang_hoso.nghiepdoan_donhang.buudien_nghiepdoan[
                                                  3:]
            else:
                context['bs_0007'] = ''

            context['hhjp_0098'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.diachi_nghiepdoan)
            context['hhjp_0099'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.dienthoai_nghiepdoan)
            context['hhjp_0102'] = format_hoso.kiemtra(
                document.donhang_hoso.nghiepdoan_donhang.ten_daidien_phienam_nghiepdoan)
            context['hhjp_0103'] = format_hoso.kiemtra(
                document.donhang_hoso.nghiepdoan_donhang.ten_daidien_han_nghiepdoan)

            for danhsach_nhanvien_tts in document.donhang_hoso.nghiepdoan_donhang.danhsach_nhanvien_tts:
                if danhsach_nhanvien_tts.quanly_vaitro_nghiepdoan == True:
                    context['hhjp_0104'] = format_hoso.kiemtra(danhsach_nhanvien_tts.ten_nvien_furi_nghiepdoan)
                    context['hhjp_0105'] = format_hoso.kiemtra(danhsach_nhanvien_tts.ten_nvien_han_nghiepdoan)
                    break
                else:
                    context['hhjp_0104'] = ''
                    context['hhjp_0105'] = ''

            context['hhjp_0106'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_nghiepdoan.ten_phienam_chinhanh)
            context['hhjp_0107'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_nghiepdoan.ten_han_chinhanh)
            if document.donhang_hoso.chinhanh_nghiepdoan.buudien_chinhanh:
                context['bs_0008'] = document.donhang_hoso.chinhanh_nghiepdoan.buudien_chinhanh[
                                     0:3] + '-' + document.donhang_hoso.chinhanh_nghiepdoan.buudien_chinhanh[
                                                  3:]
            else:
                context['bs_0008'] = ''

            context['hhjp_0108'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_nghiepdoan.dichi_chinhanh)
            context['hhjp_0109'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_nghiepdoan.dienthoai_chinhanh)

            for danhsach_nhanvien_tts in document.donhang_hoso.nghiepdoan_donhang.danhsach_nhanvien_tts:
                if danhsach_nhanvien_tts.chidao_vaitro_nghiepdoan == True:
                    context['hhjp_0110'] = format_hoso.kiemtra(danhsach_nhanvien_tts.ten_nvien_furi_nghiepdoan)
                    context['hhjp_0111'] = format_hoso.kiemtra(danhsach_nhanvien_tts.ten_nvien_han_nghiepdoan)
                    break
                else:
                    context['hhjp_0110'] = ''
                    context['hhjp_0111'] = ''

            context['hhjp_0112'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_anh_phaicu)

            if hopdonglaodong.hinhthuc_luongthang:
                docdata.remove_shape(u'id="5" name="Oval 5"')
                docdata.remove_shape(u'id="4" name="Oval 4"')
                context['hhjp_0147'] = format_hoso.kiemtra(format_hoso.place_value(hopdonglaodong.sotien_luongthang))
            elif hopdonglaodong.hinhthuc_luongngay:
                docdata.remove_shape(u'id="5" name="Oval 5"')
                docdata.remove_shape(u'id="3" name="Oval 3"')
                context['hhjp_0147'] = format_hoso.kiemtra(format_hoso.place_value(hopdonglaodong.sotien_luongngay))
            elif hopdonglaodong.hinhthuc_luonggio:
                docdata.remove_shape(u'id="3" name="Oval 3"')
                docdata.remove_shape(u'id="4" name="Oval 4"')
                context['hhjp_0147'] = format_hoso.kiemtra(format_hoso.place_value(hopdonglaodong.sotien_luonggio))
            else:
                docdata.remove_shape(u'id="3" name="Oval 3"')
                docdata.remove_shape(u'id="4" name="Oval 4"')
                docdata.remove_shape(u'id="5" name="Oval 5"')
                context['hhjp_0147'] = ''

            if hopdonglaodong.quydinh_hopdong_thoihan == 'Có':
                docdata.remove_shape(u'id="2" name="Oval 2"')
                context['hhjp_0148'] = format_hoso.convert_date(hopdonglaodong.batdau_tgian_congviec)
                context['hhjp_0380'] = format_hoso.convert_date(hopdonglaodong.kethuc_tgian_congviec)
            elif hopdonglaodong.quydinh_hopdong_thoihan == 'Không':
                docdata.remove_shape(u'id="6" name="Oval 6"')
                context['hhjp_0148'] = '年　月　日'
                context['hhjp_0380'] = '年　月　日'
            else:
                docdata.remove_shape(u'id="2" name="Oval 2"')
                docdata.remove_shape(u'id="6" name="Oval 6"')
                context['hhjp_0148'] = '年　月　日'
                context['hhjp_0380'] = '年　月　日'

            context['bs_0013'] = format_hoso.kiemtra(hopdonglaodong.sotien_phuccap_daotao)

            bs_0016 = hopdonglaodong.sotien_phuccap_nhao + hopdonglaodong.sotien_phuccap_an + hopdonglaodong.sotien_phuccap_dilai + hopdonglaodong.sotien_phuccap_thue + hopdonglaodong.sotien_phuccap_mot + hopdonglaodong.sotien_phuccap_hai + hopdonglaodong.sotien_phuccap_ba + hopdonglaodong.sotien_phuccap_bon

            context['bs_0016'] = format_hoso.kiemtra(bs_0016)
            context['hhjp_0704'] = format_hoso.convert(hopdonglaodong.batdau_codinh_sang_hopdong)
            context['hhjp_0705'] = format_hoso.convert(hopdonglaodong.kethuc_codinh_chieu_hopdong)

            thoigian = ''

            if hopdonglaodong.batdau_codinh_chieu1_hopdong and hopdonglaodong.kethuc_codinh_chieu1_hopdong:
                thoigian = thoigian + '%s ～ %s ' % (
                    format_hoso.convert_hour(hopdonglaodong.batdau_codinh_chieu1_hopdong),
                    format_hoso.convert_hour(hopdonglaodong.kethuc_codinh_chieu1_hopdong))

            if hopdonglaodong.batdau_codinh_trua_hopdong and hopdonglaodong.kethuc_codinh_trua_hopdong:
                thoigian = thoigian + ', %s ～ %s ' % (
                    format_hoso.convert_hour(hopdonglaodong.batdau_codinh_trua_hopdong),
                    format_hoso.convert_hour(hopdonglaodong.kethuc_codinh_trua_hopdong))

            if hopdonglaodong.batdau_codinh_chieu2_hopdong and hopdonglaodong.kethuc_codinh_chieu2_hopdong:
                thoigian = thoigian + ', %s ～ %s' % (
                    format_hoso.convert_hour(hopdonglaodong.batdau_codinh_chieu2_hopdong),
                    format_hoso.convert_hour(hopdonglaodong.kethuc_codinh_chieu2_hopdong))

            context['hhjp_0706'] = format_hoso.kiemtra(thoigian)

            context['hhjp_0153'] = format_hoso.kiemtra(hopdonglaodong.giolaodong_mottuan)
            context['hhjp_0152_1'] = format_hoso.kiemtra(hopdonglaodong.giolaodong_motnam)

            context['hhjp_0393'] = format_hoso.kiemtra(hopdonglaodong.ngaynghiphep_coluong_lientuc_sauthang)

            ngaynghi_dinhki = []
            if hopdonglaodong.ngaynghi_dinhki_thuhai == True:
                ngaynghi_dinhki.append('月')

            if hopdonglaodong.ngaynghi_dinhki_thuba == True:
                ngaynghi_dinhki.append('火')

            if hopdonglaodong.ngaynghi_dinhki_thutu == True:
                ngaynghi_dinhki.append('水')

            if hopdonglaodong.ngaynghi_dinhki_thunam == True:
                ngaynghi_dinhki.append('木')

            if hopdonglaodong.ngaynghi_dinhki_thusau == True:
                ngaynghi_dinhki.append('金')

            if hopdonglaodong.ngaynghi_dinhki_thubay == True:
                ngaynghi_dinhki.append('土')

            if hopdonglaodong.ngaynghi_dinhki_chunhat == True:
                ngaynghi_dinhki.append('日')

            # if hopdonglaodong.ngaynghi_dinhki_ngayle == True:
            #     ngaynghi_dinhki.append('祝')

            context['hhjp_0154'] = "、".join(ngaynghi_dinhki)

            context['hhjp_0154_1'] = format_hoso.kiemtra(hopdonglaodong.ngaynghi_hopdong)

            context['hhjp_0156'] = format_hoso.kiemtra(hopdonglaodong.diachi_cutru)

            context['hhjp_0310'] = format_hoso.place_value(
                hopdonglaodong.sotien_khoanmuc_7) if hopdonglaodong.sotien_khoanmuc_7 else '0'
            context['hhjp_0311'] = format_hoso.kiemtra(format_hoso.place_value(hopdonglaodong.sotien_khoanmuc_8))

            context['hhjp_0312'] = format_hoso.kiemtra(
                format_hoso.place_value(document.donhang_hoso.donhang_daingo.thuphi_ga))

            khoankhac = ''

            if hopdonglaodong.noidung_khoanmuc_9:
                if hopdonglaodong.chiphi_khoanmuc_9 and hopdonglaodong.sotien_khoanmuc_9:
                    khoankhac = khoankhac + u'%s %s 円' % (hopdonglaodong.noidung_khoanmuc_9,
                                                          str(format_hoso.place_value(
                                                              hopdonglaodong.sotien_khoanmuc_9)))
                elif hopdonglaodong.chiphi_khoanmuc_9:
                    khoankhac = khoankhac + u'%s %s' % (hopdonglaodong.noidung_khoanmuc_9, '実費')
                elif hopdonglaodong.sotien_khoanmuc_9:
                    khoankhac = khoankhac + u'%s %s 円' % (hopdonglaodong.noidung_khoanmuc_9,
                                                          str(format_hoso.place_value(
                                                              hopdonglaodong.sotien_khoanmuc_9)))
                else:
                    khoankhac = khoankhac + u'%s' % (hopdonglaodong.noidung_khoanmuc_9)

            if hopdonglaodong.noidung_khoanmuc_10:
                if hopdonglaodong.chiphi_khoanmuc_10 and hopdonglaodong.sotien_khoanmuc_10:
                    khoankhac = khoankhac + u', %s %s 円' % (hopdonglaodong.noidung_khoanmuc_10,
                                                            str(format_hoso.place_value(
                                                                hopdonglaodong.sotien_khoanmuc_10)))
                elif hopdonglaodong.chiphi_khoanmuc_10:
                    khoankhac = khoankhac + u', %s %s' % (hopdonglaodong.noidung_khoanmuc_10, '実費')
                elif hopdonglaodong.sotien_khoanmuc_10:
                    khoankhac = khoankhac + u', %s %s 円' % (hopdonglaodong.noidung_khoanmuc_10,
                                                            str(format_hoso.place_value(
                                                                hopdonglaodong.sotien_khoanmuc_10)))
                else:
                    khoankhac = khoankhac + u', %s' % (hopdonglaodong.noidung_khoanmuc_10)

            if hopdonglaodong.noidung_khoanmuc_11:
                if hopdonglaodong.chiphi_khoanmuc_11 and hopdonglaodong.sotien_khoanmuc_11:
                    khoankhac = khoankhac + u', %s %s 円' % (hopdonglaodong.noidung_khoanmuc_11,
                                                            str(format_hoso.place_value(
                                                                hopdonglaodong.sotien_khoanmuc_11)))
                elif hopdonglaodong.chiphi_khoanmuc_11:
                    khoankhac = khoankhac + u', %s %s' % (hopdonglaodong.noidung_khoanmuc_11, '実費')
                elif hopdonglaodong.sotien_khoanmuc_11:
                    khoankhac = khoankhac + u', %s %s 円' % (hopdonglaodong.noidung_khoanmuc_11,
                                                            str(format_hoso.place_value(
                                                                hopdonglaodong.sotien_khoanmuc_11)))
                else:
                    khoankhac = khoankhac + u', %s' % (hopdonglaodong.noidung_khoanmuc_11)

            if hopdonglaodong.noidung_khoanmuc_12:
                if hopdonglaodong.chiphi_khoanmuc_12 and hopdonglaodong.sotien_khoanmuc_12:
                    khoankhac = khoankhac + u', %s %s 円' % (hopdonglaodong.noidung_khoanmuc_12,
                                                            str(format_hoso.place_value(
                                                                hopdonglaodong.sotien_khoanmuc_12)))
                elif hopdonglaodong.chiphi_khoanmuc_12:
                    khoankhac = khoankhac + u', %s %s' % (hopdonglaodong.noidung_khoanmuc_12, '実費')
                elif hopdonglaodong.sotien_khoanmuc_12:
                    khoankhac = khoankhac + u', %s %s 円' % (hopdonglaodong.noidung_khoanmuc_12,
                                                            str(format_hoso.place_value(
                                                                hopdonglaodong.sotien_khoanmuc_12)))
                else:
                    khoankhac = khoankhac + u', %s' % (hopdonglaodong.noidung_khoanmuc_12)

            if hopdonglaodong.noidung_khoanmuc_13:
                if hopdonglaodong.chiphi_khoanmuc_13 and hopdonglaodong.sotien_khoanmuc_13:
                    khoankhac = khoankhac + u', %s %s 円' % (hopdonglaodong.noidung_khoanmuc_13,
                                                            str(format_hoso.place_value(
                                                                hopdonglaodong.sotien_khoanmuc_13)))
                elif hopdonglaodong.chiphi_khoanmuc_13:
                    khoankhac = khoankhac + u', %s %s' % (hopdonglaodong.noidung_khoanmuc_13, '実費')
                elif hopdonglaodong.sotien_khoanmuc_13:
                    khoankhac = khoankhac + u', %s %s 円' % (hopdonglaodong.noidung_khoanmuc_13,
                                                            str(format_hoso.place_value(
                                                                hopdonglaodong.sotien_khoanmuc_13)))
                else:
                    khoankhac = khoankhac + u', %s' % (hopdonglaodong.noidung_khoanmuc_13)

            context['hhjp_0312_1'] = format_hoso.kiemtra(khoankhac)

            context['bc_0159'] = format_hoso.kiemtra(
                document.donhang_hoso.nghiepdoan_donhang.chucvu_daidien_phienam_nghiepdoan)
            context['xn_104'] = format_hoso.kiemtra(document.xinghiep_hoso.chucvu_daidien_han)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_3(self, ma_thuctapsinh, document):
        print('3')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_3")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            daotaosau = self.env['daotao.daotaosau'].search([('donhang_daotaosau', '=', thuctapsinh.donhang_tts.id)],
                                                            limit=1)
            if daotaosau.coso_daotaosau_a:
                context['hhjp_0174_1'] = format_hoso.kiemtra(daotaosau.coso_daotaosau_a.ten_han_cosodaotao)
                context['hhjp_0175_1'] = format_hoso.kiemtra(daotaosau.coso_daotaosau_a.diachi_cosodaotao)
                context['hhjp_0176_1'] = format_hoso.kiemtra(daotaosau.coso_daotaosau_a.dienthoai_cosodaotao)
            else:
                context['hhjp_0174_1'] = ''
                context['hhjp_0175_1'] = ''
                context['hhjp_0176_1'] = ''

            if daotaosau.coso_daotaosau_b:
                context['hhjp_0174_2'] = format_hoso.kiemtra(daotaosau.coso_daotaosau_b.ten_han_cosodaotao)
                context['hhjp_0175_2'] = format_hoso.kiemtra(daotaosau.coso_daotaosau_b.diachi_cosodaotao)
                context['hhjp_0176_2'] = format_hoso.kiemtra(daotaosau.coso_daotaosau_b.dienthoai_cosodaotao)
            else:
                context['hhjp_0174_2'] = ''
                context['hhjp_0175_2'] = ''
                context['hhjp_0176_2'] = ''

            if daotaosau.coso_daotaosau_c:
                context['hhjp_0174_3'] = format_hoso.kiemtra(daotaosau.coso_daotaosau_c.ten_han_cosodaotao)
                context['hhjp_0175_3'] = format_hoso.kiemtra(daotaosau.coso_daotaosau_c.diachi_cosodaotao)
                context['hhjp_0176_3'] = format_hoso.kiemtra(daotaosau.coso_daotaosau_c.dienthoai_cosodaotao)
            else:
                context['hhjp_0174_3'] = ''
                context['hhjp_0175_3'] = ''
                context['hhjp_0176_3'] = ''

            context['hhjp_0097'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_han_nghiepdoan)
            context['hhjp_0098'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.diachi_nghiepdoan)
            context['hhjp_0103'] = format_hoso.kiemtra(
                document.donhang_hoso.nghiepdoan_donhang.ten_daidien_han_nghiepdoan)
            context['hhjp_0320'] = format_hoso.kiemtra(daotaosau.giangvien_phapluat_daotaosau)
            context['hhjp_0321'] = format_hoso.kiemtra(daotaosau.nghenghiep_giangvien_daotaosau)
            context['hhjp_0322'] = format_hoso.kiemtra(daotaosau.coquan_giangvien_daotaosau)
            context['hhjp_0323'] = format_hoso.kiemtra(daotaosau.kinhnghiem_giangvien_daotaosau)
            context['hhjp_0324'] = format_hoso.kiemtra(daotaosau.bangcap_giangvien_daotaosau)
            context['hhjp_0325'] = format_hoso.convert_date(daotaosau.batdau_daotao)
            context['hhjp_0326'] = format_hoso.convert_date(daotaosau.ketthuc_daotao)
            context['dt_0001'] = format_hoso.convert(daotaosau.batdau_sang)
            context['dt_0002'] = format_hoso.convert(daotaosau.ketthuc_sang)
            context['dt_0003'] = format_hoso.convert(daotaosau.batdau_chieu)
            context['dt_0004'] = format_hoso.convert(daotaosau.ketthuc_chieu)
            context['dt_0004'] = format_hoso.convert(daotaosau.ketthuc_chieu)
            context['hhjp_0250'] = format_hoso.kiemtra(daotaosau.tonggio_daotao)

            table_daotaosau = []

            count_item = 0
            # Dòng 1
            infor1 = {}
            infor1['dt5'] = format_hoso.kiemtra(daotaosau.thang_1)
            infor1['dt13'] = format_hoso.kiemtra(daotaosau.ngay_1)
            infor1['dt14'] = format_hoso.kiemtra(daotaosau.thu_1)
            if daotaosau.cangay_1 == True:
                infor1['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_1.noidungdaotao)
                infor1['dt7'] = u'◯' if daotaosau.coso_sang_1 else ''
                infor1['dt8'] = ''
                infor1['dt9'] = ''
                if daotaosau.cosao_daotao_a_1:
                    infor1['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_1:
                    infor1['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_1:
                    infor1['dt10'] = u'③'
                else:
                    infor1['dt10'] = ''
                infor1['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_1)
                infor1['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_1.ten_giangvien_phaply)
                table_daotaosau.append(infor1)

                infor2 = {}
                infor2['dt5'] = ''
                infor2['dt13'] = ''
                infor2['dt14'] = ''
                infor2['dt6'] = ''
                infor2['dt7'] = ''
                infor2['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_1.noidungdaotao)
                infor2['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_1 else ''

                if daotaosau.coso_chieu_chieu_a_1:
                    infor2['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_1:
                    infor2['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_1:
                    infor2['dt10'] = u'③'
                else:
                    infor2['dt10'] = ''

                infor2['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_1)
                infor2['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_1.ten_giangvien_phaply)
                table_daotaosau.append(infor2)
                count_item += 2
            else:
                if daotaosau.ngaynghi_1:
                    infor1['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_1)
                    infor1['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_1)
                else:
                    infor1['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_1.noidungdaotao)
                    infor1['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_1.noidungdaotao)
                infor1['dt7'] = u'◯' if daotaosau.coso_sang_1 else ''
                infor1['dt9'] = u'◯' if daotaosau.coso_chieu_1 else ''
                if daotaosau.cosao_daotao_a_1:
                    infor1['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_1:
                    infor1['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_1:
                    infor1['dt10'] = u'③'
                else:
                    infor1['dt10'] = ''

                infor1['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_1)
                infor1['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_1.ten_giangvien_phaply)
                table_daotaosau.append(infor1)
                count_item += 1

            # Dòng 2
            infor3 = {}
            infor3['dt5'] = format_hoso.kiemtra(daotaosau.thang_2)
            infor3['dt13'] = format_hoso.kiemtra(daotaosau.ngay_2)
            infor3['dt14'] = format_hoso.kiemtra(daotaosau.thu_2)
            if daotaosau.cangay_2 == True:
                infor3['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_2.noidungdaotao)
                infor3['dt7'] = u'◯' if daotaosau.coso_sang_2 else ''
                infor3['dt8'] = ''
                infor3['dt9'] = ''
                if daotaosau.cosao_daotao_a_2:
                    infor3['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_2:
                    infor3['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_2:
                    infor3['dt10'] = u'③'
                else:
                    infor3['dt10'] = ''
                infor3['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_2)
                infor3['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_2.ten_giangvien_phaply)
                table_daotaosau.append(infor3)

                infor4 = {}
                infor4['dt5'] = ''
                infor4['dt13'] = ''
                infor4['dt14'] = ''
                infor4['dt6'] = ''
                infor4['dt7'] = ''
                infor4['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_2.noidungdaotao)
                infor4['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_2 else ''

                if daotaosau.coso_chieu_chieu_a_2:
                    infor4['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_2:
                    infor4['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_2:
                    infor4['dt10'] = u'③'
                else:
                    infor4['dt10'] = ''

                infor4['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_2)
                infor4['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_2.ten_giangvien_phaply)
                table_daotaosau.append(infor4)
                count_item += 2
            else:
                if daotaosau.ngaynghi_2:
                    infor3['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_2)
                    infor3['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_2)
                else:
                    infor3['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_2.noidungdaotao)
                    infor3['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_2.noidungdaotao)
                infor3['dt7'] = u'◯' if daotaosau.coso_sang_2 else ''
                infor3['dt9'] = u'◯' if daotaosau.coso_chieu_2 else ''
                if daotaosau.cosao_daotao_a_2:
                    infor3['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_2:
                    infor3['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_2:
                    infor3['dt10'] = u'③'
                else:
                    infor3['dt10'] = ''

                infor3['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_2)
                infor3['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_2.ten_giangvien_phaply)
                table_daotaosau.append(infor3)
                count_item += 1

            # Dòng 3
            infor5 = {}
            infor5['dt5'] = format_hoso.kiemtra(daotaosau.thang_3)
            infor5['dt13'] = format_hoso.kiemtra(daotaosau.ngay_3)
            infor5['dt14'] = format_hoso.kiemtra(daotaosau.thu_3)
            if daotaosau.cangay_3 == True:
                infor5['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_3.noidungdaotao)
                infor5['dt7'] = u'◯' if daotaosau.coso_sang_3 else ''
                infor5['dt8'] = ''
                infor5['dt9'] = ''
                if daotaosau.cosao_daotao_a_3:
                    infor5['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_3:
                    infor5['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_3:
                    infor5['dt10'] = u'③'
                else:
                    infor5['dt10'] = ''
                infor5['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_3)
                infor5['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_3.ten_giangvien_phaply)
                table_daotaosau.append(infor5)

                infor6 = {}
                infor6['dt5'] = ''
                infor6['dt13'] = ''
                infor6['dt14'] = ''
                infor6['dt6'] = ''
                infor6['dt7'] = ''
                infor6['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_3.noidungdaotao)
                infor6['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_3 else ''

                if daotaosau.coso_chieu_chieu_a_3:
                    infor6['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_3:
                    infor6['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_3:
                    infor6['dt10'] = u'③'
                else:
                    infor6['dt10'] = ''

                infor6['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_3)
                infor6['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_3.ten_giangvien_phaply)
                table_daotaosau.append(infor6)
                count_item += 2
            else:
                if daotaosau.ngaynghi_3:
                    infor5['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_3)
                    infor5['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_3)
                else:
                    infor5['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_3.noidungdaotao)
                    infor5['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_3.noidungdaotao)
                infor5['dt7'] = u'◯' if daotaosau.coso_sang_3 else ''
                infor5['dt9'] = u'◯' if daotaosau.coso_chieu_3 else ''
                if daotaosau.cosao_daotao_a_3:
                    infor5['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_3:
                    infor5['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_3:
                    infor5['dt10'] = u'③'
                else:
                    infor5['dt10'] = ''

                infor5['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_3)
                infor5['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_3.ten_giangvien_phaply)
                table_daotaosau.append(infor5)
                count_item += 1

            # Dòng 4
            infor7 = {}
            infor7['dt5'] = format_hoso.kiemtra(daotaosau.thang_4)
            infor7['dt13'] = format_hoso.kiemtra(daotaosau.ngay_4)
            infor7['dt14'] = format_hoso.kiemtra(daotaosau.thu_4)
            if daotaosau.cangay_4 == True:
                infor7['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_4.noidungdaotao)
                infor7['dt7'] = u'◯' if daotaosau.coso_sang_4 else ''
                infor7['dt8'] = ''
                infor7['dt9'] = ''
                if daotaosau.cosao_daotao_a_4:
                    infor7['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_4:
                    infor7['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_4:
                    infor7['dt10'] = u'③'
                else:
                    infor7['dt10'] = ''
                infor7['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_4)
                infor7['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_4.ten_giangvien_phaply)
                table_daotaosau.append(infor7)

                infor8 = {}
                infor8['dt5'] = ''
                infor8['dt13'] = ''
                infor8['dt14'] = ''
                infor8['dt6'] = ''
                infor8['dt7'] = ''
                infor8['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_4.noidungdaotao)
                infor8['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_4 else ''

                if daotaosau.coso_chieu_chieu_a_4:
                    infor8['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_4:
                    infor8['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_4:
                    infor8['dt10'] = u'③'
                else:
                    infor8['dt10'] = ''

                infor8['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_4)
                infor8['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_4.ten_giangvien_phaply)
                table_daotaosau.append(infor8)
                count_item += 2
            else:
                if daotaosau.ngaynghi_4:
                    infor7['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_4)
                    infor7['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_4)
                else:
                    infor7['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_4.noidungdaotao)
                    infor7['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_4.noidungdaotao)
                infor7['dt7'] = u'◯' if daotaosau.coso_sang_4 else ''
                infor7['dt9'] = u'◯' if daotaosau.coso_chieu_4 else ''
                if daotaosau.cosao_daotao_a_4:
                    infor7['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_4:
                    infor7['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_4:
                    infor7['dt10'] = u'③'
                else:
                    infor7['dt10'] = ''

                infor7['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_4)
                infor7['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_4.ten_giangvien_phaply)
                table_daotaosau.append(infor7)
                count_item += 1

            # Dòng 5
            infor9 = {}
            infor9['dt5'] = format_hoso.kiemtra(daotaosau.thang_5)
            infor9['dt13'] = format_hoso.kiemtra(daotaosau.ngay_5)
            infor9['dt14'] = format_hoso.kiemtra(daotaosau.thu_5)
            if daotaosau.cangay_5 == True:
                infor9['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_5.noidungdaotao)
                infor9['dt7'] = u'◯' if daotaosau.coso_sang_5 else ''
                infor9['dt8'] = ''
                infor9['dt9'] = ''
                if daotaosau.cosao_daotao_a_5:
                    infor9['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_5:
                    infor9['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_5:
                    infor9['dt10'] = u'③'
                else:
                    infor9['dt10'] = ''
                infor9['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_5)
                infor9['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_5.ten_giangvien_phaply)
                table_daotaosau.append(infor9)

                infor10 = {}
                infor10['dt5'] = ''
                infor10['dt13'] = ''
                infor10['dt14'] = ''
                infor10['dt6'] = ''
                infor10['dt7'] = ''
                infor10['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_5.noidungdaotao)
                infor10['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_5 else ''

                if daotaosau.coso_chieu_chieu_a_5:
                    infor10['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_5:
                    infor10['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_5:
                    infor10['dt10'] = u'③'
                else:
                    infor10['dt10'] = ''

                infor10['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_5)
                infor10['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_5.ten_giangvien_phaply)
                table_daotaosau.append(infor10)
                count_item += 2
            else:
                if daotaosau.ngaynghi_5:
                    infor9['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_5)
                    infor9['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_5)
                else:
                    infor9['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_5.noidungdaotao)
                    infor9['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_5.noidungdaotao)
                infor9['dt7'] = u'◯' if daotaosau.coso_sang_5 else ''
                infor9['dt9'] = u'◯' if daotaosau.coso_chieu_5 else ''
                if daotaosau.cosao_daotao_a_5:
                    infor9['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_5:
                    infor9['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_5:
                    infor9['dt10'] = u'③'
                else:
                    infor9['dt10'] = ''

                infor9['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_5)
                infor9['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_5.ten_giangvien_phaply)
                table_daotaosau.append(infor9)
                count_item += 1

            # Dòng 6
            infor11 = {}
            infor11['dt5'] = format_hoso.kiemtra(daotaosau.thang_6)
            infor11['dt13'] = format_hoso.kiemtra(daotaosau.ngay_6)
            infor11['dt14'] = format_hoso.kiemtra(daotaosau.thu_6)
            if daotaosau.cangay_6 == True:
                infor11['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_6.noidungdaotao)
                infor11['dt7'] = u'◯' if daotaosau.coso_sang_6 else ''
                infor11['dt8'] = ''
                infor11['dt9'] = ''
                if daotaosau.cosao_daotao_a_6:
                    infor11['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_6:
                    infor11['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_6:
                    infor11['dt10'] = u'③'
                else:
                    infor11['dt10'] = ''
                infor11['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_6)
                infor11['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_6.ten_giangvien_phaply)
                table_daotaosau.append(infor11)

                infor12 = {}
                infor12['dt5'] = ''
                infor12['dt13'] = ''
                infor12['dt14'] = ''
                infor12['dt6'] = ''
                infor12['dt7'] = ''
                infor12['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_6.noidungdaotao)
                infor12['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_6 else ''

                if daotaosau.coso_chieu_chieu_a_6:
                    infor12['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_6:
                    infor12['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_6:
                    infor12['dt10'] = u'③'
                else:
                    infor12['dt10'] = ''

                infor12['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_6)
                infor12['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_6.ten_giangvien_phaply)
                table_daotaosau.append(infor12)
                count_item += 2
            else:
                if daotaosau.ngaynghi_6:
                    infor11['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_6)
                    infor11['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_6)
                else:
                    infor11['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_6.noidungdaotao)
                    infor11['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_6.noidungdaotao)
                infor11['dt7'] = u'◯' if daotaosau.coso_sang_6 else ''
                infor11['dt9'] = u'◯' if daotaosau.coso_chieu_6 else ''
                if daotaosau.cosao_daotao_a_6:
                    infor11['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_6:
                    infor11['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_6:
                    infor11['dt10'] = u'③'
                else:
                    infor11['dt10'] = ''

                infor11['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_6)
                infor11['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_6.ten_giangvien_phaply)
                table_daotaosau.append(infor11)
                count_item += 1

            # Dòng 7
            infor13 = {}
            infor13['dt5'] = format_hoso.kiemtra(daotaosau.thang_7)
            infor13['dt13'] = format_hoso.kiemtra(daotaosau.ngay_7)
            infor13['dt14'] = format_hoso.kiemtra(daotaosau.thu_7)
            if daotaosau.cangay_7 == True:
                infor13['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_7.noidungdaotao)
                infor13['dt7'] = u'◯' if daotaosau.coso_sang_7 else ''
                infor13['dt8'] = ''
                infor13['dt9'] = ''
                if daotaosau.cosao_daotao_a_7:
                    infor13['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_7:
                    infor13['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_7:
                    infor13['dt10'] = u'③'
                else:
                    infor13['dt10'] = ''
                infor13['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_7)
                infor13['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_7.ten_giangvien_phaply)
                table_daotaosau.append(infor13)

                infor14 = {}
                infor14['dt5'] = ''
                infor14['dt13'] = ''
                infor14['dt14'] = ''
                infor14['dt6'] = ''
                infor14['dt7'] = ''
                infor14['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_7.noidungdaotao)
                infor14['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_7 else ''

                if daotaosau.coso_chieu_chieu_a_7:
                    infor14['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_7:
                    infor14['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_7:
                    infor14['dt10'] = u'③'
                else:
                    infor14['dt10'] = ''

                infor14['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_7)
                infor14['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_7.ten_giangvien_phaply)
                table_daotaosau.append(infor14)
                count_item += 2
            else:
                if daotaosau.ngaynghi_7:
                    infor13['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_7)
                    infor13['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_7)
                else:
                    infor13['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_7.noidungdaotao)
                    infor13['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_7.noidungdaotao)
                infor13['dt7'] = u'◯' if daotaosau.coso_sang_7 else ''
                infor13['dt9'] = u'◯' if daotaosau.coso_chieu_7 else ''
                if daotaosau.cosao_daotao_a_7:
                    infor13['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_7:
                    infor13['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_7:
                    infor13['dt10'] = u'③'
                else:
                    infor13['dt10'] = ''

                infor13['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_7)
                infor13['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_7.ten_giangvien_phaply)
                table_daotaosau.append(infor13)
                count_item += 1

            # Dòng 8
            infor15 = {}
            infor15['dt5'] = format_hoso.kiemtra(daotaosau.thang_8)
            infor15['dt13'] = format_hoso.kiemtra(daotaosau.ngay_8)
            infor15['dt14'] = format_hoso.kiemtra(daotaosau.thu_8)
            if daotaosau.cangay_8 == True:
                infor15['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_8.noidungdaotao)
                infor15['dt7'] = u'◯' if daotaosau.coso_sang_8 else ''
                infor15['dt8'] = ''
                infor15['dt9'] = ''
                if daotaosau.cosao_daotao_a_8:
                    infor15['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_8:
                    infor15['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_8:
                    infor15['dt10'] = u'③'
                else:
                    infor15['dt10'] = ''
                infor15['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_8)
                infor15['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_8.ten_giangvien_phaply)
                table_daotaosau.append(infor15)

                infor16 = {}
                infor16['dt5'] = ''
                infor16['dt13'] = ''
                infor16['dt14'] = ''
                infor16['dt6'] = ''
                infor16['dt7'] = ''
                infor16['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_8.noidungdaotao)
                infor16['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_8 else ''

                if daotaosau.coso_chieu_chieu_a_8:
                    infor16['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_8:
                    infor16['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_8:
                    infor16['dt10'] = u'③'
                else:
                    infor16['dt10'] = ''

                infor16['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_8)
                infor16['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_8.ten_giangvien_phaply)
                table_daotaosau.append(infor16)
                count_item += 2
            else:
                if daotaosau.ngaynghi_8:
                    infor15['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_8)
                    infor15['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_8)
                else:
                    infor15['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_8.noidungdaotao)
                    infor15['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_8.noidungdaotao)
                infor15['dt7'] = u'◯' if daotaosau.coso_sang_8 else ''
                infor15['dt9'] = u'◯' if daotaosau.coso_chieu_8 else ''
                if daotaosau.cosao_daotao_a_8:
                    infor15['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_8:
                    infor15['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_8:
                    infor15['dt10'] = u'③'
                else:
                    infor15['dt10'] = ''

                infor15['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_8)
                infor15['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_8.ten_giangvien_phaply)
                table_daotaosau.append(infor15)
                count_item += 1

            # Dòng 9
            infor17 = {}
            infor17['dt5'] = format_hoso.kiemtra(daotaosau.thang_9)
            infor17['dt13'] = format_hoso.kiemtra(daotaosau.ngay_9)
            infor17['dt14'] = format_hoso.kiemtra(daotaosau.thu_9)
            if daotaosau.cangay_9 == True:
                infor17['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_9.noidungdaotao)
                infor17['dt7'] = u'◯' if daotaosau.coso_sang_9 else ''
                infor17['dt8'] = ''
                infor17['dt9'] = ''
                if daotaosau.cosao_daotao_a_9:
                    infor17['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_9:
                    infor17['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_9:
                    infor17['dt10'] = u'③'
                else:
                    infor17['dt10'] = ''
                infor17['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_9)
                infor17['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_9.ten_giangvien_phaply)
                table_daotaosau.append(infor17)

                infor18 = {}
                infor18['dt5'] = ''
                infor18['dt13'] = ''
                infor18['dt14'] = ''
                infor18['dt6'] = ''
                infor18['dt7'] = ''
                infor18['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_9.noidungdaotao)
                infor18['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_9 else ''

                if daotaosau.coso_chieu_chieu_a_9:
                    infor18['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_9:
                    infor18['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_9:
                    infor18['dt10'] = u'③'
                else:
                    infor18['dt10'] = ''

                infor18['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_9)
                infor18['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_9.ten_giangvien_phaply)
                table_daotaosau.append(infor18)
                count_item += 2
            else:
                if daotaosau.ngaynghi_9:
                    infor17['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_9)
                    infor17['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_9)
                else:
                    infor17['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_9.noidungdaotao)
                    infor17['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_9.noidungdaotao)
                infor17['dt7'] = u'◯' if daotaosau.coso_sang_9 else ''
                infor17['dt9'] = u'◯' if daotaosau.coso_chieu_9 else ''
                if daotaosau.cosao_daotao_a_9:
                    infor17['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_9:
                    infor17['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_9:
                    infor17['dt10'] = u'③'
                else:
                    infor17['dt10'] = ''

                infor17['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_9)
                infor17['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_9.ten_giangvien_phaply)
                table_daotaosau.append(infor17)
                count_item += 1

            # Dòng 10
            infor19 = {}
            infor19['dt5'] = format_hoso.kiemtra(daotaosau.thang_10)
            infor19['dt13'] = format_hoso.kiemtra(daotaosau.ngay_10)
            infor19['dt14'] = format_hoso.kiemtra(daotaosau.thu_10)
            if daotaosau.cangay_10 == True:
                infor19['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_10.noidungdaotao)
                infor19['dt7'] = u'◯' if daotaosau.coso_sang_10 else ''
                infor19['dt8'] = ''
                infor19['dt9'] = ''
                if daotaosau.cosao_daotao_a_10:
                    infor19['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_10:
                    infor19['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_10:
                    infor19['dt10'] = u'③'
                else:
                    infor19['dt10'] = ''
                infor19['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_10)
                infor19['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_10.ten_giangvien_phaply)
                table_daotaosau.append(infor19)

                infor20 = {}
                infor20['dt5'] = ''
                infor20['dt13'] = ''
                infor20['dt14'] = ''
                infor20['dt6'] = ''
                infor20['dt7'] = ''
                infor20['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_10.noidungdaotao)
                infor20['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_10 else ''

                if daotaosau.coso_chieu_chieu_a_10:
                    infor20['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_10:
                    infor20['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_10:
                    infor20['dt10'] = u'③'
                else:
                    infor20['dt10'] = ''

                infor20['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_10)
                infor20['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_10.ten_giangvien_phaply)
                table_daotaosau.append(infor20)
                count_item += 2
            else:
                if daotaosau.ngaynghi_10:
                    infor19['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_10)
                    infor19['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_10)
                else:
                    infor19['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_10.noidungdaotao)
                    infor19['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_10.noidungdaotao)
                infor19['dt7'] = u'◯' if daotaosau.coso_sang_10 else ''
                infor19['dt9'] = u'◯' if daotaosau.coso_chieu_10 else ''
                if daotaosau.cosao_daotao_a_10:
                    infor19['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_10:
                    infor19['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_10:
                    infor19['dt10'] = u'③'
                else:
                    infor19['dt10'] = ''

                infor19['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_10)
                infor19['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_10.ten_giangvien_phaply)
                table_daotaosau.append(infor19)
                count_item += 1

            # Dòng 11
            infor21 = {}
            infor21['dt5'] = format_hoso.kiemtra(daotaosau.thang_11)
            infor21['dt13'] = format_hoso.kiemtra(daotaosau.ngay_11)
            infor21['dt14'] = format_hoso.kiemtra(daotaosau.thu_11)
            if daotaosau.cangay_11 == True:
                infor21['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_11.noidungdaotao)
                infor21['dt7'] = u'◯' if daotaosau.coso_sang_11 else ''
                infor21['dt8'] = ''
                infor21['dt9'] = ''
                if daotaosau.cosao_daotao_a_11:
                    infor21['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_11:
                    infor21['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_11:
                    infor21['dt10'] = u'③'
                else:
                    infor21['dt10'] = ''
                infor21['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_11)
                infor21['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_11.ten_giangvien_phaply)
                table_daotaosau.append(infor21)

                infor22 = {}
                infor22['dt5'] = ''
                infor22['dt13'] = ''
                infor22['dt14'] = ''
                infor22['dt6'] = ''
                infor22['dt7'] = ''
                infor22['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_11.noidungdaotao)
                infor22['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_11 else ''

                if daotaosau.coso_chieu_chieu_a_11:
                    infor22['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_11:
                    infor22['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_11:
                    infor22['dt10'] = u'③'
                else:
                    infor22['dt10'] = ''

                infor22['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_11)
                infor22['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_11.ten_giangvien_phaply)
                table_daotaosau.append(infor22)
                count_item += 2
            else:
                if daotaosau.ngaynghi_11:
                    infor21['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_11)
                    infor21['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_11)
                else:
                    infor21['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_11.noidungdaotao)
                    infor21['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_11.noidungdaotao)
                infor21['dt7'] = u'◯' if daotaosau.coso_sang_11 else ''
                infor21['dt9'] = u'◯' if daotaosau.coso_chieu_11 else ''
                if daotaosau.cosao_daotao_a_11:
                    infor21['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_11:
                    infor21['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_11:
                    infor21['dt10'] = u'③'
                else:
                    infor21['dt10'] = ''

                infor21['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_11)
                infor21['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_11.ten_giangvien_phaply)
                table_daotaosau.append(infor21)
                count_item += 1

            # Dòng 12
            infor23 = {}
            infor23['dt5'] = format_hoso.kiemtra(daotaosau.thang_12)
            infor23['dt13'] = format_hoso.kiemtra(daotaosau.ngay_12)
            infor23['dt14'] = format_hoso.kiemtra(daotaosau.thu_12)
            if daotaosau.cangay_12 == True:
                infor23['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_12.noidungdaotao)
                infor23['dt7'] = u'◯' if daotaosau.coso_sang_12 else ''
                infor23['dt8'] = ''
                infor23['dt9'] = ''
                if daotaosau.cosao_daotao_a_12:
                    infor23['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_12:
                    infor23['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_12:
                    infor23['dt10'] = u'③'
                else:
                    infor23['dt10'] = ''
                infor23['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_12)
                infor23['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_12.ten_giangvien_phaply)
                table_daotaosau.append(infor23)

                infor24 = {}
                infor24['dt5'] = ''
                infor24['dt13'] = ''
                infor24['dt14'] = ''
                infor24['dt6'] = ''
                infor24['dt7'] = ''
                infor24['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_12.noidungdaotao)
                infor24['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_12 else ''

                if daotaosau.coso_chieu_chieu_a_12:
                    infor24['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_12:
                    infor24['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_12:
                    infor24['dt10'] = u'③'
                else:
                    infor24['dt10'] = ''

                infor24['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_12)
                infor24['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_12.ten_giangvien_phaply)
                table_daotaosau.append(infor24)
                count_item += 2
            else:
                if daotaosau.ngaynghi_12:
                    infor23['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_12)
                    infor23['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_12)
                else:
                    infor23['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_12.noidungdaotao)
                    infor23['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_12.noidungdaotao)
                infor23['dt7'] = u'◯' if daotaosau.coso_sang_12 else ''
                infor23['dt9'] = u'◯' if daotaosau.coso_chieu_12 else ''
                if daotaosau.cosao_daotao_a_12:
                    infor23['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_12:
                    infor23['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_12:
                    infor23['dt10'] = u'③'
                else:
                    infor23['dt10'] = ''

                infor23['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_12)
                infor23['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_12.ten_giangvien_phaply)
                table_daotaosau.append(infor23)
                count_item += 1

            # Dòng 13
            infor25 = {}
            infor25['dt5'] = format_hoso.kiemtra(daotaosau.thang_13)
            infor25['dt13'] = format_hoso.kiemtra(daotaosau.ngay_13)
            infor25['dt14'] = format_hoso.kiemtra(daotaosau.thu_13)
            if daotaosau.cangay_13 == True:
                infor25['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_13.noidungdaotao)
                infor25['dt7'] = u'◯' if daotaosau.coso_sang_13 else ''
                infor25['dt8'] = ''
                infor25['dt9'] = ''
                if daotaosau.cosao_daotao_a_13:
                    infor25['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_13:
                    infor25['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_13:
                    infor25['dt10'] = u'③'
                else:
                    infor25['dt10'] = ''
                infor25['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_13)
                infor25['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_13.ten_giangvien_phaply)
                table_daotaosau.append(infor25)

                infor26 = {}
                infor26['dt5'] = ''
                infor26['dt13'] = ''
                infor26['dt14'] = ''
                infor26['dt6'] = ''
                infor26['dt7'] = ''
                infor26['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_13.noidungdaotao)
                infor26['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_13 else ''

                if daotaosau.coso_chieu_chieu_a_13:
                    infor26['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_13:
                    infor26['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_13:
                    infor26['dt10'] = u'③'
                else:
                    infor26['dt10'] = ''

                infor26['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_13)
                infor26['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_13.ten_giangvien_phaply)
                table_daotaosau.append(infor26)
                count_item += 2
            else:
                if daotaosau.ngaynghi_13:
                    infor25['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_13)
                    infor25['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_13)
                else:
                    infor25['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_13.noidungdaotao)
                    infor25['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_13.noidungdaotao)
                infor25['dt7'] = u'◯' if daotaosau.coso_sang_13 else ''
                infor25['dt9'] = u'◯' if daotaosau.coso_chieu_13 else ''
                if daotaosau.cosao_daotao_a_13:
                    infor25['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_13:
                    infor25['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_13:
                    infor25['dt10'] = u'③'
                else:
                    infor25['dt10'] = ''

                infor25['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_13)
                infor25['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_13.ten_giangvien_phaply)
                table_daotaosau.append(infor25)
                count_item += 1

            # Dòng 14
            infor27 = {}
            infor27['dt5'] = format_hoso.kiemtra(daotaosau.thang_14)
            infor27['dt13'] = format_hoso.kiemtra(daotaosau.ngay_14)
            infor27['dt14'] = format_hoso.kiemtra(daotaosau.thu_14)
            if daotaosau.cangay_14 == True:
                infor27['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_14.noidungdaotao)
                infor27['dt7'] = u'◯' if daotaosau.coso_sang_14 else ''
                infor27['dt8'] = ''
                infor27['dt9'] = ''
                if daotaosau.cosao_daotao_a_14:
                    infor27['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_14:
                    infor27['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_14:
                    infor27['dt10'] = u'③'
                else:
                    infor27['dt10'] = ''
                infor27['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_14)
                infor27['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_14.ten_giangvien_phaply)
                table_daotaosau.append(infor27)

                infor28 = {}
                infor28['dt5'] = ''
                infor28['dt13'] = ''
                infor28['dt14'] = ''
                infor28['dt6'] = ''
                infor28['dt7'] = ''
                infor28['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_14.noidungdaotao)
                infor28['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_14 else ''

                if daotaosau.coso_chieu_chieu_a_14:
                    infor28['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_14:
                    infor28['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_14:
                    infor28['dt10'] = u'③'
                else:
                    infor28['dt10'] = ''

                infor28['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_14)
                infor28['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_14.ten_giangvien_phaply)
                table_daotaosau.append(infor28)
                count_item += 2
            else:
                if daotaosau.ngaynghi_14:
                    infor27['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_14)
                    infor27['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_14)
                else:
                    infor27['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_14.noidungdaotao)
                    infor27['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_14.noidungdaotao)
                infor27['dt7'] = u'◯' if daotaosau.coso_sang_14 else ''
                infor27['dt9'] = u'◯' if daotaosau.coso_chieu_14 else ''
                if daotaosau.cosao_daotao_a_14:
                    infor27['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_14:
                    infor27['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_14:
                    infor27['dt10'] = u'③'
                else:
                    infor27['dt10'] = ''

                infor27['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_14)
                infor27['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_14.ten_giangvien_phaply)
                table_daotaosau.append(infor27)
                count_item += 1

            # Dòng 15
            infor29 = {}
            infor29['dt5'] = format_hoso.kiemtra(daotaosau.thang_15)
            infor29['dt13'] = format_hoso.kiemtra(daotaosau.ngay_15)
            infor29['dt14'] = format_hoso.kiemtra(daotaosau.thu_15)
            if daotaosau.cangay_15 == True:
                infor29['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_15.noidungdaotao)
                infor29['dt7'] = u'◯' if daotaosau.coso_sang_15 else ''
                infor29['dt8'] = ''
                infor29['dt9'] = ''
                if daotaosau.cosao_daotao_a_15:
                    infor29['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_15:
                    infor29['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_15:
                    infor29['dt10'] = u'③'
                else:
                    infor29['dt10'] = ''
                infor29['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_15)
                infor29['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_15.ten_giangvien_phaply)
                table_daotaosau.append(infor29)

                infor30 = {}
                infor30['dt5'] = ''
                infor30['dt13'] = ''
                infor30['dt14'] = ''
                infor30['dt6'] = ''
                infor30['dt7'] = ''
                infor30['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_15.noidungdaotao)
                infor30['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_15 else ''

                if daotaosau.coso_chieu_chieu_a_15:
                    infor30['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_15:
                    infor30['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_15:
                    infor30['dt10'] = u'③'
                else:
                    infor30['dt10'] = ''

                infor30['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_15)
                infor30['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_15.ten_giangvien_phaply)
                table_daotaosau.append(infor30)
                count_item += 2
            else:
                if daotaosau.ngaynghi_15:
                    infor29['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_15)
                    infor29['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_15)
                else:
                    infor29['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_15.noidungdaotao)
                    infor29['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_15.noidungdaotao)
                infor29['dt7'] = u'◯' if daotaosau.coso_sang_15 else ''
                infor29['dt9'] = u'◯' if daotaosau.coso_chieu_15 else ''
                if daotaosau.cosao_daotao_a_15:
                    infor29['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_15:
                    infor29['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_15:
                    infor29['dt10'] = u'③'
                else:
                    infor29['dt10'] = ''

                infor29['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_15)
                infor29['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_15.ten_giangvien_phaply)
                table_daotaosau.append(infor29)
                count_item += 1

            # Dòng 16
            infor31 = {}
            infor31['dt5'] = format_hoso.kiemtra(daotaosau.thang_16)
            infor31['dt13'] = format_hoso.kiemtra(daotaosau.ngay_16)
            infor31['dt14'] = format_hoso.kiemtra(daotaosau.thu_16)
            if daotaosau.cangay_16 == True:
                infor31['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_16.noidungdaotao)
                infor31['dt7'] = u'◯' if daotaosau.coso_sang_16 else ''
                infor31['dt8'] = ''
                infor31['dt9'] = ''
                if daotaosau.cosao_daotao_a_16:
                    infor31['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_16:
                    infor31['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_16:
                    infor31['dt10'] = u'③'
                else:
                    infor31['dt10'] = ''
                infor31['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_16)
                infor31['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_16.ten_giangvien_phaply)
                table_daotaosau.append(infor31)

                infor32 = {}
                infor32['dt5'] = ''
                infor32['dt13'] = ''
                infor32['dt14'] = ''
                infor32['dt6'] = ''
                infor32['dt7'] = ''
                infor32['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_16.noidungdaotao)
                infor32['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_16 else ''

                if daotaosau.coso_chieu_chieu_a_16:
                    infor32['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_16:
                    infor32['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_16:
                    infor32['dt10'] = u'③'
                else:
                    infor32['dt10'] = ''

                infor32['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_16)
                infor32['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_16.ten_giangvien_phaply)
                table_daotaosau.append(infor32)
                count_item += 2
            else:
                if daotaosau.ngaynghi_16:
                    infor31['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_16)
                    infor31['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_16)
                else:
                    infor31['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_16.noidungdaotao)
                    infor31['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_16.noidungdaotao)
                infor31['dt7'] = u'◯' if daotaosau.coso_sang_16 else ''
                infor31['dt9'] = u'◯' if daotaosau.coso_chieu_16 else ''
                if daotaosau.cosao_daotao_a_16:
                    infor31['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_16:
                    infor31['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_16:
                    infor31['dt10'] = u'③'
                else:
                    infor31['dt10'] = ''

                infor31['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_16)
                infor31['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_16.ten_giangvien_phaply)
                table_daotaosau.append(infor31)
                count_item += 1
            # Dòng 17
            infor33 = {}
            infor33['dt5'] = format_hoso.kiemtra(daotaosau.thang_17)
            infor33['dt13'] = format_hoso.kiemtra(daotaosau.ngay_17)
            infor33['dt14'] = format_hoso.kiemtra(daotaosau.thu_17)
            if daotaosau.cangay_17 == True:
                infor33['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_17.noidungdaotao)
                infor33['dt7'] = u'◯' if daotaosau.coso_sang_17 else ''
                infor33['dt8'] = ''
                infor33['dt9'] = ''
                if daotaosau.cosao_daotao_a_17:
                    infor33['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_17:
                    infor33['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_17:
                    infor33['dt10'] = u'③'
                else:
                    infor33['dt10'] = ''
                infor33['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_17)
                infor33['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_17.ten_giangvien_phaply)
                table_daotaosau.append(infor33)

                infor34 = {}
                infor34['dt5'] = ''
                infor34['dt13'] = ''
                infor34['dt14'] = ''
                infor34['dt6'] = ''
                infor34['dt7'] = ''
                infor34['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_17.noidungdaotao)
                infor34['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_17 else ''

                if daotaosau.coso_chieu_chieu_a_17:
                    infor34['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_17:
                    infor34['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_17:
                    infor34['dt10'] = u'③'
                else:
                    infor34['dt10'] = ''

                infor34['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_17)
                infor34['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_17.ten_giangvien_phaply)
                table_daotaosau.append(infor34)
                count_item += 2
            else:
                if daotaosau.ngaynghi_17:
                    infor33['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_17)
                    infor33['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_17)
                else:
                    infor33['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_17.noidungdaotao)
                    infor33['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_17.noidungdaotao)
                infor33['dt7'] = u'◯' if daotaosau.coso_sang_17 else ''
                infor33['dt9'] = u'◯' if daotaosau.coso_chieu_17 else ''
                if daotaosau.cosao_daotao_a_17:
                    infor33['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_17:
                    infor33['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_17:
                    infor33['dt10'] = u'③'
                else:
                    infor33['dt10'] = ''

                infor33['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_17)
                infor33['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_17.ten_giangvien_phaply)
                table_daotaosau.append(infor33)
                count_item += 1

            # Dòng 18
            infor35 = {}
            infor35['dt5'] = format_hoso.kiemtra(daotaosau.thang_18)
            infor35['dt13'] = format_hoso.kiemtra(daotaosau.ngay_18)
            infor35['dt14'] = format_hoso.kiemtra(daotaosau.thu_18)
            if daotaosau.cangay_18 == True:
                infor35['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_18.noidungdaotao)
                infor35['dt7'] = u'◯' if daotaosau.coso_sang_18 else ''
                infor35['dt8'] = ''
                infor35['dt9'] = ''
                if daotaosau.cosao_daotao_a_18:
                    infor35['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_18:
                    infor35['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_18:
                    infor35['dt10'] = u'③'
                else:
                    infor35['dt10'] = ''
                infor35['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_18)
                infor35['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_18.ten_giangvien_phaply)
                table_daotaosau.append(infor35)

                infor36 = {}
                infor36['dt5'] = ''
                infor36['dt13'] = ''
                infor36['dt14'] = ''
                infor36['dt6'] = ''
                infor36['dt7'] = ''
                infor36['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_18.noidungdaotao)
                infor36['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_18 else ''

                if daotaosau.coso_chieu_chieu_a_18:
                    infor36['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_18:
                    infor36['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_18:
                    infor36['dt10'] = u'③'
                else:
                    infor36['dt10'] = ''

                infor36['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_18)
                infor36['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_18.ten_giangvien_phaply)
                table_daotaosau.append(infor36)
                count_item += 2
            else:
                if daotaosau.ngaynghi_18:
                    infor35['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_18)
                    infor35['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_18)
                else:
                    infor35['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_18.noidungdaotao)
                    infor35['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_18.noidungdaotao)
                infor35['dt7'] = u'◯' if daotaosau.coso_sang_18 else ''
                infor35['dt9'] = u'◯' if daotaosau.coso_chieu_18 else ''
                if daotaosau.cosao_daotao_a_18:
                    infor35['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_18:
                    infor35['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_18:
                    infor35['dt10'] = u'③'
                else:
                    infor35['dt10'] = ''

                infor35['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_18)
                infor35['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_18.ten_giangvien_phaply)
                table_daotaosau.append(infor35)
                count_item += 1

            # Dòng 19
            infor37 = {}
            infor37['dt5'] = format_hoso.kiemtra(daotaosau.thang_19)
            infor37['dt13'] = format_hoso.kiemtra(daotaosau.ngay_19)
            infor37['dt14'] = format_hoso.kiemtra(daotaosau.thu_19)
            if daotaosau.cangay_19 == True:
                infor37['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_19.noidungdaotao)
                infor37['dt7'] = u'◯' if daotaosau.coso_sang_19 else ''
                infor37['dt8'] = ''
                infor37['dt9'] = ''
                if daotaosau.cosao_daotao_a_19:
                    infor37['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_19:
                    infor37['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_19:
                    infor37['dt10'] = u'③'
                else:
                    infor37['dt10'] = ''
                infor37['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_19)
                infor37['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_19.ten_giangvien_phaply)
                table_daotaosau.append(infor37)

                infor38 = {}
                infor38['dt5'] = ''
                infor38['dt13'] = ''
                infor38['dt14'] = ''
                infor38['dt6'] = ''
                infor38['dt7'] = ''
                infor38['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_19.noidungdaotao)
                infor38['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_19 else ''

                if daotaosau.coso_chieu_chieu_a_19:
                    infor38['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_19:
                    infor38['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_19:
                    infor38['dt10'] = u'③'
                else:
                    infor38['dt10'] = ''

                infor38['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_19)
                infor38['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_19.ten_giangvien_phaply)
                table_daotaosau.append(infor38)
                count_item += 2
            else:
                if daotaosau.ngaynghi_19:
                    infor37['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_19)
                    infor37['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_19)
                else:
                    infor37['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_19.noidungdaotao)
                    infor37['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_19.noidungdaotao)
                infor37['dt7'] = u'◯' if daotaosau.coso_sang_19 else ''
                infor37['dt9'] = u'◯' if daotaosau.coso_chieu_19 else ''
                if daotaosau.cosao_daotao_a_19:
                    infor37['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_19:
                    infor37['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_19:
                    infor37['dt10'] = u'③'
                else:
                    infor37['dt10'] = ''

                infor37['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_19)
                infor37['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_19.ten_giangvien_phaply)
                table_daotaosau.append(infor37)
                count_item += 1

            # Dòng 20
            infor39 = {}
            infor39['dt5'] = format_hoso.kiemtra(daotaosau.thang_20)
            infor39['dt13'] = format_hoso.kiemtra(daotaosau.ngay_20)
            infor39['dt14'] = format_hoso.kiemtra(daotaosau.thu_20)
            if daotaosau.cangay_20 == True:
                infor39['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_20.noidungdaotao)
                infor39['dt7'] = u'◯' if daotaosau.coso_sang_20 else ''
                infor39['dt8'] = ''
                infor39['dt9'] = ''
                if daotaosau.cosao_daotao_a_20:
                    infor39['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_20:
                    infor39['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_20:
                    infor39['dt10'] = u'③'
                else:
                    infor39['dt10'] = ''
                infor39['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_20)
                infor39['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_20.ten_giangvien_phaply)
                table_daotaosau.append(infor39)

                infor40 = {}
                infor40['dt5'] = ''
                infor40['dt13'] = ''
                infor40['dt14'] = ''
                infor40['dt6'] = ''
                infor40['dt7'] = ''
                infor40['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_20.noidungdaotao)
                infor40['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_20 else ''

                if daotaosau.coso_chieu_chieu_a_20:
                    infor40['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_20:
                    infor40['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_20:
                    infor40['dt10'] = u'③'
                else:
                    infor40['dt10'] = ''

                infor40['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_20)
                infor40['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_20.ten_giangvien_phaply)
                table_daotaosau.append(infor40)
                count_item += 2
            else:
                if daotaosau.ngaynghi_20:
                    infor39['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_20)
                    infor39['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_20)
                else:
                    infor39['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_20.noidungdaotao)
                    infor39['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_20.noidungdaotao)
                infor39['dt7'] = u'◯' if daotaosau.coso_sang_20 else ''
                infor39['dt9'] = u'◯' if daotaosau.coso_chieu_20 else ''
                if daotaosau.cosao_daotao_a_20:
                    infor39['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_20:
                    infor39['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_20:
                    infor39['dt10'] = u'③'
                else:
                    infor39['dt10'] = ''

                infor39['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_20)
                infor39['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_20.ten_giangvien_phaply)
                table_daotaosau.append(infor39)
                count_item += 1

            # Dòng 21
            infor41 = {}
            infor41['dt5'] = format_hoso.kiemtra(daotaosau.thang_21)
            infor41['dt13'] = format_hoso.kiemtra(daotaosau.ngay_21)
            infor41['dt14'] = format_hoso.kiemtra(daotaosau.thu_21)
            if daotaosau.cangay_21 == True:
                infor41['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_21.noidungdaotao)
                infor41['dt7'] = u'◯' if daotaosau.coso_sang_21 else ''
                infor41['dt8'] = ''
                infor41['dt9'] = ''
                if daotaosau.cosao_daotao_a_21:
                    infor41['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_21:
                    infor41['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_21:
                    infor41['dt10'] = u'③'
                else:
                    infor41['dt10'] = ''
                infor41['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_21)
                infor41['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_21.ten_giangvien_phaply)
                table_daotaosau.append(infor41)

                infor42 = {}
                infor42['dt5'] = ''
                infor42['dt13'] = ''
                infor42['dt14'] = ''
                infor42['dt6'] = ''
                infor42['dt7'] = ''
                infor42['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_21.noidungdaotao)
                infor42['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_21 else ''

                if daotaosau.coso_chieu_chieu_a_21:
                    infor42['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_21:
                    infor42['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_21:
                    infor42['dt10'] = u'③'
                else:
                    infor42['dt10'] = ''

                infor42['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_21)
                infor42['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_21.ten_giangvien_phaply)
                table_daotaosau.append(infor42)
                count_item += 2
            else:
                if daotaosau.ngaynghi_21:
                    infor41['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_21)
                    infor41['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_21)
                else:
                    infor41['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_21.noidungdaotao)
                    infor41['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_21.noidungdaotao)
                infor41['dt7'] = u'◯' if daotaosau.coso_sang_21 else ''
                infor41['dt9'] = u'◯' if daotaosau.coso_chieu_21 else ''
                if daotaosau.cosao_daotao_a_21:
                    infor41['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_21:
                    infor41['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_21:
                    infor41['dt10'] = u'③'
                else:
                    infor41['dt10'] = ''

                infor41['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_21)
                infor41['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_21.ten_giangvien_phaply)
                table_daotaosau.append(infor41)
                count_item += 1

            # Dòng 22

            infor43 = {}
            infor43['dt5'] = format_hoso.kiemtra(daotaosau.thang_22)
            infor43['dt13'] = format_hoso.kiemtra(daotaosau.ngay_22)
            infor43['dt14'] = format_hoso.kiemtra(daotaosau.thu_22)
            if daotaosau.cangay_22 == True:
                infor43['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_22.noidungdaotao)
                infor43['dt7'] = u'◯' if daotaosau.coso_sang_22 else ''
                infor43['dt8'] = ''
                infor43['dt9'] = ''
                if daotaosau.cosao_daotao_a_22:
                    infor43['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_22:
                    infor43['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_22:
                    infor43['dt10'] = u'③'
                else:
                    infor43['dt10'] = ''
                infor43['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_22)
                infor43['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_22.ten_giangvien_phaply)
                table_daotaosau.append(infor43)

                infor44 = {}
                infor44['dt5'] = ''
                infor44['dt13'] = ''
                infor44['dt14'] = ''
                infor44['dt6'] = ''
                infor44['dt7'] = ''
                infor44['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_22.noidungdaotao)
                infor44['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_22 else ''

                if daotaosau.coso_chieu_chieu_a_22:
                    infor44['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_22:
                    infor44['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_22:
                    infor44['dt10'] = u'③'
                else:
                    infor44['dt10'] = ''

                infor44['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_22)
                infor44['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_22.ten_giangvien_phaply)
                table_daotaosau.append(infor44)
                count_item += 2
            else:
                if daotaosau.ngaynghi_22:
                    infor43['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_22)
                    infor43['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_22)
                else:
                    infor43['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_22.noidungdaotao)
                    infor43['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_22.noidungdaotao)
                infor43['dt7'] = u'◯' if daotaosau.coso_sang_22 else ''
                infor43['dt9'] = u'◯' if daotaosau.coso_chieu_22 else ''
                if daotaosau.cosao_daotao_a_22:
                    infor43['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_22:
                    infor43['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_22:
                    infor43['dt10'] = u'③'
                else:
                    infor43['dt10'] = ''

                infor43['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_22)
                infor43['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_22.ten_giangvien_phaply)
                table_daotaosau.append(infor43)
                count_item += 1

            # Dòng 23
            infor45 = {}
            infor45['dt5'] = format_hoso.kiemtra(daotaosau.thang_23)
            infor45['dt13'] = format_hoso.kiemtra(daotaosau.ngay_23)
            infor45['dt14'] = format_hoso.kiemtra(daotaosau.thu_23)
            if daotaosau.cangay_23 == True:
                infor45['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_23.noidungdaotao)
                infor45['dt7'] = u'◯' if daotaosau.coso_sang_23 else ''
                infor45['dt8'] = ''
                infor45['dt9'] = ''
                if daotaosau.cosao_daotao_a_23:
                    infor45['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_23:
                    infor45['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_23:
                    infor45['dt10'] = u'③'
                else:
                    infor45['dt10'] = ''
                infor45['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_23)
                infor45['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_23.ten_giangvien_phaply)
                table_daotaosau.append(infor45)

                infor46 = {}
                infor46['dt5'] = ''
                infor46['dt13'] = ''
                infor46['dt14'] = ''
                infor46['dt6'] = ''
                infor46['dt7'] = ''
                infor46['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_23.noidungdaotao)
                infor46['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_23 else ''

                if daotaosau.coso_chieu_chieu_a_23:
                    infor46['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_23:
                    infor46['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_23:
                    infor46['dt10'] = u'③'
                else:
                    infor46['dt10'] = ''

                infor46['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_23)
                infor46['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_23.ten_giangvien_phaply)
                table_daotaosau.append(infor46)
                count_item += 2
            else:
                if daotaosau.ngaynghi_23:
                    infor45['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_23)
                    infor45['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_23)
                else:
                    infor45['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_23.noidungdaotao)
                    infor45['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_23.noidungdaotao)
                infor45['dt7'] = u'◯' if daotaosau.coso_sang_23 else ''
                infor45['dt9'] = u'◯' if daotaosau.coso_chieu_23 else ''
                if daotaosau.cosao_daotao_a_23:
                    infor45['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_23:
                    infor45['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_23:
                    infor45['dt10'] = u'③'
                else:
                    infor45['dt10'] = ''

                infor45['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_23)
                infor45['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_23.ten_giangvien_phaply)
                table_daotaosau.append(infor45)
                count_item += 1

            # Dòng 24
            infor47 = {}
            infor47['dt5'] = format_hoso.kiemtra(daotaosau.thang_24)
            infor47['dt13'] = format_hoso.kiemtra(daotaosau.ngay_24)
            infor47['dt14'] = format_hoso.kiemtra(daotaosau.thu_24)
            if daotaosau.cangay_24 == True:
                infor47['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_24.noidungdaotao)
                infor47['dt7'] = u'◯' if daotaosau.coso_sang_24 else ''
                infor47['dt8'] = ''
                infor47['dt9'] = ''
                if daotaosau.cosao_daotao_a_24:
                    infor47['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_24:
                    infor47['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_24:
                    infor47['dt10'] = u'③'
                else:
                    infor47['dt10'] = ''
                infor47['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_24)
                infor47['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_24.ten_giangvien_phaply)
                table_daotaosau.append(infor47)

                infor48 = {}
                infor48['dt5'] = ''
                infor48['dt13'] = ''
                infor48['dt14'] = ''
                infor48['dt6'] = ''
                infor48['dt7'] = ''
                infor48['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_24.noidungdaotao)
                infor48['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_24 else ''

                if daotaosau.coso_chieu_chieu_a_24:
                    infor48['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_24:
                    infor48['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_24:
                    infor48['dt10'] = u'③'
                else:
                    infor48['dt10'] = ''

                infor48['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_24)
                infor48['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_24.ten_giangvien_phaply)
                table_daotaosau.append(infor48)
                count_item += 2
            else:
                if daotaosau.ngaynghi_24:
                    infor47['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_24)
                    infor47['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_24)
                else:
                    infor47['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_24.noidungdaotao)
                    infor47['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_24.noidungdaotao)
                infor47['dt7'] = u'◯' if daotaosau.coso_sang_24 else ''
                infor47['dt9'] = u'◯' if daotaosau.coso_chieu_24 else ''
                if daotaosau.cosao_daotao_a_24:
                    infor47['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_24:
                    infor47['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_24:
                    infor47['dt10'] = u'③'
                else:
                    infor47['dt10'] = ''

                infor47['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_24)
                infor47['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_24.ten_giangvien_phaply)
                table_daotaosau.append(infor47)
                count_item += 1

            # Dòng 25
            infor49 = {}
            infor49['dt5'] = format_hoso.kiemtra(daotaosau.thang_25)
            infor49['dt13'] = format_hoso.kiemtra(daotaosau.ngay_25)
            infor49['dt14'] = format_hoso.kiemtra(daotaosau.thu_25)
            if daotaosau.cangay_25 == True:
                infor49['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_25.noidungdaotao)
                infor49['dt7'] = u'◯' if daotaosau.coso_sang_25 else ''
                infor49['dt8'] = ''
                infor49['dt9'] = ''
                if daotaosau.cosao_daotao_a_25:
                    infor49['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_25:
                    infor49['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_25:
                    infor49['dt10'] = u'③'
                else:
                    infor49['dt10'] = ''
                infor49['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_25)
                infor49['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_25.ten_giangvien_phaply)
                table_daotaosau.append(infor49)

                infor50 = {}
                infor50['dt5'] = ''
                infor50['dt13'] = ''
                infor50['dt14'] = ''
                infor50['dt6'] = ''
                infor50['dt7'] = ''
                infor50['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_25.noidungdaotao)
                infor50['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_25 else ''

                if daotaosau.coso_chieu_chieu_a_25:
                    infor50['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_25:
                    infor50['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_25:
                    infor50['dt10'] = u'③'
                else:
                    infor50['dt10'] = ''

                infor50['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_25)
                infor50['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_25.ten_giangvien_phaply)
                table_daotaosau.append(infor50)
                count_item += 2
            else:
                if daotaosau.ngaynghi_25:
                    infor49['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_25)
                    infor49['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_25)
                else:
                    infor49['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_25.noidungdaotao)
                    infor49['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_25.noidungdaotao)
                infor49['dt7'] = u'◯' if daotaosau.coso_sang_25 else ''
                infor49['dt9'] = u'◯' if daotaosau.coso_chieu_25 else ''
                if daotaosau.cosao_daotao_a_25:
                    infor49['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_25:
                    infor49['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_25:
                    infor49['dt10'] = u'③'
                else:
                    infor49['dt10'] = ''

                infor49['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_25)
                infor49['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_25.ten_giangvien_phaply)
                table_daotaosau.append(infor49)
                count_item += 1

            # Dòng 26
            infor51 = {}
            infor51['dt5'] = format_hoso.kiemtra(daotaosau.thang_26)
            infor51['dt13'] = format_hoso.kiemtra(daotaosau.ngay_26)
            infor51['dt14'] = format_hoso.kiemtra(daotaosau.thu_26)
            if daotaosau.cangay_26 == True:
                infor51['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_26.noidungdaotao)
                infor51['dt7'] = u'◯' if daotaosau.coso_sang_26 else ''
                infor51['dt8'] = ''
                infor51['dt9'] = ''
                if daotaosau.cosao_daotao_a_26:
                    infor51['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_26:
                    infor51['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_26:
                    infor51['dt10'] = u'③'
                else:
                    infor51['dt10'] = ''
                infor51['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_26)
                infor51['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_26.ten_giangvien_phaply)
                table_daotaosau.append(infor51)

                infor52 = {}
                infor52['dt5'] = ''
                infor52['dt13'] = ''
                infor52['dt14'] = ''
                infor52['dt6'] = ''
                infor52['dt7'] = ''
                infor52['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_26.noidungdaotao)
                infor52['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_26 else ''

                if daotaosau.coso_chieu_chieu_a_26:
                    infor52['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_26:
                    infor52['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_26:
                    infor52['dt10'] = u'③'
                else:
                    infor52['dt10'] = ''

                infor52['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_26)
                infor52['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_26.ten_giangvien_phaply)
                table_daotaosau.append(infor52)
                count_item += 2
            else:
                if daotaosau.ngaynghi_26:
                    infor51['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_26)
                    infor51['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_26)
                else:
                    infor51['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_26.noidungdaotao)
                    infor51['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_26.noidungdaotao)
                infor51['dt7'] = u'◯' if daotaosau.coso_sang_26 else ''
                infor51['dt9'] = u'◯' if daotaosau.coso_chieu_26 else ''
                if daotaosau.cosao_daotao_a_26:
                    infor51['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_26:
                    infor51['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_26:
                    infor51['dt10'] = u'③'
                else:
                    infor51['dt10'] = ''

                infor51['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_26)
                infor51['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_26.ten_giangvien_phaply)
                table_daotaosau.append(infor51)
                count_item += 1

            # Dòng 27
            infor53 = {}
            infor53['dt5'] = format_hoso.kiemtra(daotaosau.thang_27)
            infor53['dt13'] = format_hoso.kiemtra(daotaosau.ngay_27)
            infor53['dt14'] = format_hoso.kiemtra(daotaosau.thu_27)
            if daotaosau.cangay_27 == True:
                infor53['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_27.noidungdaotao)
                infor53['dt7'] = u'◯' if daotaosau.coso_sang_27 else ''
                infor53['dt8'] = ''
                infor53['dt9'] = ''
                if daotaosau.cosao_daotao_a_27:
                    infor53['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_27:
                    infor53['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_27:
                    infor53['dt10'] = u'③'
                else:
                    infor53['dt10'] = ''
                infor53['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_27)
                infor53['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_27.ten_giangvien_phaply)
                table_daotaosau.append(infor53)

                infor54 = {}
                infor54['dt5'] = ''
                infor54['dt13'] = ''
                infor54['dt14'] = ''
                infor54['dt6'] = ''
                infor54['dt7'] = ''
                infor54['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_27.noidungdaotao)
                infor54['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_27 else ''

                if daotaosau.coso_chieu_chieu_a_27:
                    infor54['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_27:
                    infor54['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_27:
                    infor54['dt10'] = u'③'
                else:
                    infor54['dt10'] = ''

                infor54['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_27)
                infor54['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_27.ten_giangvien_phaply)
                table_daotaosau.append(infor54)
                count_item += 2
            else:
                if daotaosau.ngaynghi_27:
                    infor53['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_27)
                    infor53['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_27)
                else:
                    infor53['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_27.noidungdaotao)
                    infor53['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_27.noidungdaotao)
                infor53['dt7'] = u'◯' if daotaosau.coso_sang_27 else ''
                infor53['dt9'] = u'◯' if daotaosau.coso_chieu_27 else ''
                if daotaosau.cosao_daotao_a_27:
                    infor53['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_27:
                    infor53['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_27:
                    infor53['dt10'] = u'③'
                else:
                    infor53['dt10'] = ''

                infor53['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_27)
                infor53['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_27.ten_giangvien_phaply)
                table_daotaosau.append(infor53)
                count_item += 1

            # Dòng 28
            infor55 = {}
            infor55['dt5'] = format_hoso.kiemtra(daotaosau.thang_28)
            infor55['dt13'] = format_hoso.kiemtra(daotaosau.ngay_28)
            infor55['dt14'] = format_hoso.kiemtra(daotaosau.thu_28)
            if daotaosau.cangay_28 == True:
                infor55['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_28.noidungdaotao)
                infor55['dt7'] = u'◯' if daotaosau.coso_sang_28 else ''
                infor55['dt8'] = ''
                infor55['dt9'] = ''
                if daotaosau.cosao_daotao_a_28:
                    infor55['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_28:
                    infor55['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_28:
                    infor55['dt10'] = u'③'
                else:
                    infor55['dt10'] = ''
                infor55['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_28)
                infor55['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_28.ten_giangvien_phaply)
                table_daotaosau.append(infor55)

                infor56 = {}
                infor56['dt5'] = ''
                infor56['dt13'] = ''
                infor56['dt14'] = ''
                infor56['dt6'] = ''
                infor56['dt7'] = ''
                infor56['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_28.noidungdaotao)
                infor56['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_28 else ''

                if daotaosau.coso_chieu_chieu_a_28:
                    infor56['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_28:
                    infor56['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_28:
                    infor56['dt10'] = u'③'
                else:
                    infor56['dt10'] = ''

                infor56['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_28)
                infor56['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_28.ten_giangvien_phaply)
                table_daotaosau.append(infor56)
                count_item += 2
            else:
                if daotaosau.ngaynghi_28:
                    infor55['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_28)
                    infor55['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_28)
                else:
                    infor55['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_28.noidungdaotao)
                    infor55['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_28.noidungdaotao)
                infor55['dt7'] = u'◯' if daotaosau.coso_sang_28 else ''
                infor55['dt9'] = u'◯' if daotaosau.coso_chieu_28 else ''
                if daotaosau.cosao_daotao_a_28:
                    infor55['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_28:
                    infor55['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_28:
                    infor55['dt10'] = u'③'
                else:
                    infor55['dt10'] = ''

                infor55['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_28)
                infor55['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_28.ten_giangvien_phaply)
                table_daotaosau.append(infor55)
                count_item += 1

            # Dòng 29
            infor57 = {}
            infor57['dt5'] = format_hoso.kiemtra(daotaosau.thang_29)
            infor57['dt13'] = format_hoso.kiemtra(daotaosau.ngay_29)
            infor57['dt14'] = format_hoso.kiemtra(daotaosau.thu_29)
            if daotaosau.cangay_29 == True:
                infor57['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_29.noidungdaotao)
                infor57['dt7'] = u'◯' if daotaosau.coso_sang_29 else ''
                infor57['dt8'] = ''
                infor57['dt9'] = ''
                if daotaosau.cosao_daotao_a_29:
                    infor57['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_29:
                    infor57['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_29:
                    infor57['dt10'] = u'③'
                else:
                    infor57['dt10'] = ''
                infor57['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_29)
                infor57['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_29.ten_giangvien_phaply)
                table_daotaosau.append(infor57)

                infor58 = {}
                infor58['dt5'] = ''
                infor58['dt13'] = ''
                infor58['dt14'] = ''
                infor58['dt6'] = ''
                infor58['dt7'] = ''
                infor58['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_29.noidungdaotao)
                infor58['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_29 else ''

                if daotaosau.coso_chieu_chieu_a_29:
                    infor58['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_29:
                    infor58['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_29:
                    infor58['dt10'] = u'③'
                else:
                    infor58['dt10'] = ''

                infor58['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_29)
                infor58['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_29.ten_giangvien_phaply)
                table_daotaosau.append(infor58)
                count_item += 2
            else:
                if daotaosau.ngaynghi_29:
                    infor57['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_29)
                    infor57['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_29)
                else:
                    infor57['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_29.noidungdaotao)
                    infor57['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_29.noidungdaotao)
                infor57['dt7'] = u'◯' if daotaosau.coso_sang_29 else ''
                infor57['dt9'] = u'◯' if daotaosau.coso_chieu_29 else ''
                if daotaosau.cosao_daotao_a_29:
                    infor57['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_29:
                    infor57['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_29:
                    infor57['dt10'] = u'③'
                else:
                    infor57['dt10'] = ''

                infor57['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_29)
                infor57['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_29.ten_giangvien_phaply)
                table_daotaosau.append(infor57)
                count_item += 1

            # Dòng 30
            infor59 = {}
            infor59['dt5'] = format_hoso.kiemtra(daotaosau.thang_30)
            infor59['dt13'] = format_hoso.kiemtra(daotaosau.ngay_30)
            infor59['dt14'] = format_hoso.kiemtra(daotaosau.thu_30)
            if daotaosau.cangay_30 == True:
                infor59['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_30.noidungdaotao)
                infor59['dt7'] = u'◯' if daotaosau.coso_sang_30 else ''
                infor59['dt8'] = ''
                infor59['dt9'] = ''
                if daotaosau.cosao_daotao_a_30:
                    infor59['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_30:
                    infor59['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_30:
                    infor59['dt10'] = u'③'
                else:
                    infor59['dt10'] = ''
                infor59['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_30)
                infor59['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_30.ten_giangvien_phaply)
                table_daotaosau.append(infor59)

                infor60 = {}
                infor60['dt5'] = ''
                infor60['dt13'] = ''
                infor60['dt14'] = ''
                infor60['dt6'] = ''
                infor60['dt7'] = ''
                infor60['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_30.noidungdaotao)
                infor60['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_30 else ''

                if daotaosau.coso_chieu_chieu_a_30:
                    infor60['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_30:
                    infor60['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_30:
                    infor60['dt10'] = u'③'
                else:
                    infor60['dt10'] = ''

                infor60['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_30)
                infor60['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_30.ten_giangvien_phaply)
                table_daotaosau.append(infor60)
                count_item += 2
            else:
                if daotaosau.ngaynghi_30:
                    infor59['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_30)
                    infor59['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_30)
                else:
                    infor59['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_30.noidungdaotao)
                    infor59['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_30.noidungdaotao)
                infor59['dt7'] = u'◯' if daotaosau.coso_sang_30 else ''
                infor59['dt9'] = u'◯' if daotaosau.coso_chieu_30 else ''
                if daotaosau.cosao_daotao_a_30:
                    infor59['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_30:
                    infor59['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_30:
                    infor59['dt10'] = u'③'
                else:
                    infor59['dt10'] = ''

                infor59['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_30)
                infor59['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_30.ten_giangvien_phaply)
                table_daotaosau.append(infor59)
                count_item += 1

            # Dòng 31
            infor61 = {}
            infor61['dt5'] = format_hoso.kiemtra(daotaosau.thang_31)
            infor61['dt13'] = format_hoso.kiemtra(daotaosau.ngay_31)
            infor61['dt14'] = format_hoso.kiemtra(daotaosau.thu_31)
            if daotaosau.cangay_31 == True:
                infor61['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_31.noidungdaotao)
                infor61['dt7'] = u'◯' if daotaosau.coso_sang_31 else ''
                infor61['dt8'] = ''
                infor61['dt9'] = ''
                if daotaosau.cosao_daotao_a_31:
                    infor61['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_31:
                    infor61['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_31:
                    infor61['dt10'] = u'③'
                else:
                    infor61['dt10'] = ''
                infor61['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_31)
                infor61['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_31.ten_giangvien_phaply)
                table_daotaosau.append(infor61)

                infor62 = {}
                infor62['dt5'] = ''
                infor62['dt13'] = ''
                infor62['dt14'] = ''
                infor62['dt6'] = ''
                infor62['dt7'] = ''
                infor62['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_31.noidungdaotao)
                infor62['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_31 else ''

                if daotaosau.coso_chieu_chieu_a_31:
                    infor62['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_31:
                    infor62['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_31:
                    infor62['dt10'] = u'③'
                else:
                    infor62['dt10'] = ''

                infor62['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_31)
                infor62['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_31.ten_giangvien_phaply)
                table_daotaosau.append(infor62)
                count_item += 2
            else:
                if daotaosau.ngaynghi_31:
                    infor61['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_31)
                    infor61['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_31)
                else:
                    infor61['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_31.noidungdaotao)
                    infor61['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_31.noidungdaotao)
                infor61['dt7'] = u'◯' if daotaosau.coso_sang_31 else ''
                infor61['dt9'] = u'◯' if daotaosau.coso_chieu_31 else ''
                if daotaosau.cosao_daotao_a_31:
                    infor61['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_31:
                    infor61['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_31:
                    infor61['dt10'] = u'③'
                else:
                    infor61['dt10'] = ''

                infor61['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_31)
                infor61['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_31.ten_giangvien_phaply)
                table_daotaosau.append(infor61)
                count_item += 1

            # Dòng 32
            infor63 = {}
            infor63['dt5'] = format_hoso.kiemtra(daotaosau.thang_32)
            infor63['dt13'] = format_hoso.kiemtra(daotaosau.ngay_32)
            infor63['dt14'] = format_hoso.kiemtra(daotaosau.thu_32)
            if daotaosau.cangay_32 == True:
                infor63['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_32.noidungdaotao)
                infor63['dt7'] = u'◯' if daotaosau.coso_sang_32 else ''
                infor63['dt8'] = ''
                infor63['dt9'] = ''
                if daotaosau.cosao_daotao_a_32:
                    infor63['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_32:
                    infor63['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_32:
                    infor63['dt10'] = u'③'
                else:
                    infor63['dt10'] = ''
                infor63['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_32)
                infor63['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_32.ten_giangvien_phaply)
                table_daotaosau.append(infor63)

                infor64 = {}
                infor64['dt5'] = ''
                infor64['dt13'] = ''
                infor64['dt14'] = ''
                infor64['dt6'] = ''
                infor64['dt7'] = ''
                infor64['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_32.noidungdaotao)
                infor64['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_32 else ''

                if daotaosau.coso_chieu_chieu_a_32:
                    infor64['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_32:
                    infor64['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_32:
                    infor64['dt10'] = u'③'
                else:
                    infor64['dt10'] = ''

                infor64['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_32)
                infor64['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_32.ten_giangvien_phaply)
                table_daotaosau.append(infor64)
                count_item += 2
            else:
                if daotaosau.ngaynghi_32:
                    infor63['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_32)
                    infor63['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_32)
                else:
                    infor63['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_32.noidungdaotao)
                    infor63['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_32.noidungdaotao)
                infor63['dt7'] = u'◯' if daotaosau.coso_sang_32 else ''
                infor63['dt9'] = u'◯' if daotaosau.coso_chieu_32 else ''
                if daotaosau.cosao_daotao_a_32:
                    infor63['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_32:
                    infor63['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_32:
                    infor63['dt10'] = u'③'
                else:
                    infor63['dt10'] = ''

                infor63['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_32)
                infor63['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_32.ten_giangvien_phaply)
                table_daotaosau.append(infor63)
                count_item += 1

            # Dòng 33
            infor65 = {}
            infor65['dt5'] = format_hoso.kiemtra(daotaosau.thang_33)
            infor65['dt13'] = format_hoso.kiemtra(daotaosau.ngay_33)
            infor65['dt14'] = format_hoso.kiemtra(daotaosau.thu_33)
            if daotaosau.cangay_33 == True:
                infor65['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_33.noidungdaotao)
                infor65['dt7'] = u'◯' if daotaosau.coso_sang_33 else ''
                infor65['dt8'] = ''
                infor65['dt9'] = ''
                if daotaosau.cosao_daotao_a_33:
                    infor65['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_33:
                    infor65['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_33:
                    infor65['dt10'] = u'③'
                else:
                    infor65['dt10'] = ''
                infor65['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_33)
                infor65['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_33.ten_giangvien_phaply)
                table_daotaosau.append(infor65)

                infor66 = {}
                infor66['dt5'] = ''
                infor66['dt13'] = ''
                infor66['dt14'] = ''
                infor66['dt6'] = ''
                infor66['dt7'] = ''
                infor66['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_33.noidungdaotao)
                infor66['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_33 else ''

                if daotaosau.coso_chieu_chieu_a_33:
                    infor66['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_33:
                    infor66['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_33:
                    infor66['dt10'] = u'③'
                else:
                    infor66['dt10'] = ''

                infor66['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_33)
                infor66['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_33.ten_giangvien_phaply)
                table_daotaosau.append(infor66)
                count_item += 2
            else:
                if daotaosau.ngaynghi_33:
                    infor65['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_33)
                    infor65['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_33)
                else:
                    infor65['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_33.noidungdaotao)
                    infor65['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_33.noidungdaotao)
                infor65['dt7'] = u'◯' if daotaosau.coso_sang_33 else ''
                infor65['dt9'] = u'◯' if daotaosau.coso_chieu_33 else ''
                if daotaosau.cosao_daotao_a_33:
                    infor65['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_33:
                    infor65['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_33:
                    infor65['dt10'] = u'③'
                else:
                    infor65['dt10'] = ''

                infor65['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_33)
                infor65['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_33.ten_giangvien_phaply)
                table_daotaosau.append(infor65)
                count_item += 1

            # Dòng 34
            infor67 = {}
            infor67['dt5'] = format_hoso.kiemtra(daotaosau.thang_34)
            infor67['dt13'] = format_hoso.kiemtra(daotaosau.ngay_34)
            infor67['dt14'] = format_hoso.kiemtra(daotaosau.thu_34)
            if daotaosau.cangay_34 == True:
                infor67['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_34.noidungdaotao)
                infor67['dt7'] = u'◯' if daotaosau.coso_sang_34 else ''
                infor67['dt8'] = ''
                infor67['dt9'] = ''
                if daotaosau.cosao_daotao_a_34:
                    infor67['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_34:
                    infor67['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_34:
                    infor67['dt10'] = u'③'
                else:
                    infor67['dt10'] = ''
                infor67['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_34)
                infor67['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_34.ten_giangvien_phaply)
                table_daotaosau.append(infor67)

                infor68 = {}
                infor68['dt5'] = ''
                infor68['dt13'] = ''
                infor68['dt14'] = ''
                infor68['dt6'] = ''
                infor68['dt7'] = ''
                infor68['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_34.noidungdaotao)
                infor68['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_34 else ''

                if daotaosau.coso_chieu_chieu_a_34:
                    infor68['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_34:
                    infor68['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_34:
                    infor68['dt10'] = u'③'
                else:
                    infor68['dt10'] = ''

                infor68['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_34)
                infor68['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_34.ten_giangvien_phaply)
                table_daotaosau.append(infor68)
                count_item += 2
            else:
                if daotaosau.ngaynghi_34:
                    infor67['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_34)
                    infor67['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_34)
                else:
                    infor67['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_34.noidungdaotao)
                    infor67['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_34.noidungdaotao)
                infor67['dt7'] = u'◯' if daotaosau.coso_sang_34 else ''
                infor67['dt9'] = u'◯' if daotaosau.coso_chieu_34 else ''
                if daotaosau.cosao_daotao_a_34:
                    infor67['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_34:
                    infor67['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_34:
                    infor67['dt10'] = u'③'
                else:
                    infor67['dt10'] = ''

                infor67['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_34)
                infor67['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_34.ten_giangvien_phaply)
                table_daotaosau.append(infor67)
                count_item += 1

            # Dòng 35
            infor69 = {}
            infor69['dt5'] = format_hoso.kiemtra(daotaosau.thang_35)
            infor69['dt13'] = format_hoso.kiemtra(daotaosau.ngay_35)
            infor69['dt14'] = format_hoso.kiemtra(daotaosau.thu_35)
            if daotaosau.cangay_35 == True:
                infor69['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_35.noidungdaotao)
                infor69['dt7'] = u'◯' if daotaosau.coso_sang_35 else ''
                infor69['dt8'] = ''
                infor69['dt9'] = ''
                if daotaosau.cosao_daotao_a_35:
                    infor69['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_35:
                    infor69['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_35:
                    infor69['dt10'] = u'③'
                else:
                    infor69['dt10'] = ''
                infor69['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_35)
                infor69['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_35.ten_giangvien_phaply)
                table_daotaosau.append(infor69)

                infor70 = {}
                infor70['dt5'] = ''
                infor70['dt13'] = ''
                infor70['dt14'] = ''
                infor70['dt6'] = ''
                infor70['dt7'] = ''
                infor70['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_35.noidungdaotao)
                infor70['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_35 else ''

                if daotaosau.coso_chieu_chieu_a_35:
                    infor70['dt10'] = u'①'
                elif daotaosau.coso_chieu_chieu_b_35:
                    infor70['dt10'] = u'②'
                elif daotaosau.coso_chieu_chieu_c_35:
                    infor70['dt10'] = u'③'
                else:
                    infor70['dt10'] = ''

                infor70['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_35)
                infor70['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_35.ten_giangvien_phaply)
                table_daotaosau.append(infor70)
                count_item += 2
            else:
                if daotaosau.ngaynghi_35:
                    infor69['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_35)
                    infor69['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_35)
                else:
                    infor69['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_35.noidungdaotao)
                    infor69['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_35.noidungdaotao)
                infor69['dt7'] = u'◯' if daotaosau.coso_sang_35 else ''
                infor69['dt9'] = u'◯' if daotaosau.coso_chieu_35 else ''
                if daotaosau.cosao_daotao_a_35:
                    infor69['dt10'] = u'①'
                elif daotaosau.cosao_daotao_b_35:
                    infor69['dt10'] = u'②'
                elif daotaosau.cosao_daotao_c_35:
                    infor69['dt10'] = u'③'
                else:
                    infor69['dt10'] = ''

                infor69['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_35)
                infor69['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_35.ten_giangvien_phaply)
                table_daotaosau.append(infor69)
                count_item += 1

            for i in range(count_item, 62):
                infor = {}
                infor['dt5'] = ''
                infor['dt13'] = ''
                infor['dt14'] = ''
                table_daotaosau.append(infor)

            context['tbl_daotaosau'] = table_daotaosau

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_4(self, ma_thuctapsinh, document):
        print('4')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_4")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            giaidoan = self.env['giaidoan.giaidoan'].search([('ten_giaidoan', '=', '１号')], limit=1)
            donhang_congviec = self.env['chitiet.chitiet'].search(
                [('donhang_chitiet', '=', document.donhang_hoso.id), ('giaidoan', '=', giaidoan.id)], limit=1)

            for chinnhanh_chitiet in donhang_congviec.chinnhanh_chitiet:
                context['hhjp_0041_1'] = format_hoso.kiemtra(chinnhanh_chitiet.ten_chinhanh_han)
                context['hhjp_0042_1'] = format_hoso.kiemtra(chinnhanh_chitiet.diachi_chinhanh)
            for chinnhanh_chitiet_2 in donhang_congviec.chinnhanh_chitiet_2:
                context['hhjp_0041_2'] = format_hoso.kiemtra(chinnhanh_chitiet_2.ten_chinhanh_han)
                context['hhjp_0042_2'] = format_hoso.kiemtra(chinnhanh_chitiet_2.diachi_chinhanh)
            for chinnhanh_chitiet_3 in donhang_congviec.chinnhanh_chitiet_3:
                context['hhjp_0041_3'] = format_hoso.kiemtra(chinnhanh_chitiet_3.ten_chinhanh_han)
                context['hhjp_0042_3'] = format_hoso.kiemtra(chinnhanh_chitiet_3.diachi_chinhanh)

            context['hhjp_0313'] = format_hoso.convert_date(thuctapsinh.batdau_kehoach_namnhat)
            context['hhjp_0314'] = format_hoso.convert_date(thuctapsinh.ketthuc_kehoach_namnhat)
            context['hhjp_0127'] = Listing(
                donhang_congviec.nguyenluyen_chitiet) if donhang_congviec.nguyenluyen_chitiet else ''
            context['hhjp_0128'] = Listing(
                donhang_congviec.congcu_maymoc_chitiet) if donhang_congviec.congcu_maymoc_chitiet else ''
            context['hhjp_0129'] = Listing(donhang_congviec.sanpham_chitiet) if donhang_congviec.sanpham_chitiet else ''
            context['hhjp_0121'] = Listing(
                donhang_congviec.noidung_congviec_mot) if donhang_congviec.noidung_congviec_mot else ''

            context['hhjp_0121_15'] = ''
            context['hhjp_0121_14'] = ''
            context['hhjp_0121_13'] = ''
            context['hhjp_0130'] = ''

            for danhsach_nhanvien in document.xinghiep_hoso.danhsach_nhanvien:
                if danhsach_nhanvien.vaitro_six == True:
                    context['hhjp_0121_15'] = format_hoso.kiemtra(danhsach_nhanvien.ten_han_nhanvien)
                    context['hhjp_0121_14'] = format_hoso.kiemtra(danhsach_nhanvien.kinhnghiem_nhanvien_chidaothuctap)
                    context['hhjp_0121_13'] = format_hoso.kiemtra(danhsach_nhanvien.chucdanh_han_nhanvien)
                    context['hhjp_0130'] = format_hoso.kiemtra(danhsach_nhanvien.bangcap_nhanvien)

            context['hhjp_0121_ts'] = Listing(
                donhang_congviec.coso_congviec_mot) if donhang_congviec.coso_congviec_mot else ''
            context['hhjp_0121_t'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_mot)
            context['hhjp_0121_1'] = format_hoso.kiemtra(donhang_congviec.thangmot_thoigian_mot)
            context['hhjp_0121_2'] = format_hoso.kiemtra(donhang_congviec.thanghai_thoigian_mot)
            context['hhjp_0121_3'] = format_hoso.kiemtra(donhang_congviec.thangba_thoigian_mot)
            context['hhjp_0121_4'] = format_hoso.kiemtra(donhang_congviec.thangbon_thoigian_mot)
            context['hhjp_0121_5'] = format_hoso.kiemtra(donhang_congviec.thangnam_thoigian_mot)
            context['hhjp_0121_6'] = format_hoso.kiemtra(donhang_congviec.thangsau_thoigian_mot)
            context['hhjp_0121_7'] = format_hoso.kiemtra(donhang_congviec.thangbay_thoigian_mot)
            context['hhjp_0121_8'] = format_hoso.kiemtra(donhang_congviec.thangtam_thoigian_mot)
            context['hhjp_0121_9'] = format_hoso.kiemtra(donhang_congviec.thangchin_thoigian_mot)
            context['hhjp_0121_10'] = format_hoso.kiemtra(donhang_congviec.thangmuoi_thoigian_mot)
            context['hhjp_0121_11'] = format_hoso.kiemtra(donhang_congviec.thangmuoimot_thoigian_mot)
            context['hhjp_0121_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_mot)
            context['hhjp_0121_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_mot)

            context['hhjp_0122'] = Listing(
                donhang_congviec.noidung_congviec_hai) if donhang_congviec.noidung_congviec_hai else ''
            context['hhjp_0122_ts'] = format_hoso.kiemtra(donhang_congviec.coso_congviec_hai)
            context['hhjp_0122_t'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_hai)
            context['hhjp_0122_1'] = format_hoso.kiemtra(donhang_congviec.thangmot_thoigian_hai)
            context['hhjp_0122_2'] = format_hoso.kiemtra(donhang_congviec.thanghai_thoigian_hai)
            context['hhjp_0122_3'] = format_hoso.kiemtra(donhang_congviec.thangba_thoigian_hai)
            context['hhjp_0122_4'] = format_hoso.kiemtra(donhang_congviec.thangbon_thoigian_hai)
            context['hhjp_0122_5'] = format_hoso.kiemtra(donhang_congviec.thangnam_thoigian_hai)
            context['hhjp_0122_6'] = format_hoso.kiemtra(donhang_congviec.thangsau_thoigian_hai)
            context['hhjp_0122_7'] = format_hoso.kiemtra(donhang_congviec.thangbay_thoigian_hai)
            context['hhjp_0122_8'] = format_hoso.kiemtra(donhang_congviec.thangtam_thoigian_hai)
            context['hhjp_0122_9'] = format_hoso.kiemtra(donhang_congviec.thangchin_thoigian_hai)
            context['hhjp_0122_10'] = format_hoso.kiemtra(donhang_congviec.thangmuoi_thoigian_hai)
            context['hhjp_0122_11'] = format_hoso.kiemtra(donhang_congviec.thangmuoimot_thoigian_hai)
            context['hhjp_0122_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_hai)
            context['hhjp_0122_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_hai)

            context['hhjp_0123'] = Listing(
                donhang_congviec.noidung_congviec_ba) if donhang_congviec.noidung_congviec_ba else ''
            context['hhjp_0123_ts'] = format_hoso.kiemtra(donhang_congviec.coso_congviec_ba)
            context['hhjp_0123_t'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_ba)
            context['hhjp_0123_1'] = format_hoso.kiemtra(donhang_congviec.thangmot_thoigian_ba)
            context['hhjp_0123_2'] = format_hoso.kiemtra(donhang_congviec.thanghai_thoigian_ba)
            context['hhjp_0123_3'] = format_hoso.kiemtra(donhang_congviec.thangba_thoigian_ba)
            context['hhjp_0123_4'] = format_hoso.kiemtra(donhang_congviec.thangbon_thoigian_ba)
            context['hhjp_0123_5'] = format_hoso.kiemtra(donhang_congviec.thangnam_thoigian_ba)
            context['hhjp_0123_6'] = format_hoso.kiemtra(donhang_congviec.thangsau_thoigian_ba)
            context['hhjp_0123_7'] = format_hoso.kiemtra(donhang_congviec.thangbay_thoigian_ba)
            context['hhjp_0123_8'] = format_hoso.kiemtra(donhang_congviec.thangtam_thoigian_ba)
            context['hhjp_0123_9'] = format_hoso.kiemtra(donhang_congviec.thangchin_thoigian_ba)
            context['hhjp_0123_10'] = format_hoso.kiemtra(donhang_congviec.thangmuoi_thoigian_ba)
            context['hhjp_0123_11'] = format_hoso.kiemtra(donhang_congviec.thangmuoimot_thoigian_ba)
            context['hhjp_0123_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_ba)
            context['hhjp_0123_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_ba)

            context['hhjp_0124'] = Listing(
                donhang_congviec.noidung_congviec_bon) if donhang_congviec.noidung_congviec_bon else ''
            context['hhjp_0124_ts'] = format_hoso.kiemtra(donhang_congviec.coso_congviec_bon)
            context['hhjp_0124_t'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_bon)
            context['hhjp_0124_1'] = format_hoso.kiemtra(donhang_congviec.thangmot_thoigian_bon)
            context['hhjp_0124_2'] = format_hoso.kiemtra(donhang_congviec.thanghai_thoigian_bon)
            context['hhjp_0124_3'] = format_hoso.kiemtra(donhang_congviec.thangba_thoigian_bon)
            context['hhjp_0124_4'] = format_hoso.kiemtra(donhang_congviec.thangbon_thoigian_bon)
            context['hhjp_0124_5'] = format_hoso.kiemtra(donhang_congviec.thangnam_thoigian_bon)
            context['hhjp_0124_6'] = format_hoso.kiemtra(donhang_congviec.thangsau_thoigian_bon)
            context['hhjp_0124_7'] = format_hoso.kiemtra(donhang_congviec.thangbay_thoigian_bon)
            context['hhjp_0124_8'] = format_hoso.kiemtra(donhang_congviec.thangtam_thoigian_bon)
            context['hhjp_0124_9'] = format_hoso.kiemtra(donhang_congviec.thangchin_thoigian_bon)
            context['hhjp_0124_10'] = format_hoso.kiemtra(donhang_congviec.thangmuoi_thoigian_bon)
            context['hhjp_0124_11'] = format_hoso.kiemtra(donhang_congviec.thangmuoimot_thoigian_bon)
            context['hhjp_0124_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_bon)
            context['hhjp_0124_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_bon)

            context['hhjp_0125'] = Listing(
                donhang_congviec.noidung_congviec_nam) if donhang_congviec.noidung_congviec_nam else ''
            context['hhjp_0125_ts'] = format_hoso.kiemtra(donhang_congviec.coso_congviec_nam)
            context['hhjp_0125_t'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_nam)
            context['hhjp_0125_1'] = format_hoso.kiemtra(donhang_congviec.thangmot_thoigian_nam)
            context['hhjp_0125_2'] = format_hoso.kiemtra(donhang_congviec.thanghai_thoigian_nam)
            context['hhjp_0125_3'] = format_hoso.kiemtra(donhang_congviec.thangba_thoigian_nam)
            context['hhjp_0125_4'] = format_hoso.kiemtra(donhang_congviec.thangbon_thoigian_nam)
            context['hhjp_0125_5'] = format_hoso.kiemtra(donhang_congviec.thangnam_thoigian_nam)
            context['hhjp_0125_6'] = format_hoso.kiemtra(donhang_congviec.thangsau_thoigian_nam)
            context['hhjp_0125_7'] = format_hoso.kiemtra(donhang_congviec.thangbay_thoigian_nam)
            context['hhjp_0125_8'] = format_hoso.kiemtra(donhang_congviec.thangtam_thoigian_nam)
            context['hhjp_0125_9'] = format_hoso.kiemtra(donhang_congviec.thangchin_thoigian_nam)
            context['hhjp_0125_10'] = format_hoso.kiemtra(donhang_congviec.thangmuoi_thoigian_nam)
            context['hhjp_0125_11'] = format_hoso.kiemtra(donhang_congviec.thangmuoimot_thoigian_nam)
            context['hhjp_0125_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_nam)
            context['hhjp_0125_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_nam)

            context['hhjp_0126'] = Listing(
                donhang_congviec.noidung_congviec_sau) if donhang_congviec.noidung_congviec_sau else ''
            context['hhjp_0126_ts'] = format_hoso.kiemtra(donhang_congviec.coso_congviec_sau)
            context['hhjp_0126_t'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_sau)
            context['hhjp_0126_1'] = format_hoso.kiemtra(donhang_congviec.thangmot_thoigian_sau)
            context['hhjp_0126_2'] = format_hoso.kiemtra(donhang_congviec.thanghai_thoigian_sau)
            context['hhjp_0126_3'] = format_hoso.kiemtra(donhang_congviec.thangba_thoigian_sau)
            context['hhjp_0126_4'] = format_hoso.kiemtra(donhang_congviec.thangbon_thoigian_sau)
            context['hhjp_0126_5'] = format_hoso.kiemtra(donhang_congviec.thangnam_thoigian_sau)
            context['hhjp_0126_6'] = format_hoso.kiemtra(donhang_congviec.thangsau_thoigian_sau)
            context['hhjp_0126_7'] = format_hoso.kiemtra(donhang_congviec.thangbay_thoigian_sau)
            context['hhjp_0126_8'] = format_hoso.kiemtra(donhang_congviec.thangtam_thoigian_sau)
            context['hhjp_0126_9'] = format_hoso.kiemtra(donhang_congviec.thangchin_thoigian_sau)
            context['hhjp_0126_10'] = format_hoso.kiemtra(donhang_congviec.thangmuoi_thoigian_sau)
            context['hhjp_0126_11'] = format_hoso.kiemtra(donhang_congviec.thangmuoimot_thoigian_sau)
            context['hhjp_0126_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_sau)
            context['hhjp_0126_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_sau)

            context['hhjp_0121_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian)
            context['hhjp_0122_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangmot)
            context['hhjp_0123_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thanghai)
            context['hhjp_0124_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangba)
            context['hhjp_0125_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangbon)
            context['hhjp_0126_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangnam)
            context['hhjp_0127_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangsau)
            context['hhjp_0128_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangbay)
            context['hhjp_0129_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangtam)
            context['hhjp_0130_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangchin)
            context['hhjp_0131_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangmuoi)
            context['hhjp_0132_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangmuoimot)
            context['hhjp_0133_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangmuoihai)
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_5(self):
        print('5')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_5")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}
            docdata.render(context)

            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_6(self, ma_thuctapsinh, document):
        print('6')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_6")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}
            context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            context['hhjp_0014'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep)
            context['hhjp_0014_1'] = format_hoso.kiemtra(document.xinghiep_hoso.chucvu_daidien_han)
            context['hhjp_0009'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_furi_xnghiep)
            if document.xinghiep_hoso.sobuudien_xnghiep:
                context['bs_0006'] = document.xinghiep_hoso.sobuudien_xnghiep[
                                     0:3] + '-' + document.xinghiep_hoso.sobuudien_xnghiep[
                                                  3:]
            else:
                context['bs_0006'] = ''
            context['hhjp_0011'] = format_hoso.kiemtra(document.xinghiep_hoso.diachi_han_xnghiep)
            context['hhjp_0012'] = format_hoso.kiemtra(document.xinghiep_hoso.sdt_xnghiep)
            context['hhjp_0911'] = format_hoso.kiemtra(document.xinghiep_hoso.so_chungnhan_daotao)
            context['hhjp_0582'] = format_hoso.convert_date(document.xinghiep_hoso.ngay_chungnhan_daotao)
            context['hhjp_0902'] = format_hoso.convert_date(document.xinghiep_hoso.ngay_batdau_daotao)
            context['bs_0051'] = format_hoso.convert_date(document.xinghiep_hoso.ghichu_chungnhan_daotao)
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_7(self, ma_thuctapsinh, document):
        print('7')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_7")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            thuctapsinh_thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search(
                [('xinghiep_thuctapsinh', '=', document.xinghiep_hoso.id)])
            context = {}

            context['hhjp_0811'] = format_hoso.kiemtra(document.xinghiep_hoso.so_chapnhan_daotao)
            context['hhjp_0811_2'] = format_hoso.kiemtra(document.xinghiep_hoso.sobaohiem_laodong)
            if document.xinghiep_hoso.kehoach_cochua == True:
                context['hhjp_0811_1'] = u'■'
            else:
                context['hhjp_0811_1'] = u'□'
            context['hhjp_0009'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_furi_xnghiep)
            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            context['h0020'] = format_hoso.kiemtra(document.xinghiep_hoso.so_nvien_xnghiep)

            tts_tn = 1
            for tts_xnghiep in document.xinghiep_hoso.tts_xnghiep:
                context['a28%d' % tts_tn] = format_hoso.kiemtra(tts_xnghiep.quoctich_tts_xnghiep.name_quoctich)
                context['a29%d' % tts_tn] = format_hoso.kiemtra(tts_xnghiep.soluong_tts_xnghiep)
                tts_tn += 1
            context['hhjp_0918'] = format_hoso.kiemtra(document.xinghiep_hoso.ttskn_first)
            context['hhjp_0920'] = format_hoso.kiemtra(document.xinghiep_hoso.ttskn_second)

            context['hhjp_0922'] = format_hoso.kiemtra(document.xinghiep_hoso.ttskn_third)
            context['hhjp_0030'] = format_hoso.kiemtra(document.xinghiep_hoso.ttskn_first_dtql)

            context['hhjp_0534'] = format_hoso.kiemtra(document.xinghiep_hoso.ttskn_second_dtql)
            context['hhjp_0558'] = format_hoso.kiemtra(document.xinghiep_hoso.tts_vn_namnhat_bt)
            context['hhjp_0559'] = format_hoso.kiemtra(document.xinghiep_hoso.tts_vn_namnhat_chedocu_bt)

            context['hhjp_0536'] = format_hoso.kiemtra(document.xinghiep_hoso.ttskn_third_dtql)

            context['hhjp_0563'] = format_hoso.kiemtra(document.xinghiep_hoso.tts_vn_namhai_bt)
            context['hhjp_0564'] = format_hoso.kiemtra(document.xinghiep_hoso.tts_vn_namhai_chedocu_bt)

            context['hhjp_0568'] = format_hoso.kiemtra(document.xinghiep_hoso.tts_vn_namba_bt)
            context['hhjp_0569'] = format_hoso.kiemtra(document.xinghiep_hoso.tts_vn_namba_chedocu_bt)
            context['hhjp_0573'] = Listing(
                document.xinghiep_hoso.ghichu_botron) if document.xinghiep_hoso.ghichu_botron else ''

            table_thuctapsinh = []
            for thuctapsinh_thuctapsinh in thuctapsinh_thuctapsinh:
                info = {}
                if thuctapsinh_thuctapsinh.sochungnhan_namba:
                    info['hhjp_0911'] = format_hoso.kiemtra(thuctapsinh_thuctapsinh.sochungnhan_namba)
                    info['hhjp_0502'] = format_hoso.convert_date(thuctapsinh_thuctapsinh.ngaychungnhan_namba)
                    info['hhjp_0113_A'] = '□'
                    info['hhjp_0113_B'] = '□'
                    info['hhjp_0113_C'] = '■'
                    info['hhjp_0584'] = format_hoso.convert_date(thuctapsinh_thuctapsinh.ketthuc_thucte_namba)
                elif thuctapsinh_thuctapsinh.sochungnhan_namhai:
                    info['hhjp_0911'] = format_hoso.kiemtra(thuctapsinh_thuctapsinh.sochungnhan_namhai)
                    info['hhjp_0502'] = format_hoso.convert_date(thuctapsinh_thuctapsinh.ngaychungnhan_namhai)
                    info['hhjp_0113_A'] = '□'
                    info['hhjp_0113_B'] = '■'
                    info['hhjp_0113_C'] = '□'
                    info['hhjp_0584'] = format_hoso.convert_date(thuctapsinh_thuctapsinh.ketthuc_thucte_namhai)
                elif thuctapsinh_thuctapsinh.sochungnhan_namnhat:
                    info['hhjp_0911'] = format_hoso.kiemtra(thuctapsinh_thuctapsinh.sochungnhan_namnhat)
                    info['hhjp_0502'] = format_hoso.convert_date(thuctapsinh_thuctapsinh.ngaychungnhan_namnhat)
                    info['hhjp_0113_A'] = '■'
                    info['hhjp_0113_B'] = '□'
                    info['hhjp_0113_C'] = '□'
                    info['hhjp_0584'] = format_hoso.convert_date(thuctapsinh_thuctapsinh.ketthuc_thucte_namnhat)
                else:
                    info['hhjp_0911'] = ''
                    info['hhjp_0502'] = ''
                    info['hhjp_0113_A'] = '□'
                    info['hhjp_0113_B'] = '□'
                    info['hhjp_0113_C'] = '□'
                    info['hhjp_0584'] = ''


                info['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh_thuctapsinh.ten_lt_tts)
                info['hhjp_0003'] = format_hoso.kiemtra(thuctapsinh_thuctapsinh.quoctich_tts.name_quoctich)
                info['hhjp_0004'] = format_hoso.convert_date(thuctapsinh_thuctapsinh.ngaysinh_tts)
                info['hhjp_0006'] = format_hoso.gender_check(thuctapsinh.gtinh_tts)
                table_thuctapsinh.append(info)
            context['tbl_danhsachtts'] = table_thuctapsinh

            context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_8(self, ma_thuctapsinh, document):
        print('8')

        thuctapsinh = len(document.thuctapsinh_hoso)

        if thuctapsinh == 1:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_8")], limit=1)
        else:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_8_1")], limit=1)

        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)

            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            context['hhjp_0014'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep)
            context['hhjp_0014_1'] = format_hoso.kiemtra(document.xinghiep_hoso.chucvu_daidien_han)

            if thuctapsinh == 1:
                for tts_hoso in document.thuctapsinh_hoso:
                    context['hhjp_0001'] = format_hoso.kiemtra(tts_hoso.ten_lt_tts)
                    context['hhjp_0003'] = format_hoso.kiemtra(tts_hoso.quoctich_tts.name_quoctich)
            else:
                table_tts = []
                for tts_hoso in document.thuctapsinh_hoso:
                    infor = {}
                    infor['hhjp_0001'] = format_hoso.kiemtra(tts_hoso.ten_lt_tts)
                    infor['hhjp_0003'] = format_hoso.kiemtra(tts_hoso.quoctich_tts.name_quoctich)
                    table_tts.append(infor)
                context['tbl_tts'] = table_tts

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_9(self, ma_thuctapsinh, document):
        print('9')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_9")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            context = {}

            if document.ngay_lapvanban:
                context['hhjp_0193'] = str(document.ngay_lapvanban.year) + '年' + \
                                       str(document.ngay_lapvanban.month) + '月' + \
                                       str(document.ngay_lapvanban.day) + '日'
                context['hhjp_0193_v'] = 'Ngày ' + str(document.ngay_lapvanban.day) + \
                                         ' tháng ' + str(document.ngay_lapvanban.month) + \
                                         ' năm ' + str(document.ngay_lapvanban.year)
            else:
                context['hhjp_0193'] = '年　月　日'
                context['hhjp_0193_v'] = 'Ngày tháng năm'

            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            context['hhjp_0002'] = format_hoso.kiemtra(thuctapsinh.ten_han_tts)
            context['hhjp_0003'] = format_hoso.kiemtra(thuctapsinh.quoctich_tts.name_quoctich)
            context['hhjp_0003_v'] = format_hoso.kiemtra(thuctapsinh.quoctich_tts_v)

            if thuctapsinh.gtinh_tts == u'Nam':
                context['a12_1'] = '■'
                context['a12_2'] = '□'
            elif thuctapsinh.gtinh_tts == u'Nữ':
                context['a12_1'] = '□'
                context['a12_2'] = '■'
            else:
                context['a12_1'] = '□'
                context['a12_2'] = '□'

            context['hhjp_0330'] = format_hoso.kiemtra(thuctapsinh.tiengmede_tts)
            context['hhjp_0330_v'] = format_hoso.kiemtra(thuctapsinh.tiengmede_tts_v)
            if thuctapsinh.ngaysinh_tts:
                context['hhjp_0004'] = str(thuctapsinh.ngaysinh_tts.year) + '年' + \
                                       str(thuctapsinh.ngaysinh_tts.month) + '月' + \
                                       str(thuctapsinh.ngaysinh_tts.day) + '日'

                context['hhjp_0004_v'] = 'Ngày ' + str(thuctapsinh.ngaysinh_tts.day) + \
                                         ' tháng ' + str(thuctapsinh.ngaysinh_tts.month) + \
                                         ' năm ' + str(thuctapsinh.ngaysinh_tts.year)

            else:
                context['hhjp_0004'] = '年　月 日'
                context['hhjp_0004_v'] = 'Ngày tháng năm'

            context['hhjp_0005'] = format_hoso.kiemtra(thuctapsinh.tuoi_tts)
            context['hhjp_0005_v'] = format_hoso.kiemtra(thuctapsinh.tuoi_tts)
            context['hhjp_0331'] = format_hoso.kiemtra(thuctapsinh.diachi_tts)
            context['hhjp_0331_v'] = format_hoso.kiemtra(thuctapsinh.diachi_tts_v)

            hhjp_0342_1 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_hoctap_nam_1,
                                                              thuctapsinh.batdau_hoctap_thang_1)) if thuctapsinh.batdau_hoctap_nam_1 else ''
            hhjp_0343_1 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_hoctap_nam_1,
                                                              thuctapsinh.ketthuc_hoctap_thang_1)) if thuctapsinh.ketthuc_hoctap_nam_1 else ''
            context['hhjp_0342_1'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0342_1, hhjp_0343_1))

            context['hhjp_0344_1'] = format_hoso.kiemtra(thuctapsinh.truong_tts_1)

            hhjp_0342_v1 = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_hoctap_nam_1,
                                                                 thuctapsinh.batdau_hoctap_thang_1)) if thuctapsinh.batdau_hoctap_nam_1 else ''
            hhjp_0343_v1 = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_hoctap_nam_1,
                                                                 thuctapsinh.ketthuc_hoctap_thang_1)) if thuctapsinh.ketthuc_hoctap_nam_1 else ''
            context['hhjp_0342_v1'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0342_v1, hhjp_0343_v1))

            context['hhjp_0344_v1'] = format_hoso.kiemtra(thuctapsinh.truong_tts_v_1)

            hhjp_0342_2 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_hoctap_nam_2,
                                                              thuctapsinh.batdau_hoctap_thang_2)) if thuctapsinh.batdau_hoctap_nam_2 else ''
            hhjp_0343_2 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_hoctap_nam_2,
                                                              thuctapsinh.ketthuc_hoctap_thang_2)) if thuctapsinh.ketthuc_hoctap_nam_2 else ''
            context['hhjp_0342_2'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0342_2, hhjp_0343_2))
            context['hhjp_0344_2'] = format_hoso.kiemtra(thuctapsinh.truong_tts_2)
            hhjp_0342_v2 = format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_hoctap_nam_2,
                                                             thuctapsinh.batdau_hoctap_thang_2) if thuctapsinh.batdau_hoctap_nam_2 else ''
            hhjp_0343_v2 = format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_hoctap_nam_2,
                                                             thuctapsinh.ketthuc_hoctap_thang_2) if thuctapsinh.ketthuc_hoctap_nam_2 else ''
            context['hhjp_0342_v2'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0342_v2, hhjp_0343_v2))
            context['hhjp_0344_v2'] = format_hoso.kiemtra(thuctapsinh.truong_tts_v_2)

            hhjp_0342_3 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_hoctap_nam_3,
                                                              thuctapsinh.batdau_hoctap_thang_3)) if thuctapsinh.batdau_hoctap_nam_3 else ''
            hhjp_0343_3 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_hoctap_nam_3,
                                                              thuctapsinh.ketthuc_hoctap_thang_3)) if thuctapsinh.ketthuc_hoctap_nam_3 else ''

            context['hhjp_0342_3'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0342_3, hhjp_0343_3))
            context['hhjp_0344_3'] = format_hoso.kiemtra(thuctapsinh.truong_tts_3)
            hhjp_0342_v3 = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_hoctap_nam_3,
                                                                 thuctapsinh.batdau_hoctap_thang_3)) if thuctapsinh.batdau_hoctap_nam_3 else ''
            hhjp_0343_v3 = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_hoctap_nam_3,
                                                                 thuctapsinh.ketthuc_hoctap_thang_3)) if thuctapsinh.ketthuc_hoctap_nam_3 else ''
            context['hhjp_0344_v3'] = format_hoso.kiemtra(thuctapsinh.truong_tts_v_3)

            context['hhjp_0342_v3'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0342_v3, hhjp_0343_v3))

            ### Công tác 1
            hhjp_0347_1 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_congtac_tts_nam_1,
                                                              thuctapsinh.batdau_congtac_tts_thang_1)) if thuctapsinh.batdau_congtac_tts_nam_1 else ''

            if thuctapsinh.den_nay_1 == True:
                hhjp_0348_1 = '現在'
            else:
                hhjp_0348_1 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_congtac_tts_nam_1,
                                                                  thuctapsinh.ketthuc_congtac_tts_thang_1)) if thuctapsinh.ketthuc_congtac_tts_nam_1 else ''

            context['hhjp_0347_1'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_1, hhjp_0348_1))

            hhjp_0349_1 = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_1)
            hhjp_0350_1 = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_1.name_loaicongviec)
            context['hhjp_0349_1'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_1, hhjp_0350_1))

            hhjp_0347_1_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_congtac_tts_nam_1,
                                                                  thuctapsinh.batdau_congtac_tts_thang_1)) if thuctapsinh.batdau_congtac_tts_nam_1 else ''
            if thuctapsinh.den_nay_1 == True:
                hhjp_0348_1_v = 'Hiện nay'
            else:
                hhjp_0348_1_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_congtac_tts_nam_1,
                                                                      thuctapsinh.ketthuc_congtac_tts_thang_1)) if thuctapsinh.ketthuc_congtac_tts_nam_1 else ''
            context['hhjp_0347_1_v'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_1_v, hhjp_0348_1_v))

            hhjp_0349_1_v = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_v_1)
            hhjp_0350_1_v = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_1)
            context['hhjp_0349_1_v'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_1_v, hhjp_0350_1_v))

            ### Công tác 2
            hhjp_0347_2 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_congtac_tts_nam_1,
                                                              thuctapsinh.batdau_congtac_tts_thang_1)) if thuctapsinh.batdau_congtac_tts_nam_1 else ''

            if thuctapsinh.den_nay_2 == True:
                hhjp_0348_2 = '現在'
            else:
                hhjp_0348_2 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_congtac_tts_nam_2,
                                                                  thuctapsinh.ketthuc_congtac_tts_thang_2)) if thuctapsinh.ketthuc_congtac_tts_nam_2 else ''

            context['hhjp_0347_2'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_2, hhjp_0348_2))

            hhjp_0349_2 = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_2)
            hhjp_0350_2 = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_2.name_loaicongviec)
            context['hhjp_0349_2'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_2, hhjp_0350_2))

            hhjp_0347_2_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_congtac_tts_nam_2,
                                                                  thuctapsinh.batdau_congtac_tts_thang_2)) if thuctapsinh.batdau_congtac_tts_nam_2 else ''
            if thuctapsinh.den_nay_2 == True:
                hhjp_0348_2_v = 'Hiện nay'
            else:
                hhjp_0348_2_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_congtac_tts_nam_2,
                                                                      thuctapsinh.ketthuc_congtac_tts_thang_2)) if thuctapsinh.ketthuc_congtac_tts_nam_2 else ''

            context['hhjp_0347_2_v'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_2_v, hhjp_0348_2_v))

            hhjp_0349_2_v = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_v_2)
            hhjp_0350_2_v = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_2)
            context['hhjp_0349_2_v'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_2_v, hhjp_0350_2_v))

            ### Công tác 3
            hhjp_0347_3 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_congtac_tts_nam_3,
                                                              thuctapsinh.batdau_congtac_tts_thang_3)) if thuctapsinh.batdau_congtac_tts_nam_3 else ''

            if thuctapsinh.den_nay_3 == True:
                hhjp_0348_3 = '現在'
            else:
                hhjp_0348_3 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_congtac_tts_nam_3,
                                                                  thuctapsinh.ketthuc_congtac_tts_thang_3)) if thuctapsinh.ketthuc_congtac_tts_nam_3 else ''

            context['hhjp_0347_3'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_3, hhjp_0348_3))

            hhjp_0349_3 = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_3)
            hhjp_0350_3 = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_3.name_loaicongviec)
            context['hhjp_0349_3'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_3, hhjp_0350_3))

            context['hhjp_0347_3_v'] = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_congtac_tts_nam_3,
                                                                             thuctapsinh.batdau_congtac_tts_thang_3)) if thuctapsinh.batdau_congtac_tts_nam_3 else ''

            if thuctapsinh.den_nay_3 == True:
                hhjp_0348_3_v = 'Hiện nay'
            else:
                hhjp_0348_3_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_congtac_tts_nam_3,
                                                                      thuctapsinh.ketthuc_congtac_tts_thang_3)) if thuctapsinh.ketthuc_congtac_tts_nam_3 else ''

            context['hhjp_0348_3_v'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0348_3_v, hhjp_0348_3_v))

            hhjp_0349_3_v = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_v_3)
            hhjp_0350_3_v = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_3)
            context['hhjp_0349_3_v'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_3_v, hhjp_0350_3_v))

            ### Công tác 4
            hhjp_0347_4 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_congtac_tts_nam_4,
                                                              thuctapsinh.batdau_congtac_tts_thang_4)) if thuctapsinh.batdau_congtac_tts_nam_4 else ''

            if thuctapsinh.den_nay_4 == True:
                hhjp_0348_4 = '現在'
            else:
                hhjp_0348_4 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_congtac_tts_nam_4,
                                                                  thuctapsinh.ketthuc_congtac_tts_thang_4)) if thuctapsinh.ketthuc_congtac_tts_nam_4 else ''

            context['hhjp_0347_4'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_4, hhjp_0348_4))

            hhjp_0349_4 = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_4)
            hhjp_0350_4 = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_4.name_loaicongviec)
            context['hhjp_0349_4'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_4, hhjp_0350_4))
            hhjp_0347_4_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_congtac_tts_nam_4,
                                                                  thuctapsinh.batdau_congtac_tts_thang_4)) if thuctapsinh.batdau_congtac_tts_nam_4 else ''
            if thuctapsinh.den_nay_4 == True:
                hhjp_0348_4_v = 'Hiện nay'
            else:
                hhjp_0348_4_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_congtac_tts_nam_4,
                                                                      thuctapsinh.ketthuc_congtac_tts_thang_4)) if thuctapsinh.ketthuc_congtac_tts_nam_4 else ''
            context['hhjp_0347_4_v'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_4_v, hhjp_0348_4_v))

            hhjp_0349_4_v = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_v_4)
            hhjp_0350_4_v = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_4)
            context['hhjp_0349_4_v'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_4_v, hhjp_0350_4_v))

            ### Công tác 5
            hhjp_0347_5 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_congtac_tts_nam_5,
                                                              thuctapsinh.batdau_congtac_tts_thang_5)) if thuctapsinh.batdau_congtac_tts_nam_5 else ''

            if thuctapsinh.den_nay_5 == True:
                hhjp_0348_5 = '現在'
            else:
                hhjp_0348_5 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_congtac_tts_nam_5,
                                                                  thuctapsinh.ketthuc_congtac_tts_thang_5)) if thuctapsinh.ketthuc_congtac_tts_nam_5 else ''
            context['hhjp_0347_5'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_5, hhjp_0348_5))

            hhjp_0349_5 = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_5)
            hhjp_0350_5 = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_5.name_loaicongviec)
            context['hhjp_0349_5'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_5, hhjp_0350_5))

            hhjp_0347_5_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_congtac_tts_nam_5,
                                                                  thuctapsinh.batdau_congtac_tts_thang_5)) if thuctapsinh.batdau_congtac_tts_nam_5 else ''
            if thuctapsinh.den_nay_5 == True:
                hhjp_0348_5_v = 'Hiện nay'
            else:
                hhjp_0348_5_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_congtac_tts_nam_5,
                                                                      thuctapsinh.ketthuc_congtac_tts_thang_5)) if thuctapsinh.ketthuc_congtac_tts_nam_5 else ''

            context['hhjp_0347_5_v'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_5_v, hhjp_0348_5_v))

            hhjp_0349_5_v = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_v_5)
            hhjp_0350_5_v = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_5)
            context['hhjp_0349_5_v'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_5_v, hhjp_0350_5_v))
            ### Công tác 6
            hhjp_0347_6 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_congtac_tts_nam_6,
                                                              thuctapsinh.batdau_congtac_tts_thang_6)) if thuctapsinh.batdau_congtac_tts_nam_6 else ''

            if thuctapsinh.den_nay_6 == True:
                hhjp_0348_6 = '現在'
            else:
                hhjp_0348_6 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_congtac_tts_nam_6,
                                                                  thuctapsinh.ketthuc_congtac_tts_thang_6)) if thuctapsinh.ketthuc_congtac_tts_nam_6 else ''
            context['hhjp_0347_6'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_6, hhjp_0348_6))

            hhjp_0349_6 = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_6)
            hhjp_0350_6 = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_6.name_loaicongviec)
            context['hhjp_0349_6'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_6, hhjp_0350_6))

            hhjp_0347_6_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_congtac_tts_nam_6,
                                                                  thuctapsinh.batdau_congtac_tts_thang_6)) if thuctapsinh.batdau_congtac_tts_nam_6 else ''
            if thuctapsinh.den_nay_6 == True:
                hhjp_0348_6_v = 'Hiện nay'
            else:
                hhjp_0348_6_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_congtac_tts_nam_6,
                                                                      thuctapsinh.ketthuc_congtac_tts_thang_6)) if thuctapsinh.ketthuc_congtac_tts_nam_6 else ''

            context['hhjp_0347_6_v'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_6_v, hhjp_0348_6_v))

            hhjp_0349_6_v = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_v_6)
            hhjp_0350_6_v = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_6)
            context['hhjp_0349_6_v'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_6_v, hhjp_0350_6_v))

            ### Công tác 7
            hhjp_0347_7 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_congtac_tts_nam_7,
                                                              thuctapsinh.batdau_congtac_tts_thang_7)) if thuctapsinh.batdau_congtac_tts_nam_7 else ''

            if thuctapsinh.den_nay_7 == True:
                hhjp_0348_7 = '現在'
            else:
                hhjp_0348_7 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_congtac_tts_nam_7,
                                                                  thuctapsinh.ketthuc_congtac_tts_thang_7)) if thuctapsinh.ketthuc_congtac_tts_nam_7 else ''
            context['hhjp_0347_7'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_7, hhjp_0348_7))

            hhjp_0349_7 = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_7)
            hhjp_0350_7 = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_7.name_loaicongviec)
            context['hhjp_0349_7'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_7, hhjp_0350_7))

            hhjp_0347_7_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_congtac_tts_nam_7,
                                                                  thuctapsinh.batdau_congtac_tts_thang_7)) if thuctapsinh.batdau_congtac_tts_nam_7 else ''
            if thuctapsinh.den_nay_7 == True:
                hhjp_0348_7_v = 'Hiện nay'
            else:
                hhjp_0348_7_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_congtac_tts_nam_7,
                                                                      thuctapsinh.ketthuc_congtac_tts_thang_7)) if thuctapsinh.ketthuc_congtac_tts_nam_7 else ''

            context['hhjp_0347_7_v'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_7_v, hhjp_0348_7_v))

            if thuctapsinh.congviec_lienquan_1:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_1.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_1)
                context['ahh_0352'] = format_hoso.kiemtra(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.kiemtra(thuctapsinh.fill)
            elif thuctapsinh.congviec_lienquan_2:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_2.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_2)
                context['ahh_0352'] = format_hoso.kiemtra(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.kiemtra(thuctapsinh.fill)
            elif thuctapsinh.congviec_lienquan_3:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_3.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_3)
                context['ahh_0352'] = format_hoso.kiemtra(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.kiemtra(thuctapsinh.fill)
            elif thuctapsinh.congviec_lienquan_4:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_4.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_4)
                context['ahh_0352'] = format_hoso.kiemtra(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.kiemtra(thuctapsinh.fill)
            elif thuctapsinh.congviec_lienquan_5:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_5.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_5)
                context['ahh_0352'] = format_hoso.kiemtra(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.kiemtra(thuctapsinh.fill)
            elif thuctapsinh.congviec_lienquan_6:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_6.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_6)
                context['ahh_0352'] = format_hoso.round_nam(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.round_nam(thuctapsinh.fill)
            elif thuctapsinh.congviec_lienquan_7:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_7.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_7)
                context['ahh_0352'] = format_hoso.round_nam(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.round_nam(thuctapsinh.fill)

            if thuctapsinh.lichsunhapcu_tts == 'Có':
                context['hhjp_0360_1'] = u'■'
                context['hhjp_0360_4'] = u'□'
                if thuctapsinh.tucach_luutru_tts == 'Thực tập sinh kỹ năng':
                    context['hhjp_0360_2'] = u'■'
                    context['hhjp_0360_3'] = u'□'
                elif thuctapsinh.tucach_luutru_tts == 'Không phải thực tập sinh kỹ năng':
                    context['hhjp_0360_2'] = u'□'
                    context['hhjp_0360_3'] = u'■'
                else:
                    context['hhjp_0360_2'] = u'□'
                    context['hhjp_0360_3'] = u'□'

                if thuctapsinh.ngaydinhat_tts:
                    context['hhjp_0361'] = str(thuctapsinh.ngaydinhat_tts.year) + '.' + str(
                        thuctapsinh.ngaydinhat_tts.month) + '.' + str(thuctapsinh.ngaydinhat_tts.day)
                else:
                    context['hhjp_0361'] = ''
                if thuctapsinh.onhatden_tts:
                    context['hhjp_0362'] = str(thuctapsinh.onhatden_tts.year) + '.' + str(
                        thuctapsinh.onhatden_tts.month) + '.' + str(thuctapsinh.onhatden_tts.day)
                else:
                    context['hhjp_0362'] = ''

                if (thuctapsinh.batdau_kynang_tts and thuctapsinh.ketthuc_kynang_tts):
                    context['bs_0052'] = str(thuctapsinh.batdau_kynang_tts.year) + '年' + \
                                         str(thuctapsinh.batdau_kynang_tts.month) + '月' + \
                                         str(thuctapsinh.batdau_kynang_tts.day) + '日'
                    context['bs_0052_v'] = 'Ngày ' + str(thuctapsinh.batdau_kynang_tts.day) + \
                                           ' tháng ' + str(thuctapsinh.batdau_kynang_tts.month) + \
                                           ' năm ' + str(thuctapsinh.batdau_kynang_tts.year)
                    context['bs_0053'] = str(thuctapsinh.ketthuc_kynang_tts.year) + '年' + \
                                         str(thuctapsinh.ketthuc_kynang_tts.month) + '月' + \
                                         str(thuctapsinh.ketthuc_kynang_tts.day) + '日'
                    context['bs_0053_v'] = 'Ngày ' + str(thuctapsinh.ketthuc_kynang_tts.day) + \
                                           ' tháng ' + str(thuctapsinh.ketthuc_kynang_tts.month) + \
                                           ' năm ' + str(thuctapsinh.ketthuc_kynang_tts.year)
                    context['bs_0052_1'] = '■'
                else:
                    context['bs_0052'] = '年　月　日'
                    context['bs_0052_v'] = 'Ngày tháng năm'
                    context['bs_0053'] = '年　月　日'
                    context['bs_0053_v'] = 'Ngày tháng năm'
                    context['bs_0052_1'] = '□'

                if (thuctapsinh.batdau_xaydung_tts and thuctapsinh.ketthuc_xaydung_tts):
                    context['bs_0054'] = str(thuctapsinh.batdau_xaydung_tts.year) + '年' + \
                                         str(thuctapsinh.batdau_xaydung_tts.month) + '月' + \
                                         str(thuctapsinh.batdau_xaydung_tts.day) + '日'
                    context['bs_0054_v'] = 'Ngày ' + str(thuctapsinh.batdau_xaydung_tts.day) + \
                                           ' tháng ' + str(thuctapsinh.batdau_xaydung_tts.month) + \
                                           ' năm ' + str(thuctapsinh.batdau_xaydung_tts.year)
                    context['bs_0055'] = str(thuctapsinh.ketthuc_xaydung_tts.year) + '年' + \
                                         str(thuctapsinh.ketthuc_xaydung_tts.month) + '月' + \
                                         str(thuctapsinh.ketthuc_xaydung_tts.day) + '日'
                    context['bs_0055_v'] = 'Ngày ' + str(thuctapsinh.ketthuc_xaydung_tts.day) + \
                                           ' tháng ' + str(thuctapsinh.ketthuc_xaydung_tts.month) + \
                                           ' năm ' + str(thuctapsinh.ketthuc_xaydung_tts.year)
                    context['bs_0052_1'] = '■'

                else:
                    context['bs_0054'] = '年　月　日'
                    context['bs_0054_v'] = 'Ngày tháng năm'
                    context['bs_0055'] = '年　月　日'
                    context['bs_0055_v'] = 'Ngày tháng năm'
                    if (thuctapsinh.batdau_kynang_tts and thuctapsinh.ketthuc_kynang_tts):
                        context['bs_0052_1'] = '■'
                    else:
                        context['bs_0052_1'] = '□'

                if (thuctapsinh.batdau_ungvien_tts and thuctapsinh.ketthuc_ungvien_tts):
                    context['bs_0056'] = str(thuctapsinh.batdau_ungvien_tts.year) + '年' + \
                                         str(thuctapsinh.batdau_ungvien_tts.month) + '月' + \
                                         str(thuctapsinh.batdau_ungvien_tts.day) + '日'
                    context['bs_0056_v'] = 'Ngày ' + str(thuctapsinh.batdau_ungvien_tts.day) + \
                                           ' tháng ' + str(thuctapsinh.batdau_ungvien_tts.month) + \
                                           ' năm ' + str(thuctapsinh.batdau_ungvien_tts.year)
                    context['bs_0057'] = str(thuctapsinh.ketthuc_ungvien_tts.year) + '年' + \
                                         str(thuctapsinh.ketthuc_ungvien_tts.month) + '月' + \
                                         str(thuctapsinh.ketthuc_ungvien_tts.day) + '日'
                    context['bs_0057_v'] = 'Ngày ' + str(thuctapsinh.ketthuc_ungvien_tts.day) + \
                                           ' tháng ' + str(thuctapsinh.ketthuc_ungvien_tts.month) + \
                                           ' năm ' + str(thuctapsinh.ketthuc_ungvien_tts.year)
                    context['bs_0056_1'] = '■'
                else:
                    context['bs_0056'] = '年　月　日'
                    context['bs_0056_v'] = 'Ngày tháng năm'
                    context['bs_0057'] = '年　月　日'
                    context['bs_0057_v'] = 'Ngày tháng năm'
                    context['bs_0056_1'] = '□'
            else:
                context['hhjp_0360_4'] = u'■'
                context['hhjp_0360_1'] = u'□'

                context['hhjp_0360_2'] = u'□'
                context['hhjp_0360_3'] = u'□'

                context['hhjp_0361'] = ''
                context['hhjp_0362'] = ''
                context['bs_0052'] = '年　月　日'
                context['bs_0052_v'] = 'Ngày tháng năm'
                context['bs_0053'] = '年　月　日'
                context['bs_0053_v'] = 'Ngày tháng năm'
                context['bs_0054'] = '年　月　日'
                context['bs_0054_v'] = 'Ngày tháng năm'
                context['bs_0055'] = '年　月　日'
                context['bs_0055_v'] = 'Ngày tháng năm'
                context['bs_0056'] = '年　月　日'
                context['bs_0056_v'] = 'Ngày tháng năm'
                context['bs_0057'] = '年　月　日'
                context['bs_0057_v'] = 'Ngày tháng năm'
                context['bs_0056_1'] = '□'
                context['bs_0052_1'] = '□'

            if thuctapsinh.kinhnghiem_tts == 'Có':
                context['hhjp_0113_c'] = u'■'
                context['hhjp_0113_k'] = u'□'
            elif thuctapsinh.kinhnghiem_tts == 'Không':
                context['hhjp_0113_c'] = u'□'
                context['hhjp_0113_k'] = u'■'
            else:
                context['hhjp_0113_c'] = u'□'
                context['hhjp_0113_k'] = u'■'

            if thuctapsinh.phanloai_kehoach_thuctap_tts_1 == True:
                context['hhjp_0113_A'] = '■'
            else:
                context['hhjp_0113_A'] = '□'

            if thuctapsinh.phanloai_kehoach_thuctap_tts_2 == True:
                context['hhjp_0113_B'] = '■'
            else:
                context['hhjp_0113_B'] = '□'
            if thuctapsinh.phanloai_kehoach_thuctap_tts_3 == True:
                context['hhjp_0113_C'] = '■'
            else:
                context['hhjp_0113_C'] = '□'
            if thuctapsinh.phanloai_kehoach_thuctap_tts_4 == True:
                context['hhjp_0113_D'] = '■'
            else:
                context['hhjp_0113_D'] = '□'
            if thuctapsinh.phanloai_kehoach_thuctap_tts_5 == True:
                context['hhjp_0113_E'] = '■'
            else:
                context['hhjp_0113_E'] = '□'
            if thuctapsinh.phanloai_kehoach_thuctap_tts_6 == True:
                context['hhjp_0113_F'] = '■'
            else:
                context['hhjp_0113_F'] = '□'

            if thuctapsinh.knttkn_tu_tts:
                context['hhjp_0364'] = str(thuctapsinh.knttkn_tu_tts.year) + '.' + str(
                    thuctapsinh.knttkn_tu_tts.month) + '.' + str(thuctapsinh.knttkn_tu_tts.day)
            else:
                context['hhjp_0364'] = ''
            if thuctapsinh.knttkn_den_tts:
                context['hhjp_0365'] = str(thuctapsinh.knttkn_den_tts.year) + '.' + str(
                    thuctapsinh.knttkn_den_tts.month) + '.' + str(thuctapsinh.knttkn_den_tts.day)
            else:
                context['hhjp_0365'] = ''

            if thuctapsinh.buocloaibo_tts == u'Có':
                context['bs_0009_c'] = u'■'
                context['bs_0009_k'] = u'□'
                context['bs_0009'] = format_hoso.kiemtra(thuctapsinh.lydo_buocloaibo_tts)
                context['bs_0009_v'] = format_hoso.kiemtra(thuctapsinh.lydo_buocloaibo_tts_v)
            else:
                context['bs_0009_k'] = u'■'
                context['bs_0009_c'] = u'□'
                context['bs_0009'] = ''
                context['bs_0009_v'] = ''

            context['hhjp_0373'] = format_hoso.kiemtra(thuctapsinh.thongtinkhac_tts)
            context['hhjp_0373_v'] = format_hoso.kiemtra(thuctapsinh.thongtinkhac_tts_v)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_10(self, ma_thuctapsinh, document):
        print('10')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_10")], limit=1)
        demo = 'file_10'
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            # Hướng Dẫn Thực Tập
            table_huongdantt = []
            if document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.ten_han_nhanvien:
                infor1 = {}
                infor1['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.ten_phienam_nhanvien)
                infor1['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.ten_han_nhanvien)

                if document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.gtinh_nhanvien == u'Nam':
                    infor1['hhjp_0065_1'] = '■'
                    infor1['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.gtinh_nhanvien == u'Nữ':
                    infor1['hhjp_0065_1'] = '□'
                    infor1['hhjp_0065_2'] = '■'
                else:
                    infor1['hhjp_0065_1'] = '□'
                    infor1['hhjp_0065_2'] = '□'

                infor1['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.ngaysinh_nhanvien)
                infor1['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.sobuudien_nhanvien:
                    infor1[
                        'bs_0003'] = document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor1['bs_0003'] = ''
                infor1['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.diachi_nhanvien)
                infor1['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.dienthoai_nhanvien)
                infor1['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor1['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh:
                    infor1['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[3:]
                else:
                    infor1['bs_0005'] = ''
                infor1['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.sdt_chinhanh)
                infor1['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.congtac_nhanvien:
                    if congtac <= 6:
                        infor1['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor1['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor1['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor1['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.bangcap_nhanvien)

                infor1['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.nam_kinhnghiem_quanly_nhanvien)
                infor1['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.dugiang_kynang_nhanvien)
                infor1['b_0077_1'] = '□'
                infor1['b_0077_2'] = '□'
                infor1['a_0077_1'] = '□'
                infor1['a_0077_2'] = '□'
                table_huongdantt.append(infor1)

            if document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.ten_han_nhanvien:
                infor2 = {}
                infor2['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.ten_phienam_nhanvien)
                infor2['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.ten_han_nhanvien)

                if document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.gtinh_nhanvien == u'Nam':
                    infor2['hhjp_0065_1'] = '■'
                    infor2['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.gtinh_nhanvien == u'Nữ':
                    infor2['hhjp_0065_1'] = '□'
                    infor2['hhjp_0065_2'] = '■'
                else:
                    infor2['hhjp_0065_1'] = '□'
                    infor2['hhjp_0065_2'] = '□'

                infor2['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.ngaysinh_nhanvien)
                infor2['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.sobuudien_nhanvien:
                    infor2[
                        'bs_0003'] = document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor2['bs_0003'] = ''
                infor2['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.diachi_nhanvien)
                infor2['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.dienthoai_nhanvien)
                infor2['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor2['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh:
                    infor2['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[3:]
                else:
                    infor2['bs_0005'] = ''
                infor2['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.sdt_chinhanh)
                infor2['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.congtac_nhanvien:
                    if congtac <= 6:
                        infor2['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor2['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor2['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor2['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.bangcap_nhanvien)
                infor2['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.nam_kinhnghiem_quanly_nhanvien)
                infor2['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.dugiang_kynang_nhanvien)
                infor2['b_0077_1'] = '□'
                infor2['b_0077_2'] = '□'
                infor2['a_0077_1'] = '□'
                infor2['a_0077_2'] = '□'
                table_huongdantt.append(infor2)

            if document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.ten_han_nhanvien:
                infor3 = {}
                infor3['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.ten_phienam_nhanvien)
                infor3['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.ten_han_nhanvien)

                if document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.gtinh_nhanvien == u'Nam':
                    infor3['hhjp_0065_1'] = '■'
                    infor3['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.gtinh_nhanvien == u'Nữ':
                    infor3['hhjp_0065_1'] = '□'
                    infor3['hhjp_0065_2'] = '■'
                else:
                    infor3['hhjp_0065_1'] = '□'
                    infor3['hhjp_0065_2'] = '□'

                infor3['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.ngaysinh_nhanvien)
                infor3['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.sobuudien_nhanvien:
                    infor3[
                        'bs_0003'] = document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor3['bs_0003'] = ''
                infor3['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.diachi_nhanvien)
                infor3['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.dienthoai_nhanvien)
                infor3['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor3['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh:
                    infor3['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[3:]
                else:
                    infor3['bs_0005'] = ''
                infor3['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.sdt_chinhanh)
                infor3['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.congtac_nhanvien:
                    if congtac <= 6:
                        infor3['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor3['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor3['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor3['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.bangcap_nhanvien)
                infor3['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.nam_kinhnghiem_quanly_nhanvien)
                infor3['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.dugiang_kynang_nhanvien)
                infor3['b_0077_1'] = '□'
                infor3['b_0077_2'] = '□'
                infor3['a_0077_1'] = '□'
                infor3['a_0077_2'] = '□'
                table_huongdantt.append(infor3)

            context['huongdantt'] = table_huongdantt

            # Chỉ đạo thực tập
            table_chidaott = []
            # Nhân viên 1
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.ten_han_nhanvien:
                infor1 = {}
                infor1['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.ten_phienam_nhanvien)
                infor1['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.ten_han_nhanvien)
                infor1['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.ngaysinh_nhanvien)
                infor1['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.sobuudien_nhanvien:
                    infor1[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor1['bs_0003'] = ''
                infor1['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.diachi_nhanvien)
                infor1['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.dienthoai_nhanvien)
                infor1['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor1['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh:
                    infor1['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[3:]
                else:
                    infor1['bs_0005'] = ''
                infor1['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.sdt_chinhanh)
                infor1['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.congtac_nhanvien:
                    if congtac <= 6:
                        infor1['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor1['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor1['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor1['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.bangcap_nhanvien)
                infor1['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.congviec_nhanvien_chidaothuctap)
                infor1['b_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.congviec_nhanvien_chidaothuctap_2)
                infor1['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.kinhnghiem_nhanvien_chidaothuctap)

                infor1['b_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.kinhnghiem_nhanvien_chidaothuctap_2)

                infor1['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.nam_kinhnghiem_quanly_nhanvien)
                infor1['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.dugiang_kynang_nhanvien)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.gtinh_nhanvien == u'Nam':
                    infor1['hhjp_0065_1'] = '■'
                    infor1['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.gtinh_nhanvien == u'Nữ':
                    infor1['hhjp_0065_1'] = '□'
                    infor1['hhjp_0065_2'] = '■'
                else:
                    infor1['hhjp_0065_1'] = '□'
                    infor1['hhjp_0065_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    infor1['a_0077_1'] = '■'
                    infor1['a_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    infor1['a_0077_1'] = '□'
                    infor1['a_0077_2'] = '■'
                else:
                    infor1['a_0077_1'] = '□'
                    infor1['a_0077_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.thuongtruc_nhanvien_chidaothuctap_2 == u'Có':
                    infor1['b_0077_1'] = '■'
                    infor1['b_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.thuongtruc_nhanvien_chidaothuctap_2 == u'Không':
                    infor1['b_0077_1'] = '□'
                    infor1['b_0077_2'] = '■'
                else:
                    infor1['b_0077_1'] = '□'
                    infor1['b_0077_2'] = '□'
                table_chidaott.append(infor1)

            # Nhân viên 2
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.ten_han_nhanvien:
                infor2 = {}
                infor2['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.ten_phienam_nhanvien)
                infor2['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.ten_han_nhanvien)
                infor2['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.ngaysinh_nhanvien)
                infor2['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.sobuudien_nhanvien:
                    infor2[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor2['bs_0003'] = ''
                infor2['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.diachi_nhanvien)
                infor2['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.dienthoai_nhanvien)
                infor2['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor2['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh:
                    infor2['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[3:]
                else:
                    infor2['bs_0005'] = ''
                infor2['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.sdt_chinhanh)
                infor2['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.congtac_nhanvien:
                    if congtac <= 6:
                        infor2['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor2['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor2['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor2['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.bangcap_nhanvien)
                infor2['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.congviec_nhanvien_chidaothuctap)
                infor2['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.kinhnghiem_nhanvien_chidaothuctap)
                infor2['b_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.congviec_nhanvien_chidaothuctap_2)
                infor2['b_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.kinhnghiem_nhanvien_chidaothuctap_2)
                infor2['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.nam_kinhnghiem_quanly_nhanvien)
                infor2['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.gtinh_nhanvien == u'Nam':
                    infor2['hhjp_0065_1'] = '■'
                    infor2['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.gtinh_nhanvien == u'Nữ':
                    infor2['hhjp_0065_1'] = '□'
                    infor2['hhjp_0065_2'] = '■'
                else:
                    infor2['hhjp_0065_1'] = '□'
                    infor2['hhjp_0065_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    infor2['a_0077_1'] = '■'
                    infor2['a_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    infor2['a_0077_1'] = '□'
                    infor2['a_0077_2'] = '■'
                else:
                    infor2['a_0077_1'] = '□'
                    infor2['a_0077_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.thuongtruc_nhanvien_chidaothuctap_2 == u'Có':
                    infor2['b_0077_1'] = '■'
                    infor2['b_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.thuongtruc_nhanvien_chidaothuctap_2 == u'Không':
                    infor2['b_0077_1'] = '□'
                    infor2['b_0077_2'] = '■'
                else:
                    infor2['b_0077_1'] = '□'
                    infor2['b_0077_2'] = '□'
                table_chidaott.append(infor2)

            # Nhân viên 3
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.ten_han_nhanvien:
                infor3 = {}
                infor3['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.ten_phienam_nhanvien)
                infor3['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.ten_han_nhanvien)
                infor3['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.ngaysinh_nhanvien)
                infor3['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.sobuudien_nhanvien:
                    infor3[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor3['bs_0003'] = ''
                infor3['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.diachi_nhanvien)
                infor3['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.dienthoai_nhanvien)
                infor3['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor3['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh:
                    infor3['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[3:]
                else:
                    infor3['bs_0005'] = ''
                infor3['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.sdt_chinhanh)
                infor3['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.congtac_nhanvien:
                    if congtac <= 6:
                        infor3['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor3['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor3['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor3['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.bangcap_nhanvien)
                infor3['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.congviec_nhanvien_chidaothuctap)
                infor3['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.kinhnghiem_nhanvien_chidaothuctap)
                infor3['b_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.congviec_nhanvien_chidaothuctap_2)
                infor3['b_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.kinhnghiem_nhanvien_chidaothuctap_2)

                infor3['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.nam_kinhnghiem_quanly_nhanvien)
                infor3['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.gtinh_nhanvien == u'Nam':
                    infor3['hhjp_0065_1'] = '■'
                    infor3['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.gtinh_nhanvien == u'Nữ':
                    infor3['hhjp_0065_1'] = '□'
                    infor3['hhjp_0065_2'] = '■'
                else:
                    infor3['hhjp_0065_1'] = '□'
                    infor3['hhjp_0065_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    infor3['a_0077_1'] = '■'
                    infor3['a_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    infor3['a_0077_1'] = '□'
                    infor3['a_0077_2'] = '■'
                else:
                    infor3['a_0077_1'] = '□'
                    infor3['a_0077_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.thuongtruc_nhanvien_chidaothuctap_2 == u'Có':
                    infor3['b_0077_1'] = '■'
                    infor3['b_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.thuongtruc_nhanvien_chidaothuctap_2 == u'Không':
                    infor3['b_0077_1'] = '□'
                    infor3['b_0077_2'] = '■'
                else:
                    infor3['b_0077_1'] = '□'
                    infor3['b_0077_2'] = '□'
                table_chidaott.append(infor3)

            # Nhân viên 4
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.ten_han_nhanvien:
                infor4 = {}
                infor4['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.ten_phienam_nhanvien)
                infor4['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.ten_han_nhanvien)
                infor4['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.ngaysinh_nhanvien)
                infor4['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.sobuudien_nhanvien:
                    infor4[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor4['bs_0003'] = ''
                infor4['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.diachi_nhanvien)
                infor4['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.dienthoai_nhanvien)
                infor4['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor4['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh:
                    infor4['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[3:]
                else:
                    infor4['bs_0005'] = ''
                infor4['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.sdt_chinhanh)
                infor4['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.congtac_nhanvien:
                    if congtac <= 6:
                        infor4['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor4['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor4['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor4['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.bangcap_nhanvien)
                infor4['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.congviec_nhanvien_chidaothuctap)
                infor4['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.kinhnghiem_nhanvien_chidaothuctap)

                infor4['b_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.congviec_nhanvien_chidaothuctap_2)
                infor4['b_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.kinhnghiem_nhanvien_chidaothuctap_2)

                infor4['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.nam_kinhnghiem_quanly_nhanvien)
                infor4['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.gtinh_nhanvien == u'Nam':
                    infor4['hhjp_0065_1'] = '■'
                    infor4['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.gtinh_nhanvien == u'Nữ':
                    infor4['hhjp_0065_1'] = '□'
                    infor4['hhjp_0065_2'] = '■'
                else:
                    infor4['hhjp_0065_1'] = '□'
                    infor4['hhjp_0065_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    infor4['a_0077_1'] = '■'
                    infor4['a_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    infor4['a_0077_1'] = '□'
                    infor4['a_0077_2'] = '■'
                else:
                    infor4['a_0077_1'] = '□'
                    infor4['a_0077_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.thuongtruc_nhanvien_chidaothuctap_2 == u'Có':
                    infor4['b_0077_1'] = '■'
                    infor4['b_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.thuongtruc_nhanvien_chidaothuctap_2 == u'Không':
                    infor4['b_0077_1'] = '□'
                    infor4['b_0077_2'] = '■'
                else:
                    infor4['b_0077_1'] = '□'
                    infor4['b_0077_2'] = '□'
                table_chidaott.append(infor4)

            # Nhân viên 5
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.ten_han_nhanvien:
                infor5 = {}
                infor5['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.ten_phienam_nhanvien)
                infor5['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.ten_han_nhanvien)
                infor5['hhjp_0065'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.gtinh_nhanvien)
                infor5['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.ngaysinh_nhanvien)
                infor5['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.sobuudien_nhanvien:
                    infor5[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor5['bs_0003'] = ''
                infor5['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.diachi_nhanvien)
                infor5['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.dienthoai_nhanvien)
                infor5['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor5['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh:
                    infor5['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[3:]
                else:
                    infor5['bs_0005'] = ''
                infor5['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.sdt_chinhanh)
                infor5['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.congtac_nhanvien:
                    if congtac <= 6:
                        infor5['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor5['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor5['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor5['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.bangcap_nhanvien)
                infor5['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.congviec_nhanvien_chidaothuctap)
                infor5['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.kinhnghiem_nhanvien_chidaothuctap)
                infor5['b_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.congviec_nhanvien_chidaothuctap_2)
                infor5['b_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.kinhnghiem_nhanvien_chidaothuctap_2)
                infor5['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.nam_kinhnghiem_quanly_nhanvien)
                infor5['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.gtinh_nhanvien == u'Nam':
                    infor5['hhjp_0065_1'] = '■'
                    infor5['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.gtinh_nhanvien == u'Nữ':
                    infor5['hhjp_0065_1'] = '□'
                    infor5['hhjp_0065_2'] = '■'
                else:
                    infor5['hhjp_0065_1'] = '□'
                    infor5['hhjp_0065_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    infor5['a_0077_1'] = '■'
                    infor5['a_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    infor5['a_0077_1'] = '□'
                    infor5['a_0077_2'] = '■'
                else:
                    infor5['a_0077_1'] = '□'
                    infor5['a_0077_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.thuongtruc_nhanvien_chidaothuctap_2 == u'Có':
                    infor5['b_0077_1'] = '■'
                    infor5['b_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.thuongtruc_nhanvien_chidaothuctap_2 == u'Không':
                    infor5['b_0077_1'] = '□'
                    infor5['b_0077_2'] = '■'
                else:
                    infor5['b_0077_1'] = '□'
                    infor5['b_0077_2'] = '□'
                table_chidaott.append(infor5)

            # Nhân viên 6
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.ten_han_nhanvien:
                infor6 = {}
                infor6['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.ten_phienam_nhanvien)
                infor6['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.ten_han_nhanvien)
                infor6['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.ngaysinh_nhanvien)
                infor6['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.sobuudien_nhanvien:
                    infor6[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor6['bs_0003'] = ''
                infor6['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.diachi_nhanvien)
                infor6['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.dienthoai_nhanvien)

                infor6['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor6['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh:
                    infor6['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                                     3:]
                else:
                    infor6['bs_0005'] = ''
                infor6['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.sdt_chinhanh)
                infor6['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.congtac_nhanvien:
                    if congtac <= 6:
                        infor6['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor6['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor6['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor6['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.bangcap_nhanvien)
                infor6['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.congviec_nhanvien_chidaothuctap)
                infor6['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.kinhnghiem_nhanvien_chidaothuctap)
                infor6['b_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.congviec_nhanvien_chidaothuctap_2)
                infor6['b_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.kinhnghiem_nhanvien_chidaothuctap_2)
                infor6['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.nam_kinhnghiem_quanly_nhanvien)
                infor6['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.gtinh_nhanvien == u'Nam':
                    infor6['hhjp_0065_1'] = '■'
                    infor6['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.gtinh_nhanvien == u'Nữ':
                    infor6['hhjp_0065_1'] = '□'
                    infor6['hhjp_0065_2'] = '■'
                else:
                    infor6['hhjp_0065_1'] = '□'
                    infor6['hhjp_0065_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    infor6['a_0077_1'] = '■'
                    infor6['a_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    infor6['a_0077_1'] = '□'
                    infor6['a_0077_2'] = '■'
                else:
                    infor6['a_0077_1'] = '□'
                    infor6['a_0077_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.thuongtruc_nhanvien_chidaothuctap_2 == u'Có':
                    infor6['b_0077_1'] = '■'
                    infor6['b_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.thuongtruc_nhanvien_chidaothuctap_2 == u'Không':
                    infor6['b_0077_1'] = '□'
                    infor6['b_0077_2'] = '■'
                else:
                    infor6['b_0077_1'] = '□'
                    infor6['b_0077_2'] = '□'
                table_chidaott.append(infor6)

            # Nhân viên 7
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.ten_han_nhanvien:
                infor7 = {}
                infor7['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.ten_phienam_nhanvien)
                infor7['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.ten_han_nhanvien)
                infor7['hhjp_0065'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.gtinh_nhanvien)
                infor7['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.ngaysinh_nhanvien)
                infor7['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.sobuudien_nhanvien:
                    infor7[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor7['bs_0003'] = ''
                infor7['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.diachi_nhanvien)
                infor7['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.dienthoai_nhanvien)
                infor7['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor7['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh:
                    infor7['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                                     3:]
                else:
                    infor7['bs_0005'] = ''
                infor7['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.sdt_chinhanh)
                infor7['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.congtac_nhanvien:
                    if congtac <= 6:
                        infor7['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor7['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor7['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor7['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.bangcap_nhanvien)
                infor7['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.congviec_nhanvien_chidaothuctap)
                infor7['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.kinhnghiem_nhanvien_chidaothuctap)

                infor7['b_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.congviec_nhanvien_chidaothuctap_2)
                infor7['b_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.kinhnghiem_nhanvien_chidaothuctap_2)

                infor7['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.nam_kinhnghiem_quanly_nhanvien)
                infor7['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.gtinh_nhanvien == u'Nam':
                    infor7['hhjp_0065_1'] = '■'
                    infor7['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.gtinh_nhanvien == u'Nữ':
                    infor7['hhjp_0065_1'] = '□'
                    infor7['hhjp_0065_2'] = '■'
                else:
                    infor7['hhjp_0065_1'] = '□'
                    infor7['hhjp_0065_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    infor7['a_0077_1'] = '■'
                    infor7['a_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    infor7['a_0077_1'] = '□'
                    infor7['a_0077_2'] = '■'
                else:
                    infor7['a_0077_1'] = '□'
                    infor7['a_0077_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.thuongtruc_nhanvien_chidaothuctap_2 == u'Có':
                    infor7['b_0077_1'] = '■'
                    infor7['b_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.thuongtruc_nhanvien_chidaothuctap_2 == u'Không':
                    infor7['b_0077_1'] = '□'
                    infor7['b_0077_2'] = '■'
                else:
                    infor7['b_0077_1'] = '□'
                    infor7['b_0077_2'] = '□'
                table_chidaott.append(infor7)

            # Nhân viên 8
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.ten_han_nhanvien:
                infor8 = {}
                infor8['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.ten_phienam_nhanvien)
                infor8['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.ten_han_nhanvien)

                infor8['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.ngaysinh_nhanvien)
                infor8['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.sobuudien_nhanvien:
                    infor8[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor8['bs_0003'] = ''
                infor8['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.diachi_nhanvien)
                infor8['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.dienthoai_nhanvien)
                infor8['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor8['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh:
                    infor8['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                                     3:]
                else:
                    infor8['bs_0005'] = ''
                infor8['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.sdt_chinhanh)
                infor8['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.congtac_nhanvien:
                    if congtac <= 6:
                        infor8['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor8['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor8['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor8['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.bangcap_nhanvien)
                infor8['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.congviec_nhanvien_chidaothuctap)
                infor8['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.kinhnghiem_nhanvien_chidaothuctap)
                infor8['b_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.congviec_nhanvien_chidaothuctap_2)
                infor8['b_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.kinhnghiem_nhanvien_chidaothuctap_2)
                infor8['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.nam_kinhnghiem_quanly_nhanvien)
                infor8['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.gtinh_nhanvien == u'Nam':
                    infor8['hhjp_0065_1'] = '■'
                    infor8['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.gtinh_nhanvien == u'Nữ':
                    infor8['hhjp_0065_1'] = '□'
                    infor8['hhjp_0065_2'] = '■'
                else:
                    infor8['hhjp_0065_1'] = '□'
                    infor8['hhjp_0065_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    infor8['a_0077_1'] = '■'
                    infor8['a_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    infor8['a_0077_1'] = '□'
                    infor8['a_0077_2'] = '■'
                else:
                    infor8['a_0077_1'] = '□'
                    infor8['a_0077_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.thuongtruc_nhanvien_chidaothuctap_2 == u'Có':
                    infor8['b_0077_1'] = '■'
                    infor8['b_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.thuongtruc_nhanvien_chidaothuctap_2 == u'Không':
                    infor8['b_0077_1'] = '□'
                    infor8['b_0077_2'] = '■'
                else:
                    infor8['b_0077_1'] = '□'
                    infor8['b_0077_2'] = '□'
                table_chidaott.append(infor8)

            # Nhân viên 9
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.ten_han_nhanvien:
                infor9 = {}
                infor9['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.ten_phienam_nhanvien)
                infor9['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.ten_han_nhanvien)
                infor9['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.ngaysinh_nhanvien)
                infor9['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.sobuudien_nhanvien:
                    infor9[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor9['bs_0003'] = ''
                infor9['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.diachi_nhanvien)
                infor9['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.dienthoai_nhanvien)
                infor9['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor9['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh:
                    infor9['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                                     3:]
                else:
                    infor9['bs_0005'] = ''
                infor9['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.sdt_chinhanh)
                infor9['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.congtac_nhanvien:
                    if congtac <= 6:
                        infor9['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor9['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor9['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor9['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.bangcap_nhanvien)
                infor9['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.congviec_nhanvien_chidaothuctap)
                infor9['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.kinhnghiem_nhanvien_chidaothuctap)

                infor9['b_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.congviec_nhanvien_chidaothuctap_2)
                infor9['b_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.kinhnghiem_nhanvien_chidaothuctap_2)

                infor9['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.nam_kinhnghiem_quanly_nhanvien)
                infor9['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.gtinh_nhanvien == u'Nam':
                    infor9['hhjp_0065_1'] = '■'
                    infor9['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.gtinh_nhanvien == u'Nữ':
                    infor9['hhjp_0065_1'] = '□'
                    infor9['hhjp_0065_2'] = '■'
                else:
                    infor9['hhjp_0065_1'] = '□'
                    infor9['hhjp_0065_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    infor9['a_0077_1'] = '■'
                    infor9['a_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    infor9['a_0077_1'] = '□'
                    infor9['a_0077_2'] = '■'
                else:
                    infor9['a_0077_1'] = '□'
                    infor9['a_0077_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.thuongtruc_nhanvien_chidaothuctap_2 == u'Có':
                    infor9['b_0077_1'] = '■'
                    infor9['b_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.thuongtruc_nhanvien_chidaothuctap_2 == u'Không':
                    infor9['b_0077_1'] = '□'
                    infor9['b_0077_2'] = '■'
                else:
                    infor9['b_0077_1'] = '□'
                    infor9['b_0077_2'] = '□'
                table_chidaott.append(infor9)

            # Nhân viên 10
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.ten_han_nhanvien:
                infor10 = {}
                infor10['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.ten_phienam_nhanvien)
                infor10['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.ten_han_nhanvien)

                infor10['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.ngaysinh_nhanvien)
                infor10['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.sobuudien_nhanvien:
                    infor10[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor10['bs_0003'] = ''
                infor10['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.diachi_nhanvien)
                infor10['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.dienthoai_nhanvien)
                infor10['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor10['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh:
                    infor10['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                         0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                                      3:]
                else:
                    infor10['bs_0005'] = ''
                infor10['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.sdt_chinhanh)
                infor10['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.congtac_nhanvien:
                    if congtac <= 6:
                        infor10['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor10['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor10['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor10['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.bangcap_nhanvien)
                infor10['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.congviec_nhanvien_chidaothuctap)
                infor10['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.kinhnghiem_nhanvien_chidaothuctap)
                infor10['b_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.congviec_nhanvien_chidaothuctap_2)
                infor10['b_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.kinhnghiem_nhanvien_chidaothuctap_2)
                infor10['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.nam_kinhnghiem_quanly_nhanvien)
                infor10['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.gtinh_nhanvien == u'Nam':
                    infor10['hhjp_0065_1'] = '■'
                    infor10['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.gtinh_nhanvien == u'Nữ':
                    infor10['hhjp_0065_1'] = '□'
                    infor10['hhjp_0065_2'] = '■'
                else:
                    infor10['hhjp_0065_1'] = '□'
                    infor10['hhjp_0065_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    infor10['a_0077_1'] = '■'
                    infor10['a_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    infor10['a_0077_1'] = '□'
                    infor10['a_0077_2'] = '■'
                else:
                    infor10['a_0077_1'] = '□'
                    infor10['a_0077_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.thuongtruc_nhanvien_chidaothuctap_2 == u'Có':
                    infor10['b_0077_1'] = '■'
                    infor10['b_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.thuongtruc_nhanvien_chidaothuctap_2 == u'Không':
                    infor10['b_0077_1'] = '□'
                    infor10['b_0077_2'] = '■'
                else:
                    infor10['b_0077_1'] = '□'
                    infor10['b_0077_2'] = '□'
                table_chidaott.append(infor10)

            # Nhân viên 11
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.ten_han_nhanvien:
                infor11 = {}
                infor11['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.ten_phienam_nhanvien)
                infor11['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.ten_han_nhanvien)
                infor11['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.ngaysinh_nhanvien)
                infor11['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.sobuudien_nhanvien:
                    infor11[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor11['bs_0003'] = ''
                infor11['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.diachi_nhanvien)
                infor11['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.dienthoai_nhanvien)

                infor11['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor11['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)

                if document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh:
                    infor11['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                         0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                                      3:]
                else:
                    infor11['bs_0005'] = ''
                infor11['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.sdt_chinhanh)
                infor11['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.congtac_nhanvien:
                    if congtac <= 6:
                        infor11['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor11['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor11['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor11['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.bangcap_nhanvien)
                infor11['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.congviec_nhanvien_chidaothuctap)
                infor11['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.kinhnghiem_nhanvien_chidaothuctap)
                infor11['b_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.congviec_nhanvien_chidaothuctap_2)
                infor11['b_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.kinhnghiem_nhanvien_chidaothuctap_2)
                infor11['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.nam_kinhnghiem_quanly_nhanvien)
                infor11['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.gtinh_nhanvien == u'Nam':
                    infor11['hhjp_0065_1'] = '■'
                    infor11['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.gtinh_nhanvien == u'Nữ':
                    infor11['hhjp_0065_1'] = '□'
                    infor11['hhjp_0065_2'] = '■'
                else:
                    infor11['hhjp_0065_1'] = '□'
                    infor11['hhjp_0065_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    infor11['a_0077_1'] = '■'
                    infor11['a_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    infor11['a_0077_1'] = '□'
                    infor11['a_0077_2'] = '■'
                else:
                    infor11['a_0077_1'] = '□'
                    infor11['a_0077_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.thuongtruc_nhanvien_chidaothuctap_2 == u'Có':
                    infor11['b_0077_1'] = '■'
                    infor11['b_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.thuongtruc_nhanvien_chidaothuctap_2 == u'Không':
                    infor11['b_0077_1'] = '□'
                    infor11['b_0077_2'] = '■'
                else:
                    infor11['b_0077_1'] = '□'
                    infor11['b_0077_2'] = '□'
                table_chidaott.append(infor11)

            # Nhân viên 12
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.ten_han_nhanvien:
                infor12 = {}
                infor12['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.ten_phienam_nhanvien)
                infor12['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.ten_han_nhanvien)

                infor12['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.ngaysinh_nhanvien)
                infor12['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.sobuudien_nhanvien:
                    infor12[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor12['bs_0003'] = ''
                infor12['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.diachi_nhanvien)
                infor12['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.dienthoai_nhanvien)
                infor12['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor12['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh:
                    infor12['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                         0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                                      3:]
                else:
                    infor12['bs_0005'] = ''
                infor12['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.sdt_chinhanh)
                infor12['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.congtac_nhanvien:
                    if congtac <= 6:
                        infor12['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor12['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor12['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor12['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.bangcap_nhanvien)
                infor12['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.congviec_nhanvien_chidaothuctap)
                infor12['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.kinhnghiem_nhanvien_chidaothuctap)

                infor12['b_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.congviec_nhanvien_chidaothuctap_2)
                infor12['b_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.kinhnghiem_nhanvien_chidaothuctap_2)

                infor12['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.nam_kinhnghiem_quanly_nhanvien)
                infor12['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.gtinh_nhanvien == u'Nam':
                    infor12['hhjp_0065_1'] = '■'
                    infor12['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.gtinh_nhanvien == u'Nữ':
                    infor12['hhjp_0065_1'] = '□'
                    infor12['hhjp_0065_2'] = '■'
                else:
                    infor12['hhjp_0065_1'] = '□'
                    infor12['hhjp_0065_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    infor12['a_0077_1'] = '■'
                    infor12['a_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    infor12['a_0077_1'] = '□'
                    infor12['a_0077_2'] = '■'
                else:
                    infor12['a_0077_1'] = '□'
                    infor12['a_0077_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.thuongtruc_nhanvien_chidaothuctap_2 == u'Có':
                    infor12['b_0077_1'] = '■'
                    infor12['b_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.thuongtruc_nhanvien_chidaothuctap_2 == u'Không':
                    infor12['b_0077_1'] = '□'
                    infor12['b_0077_2'] = '■'
                else:
                    infor12['b_0077_1'] = '□'
                    infor12['b_0077_2'] = '□'
                table_chidaott.append(infor12)

            # Nhân viên 13
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.ten_han_nhanvien:
                infor13 = {}
                infor13['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.ten_phienam_nhanvien)
                infor13['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.ten_han_nhanvien)
                infor13['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.ngaysinh_nhanvien)
                infor13['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.sobuudien_nhanvien:
                    infor13[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor13['bs_0003'] = ''
                infor13['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.diachi_nhanvien)
                infor13['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.dienthoai_nhanvien)
                infor13['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor13['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh:
                    infor13['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                         0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                                      3:]
                else:
                    infor13['bs_0005'] = ''
                infor13['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.sdt_chinhanh)
                infor13['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.congtac_nhanvien:
                    if congtac <= 6:
                        infor13['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor13['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor13['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor13['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.bangcap_nhanvien)
                infor13['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.congviec_nhanvien_chidaothuctap)
                infor13['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.kinhnghiem_nhanvien_chidaothuctap)

                infor13['b_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.congviec_nhanvien_chidaothuctap_2)
                infor13['b_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.kinhnghiem_nhanvien_chidaothuctap_2)

                infor13['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.nam_kinhnghiem_quanly_nhanvien)
                infor13['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.gtinh_nhanvien == u'Nam':
                    infor13['hhjp_0065_1'] = '■'
                    infor13['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.gtinh_nhanvien == u'Nữ':
                    infor13['hhjp_0065_1'] = '□'
                    infor13['hhjp_0065_2'] = '■'
                else:
                    infor13['hhjp_0065_1'] = '□'
                    infor13['hhjp_0065_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    infor13['a_0077_1'] = '■'
                    infor13['a_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    infor13['a_0077_1'] = '□'
                    infor13['a_0077_2'] = '■'
                else:
                    infor13['a_0077_1'] = '□'
                    infor13['a_0077_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.thuongtruc_nhanvien_chidaothuctap_2 == u'Có':
                    infor13['b_0077_1'] = '■'
                    infor13['b_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.thuongtruc_nhanvien_chidaothuctap_2 == u'Không':
                    infor13['b_0077_1'] = '□'
                    infor13['b_0077_2'] = '■'
                else:
                    infor13['b_0077_1'] = '□'
                    infor13['b_0077_2'] = '□'
                table_chidaott.append(infor13)

            # Nhân viên 14
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.ten_han_nhanvien:
                infor14 = {}
                infor14['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.ten_phienam_nhanvien)
                infor14['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.ten_han_nhanvien)
                infor14['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.ngaysinh_nhanvien)
                infor14['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.sobuudien_nhanvien:
                    infor14[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor14['bs_0003'] = ''
                infor14['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.diachi_nhanvien)
                infor14['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.dienthoai_nhanvien)
                infor14['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor14['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh:
                    infor14['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                         0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                                      3:]
                else:
                    infor14['bs_0005'] = ''
                infor14['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.sdt_chinhanh)
                infor14['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.congtac_nhanvien:
                    if congtac <= 6:
                        infor14['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor14['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor14['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor14['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.bangcap_nhanvien)
                infor14['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.congviec_nhanvien_chidaothuctap)
                infor14['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.kinhnghiem_nhanvien_chidaothuctap)
                infor14['b_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.congviec_nhanvien_chidaothuctap_2)
                infor14['b_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.kinhnghiem_nhanvien_chidaothuctap_2)
                infor14['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.nam_kinhnghiem_quanly_nhanvien)
                infor14['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.gtinh_nhanvien == u'Nam':
                    infor14['hhjp_0065_1'] = '■'
                    infor14['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.gtinh_nhanvien == u'Nữ':
                    infor14['hhjp_0065_1'] = '□'
                    infor14['hhjp_0065_2'] = '■'
                else:
                    infor14['hhjp_0065_1'] = '□'
                    infor14['hhjp_0065_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    infor14['a_0077_1'] = '■'
                    infor14['a_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    infor14['a_0077_1'] = '□'
                    infor14['a_0077_2'] = '■'
                else:
                    infor14['a_0077_1'] = '□'
                    infor14['a_0077_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.thuongtruc_nhanvien_chidaothuctap_2 == u'Có':
                    infor14['b_0077_1'] = '■'
                    infor14['b_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.thuongtruc_nhanvien_chidaothuctap_2 == u'Không':
                    infor14['b_0077_1'] = '□'
                    infor14['b_0077_2'] = '■'
                else:
                    infor14['b_0077_1'] = '□'
                    infor14['b_0077_2'] = '□'
                table_chidaott.append(infor14)

            # Nhân viên 15
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.ten_han_nhanvien:
                infor15 = {}
                infor15['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.ten_phienam_nhanvien)
                infor15['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.ten_han_nhanvien)
                infor15['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.ngaysinh_nhanvien)
                infor15['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.sobuudien_nhanvien:
                    infor15[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor15['bs_0003'] = ''
                infor15['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.diachi_nhanvien)
                infor15['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.dienthoai_nhanvien)
                infor15['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor15['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh:
                    infor15['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                         0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                                      3:]
                else:
                    infor15['bs_0005'] = ''
                infor15['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.sdt_chinhanh)
                infor15['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.congtac_nhanvien:
                    if congtac <= 6:
                        infor15['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor15['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor15['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor15['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.bangcap_nhanvien)
                infor15['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.congviec_nhanvien_chidaothuctap)
                infor15['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.kinhnghiem_nhanvien_chidaothuctap)

                infor15['b_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.congviec_nhanvien_chidaothuctap_2)
                infor15['b_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.kinhnghiem_nhanvien_chidaothuctap_2)

                infor15['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.nam_kinhnghiem_quanly_nhanvien)
                infor15['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.gtinh_nhanvien == u'Nam':
                    infor15['hhjp_0065_1'] = '■'
                    infor15['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.gtinh_nhanvien == u'Nữ':
                    infor15['hhjp_0065_1'] = '□'
                    infor15['hhjp_0065_2'] = '■'
                else:
                    infor15['hhjp_0065_1'] = '□'
                    infor15['hhjp_0065_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    infor15['a_0077_1'] = '■'
                    infor15['a_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    infor15['a_0077_1'] = '□'
                    infor15['a_0077_2'] = '■'
                else:
                    infor15['a_0077_1'] = '□'
                    infor15['a_0077_2'] = '□'

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.thuongtruc_nhanvien_chidaothuctap_2 == u'Có':
                    infor15['b_0077_1'] = '■'
                    infor15['b_0077_2'] = '□'
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.thuongtruc_nhanvien_chidaothuctap_2 == u'Không':
                    infor15['b_0077_1'] = '□'
                    infor15['b_0077_2'] = '■'
                else:
                    infor15['b_0077_1'] = '□'
                    infor15['b_0077_2'] = '□'
                table_chidaott.append(infor15)
            context['chidaott'] = table_chidaott

            # Chỉ đạo đời sống
            table_chidaods = []
            # Nhân viên đời sống 1
            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.ten_phienam_nhanvien:
                infor = {}
                infor['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.ten_phienam_nhanvien)
                infor['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.ten_han_nhanvien)

                if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.gtinh_nhanvien == u'Nam':
                    infor['hhjp_0065_1'] = '■'
                    infor['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.gtinh_nhanvien == u'Nữ':
                    infor['hhjp_0065_1'] = '□'
                    infor['hhjp_0065_2'] = '■'
                else:
                    infor['hhjp_0065_1'] = '□'
                    infor['hhjp_0065_2'] = '□'

                infor['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.ngaysinh_nhanvien)
                infor['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.sobuudien_nhanvien:
                    infor[
                        'bs_0003'] = document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor['bs_0003'] = ''

                infor['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.diachi_nhanvien)
                infor['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.dienthoai_nhanvien)
                for noilamviec in document.donhang_hoso.chinhanh_xinghiep:
                    infor['hhjp_0041'] = format_hoso.kiemtra(noilamviec.ten_chinhanh_han)
                    infor['hhjp_0042'] = format_hoso.kiemtra(noilamviec.diachi_chinhanh)
                    break
                if document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh:
                    infor['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                       0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                                    3:]
                else:
                    infor['bs_0005'] = ''

                infor['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.sdt_chinhanh)

                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.congtac_nhanvien:
                    if congtac <= 6:
                        infor['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.bangcap_nhanvien)
                infor['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.nam_kinhnghiem_quanly_nhanvien)
                infor['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.dugiang_kynang_nhanvien)
                infor['b_0077_1'] = '□'
                infor['b_0077_2'] = '□'
                infor['a_0077_1'] = '□'
                infor['a_0077_2'] = '□'
                table_chidaods.append(infor)

            # Nhân viên đời sống 2
            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.ten_phienam_nhanvien:
                infor1 = {}
                infor1['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.ten_phienam_nhanvien)
                infor1['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.ten_han_nhanvien)

                if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.gtinh_nhanvien == u'Nam':
                    infor1['hhjp_0065_1'] = '■'
                    infor1['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.gtinh_nhanvien == u'Nữ':
                    infor1['hhjp_0065_1'] = '□'
                    infor1['hhjp_0065_2'] = '■'
                else:
                    infor1['hhjp_0065_1'] = '□'
                    infor1['hhjp_0065_2'] = '□'

                infor1['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.ngaysinh_nhanvien)
                infor1['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.sobuudien_nhanvien:
                    infor1[
                        'bs_0003'] = document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor1['bs_0003'] = ''

                infor1['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.diachi_nhanvien)
                infor1['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.dienthoai_nhanvien)
                for noilamviec in document.donhang_hoso.chinhanh_xinghiep:
                    infor1['hhjp_0041'] = format_hoso.kiemtra(noilamviec.ten_chinhanh_han)
                    infor1['hhjp_0042'] = format_hoso.kiemtra(noilamviec.diachi_chinhanh)
                    break
                if document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh:
                    infor1['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                                     3:]
                else:
                    infor1['bs_0005'] = ''

                infor1['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.sdt_chinhanh)

                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.congtac_nhanvien:
                    if congtac <= 6:
                        infor1['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor1['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor1['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor1['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.bangcap_nhanvien)
                infor1['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.nam_kinhnghiem_quanly_nhanvien)
                infor1['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.dugiang_kynang_nhanvien)
                infor1['b_0077_1'] = '□'
                infor1['b_0077_2'] = '□'
                infor1['a_0077_1'] = '□'
                infor1['a_0077_2'] = '□'
                table_chidaods.append(infor1)

            # Nhân viên đời sống 3
            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.ten_phienam_nhanvien:
                infor2 = {}
                infor2['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.ten_phienam_nhanvien)
                infor2['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.ten_han_nhanvien)

                if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.gtinh_nhanvien == u'Nam':
                    infor2['hhjp_0065_1'] = '■'
                    infor2['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.gtinh_nhanvien == u'Nữ':
                    infor2['hhjp_0065_1'] = '□'
                    infor2['hhjp_0065_2'] = '■'
                else:
                    infor2['hhjp_0065_1'] = '□'
                    infor2['hhjp_0065_2'] = '□'

                infor2['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.ngaysinh_nhanvien)
                infor2['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.sobuudien_nhanvien:
                    infor2[
                        'bs_0003'] = document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor2['bs_0003'] = ''

                infor2['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.diachi_nhanvien)
                infor2['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.dienthoai_nhanvien)
                for noilamviec in document.donhang_hoso.chinhanh_xinghiep_2:
                    infor2['hhjp_0041'] = format_hoso.kiemtra(noilamviec.ten_chinhanh_han)
                    infor2['hhjp_0042'] = format_hoso.kiemtra(noilamviec.diachi_chinhanh)
                    break
                if document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh:
                    infor2['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                                     3:]
                else:
                    infor2['bs_0005'] = ''

                infor2['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.sdt_chinhanh)

                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.congtac_nhanvien:
                    if congtac <= 6:
                        infor2['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor2['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor2['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor2['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.bangcap_nhanvien)
                infor2['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.nam_kinhnghiem_quanly_nhanvien)
                infor2['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.dugiang_kynang_nhanvien)
                infor2['b_0077_1'] = '□'
                infor2['b_0077_2'] = '□'
                infor2['a_0077_1'] = '□'
                infor2['a_0077_2'] = '□'
                table_chidaods.append(infor2)

            # Nhân viên đời sống 4
            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.ten_phienam_nhanvien:
                infor3 = {}
                infor3['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.ten_phienam_nhanvien)
                infor3['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.ten_han_nhanvien)

                if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.gtinh_nhanvien == u'Nam':
                    infor3['hhjp_0065_1'] = '■'
                    infor3['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.gtinh_nhanvien == u'Nữ':
                    infor3['hhjp_0065_1'] = '□'
                    infor3['hhjp_0065_2'] = '■'
                else:
                    infor3['hhjp_0065_1'] = '□'
                    infor3['hhjp_0065_2'] = '□'

                infor3['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.ngaysinh_nhanvien)
                infor3['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.sobuudien_nhanvien:
                    infor3[
                        'bs_0003'] = document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor3['bs_0003'] = ''

                infor3['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.diachi_nhanvien)
                infor3['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.dienthoai_nhanvien)
                for noilamviec in document.donhang_hoso.chinhanh_xinghiep_2:
                    infor3['hhjp_0041'] = format_hoso.kiemtra(noilamviec.ten_chinhanh_han)
                    infor3['hhjp_0042'] = format_hoso.kiemtra(noilamviec.diachi_chinhanh)
                    break
                if document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh:
                    infor3['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                                     3:]
                else:
                    infor3['bs_0005'] = ''

                infor3['hhjp_0043'] = format_hoso.kiemtra(
                    document.donhang_hoso.chinhanh_xinghiep_2.sdt_chinhanh)

                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.congtac_nhanvien:
                    if congtac <= 6:
                        infor3['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor3['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor3['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor3['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.bangcap_nhanvien)
                infor3['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.nam_kinhnghiem_quanly_nhanvien)
                infor3['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.dugiang_kynang_nhanvien)
                infor3['b_0077_1'] = '□'
                infor3['b_0077_2'] = '□'
                infor3['a_0077_1'] = '□'
                infor3['a_0077_2'] = '□'
                table_chidaods.append(infor3)

            # Nhân viên đời sống 5
            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.ten_phienam_nhanvien:
                infor4 = {}
                infor4['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.ten_phienam_nhanvien)
                infor4['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.ten_han_nhanvien)

                if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.gtinh_nhanvien == u'Nam':
                    infor4['hhjp_0065_1'] = '■'
                    infor4['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.gtinh_nhanvien == u'Nữ':
                    infor4['hhjp_0065_1'] = '□'
                    infor4['hhjp_0065_2'] = '■'
                else:
                    infor4['hhjp_0065_1'] = '□'
                    infor4['hhjp_0065_2'] = '□'

                infor4['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.ngaysinh_nhanvien)
                infor4['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.sobuudien_nhanvien:
                    infor4[
                        'bs_0003'] = document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor4['bs_0003'] = ''

                infor4['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.diachi_nhanvien)
                infor4['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.dienthoai_nhanvien)
                for noilamviec in document.donhang_hoso.chinhanh_xinghiep_3:
                    infor4['hhjp_0041'] = format_hoso.kiemtra(noilamviec.ten_chinhanh_han)
                    infor4['hhjp_0042'] = format_hoso.kiemtra(noilamviec.diachi_chinhanh)
                    break
                if document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh:
                    infor4['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                                     3:]
                else:
                    infor4['bs_0005'] = ''

                infor4['hhjp_0043'] = format_hoso.kiemtra(
                    document.donhang_hoso.chinhanh_xinghiep_3.sdt_chinhanh)

                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.congtac_nhanvien:
                    if congtac <= 6:
                        infor4['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor4['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor4['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor4['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.bangcap_nhanvien)
                infor4['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.nam_kinhnghiem_quanly_nhanvien)
                infor4['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.dugiang_kynang_nhanvien)
                infor4['b_0077_1'] = '□'
                infor4['b_0077_2'] = '□'
                infor4['a_0077_1'] = '□'
                infor4['a_0077_2'] = '□'
                table_chidaods.append(infor4)

            # Nhân viên đời sống 6
            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.ten_phienam_nhanvien:
                infor5 = {}
                infor5['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.ten_phienam_nhanvien)
                infor5['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.ten_han_nhanvien)

                if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.gtinh_nhanvien == u'Nam':
                    infor5['hhjp_0065_1'] = '■'
                    infor5['hhjp_0065_2'] = '□'
                elif document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.gtinh_nhanvien == u'Nữ':
                    infor5['hhjp_0065_1'] = '□'
                    infor5['hhjp_0065_2'] = '■'
                else:
                    infor5['hhjp_0065_1'] = '□'
                    infor5['hhjp_0065_2'] = '□'

                infor5['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.ngaysinh_nhanvien)
                infor5['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.sobuudien_nhanvien:
                    infor5[
                        'bs_0003'] = document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor5['bs_0003'] = ''

                infor5['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.diachi_nhanvien)
                infor5['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.dienthoai_nhanvien)
                for noilamviec in document.donhang_hoso.chinhanh_xinghiep_3:
                    infor5['hhjp_0041'] = format_hoso.kiemtra(noilamviec.ten_chinhanh_han)
                    infor5['hhjp_0042'] = format_hoso.kiemtra(noilamviec.diachi_chinhanh)
                    break
                if document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh:
                    infor5['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                                     3:]
                else:
                    infor5['bs_0005'] = ''

                infor5['hhjp_0043'] = format_hoso.kiemtra(
                    document.donhang_hoso.chinhanh_xinghiep_3.sdt_chinhanh)

                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.congtac_nhanvien:
                    if congtac <= 6:
                        infor5['a_39%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor5['a_40%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor5['a_75%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor5['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.bangcap_nhanvien)
                infor5['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.nam_kinhnghiem_quanly_nhanvien)
                infor5['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.dugiang_kynang_nhanvien)
                infor5['b_0077_1'] = '□'
                infor5['b_0077_2'] = '□'
                infor5['a_0077_1'] = '□'
                infor5['a_0077_2'] = '□'
                table_chidaods.append(infor5)

            context['chidaodoisong'] = table_chidaods

            context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_11(self, ma_thuctapsinh, document):
        print('11')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_11")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            # Hướng dẫn thực tập
            table_chidaott = []
            if document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.ten_han_nhanvien:
                infor1 = {}
                infor1['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor1['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                infor1['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor1['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.ten_han_nhanvien)
                infor1['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_1.chucdanh_han_nhanvien)
                table_chidaott.append(infor1)
            if document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.ten_han_nhanvien:
                infor2 = {}
                infor2['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor2['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                infor2['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor2['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.ten_han_nhanvien)
                infor2['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_2.chucdanh_han_nhanvien)
                table_chidaott.append(infor2)
            if document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.ten_han_nhanvien:
                infor3 = {}
                infor3['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor3['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                infor3['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor3['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.ten_han_nhanvien)
                infor3['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.chucdanh_han_nhanvien)
                table_chidaott.append(infor3)
            context['huongdantt'] = table_chidaott

            # Chỉ đạo thực tập
            table_cdtt = []
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.ten_han_nhanvien:
                infor1 = {}
                infor1['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor1['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                infor1['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor1['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.ten_han_nhanvien)
                infor1['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.chucdanh_han_nhanvien)
                infor1[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.loainghe_nhanvien.name_loaicongviec)
                infor1[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.congviec_nhanvien.name_congviec)
                table_cdtt.append(infor1)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.ten_han_nhanvien:
                infor2 = {}
                infor2['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor2['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                infor2['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor2['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.ten_han_nhanvien)
                infor2['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.chucdanh_han_nhanvien)
                infor2[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.loainghe_nhanvien.name_loaicongviec)
                infor2[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.congviec_nhanvien.name_congviec)
                table_cdtt.append(infor2)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.ten_han_nhanvien:
                infor3 = {}
                infor3['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor3['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                infor3['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor3['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.ten_han_nhanvien)
                infor3['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.chucdanh_han_nhanvien)
                infor3[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.loainghe_nhanvien.name_loaicongviec)
                infor3[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.congviec_nhanvien.name_congviec)
                table_cdtt.append(infor3)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.ten_han_nhanvien:
                infor4 = {}
                infor4['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor4['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                infor4['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor4['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.ten_han_nhanvien)
                infor4['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.chucdanh_han_nhanvien)
                infor4[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.loainghe_nhanvien.name_loaicongviec)
                infor4[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.congviec_nhanvien.name_congviec)
                table_chidaott.append(infor4)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.ten_han_nhanvien:
                infor5 = {}
                infor5['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor5['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                infor5['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor5['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.ten_han_nhanvien)
                infor5['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.chucdanh_han_nhanvien)
                infor5[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.loainghe_nhanvien.name_loaicongviec)
                infor5[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.congviec_nhanvien.name_congviec)
                table_cdtt.append(infor5)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.ten_han_nhanvien:
                infor6 = {}
                infor6['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor6['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                infor6['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor6['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.ten_han_nhanvien)
                infor6['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor6['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.chucdanh_han_nhanvien)

                infor6[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.loainghe_nhanvien.name_loaicongviec)
                infor6[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.congviec_nhanvien.name_congviec)
                table_cdtt.append(infor6)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.ten_han_nhanvien:
                infor7 = {}
                infor7['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor7['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                infor7['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor7['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.ten_han_nhanvien)
                infor7['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.chucdanh_han_nhanvien)
                infor7[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.loainghe_nhanvien.name_loaicongviec)
                infor7[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.congviec_nhanvien.name_congviec)
                table_cdtt.append(infor7)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.ten_han_nhanvien:
                infor8 = {}
                infor8['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor8['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                infor8['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor8['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.ten_han_nhanvien)
                infor8['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.chucdanh_han_nhanvien)

                infor8[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.loainghe_nhanvien.name_loaicongviec)
                infor8[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.congviec_nhanvien.name_congviec)
                table_cdtt.append(infor8)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.ten_han_nhanvien:
                infor9 = {}
                infor9['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor9['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                infor9['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor9['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.ten_han_nhanvien)
                infor9['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.chucdanh_han_nhanvien)

                infor9[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.loainghe_nhanvien.name_loaicongviec)
                infor9[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.congviec_nhanvien.name_congviec)
                table_cdtt.append(infor9)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.ten_han_nhanvien:
                infor10 = {}
                infor10['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor10['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                infor10['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor10['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.ten_han_nhanvien)
                infor10['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.chucdanh_han_nhanvien)

                infor10[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.loainghe_nhanvien.name_loaicongviec)
                infor10[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.congviec_nhanvien.name_congviec)
                table_cdtt.append(infor10)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.ten_han_nhanvien:
                infor11 = {}
                infor11['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor11['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                infor11['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor11['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.ten_han_nhanvien)
                infor11['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.chucdanh_han_nhanvien)

                infor11[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.loainghe_nhanvien.name_loaicongviec)
                infor11[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.congviec_nhanvien.name_congviec)
                table_cdtt.append(infor11)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.ten_han_nhanvien:
                infor12 = {}
                infor12['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor12['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                infor12['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor12['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.ten_han_nhanvien)
                infor12['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.chucdanh_han_nhanvien)

                infor12[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.loainghe_nhanvien.name_loaicongviec)
                infor12[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.congviec_nhanvien.name_congviec)
                table_cdtt.append(infor12)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.ten_han_nhanvien:
                infor13 = {}
                infor13['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor13['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                infor13['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor13['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.ten_han_nhanvien)
                infor13['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.chucdanh_han_nhanvien)

                infor13[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.loainghe_nhanvien.name_loaicongviec)
                infor13[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.congviec_nhanvien.name_congviec)
                table_cdtt.append(infor13)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.ten_han_nhanvien:
                infor14 = {}
                infor14['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor14['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                infor14['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor14['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.ten_han_nhanvien)
                infor14['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.chucdanh_han_nhanvien)

                infor14[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.loainghe_nhanvien.name_loaicongviec)
                infor14[
                    'hhjp_0116'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.congviec_nhanvien.name_congviec[
                                   2:] if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.congviec_nhanvien.name_congviec else ''
                table_cdtt.append(infor14)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.ten_han_nhanvien:
                infor15 = {}
                infor15['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor15['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                infor15['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor15['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.ten_han_nhanvien)
                infor15['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.chucdanh_han_nhanvien)

                infor15[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.loainghe_nhanvien.name_loaicongviec)
                infor15[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.congviec_nhanvien.name_congviec)
                table_cdtt.append(infor15)
            context['chidaott'] = table_cdtt

            # Chỉ đạo đời sống
            table_chidaods = []
            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.ten_han_nhanvien:
                infor1 = {}
                infor1['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor1['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                infor1['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor1['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.ten_han_nhanvien)
                infor1['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.chucdanh_han_nhanvien)
                table_chidaods.append(infor1)
            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.ten_han_nhanvien:
                infor2 = {}
                infor2['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor2['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                infor2['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor2['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.ten_han_nhanvien)
                infor2['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.chucdanh_han_nhanvien)
                table_chidaods.append(infor2)
            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.ten_han_nhanvien:
                infor3 = {}
                infor3['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor3['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                infor3['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor3['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.ten_han_nhanvien)
                infor3['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.chucdanh_han_nhanvien)
                table_chidaods.append(infor3)
            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.ten_han_nhanvien:
                infor4 = {}
                infor4['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor4['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                infor4['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor4['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.ten_han_nhanvien)
                infor4['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.chucdanh_han_nhanvien)
                table_chidaods.append(infor4)
            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.ten_han_nhanvien:
                infor5 = {}
                infor5['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor5['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                infor5['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor5['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.ten_han_nhanvien)
                infor5['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.chucdanh_han_nhanvien)
                table_chidaods.append(infor5)
            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.ten_han_nhanvien:
                infor6 = {}
                infor6['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor6['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                infor6['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor6['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.ten_han_nhanvien)
                infor6['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.chucdanh_han_nhanvien)
                table_chidaods.append(infor6)
            context['chidaodoisong'] = table_chidaods

            context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_12(self, ma_thuctapsinh, document):
        print('12')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_12")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            table_chidaott = []
            table_chidaott2 = []

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.ten_han_nhanvien:
                infor1 = {}
                infor1['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.ten_phienam_nhanvien)
                infor1['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.ten_han_nhanvien)
                infor1['hhjp_0065'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.gtinh_nhanvien)
                infor1['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.ngaysinh_nhanvien)
                infor1['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.sobuudien_nhanvien:
                    infor1[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor1['bs_0003'] = ''
                infor1['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.diachi_nhanvien)
                infor1['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.dienthoai_nhanvien)
                infor1['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor1['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh:
                    infor1['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[3:]
                else:
                    infor1['bs_0005'] = ''
                infor1['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.sdt_chinhanh)
                infor1['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.congtac_nhanvien:
                    if congtac <= 6:
                        infor1['a_39%d' % congtac] = congtac_nhanvien.nam
                        infor1['a_40%d' % congtac] = congtac_nhanvien.thang
                        infor1['a_75%d' % congtac] = congtac_nhanvien.noidung
                        congtac += 1
                    else:
                        break
                infor1['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.bangcap_nhanvien)
                infor1['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.congviec_nhanvien_chidaothuctap)
                infor1['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.kinhnghiem_nhanvien_chidaothuctap)

                infor1['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.nam_kinhnghiem_quanly_nhanvien)
                infor1['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.dugiang_kynang_nhanvien)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    table_chidaott.append(infor1)
                else:
                    table_chidaott2.append(infor1)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.ten_han_nhanvien:
                infor2 = {}
                infor2['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.ten_phienam_nhanvien)
                infor2['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.ten_han_nhanvien)
                infor2['hhjp_0065'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.gtinh_nhanvien)
                infor2['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.ngaysinh_nhanvien)
                infor2['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.sobuudien_nhanvien:
                    infor2[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor2['bs_0003'] = ''
                infor2['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.diachi_nhanvien)
                infor2['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.dienthoai_nhanvien)
                infor2['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor2['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh:
                    infor2['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[3:]
                else:
                    infor2['bs_0005'] = ''
                infor2['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.sdt_chinhanh)
                infor2['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.congtac_nhanvien:
                    if congtac <= 6:
                        infor2['a_39%d' % congtac] = congtac_nhanvien.nam
                        infor2['a_40%d' % congtac] = congtac_nhanvien.thang
                        infor2['a_75%d' % congtac] = congtac_nhanvien.noidung
                        congtac += 1
                    else:
                        break
                infor2['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.bangcap_nhanvien)
                infor2['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.congviec_nhanvien_chidaothuctap)
                infor2['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.kinhnghiem_nhanvien_chidaothuctap)
                infor2['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.nam_kinhnghiem_quanly_nhanvien)
                infor2['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    table_chidaott.append(infor2)
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    table_chidaott2.append(infor2)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.ten_han_nhanvien:
                infor3 = {}
                infor3['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.ten_phienam_nhanvien)
                infor3['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.ten_han_nhanvien)
                infor3['hhjp_0065'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.gtinh_nhanvien)
                infor3['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.ngaysinh_nhanvien)
                infor3['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.sobuudien_nhanvien:
                    infor3[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor3['bs_0003'] = ''
                infor3['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.diachi_nhanvien)
                infor3['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.dienthoai_nhanvien)
                infor3['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor3['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh:
                    infor3['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[3:]
                else:
                    infor3['bs_0005'] = ''
                infor3['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.sdt_chinhanh)
                infor3['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.congtac_nhanvien:
                    if congtac <= 6:
                        infor3['a_39%d' % congtac] = congtac_nhanvien.nam
                        infor3['a_40%d' % congtac] = congtac_nhanvien.thang
                        infor3['a_75%d' % congtac] = congtac_nhanvien.noidung
                        congtac += 1
                    else:
                        break
                infor3['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.bangcap_nhanvien)
                infor3['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.congviec_nhanvien_chidaothuctap)
                infor3['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.kinhnghiem_nhanvien_chidaothuctap)

                infor3['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.nam_kinhnghiem_quanly_nhanvien)
                infor3['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    table_chidaott.append(infor3)
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    table_chidaott2.append(infor3)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.ten_han_nhanvien:
                infor4 = {}
                infor4['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.ten_phienam_nhanvien)
                infor4['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.ten_han_nhanvien)
                infor4['hhjp_0065'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.gtinh_nhanvien)
                infor4['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.ngaysinh_nhanvien)
                infor4['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.sobuudien_nhanvien:
                    infor4[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor4['bs_0003'] = ''
                infor4['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.diachi_nhanvien)
                infor4['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.dienthoai_nhanvien)
                infor4['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor4['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh:
                    infor4['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[3:]
                else:
                    infor4['bs_0005'] = ''
                infor4['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.sdt_chinhanh)
                infor4['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.congtac_nhanvien:
                    if congtac <= 6:
                        infor4['a_39%d' % congtac] = congtac_nhanvien.nam
                        infor4['a_40%d' % congtac] = congtac_nhanvien.thang
                        infor4['a_75%d' % congtac] = congtac_nhanvien.noidung
                        congtac += 1
                    else:
                        break
                infor4['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.bangcap_nhanvien)
                infor4['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.congviec_nhanvien_chidaothuctap)
                infor4['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.kinhnghiem_nhanvien_chidaothuctap)

                infor4['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.nam_kinhnghiem_quanly_nhanvien)
                infor4['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    table_chidaott.append(infor4)
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    table_chidaott2.append(infor4)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.ten_han_nhanvien:
                infor5 = {}
                infor5['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.ten_phienam_nhanvien)
                infor5['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.ten_han_nhanvien)
                infor5['hhjp_0065'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.gtinh_nhanvien)
                infor5['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.ngaysinh_nhanvien)
                infor5['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.sobuudien_nhanvien:
                    infor5[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor5['bs_0003'] = ''
                infor5['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.diachi_nhanvien)
                infor5['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.dienthoai_nhanvien)
                infor5['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor5['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh:
                    infor5['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[3:]
                else:
                    infor5['bs_0005'] = ''
                infor5['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.sdt_chinhanh)
                infor5['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.congtac_nhanvien:
                    if congtac <= 6:
                        infor5['a_39%d' % congtac] = congtac_nhanvien.nam
                        infor5['a_40%d' % congtac] = congtac_nhanvien.thang
                        infor5['a_75%d' % congtac] = congtac_nhanvien.noidung
                        congtac += 1
                    else:
                        break
                infor5['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.bangcap_nhanvien)
                infor5['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.congviec_nhanvien_chidaothuctap)
                infor5['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.kinhnghiem_nhanvien_chidaothuctap)

                infor5['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.nam_kinhnghiem_quanly_nhanvien)
                infor5['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    table_chidaott.append(infor5)
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    table_chidaott2.append(infor5)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.ten_han_nhanvien:
                infor6 = {}
                infor6['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.ten_phienam_nhanvien)
                infor6['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.ten_han_nhanvien)
                infor6['hhjp_0065'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.gtinh_nhanvien)
                infor6['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.ngaysinh_nhanvien)
                infor6['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.sobuudien_nhanvien:
                    infor6[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor6['bs_0003'] = ''
                infor6['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.diachi_nhanvien)
                infor6['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.dienthoai_nhanvien)

                infor6['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor6['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh:
                    infor6['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[3:]
                else:
                    infor6['bs_0005'] = ''
                infor6['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.sdt_chinhanh)
                infor6['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.congtac_nhanvien:
                    if congtac <= 6:
                        infor6['a_39%d' % congtac] = congtac_nhanvien.nam
                        infor6['a_40%d' % congtac] = congtac_nhanvien.thang
                        infor6['a_75%d' % congtac] = congtac_nhanvien.noidung
                        congtac += 1
                    else:
                        break
                infor6['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.bangcap_nhanvien)
                infor6['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.congviec_nhanvien_chidaothuctap)
                infor6['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.kinhnghiem_nhanvien_chidaothuctap)

                infor6['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.nam_kinhnghiem_quanly_nhanvien)
                infor6['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    table_chidaott.append(infor6)
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    table_chidaott2.append(infor6)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.ten_han_nhanvien:
                infor7 = {}
                infor7['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.ten_phienam_nhanvien)
                infor7['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.ten_han_nhanvien)
                infor7['hhjp_0065'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.gtinh_nhanvien)
                infor7['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.ngaysinh_nhanvien)
                infor7['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.sobuudien_nhanvien:
                    infor7[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor7['bs_0003'] = ''
                infor7['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.diachi_nhanvien)
                infor7['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.dienthoai_nhanvien)
                infor7['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor7['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh:
                    infor7['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[3:]
                else:
                    infor7['bs_0005'] = ''
                infor7['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.sdt_chinhanh)
                infor7['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.congtac_nhanvien:
                    if congtac <= 6:
                        infor7['a_39%d' % congtac] = congtac_nhanvien.nam
                        infor7['a_40%d' % congtac] = congtac_nhanvien.thang
                        infor7['a_75%d' % congtac] = congtac_nhanvien.noidung
                        congtac += 1
                    else:
                        break
                infor7['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.bangcap_nhanvien)
                infor7['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.congviec_nhanvien_chidaothuctap)
                infor7['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.kinhnghiem_nhanvien_chidaothuctap)

                infor7['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.nam_kinhnghiem_quanly_nhanvien)
                infor7['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    table_chidaott.append(infor7)
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    table_chidaott2.append(infor7)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.ten_han_nhanvien:
                infor8 = {}
                infor8['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.ten_phienam_nhanvien)
                infor8['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.ten_han_nhanvien)
                infor8['hhjp_0065'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.gtinh_nhanvien)
                infor8['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.ngaysinh_nhanvien)
                infor8['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.sobuudien_nhanvien:
                    infor8[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor8['bs_0003'] = ''
                infor8['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.diachi_nhanvien)
                infor8['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.dienthoai_nhanvien)
                infor8['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor8['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh:
                    infor8['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                                     3:]
                else:
                    infor8['bs_0005'] = ''
                infor8['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.sdt_chinhanh)
                infor8['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.congtac_nhanvien:
                    if congtac <= 6:
                        infor8['a_39%d' % congtac] = congtac_nhanvien.nam
                        infor8['a_40%d' % congtac] = congtac_nhanvien.thang
                        infor8['a_75%d' % congtac] = congtac_nhanvien.noidung
                        congtac += 1
                    else:
                        break
                infor8['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.bangcap_nhanvien)
                infor8['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.congviec_nhanvien_chidaothuctap)
                infor8['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.kinhnghiem_nhanvien_chidaothuctap)

                infor8['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.nam_kinhnghiem_quanly_nhanvien)
                infor8['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    table_chidaott.append(infor8)
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    table_chidaott2.append(infor8)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.ten_han_nhanvien:
                infor9 = {}
                infor9['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.ten_phienam_nhanvien)
                infor9['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.ten_han_nhanvien)
                infor9['hhjp_0065'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.gtinh_nhanvien)
                infor9['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.ngaysinh_nhanvien)
                infor9['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.sobuudien_nhanvien:
                    infor9[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor9['bs_0003'] = ''
                infor9['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.diachi_nhanvien)
                infor9['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.dienthoai_nhanvien)
                infor9['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor9['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh:
                    infor9['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                                     3:]
                else:
                    infor9['bs_0005'] = ''
                infor9['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.sdt_chinhanh)
                infor9['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.congtac_nhanvien:
                    if congtac <= 6:
                        infor9['a_39%d' % congtac] = congtac_nhanvien.nam
                        infor9['a_40%d' % congtac] = congtac_nhanvien.thang
                        infor9['a_75%d' % congtac] = congtac_nhanvien.noidung
                        congtac += 1
                    else:
                        break
                infor9['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.bangcap_nhanvien)
                infor9['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.congviec_nhanvien_chidaothuctap)
                infor9['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.kinhnghiem_nhanvien_chidaothuctap)

                infor9['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.nam_kinhnghiem_quanly_nhanvien)
                infor9['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    table_chidaott.append(infor9)
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    table_chidaott2.append(infor9)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.ten_han_nhanvien:
                infor10 = {}
                infor10['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.ten_phienam_nhanvien)
                infor10['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.ten_han_nhanvien)
                infor10['hhjp_0065'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.gtinh_nhanvien)
                infor10['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.ngaysinh_nhanvien)
                infor10['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.sobuudien_nhanvien:
                    infor10[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor10['bs_0003'] = ''
                infor10['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.diachi_nhanvien)
                infor10['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.dienthoai_nhanvien)
                infor10['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor10['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh:
                    infor10['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                         0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                                      3:]
                else:
                    infor10['bs_0005'] = ''
                infor10['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.sdt_chinhanh)
                infor10['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.congtac_nhanvien:
                    if congtac <= 6:
                        infor10['a_39%d' % congtac] = congtac_nhanvien.nam
                        infor10['a_40%d' % congtac] = congtac_nhanvien.thang
                        infor10['a_75%d' % congtac] = congtac_nhanvien.noidung
                        congtac += 1
                    else:
                        break
                infor10['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.bangcap_nhanvien)
                infor10['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.congviec_nhanvien_chidaothuctap)
                infor10['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.kinhnghiem_nhanvien_chidaothuctap)

                infor10['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.nam_kinhnghiem_quanly_nhanvien)
                infor10['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    table_chidaott.append(infor10)
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    table_chidaott2.append(infor10)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.ten_han_nhanvien:
                infor11 = {}
                infor11['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.ten_phienam_nhanvien)
                infor11['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.ten_han_nhanvien)
                infor11['hhjp_0065'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.gtinh_nhanvien)
                infor11['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.ngaysinh_nhanvien)
                infor11['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.sobuudien_nhanvien:
                    infor11[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor11['bs_0003'] = ''
                infor11['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.diachi_nhanvien)
                infor11['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.dienthoai_nhanvien)

                infor11['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor11['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)

                if document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh:
                    infor11['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                         0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                                      3:]
                else:
                    infor11['bs_0005'] = ''
                infor11['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.sdt_chinhanh)
                infor11['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.congtac_nhanvien:
                    if congtac <= 6:
                        infor11['a_39%d' % congtac] = congtac_nhanvien.nam
                        infor11['a_40%d' % congtac] = congtac_nhanvien.thang
                        infor11['a_75%d' % congtac] = congtac_nhanvien.noidung
                        congtac += 1
                    else:
                        break
                infor11['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.bangcap_nhanvien)
                infor11['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.congviec_nhanvien_chidaothuctap)
                infor11['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.kinhnghiem_nhanvien_chidaothuctap)

                infor11['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.nam_kinhnghiem_quanly_nhanvien)
                infor11['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    table_chidaott.append(infor11)
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    table_chidaott2.append(infor11)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.ten_han_nhanvien:
                infor12 = {}
                infor12['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.ten_phienam_nhanvien)
                infor12['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.ten_han_nhanvien)
                infor12['hhjp_0065'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.gtinh_nhanvien)
                infor12['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.ngaysinh_nhanvien)
                infor12['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.sobuudien_nhanvien:
                    infor12[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor12['bs_0003'] = ''
                infor12['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.diachi_nhanvien)
                infor12['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.dienthoai_nhanvien)
                infor12['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor12['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh:
                    infor12['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                         0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                                      3:]
                else:
                    infor12['bs_0005'] = ''
                infor12['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.sdt_chinhanh)
                infor12['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.congtac_nhanvien:
                    if congtac <= 6:
                        infor12['a_39%d' % congtac] = congtac_nhanvien.nam
                        infor12['a_40%d' % congtac] = congtac_nhanvien.thang
                        infor12['a_75%d' % congtac] = congtac_nhanvien.noidung
                        congtac += 1
                    else:
                        break
                infor12['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.bangcap_nhanvien)
                infor12['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.congviec_nhanvien_chidaothuctap)
                infor12['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.kinhnghiem_nhanvien_chidaothuctap)

                infor12['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.nam_kinhnghiem_quanly_nhanvien)
                infor12['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    table_chidaott.append(infor12)
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    table_chidaott2.append(infor12)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.ten_han_nhanvien:
                infor13 = {}
                infor13['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.ten_phienam_nhanvien)
                infor13['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.ten_han_nhanvien)
                infor13['hhjp_0065'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.gtinh_nhanvien)
                infor13['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.ngaysinh_nhanvien)
                infor13['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.sobuudien_nhanvien:
                    infor13[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor13['bs_0003'] = ''
                infor13['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.diachi_nhanvien)
                infor13['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.dienthoai_nhanvien)
                infor13['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor13['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh:
                    infor13['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                         0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                                      3:]
                else:
                    infor13['bs_0005'] = ''
                infor13['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.sdt_chinhanh)
                infor13['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.congtac_nhanvien:
                    if congtac <= 6:
                        infor13['a_39%d' % congtac] = congtac_nhanvien.nam
                        infor13['a_40%d' % congtac] = congtac_nhanvien.thang
                        infor13['a_75%d' % congtac] = congtac_nhanvien.noidung
                        congtac += 1
                    else:
                        break
                infor13['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.bangcap_nhanvien)
                infor13['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.congviec_nhanvien_chidaothuctap)
                infor13['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.kinhnghiem_nhanvien_chidaothuctap)

                infor13['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.nam_kinhnghiem_quanly_nhanvien)
                infor13['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    table_chidaott.append(infor13)
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    table_chidaott2.append(infor13)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.ten_han_nhanvien:
                infor14 = {}
                infor14['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.ten_phienam_nhanvien)
                infor14['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.ten_han_nhanvien)
                infor14['hhjp_0065'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.gtinh_nhanvien)
                infor14['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.ngaysinh_nhanvien)
                infor14['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.sobuudien_nhanvien:
                    infor14[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor14['bs_0003'] = ''
                infor14['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.diachi_nhanvien)
                infor14['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.dienthoai_nhanvien)
                infor14['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor14['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh:
                    infor14['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                         0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                                      3:]
                else:
                    infor14['bs_0005'] = ''
                infor14['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.sdt_chinhanh)
                infor14['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.congtac_nhanvien:
                    if congtac <= 6:
                        infor14['a_39%d' % congtac] = congtac_nhanvien.nam
                        infor14['a_40%d' % congtac] = congtac_nhanvien.thang
                        infor14['a_75%d' % congtac] = congtac_nhanvien.noidung
                        congtac += 1
                    else:
                        break
                infor14['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.bangcap_nhanvien)
                infor14['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.congviec_nhanvien_chidaothuctap)
                infor14['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.kinhnghiem_nhanvien_chidaothuctap)

                infor14['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.nam_kinhnghiem_quanly_nhanvien)
                infor14['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    table_chidaott.append(infor14)
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    table_chidaott2.append(infor14)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.ten_han_nhanvien:
                infor15 = {}
                infor15['hhjp_0063'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.ten_phienam_nhanvien)
                infor15['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.ten_han_nhanvien)
                infor15['hhjp_0065'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.gtinh_nhanvien)
                infor15['hhjp_0066'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.ngaysinh_nhanvien)
                infor15['hhjp_0067'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.sobuudien_nhanvien:
                    infor15[
                        'bs_0003'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor15['bs_0003'] = ''
                infor15['hhjp_0068'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.diachi_nhanvien)
                infor15['hhjp_0069'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.dienthoai_nhanvien)
                infor15['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor15['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh:
                    infor15['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                         0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                                      3:]
                else:
                    infor15['bs_0005'] = ''
                infor15['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.sdt_chinhanh)
                infor15['hhjp_0074'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.congtac_nhanvien:
                    if congtac <= 6:
                        infor15['a_39%d' % congtac] = congtac_nhanvien.nam
                        infor15['a_40%d' % congtac] = congtac_nhanvien.thang
                        infor15['a_75%d' % congtac] = congtac_nhanvien.noidung
                        congtac += 1
                    else:
                        break
                infor15['hhjp_0076'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.bangcap_nhanvien)
                infor15['a_0115'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.congviec_nhanvien_chidaothuctap)
                infor15['a_0077'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.kinhnghiem_nhanvien_chidaothuctap)

                infor15['hhjp_0078'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.nam_kinhnghiem_quanly_nhanvien)
                infor15['hhjp_0079'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.dugiang_kynang_nhanvien)

                if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.thuongtruc_nhanvien_chidaothuctap == u'Có':
                    table_chidaott.append(infor15)
                elif document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.thuongtruc_nhanvien_chidaothuctap == u'Không':
                    table_chidaott2.append(infor15)

            context['chidaott1'] = table_chidaott
            context['chidaott2'] = table_chidaott2

            if table_chidaott == [] and table_chidaott2 == []:
                infor16 = {}
                infor16['hhjp_0079'] = ''
                table_chidaott.append(infor16)
                context['chidaott1'] = table_chidaott

            context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_13(self, ma_thuctapsinh, document):
        print('13')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_13")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            table_chidaott = []
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.ten_han_nhanvien:
                infor1 = {}
                infor1['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor1['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                infor1['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor1['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.ten_han_nhanvien)
                infor1['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.chucdanh_han_nhanvien)
                infor1['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor1[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.loainghe_nhanvien.name_loaicongviec)
                infor1[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_11.congviec_nhanvien.name_congviec)
                table_chidaott.append(infor1)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.ten_han_nhanvien:
                infor2 = {}
                infor2['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor2['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                infor2['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor2['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.ten_han_nhanvien)
                infor2['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.chucdanh_han_nhanvien)

                infor2[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.loainghe_nhanvien.name_loaicongviec)
                infor2[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_12.congviec_nhanvien.name_congviec)
                infor2['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                table_chidaott.append(infor2)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.ten_han_nhanvien:
                infor3 = {}
                infor3['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor3['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                infor3['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor3['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.ten_han_nhanvien)
                infor3['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor3['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.chucdanh_han_nhanvien)

                infor3[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.loainghe_nhanvien.name_loaicongviec)
                infor3[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_13.congviec_nhanvien.name_congviec)
                table_chidaott.append(infor3)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.ten_han_nhanvien:
                infor4 = {}
                infor4['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor4['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                infor4['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor4['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.ten_han_nhanvien)
                infor4['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor4['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.chucdanh_han_nhanvien)

                infor4[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.loainghe_nhanvien.name_loaicongviec)
                infor4[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_14.congviec_nhanvien.name_congviec)
                table_chidaott.append(infor4)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.ten_han_nhanvien:
                infor5 = {}
                infor5['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor5['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                infor5['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor5['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.ten_han_nhanvien)
                infor5['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor5['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.chucdanh_han_nhanvien)

                infor5[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.loainghe_nhanvien.name_loaicongviec)
                infor5[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_15.congviec_nhanvien.name_congviec)
                table_chidaott.append(infor5)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.ten_han_nhanvien:
                infor6 = {}
                infor6['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor6['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                infor6['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor6['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.ten_han_nhanvien)
                infor6['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor6['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.chucdanh_han_nhanvien)

                infor6[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.loainghe_nhanvien.name_loaicongviec)
                infor6[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_21.congviec_nhanvien.name_congviec)
                table_chidaott.append(infor6)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.ten_han_nhanvien:
                infor7 = {}
                infor7['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor7['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                infor7['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor7['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.ten_han_nhanvien)
                infor7['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor7['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.chucdanh_han_nhanvien)

                infor7[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.loainghe_nhanvien.name_loaicongviec)
                infor7[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_22.congviec_nhanvien.name_congviec)
                table_chidaott.append(infor7)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.ten_han_nhanvien:
                infor8 = {}
                infor8['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor8['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                infor8['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor8['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.ten_han_nhanvien)
                infor8['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor8['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.chucdanh_han_nhanvien)

                infor8[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.loainghe_nhanvien.name_loaicongviec)
                infor8[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_23.congviec_nhanvien.name_congviec)
                table_chidaott.append(infor8)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.ten_han_nhanvien:
                infor9 = {}
                infor9['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor9['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                infor9['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor9['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.ten_han_nhanvien)
                infor9['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor9['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.chucdanh_han_nhanvien)

                infor9[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.loainghe_nhanvien.name_loaicongviec)
                infor9[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_24.congviec_nhanvien.name_congviec)
                table_chidaott.append(infor9)

            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.ten_han_nhanvien:
                infor10 = {}
                infor10['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor10['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                infor10['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor10['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.ten_han_nhanvien)
                infor10['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor10['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.chucdanh_han_nhanvien)

                infor10[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.loainghe_nhanvien.name_loaicongviec)
                infor10[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_25.congviec_nhanvien.name_congviec)
                table_chidaott.append(infor10)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.ten_han_nhanvien:
                infor11 = {}
                infor11['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor11['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                infor11['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor11['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.ten_han_nhanvien)
                infor11['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor11['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.chucdanh_han_nhanvien)

                infor11[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.loainghe_nhanvien.name_loaicongviec)
                infor11[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_31.congviec_nhanvien.name_congviec)
                table_chidaott.append(infor11)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.ten_han_nhanvien:
                infor12 = {}
                infor12['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor12['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                infor12['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor12['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.ten_han_nhanvien)
                infor12['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor12['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.chucdanh_han_nhanvien)

                infor12[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.loainghe_nhanvien.name_loaicongviec)
                infor12[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_32.congviec_nhanvien.name_congviec)
                table_chidaott.append(infor12)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.ten_han_nhanvien:
                infor13 = {}
                infor13['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor13['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                infor13['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor13['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.ten_han_nhanvien)
                infor13['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor13['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.chucdanh_han_nhanvien)

                infor13[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.loainghe_nhanvien.name_loaicongviec)
                infor13[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_33.congviec_nhanvien.name_congviec)
                table_chidaott.append(infor13)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.ten_han_nhanvien:
                infor14 = {}
                infor14['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor14['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                infor14['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor14['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.ten_han_nhanvien)
                infor14['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor14['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.chucdanh_han_nhanvien)

                infor14[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.loainghe_nhanvien.name_loaicongviec)
                infor14[
                    'hhjp_0116'] = document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.congviec_nhanvien.name_congviec[
                                   2:] if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_34.congviec_nhanvien.name_congviec else ''
                table_chidaott.append(infor14)
            if document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.ten_han_nhanvien:
                infor15 = {}
                infor15['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor15['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                infor15['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor15['hhjp_0064'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.ten_han_nhanvien)
                infor15['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor15['hhjp_0064_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.chucdanh_han_nhanvien)

                infor15[
                    'hhjp_0115'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.loainghe_nhanvien.name_loaicongviec)
                infor15[
                    'hhjp_0116'] = format_hoso.nganhnghe(
                    document.donhang_hoso.nhanvien_thuctap_chinhanh_xinghiep_35.congviec_nhanvien.name_congviec)
                table_chidaott.append(infor15)

            if table_chidaott:
                context['chidaott'] = table_chidaott
            else:
                infor16 = {}
                infor16['hhjp_0116'] = ''
                table_chidaott.append(infor16)
                context['chidaott'] = table_chidaott

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_14(self, ma_thuctapsinh, document):
        print('14')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_14")], limit=1)

        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            table_chidaods = []
            # FILE_nhanvien_doisonh_chinhanh_xinghiep_11
            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.ten_phienam_nhanvien:
                infor = {}
                infor['hhjp_0080'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.ten_phienam_nhanvien)
                infor['hhjp_0081'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.ten_han_nhanvien)
                infor['hhjp_0082'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.gtinh_nhanvien)
                infor['hhjp_0083'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.ngaysinh_nhanvien)
                infor['hhjp_0084'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.sobuudien_nhanvien:
                    infor[
                        'bs_0003'] = document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor['bs_0003'] = ''

                infor['hhjp_0085'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.diachi_nhanvien)
                infor['hhjp_0086'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.dienthoai_nhanvien)
                for noilamviec in document.donhang_hoso.chinhanh_xinghiep:
                    infor['hhjp_0041'] = format_hoso.kiemtra(noilamviec.ten_chinhanh_han)
                    infor['hhjp_0042'] = format_hoso.kiemtra(noilamviec.diachi_chinhanh)
                    break
                if document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh:
                    infor['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                       0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                                    3:]
                else:
                    infor['bs_0005'] = ''

                infor['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.sdt_chinhanh)
                infor['hhjp_0091'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.chucdanh_han_nhanvien)

                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.congtac_nhanvien:
                    if congtac <= 6:
                        infor['bs_0041_%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor['bs_0042_%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor['hhjp_0092_%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor['hhjp_0093'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.bangcap_nhanvien)
                infor['hhjp_0094'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.nam_kinhnghiem_quanly_nhanvien)
                infor['hhjp_0095'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.dugiang_kynang_nhanvien)
                infor['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                table_chidaods.append(infor)

            # FILE_nhanvien_doisonh_chinhanh_xinghiep_12
            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.ten_phienam_nhanvien:
                infor1 = {}
                infor1['hhjp_0080'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.ten_phienam_nhanvien)
                infor1['hhjp_0081'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.ten_han_nhanvien)
                infor1['hhjp_0082'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.gtinh_nhanvien)
                infor1['hhjp_0083'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.ngaysinh_nhanvien)
                infor1['hhjp_0084'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.sobuudien_nhanvien:
                    infor1[
                        'bs_0003'] = document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor1['bs_0003'] = ''

                infor1['hhjp_0085'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.diachi_nhanvien)
                infor1['hhjp_0086'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.dienthoai_nhanvien)
                for noilamviec in document.donhang_hoso.chinhanh_xinghiep:
                    infor1['hhjp_0041'] = format_hoso.kiemtra(noilamviec.ten_chinhanh_han)
                    infor1['hhjp_0042'] = format_hoso.kiemtra(noilamviec.diachi_chinhanh)
                    break
                if document.donhang_hoso.chinhanh_xinghiep:
                    infor1['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep.sobuudien_chinhanh[
                                                     3:]
                else:
                    infor1['bs_0005'] = ''
                infor1['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.sdt_chinhanh)
                infor1['hhjp_0091'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.congtac_nhanvien:
                    if congtac <= 6:
                        infor1['bs_0041_%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor1['bs_0042_%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor1['hhjp_0092_%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor1['hhjp_0093'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.bangcap_nhanvien)
                infor1['hhjp_0094'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.nam_kinhnghiem_quanly_nhanvien)
                infor1['hhjp_0095'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.dugiang_kynang_nhanvien)
                infor1['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor1['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                table_chidaods.append(infor1)

            # FILE_nhanvien_doisonh_chinhanh_xinghiep_21
            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.ten_phienam_nhanvien:
                infor2 = {}
                infor2['hhjp_0080'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.ten_phienam_nhanvien)
                infor2['hhjp_0081'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.ten_han_nhanvien)
                infor2['hhjp_0082'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.gtinh_nhanvien)
                infor2['hhjp_0083'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.ngaysinh_nhanvien)
                infor2['hhjp_0084'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.sobuudien_nhanvien:
                    infor2[
                        'bs_0003'] = document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor2['bs_0003'] = ''

                infor2['hhjp_0085'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.diachi_nhanvien)
                infor2['hhjp_0086'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.dienthoai_nhanvien)
                infor2['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor2['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                if document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh:
                    infor2['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                                     3:]
                else:
                    infor2['bs_0005'] = ''
                infor2['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.sdt_chinhanh)
                infor2['hhjp_0091'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.chucdanh_han_nhanvien)
                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.congtac_nhanvien:
                    if congtac <= 6:
                        infor2['bs_0041_%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor2['bs_0042_%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor2['hhjp_0092_%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor2['hhjp_0093'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.bangcap_nhanvien)
                infor2['hhjp_0094'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.nam_kinhnghiem_quanly_nhanvien)
                infor2['hhjp_0095'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.dugiang_kynang_nhanvien)
                infor2['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor2['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                table_chidaods.append(infor2)

            # FILE_nhanvien_doisonh_chinhanh_xinghiep_22
            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.ten_phienam_nhanvien:
                infor3 = {}
                infor3['hhjp_0080'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.ten_phienam_nhanvien)
                infor3['hhjp_0081'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.ten_han_nhanvien)
                infor3['hhjp_0082'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.gtinh_nhanvien)
                infor3['hhjp_0083'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.ngaysinh_nhanvien)
                infor3['hhjp_0084'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.sobuudien_nhanvien:
                    infor3[
                        'bs_0003'] = document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor3['bs_0003'] = ''

                infor3['hhjp_0085'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.diachi_nhanvien)
                infor3['hhjp_0086'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.dienthoai_nhanvien)
                infor3['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor3['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)

                if document.donhang_hoso.chinhanh_xinghiep_2:
                    infor3['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_2.sobuudien_chinhanh[
                                                     3:]
                else:
                    infor3['bs_0005'] = ''

                infor3['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.sdt_chinhanh)
                infor3['hhjp_0091'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.chucdanh_han_nhanvien)

                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.congtac_nhanvien:
                    if congtac <= 6:
                        infor3['bs_0041_%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor3['bs_0042_%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor3['hhjp_0092_%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor3['hhjp_0093'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.bangcap_nhanvien)
                infor3['hhjp_0094'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.nam_kinhnghiem_quanly_nhanvien)
                infor3['hhjp_0095'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.dugiang_kynang_nhanvien)
                infor3['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor3['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                table_chidaods.append(infor3)

            # FILE_nhanvien_doisonh_chinhanh_xinghiep_31
            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.ten_phienam_nhanvien:
                infor4 = {}
                infor4['hhjp_0080'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.ten_phienam_nhanvien)
                infor4['hhjp_0081'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.ten_han_nhanvien)
                infor4['hhjp_0082'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.gtinh_nhanvien)
                infor4['hhjp_0083'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.ngaysinh_nhanvien)
                infor4['hhjp_0084'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.sobuudien_nhanvien:
                    infor4[
                        'bs_0003'] = document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor4['bs_0003'] = ''
                infor4['hhjp_0085'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.diachi_nhanvien)
                infor4['hhjp_0086'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.dienthoai_nhanvien)
                infor4['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor4['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)

                if document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh:
                    infor4['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                                     3:]
                else:
                    infor4['bs_0005'] = ''

                infor4['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.sdt_chinhanh)
                infor4['hhjp_0091'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.chucdanh_han_nhanvien)

                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.congtac_nhanvien:
                    if congtac <= 6:
                        infor4['bs_0041_%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor4['bs_0042_%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor4['hhjp_0092_%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor4['hhjp_0093'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.bangcap_nhanvien)
                infor4['hhjp_0094'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.nam_kinhnghiem_quanly_nhanvien)
                infor4['hhjp_0095'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.dugiang_kynang_nhanvien)
                infor4['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor4['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                table_chidaods.append(infor4)

            # FILE_nhanvien_doisonh_chinhanh_xinghiep_32
            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.ten_phienam_nhanvien:
                infor5 = {}
                infor5['hhjp_0080'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.ten_phienam_nhanvien)
                infor5['hhjp_0081'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.ten_han_nhanvien)
                infor5['hhjp_0082'] = format_hoso.gender_check(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.gtinh_nhanvien)
                infor5['hhjp_0083'] = format_hoso.convert_date(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.ngaysinh_nhanvien)
                infor5['hhjp_0084'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.quoctich_nhanvien.name_quoctich)
                if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.sobuudien_nhanvien:
                    infor5[
                        'bs_0003'] = document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.sobuudien_nhanvien[
                                     0:3] + '-' + document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.sobuudien_nhanvien[
                                                  3:]
                else:
                    infor5['bs_0003'] = ''

                infor5['hhjp_0085'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.diachi_nhanvien)
                infor5['hhjp_0086'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.dienthoai_nhanvien)
                infor5['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor5['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)

                if document.donhang_hoso.chinhanh_xinghiep_3:
                    infor5['bs_0005'] = document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                        0:3] + '-' + document.donhang_hoso.chinhanh_xinghiep_3.sobuudien_chinhanh[
                                                     3:]
                else:
                    infor5['bs_0005'] = ''

                infor5['hhjp_0043'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.sdt_chinhanh)
                infor5['hhjp_0091'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.chucdanh_han_nhanvien)

                congtac = 1
                for congtac_nhanvien in document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.congtac_nhanvien:
                    if congtac <= 6:
                        infor5['bs_0041_%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.nam)
                        infor5['bs_0042_%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.thang)
                        infor5['hhjp_0092_%d' % congtac] = format_hoso.kiemtra(congtac_nhanvien.noidung)
                        congtac += 1
                    else:
                        break
                infor5['hhjp_0093'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.bangcap_nhanvien)
                infor5['hhjp_0094'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.nam_kinhnghiem_quanly_nhanvien)
                infor5['hhjp_0095'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.dugiang_kynang_nhanvien)
                infor5['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                infor5['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                table_chidaods.append(infor5)

            if table_chidaods:
                context['chidaods'] = table_chidaods
            else:
                infor6 = {}
                infor6['hhjp_0010'] = ''
                table_chidaods.append(infor6)
                context['chidaods'] = table_chidaods

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_15(self, ma_thuctapsinh, document):
        print('15')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_15")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            table_chidaods = []
            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.ten_han_nhanvien:
                infor1 = {}
                infor1['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor1['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                infor1['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor1['hhjp_0081'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.ten_han_nhanvien)
                infor1['hhjp_0081_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_11.chucdanh_han_nhanvien)
                infor1['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                table_chidaods.append(infor1)

            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.ten_han_nhanvien:
                infor2 = {}
                infor2['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.ten_chinhanh_han)
                infor2['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep.diachi_chinhanh)
                infor2['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor2['hhjp_0081'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.ten_han_nhanvien)
                infor2['hhjp_0081_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_12.chucdanh_han_nhanvien)
                infor2['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                table_chidaods.append(infor2)

            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.ten_han_nhanvien:
                infor3 = {}
                infor3['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor3['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                infor3['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor3['hhjp_0081'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.ten_han_nhanvien)
                infor3['hhjp_0081_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_21.chucdanh_han_nhanvien)
                infor3['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                table_chidaods.append(infor3)

            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_22.ten_han_nhanvien:
                infor4 = {}
                infor4['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor4['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                infor4['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor4['hhjp_0081'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.ten_han_nhanvien)
                infor4['hhjp_0081_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_trachnhiem_chinhanh_xinghiep_3.chucdanh_han_nhanvien)
                infor4['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                table_chidaods.append(infor4)

            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.ten_han_nhanvien:
                infor5 = {}
                infor5['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.ten_chinhanh_han)
                infor5['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_2.diachi_chinhanh)
                infor5['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor5['hhjp_0081'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.ten_han_nhanvien)
                infor5['hhjp_0081_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_31.chucdanh_han_nhanvien)
                infor5['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                table_chidaods.append(infor5)

            if document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.ten_han_nhanvien:
                infor6 = {}
                infor6['hhjp_0041'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.ten_chinhanh_han)
                infor6['hhjp_0042'] = format_hoso.kiemtra(document.donhang_hoso.chinhanh_xinghiep_3.diachi_chinhanh)
                infor6['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                infor6['hhjp_0081'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.ten_han_nhanvien)
                infor6['hhjp_0081_cv'] = format_hoso.kiemtra(
                    document.donhang_hoso.nhanvien_doisonh_chinhanh_xinghiep_32.chucdanh_han_nhanvien)
                infor6['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
                table_chidaods.append(infor6)

            if table_chidaods:
                context['chidaods'] = table_chidaods
            else:
                infor7 = {}
                infor7['hhjp_0041'] = ''
                table_chidaods.append(infor7)
                context['chidaods'] = table_chidaods

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_16(self, ma_thuctapsinh, document):
        print('16')

        thuctapsinh_hoso = len(document.thuctapsinh_hoso)

        if thuctapsinh_hoso <= 3:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_16")], limit=1)
        else:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_16_1")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)

            hopdonglaodong = self.env['hopdong.hopdong'].search([('thuctapsinh_hopdong', '=', thuctapsinh.id),
                                                                 ('donhang_hopdong', '=', document.donhang_hoso.id),
                                                                 ('giaidoan_hopdong', '=', document.giaidoan_hoso)],
                                                                limit=1)
            context = {}

            # context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)

            if thuctapsinh_hoso <= 3:
                i = 1
                for tts_hoso in document.thuctapsinh_hoso:
                    context['hhjp_0001_%i' % i] = format_hoso.kiemtra(tts_hoso.ten_lt_tts)
                    context['hhjp_0002_%i' % i] = format_hoso.kiemtra(tts_hoso.ten_han_tts)
                    context['hhjp_0005_%i' % i] = format_hoso.kiemtra(tts_hoso.tuoi_tts)
                    context['hhjp_0006'] = format_hoso.gender_check(tts_hoso.gtinh_tts)
                    context['hhjp_0008_%i' % i] = format_hoso.kiemtra(tts_hoso.fill)
                    i += 1
            else:
                table_tts = []
                for tts_hoso in document.thuctapsinh_hoso:
                    infor = {}
                    infor['hhjp_0001'] = format_hoso.kiemtra(tts_hoso.ten_lt_tts)
                    infor['hhjp_0002'] = format_hoso.kiemtra(tts_hoso.ten_han_tts)
                    infor['hhjp_0005'] = format_hoso.kiemtra(tts_hoso.tuoi_tts)
                    infor['hhjp_0006'] = format_hoso.gender_check(tts_hoso.gtinh_tts)
                    infor['hhjp_0008'] = format_hoso.gender_check(tts_hoso.fill)
                    # if tts_hoso.congviec_lienquan_1:
                    #     infor['hhjp_0008'] = format_hoso.kiemtra(tts_hoso.thoigian_lamviec_nam_1)
                    # elif tts_hoso.congviec_lienquan_2:
                    #     infor['hhjp_0008'] = format_hoso.kiemtra(tts_hoso.thoigian_lamviec_nam_2)
                    # elif tts_hoso.congviec_lienquan_3:
                    #     infor['hhjp_0008'] = format_hoso.kiemtra(tts_hoso.thoigian_lamviec_nam_3)
                    # elif tts_hoso.congviec_lienquan_4:
                    #     infor['hhjp_0008'] = format_hoso.kiemtra(tts_hoso.thoigian_lamviec_nam_4)
                    # elif tts_hoso.congviec_lienquan_5:
                    #     infor['hhjp_0008'] = format_hoso.kiemtra(tts_hoso.thoigian_lamviec_nam_5)
                    # else:
                    #     infor['hhjp_0008'] = ''

                    table_tts.append(infor)
                context['tbl_tts'] = table_tts

            context['hhjp_0097'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_han_nghiepdoan)
            context['hhjp_0007'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.nhiemvu_trachnhiem_tts_daingo)
            context['hhjp_0442'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.luongthang_phanthuong_hai_daingo)
            context['hhjp_0443'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.luonggio_phanthuong_hai_daingo)
            context['hhjp_0444'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.luonggio_phanthuong_khac_daingo)
            context['hhjp_0439'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.luongthang_phanthuong_mot_daingo)
            context['hhjp_0440'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.luonggio_phanthuong_mot_daingo)

            if hopdonglaodong.hinhthuc_luongthang == True:
                if document.donhang_hoso.donhang_daingo.luong_quydinh_thang_ko:
                    context['hhjp_0436'] = format_hoso.place_value(hopdonglaodong.sotien_luongthang)
                    context['hhjp_0437'] = ''
                elif document.donhang_hoso.donhang_daingo.luong_quydinh_gio_ko:
                    context['hhjp_0436'] = ''
                    context['hhjp_0437'] = format_hoso.place_value(hopdonglaodong.sotien_luongthang_tong)
                else:
                    context['hhjp_0436'] = format_hoso.place_value(hopdonglaodong.sotien_luongthang)
                    context['hhjp_0437'] = ''
            elif hopdonglaodong.hinhthuc_luonggio == True:
                if document.donhang_hoso.donhang_daingo.luong_quydinh_thang_ko:
                    context['hhjp_0436'] = format_hoso.kiemtra(hopdonglaodong.sotien_luonggio_tong)
                    context['hhjp_0437'] = ''
                elif document.donhang_hoso.donhang_daingo.luong_quydinh_gio_ko:
                    context['hhjp_0436'] = ''
                    context['hhjp_0437'] = format_hoso.kiemtra(hopdonglaodong.sotien_luonggio)
                else:
                    context['hhjp_0436'] = ''
                    context['hhjp_0437'] = format_hoso.kiemtra(hopdonglaodong.sotien_luonggio)
            elif hopdonglaodong.hinhthuc_luongngay == True:
                if document.donhang_hoso.donhang_daingo.luong_quydinh_thang_ko:
                    context['hhjp_0436'] = format_hoso.kiemtra(hopdonglaodong.tongtien_thang_luongngay_2)
                    context['hhjp_0437'] = ''
                elif document.donhang_hoso.donhang_daingo.luong_quydinh_gio_ko:
                    context['hhjp_0436'] = ''
                    context['hhjp_0437'] = format_hoso.kiemtra(hopdonglaodong.tongtien_thang_luongngay_1)
                else:
                    context['hhjp_0436'] = format_hoso.kiemtra(hopdonglaodong.tongtien_thang_luongngay_2)
                    context['hhjp_0437'] = ''
            else:
                context['hhjp_0436'] = ''
                context['hhjp_0437'] = ''

            if document.donhang_hoso.donhang_daingo.laodong_nguoinhat_sosanh == 'Có':
                context['hhjp_0178_1'] = u'■'
                context['hhjp_0178_2'] = u'□'
            elif document.donhang_hoso.donhang_daingo.laodong_nguoinhat_sosanh == 'Không':
                context['hhjp_0178_1'] = u'□'
                context['hhjp_0178_2'] = u'■'
            else:
                context['hhjp_0178_1'] = u'□'
                context['hhjp_0178_2'] = u'□'

            context['hhjp_0178'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.mucdo_trachnhiem_ss_ko)
            context['hhjp_0904'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.tuoi_ss_ko)
            # context['hhjp_0453'] = format_hoso.gender_check(document.donhang_hoso.donhang_daingo.gioitinh_ss_ko)
            context['hhjp_0446'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.namkinhnghiem_ss_ko)

            context['hhjp_0447'] = format_hoso.place_value(
                document.donhang_hoso.donhang_daingo.luong_quydinh_thang_ko)
            context['hhjp_0448'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.luong_quydinh_gio_ko)
            if document.donhang_hoso.donhang_daingo.quydinhtienluong == 'Có':
                context['hhjp_0460_1'] = u'■'
                context['hhjp_0460_2'] = u'□'
            elif document.donhang_hoso.donhang_daingo.quydinhtienluong == 'Không':
                context['hhjp_0460_1'] = u'□'
                context['hhjp_0460_2'] = u'■'
            else:
                context['hhjp_0460_1'] = u'□'
                context['hhjp_0460_2'] = u'□'

            context['hhjp_0460'] = format_hoso.place_value(document.donhang_hoso.donhang_daingo.luong_thang_ko)
            context['hhjp_0461'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.luong_quydinh_gio_ko)
            context['hhjp_0183'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.lydo_xemxet_thulao_ko)

            if document.donhang_hoso.donhang_daingo.muckhac_ko == False:
                context['hhjp_0449'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.muckhac_ss_ko)
            elif document.donhang_hoso.donhang_daingo.muckhac_ko == True:
                context['hhjp_0449'] = ''

            # Mục 1
            if document.donhang_hoso.donhang_daingo.bienphap_1_donhang_xinghiep == 'Có':
                context['nhao_0009_1'] = u'■'
                context['nhao_0009_2'] = u'□'
            elif document.donhang_hoso.donhang_daingo.bienphap_1_donhang_xinghiep == 'Không':
                context['nhao_0009_1'] = u'□'
                context['nhao_0009_2'] = u'■'
            else:
                context['nhao_0009_1'] = u'□'
                context['nhao_0009_2'] = u'□'

            # Mục 2
            if document.donhang_hoso.donhang_daingo.bienphap_2_donhang_xinghiep == 'Có':
                context['nhao_0010_1'] = u'■'
                context['nhao_0010_2'] = u'□'
            elif document.donhang_hoso.donhang_daingo.bienphap_2_donhang_xinghiep == 'Không':
                context['nhao_0010_1'] = u'□'
                context['nhao_0010_2'] = u'■'
            else:
                context['nhao_0010_1'] = u'□'
                context['nhao_0010_2'] = u'□'

            # Mục 3
            if document.donhang_hoso.donhang_daingo.bienphap_3_donhang_xinghiep == 'Có':
                context['nhao_0011_1'] = u'■'
                context['nhao_0011_2'] = u'□'
            elif document.donhang_hoso.donhang_daingo.bienphap_3_donhang_xinghiep == 'Không':
                context['nhao_0011_1'] = u'□'
                context['nhao_0011_2'] = u'■'
            else:
                context['nhao_0011_1'] = u'□'
                context['nhao_0011_2'] = u'□'

            # Mục 4
            if document.donhang_hoso.donhang_daingo.bienphap_4_donhang_xinghiep == 'Có':
                context['nhao_0012_1'] = u'■'
                context['nhao_0012_2'] = u'□'
            elif document.donhang_hoso.donhang_daingo.bienphap_4_donhang_xinghiep == 'Không':
                context['nhao_0012_1'] = u'□'
                context['nhao_0012_2'] = u'■'
            else:
                context['nhao_0012_1'] = u'□'
                context['nhao_0012_2'] = u'□'

            # Mục 5
            if document.donhang_hoso.donhang_daingo.bienphap_5_donhang_xinghiep == 'Có':
                context['nhao_0013_1'] = u'■'
                context['nhao_0013_2'] = u'□'
            elif document.donhang_hoso.donhang_daingo.bienphap_5_donhang_xinghiep == 'Không':
                context['nhao_0013_1'] = u'□'
                context['nhao_0013_2'] = u'■'
            else:
                context['nhao_0013_1'] = u'□'
                context['nhao_0013_2'] = u'□'

            # Mục 6
            if document.donhang_hoso.donhang_daingo.bienphap_6_donhang_xinghiep == 'Có':
                context['nhao_0014_1'] = u'■'
                context['nhao_0014_2'] = u'□'
            elif document.donhang_hoso.donhang_daingo.bienphap_6_donhang_xinghiep == 'Không':
                context['nhao_0014_1'] = u'□'
                context['nhao_0014_2'] = u'■'
            else:
                context['nhao_0014_1'] = u'□'
                context['nhao_0014_2'] = u'□'

            # Mục 7
            if document.donhang_hoso.donhang_daingo.bienphap_7_donhang_xinghiep == 'Có':
                context['nhao_0015_1'] = u'■'
                context['nhao_0015_2'] = u'□'
            elif document.donhang_hoso.donhang_daingo.bienphap_7_donhang_xinghiep == 'Không':
                context['nhao_0015_1'] = u'□'
                context['nhao_0015_2'] = u'■'
            else:
                context['nhao_0015_1'] = u'□'
                context['nhao_0015_2'] = u'□'

            # Mục 8
            if document.donhang_hoso.donhang_daingo.bienphap_8_donhang_xinghiep == 'Có':
                context['nhao_0016_1'] = u'■'
                context['nhao_0016_2'] = u'□'
            elif document.donhang_hoso.donhang_daingo.bienphap_8_donhang_xinghiep == 'Không':
                context['nhao_0016_1'] = u'□'
                context['nhao_0016_2'] = u'■'
            else:
                context['nhao_0016_1'] = u'□'
                context['nhao_0016_2'] = u'□'

            context['hhjp_0014'] = format_hoso.kiemtra(
                document.donhang_hoso.nghiepdoan_donhang.ten_daidien_han_nghiepdoan)
            context['nhao_0009'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.ghichu_1_donhang_xinghiep)
            context['nhao_0010'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.ghichu_2_donhang_xinghiep)
            context['nhao_0011'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.ghichu_3_donhang_xinghiep)
            context['nhao_0012'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.ghichu_4_donhang_xinghiep)
            context['nhao_0013'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.ghichu_5_donhang_xinghiep)
            context['nhao_0014'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.ghichu_6_donhang_xinghiep)
            context['nhao_0015'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.ghichu_7_donhang_xinghiep)
            context['nhao_0016'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.ghichu_8_donhang_xinghiep)

            context['hhjp_0147'] = format_hoso.place_value(
                hopdonglaodong.khoankhac_phucap_tts) if hopdonglaodong.khoankhac_phucap_tts else ''

            if document.donhang_hoso.donhang_daingo.cc_tp == u'Có':
                context['bs_0043_1'] = u'■'
                context['bs_0043_2'] = u'□'
            elif document.donhang_hoso.donhang_daingo.cc_tp == u'Không':
                context['bs_0043_1'] = u'□'
                context['bs_0043_2'] = u'■'
            else:
                context['bs_0043_1'] = u'□'
                context['bs_0043_2'] = u'□'

            # context['bs_0043'] = u'■ 有 、  □ 無' if document.donhang_hoso.donhang_daingo.cc_tp == u'Có' else u'□ 有 、  ■無'

            context['hhjp_0162'] = format_hoso.kiemtra(hopdonglaodong.sotien_khoanmuc_7)
            context['hhjp_0163'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.cungcap_thucpham_chitiet)
            context['hhjp_0164'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.giaithich_tienan)

            context['hhjp_0165'] = format_hoso.place_value(hopdonglaodong.sotien_khoanmuc_8)  # Lương tháng TTS

            if document.donhang_hoso.donhang_daingo.cungcap_coso_vatchat == 'Tài sản riêng':
                context['hhjp_0165_1'] = u'■'
                context['hhjp_0165_2'] = u'□'
            elif document.donhang_hoso.donhang_daingo.cungcap_coso_vatchat == 'Tài sản vay mượn':
                context['hhjp_0165_1'] = u'□'
                context['hhjp_0165_2'] = u'■'
            else:
                context['hhjp_0165_1'] = u'□'
                context['hhjp_0165_2'] = u'□'

            context[
                'hhjp_0167'] = document.donhang_hoso.donhang_daingo.giaithich_tiennha if document.donhang_hoso.donhang_daingo.giaithich_tiennha else ''

            if document.donhang_hoso.donhang_daingo.tp_ga == u'Có':
                context['hhjp_0168_1'] = u'■'
                context['hhjp_0168_2'] = u'□'
                context['hhjp_0169'] = format_hoso.place_value(document.donhang_hoso.donhang_daingo.thuphi_ga)
                if document.donhang_hoso.donhang_daingo.thuphi_ga_chiphithucte:
                    context['hhjp_0169_1'] = '実費'
                else:
                    context['hhjp_0169_1'] = ''
            else:
                context['hhjp_0168_1'] = u'□'
                context['hhjp_0168_2'] = u'■'
                context['hhjp_0169'] = format_hoso.place_value(document.donhang_hoso.donhang_daingo.thuphi_ga)
                if document.donhang_hoso.donhang_daingo.thuphi_ga_chiphithucte:
                    context['hhjp_0169_1'] = '実費'
                else:
                    context['hhjp_0169_1'] = ''

            if document.donhang_hoso.donhang_daingo.phikhac == u'Có':
                context['hhjp_0170_1'] = u'■'
                context['hhjp_0170_2'] = u'□'
            elif document.donhang_hoso.donhang_daingo.phikhac == u'Không':
                context['hhjp_0170_1'] = u'□'
                context['hhjp_0170_2'] = u'■'
            else:
                context['hhjp_0170_1'] = u'□'
                context['hhjp_0170_2'] = u'□'

            context['hhjp_0170'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.phithu_khac)

            if document.donhang_hoso.donhang_daingo.noidung_phi_mot:
                context['hhjp_0171'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.noidung_phi_mot)
                context['hhjp_0575'] = format_hoso.place_value(document.donhang_hoso.donhang_daingo.tienthu_mot)
            else:
                context['hhjp_0171'] = ''
                context['hhjp_0575'] = ''

            if document.donhang_hoso.donhang_daingo.noidung_phi_hai:
                context['hhjp_0576'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.noidung_phi_hai)
                context['hhjp_0177'] = format_hoso.place_value(document.donhang_hoso.donhang_daingo.tienthu_hai)
            else:
                context['hhjp_0576'] = ''
                context['hhjp_0177'] = ''

            if document.donhang_hoso.donhang_daingo.noidung_phi_ba:
                context['hhjp_0578'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.noidung_phi_ba)
                context['hhjp_0179'] = format_hoso.place_value(document.donhang_hoso.donhang_daingo.tienthu_ba)
            else:
                context['hhjp_0578'] = ''
                context['hhjp_0179'] = ''
            context['hhjp_0172'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.noidung_loiich_traphi)
            context['hhjp_0173'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.giaithich_phithu)

            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_17(self, ma_thuctapsinh, document):
        print('17')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_17")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            context = {}
            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            context['hhjp_0010_v'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_viet_xnghiep)
            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            context['hhjp_0001_v'] = format_hoso.kiemtra(thuctapsinh.ten_tv_tts)
            context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
            if document.ngay_lapvanban:
                context['hhjp_0193_v'] = 'Ngày ' + str(document.ngay_lapvanban.day) + \
                                         ' tháng ' + str(document.ngay_lapvanban.month) + \
                                         ' năm ' + str(document.ngay_lapvanban.year)
            else:
                context['hhjp_0193_v'] = 'Ngày tháng năm'
            context['hhjp_0014'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep)
            context['hhjp_0014_v'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep_v)
            context['xn_0104'] = format_hoso.kiemtra(document.xinghiep_hoso.chucvu_daidien_han)
            context['xn_0104_v'] = format_hoso.kiemtra(document.xinghiep_hoso.chucvu_daidien_viet)
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_18(self, ma_thuctapsinh, document):
        print('18')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_18")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            hopdonglaodong = self.env['hopdong.hopdong'].search(
                [('thuctapsinh_hopdong', '=', thuctapsinh.id), ('donhang_hopdong', '=', document.donhang_hoso.id),
                 ('giaidoan_hopdong', '=', document.giaidoan_hoso)],
                limit=1)
            context = {}

            if document.ngay_lapvanban != False:
                context['hhjp_0193'] = str(document.ngay_lapvanban.year) + '年' + \
                                       str(document.ngay_lapvanban.month) + '月' + \
                                       str(document.ngay_lapvanban.day) + '日'
                context['hhjp_0193_v'] = 'Ngày ' + str(document.ngay_lapvanban.day) + \
                                         ' tháng ' + str(document.ngay_lapvanban.month) + \
                                         ' năm ' + str(document.ngay_lapvanban.year)
            else:
                context['hhjp_0193'] = '年　月　日'
                context['hhjp_0193_v'] = 'Ngày tháng năm'

            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            context['hhjp_0010_v'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_viet_xnghiep)
            context['hhjp_0011'] = format_hoso.kiemtra(document.xinghiep_hoso.diachi_han_xnghiep)
            context['hhjp_0011_v'] = format_hoso.kiemtra(document.xinghiep_hoso.diachi_viet_xnghiep)
            # context['hhjp_0012'] = format_hoso.kiemtra(document.xinghiep_hoso.sdt_xnghiep)
            context['xn_0104'] = format_hoso.kiemtra(document.xinghiep_hoso.chucvu_daidien_han)
            context['xn_0104_v'] = format_hoso.kiemtra(document.xinghiep_hoso.chucvu_daidien_viet)
            context['hhjp_0014'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep)
            context['hhjp_0014_v'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep_v)

            if hopdonglaodong.batdau_tgian_congviec:
                context['hhjp_0148'] = str(hopdonglaodong.batdau_tgian_congviec.year) + '年' + \
                                       str(hopdonglaodong.batdau_tgian_congviec.month) + '月' + \
                                       str(hopdonglaodong.batdau_tgian_congviec.day) + '日'

                context['hhjp_0148_v'] = 'Ngày ' + str(hopdonglaodong.batdau_tgian_congviec.day) + \
                                         ' tháng ' + str(hopdonglaodong.batdau_tgian_congviec.month) + \
                                         ' năm ' + str(hopdonglaodong.batdau_tgian_congviec.year)
            else:
                context['hhjp_0148'] = '年　月　日'
                context['hhjp_0148_v'] = 'Ngày tháng năm'

            if hopdonglaodong.kethuc_tgian_congviec:
                context['hhjp_0380'] = str(hopdonglaodong.kethuc_tgian_congviec.year) + '年' + \
                                       str(hopdonglaodong.kethuc_tgian_congviec.month) + '月' + \
                                       str(hopdonglaodong.kethuc_tgian_congviec.day) + '日'

                context['hhjp_0380_v'] = 'Ngày ' + str(hopdonglaodong.kethuc_tgian_congviec.day) + \
                                         ' tháng ' + str(hopdonglaodong.kethuc_tgian_congviec.month) + \
                                         ' năm ' + str(hopdonglaodong.kethuc_tgian_congviec.year)
            else:
                context['hhjp_0380'] = '年　月　日'
                context['hhjp_0380_v'] = 'Ngày tháng năm'

            if hopdonglaodong.dukien_nhapcanh != False:
                context['hhjp_0902'] = str(hopdonglaodong.dukien_nhapcanh.year) + '年' + \
                                       str(hopdonglaodong.dukien_nhapcanh.month) + '月' + \
                                       str(hopdonglaodong.dukien_nhapcanh.day) + '日'
                context['hhjp_0902_v'] = 'Ngày ' + str(hopdonglaodong.dukien_nhapcanh.day) + \
                                         ' tháng ' + str(hopdonglaodong.dukien_nhapcanh.month) + \
                                         ' năm ' + str(hopdonglaodong.dukien_nhapcanh.year)
            else:
                context['hhjp_0902'] = '年　月　日'
                context['hhjp_0902_v'] = 'Ngày tháng năm'

            if hopdonglaodong.giahan_hopdong == u'Có':
                context['hhjp_0381_1'] = '□'
                context['hhjp_0381_2'] = '■'
            elif hopdonglaodong.giahan_hopdong == u'Không':
                context['hhjp_0381_1'] = '■'
                context['hhjp_0381_2'] = '□'
            else:
                context['hhjp_0381_1'] = '□'
                context['hhjp_0381_2'] = '□'

            table_noilam = []
            for ten_noilam_han in hopdonglaodong.ten_noilam_han:
                info = {}
                info['hhjp_0041'] = format_hoso.kiemtra(ten_noilam_han.ten_chinhanh_han)
                info['hhjp_0041_v'] = format_hoso.kiemtra(ten_noilam_han.ten_chinhanh_viet)

                info['hhjp_0042'] = format_hoso.kiemtra(ten_noilam_han.diachi_chinhanh)
                info['hhjp_0042_v'] = format_hoso.kiemtra(ten_noilam_han.diachi_chinhanh_viet)

                table_noilam.append(info)
            context['tbl_noilamviec'] = table_noilam

            context['hhjp_0115'] = format_hoso.nganhnghe(hopdonglaodong.loai_nghe_nganhnghe_tts.name_loaicongviec)
            context['hhjp_0116'] = format_hoso.nganhnghe(hopdonglaodong.congviec_nganhnghe_tts.name_congviec)
            context['hhjp_0115_v'] = format_hoso.kiemtra(hopdonglaodong.loai_nghe_nganhnghe_tts_viet)
            context['hhjp_0116_v'] = format_hoso.kiemtra(hopdonglaodong.congviec_nganhnghe_tts_viet)

            context['hhjp_0903'] = format_hoso.convert(hopdonglaodong.batdau_codinh_sang_hopdong)
            context['hhjp_0382'] = format_hoso.convert(hopdonglaodong.kethuc_codinh_chieu_hopdong)
            context['hhjp_0383'] = format_hoso.convert(hopdonglaodong.laodong_gio_tts)
            context['hhjp_0903_v'] = format_hoso.convert_viet(hopdonglaodong.batdau_codinh_sang_hopdong)
            context['hhjp_0382_v'] = format_hoso.convert_viet(hopdonglaodong.kethuc_codinh_chieu_hopdong)
            context['hhjp_0383_v'] = format_hoso.convert_viet(hopdonglaodong.laodong_gio_tts)
            context['hhjp_0384'] = format_hoso.kiemtra(hopdonglaodong.donvithaydoi_tts)
            context['hhjp_0384_1'] = '■' if hopdonglaodong.donvithaydoi_tts else '□'
            context['hhjp_0384_v'] = format_hoso.kiemtra(hopdonglaodong.donvithaydoi_tts_viet)

            if hopdonglaodong.batdau_tgian_viecca_mot or hopdonglaodong.ketthuc_tgian_viecca_mot:
                context['hhjp_0384_2'] = '■'
            else:
                context['hhjp_0384_2'] = '□'

            # Viec_Ca_Mot
            context['hhjp_0386_1'] = format_hoso.convert(hopdonglaodong.batdau_tgian_viecca_mot)
            context['hhjp_0387_1'] = format_hoso.convert(hopdonglaodong.ketthuc_tgian_viecca_mot)
            context['hhjp_0386_1_ngay'] = format_hoso.kiemtra(hopdonglaodong.apdung_tgian_viecca_mot)
            context['hhjp_0387_1_gio'] = format_hoso.convert(hopdonglaodong.sogio_laodong_motngay_mot)

            context['hhjp_0386_1_v'] = format_hoso.convert_viet(hopdonglaodong.batdau_tgian_viecca_mot)
            context['hhjp_0387_1_v'] = format_hoso.convert_viet(hopdonglaodong.ketthuc_tgian_viecca_mot)
            context['hhjp_0386_1_ngay_v'] = format_hoso.kiemtra(hopdonglaodong.apdung_tgian_viecca_mot)
            context['hhjp_0387_1_gio_v'] = format_hoso.convert_viet(hopdonglaodong.sogio_laodong_motngay_mot)

            # Viec_Ca_Hai
            context['hhjp_0386_2'] = format_hoso.convert(hopdonglaodong.batdau_tgian_viecca_hai)
            context['hhjp_0387_2'] = format_hoso.convert(hopdonglaodong.ketthuc_tgian_viecca_hai)
            context['hhjp_0386_2_ngay'] = format_hoso.kiemtra(hopdonglaodong.apdung_tgian_viecca_hai)
            context['hhjp_0387_2_gio'] = format_hoso.convert(hopdonglaodong.sogio_laodong_motngay_hai)

            context['hhjp_0386_2_v'] = format_hoso.convert_viet(hopdonglaodong.batdau_tgian_viecca_hai)
            context['hhjp_0387_2_v'] = format_hoso.convert_viet(hopdonglaodong.ketthuc_tgian_viecca_hai)
            context['hhjp_0386_2_ngay_v'] = format_hoso.kiemtra(hopdonglaodong.apdung_tgian_viecca_hai)
            context['hhjp_0387_2_gio_v'] = format_hoso.convert_viet(hopdonglaodong.sogio_laodong_motngay_hai)

            # Viec_Ca_Ba
            context['hhjp_0386_3'] = format_hoso.convert(hopdonglaodong.batdau_tgian_viecca_ba)
            context['hhjp_0387_3'] = format_hoso.convert(hopdonglaodong.ketthuc_tgian_viecca_ba)
            context['hhjp_0386_3_ngay'] = format_hoso.kiemtra(hopdonglaodong.apdung_tgian_viecca_ba)
            context['hhjp_0387_3_gio'] = format_hoso.convert(hopdonglaodong.sogio_laodong_motngay_ba)

            context['hhjp_0386_3_v'] = format_hoso.convert_viet(hopdonglaodong.batdau_tgian_viecca_ba)
            context['hhjp_0387_3_v'] = format_hoso.convert_viet(hopdonglaodong.ketthuc_tgian_viecca_ba)
            context['hhjp_0386_3_ngay_v'] = format_hoso.kiemtra(hopdonglaodong.apdung_tgian_viecca_ba)
            context['hhjp_0387_3_gio_v'] = format_hoso.convert_viet(hopdonglaodong.sogio_laodong_motngay_ba)

            context['hhjp_0151'] = format_hoso.kiemtra(hopdonglaodong.thoigian_giailao)
            context['hhjp_0151_v'] = format_hoso.kiemtra(hopdonglaodong.thoigian_giailao)

            context['hhjp_0388_h'] = format_hoso.kiemtra(hopdonglaodong.giolaodong_motthang)
            context['hhjp_0388_p'] = format_hoso.kiemtra(hopdonglaodong.phutlaodong_motthang)
            context['hhjp_0388_v'] = '%s Giờ %s Phút' % (format_hoso.kiemtra(hopdonglaodong.giolaodong_motthang),
                                                         format_hoso.kiemtra(hopdonglaodong.phutlaodong_motthang))
            context['hhjp_0152'] = format_hoso.place_value(hopdonglaodong.giolaodong_motnam)
            context['hhjp_0152_v'] = format_hoso.place_value(hopdonglaodong.giolaodong_motnam)
            context['hhjp_0389'] = format_hoso.kiemtra(hopdonglaodong.namnhat_ngaylaodong)
            context['hhjp_0390'] = format_hoso.kiemtra(hopdonglaodong.namhai_ngaylaodong)
            context['hhjp_0391'] = format_hoso.kiemtra(hopdonglaodong.namba_ngaylaodong)

            context['hhjp_0392_1'] = '■' if hopdonglaodong.loadong_ngoaigioi == u'Có' else '□'
            context['hhjp_0392_0'] = '■' if hopdonglaodong.loadong_ngoaigioi == u'Không' else '□'

            noiquy = []
            noiquy_v = []
            if hopdonglaodong.tudieu_mot_hopdong and hopdonglaodong.dendieu_mot_hopdong:
                dieu1 = '第 ' + str(hopdonglaodong.tudieu_mot_hopdong) + ' 条～第 ' + str(
                    hopdonglaodong.dendieu_mot_hopdong) + ' 条 ,'
                dieu1_v = 'Từ điều ' + str(hopdonglaodong.tudieu_mot_hopdong) + ' đến Điều ' + str(
                    hopdonglaodong.dendieu_mot_hopdong)
                noiquy.append(dieu1)
                noiquy_v.append(dieu1_v)

            if hopdonglaodong.tudieu_hai_hopdong and hopdonglaodong.dendieu_hai_hopdong:
                dieu2 = '第 ' + str(hopdonglaodong.tudieu_hai_hopdong) + ' 条～第 ' + str(
                    hopdonglaodong.dendieu_hai_hopdong) + ' 条 ,'
                dieu2_v = 'Từ điều ' + str(hopdonglaodong.tudieu_hai_hopdong) + ' đến Điều ' + str(
                    hopdonglaodong.dendieu_hai_hopdong)
                noiquy.append(dieu2)
                noiquy_v.append(dieu2_v)

            if hopdonglaodong.tudieu_ba_hopdong and hopdonglaodong.dendieu_ba_hopdong:
                dieu3 = '第 ' + str(hopdonglaodong.tudieu_ba_hopdong) + ' 条～第 ' + str(
                    hopdonglaodong.dendieu_ba_hopdong) + ' 条 ,'
                dieu3_v = 'Từ điều ' + str(hopdonglaodong.tudieu_ba_hopdong) + ' đến Điều ' + str(
                    hopdonglaodong.dendieu_ba_hopdong)
                noiquy_v.append(dieu3_v)
                noiquy.append(dieu3)

            if noiquy != []:
                context['hhjp_nq'] = ','.join(noiquy)
            else:
                context['hhjp_nq'] = '第     条 ～ 第     条 '

            if noiquy_v != []:
                context['hhjp_nq_v'] = ','.join(noiquy_v)
            else:
                context['hhjp_nq_v'] = 'Từ điều      đến Điều     '

            ngaynghi_dinhki = []
            ngaynghi_dinhki_v = []
            if hopdonglaodong.ngaynghi_dinhki_thuhai == True:
                ngaynghi_dinhki.append('月')
                ngaynghi_dinhki_v.append('Thứ hai')

            if hopdonglaodong.ngaynghi_dinhki_thuba == True:
                ngaynghi_dinhki.append('火')
                ngaynghi_dinhki_v.append('Thứ ba')

            if hopdonglaodong.ngaynghi_dinhki_thutu == True:
                ngaynghi_dinhki.append('水')
                ngaynghi_dinhki_v.append('Thứ tư')

            if hopdonglaodong.ngaynghi_dinhki_thunam == True:
                ngaynghi_dinhki.append('木')
                ngaynghi_dinhki_v.append('Thứ năm')

            if hopdonglaodong.ngaynghi_dinhki_thusau == True:
                ngaynghi_dinhki.append('金')
                ngaynghi_dinhki_v.append('Thứ sáu')

            if hopdonglaodong.ngaynghi_dinhki_thubay == True:
                ngaynghi_dinhki.append('土')
                ngaynghi_dinhki_v.append('Thứ bảy')

            if hopdonglaodong.ngaynghi_dinhki_chunhat == True:
                ngaynghi_dinhki.append('日')
                ngaynghi_dinhki_v.append(u'Chủ nhật')

            # if hopdonglaodong.ngaynghi_dinhki_ngayle == True:
            #     ngaynghi_dinhki.append('祝')

            context['hhjp_0154'] = "、".join(ngaynghi_dinhki)
            context['hhjp_0154_v'] = "、".join(ngaynghi_dinhki_v)

            context['hhjp_0154_1'] = format_hoso.kiemtra(hopdonglaodong.ngaynghi_dinhki_khac)
            context['hhjp_0154_1_v'] = format_hoso.kiemtra(hopdonglaodong.ngaynghi_dinhki_khac_viet)

            context['hhjp_0393'] = format_hoso.kiemtra(hopdonglaodong.tongso_ngaynghi_nam)

            if hopdonglaodong.ngaynghi_khac == u'Tuần':
                context['hhjp_0396_1'] = format_hoso.kiemtra(hopdonglaodong.so_ngaynghi_khac)
            if hopdonglaodong.ngaynghi_khac == u'Tháng':
                context['hhjp_0396_1'] = format_hoso.kiemtra(hopdonglaodong.so_ngaynghi_khac)

            context['hhjp_0396_1_khac'] = format_hoso.kiemtra(hopdonglaodong.ngaynghi_khac_nhat)
            context['hhjp_0396_1_khac_v'] = format_hoso.kiemtra(hopdonglaodong.ngaynghi_khac_viet)

            noiquy_nn = []
            noiquy_nn_v = []
            if hopdonglaodong.tudieu_mot_ngaynghi and hopdonglaodong.dendieu_mot_ngaynghi:
                dieu_nn = '第 ' + str(hopdonglaodong.tudieu_mot_ngaynghi) + ' 条～第 ' + str(
                    hopdonglaodong.dendieu_mot_ngaynghi) + ' 条 ,'
                dieu_nnv = 'Từ điều ' + str(hopdonglaodong.tudieu_mot_ngaynghi) + ' đến Điều ' + str(
                    hopdonglaodong.dendieu_mot_ngaynghi)
                noiquy_nn.append(dieu_nn)
                noiquy_nn_v.append(dieu_nnv)

            if hopdonglaodong.tudieu_hai_ngaynghi and hopdonglaodong.dendieu_hai_ngaynghi:
                dieu_nn2 = '第 ' + str(hopdonglaodong.tudieu_hai_ngaynghi) + ' 条～第 ' + str(
                    hopdonglaodong.dendieu_hai_ngaynghi) + ' 条 ,'
                dieu_2_nnv = 'Từ điều ' + str(hopdonglaodong.tudieu_hai_ngaynghi) + ' đến Điều ' + str(
                    hopdonglaodong.dendieu_hai_ngaynghi)
                noiquy_nn.append(dieu_nn2)
                noiquy_nn_v.append(dieu_2_nnv)

            if noiquy_nn != []:
                context['hhjp_nn'] = ','.join(noiquy_nn)
            else:
                context['hhjp_nn'] = '第     条 ～ 第     条 '

            if noiquy_nn_v != []:
                context['hhjp_nn_v'] = ','.join(noiquy_nn_v)
            else:
                context['hhjp_nn_v'] = 'Từ điều      đến Điều     '
            context['hhjp_0394'] = format_hoso.kiemtra(hopdonglaodong.ngaynghiphep_coluong_lientuc_sauthang)

            if hopdonglaodong.duoi_sauthang_nghiphep_coluong == u'Có':
                context['hhjp_0395_1'] = '■'
                context['hhjp_0395_0'] = '□'
                docdata.remove_shape(u'id="3" name="Oval 3"')
            elif hopdonglaodong.duoi_sauthang_nghiphep_coluong == u'Không':
                context['hhjp_0395_0'] = '■'
                context['hhjp_0395_1'] = '□'
                docdata.remove_shape(u'id="2" name="Oval 2"')
            else:
                context['hhjp_0395_0'] = '□'
                context['hhjp_0395_1'] = '□'
                docdata.remove_shape(u'id="2" name="Oval 2"')
                docdata.remove_shape(u'id="3" name="Oval 3"')

            context['hhjp_0740'] = format_hoso.kiemtra(hopdonglaodong.nghiphep_coluong_thang)
            context['hhjp_0741'] = format_hoso.kiemtra(hopdonglaodong.nghiphep_coluong_ngay)
            context['hhjp_0740_v'] = format_hoso.kiemtra(hopdonglaodong.nghiphep_coluong_thang)
            context['hhjp_0741_v'] = format_hoso.kiemtra(hopdonglaodong.nghiphep_coluong_ngay)
            context['hhjp_0397'] = format_hoso.kiemtra(hopdonglaodong.nghiphep_nghicoluong)
            context['hhjp_0398'] = format_hoso.kiemtra(hopdonglaodong.nghikhac_khongluong)

            context['hhjp_0397_v'] = format_hoso.kiemtra(hopdonglaodong.nghiphep_nghicoluong_viet)
            context['hhjp_0398_v'] = format_hoso.kiemtra(hopdonglaodong.nghikhac_khongluong_viet)

            noiquy_np = []
            noiquy_np_v = []
            if hopdonglaodong.tudieu_mot_luong and hopdonglaodong.dendieu_mot_luong:
                dieu_np = '第 ' + str(hopdonglaodong.tudieu_mot_luong) + ' 条～第 ' + str(
                    hopdonglaodong.dendieu_mot_luong) + ' 条 ,'
                dieu_npv = 'Từ điều ' + str(hopdonglaodong.tudieu_mot_luong) + ' đến Điều ' + str(
                    hopdonglaodong.dendieu_mot_luong)
                noiquy_np.append(dieu_np)
                noiquy_np_v.append(dieu_npv)

            if hopdonglaodong.tudieu_hai_luong and hopdonglaodong.dendieu_hai_luong:
                dieu_np2 = '第 ' + str(hopdonglaodong.tudieu_hai_luong) + ' 条～第 ' + str(
                    hopdonglaodong.dendieu_hai_luong) + ' 条 ,'
                dieu_2_npv = 'Từ điều ' + str(hopdonglaodong.tudieu_hai_luong) + ' đến Điều ' + str(
                    hopdonglaodong.dendieu_hai_luong)
                noiquy_np.append(dieu_np2)
                noiquy_np_v.append(dieu_2_npv)

            if noiquy_np != []:
                context['hhjp_np'] = ','.join(noiquy_np)
            else:
                context['hhjp_np'] = '第     条 ～ 第     条 '

            if noiquy_np_v != []:
                context['hhjp_np_v'] = ','.join(noiquy_np_v)
            else:
                context['hhjp_np_v'] = 'Từ điều      đến Điều     '

            if hopdonglaodong.hinhthuc_luongthang:
                context['hhjp_0400_thang'] = format_hoso.place_value(hopdonglaodong.sotien_luongthang)
            elif hopdonglaodong.hinhthuc_luongngay:
                context['hhjp_0400_ngay'] = format_hoso.place_value(hopdonglaodong.sotien_luongngay)
            elif hopdonglaodong.hinhthuc_luonggio:
                context['hhjp_0400_gio'] = format_hoso.place_value(hopdonglaodong.sotien_luonggio)
            else:
                context['hhjp_0400_thang'] = ''
                context['hhjp_0400_ngay'] = ''
                context['hhjp_0400_gio'] = ''

            context['hhjp_0403'] = format_hoso.kiemtra(hopdonglaodong.lamthem_vuotqua_gio)
            if hopdonglaodong.lamthem_trongvong_giokhong == True:
                context['hhjp_0404'] = format_hoso.kiemtra(hopdonglaodong.lamthem_trongvong_gio)
            else:
                context['hhjp_0404'] = ''
            context['hhjp_0405'] = format_hoso.kiemtra(hopdonglaodong.theo_quydinh)
            context['hhjp_0406'] = format_hoso.kiemtra(hopdonglaodong.ngayle_theoquydinh)
            context['hhjp_0407'] = format_hoso.kiemtra(hopdonglaodong.ngayle_khong_theoquydinh)
            context['hhjp_0408'] = format_hoso.kiemtra(hopdonglaodong.laodong_bandem)

            if hopdonglaodong.ngaytinh_luong == u'Mỗi tháng':
                if hopdonglaodong.tinhluong_cuoithang == True:
                    context['hhjp_0409_1'] = '■'
                    context['hhjp_0409_2'] = '月末'
                    context['hhjp_0409_3'] = '□'
                    context['hhjp_0409_4'] = '____'
                    context['hhjp_0409_v'] = 'Ngày cuối hàng tháng'
                    context['hhjp_0409_v1'] = ''
                elif hopdonglaodong.tinhluong_cuoithang == False:
                    if hopdonglaodong.ngaytinh_luong_mot:
                        context['hhjp_0409_1'] = '■'
                        context['hhjp_0409_2'] = format_hoso.kiemtra(hopdonglaodong.ngaytinh_luong_mot)
                        context['hhjp_0409_3'] = '□'
                        context['hhjp_0409_4'] = '____'
                        if hopdonglaodong.ngaytinh_luong_mot_v:
                            context['hhjp_0409_v'] = 'Ngày ' + str(hopdonglaodong.ngaytinh_luong_mot_v) + ' hàng tháng'
                        else:
                            context['hhjp_0409_v'] = ''
                        context['hhjp_0409_v1'] = ''
                    else:
                        context['hhjp_0409_1'] = '□'
                        context['hhjp_0409_2'] = '____'
                        context['hhjp_0409_3'] = '□'
                        context['hhjp_0409_4'] = '____'
                        context['hhjp_0409_v'] = ''
                        context['hhjp_0409_v1'] = ''

            elif hopdonglaodong.ngaytinh_luong == u'Mỗi tuần':
                if hopdonglaodong.tinhluong_cuoithang == True:
                    context['hhjp_0409_1'] = '■'
                    context['hhjp_0409_2'] = '月末'
                    context['hhjp_0409_v'] = 'Ngày cuối hàng tháng'

                elif hopdonglaodong.tinhluong_cuoithang == False:
                    if hopdonglaodong.ngaytinh_luong_mot:
                        context['hhjp_0409_1'] = '■'
                        if hopdonglaodong.ngaytinh_luong_mot_v:
                            context['hhjp_0409_v'] = 'Ngày ' + str(hopdonglaodong.ngaytinh_luong_mot_v) + ' hàng tháng'
                        else:
                            context['hhjp_0409_v'] = ''
                        context['hhjp_0409_2'] = format_hoso.kiemtra(hopdonglaodong.ngaytinh_luong_mot)
                    else:
                        context['hhjp_0409_1'] = '□'
                        context['hhjp_0409_2'] = '____'
                        context['hhjp_0409_v'] = ''

                if hopdonglaodong.ngaytinh_luong_hai:
                    context['hhjp_0409_3'] = '■'
                    context['hhjp_0409_4'] = format_hoso.kiemtra(hopdonglaodong.ngaytinh_luong_hai)
                    if hopdonglaodong.ngaytinh_luong_hai_v:
                        context['hhjp_0409_v1'] = 'Ngày ' + str(hopdonglaodong.ngaytinh_luong_hai_v) + ' hàng tháng'
                    else:
                        context['hhjp_0409_v1'] = ''
                else:
                    context['hhjp_0409_3'] = '□'
                    context['hhjp_0409_4'] = ''
                    context['hhjp_0409_v1'] = ''

            if hopdonglaodong.ngaytra_luong == u'Mỗi tháng':
                if hopdonglaodong.traluong_cuoithang == True:
                    context['hhjp_0410_1'] = '■'
                    context['hhjp_0410_2'] = '月末'
                    context['hhjp_0410_3'] = '□'
                    context['hhjp_0410_4'] = '____'
                    context['hhjp_0410_v'] = 'Ngày cuối hàng tháng'
                    context['hhjp_0410_v1'] = ''

                elif hopdonglaodong.traluong_cuoithang == False:
                    if hopdonglaodong.ngaytra_luong_mot:
                        context['hhjp_0410_1'] = '■'
                        context['hhjp_0410_2'] = format_hoso.kiemtra(hopdonglaodong.ngaytra_luong_mot)
                        context['hhjp_0410_3'] = '□'
                        context['hhjp_0410_4'] = '____'
                        if hopdonglaodong.ngaytra_luong_mot_v:
                            context['hhjp_0410_v'] = 'Ngày ' + str(hopdonglaodong.ngaytra_luong_mot_v) + ' hàng tháng'
                        else:
                            context['hhjp_0410_v'] = ''
                        context['hhjp_0410_v1'] = ''
                    else:
                        context['hhjp_0410_1'] = '□'
                        context['hhjp_0410_2'] = '____'
                        context['hhjp_0410_3'] = '□'
                        context['hhjp_0410_4'] = '____'
                        context['hhjp_0410_v'] = ''
                        context['hhjp_0410_v1'] = ''

            elif hopdonglaodong.ngaytra_luong == u'Mỗi tuần':
                if hopdonglaodong.traluong_cuoithang == True:
                    context['hhjp_0410_1'] = '■'
                    context['hhjp_0410_2'] = '月末'
                    context['hhjp_0410_v'] = 'Ngày cuối hàng tháng'

                elif hopdonglaodong.traluong_cuoithang == False:
                    if hopdonglaodong.ngaytra_luong_mot:
                        context['hhjp_0410_1'] = '■'
                        context['hhjp_0410_2'] = format_hoso.kiemtra(hopdonglaodong.ngaytra_luong_mot)
                        if hopdonglaodong.ngaytra_luong_mot_v:
                            context['hhjp_0410_v'] = 'Ngày ' + str(hopdonglaodong.ngaytra_luong_mot_v) + ' hàng tháng'
                        else:
                            context['hhjp_0410_v'] = ''
                    else:
                        context['hhjp_0410_1'] = '□'
                        context['hhjp_0410_2'] = '____'
                        context['hhjp_0410_v'] = ''

                if hopdonglaodong.ngaytra_luong_hai:
                    context['hhjp_0410_3'] = '■'
                    context['hhjp_0410_4'] = format_hoso.kiemtra(hopdonglaodong.ngaytra_luong_hai)
                    if hopdonglaodong.ngaytra_luong_hai_v:
                        context['hhjp_0410_v1'] = 'Ngày ' + str(hopdonglaodong.ngaytra_luong_hai_v) + ' hàng tháng'
                    else:
                        context['hhjp_0410_v1'] = ''
                else:
                    context['hhjp_0410_3'] = '□'
                    context['hhjp_0410_4'] = ''
                    context['hhjp_0410_v1'] = ''

            if hopdonglaodong.hinhthuc_thanhtoan == 'Tiền mặt':
                context['hhjp_0411_1'] = '■'
                context['hhjp_0411_2'] = '□'
                context['hhjp_0411_v'] = 'Trả tiền mặt'
            if hopdonglaodong.hinhthuc_thanhtoan == 'Chuyển khoản':
                context['hhjp_0411_1'] = '□'
                context['hhjp_0411_2'] = '■'
                context['hhjp_0411_v'] = 'Chuyển khoản ngân hàng'

            if hopdonglaodong.khautru_luong == 'Không':
                context['hhjp_0412_1'] = '■'
                context['hhjp_0412_2'] = '□'
                context['hhjp_0412_v'] = 'Không'
            elif hopdonglaodong.khautru_luong == 'Có':
                context['hhjp_0412_1'] = '□'
                context['hhjp_0412_2'] = '■'
                context['hhjp_0412_v'] = 'Có'
            else:
                context['hhjp_0412_1'] = '□'
                context['hhjp_0412_2'] = '□'
                context['hhjp_0412_v'] = ''

            if hopdonglaodong.tangluong == 'Có':
                context['hhjp_0413_1'] = '■'
                context['hhjp_0413_3'] = format_hoso.kiemtra(hopdonglaodong.thoidiem_tangluong)
                context[
                    'hhjp_0413_3_v'] = format_hoso.kiemtra(hopdonglaodong.thoidiem_tangluong_viet)
                context['hhjp_0413_2'] = '□'
            elif hopdonglaodong.tangluong == 'Không':
                context['hhjp_0413_1'] = '□'
                context['hhjp_0413_2'] = '■'
                context['hhjp_0413_3'] = ''
                context['hhjp_0413_3_v'] = ''
            else:
                context['hhjp_0413_1'] = '□'
                context['hhjp_0413_2'] = '□'
                context['hhjp_0413_3'] = ''
                context['hhjp_0413_3_v'] = ''

            if hopdonglaodong.thuong == 'Có':
                context['hhjp_0414_1'] = '■'
                context['hhjp_0414_3'] = format_hoso.kiemtra(hopdonglaodong.thoidiem_thuong)
                context['hhjp_0414_3_v'] = format_hoso.kiemtra(hopdonglaodong.thoidiem_thuong_viet)
                context['hhjp_0414_2'] = '□'
            elif hopdonglaodong.thuong == 'Không':
                context['hhjp_0414_2'] = '■'
                context['hhjp_0414_1'] = '□'
                context['hhjp_0414_3'] = ''
                context['hhjp_0414_3_v'] = ''
            else:
                context['hhjp_0414_2'] = '□'
                context['hhjp_0414_1'] = '□'
                context['hhjp_0414_3'] = ''
                context['hhjp_0414_3_v'] = ''

            if hopdonglaodong.trocap_thoiviec == 'Có':
                context['hhjp_0415_2'] = '□'
                context['hhjp_0415_1'] = '■'
                context['hhjp_0415_3'] = format_hoso.kiemtra(hopdonglaodong.thoidiem_trocap_thoiviec)
                context['hhjp_0415_3_v'] = format_hoso.kiemtra(hopdonglaodong.thoidiem_trocap_thoiviec_viet)
            elif hopdonglaodong.trocap_thoiviec == 'Không':
                context['hhjp_0415_1'] = '□'
                context['hhjp_0415_2'] = '■'
                context['hhjp_0415_3'] = ''
                context['hhjp_0415_3_v'] = ''
            else:
                context['hhjp_0415_1'] = '□'
                context['hhjp_0415_2'] = '□'
                context['hhjp_0415_3'] = ''
                context['hhjp_0415_3_v'] = ''

            if hopdonglaodong.phucap_ngung_kinhdoanh == 'Có':
                context['hhjp_0416'] = '■'
                context['hhjp_0416_1'] = format_hoso.kiemtra(hopdonglaodong.tyle_ngung_kinhdoanh)
                context['hhjp_0416_1_v'] = format_hoso.kiemtra(hopdonglaodong.tyle_ngung_kinhdoanh)
            elif hopdonglaodong.phucap_ngung_kinhdoanh == 'Không':
                context['hhjp_0416'] = '□'
                context['hhjp_0416_1'] = ''
                context['hhjp_0416_1_v'] = ''
            else:
                context['hhjp_0416'] = '□'
                context['hhjp_0416_1'] = ''
                context['hhjp_0416_1_v'] = ''

            context['hhjp_0417'] = format_hoso.kiemtra(hopdonglaodong.noptruoc_thoiviec)

            noiquy_tv = []
            noiquy_tvv = []
            if hopdonglaodong.tudieu_mot_thoiviec and hopdonglaodong.dendieu_mot_thoiviec:
                dieu_tv = '第 ' + str(hopdonglaodong.tudieu_mot_thoiviec) + ' 条～第 ' + str(
                    hopdonglaodong.dendieu_mot_thoiviec) + ' 条 ,'
                dieu_tvv = 'Từ điều ' + str(hopdonglaodong.tudieu_mot_thoiviec) + ' đến Điều ' + str(
                    hopdonglaodong.dendieu_mot_thoiviec)
                noiquy_tv.append(dieu_tv)
                noiquy_tvv.append(dieu_tvv)

            if hopdonglaodong.tudieu_hai_thoiviec and hopdonglaodong.dendieu_hai_thoiviec:
                dieu_tv = '第 ' + str(hopdonglaodong.tudieu_hai_thoiviec) + ' 条～第 ' + str(
                    hopdonglaodong.dendieu_hai_thoiviec) + ' 条 ,'
                dieu_tvv = 'Từ điều ' + str(hopdonglaodong.tudieu_hai_thoiviec) + ' đến Điều ' + str(
                    hopdonglaodong.dendieu_hai_thoiviec)
                noiquy_tv.append(dieu_tv)
                noiquy_tvv.append(dieu_tvv)

            if noiquy_tv != []:
                context['hhjp_tv'] = ','.join(noiquy_tv)
            else:
                context['hhjp_tv'] = '第     条 ～ 第     条 '

            if noiquy_tvv != []:
                context['hhjp_tvv'] = ','.join(noiquy_tvv)
            else:
                context['hhjp_tvv'] = 'Từ điều      đến Điều     '

            context['hhjp_0157'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.tennha_daingo_daotao_nhao)
            context['hhjp_0157_v'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.tennha_daingo_daotao_nhao_v)

            if document.donhang_hoso.donhang_daingo.loainha_daingo_daotao_nhao == 'Kí túc xá':
                context['hhjp_0482_1'] = '■'
                context['hhjp_0482_2'] = '□'
                context['hhjp_0482_3'] = '□'
                context['hhjp_0482'] = ''
                context['hhjp_0482_v'] = ''
            elif document.donhang_hoso.donhang_daingo.loainha_daingo_daotao_nhao == 'Nhà thuê':
                context['hhjp_0482_1'] = '□'
                context['hhjp_0482_2'] = '■'
                context['hhjp_0482_3'] = '□'
                context['hhjp_0482'] = ''
                context['hhjp_0482_v'] = ''
            elif document.donhang_hoso.donhang_daingo.loainha_daingo_daotao_nhao == 'Khác':
                context['hhjp_0482_1'] = '□'
                context['hhjp_0482_2'] = '□'
                context['hhjp_0482_3'] = '■'
                context['hhjp_0482'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.khac_loainha_daingo_daotao_nhao)
                context['hhjp_0482_v'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.khac_loainha_daingo_daotao_nhao_v)
            else:
                context['hhjp_0482_1'] = '□'
                context['hhjp_0482_2'] = '□'
                context['hhjp_0482_3'] = '□'
                context['hhjp_0482'] = ''
                context['hhjp_0482_v'] = ''

            if document.donhang_hoso.donhang_daingo.sobuudien_daingo_nhao:
                context['bs_0019'] = document.donhang_hoso.donhang_daingo.sobuudien_daingo_nhao[
                                     0:3] + '-' + document.donhang_hoso.donhang_daingo.sobuudien_daingo_nhao[3:]
            else:
                context['bs_0019'] = ''

            context['hhjp_0158'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.diachi_daingo_daotao_nhao)
            context['hhjp_0483'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.dienthoai_daingo_daotao_nhao)
            context['hhjp_0158_v'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.diachi_daingo_daotao_nhao_v)

            context['hhjp_0484'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.matbang_daingo_daotao_nhao)
            context['hhjp_0485'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.songuoi_daingo_daotao_nhao)
            context['hhjp_0486'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.dientich_nguoi_daingo_daotao_nhao)

            context['hhjp_0470_v'] = Listing(
                document.donhang_hoso.donhang_daingo.thuctapsinhtra_daingo_daotao_nhao_viet) if document.donhang_hoso.donhang_daingo.thuctapsinhtra_daingo_daotao_nhao_viet else ''
            context['hhjp_040'] = Listing(
                document.donhang_hoso.donhang_daingo.thuctapsinhtra_daingo_daotao_nhao) if document.donhang_hoso.donhang_daingo.thuctapsinhtra_daingo_daotao_nhao else ''

            if hopdonglaodong.huutri_baohiem == True:
                context['hhjp_0418_1'] = '■'
            else:
                context['hhjp_0418_1'] = '□'
            if hopdonglaodong.huwuquocgia_baohiem == True:
                context['hhjp_0418_2'] = '■'
            else:
                context['hhjp_0418_2'] = '□'
            if hopdonglaodong.yte_baohiem == True:
                context['hhjp_0418_3'] = '■'
            else:
                context['hhjp_0418_3'] = '□'
            if hopdonglaodong.yte_quocgia_baohiem == True:
                context['hhjp_0418_4'] = '■'
            else:
                context['hhjp_0418_4'] = '□'

            if hopdonglaodong.vieclam_baohiem == True:
                context['hhjp_0418_5'] = '■'
            else:
                context['hhjp_0418_5'] = '□'

            if hopdonglaodong.congnghiep_baohiem == True:
                context['hhjp_0418_6'] = '■'
            else:
                context['hhjp_0418_6'] = '□'

            if hopdonglaodong.khac_baohiem == True:
                context['hhjp_0418_7'] = '■'
                context['hhjp_0418_8'] = format_hoso.kiemtra(hopdonglaodong.noidung_khac_baohiem)
            else:
                context['hhjp_0418_7'] = '□'
                context['hhjp_0418_8'] = format_hoso.kiemtra(hopdonglaodong.noidung_khacviet_baohiem)

            context[
                'hhjp_0419'] = hopdonglaodong.nam_suckhoe_congty + ' 年 ' + hopdonglaodong.thang_suckhoe_congty + ' 月' if hopdonglaodong.nam_suckhoe_congty else ''
            context[
                'hhjp_0419_v'] = 'Năm ' + hopdonglaodong.nam_suckhoe_congty + ' Tháng ' + hopdonglaodong.thang_suckhoe_congty if hopdonglaodong.nam_suckhoe_congty else ''

            context[
                'hhjp_0420'] = hopdonglaodong.nam_suckhoe_dinhky_landau + ' 年 ' + hopdonglaodong.thang_suckhoe_dinhky_landau + ' 月' if hopdonglaodong.nam_suckhoe_dinhky_landau else ''
            context[
                'hhjp_0420_v'] = 'Năm ' + hopdonglaodong.nam_suckhoe_dinhky_landau + ' Tháng ' + hopdonglaodong.thang_suckhoe_dinhky_landau if hopdonglaodong.nam_suckhoe_dinhky_landau else ''

            if hopdonglaodong.tanso_khamlai_thangtuan == 'Tuần':
                context['hhjp_0421'] = format_hoso.kiemtra(hopdonglaodong.tanso_khamlai) + '週'
                context['hhjp_0421_v'] = format_hoso.kiemtra(hopdonglaodong.tanso_khamlai) + ' tuần'
            elif hopdonglaodong.tanso_khamlai_thangtuan == 'Năm':
                context['hhjp_0421'] = format_hoso.kiemtra(hopdonglaodong.tanso_khamlai) + '年'
                context['hhjp_0421_v'] = format_hoso.kiemtra(hopdonglaodong.tanso_khamlai) + 'năm'
            else:
                context['hhjp_0421_v'] = ''
                context['hhjp_0421'] = ''

            if hopdonglaodong.hinhthuc_luongthang:
                context['hhjp_0400_thang'] = format_hoso.place_value(hopdonglaodong.sotien_luongthang)
            elif hopdonglaodong.hinhthuc_luongngay:
                context['hhjp_0400_ngay'] = format_hoso.place_value(hopdonglaodong.sotien_luongngay)
            elif hopdonglaodong.hinhthuc_luonggio:
                context['hhjp_0400_gio'] = format_hoso.place_value(hopdonglaodong.sotien_luonggio)
            else:
                context['hhjp_0400_thang'] = ''
                context['hhjp_0400_ngay'] = ''
                context['hhjp_0400_gio'] = ''

            if hopdonglaodong.hinhthuc_luongthang:
                context['hhjp_0400_0'] = '■'
                context['hhjp_0400_1'] = '□'
                context['hhjp_0400_2'] = '□'
                context['hhjp_0422'] = format_hoso.place_value(hopdonglaodong.sotien_luongthang_tong)
                context['hhjp_0147'] = ''
            elif hopdonglaodong.hinhthuc_luongngay:
                context['hhjp_0400_0'] = '□'
                context['hhjp_0400_1'] = '■'
                context['hhjp_0400_2'] = '□'
                context['hhjp_0422'] = format_hoso.place_value(hopdonglaodong.tongtien_thang_luongngay_1)
                context['hhjp_0147'] = format_hoso.place_value(hopdonglaodong.tongtien_thang_luongngay_2)
            elif hopdonglaodong.hinhthuc_luonggio:
                context['hhjp_0400_0'] = '□'
                context['hhjp_0400_1'] = '□'
                context['hhjp_0400_2'] = '■'
                context['hhjp_0422'] = ''
                context['hhjp_0147'] = format_hoso.place_value(hopdonglaodong.sotien_luonggio_tong)
            else:
                context['hhjp_0400_0'] = '□'
                context['hhjp_0400_1'] = '□'
                context['hhjp_0400_2'] = '□'
                context['hhjp_0422'] = ''
                context['hhjp_0147'] = ''

            if hopdonglaodong.sotien_phuccap_nhao:
                context['hhjp_0425_1'] = '住宅'
                context['hhjp_0424_1'] = format_hoso.kiemtra(hopdonglaodong.sotien_phuccap_nhao)
                context['hhjp_0426_1'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_nhao)

                context['hhjp_0425_1_v'] = format_hoso.kiemtra(hopdonglaodong.khac_phucap_nhao_viet)
                context['hhjp_0426_1_v'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_nhao_viet)
            else:
                context['hhjp_0425_1'] = ''
                context['hhjp_0424_1'] = ''
                context['hhjp_0426_1'] = ''
                context['hhjp_0425_1_v'] = ''
                context['hhjp_0426_1_v'] = ''

            if hopdonglaodong.sotien_phuccap_an:
                context['hhjp_0425_2'] = '食事'
                context['hhjp_0424_2'] = format_hoso.kiemtra(hopdonglaodong.sotien_phuccap_an)
                context['hhjp_0426_2'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_an)

                context['hhjp_0425_2_v'] = format_hoso.kiemtra(hopdonglaodong.khac_phucap_an_viet)
                context['hhjp_0426_2_v'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_an_viet)
            else:
                context['hhjp_0425_2'] = ''
                context['hhjp_0424_2'] = ''
                context['hhjp_0426_2'] = ''
                context['hhjp_0425_2_v'] = ''
                context['hhjp_0426_2_v'] = ''

            if hopdonglaodong.sotien_phuccap_dilai:
                context['hhjp_0425_3'] = '通勤'
                context['hhjp_0424_3'] = format_hoso.kiemtra(hopdonglaodong.sotien_phuccap_dilai)
                context['hhjp_0426_3'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_dilai)

                context['hhjp_0425_3_v'] = format_hoso.kiemtra(hopdonglaodong.khac_phucap_dilai_viet)
                context['hhjp_0426_3_v'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_dilai_viet)
            else:
                context['hhjp_0425_3'] = ''
                context['hhjp_0424_3'] = ''
                context['hhjp_0426_3'] = ''
                context['hhjp_0425_3_v'] = ''
                context['hhjp_0426_3_v'] = ''

            if hopdonglaodong.sotien_phuccap_thue:
                context['hhjp_0425_4'] = '課税通勤'
                context['hhjp_0424_4'] = format_hoso.kiemtra(hopdonglaodong.sotien_phuccap_thue)
                context['hhjp_0426_4'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_thue)

                context['hhjp_0425_4_v'] = format_hoso.kiemtra(hopdonglaodong.khac_phucap_thue_viet)
                context['hhjp_0426_4_v'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_thue_viet)
            else:
                context['hhjp_0425_4'] = ''
                context['hhjp_0424_4'] = ''
                context['hhjp_0426_4'] = ''
                context['hhjp_0425_4_v'] = ''
                context['hhjp_0426_4_v'] = ''

            if hopdonglaodong.phucap_them != True:
                if hopdonglaodong.sotien_phuccap_mot:
                    context['hhjp_0425_5'] = format_hoso.kiemtra(hopdonglaodong.phuccap_mot)
                    context['hhjp_0424_5'] = format_hoso.kiemtra(hopdonglaodong.sotien_phuccap_mot)
                    context['hhjp_0426_5'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_mot)

                    context['hhjp_0425_5_v'] = format_hoso.kiemtra(hopdonglaodong.khac_phucap_mot_viet)
                    context['hhjp_0426_5_v'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_mot_viet)
                else:
                    context['hhjp_0425_5'] = ''
                    context['hhjp_0424_5'] = ''
                    context['hhjp_0426_5'] = ''
                    context['hhjp_0425_5_v'] = ''
                    context['hhjp_0426_5_v'] = ''

                if hopdonglaodong.sotien_phuccap_hai:
                    context['hhjp_0425_6'] = format_hoso.kiemtra(hopdonglaodong.phuccap_hai)
                    context['hhjp_0424_6'] = format_hoso.kiemtra(hopdonglaodong.sotien_phuccap_hai)
                    context['hhjp_0426_6'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_hai)

                    context['hhjp_0425_6_v'] = format_hoso.kiemtra(hopdonglaodong.khac_phucap_hai_viet)
                    context['hhjp_0426_6_v'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_hai_viet)
                else:
                    context['hhjp_0425_6'] = ''
                    context['hhjp_0424_6'] = ''
                    context['hhjp_0426_6'] = ''
                    context['hhjp_0425_6_v'] = ''
                    context['hhjp_0426_6_v'] = ''

                if hopdonglaodong.sotien_phuccap_ba:
                    context['hhjp_0425_7'] = format_hoso.kiemtra(hopdonglaodong.phuccap_ba)
                    context['hhjp_0424_7'] = format_hoso.kiemtra(hopdonglaodong.sotien_phuccap_ba)
                    context['hhjp_0426_7'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_ba)

                    context['hhjp_0425_7_v'] = format_hoso.kiemtra(hopdonglaodong.khac_phucap_ba_viet)
                    context['hhjp_0426_7_v'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_ba_viet)
                else:
                    context['hhjp_0425_7'] = ''
                    context['hhjp_0424_7'] = ''
                    context['hhjp_0426_7'] = ''
                    context['hhjp_0425_7_v'] = ''
                    context['hhjp_0426_7_v'] = ''

                if hopdonglaodong.sotien_phuccap_bon:
                    context['hhjp_0425_8'] = format_hoso.kiemtra(hopdonglaodong.phuccap_bon)
                    context['hhjp_0424_8'] = format_hoso.kiemtra(hopdonglaodong.sotien_phuccap_bon)
                    context['hhjp_0426_8'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_bon)

                    context['hhjp_0425_8_v'] = format_hoso.kiemtra(hopdonglaodong.khac_phucap_bon_viet)
                    context['hhjp_0426_8_v'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_bon_viet)
                else:
                    context['hhjp_0425_8'] = ''
                    context['hhjp_0424_8'] = ''
                    context['hhjp_0426_8'] = ''
                    context['hhjp_0425_8_v'] = ''
                    context['hhjp_0426_8_v'] = ''

            context['hhjp_0701'] = format_hoso.place_value(hopdonglaodong.khoankhac_phucap_tts)

            context['hhjp_kk1'] = format_hoso.kiemtra(hopdonglaodong.noidung_khoanmuc_10)
            context['hhjp_kk1_v'] = format_hoso.kiemtra(hopdonglaodong.noidung_khoanmuc_viet_10)
            context['hhjp_kk1t'] = format_hoso.place_value(hopdonglaodong.sotien_khoanmuc_10)
            if hopdonglaodong.chiphi_khoanmuc_10 == True:
                context['hhjp_kk1_1'] = '実費'
                context['h2'] = 'Chi phí thực tế'
            else:
                context['hhjp_kk1_1'] = ''
                context['h2'] = ''

            context['hhjp_kk2'] = format_hoso.kiemtra(hopdonglaodong.noidung_khoanmuc_11)
            context['hhjp_kk2_v'] = format_hoso.kiemtra(hopdonglaodong.noidung_khoanmuc_viet_11)
            context['hhjp_kk2t'] = format_hoso.place_value(hopdonglaodong.sotien_khoanmuc_11)
            if hopdonglaodong.chiphi_khoanmuc_11 == True:
                context['hhjp_kk2_1'] = '実費'
                context['h3'] = 'Chi phí thực tế'
            else:
                context['hhjp_kk2_1'] = ''
                context['h3'] = ''

            context['hhjp_kk3'] = format_hoso.kiemtra(hopdonglaodong.noidung_khoanmuc_12)
            context['hhjp_kk3_v'] = format_hoso.kiemtra(hopdonglaodong.noidung_khoanmuc_viet_12)
            context['hhjp_kk3t'] = format_hoso.place_value(hopdonglaodong.sotien_khoanmuc_12)
            if hopdonglaodong.chiphi_khoanmuc_12 == True:
                context['hhjp_kk3_1'] = '実費'
                context['h4'] = 'Chi phí thực tế'
            else:
                context['hhjp_kk3_1'] = ''
                context['h4'] = ''

            context['hhjp_kk4'] = format_hoso.kiemtra(hopdonglaodong.noidung_khoanmuc_13)
            context['hhjp_kk4_v'] = format_hoso.kiemtra(hopdonglaodong.noidung_khoanmuc_viet_13)
            context['hhjp_kk4t'] = format_hoso.place_value(hopdonglaodong.sotien_khoanmuc_13)
            if hopdonglaodong.chiphi_khoanmuc_13 == True:
                context['hhjp_kk4_1'] = '実費'
                context['h5'] = 'Chi phí thực tế'
            else:
                context['hhjp_kk4_1'] = ''
                context['h5'] = ''

            # context['hhjp_kk5'] = format_hoso.kiemtra(hopdonglaodong.noidung_khoanmuc_14)
            # context['hhjp_kk5_v'] = format_hoso.kiemtra(hopdonglaodong.noidung_khoanmuc_viet_14)
            # context['hhjp_kk5t'] = format_hoso.place_value(hopdonglaodong.sotien_khoanmuc_14)
            # if hopdonglaodong.chiphi_khoanmuc_14 == True:
            #     context['hhjp_kk5_1'] = '実費'
            # else:
            #     context['hhjp_kk5_1'] = ''

            context['hhjp_0427'] = format_hoso.place_value(
                hopdonglaodong.sotien_khoanmuc_1 + hopdonglaodong.sotien_khoanmuc_2)
            context['hhjp_0428'] = format_hoso.place_value(
                hopdonglaodong.sotien_khoanmuc_3 + hopdonglaodong.sotien_khoanmuc_4 + hopdonglaodong.sotien_khoanmuc_5)
            context['hhjp_0429'] = format_hoso.place_value(hopdonglaodong.sotien_khoanmuc_6)
            context['hhjp_0430'] = format_hoso.place_value(hopdonglaodong.sotien_khoanmuc_7)
            if hopdonglaodong.chiphi_khoanmuc_7 == True:
                context['hhjp_0430_1'] = '実費'
            else:
                context['hhjp_0430_1'] = ''

            context['hhjp_0431'] = format_hoso.place_value(hopdonglaodong.sotien_khoanmuc_8)
            if hopdonglaodong.chiphi_khoanmuc_8 == True:
                context['hhjp_0431_1'] = '実費'
            else:
                context['hhjp_0431_1'] = ''

            context['h32'] = format_hoso.kiemtra(hopdonglaodong.sotien_khoanmuc_9)
            if hopdonglaodong.chiphi_khoanmuc_9 == True:
                context['hk'] = '実費'
                context['h1'] = 'Chi phí thực tế'
            else:
                context['hk'] = ''
                context['h1'] = ''

            context['hhjp_0433'] = format_hoso.place_value(hopdonglaodong.tong_sotien_khoanmuc)
            context['hhjp_0434'] = format_hoso.place_value(hopdonglaodong.thanhtoan_tienmat)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_19(self, ma_thuctapsinh, document):
        print('19')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_19")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            context = {}
            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            context['hhjp_0001_v'] = format_hoso.kiemtra(thuctapsinh.ten_tv_tts)
            if document.donhang_hoso.donhang_daingo.trocap_daotao_thangdau == u'Có':
                context['hhjp_0138_1'] = u'■'
                context['hhjp_0138_2'] = u'□'
                context['hhjp_0467'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.noidung_trocap_daotao_thangdau)
                context['hhjp_0467_v'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.noidung_trocap_daotao_thangdau_v)
            else:
                context['hhjp_0138_1'] = u'□'
                context['hhjp_0138_2'] = u'■'
                context['hhjp_0467'] = ''
                context['hhjp_0467_v'] = ''
            context['hhjp_0464'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.ghichu_trocap_daotao_thangdau)
            context['hhjp_0464_v'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.ghichu_trocap_daotao_thangdau_v)
            context['hhjp_0468'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.noidung_tts_tra_daingo_tienan)
            context['hhjp_0468_v'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.noidung_tts_tra_daingo_tienan_v)
            if document.donhang_hoso.donhang_daingo.trocap_daingo_tienan == u'Có':
                context['hhjp_0139_1'] = u'■'
                context['hhjp_0139_2'] = u'□'
                context['hhjp_0466'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.noidung_trocap_daingo_tienan)
                context['hhjp_0466_v'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.noidung_trocap_daingo_tienan_v)
            else:
                context['hhjp_0139_1'] = u'□'
                context['hhjp_0139_2'] = u'■'
                context['hhjp_0466'] = ''
                context['hhjp_0466_v'] = ''
            if document.donhang_hoso.donhang_daingo.tts_tra_daingo_tienan == u'Có':
                context['hhjp_0140_1'] = u'■'
                context['hhjp_0140_2'] = u'□'
                context['hhjp_0468'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.noidung_tts_tra_daingo_tienan)
                context['hhjp_0468_v'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.noidung_tts_tra_daingo_tienan_v)
            else:
                context['hhjp_0140_1'] = u'□'
                context['hhjp_0140_2'] = u'■'
                context['hhjp_0468'] = ''
                context['hhjp_0468_v'] = ''
            context['hhjp_0465'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.ghichu_daingo_tienan)
            context['hhjp_0465_v'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.ghichu_daingo_tienan_v)
            if document.donhang_hoso.donhang_daingo.trocap_daingo_nhao == u'Có':
                context['hhjp_0141_1'] = u'■'
                context['hhjp_0141_2'] = u'□'
                context['hhjp_0469'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.noidung_trocap_daingo_nhao)
                context['hhjp_0469_v'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.noidung_trocap_daingo_nhao_v)
            else:
                context['hhjp_0141_1'] = u'□'
                context['hhjp_0141_2'] = u'■'
                context['hhjp_0469'] = ''
                context['hhjp_0469_v'] = ''
            if document.donhang_hoso.donhang_daingo.trocap_daingo_nhao == u'Có':
                context['hhjp_0142_1'] = u'■'
                context['hhjp_0142_2'] = u'□'
                context['hhjp_0470_v'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.noidung_tts_tra_daingo_nhao_v)
                context['hhjp_0470'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.noidung_tts_tra_daingo_nhao)
            else:
                context['hhjp_0142_1'] = u'□'
                context['hhjp_0142_2'] = u'■'
                context['hhjp_0470'] = ''
                context['hhjp_0470_v'] = ''

            if document.donhang_hoso.donhang_daingo.loainha_daingo_nhao == 'Kí túc xá':
                docdata.remove_shape(u'id="9" name="Oval 9"')
                docdata.remove_shape(u'id="10" name="Oval 10"')
            elif document.donhang_hoso.donhang_daingo.loainha_daingo_nhao == 'Nhà thuê':
                docdata.remove_shape(u'id="11" name="Oval 11"')
                docdata.remove_shape(u'id="10" name="Oval 10"')
            elif document.donhang_hoso.donhang_daingo.loainha_daingo_nhao == 'Khác':
                docdata.remove_shape(u'id="9" name="Oval 9"')
                docdata.remove_shape(u'id="11" name="Oval 11"')
                context['hhjp_0473'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.khac_loainha_daingo_nhao)
                context['hhjp_0473_v'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.khac_loainha_daingo_nhao_v)
            else:
                docdata.remove_shape(u'id="10" name="Oval 10"')
                docdata.remove_shape(u'id="11" name="Oval 11"')
                docdata.remove_shape(u'id="9" name="Oval 9"')
                context[
                    'hhjp_0473'] = ''
                context[
                    'hhjp_0473_v'] = ''
            context['hhjp_0144'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.tennha_daingo_nhao)
            context['hhjp_0144_v'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.tennha_daingo_nhao_v)

            if document.donhang_hoso.donhang_daingo.sobuudien_daingo_nhao:
                context['bs_0018'] = document.donhang_hoso.donhang_daingo.sobuudien_daingo_nhao[
                                     0:3] + '-' + document.donhang_hoso.donhang_daingo.sobuudien_daingo_nhao[3:]
            else:
                context['bs_0018'] = ''

            context['hhjp_0145'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.diachi_daingo_nhao)
            context['hhjp_0145_v'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.diachi_daingo_nhao_v)
            context['hhjp_0474'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.dienthoai_daingo_nhao)
            context['hhjp_0475'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.matbang_daingo_nhao)
            context['hhjp_0476'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.songuoi_daingo_nhao)
            context['hhjp_0477'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.dientich_nguoi_daingo_nhao)
            context['hhjp_0478'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.muckhac_daingo_khac)
            context['hhjp_0478_v'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.muckhac_daingo_khac_v)
            if document.ngay_lapvanban:
                context['hhjp_0193'] = str(document.ngay_lapvanban.year) + '年' + \
                                       str(document.ngay_lapvanban.month) + '月' + \
                                       str(document.ngay_lapvanban.day) + '日'
                context['hhjp_0193_v'] = 'Ngày' + str(document.ngay_lapvanban.day) + \
                                         ' tháng ' + str(document.ngay_lapvanban.month) + \
                                         ' năm ' + str(document.ngay_lapvanban.year)
            else:
                context['hhjp_0193'] = '年　月　日'
                context['hhjp_0193_v'] = 'Ngày tháng năm'
            context['hhjp_0905'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_nguoidaidien)
            context['hhjp_0905_v'] = format_hoso.kiemtra(
                document.donhang_hoso.congtyphaicu_donhang.daidien_latinh_phaicu)
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_20(self, ma_thuctapsinh, document):
        print('20')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_20")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            context = {}
            hopdonglaodong = self.env['hopdong.hopdong'].search(
                [('thuctapsinh_hopdong', '=', thuctapsinh.id), ('donhang_hopdong', '=', document.donhang_hoso.id),
                 ('giaidoan_hopdong', '=', document.giaidoan_hoso)],
                limit=1)
            context['hhjp_0003'] = format_hoso.kiemtra(thuctapsinh.quoctich_tts.name_quoctich)
            context['hhjp_0003_v'] = format_hoso.kiemtra(thuctapsinh.quoctich_tts_v)
            context['hhjp_0115'] = format_hoso.nganhnghe(hopdonglaodong.loai_nghe_nganhnghe_tts.name_loaicongviec)
            context['hhjp_0115_v'] = format_hoso.kiemtra(hopdonglaodong.loai_nghe_nganhnghe_tts_viet)
            if document.ngay_lapvanban:
                context['hhjp_0193'] = str(document.ngay_lapvanban.year) + '年' + \
                                       str(document.ngay_lapvanban.month) + '月' + \
                                       str(document.ngay_lapvanban.day) + '日'
                context['hhjp_0193_v'] = 'Ngày ' + str(document.ngay_lapvanban.day) + \
                                         ' tháng ' + str(document.ngay_lapvanban.month) + \
                                         ' năm ' + str(document.ngay_lapvanban.year)
            else:
                context['hhjp_0193'] = '年　月　日'
                context['hhjp_0193_v'] = 'Ngày tháng năm'
            context['bs_0020'] = format_hoso.kiemtra(thuctapsinh.kehoach_ketthuc_venuoc)
            context['bs_0020_v'] = format_hoso.kiemtra(thuctapsinh.kehoach_ketthuc_venuoc_v)
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_21(self, ma_thuctapsinh, document):
        print('21')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_21")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            # daotaotruoc = self.env['daotao.daotaotruoc'].search(
            #     [('donhang_daotaotruoc', '=', document.donhang_hoso.id)], limit=1)
            context = {}
            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            context['hhjp_0002'] = format_hoso.kiemtra(thuctapsinh.ten_han_tts)
            context['hhjp_0924'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_anh_phaicu)
            context['hhjp_0924_v'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_viet_phaicu)
            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            context['hhjp_0010_v'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_viet_xnghiep)
            context['hhjp_0097_v'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_viet_nghiepdoan)
            context['hhjp_0097'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_han_nghiepdoan)

            context['hhjp_04941'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_1)
            context['hhjp_04941_v1'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_v_1)
            if document.donhang_hoso.ngaythu_1:
                context['hhjp_04951'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_1.day,
                                                                     document.donhang_hoso.ngaythu_1.month,
                                                                     document.donhang_hoso.ngaythu_1.year)
                context['hhjp_04951_v1'] = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_1.day,
                                                                        document.donhang_hoso.ngaythu_1.month,
                                                                        document.donhang_hoso.ngaythu_1.year)
            else:
                context['hhjp_04951'] = '年　月　日'
                context['hhjp_04951_v1'] = 'Ngày tháng năm'
            context['bs_0021'] = format_hoso.kiemtra(document.donhang_hoso.sotien_viet_1)
            context['hhjp_0491'] = format_hoso.kiemtra(document.donhang_hoso.sotien_nhat_1)

            context['hhjp_04942'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_2)
            context['hhjp_0494_v2'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_v_2)
            if document.donhang_hoso.ngaythu_2:
                context['hhjp_04952'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_2.day,
                                                                     document.donhang_hoso.ngaythu_2.month,
                                                                     document.donhang_hoso.ngaythu_2.year)
                context['hhjp_0495_v2'] = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_2.day,
                                                                       document.donhang_hoso.ngaythu_2.month,
                                                                       document.donhang_hoso.ngaythu_2.year)
            else:
                context['hhjp_04952'] = '年　月　日'
                context['hhjp_0495_v2'] = 'Ngày tháng năm'

            context['bs_0022'] = format_hoso.kiemtra(document.donhang_hoso.sotien_viet_2)
            context['hhjp_0492'] = format_hoso.kiemtra(document.donhang_hoso.sotien_nhat_2)

            context['hhjp_04943'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_3)
            context['hhjp_0494_v3'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_v_3)
            if document.donhang_hoso.ngaythu_3:
                context['hhjp_04953'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_3.day,
                                                                     document.donhang_hoso.ngaythu_3.month,
                                                                     document.donhang_hoso.ngaythu_3.year)
                context['hhjp_0495_v3'] = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_3.day,
                                                                       document.donhang_hoso.ngaythu_3.month,
                                                                       document.donhang_hoso.ngaythu_3.year)
            else:
                context['hhjp_04953'] = '年　月　日'
                context['hhjp_0495_v3'] = 'Ngày tháng năm'

            context['bs_0023'] = format_hoso.kiemtra(document.donhang_hoso.sotien_viet_3)
            context['hhjp_0493'] = format_hoso.kiemtra(document.donhang_hoso.sotien_nhat_3)

            context['hhjp_04944'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_4)
            context['hhjp_0494_v4'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_v_4)
            if document.donhang_hoso.ngaythu_4:
                context['hhjp_04954'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_4.day,
                                                                     document.donhang_hoso.ngaythu_4.month,
                                                                     document.donhang_hoso.ngaythu_4.year)
                context['hhjp_0495_v4'] = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_4.day,
                                                                       document.donhang_hoso.ngaythu_4.month,
                                                                       document.donhang_hoso.ngaythu_4.year)
            else:
                context['hhjp_04954'] = '年　月　日'
                context['hhjp_0495_v4'] = 'Ngày tháng năm'
            context['bs_0024'] = format_hoso.kiemtra(document.donhang_hoso.sotien_viet_4)
            context['hhjp_0494'] = format_hoso.kiemtra(document.donhang_hoso.sotien_nhat_4)

            context['hhjp_04945'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_5)
            context['hhjp_0494_v5'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_v_5)
            if document.donhang_hoso.ngaythu_5:
                context['hhjp_04955'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_5.day,
                                                                     document.donhang_hoso.ngaythu_5.month,
                                                                     document.donhang_hoso.ngaythu_5.year)
                context['hhjp_0495_v5'] = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_5.day,
                                                                       document.donhang_hoso.ngaythu_5.month,
                                                                       document.donhang_hoso.ngaythu_5.year)
            else:
                context['hhjp_04955'] = '年　月　日'
                context['hhjp_0495_v5'] = 'Ngày tháng năm'
            context['bs_0025'] = format_hoso.kiemtra(document.donhang_hoso.sotien_viet_5)
            context['hhjp_0495'] = format_hoso.kiemtra(document.donhang_hoso.sotien_nhat_5)

            context['bs_261'] = format_hoso.kiemtra(document.donhang_hoso.tong_sotien_v)
            context['hhjp_0490'] = format_hoso.kiemtra(document.donhang_hoso.tong_sotien_n)

            context['hhjp_0585_1'] = format_hoso.kiemtra(
                document.donhang_hoso.tencoquan_ngoai_1.ten_cquan_cbi_nuocngoai)
            # context['hhjp_0585_1'] = format_hoso.kiemtra(document.donhang_hoso.tencoquan_1.ten_cquan_cbi_nuocngoai_n)
            context['tnc_0006_1'] = format_hoso.kiemtra(document.donhang_hoso.vaitro_phaicu_ngoai_1)
            # context['tnc_0006_1'] = format_hoso.kiemtra(document.donhang_hoso.vaitro_phaicu_ngoai_v_1)
            context['tnc_0007_1'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_ngoai_1)
            context['tnc_0007_v1'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_ngoai_v_1)
            if document.donhang_hoso.ngaythu_ngoai_1:
                context['tnc_0008_1'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_ngoai_1.day,
                                                                     document.donhang_hoso.ngaythu_ngoai_1.month,
                                                                     document.donhang_hoso.ngaythu_ngoai_1.year)
                context['tnc_0008_v1'] = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_ngoai_1)
            else:
                context['tnc_0008_1'] = '年　月　日'
                context['tnc_0008_v1'] = 'Ngày tháng năm'

            context['tnc_0009_1'] = format_hoso.kiemtra(document.donhang_hoso.sotien_viet_ngoai_1)
            context['tnc_0010_1'] = format_hoso.kiemtra(document.donhang_hoso.sotien_nhat_ngoai_1)

            context['hhjp_0585_2'] = format_hoso.kiemtra(
                document.donhang_hoso.tencoquan_ngoai_1.ten_cquan_cbi_nuocngoai)
            # context['hhjp_0585_2'] = format_hoso.kiemtra(document.donhang_hoso.tencoquan_1.ten_cquan_cbi_nuocngoai_n)
            context['tnc_0006_2'] = format_hoso.kiemtra(document.donhang_hoso.vaitro_phaicu_ngoai_2)
            # context['tnc_0006_2'] = format_hoso.kiemtra(document.donhang_hoso.vaitro_phaicu_ngoai_v_2)
            context['tnc_0007_2'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_ngoai_2)
            context['tnc_0007_v2'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_ngoai_v_2)
            if document.donhang_hoso.ngaythu_ngoai_2:
                context['tnc_0008_2'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_ngoai_2.day,
                                                                     document.donhang_hoso.ngaythu_ngoai_2.month,
                                                                     document.donhang_hoso.ngaythu_ngoai_2.year)
                context['tnc_0008_v2'] = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_ngoai_2)
            else:
                context['tnc_0008_2'] = '年　月　日'
                context['tnc_0008_v2'] = 'Ngày tháng năm'

            context['tnc_0009_2'] = format_hoso.kiemtra(document.donhang_hoso.sotien_viet_ngoai_2)
            context['tnc_0010_2'] = format_hoso.kiemtra(document.donhang_hoso.sotien_nhat_ngoai_2)

            context['hhjp_0585_3'] = format_hoso.kiemtra(
                document.donhang_hoso.tencoquan_ngoai_1.ten_cquan_cbi_nuocngoai)
            # context['hhjp_0585_3'] = format_hoso.kiemtra(document.donhang_hoso.tencoquan_1.ten_cquan_cbi_nuocngoai_n)
            context['tnc_0006_3'] = format_hoso.kiemtra(document.donhang_hoso.vaitro_phaicu_ngoai_3)
            # context['tnc_0006_3'] = format_hoso.kiemtra(document.donhang_hoso.vaitro_phaicu_ngoai_v_3)
            context['tnc_0007_3'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_ngoai_3)
            context['tnc_0007_v3'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_ngoai_v_3)
            if document.donhang_hoso.ngaythu_ngoai_3:
                context['tnc_0008_3'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_ngoai_3.day,
                                                                     document.donhang_hoso.ngaythu_ngoai_3.month,
                                                                     document.donhang_hoso.ngaythu_ngoai_3.year)
                context['tnc_0008_v3'] = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_ngoai_3)
            else:
                context['tnc_0008_3'] = '年　月　日'
                context['tnc_0008_v3'] = 'Ngày tháng năm'

            context['tnc_0009_3'] = format_hoso.kiemtra(document.donhang_hoso.sotien_viet_ngoai_3)
            context['tnc_0010_3'] = format_hoso.kiemtra(document.donhang_hoso.sotien_nhat_ngoai_3)

            context['hhjp_0585_4'] = format_hoso.kiemtra(
                document.donhang_hoso.tencoquan_ngoai_1.ten_cquan_cbi_nuocngoai)
            # context['hhjp_0585_4'] = format_hoso.kiemtra(document.donhang_hoso.tencoquan_1.ten_cquan_cbi_nuocngoai_n)
            context['tnc_0006_4'] = format_hoso.kiemtra(document.donhang_hoso.vaitro_phaicu_ngoai_4)
            # context['tnc_0006_4'] = format_hoso.kiemtra(document.donhang_hoso.vaitro_phaicu_ngoai_v_4)
            context['tnc_0007_4'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_ngoai_4)
            context['tnc_0007_v4'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_ngoai_v_4)
            if document.donhang_hoso.ngaythu_ngoai_4:
                context['tnc_0008_4'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_ngoai_4.day,
                                                                     document.donhang_hoso.ngaythu_ngoai_4.month,
                                                                     document.donhang_hoso.ngaythu_ngoai_4.year)
                context['tnc_0008_v4'] = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_ngoai_4)
            else:
                context['tnc_0008_4'] = '年　月　日'
                context['tnc_0008_v4'] = 'Ngày tháng năm'

            context['tnc_0009_4'] = format_hoso.kiemtra(document.donhang_hoso.sotien_viet_ngoai_4)
            context['tnc_0010_4'] = format_hoso.kiemtra(document.donhang_hoso.sotien_nhat_ngoai_4)

            context['hhjp_0585_5'] = format_hoso.kiemtra(
                document.donhang_hoso.tencoquan_ngoai_1.ten_cquan_cbi_nuocngoai)
            # context['hhjp_0585_5'] = format_hoso.kiemtra(document.donhang_hoso.tencoquan_1.ten_cquan_cbi_nuocngoai_n)
            context['tnc_0006_5'] = format_hoso.kiemtra(document.donhang_hoso.vaitro_phaicu_ngoai_5)
            # context['tnc_0006_5'] = format_hoso.kiemtra(document.donhang_hoso.vaitro_phaicu_ngoai_v_5)
            context['tnc_0007_5'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_ngoai_5)
            context['tnc_0007_v5'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_ngoai_v_5)
            if document.donhang_hoso.ngaythu_ngoai_5:
                context['tnc_0008_5'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_ngoai_5.day,
                                                                     document.donhang_hoso.ngaythu_ngoai_5.month,
                                                                     document.donhang_hoso.ngaythu_ngoai_5.year)
                context['tnc_0008_v5'] = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_ngoai_5)
            else:
                context['tnc_0008_5'] = '年　月　日'
                context['tnc_0008_v5'] = 'Ngày tháng năm'

            context['tnc_0009_5'] = format_hoso.kiemtra(document.donhang_hoso.sotien_viet_ngoai_5)
            context['tnc_0010_5'] = format_hoso.kiemtra(document.donhang_hoso.sotien_nhat_ngoai_5)

            context['tnc_0011'] = format_hoso.kiemtra(document.donhang_hoso.tong_ngoai_v)
            context['tnc_0012'] = format_hoso.kiemtra(document.donhang_hoso.tong_ngoai_n)

            context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
            if document.ngay_lapvanban != False:
                context['hhjp_0193_v'] = 'Ngày ' + str(document.ngay_lapvanban.day) + \
                                         ' tháng ' + str(document.ngay_lapvanban.month) + \
                                         ' năm ' + str(document.ngay_lapvanban.year)
            else:
                context['hhjp_0193_v'] = 'Ngày tháng năm'
            context[
                'hhjp_0376'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_nguoidaidien)
            context[
                'hhjp_0376_cv'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.chucvu_han_phaicu)
            context[
                'hhjp_0510'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.daidien_latinh_phaicu)
            context[
                'hhjp_0510_cv'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.chucvu_latinh_phaicu)
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_22(self, ma_thuctapsinh, document):
        print('22')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_22")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            hopdonglaodong = self.env['hopdong.hopdong'].search(
                [('thuctapsinh_hopdong', '=', thuctapsinh.id), ('donhang_hopdong', '=', document.donhang_hoso.id),
                 ('giaidoan_hopdong', '=', document.giaidoan_hoso)],
                limit=1)
            context = {}
            context['hhjp_0114'] = format_hoso.kiemtra(hopdonglaodong.macongviec_tts)
            context['hhjp_0115'] = format_hoso.nganhnghe(hopdonglaodong.loai_nghe_nganhnghe_tts.name_loaicongviec)
            context['hhjp_0116'] = format_hoso.nganhnghe(hopdonglaodong.congviec_nganhnghe_tts.name_congviec)
            context['hhjp_0117'] = hopdonglaodong.macongviec_congviec if hopdonglaodong.macongviec_congviec else ''
            context['hhjp_0118'] = format_hoso.nganhnghe(hopdonglaodong.loai_nghe_nganhnghe_congviec.name_loaicongviec)
            context['hhjp_0119'] = format_hoso.nganhnghe(hopdonglaodong.congviec_nganhnghe_congviec.name_congviec)
            context['hhjp_0580'] = Listing(
                thuctapsinh.boicanh_daotao_kythuat) if thuctapsinh.boicanh_daotao_kythuat else ''
            context['hhjp_0581'] = Listing(
                thuctapsinh.thuctapkinang_canthiet) if thuctapsinh.thuctapkinang_canthiet else ''
            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            context['hhjp_0014'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep)

            # context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_23(self, ma_thuctapsinh, document):
        print('23')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_23")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            context = {}
            context['hhjp_7198'] = format_hoso.kiemtra(document.donhang_hoso.year_expire)

            context['hhjp_0924'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_anh_phaicu)
            context['hhjp_0500'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.sogiayphep_phaicu)
            context['hhjp_0501'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_nguoidaidien)
            context['hhjp_0502'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.diachi_tienganh)
            context['hhjp_0503'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.fax_phaicu)

            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            context['hhjp_0097'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_han_nghiepdoan)
            context['hhjp_0923'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_viet_nghiepdoan)
            context['hhjp_0100'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.so_giayphep_nghiepdoan)
            context['hhjp_0103'] = format_hoso.kiemtra(
                document.donhang_hoso.nghiepdoan_donhang.ten_daidien_han_nghiepdoan)
            context['hhjp_0098'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.diachi_nghiepdoan_anh)
            context['hhjp_0099'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.fax_nghiepdoan)

            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            context['hhjp_0014'] = format_hoso.kiemtra(document.xinghiep_hoso.title_hoten_nguoidaidien_xinghiep)
            context['hhjp_0010_v'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_viet_xnghiep)
            context['hhjp_0011'] = format_hoso.kiemtra(document.xinghiep_hoso.diachi_viet_xnghiep)
            context['hhjp_0012'] = format_hoso.kiemtra(document.xinghiep_hoso.fax)

            if document.ngay_lapvanban:
                context['hhjp_0193'] = intern_utils.date_time_in_jp(document.ngay_lapvanban.day,
                                                                    document.ngay_lapvanban.month,
                                                                    document.ngay_lapvanban.year)
            else:
                context['hhjp_0193'] = '年　月　日'

            table_tts = []
            for thuctapsinh_hoso in document.thuctapsinh_hoso:
                infor = {}
                infor['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh_hoso.ten_lt_tts)
                infor['hhjp_0004'] = format_hoso.kiemtra(thuctapsinh_hoso.quoctich_tts.name_quoctich)
                infor['hhjp_0006'] = format_hoso.kiemtra(thuctapsinh_hoso.gtinh_tts)
                infor['hhjp_0345'] = format_hoso.kiemtra(thuctapsinh_hoso.nganhnghe_xnpc)
                infor['hhjp_0902_Y'] = format_hoso.kiemtra(thuctapsinh_hoso.donhang_tts.year_departure_doc)
                infor['hhjp_0902_M'] = format_hoso.kiemtra(thuctapsinh_hoso.donhang_tts.month_departure_doc)
                table_tts.append(infor)
            context['tbl_danhsachtts'] = table_tts

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_24(self, ma_thuctapsinh, document):
        print('24')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_24")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            context = {}
            kehoachdoatao = self.env['thuctapsinh.kehoachdoatao'].search([])
            thuctapsinh_thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search(
                [('donhang_tts', '=', document.donhang_hoso.id)])
            table_thuctapsinh = []
            for thuctapsinh_thuctapsinh in thuctapsinh_thuctapsinh:
                info = {}
                info['hhjp_0911'] = format_hoso.kiemtra(thuctapsinh_thuctapsinh.sochungnhan_namnhat)
                if thuctapsinh_thuctapsinh.ngaychungnhan_namnhat != False:
                    info['hhjp_0502_Y'] = thuctapsinh_thuctapsinh.ngaychungnhan_namnhat.year
                    info['hhjp_0502_M'] = thuctapsinh_thuctapsinh.ngaychungnhan_namnhat.month
                    info['hhjp_0502_D'] = thuctapsinh_thuctapsinh.ngaychungnhan_namnhat.day
                else:
                    info['hhjp_0502_Y'] = ''
                    info['hhjp_0502_M'] = ''
                    info['hhjp_0502_D'] = ''

                for kehoach in kehoachdoatao:
                    dapan = 'hhjp_0113_'
                    if thuctapsinh.daotao_hientai.id == kehoach.id:
                        dapan = dapan + kehoach.dapan_kehoachdaotao
                        info[dapan] = '■ %s' % (thuctapsinh.daotao_hientai.name_kehoachdaotao)
                    else:
                        dapan = dapan + kehoach.dapan_kehoachdaotao
                        info[dapan] = '□ %s' % (kehoach.name_kehoachdaotao)
                info['hhjp_0584'] = format_hoso.convert_date(thuctapsinh_thuctapsinh.ketthuc_kehoach_namnhat)
                info['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh_thuctapsinh.ten_lt_tts)
                info['hhjp_0003'] = format_hoso.kiemtra(thuctapsinh_thuctapsinh.quoctich_tts.name_quoctich)
                info['hhjp_0004'] = format_hoso.convert_date(thuctapsinh_thuctapsinh.ngaysinh_tts)
                info['hhjp_0006'] = format_hoso.gender_check(thuctapsinh.gtinh_tts)
                table_thuctapsinh.append(info)
            context['tbl_danhsachtts'] = table_thuctapsinh
            context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_25(self, ma_thuctapsinh, document):
        print('25')
        sothuctapsinh = int(len(document.thuctapsinh_hoso))
        if sothuctapsinh <= 3:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_25")], limit=1)
            print('file_25')
        elif sothuctapsinh > 3:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_25_1")], limit=1)
            print('file_25_1')

        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))

            docdata = DocxTemplate(stream)

            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)

            context = {}
            if sothuctapsinh <= 3:
                item = 1
                for thuctapsinh_hoso in document.thuctapsinh_hoso:
                    if item == 1:
                        context['hhjp_0001_1'] = format_hoso.kiemtra(thuctapsinh_hoso.ten_lt_tts)
                        item += 1
                    elif item == 2:
                        context['hhjp_0001_2'] = format_hoso.kiemtra(thuctapsinh_hoso.ten_lt_tts)
                        item += 1
                    elif item == 3:
                        context['hhjp_0001_3'] = format_hoso.kiemtra(thuctapsinh_hoso.ten_lt_tts)
                        break
            elif sothuctapsinh > 3:
                table_thuctapsinh = []
                for thuctapsinh_hoso in document.thuctapsinh_hoso:
                    infor_thuctapsinh = {}
                    infor_thuctapsinh['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh_hoso.ten_lt_tts)
                    table_thuctapsinh.append(infor_thuctapsinh)

                context['tbl_tts'] = table_thuctapsinh

            if thuctapsinh.noidung_xacminh_a_donhang_xinghiep == True:
                context['hhjp_tich_A'] = '■' if thuctapsinh.noidung_xacminh_a_donhang_xinghiep else '□'

            context['hhjp_tich_B'] = '■' if thuctapsinh.noidung_xacminh_b_donhang_xinghiep else '□'

            if thuctapsinh.noidung_xacminh_b_donhang_xinghiep == True:
                context['hhjp_tich_a'] = '■' if thuctapsinh.noidung_xacminh_b_a_donhang_xinghiep else '□'
                context['hhjp_tich_b'] = '■' if thuctapsinh.noidung_xacminh_b_b_donhang_xinghiep else '□'
                context['hhjp_tich_c'] = '■' if thuctapsinh.noidung_xacminh_b_c_donhang_xinghiep else '□'
            else:
                context['hhjp_tich_a'] = '□'
                context['hhjp_tich_b'] = '□'
                context['hhjp_tich_c'] = '□'

            # context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
            # context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            # context['hhjp_0014'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep)
            # context['xn_104'] = format_hoso.kiemtra(document.xinghiep_hoso.chucvu_daidien_han)
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_26(self, ma_thuctapsinh, document):
        print('26')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_26")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            thuctapsinh_danhsach = self.env['thuctapsinh.thuctapsinh'].search(
                [('donhang_tts', '=', thuctapsinh.donhang_tts.id)])
            hopdonglaodong = self.env['hopdong.hopdong'].search(
                [('thuctapsinh_hopdong', '=', thuctapsinh.id), ('donhang_hopdong', '=', document.donhang_hoso.id),
                 ('giaidoan_hopdong', '=', document.giaidoan_hoso)],
                limit=1)
            context = {}
            table_tts = []
            for tts_hoso in document.thuctapsinh_hoso:
                infor = {}
                infor['hhjp_0001'] = format_hoso.kiemtra(tts_hoso.ten_lt_tts)
                infor['hhjp_0002'] = format_hoso.kiemtra(tts_hoso.ten_han_tts)
                infor['hhjp_0115'] = format_hoso.nganhnghe(hopdonglaodong.loai_nghe_nganhnghe_tts.name_loaicongviec)
                table_tts.append(infor)
            context['tbl_tts'] = table_tts
            context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)

            context['hhjp_0204'] = format_hoso.kiemtra(thuctapsinh.xinghiep_phaicu)
            context['hhjp_0900'] = format_hoso.kiemtra(thuctapsinh.bophan_xinghiep_phaicu)
            context['hhjp_0901'] = format_hoso.kiemtra(thuctapsinh.nganhnghe_xinghiep_phaicu)

            context['hhjp_0207'] = format_hoso.kiemtra(thuctapsinh.daidien_phaicu)
            context['hhjp_0207_1'] = format_hoso.kiemtra(thuctapsinh.daidien_chucvu_phaicu)

            if thuctapsinh.quatrinh_danden_thuctap_kynang == 'Xí nghiệp phái cử tiến cử':
                context['hhjp_0332_1'] = '■'
                context['hhjp_0332_2'] = '□'
                context['hhjp_0332_3'] = '□'
                context['bs_0010'] = ''
                context['bs_0011'] = format_hoso.kiemtra(thuctapsinh.quatrinh_danden_thuctap_kynang_tiencu_lydo)
            elif thuctapsinh.quatrinh_danden_thuctap_kynang == 'Nguyện vọng thực tập sinh':
                context['hhjp_0332_2'] = '■'
                context['hhjp_0332_1'] = '□'
                context['hhjp_0332_3'] = '□'
                context['bs_0010'] = ''
                context['bs_0011'] = ''
            elif thuctapsinh.quatrinh_danden_thuctap_kynang == 'Lý do khác':
                context['hhjp_0332_3'] = '■'
                context['hhjp_0332_1'] = '□'
                context['hhjp_0332_2'] = '□'
                context['bs_0011'] = ''
                context['bs_0010'] = format_hoso.kiemtra(thuctapsinh.quatrinh_danden_thuctap_kynang_khac_lydo)
            else:
                context['hhjp_0332_3'] = '□'
                context['hhjp_0332_1'] = '□'
                context['hhjp_0332_2'] = '□'
                context['bs_0011'] = ''
                context['bs_0010'] = ''

            if thuctapsinh.daingo_thuctap_xinghiep == 'Tiếp tục duy trì đãi ngộ với thực tập sinh':
                context['hhjp_0333_1'] = '■'
                context['hhjp_0333_2'] = '□'
                context['hhjp_0333_3'] = '□'
                context['bs_0012'] = ''
            elif thuctapsinh.daingo_thuctap_xinghiep == 'Nghỉ việc':
                context['hhjp_0333_2'] = '■'
                context['hhjp_0333_1'] = '□'
                context['hhjp_0333_3'] = '□'
                context['bs_0012'] = ''
            elif thuctapsinh.daingo_thuctap_xinghiep == 'Khác':
                context['hhjp_0333_3'] = '■'
                context['hhjp_0333_1'] = '□'
                context['hhjp_0333_2'] = '□'
                context['bs_0012'] = format_hoso.kiemtra(thuctapsinh.daingo_khac_thuctap_xinghiep)
            else:
                context['hhjp_0333_3'] = '□'
                context['hhjp_0333_1'] = '□'
                context['hhjp_0333_2'] = '□'
                context['bs_0012'] = ''

            if thuctapsinh.kehoach_ketthuc_thuctap_xinghiep == 'Quay lại xí nghiệp phái cử':
                context['hhjp_0334_1'] = '■'
                context['hhjp_0334_2'] = '□'
                context['hhjp_0334_3'] = '□'
                context['hhjp_0204_kt'] = format_hoso.kiemtra(thuctapsinh.xinghiep_phaicu)
                context['hhjp_0900_kt'] = format_hoso.kiemtra(thuctapsinh.bophan_xinghiep_phaicu)
                context['hhjp_0901_kt'] = format_hoso.kiemtra(thuctapsinh.nganhnghe_xinghiep_phaicu)
            elif thuctapsinh.kehoach_ketthuc_thuctap_xinghiep == 'Không quay lại xí nghiệp phái cử':
                context['hhjp_0334_2'] = '■'
                context['hhjp_0334_1'] = '□'
                context['hhjp_0334_3'] = '□'
                context['hhjp_0204_kt'] = ''
                context['hhjp_0900_kt'] = ''
                context['hhjp_0901_kt'] = ''
            elif thuctapsinh.kehoach_ketthuc_thuctap_xinghiep == 'Chưa xác định':
                context['hhjp_0334_3'] = '■'
                context['hhjp_0334_1'] = '□'
                context['hhjp_0334_2'] = '□'
                context['hhjp_0204_kt'] = ''
                context['hhjp_0900_kt'] = ''
                context['hhjp_0901_kt'] = ''
            else:
                context['hhjp_0334_3'] = '□'
                context['hhjp_0334_1'] = '□'
                context['hhjp_0334_2'] = '□'
                context['hhjp_0204_kt'] = ''
                context['hhjp_0900_kt'] = ''
                context['hhjp_0901_kt'] = ''

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_27(self, ma_thuctapsinh, document):
        print('27')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_27")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            kehoachdoatao = self.env['thuctapsinh.kehoachdoatao'].search([])
            thuctapsinh_danhsach = document.thuctapsinh_hoso

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_28(self, ma_thuctapsinh, document):
        print('28')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_28")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            hopdonglaodong = self.env['hopdong.hopdong'].search(
                [('thuctapsinh_hopdong', '=', thuctapsinh.id), ('donhang_hopdong', '=', document.donhang_hoso.id),
                 ('giaidoan_hopdong', '=', document.giaidoan_hoso)],
                limit=1)

            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            context['hhjp_0114'] = format_hoso.kiemtra(thuctapsinh.ma_congviec_chuyennhuong)
            context['hhjp_0115'] = format_hoso.nganhnghe(hopdonglaodong.loai_nghe_nganhnghe_tts.name_loaicongviec)
            context['hhjp_0116'] = format_hoso.nganhnghe(hopdonglaodong.congviec_nganhnghe_tts.name_congviec)
            context[
                'hhjp_0117'] = thuctapsinh.ma_congviec_nhieucongviec if thuctapsinh.ma_congviec_nhieucongviec else ''
            context['hhjp_0118'] = format_hoso.nganhnghe(hopdonglaodong.loai_nghe_nganhnghe_congviec.name_loaicongviec)
            context['hhjp_0119'] = format_hoso.nganhnghe(hopdonglaodong.congviec_nganhnghe_congviec.name_congviec)
            context['bs_0050'] = Listing(
                thuctapsinh.ghichu_congviec_kinang_lienquan) if thuctapsinh.ghichu_congviec_kinang_lienquan else ''
            context['bs_0051'] = Listing(
                thuctapsinh.ghichu_daotao_kithuat_nganhnghe) if thuctapsinh.ghichu_daotao_kithuat_nganhnghe else ''

            context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_29(self, ma_thuctapsinh, document):
        print('29')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_29")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}
            table_thuctapsinh = []
            for thuctapsinh_thuctapsinh in document.thuctapsinh_hoso:
                info = {}
                info['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh_thuctapsinh.ten_lt_tts)
                info['hhjp_0003'] = format_hoso.kiemtra(thuctapsinh_thuctapsinh.quoctich_tts.name_quoctich)
                info['hhjp_0004'] = format_hoso.convert_date(thuctapsinh_thuctapsinh.ngaysinh_tts)
                info['hhjp_0006'] = format_hoso.gender_check(thuctapsinh_thuctapsinh.gtinh_tts)

                info['hhjp_0113_A'] = '□ A 第１号企業単独型技能実習'
                info['hhjp_0113_B'] = '□ B 第２号企業単独型技能実習'
                info['hhjp_0113_C'] = '□ C 第３号企業単独型技能実習'
                if document.giaidoan_hoso == '1 go':
                    info['hhjp_0113_D'] = '■ D 第１号団体監理型技能実習'
                    info['hhjp_0113_E'] = '□ E 第２号団体監理型技能実習'
                    info['hhjp_0113_F'] = '□ F 第３号団体監理型技能実習'
                elif document.giaidoan_hoso == '2 go':
                    info['hhjp_0113_D'] = '□ D 第１号団体監理型技能実習'
                    info['hhjp_0113_E'] = '■ E 第２号団体監理型技能実習'
                    info['hhjp_0113_F'] = '□ F 第３号団体監理型技能実習'
                elif document.giaidoan_hoso == '3 go':
                    info['hhjp_0113_D'] = '□ D 第１号団体監理型技能実習'
                    info['hhjp_0113_E'] = '□ E 第２号団体監理型技能実習'
                    info['hhjp_0113_F'] = '■ F 第３号団体監理型技能実習'
                else:
                    info['hhjp_0113_D'] = '□ D 第１号団体監理型技能実習'
                    info['hhjp_0113_E'] = '□ E 第２号団体監理型技能実習'
                    info['hhjp_0113_F'] = '□ F 第３号団体監理型技能実習'
                table_thuctapsinh.append(info)
            context['tbl_thuctapsinh'] = table_thuctapsinh
            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_30(self, ma_thuctapsinh, document):
        print('30')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_30")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            danhsachhoso = self.env['thuctapsinh.danhsachnew'].search(
                [('thuctapsinh_danhsach', '=', thuctapsinh.id), ('giaidoan_danhsach', '=', document.giaidoan_hoso)],
                limit=1)

            context = {}
            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            context['hhjp_0113'] = format_hoso.kiemtra(thuctapsinh.mucdichnhapcu_tts)

            if document.giaidoan_hoso == '1 go':
                context['hhjp_go'] = 1
            elif document.giaidoan_hoso == '2 go':
                context['hhjp_go'] = 2
            elif document.giaidoan_hoso == '3 go':
                context['hhjp_go'] = 3
            else:
                context['hhjp_go'] = 1

            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            context['hhjp_0097'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_han_nghiepdoan)

            if danhsachhoso.file_1 == u'Có':
                docdata.remove_shape(u'id="3" name="Oval 3"')
            elif danhsachhoso.file_1 == u'Không':
                docdata.remove_shape(u'id="2" name="Oval 2"')
            else:
                docdata.remove_shape(u'id="2" name="Oval 2"')
                docdata.remove_shape(u'id="3" name="Oval 3"')

            if danhsachhoso.file_2 == u'Có':
                docdata.remove_shape(u'id="6" name="Oval 6"')
            elif danhsachhoso.file_2 == u'Không':
                docdata.remove_shape(u'id="5" name="Oval 5"')
            else:
                docdata.remove_shape(u'id="5" name="Oval 5"')
                docdata.remove_shape(u'id="6" name="Oval 6"')

            if danhsachhoso.file_3 == u'Có':
                docdata.remove_shape(u'id="8" name="Oval 8"')
            elif danhsachhoso.file_3 == u'Không':
                docdata.remove_shape(u'id="7" name="Oval 7"')
            else:
                docdata.remove_shape(u'id="8" name="Oval 8"')
                docdata.remove_shape(u'id="7" name="Oval 7"')

            if danhsachhoso.file_4 == u'Có':
                docdata.remove_shape(u'id="10" name="Oval 10"')
            elif danhsachhoso.file_4 == u'Không':
                docdata.remove_shape(u'id="9" name="Oval 9"')
            else:
                docdata.remove_shape(u'id="10" name="Oval 10"')
                docdata.remove_shape(u'id="9" name="Oval 9"')

            if danhsachhoso.file_5 == u'Có':
                docdata.remove_shape(u'id="12" name="Oval 12"')
            elif danhsachhoso.file_5 == u'Không':
                docdata.remove_shape(u'id="11" name="Oval 11"')
            else:
                docdata.remove_shape(u'id="12" name="Oval 12"')
                docdata.remove_shape(u'id="11" name="Oval 11"')

            if danhsachhoso.file_6 == u'Có':
                docdata.remove_shape(u'id="14" name="Oval 14"')
            elif danhsachhoso.file_6 == u'Không':
                docdata.remove_shape(u'id="13" name="Oval 13"')
            else:
                docdata.remove_shape(u'id="14" name="Oval 14"')
                docdata.remove_shape(u'id="13" name="Oval 13"')

            if danhsachhoso.file_7 == u'Có':
                docdata.remove_shape(u'id="16" name="Oval 16"')
            elif danhsachhoso.file_7 == u'Không':
                docdata.remove_shape(u'id="15" name="Oval 15"')
            else:
                docdata.remove_shape(u'id="15" name="Oval 15"')
                docdata.remove_shape(u'id="16" name="Oval 16"')

            if danhsachhoso.file_8 == u'Có':
                docdata.remove_shape(u'id="18" name="Oval 18"')
            elif danhsachhoso.file_8 == u'Không':
                docdata.remove_shape(u'id="17" name="Oval 17"')
            else:
                docdata.remove_shape(u'id="18" name="Oval 18"')
                docdata.remove_shape(u'id="17" name="Oval 17"')

            if danhsachhoso.file_9 == u'Có':
                docdata.remove_shape(u'id="20" name="Oval 20"')
            elif danhsachhoso.file_9 == u'Không':
                docdata.remove_shape(u'id="19" name="Oval 19"')
            else:
                docdata.remove_shape(u'id="20" name="Oval 20"')
                docdata.remove_shape(u'id="19" name="Oval 19"')

            if danhsachhoso.file_10 == u'Có':
                docdata.remove_shape(u'id="22" name="Oval 22"')
            elif danhsachhoso.file_10 == u'Không':
                docdata.remove_shape(u'id="21" name="Oval 21"')
            else:
                docdata.remove_shape(u'id="22" name="Oval 22"')
                docdata.remove_shape(u'id="21" name="Oval 21"')

            if danhsachhoso.file_11 == u'Có':
                docdata.remove_shape(u'id="24" name="Oval 24"')
            elif danhsachhoso.file_11 == u'Không':
                docdata.remove_shape(u'id="23" name="Oval 23"')
            else:
                docdata.remove_shape(u'id="24" name="Oval 24"')
                docdata.remove_shape(u'id="23" name="Oval 23"')

            context['gc_11'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_11)

            if danhsachhoso.file_12 == u'Có':
                docdata.remove_shape(u'id="26" name="Oval 26"')
            elif danhsachhoso.file_12 == u'Không':
                docdata.remove_shape(u'id="25" name="Oval 25"')
            else:
                docdata.remove_shape(u'id="26" name="Oval 26"')
                docdata.remove_shape(u'id="25" name="Oval 25')

            context['gc_12'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_12)

            if danhsachhoso.file_13 == u'Có':
                docdata.remove_shape(u'id="28" name="Oval 28"')
            elif danhsachhoso.file_13 == u'Không':
                docdata.remove_shape(u'id="27" name="Oval 27"')
            else:
                docdata.remove_shape(u'id="27" name="Oval 27"')
                docdata.remove_shape(u'id="28" name="Oval 28')

            context['gc_13'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_13)

            if danhsachhoso.file_14 == u'Có':
                docdata.remove_shape(u'id="30" name="Oval 30"')
            elif danhsachhoso.file_14 == u'Không':
                docdata.remove_shape(u'id="29" name="Oval 29"')
            else:
                docdata.remove_shape(u'id="30" name="Oval 30"')
                docdata.remove_shape(u'id="29" name="Oval 29')

            context['gc_14'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_14)

            if danhsachhoso.file_15 == u'Có':
                docdata.remove_shape(u'id="33" name="Oval 33"')
            elif danhsachhoso.file_15 == u'Không':
                docdata.remove_shape(u'id="31" name="Oval 31"')
            else:
                docdata.remove_shape(u'id="33" name="Oval 33"')
                docdata.remove_shape(u'id="31" name="Oval 31')

            if danhsachhoso.file_16 == u'Có':
                docdata.remove_shape(u'id="35" name="Oval 35"')
            elif danhsachhoso.file_16 == u'Không':
                docdata.remove_shape(u'id="34" name="Oval 34"')
            else:
                docdata.remove_shape(u'id="35" name="Oval 35"')
                docdata.remove_shape(u'id="34" name="Oval 34')

            if danhsachhoso.file_17_1 == u'Có':
                docdata.remove_shape(u'id="37" name="Oval 37"')
            elif danhsachhoso.file_17_1 == u'Không':
                docdata.remove_shape(u'id="36" name="Oval 36"')
            else:
                docdata.remove_shape(u'id="37" name="Oval 37"')
                docdata.remove_shape(u'id="36" name="Oval 36')

            if danhsachhoso.file_17_2 == u'Có':
                docdata.remove_shape(u'id="39" name="Oval 39"')
            elif danhsachhoso.file_17_2 == u'Không':
                docdata.remove_shape(u'id="38" name="Oval 38"')
            else:
                docdata.remove_shape(u'id="39" name="Oval 39"')
                docdata.remove_shape(u'id="38" name="Oval 38"')

            if danhsachhoso.file_18 == u'Có':
                docdata.remove_shape(u'id="41" name="Oval 41"')
            elif danhsachhoso.file_18 == u'Không':
                docdata.remove_shape(u'id="40" name="Oval 40"')
            else:
                docdata.remove_shape(u'id="41" name="Oval 41"')
                docdata.remove_shape(u'id="40" name="Oval 40"')

            context['gc_18'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_18)

            if danhsachhoso.file_19 == u'Có':
                docdata.remove_shape(u'id="43" name="Oval 43"')
            elif danhsachhoso.file_19 == u'Không':
                docdata.remove_shape(u'id="42" name="Oval 42"')
            else:
                docdata.remove_shape(u'id="43" name="Oval 43"')
                docdata.remove_shape(u'id="42" name="Oval 42"')

            context['gc_19'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_19)

            if danhsachhoso.file_20 == u'Có':
                docdata.remove_shape(u'id="45" name="Oval 45"')
            elif danhsachhoso.file_20 == u'Không':
                docdata.remove_shape(u'id="44" name="Oval 44"')
            else:
                docdata.remove_shape(u'id="45" name="Oval 45"')
                docdata.remove_shape(u'id="44" name="Oval 44"')

            context['gc_20'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_20)

            if danhsachhoso.file_21 == u'Có':
                docdata.remove_shape(u'id="51" name="Oval 51"')
            elif danhsachhoso.file_21 == u'Không':
                docdata.remove_shape(u'id="46" name="Oval 46"')
            else:
                docdata.remove_shape(u'id="51" name="Oval 51"')
                docdata.remove_shape(u'id="46" name="Oval 46"')

            context['gc_21'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_21)

            if danhsachhoso.file_22 == u'Có':
                docdata.remove_shape(u'id="53" name="Oval 53"')
            elif danhsachhoso.file_22 == u'Không':
                docdata.remove_shape(u'id="52" name="Oval 52"')
            else:
                docdata.remove_shape(u'id="53" name="Oval 53"')
                docdata.remove_shape(u'id="52" name="Oval 52"')

            context['gc_22'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_22)

            if danhsachhoso.file_23 == u'Có':
                docdata.remove_shape(u'id="55" name="Oval 55"')
            elif danhsachhoso.file_23 == u'Không':
                docdata.remove_shape(u'id="54" name="Oval 54"')
            else:
                docdata.remove_shape(u'id="55" name="Oval 55"')
                docdata.remove_shape(u'id="54" name="Oval 54"')

            if danhsachhoso.file_24 == u'Có':
                docdata.remove_shape(u'id="57" name="Oval 57"')
            elif danhsachhoso.file_24 == u'Không':
                docdata.remove_shape(u'id="56" name="Oval 56"')
            else:
                docdata.remove_shape(u'id="56" name="Oval 56"')
                docdata.remove_shape(u'id="57" name="Oval 57"')

            context['gc_24'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_24)

            if danhsachhoso.file_25 == u'Có':
                docdata.remove_shape(u'id="59" name="Oval 59"')
            elif danhsachhoso.file_25 == u'Không':
                docdata.remove_shape(u'id="58" name="Oval 58"')
            else:
                docdata.remove_shape(u'id="59" name="Oval 59"')
                docdata.remove_shape(u'id="58" name="Oval 58"')

            context['gc_25'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_25)

            if danhsachhoso.file_26 == u'Có':
                docdata.remove_shape(u'id="61" name="Oval 61"')
            elif danhsachhoso.file_26 == u'Không':
                docdata.remove_shape(u'id="60" name="Oval 60"')
            else:
                docdata.remove_shape(u'id="61" name="Oval 61"')
                docdata.remove_shape(u'id="60" name="Oval 60"')

            context['gc_26'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_26)

            if danhsachhoso.file_27 == u'Có':
                docdata.remove_shape(u'id="63" name="Oval 63"')
            elif danhsachhoso.file_27 == u'Không':
                docdata.remove_shape(u'id="62" name="Oval 62"')
            else:
                docdata.remove_shape(u'id="63" name="Oval 63"')
                docdata.remove_shape(u'id="62" name="Oval 62"')

            context['gc_27'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_27)

            if danhsachhoso.file_28 == u'Có':
                docdata.remove_shape(u'id="65" name="Oval 65"')
            elif danhsachhoso.file_28 == u'Không':
                docdata.remove_shape(u'id="64" name="Oval 64"')
            else:
                docdata.remove_shape(u'id="64" name="Oval 64"')
                docdata.remove_shape(u'id="65" name="Oval 65"')

            context['gc_28'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_28)

            if danhsachhoso.file_29 == u'Có':
                docdata.remove_shape(u'id="67" name="Oval 67"')
            elif danhsachhoso.file_29 == u'Không':
                docdata.remove_shape(u'id="66" name="Oval 66"')
            else:
                docdata.remove_shape(u'id="67" name="Oval 67"')
                docdata.remove_shape(u'id="66" name="Oval 66"')

            context['gc_29'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_29)

            if danhsachhoso.file_30 == u'Có':
                docdata.remove_shape(u'id="69" name="Oval 69"')
            elif danhsachhoso.file_30 == u'Không':
                docdata.remove_shape(u'id="68" name="Oval 68"')
            else:
                docdata.remove_shape(u'id="69" name="Oval 69"')
                docdata.remove_shape(u'id="68" name="Oval 68"')

            context['gc_30'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_30)

            if danhsachhoso.file_31 == u'Có':
                docdata.remove_shape(u'id="71" name="Oval 71"')
            elif danhsachhoso.file_31 == u'Không':
                docdata.remove_shape(u'id="70" name="Oval 70"')
            else:
                docdata.remove_shape(u'id="71" name="Oval 71"')
                docdata.remove_shape(u'id="70" name="Oval 70"')

            context['gc_31'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_31)

            if danhsachhoso.file_32 == u'Có':
                docdata.remove_shape(u'id="73" name="Oval 73"')
            elif danhsachhoso.file_32 == u'Không':
                docdata.remove_shape(u'id="72" name="Oval 72"')
            else:
                docdata.remove_shape(u'id="73" name="Oval 73"')
                docdata.remove_shape(u'id="72" name="Oval 72"')

            context['gc_32'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_32)

            if danhsachhoso.file_33 == u'Có':
                docdata.remove_shape(u'id="77" name="Oval 77"')
            elif danhsachhoso.file_33 == u'Không':
                docdata.remove_shape(u'id="76" name="Oval 76"')
            else:
                docdata.remove_shape(u'id="77" name="Oval 77"')
                docdata.remove_shape(u'id="76" name="Oval 76"')

            context['gc_33'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_33)

            if danhsachhoso.file_34 == u'Có':
                docdata.remove_shape(u'id="79" name="Oval 79"')
            elif danhsachhoso.file_34 == u'Không':
                docdata.remove_shape(u'id="78" name="Oval 78"')
            else:
                docdata.remove_shape(u'id="79" name="Oval 79"')
                docdata.remove_shape(u'id="78" name="Oval 78"')

            context['gc_34'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_34)

            if danhsachhoso.file_35 == u'Có':
                docdata.remove_shape(u'id="81" name="Oval 81"')
            elif danhsachhoso.file_35 == u'Không':
                docdata.remove_shape(u'id="80" name="Oval 80"')
            else:
                docdata.remove_shape(u'id="80" name="Oval 80"')
                docdata.remove_shape(u'id="81" name="Oval 81"')

            context['gc_35'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_35)

            if danhsachhoso.file_36 == u'Có':
                docdata.remove_shape(u'id="83" name="Oval 83"')
            elif danhsachhoso.file_36 == u'Không':
                docdata.remove_shape(u'id="82" name="Oval 82"')
            else:
                docdata.remove_shape(u'id="82" name="Oval 82"')
                docdata.remove_shape(u'id="83" name="Oval 83"')

            context['gc_36'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_36)

            if danhsachhoso.file_37 == u'Có':
                docdata.remove_shape(u'id="95" name="Oval 95"')
            elif danhsachhoso.file_37 == u'Không':
                docdata.remove_shape(u'id="94" name="Oval 94"')
            else:
                docdata.remove_shape(u'id="95" name="Oval 95"')
                docdata.remove_shape(u'id="94" name="Oval 94"')

            context['gc_37'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_37)

            if danhsachhoso.file_38 == u'Có':
                docdata.remove_shape(u'id="97" name="Oval 97"')
            elif danhsachhoso.file_38 == u'Không':
                docdata.remove_shape(u'id="96" name="Oval 96"')
            else:
                docdata.remove_shape(u'id="97" name="Oval 97"')
                docdata.remove_shape(u'id="96" name="Oval 96"')

            context['gc_38'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_38)

            if danhsachhoso.file_39 == u'Có':
                docdata.remove_shape(u'id="99" name="Oval 99"')
            elif danhsachhoso.file_39 == u'Không':
                docdata.remove_shape(u'id="98" name="Oval 98"')
            else:
                docdata.remove_shape(u'id="99" name="Oval 99"')
                docdata.remove_shape(u'id="98" name="Oval 98"')

            context['gc_39'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_39)

            if danhsachhoso.file_40 == u'Có':
                docdata.remove_shape(u'id="101" name="Oval 101"')
            elif danhsachhoso.file_40 == u'Không':
                docdata.remove_shape(u'id="100" name="Oval 100"')
            else:
                docdata.remove_shape(u'id="100" name="Oval 100"')
                docdata.remove_shape(u'id="101" name="Oval 101"')

            context['gc_40'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_40)

            if danhsachhoso.file_41 == u'Có':
                docdata.remove_shape(u'id="103" name="Oval 103"')
            elif danhsachhoso.file_41 == u'Không':
                docdata.remove_shape(u'id="102" name="Oval 102"')
            else:
                docdata.remove_shape(u'id="103" name="Oval 103"')
                docdata.remove_shape(u'id="102" name="Oval 102"')

            context['gc_41'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_41)

            if danhsachhoso.file_42 == u'Có':
                docdata.remove_shape(u'id="105" name="Oval 105"')
            elif danhsachhoso.file_42 == u'Không':
                docdata.remove_shape(u'id="104" name="Oval 104"')
            else:
                docdata.remove_shape(u'id="104" name="Oval 104"')
                docdata.remove_shape(u'id="105" name="Oval 105"')

            context['gc_42'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_42)

            if danhsachhoso.file_43 == u'Có':
                docdata.remove_shape(u'id="107" name="Oval 107"')
            elif danhsachhoso.file_43 == u'Không':
                docdata.remove_shape(u'id="106" name="Oval 106"')
            else:
                docdata.remove_shape(u'id="107" name="Oval 107"')
                docdata.remove_shape(u'id="106" name="Oval 106"')

            context['gc_43'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_43)

            if danhsachhoso.file_44 == u'Có':
                docdata.remove_shape(u'id="109" name="Oval 109"')
            elif danhsachhoso.file_44 == u'Không':
                docdata.remove_shape(u'id="108" name="Oval 108"')
            else:
                docdata.remove_shape(u'id="109" name="Oval 109"')
                docdata.remove_shape(u'id="108" name="Oval 108"')

            context['gc_44'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_44)

            if danhsachhoso.file_45 == u'Có':
                docdata.remove_shape(u'id="111" name="Oval 111"')
            elif danhsachhoso.file_45 == u'Không':
                docdata.remove_shape(u'id="110" name="Oval 110"')
            else:
                docdata.remove_shape(u'id="111" name="Oval 111"')
                docdata.remove_shape(u'id="110" name="Oval 110"')

            context['gc_45'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_45)

            if danhsachhoso.file_46 == u'Có':
                docdata.remove_shape(u'id="113" name="Oval 113"')
            elif danhsachhoso.file_46 == u'Không':
                docdata.remove_shape(u'id="112" name="Oval 112"')
            else:
                docdata.remove_shape(u'id="112" name="Oval 112"')
                docdata.remove_shape(u'id="113" name="Oval 113"')

            if danhsachhoso.file_47 == u'Có':
                docdata.remove_shape(u'id="115" name="Oval 115"')
            elif danhsachhoso.file_47 == u'Không':
                docdata.remove_shape(u'id="114" name="Oval 114"')
            else:
                docdata.remove_shape(u'id="115" name="Oval 115"')
                docdata.remove_shape(u'id="114" name="Oval 114"')

            context['gc_47'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_47)

            if danhsachhoso.file_48 == u'Có':
                docdata.remove_shape(u'id="117" name="Oval 117"')
            elif danhsachhoso.file_48 == u'Không':
                docdata.remove_shape(u'id="116" name="Oval 116"')
            else:
                docdata.remove_shape(u'id="117" name="Oval 117"')
                docdata.remove_shape(u'id="116" name="Oval 116"')

            context['gc_48'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_48)

            if danhsachhoso.file_49 == u'Có':
                docdata.remove_shape(u'id="119" name="Oval 119"')
            elif danhsachhoso.file_49 == u'Không':
                docdata.remove_shape(u'id="118" name="Oval 118"')
            else:
                docdata.remove_shape(u'id="119" name="Oval 119"')
                docdata.remove_shape(u'id="118" name="Oval 118"')

            if danhsachhoso.file_50 == u'Có':
                docdata.remove_shape(u'id="121" name="Oval 121"')
            elif danhsachhoso.file_50 == u'Không':
                docdata.remove_shape(u'id="120" name="Oval 120"')
            else:
                docdata.remove_shape(u'id="121" name="Oval 121"')
                docdata.remove_shape(u'id="120" name="Oval 120"')

            if danhsachhoso.file_51 == u'Có':
                docdata.remove_shape(u'id="123" name="Oval 123"')
            elif danhsachhoso.file_51 == u'Không':
                docdata.remove_shape(u'id="122" name="Oval 122"')
            else:
                docdata.remove_shape(u'id="123" name="Oval 123"')
                docdata.remove_shape(u'id="122" name="Oval 122"')

            context['gc_51'] = format_hoso.kiemtra(danhsachhoso.ghichu_file_51)

            if danhsachhoso.file_52 == u'Có':
                docdata.remove_shape(u'id="125" name="Oval 125"')
            elif danhsachhoso.file_52 == u'Không':
                docdata.remove_shape(u'id="124" name="Oval 124"')
            else:
                docdata.remove_shape(u'id="125" name="Oval 125"')
                docdata.remove_shape(u'id="124" name="Oval 124"')

            if danhsachhoso.file_53 == u'Có':
                docdata.remove_shape(u'id="127" name="Oval 127"')
            elif danhsachhoso.file_53 == u'Không':
                docdata.remove_shape(u'id="126" name="Oval 126"')
            else:
                docdata.remove_shape(u'id="127" name="Oval 127"')
                docdata.remove_shape(u'id="126" name="Oval 126"')

            if danhsachhoso.file_54 == u'Có':
                docdata.remove_shape(u'id="129" name="Oval 129"')
            elif danhsachhoso.file_54 == u'Không':
                docdata.remove_shape(u'id="128" name="Oval 128"')
            else:
                docdata.remove_shape(u'id="129" name="Oval 129"')
                docdata.remove_shape(u'id="128" name="Oval 128"')

            if danhsachhoso.file_55 == u'Có':
                docdata.remove_shape(u'id="131" name="Oval 131"')
            elif danhsachhoso.file_55 == u'Không':
                docdata.remove_shape(u'id="130" name="Oval 130"')
            else:
                docdata.remove_shape(u'id="131" name="Oval 131"')
                docdata.remove_shape(u'id="130" name="Oval 130"')

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_31(self, ma_thuctapsinh, document):
        print('31')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_31")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            danhsach_tts = document.thuctapsinh_hoso
            context = {}

            if document.donhang_hoso.nghiepdoan_donhang.buudien_nghiepdoan:
                context['bs_0007'] = document.donhang_hoso.nghiepdoan_donhang.buudien_nghiepdoan[
                                     0:3] + '-' + document.donhang_hoso.nghiepdoan_donhang.buudien_nghiepdoan[3:]
            else:
                context['bs_0007'] = ''
            context['hhjp_0098'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.diachi_nghiepdoan)
            context['hhjp_0099'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.dienthoai_nghiepdoan)
            context['hhjp_0097'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_han_nghiepdoan)
            context['hhjp_0097_2'] = format_hoso.kiemtra(
                document.donhang_hoso.nghiepdoan_donhang.ten_daidien_han_nghiepdoan)
            context['hhjp_0097_1'] = format_hoso.kiemtra(
                document.donhang_hoso.nghiepdoan_donhang.chucvu_daidien_phienam_nghiepdoan)

            context['hhjp_0613'] = format_hoso.convert_date(document.ngay_lapvanban)
            if document.xinghiep_hoso.sobuudien_xnghiep:
                context['hhjp_0189_1'] = document.xinghiep_hoso.sobuudien_xnghiep[
                                         0:3] + '-' + document.xinghiep_hoso.sobuudien_xnghiep[3:]
            else:
                context['hhjp_0189_1'] = ''

            context['hhjp_0189'] = format_hoso.kiemtra(document.xinghiep_hoso.diachi_han_xnghiep)
            context['hhjp_0190'] = format_hoso.kiemtra(document.xinghiep_hoso.sdt_xnghiep).strip()
            context['hhjp_0191'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            context['hhjp_0192'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep)
            context['hhjp_0192_1'] = format_hoso.kiemtra(document.xinghiep_hoso.chucvu_daidien_han)

            kehoachdoatao = self.env['thuctapsinh.kehoachdoatao'].search([])
            thuctapsinh_danhsach = self.env['thuctapsinh.thuctapsinh'].search(
                [('donhang_tts', '=', thuctapsinh.donhang_tts.id)])
            context['hhjp_0188_2'] = len(thuctapsinh_danhsach)
            table_thuctapsinh = []
            for thuctapsinh_thuctapsinh in thuctapsinh_danhsach:
                info = {}
                info['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh_thuctapsinh.ten_lt_tts)
                info['hhjp_0003'] = format_hoso.kiemtra(thuctapsinh_thuctapsinh.quoctich_tts.name_quoctich)
                info['hhjp_0004'] = format_hoso.convert_date(thuctapsinh_thuctapsinh.ngaysinh_tts)
                info['hhjp_0006'] = format_hoso.gender_check(thuctapsinh_thuctapsinh.gtinh_tts)

                info['hhjp_0113_A'] = '□ A 第１号企業単独型技能実習'
                info['hhjp_0113_B'] = '□ B 第２号企業単独型技能実習'
                info['hhjp_0113_C'] = '□ C 第３号企業単独型技能実習'

                if document.giaidoan_hoso == '1 go':
                    info['hhjp_0113_D'] = '■ D 第１号団体監理型技能実習'
                    info['hhjp_0113_E'] = '□ E 第２号団体監理型技能実習'
                    info['hhjp_0113_F'] = '□ F 第３号団体監理型技能実習'
                elif document.giaidoan_hoso == '2 go':
                    info['hhjp_0113_D'] = '□ D 第１号団体監理型技能実習'
                    info['hhjp_0113_E'] = '■ E 第２号団体監理型技能実習'
                    info['hhjp_0113_F'] = '□ F 第３号団体監理型技能実習'
                elif document.giaidoan_hoso == '3 go':
                    info['hhjp_0113_D'] = '□ D 第１号団体監理型技能実習'
                    info['hhjp_0113_E'] = '□ E 第２号団体監理型技能実習'
                    info['hhjp_0113_F'] = '■ F 第３号団体監理型技能実習'
                else:
                    info['hhjp_0113_D'] = '□ D 第１号団体監理型技能実習'
                    info['hhjp_0113_E'] = '□ E 第２号団体監理型技能実習'
                    info['hhjp_0113_F'] = '□ F 第３号団体監理型技能実習'
                table_thuctapsinh.append(info)
            context['tbl_thuctapsinh'] = table_thuctapsinh
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    def baocao_hoso_32(self, document):
        print('32')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_32")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}
            chamdiem_xinghiep = self.env['xinghiep.chamdiem'].search(
                [('xinghiep_chamdiem', '=', document.xinghiep_hoso.id)], limit=1)

            context['hhjp_0193'] = format_hoso.convert_date(chamdiem_xinghiep.ngaylap_vanban_chamdiem)
            context['hhjp_0010'] = format_hoso.kiemtra(chamdiem_xinghiep.xinghiep_chamdiem.ten_han_xnghiep)
            context['xn_0001'] = format_hoso.kiemtra(chamdiem_xinghiep.soluong_kiemtra_tongphu_1)
            context['xn_0002'] = format_hoso.kiemtra(chamdiem_xinghiep.hoanthanh_hientai_1)
            context['xn_0003'] = format_hoso.kiemtra(chamdiem_xinghiep.kiemtra_hientai_1)
            context['xn_0004'] = format_hoso.kiemtra(chamdiem_xinghiep.soluong_kiemtra_hientai_1)
            context['xn_0071'] = format_hoso.kiemtra(chamdiem_xinghiep.diem_1_1_chamdiem)
            context['xn_0005'] = format_hoso.kiemtra(chamdiem_xinghiep.hoanthanh_cu_1)
            context['xn_0006'] = format_hoso.kiemtra(chamdiem_xinghiep.kiemtra_cu_1)
            context['xn_0007'] = format_hoso.kiemtra(chamdiem_xinghiep.soluong_kiemtra_cu_1)
            context['xn_0008'] = format_hoso.kiemtra(chamdiem_xinghiep.ungvien_thanhcong_tongphu_1)
            context['xn_0009'] = format_hoso.kiemtra(chamdiem_xinghiep.ungvien_thanhcong_hientai_1)
            context['xn_0010'] = format_hoso.kiemtra(chamdiem_xinghiep.ungvien_thanhcong_cu_1)
            context['xn_0011'] = format_hoso.kiemtra(chamdiem_xinghiep.tile_kithi_coban)
            context['xn_0012'] = format_hoso.kiemtra(chamdiem_xinghiep.soluong_tong)
            context['xn_0013'] = format_hoso.kiemtra(chamdiem_xinghiep.soluong_kiemtra_tongphu_2)
            context['xn_0014'] = format_hoso.kiemtra(chamdiem_xinghiep.hoanthanh_hientai_2)
            context['xn_0015'] = format_hoso.kiemtra(chamdiem_xinghiep.kiemtra_hientai_2)
            context['xn_0016'] = format_hoso.kiemtra(chamdiem_xinghiep.soluong_kiemtra_hientai_2)
            context['xn_0017'] = format_hoso.kiemtra(chamdiem_xinghiep.soluong_kiemtra_cu_2)
            context['xn_0018'] = format_hoso.kiemtra(chamdiem_xinghiep.hoanthanh_hientai_3)

            context['xn_0019'] = format_hoso.kiemtra(chamdiem_xinghiep.kiemtra_hientai_3)
            context['xn_0020'] = format_hoso.kiemtra(chamdiem_xinghiep.soluong_kiemtra_hientai_3)
            context[
                'xn_0021'] = chamdiem_xinghiep.vuotqua_kiemtra_tongphu_2 + chamdiem_xinghiep.vuotqua_kiemtra_hientai_3 if chamdiem_xinghiep.vuotqua_kiemtra_tongphu_2 else ''
            context['xn_0022'] = format_hoso.kiemtra(chamdiem_xinghiep.vuotqua_kiemtra_tongphu_2)
            context['xn_0023'] = format_hoso.kiemtra(chamdiem_xinghiep.vuotqua_kiemtra_hientai_2)
            context['xn_0024'] = format_hoso.kiemtra(chamdiem_xinghiep.vuotqua_kiemtra_cu_2)
            context['xn_0025'] = format_hoso.kiemtra(chamdiem_xinghiep.vuotqua_kiemtra_hientai_3)
            context['xn_0072'] = format_hoso.kiemtra(chamdiem_xinghiep.diem_2_1_chamdiem)
            context['xn_0073'] = chamdiem_xinghiep.diem_3_1_chamdiem
            context['xn_0026'] = format_hoso.kiemtra(chamdiem_xinghiep.kithi_lop_coban_I2)
            context['xn_0027'] = format_hoso.kiemtra(chamdiem_xinghiep.tile_lop_kithi_coban)
            context['xn_0074'] = chamdiem_xinghiep.diem_4_1_chamdiem
            context['xn_0075'] = chamdiem_xinghiep.diem_5_1_chamdiem
            context['xn_0076'] = chamdiem_xinghiep.diem_6_1_chamdiem
            context['xn_0077'] = chamdiem_xinghiep.diem_1_2_chamdiem
            context['xn_0078'] = chamdiem_xinghiep.diem_2_2_chamdiem
            context['xn_0079'] = chamdiem_xinghiep.diem_1_3_chamdiem
            context['xn_0080'] = chamdiem_xinghiep.diem_2_3_chamdiem
            context['xn_0081'] = chamdiem_xinghiep.diem_1_4_chamdiem
            context['xn_0082'] = chamdiem_xinghiep.diem_2_4_chamdiem
            context['xn_0083'] = chamdiem_xinghiep.diem_3_4_chamdiem
            context['xn_0084'] = chamdiem_xinghiep.diem_1_5_chamdiem
            context['xn_0085'] = chamdiem_xinghiep.diem_2_5_chamdiem
            context['xn_0086'] = chamdiem_xinghiep.diem_3_5_chamdiem
            context['xn_0087'] = chamdiem_xinghiep.diem_1_6_chamdiem
            context['xn_0088'] = chamdiem_xinghiep.diem_2_6_chamdiem
            context['xn_0089'] = chamdiem_xinghiep.diem_3_6_chamdiem

            if chamdiem_xinghiep.ketqua_kiemtra_1 == 'Có':
                context['xn_0034_1'] = u'■'
                context['xn_0034_2'] = u'□'
            elif chamdiem_xinghiep.ketqua_kiemtra_1 == 'Không':
                context['xn_0034_1'] = u'□'
                context['xn_0034_2'] = u'■'
            else:
                context['xn_0034_1'] = u'□'
                context['xn_0034_2'] = u'□'

            context['xn_0030'] = format_hoso.kiemtra(chamdiem_xinghiep.tong_thanhcong)
            context['xn_0031'] = format_hoso.kiemtra(chamdiem_xinghiep.noidung_kiemtra_a_1)
            context['xn_0032'] = format_hoso.kiemtra(chamdiem_xinghiep.noidung_kiemtra_b_1)
            context['xn_0033'] = format_hoso.kiemtra(chamdiem_xinghiep.noidung_kiemtra_c_1)

            if chamdiem_xinghiep.kythuat_thuctapsinh_2 != False:
                context['xn_0035_1'] = u'■'
                context['xn_0035_2'] = u'□'
            elif chamdiem_xinghiep.kythuat_thuctapsinh_2 == False:
                context['xn_0035_1'] = u'□'
                context['xn_0035_2'] = u'■'
            else:
                context['xn_0035_1'] = u'□'
                context['xn_0035_2'] = u'□'

            context['xn_0036'] = format_hoso.kiemtra(chamdiem_xinghiep.kythuat_thuctapsinh_2)
            context['xn_0037'] = format_hoso.kiemtra(chamdiem_xinghiep.kythuat_nguoithamgia_2)

            if chamdiem_xinghiep.huongdan_thuctapsinh_2 != False:
                context['xn_0038_1'] = u'■'
                context['xn_0038_2'] = u'□'
            elif chamdiem_xinghiep.huongdan_thuctapsinh_2 == False:
                context['xn_0038_1'] = u'□'
                context['xn_0038_2'] = u'■'
            else:
                context['xn_0038_1'] = u'□'
                context['xn_0038_2'] = u'□'

            context['xn_0039'] = format_hoso.kiemtra(chamdiem_xinghiep.huongdan_thuctapsinh_2)
            context['xn_0040'] = format_hoso.kiemtra(chamdiem_xinghiep.huongdan_nguoithamgia_2)
            context['xn_0041'] = format_hoso.kiemtra(chamdiem_xinghiep.luong_thap_3)
            context['xn_0042'] = format_hoso.kiemtra(chamdiem_xinghiep.luong_toithieu_3)
            context['xn_0043'] = format_hoso.kiemtra(chamdiem_xinghiep.tyle_luong_3)
            context['xn_0044'] = format_hoso.kiemtra(chamdiem_xinghiep.thuctapkythuat_3.ten_lt_tts)

            if chamdiem_xinghiep.loai_luongtoithieu_3 == 'Mức lương tối thiểu theo vùng':
                context['xn_0045_1'] = u'■'
                context['xn_0045_2'] = u'□'
            elif chamdiem_xinghiep.loai_luongtoithieu_3 == 'Mức lương tối thiểu quy định':
                context['xn_0045_1'] = u'□'
                context['xn_0045_2'] = u'■'
            else:
                context['xn_0045_1'] = u'□'
                context['xn_0045_2'] = u'□'

            context['xn_0046'] = format_hoso.kiemtra(chamdiem_xinghiep.chuyen_1_2_daotao_3)
            context['xn_0047'] = format_hoso.kiemtra(chamdiem_xinghiep.chuyen_2_3_daotao_3)

            if chamdiem_xinghiep.huongdan_caitien_4 == 'Có':
                context['xn_0048_1'] = u'□'
                context['xn_0048_2'] = u'■'
                if chamdiem_xinghiep.thuchien_huongdan_caitien_4 == 'Thực hiện cải tiến':
                    context['xn_0050_1'] = u'■'
                    context['xn_0050_2'] = u'□'
                elif chamdiem_xinghiep.thuchien_huongdan_caitien_4 == 'Không thực hiện cải tiến':
                    context['xn_0050_1'] = u'□'
                    context['xn_0050_2'] = u'■'
                else:
                    context['xn_0050_1'] = u'□'
                    context['xn_0050_2'] = u'□'
            elif chamdiem_xinghiep.huongdan_caitien_4 == 'Không':
                context['xn_0048_1'] = u'■'
                context['xn_0048_2'] = u'□'
            else:
                context['xn_0048_1'] = u'□'
                context['xn_0048_2'] = u'□'

            if chamdiem_xinghiep.noidung_huongdan_caitien_4:
                context[
                    'xn_0049_D'] = chamdiem_xinghiep.noidung_huongdan_caitien_4.day
                context[
                    'xn_0049_M'] = chamdiem_xinghiep.noidung_huongdan_caitien_4.month
                context[
                    'xn_0049_Y'] = chamdiem_xinghiep.noidung_huongdan_caitien_4.year
            else:
                context[
                    'xn_0049_D'] = ''
                context[
                    'xn_0049_M'] = ''
                context[
                    'xn_0049_Y'] = ''

            if chamdiem_xinghiep.huongdan_quantri_4 == 'Có':
                context['xn_0051_1'] = u'□'
                context['xn_0051_2'] = u'■'
                if chamdiem_xinghiep.thuchien_huongdan_quantri_4 == 'Thực hiện cải tiến':
                    context['xn_0053_1'] = u'■'
                    context['xn_0053_2'] = u'□'
                elif chamdiem_xinghiep.thuchien_huongdan_quantri_4 == 'Không thực hiện cải tiến':
                    context['xn_0053_1'] = u'□'
                    context['xn_0053_2'] = u'■'
                else:
                    context['xn_0053_1'] = u'□'
                    context['xn_0053_2'] = u'□'
            elif chamdiem_xinghiep.huongdan_quantri_4 == 'Không':
                context['xn_0051_1'] = u'■'
                context['xn_0051_2'] = u'□'
            else:
                context['xn_0051_1'] = u'□'
                context['xn_0051_2'] = u'□'

            if chamdiem_xinghiep.noidung_huongdan_caitien_4:
                context[
                    'xn_0052_D'] = chamdiem_xinghiep.noidung_huongdan_quantri_4.day
                context[
                    'xn_0052_M'] = chamdiem_xinghiep.noidung_huongdan_quantri_4.month
                context[
                    'xn_0052_Y'] = chamdiem_xinghiep.noidung_huongdan_quantri_4.year
            else:
                context[
                    'xn_0052_D'] = ''
                context[
                    'xn_0052_M'] = ''
                context[
                    'xn_0052_Y'] = ''



            context['xn_0054'] = format_hoso.kiemtra(chamdiem_xinghiep.nguoi_mattich_4)
            context['xn_0055'] = format_hoso.kiemtra(chamdiem_xinghiep.nguoi_duocnhan_4)
            context['xn_0056'] = format_hoso.kiemtra(chamdiem_xinghiep.tyle_nguoi_4)

            if chamdiem_xinghiep.doloi_4 == 'Có':
                context['xn_0057_1'] = u'□'
                context['xn_0057_2'] = u'■'
            elif chamdiem_xinghiep.doloi_4 == 'Không':
                context['xn_0057_1'] = u'■'
                context['xn_0057_2'] = u'□'
            else:
                context['xn_0057_1'] = u'□'
                context['xn_0057_2'] = u'□'

            if chamdiem_xinghiep.nhansu_lienquan_5 == 'Có':
                context['xn_0058_1'] = u'■'
                context['xn_0058_2'] = u'□'
            elif chamdiem_xinghiep.nhansu_lienquan_5 == 'Không':
                context['xn_0058_1'] = u'□'
                context['xn_0058_2'] = u'■'
            else:
                context['xn_0058_1'] = u'□'
                context['xn_0058_2'] = u'□'

            if chamdiem_xinghiep.dambao_5 == 'Có':
                context['xn_0059_1'] = u'■'
                context['xn_0059_2'] = u'□'
            elif chamdiem_xinghiep.dambao_5 == 'Không':
                context['xn_0059_1'] = u'□'
                context['xn_0059_2'] = u'■'
            else:
                context['xn_0059_1'] = u'□'
                context['xn_0059_2'] = u'□'

            if chamdiem_xinghiep.thaydoi_daotao_5 == 'Có':
                context['xn_0060_1'] = u'□'
                context['xn_0060_2'] = u'■'
                if chamdiem_xinghiep.gioitinh_5 == 'Nam':
                    context['xn_0063_1'] = u'■'
                    context['xn_0063_2'] = u'□'
                elif chamdiem_xinghiep.gioitinh_5 == 'Nữ':
                    context['xn_0063_1'] = u'□'
                    context['xn_0063_2'] = u'■'
                else:
                    context['xn_0063_1'] = u'□'
                    context['xn_0063_2'] = u'□'
            elif chamdiem_xinghiep.thaydoi_daotao_5 == 'Không':
                context['xn_0060_1'] = u'■'
                context['xn_0060_2'] = u'□'
                context['xn_0063_1'] = u'□'
                context['xn_0063_2'] = u'□'
            else:
                context['xn_0060_1'] = u'□'
                context['xn_0060_2'] = u'□'
                context['xn_0063_1'] = u'□'
                context['xn_0063_2'] = u'□'

            context['xn_0061'] = format_hoso.kiemtra(chamdiem_xinghiep.nhanvien_kythuat_5)

            context['xn_0062'] = format_hoso.kiemtra(chamdiem_xinghiep.quoctich_5)

            context['xn_0063'] = format_hoso.kiemtra(chamdiem_xinghiep.gioitinh_5)

            if chamdiem_xinghiep.ngaysinh_5:
                context['xn_0064_D'] = chamdiem_xinghiep.ngaysinh_5.day
                context['xn_0064_M'] = chamdiem_xinghiep.ngaysinh_5.month
                context['xn_0064_Y'] = chamdiem_xinghiep.ngaysinh_5.year
            else:
                context['xn_0064_D'] = ''
                context['xn_0064_M'] = ''
                context['xn_0064_Y'] = ''

            if chamdiem_xinghiep.ngaysinh_5:
                context['xn_0065_D'] = chamdiem_xinghiep.ngaynhan_5.day
                context['xn_0065_M'] = chamdiem_xinghiep.ngaynhan_5.month
                context['xn_0065_Y'] = chamdiem_xinghiep.ngaynhan_5.year
            else:
                context['xn_0065_D'] = ''
                context['xn_0065_M'] = ''
                context['xn_0065_Y'] = ''

            context['xn_0066'] = format_hoso.kiemtra(chamdiem_xinghiep.souyquyen_5)
            context['xn_0067'] = format_hoso.kiemtra(chamdiem_xinghiep.noidung_decuong_6)
            context['xn_0068'] = format_hoso.kiemtra(chamdiem_xinghiep.noidung_tuongtac_6)
            context['xn_0069'] = format_hoso.kiemtra(chamdiem_xinghiep.noidung_vanhoa_6)
            context['xn_0070'] = format_hoso.kiemtra(chamdiem_xinghiep.diem_4_6_chamdiem)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    def baocao_hoso_32_2(self, document):
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_32_2")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh_danhsach_3 = self.env['thuctapsinh.thuctapsinh'].search(
                [('xinghiep_thuctapsinh', '=', document.xinghiep_hoso.id), ('capdo_chuyennhuong_namba', '!=', None)])

            thuctapsinh_danhsach_2 = self.env['thuctapsinh.thuctapsinh'].search(
                [('xinghiep_thuctapsinh', '=', document.xinghiep_hoso.id), ('capdo_chuyennhuong_namhai', '!=', None),
                 ('capdo_chuyennhuong_namba', '=', None)])

            thuctapsinh_danhsach_1 = self.env['thuctapsinh.thuctapsinh'].search(
                [('xinghiep_thuctapsinh', '=', document.xinghiep_hoso.id), ('capdo_chuyennhuong_namnhat', '!=', None),
                 ('capdo_chuyennhuong_namhai', '=', None),
                 ('capdo_chuyennhuong_namba', '=', None)])

            context = {}
            table_capdo = []
            table_capdo.append(thuctapsinh_danhsach_1)
            table_capdo.append(thuctapsinh_danhsach_2)
            table_capdo.append(thuctapsinh_danhsach_3)
            table_fill = []
            i = 1
            for item in table_capdo:
                infor = {}
                tbl_tts = []
                infor['td_0001_1'] = '■'
                infor['td_0001_2'] = '□'
                if i == 1:
                    if item:
                        infor['td_0001'] = '基礎級'
                        for tts in item:
                            infor_tts = {}
                            infor_tts['hhjp_0001'] = format_hoso.kiemtra(tts.ten_lt_tts)
                            infor_tts['hhjp_0001_1'] = format_hoso.convert_date(tts.ketthuc_thucte_namnhat)
                            infor_tts['hhjp_0006'] = format_hoso.gender_check(tts.gtinh_tts)
                            infor_tts['hhjp_0003'] = format_hoso.kiemtra(tts.quoctich_tts.name_quoctich)
                            infor_tts['hhjp_0004'] = format_hoso.convert_date(tts.ngaysinh_tts)
                            infor_tts['hhjp_0131'] = format_hoso.kiemtra(tts.kithi_chuyennhuong_namnhat.ten_kithi)
                            infor_tts['hhjp_0131_7'] = '□'
                            
                            if tts.ngayqua_lythuyet_motcongviec_namnhat and tts.ngayqua_thuchanh_motcongviec_namnhat:
                                infor_tts['td_0002'] = format_hoso.convert_date(
                                    tts.ngayqua_thuchanh_motcongviec_namnhat)
                            elif tts.ngayqua_thuchanh_motcongviec_namnhat:
                                infor_tts['td_0002'] = format_hoso.convert_date(
                                    tts.ngayqua_thuchanh_motcongviec_namnhat)
                            elif tts.ngayqua_lythuyet_motcongviec_namnhat and tts.ngayqua_thuchanh_motcongviec_namnhat == False:
                                infor_tts['td_0002'] = format_hoso.convert_date(
                                    tts.ngayqua_lythuyet_motcongviec_namnhat)
                            else:
                                infor_tts['td_0002'] = '年　月　日'

                            if tts.ketqua_lythuyet_motcongviec_namnhat == 'Qua' and tts.ketqua_thuchanh_motcongviec_namnhat == 'Qua':
                                infor_tts['hhjp_0131_1'] = '■'
                                infor_tts['hhjp_0131_2'] = '■'
                                infor_tts['hhjp_0131_3'] = '□'
                                infor_tts['hhjp_0131_4'] = '□'
                                infor_tts['hhjp_0131_5'] = '□'
                                infor_tts['hhjp_0131_6'] = '□'
                            elif tts.ketqua_lythuyet_motcongviec_namnhat == 'Qua' and tts.ketqua_thuchanh_motcongviec_namnhat == 'Không qua':
                                infor_tts['hhjp_0131_1'] = '■'
                                infor_tts['hhjp_0131_2'] = '□'
                                infor_tts['hhjp_0131_3'] = '■'
                                infor_tts['hhjp_0131_4'] = '□'
                                infor_tts['hhjp_0131_5'] = '□'
                                infor_tts['hhjp_0131_6'] = '□'
                            elif tts.ketqua_lythuyet_motcongviec_namnhat == 'Không qua' and tts.ketqua_thuchanh_motcongviec_namnhat == 'Qua':
                                infor_tts['hhjp_0131_1'] = '■'
                                infor_tts['hhjp_0131_2'] = '□'
                                infor_tts['hhjp_0131_3'] = '□'
                                infor_tts['hhjp_0131_4'] = '■'
                                infor_tts['hhjp_0131_5'] = '□'
                                infor_tts['hhjp_0131_6'] = '□'
                            elif tts.ketqua_lythuyet_motcongviec_namnhat == 'Không qua' and tts.ketqua_thuchanh_motcongviec_namnhat == 'Không qua':
                                infor_tts['hhjp_0131_1'] = '□'
                                infor_tts['hhjp_0131_2'] = '□'
                                infor_tts['hhjp_0131_3'] = '□'
                                infor_tts['hhjp_0131_4'] = '□'
                                infor_tts['hhjp_0131_5'] = '■'
                                infor_tts['hhjp_0131_6'] = '□'
                            elif tts.ketqua_lythuyet_motcongviec_namnhat == False and tts.ketqua_thuchanh_motcongviec_namnhat == False:
                                infor_tts['hhjp_0131_1'] = '□'
                                infor_tts['hhjp_0131_2'] = '□'
                                infor_tts['hhjp_0131_3'] = '□'
                                infor_tts['hhjp_0131_4'] = '□'
                                infor_tts['hhjp_0131_5'] = '□'
                                infor_tts['hhjp_0131_6'] = '■'
                                infor_tts['hhjp_0911'] = format_hoso.kiemtra(tts.sochungnhan_namnhat)
                            else:
                                infor_tts['hhjp_0131_1'] = '□'
                                infor_tts['hhjp_0131_2'] = '□'
                                infor_tts['hhjp_0131_3'] = '□'
                                infor_tts['hhjp_0131_4'] = '□'
                                infor_tts['hhjp_0131_5'] = '□'
                                infor_tts['hhjp_0131_6'] = '□'
                            
                            tbl_tts.append(infor_tts)
                    else:
                        tbl_tts = []
                    i += 1
                elif i == 2:
                    if item:
                        infor['td_0001'] = '3級'
                        for tts in item:
                            infor_tts = {}
                            infor_tts['hhjp_0001'] = format_hoso.kiemtra(tts.ten_lt_tts)
                            infor_tts['hhjp_0001_1'] = format_hoso.convert_date(tts.ketthuc_thucte_namhai)
                            infor_tts['hhjp_0006'] = format_hoso.gender_check(tts.gtinh_tts)
                            infor_tts['hhjp_0003'] = format_hoso.kiemtra(tts.quoctich_tts.name_quoctich)
                            infor_tts['hhjp_0004'] = format_hoso.convert_date(tts.ngaysinh_tts)
                            infor_tts['hhjp_0131'] = format_hoso.kiemtra(tts.kithi_chuyennhuong_namhai.ten_kithi)

                            
                            infor_tts['hhjp_0131_7'] = '□'

                            if tts.ngayqua_lythuyet_motcongviec_namhai and tts.ngayqua_thuchanh_motcongviec_namhai:
                                infor_tts['td_0002'] = format_hoso.convert_date(
                                    tts.ngayqua_thuchanh_motcongviec_namhai)
                            elif tts.ngayqua_thuchanh_motcongviec_namhai:
                                infor_tts['td_0002'] = format_hoso.convert_date(
                                    tts.ngayqua_thuchanh_motcongviec_namhai)
                            elif tts.ngayqua_lythuyet_motcongviec_namhai and tts.ngayqua_thuchanh_motcongviec_namhai == False:
                                infor_tts['td_0002'] = format_hoso.convert_date(
                                    tts.ngayqua_lythuyet_motcongviec_namhai)
                            else:
                                infor_tts['td_0002'] = '年　月　日'
                            if tts.ketqua_lythuyet_motcongviec_namhai == 'Qua' and tts.ketqua_thuchanh_motcongviec_namhai == 'Qua':
                                infor_tts['hhjp_0131_1'] = '■'
                                infor_tts['hhjp_0131_2'] = '■'
                                infor_tts['hhjp_0131_3'] = '□'
                                infor_tts['hhjp_0131_4'] = '□'
                                infor_tts['hhjp_0131_5'] = '□'
                                infor_tts['hhjp_0131_6'] = '□'
                            elif tts.ketqua_lythuyet_motcongviec_namhai == 'Qua' and tts.ketqua_thuchanh_motcongviec_namhai == 'Không qua':
                                infor_tts['hhjp_0131_1'] = '■'
                                infor_tts['hhjp_0131_2'] = '□'
                                infor_tts['hhjp_0131_3'] = '■'
                                infor_tts['hhjp_0131_4'] = '□'
                                infor_tts['hhjp_0131_5'] = '□'
                                infor_tts['hhjp_0131_6'] = '□'
                            elif tts.ketqua_lythuyet_motcongviec_namhai == 'Không qua' and tts.ketqua_thuchanh_motcongviec_namhai == 'Qua':
                                infor_tts['hhjp_0131_1'] = '■'
                                infor_tts['hhjp_0131_2'] = '□'
                                infor_tts['hhjp_0131_3'] = '□'
                                infor_tts['hhjp_0131_4'] = '■'
                                infor_tts['hhjp_0131_5'] = '□'
                                infor_tts['hhjp_0131_6'] = '□'
                            elif tts.ketqua_lythuyet_motcongviec_namhai == 'Không qua' and tts.ketqua_thuchanh_motcongviec_namhai == 'Không qua':
                                infor_tts['hhjp_0131_1'] = '□'
                                infor_tts['hhjp_0131_2'] = '□'
                                infor_tts['hhjp_0131_3'] = '□'
                                infor_tts['hhjp_0131_4'] = '□'
                                infor_tts['hhjp_0131_5'] = '■'
                                infor_tts['hhjp_0131_6'] = '□'
                            elif tts.ketqua_lythuyet_motcongviec_namhai == False and tts.ketqua_thuchanh_motcongviec_namhai == False:
                                infor_tts['hhjp_0131_1'] = '□'
                                infor_tts['hhjp_0131_2'] = '□'
                                infor_tts['hhjp_0131_3'] = '□'
                                infor_tts['hhjp_0131_4'] = '□'
                                infor_tts['hhjp_0131_5'] = '□'
                                infor_tts['hhjp_0131_6'] = '■'
                                infor_tts['hhjp_0911'] = format_hoso.kiemtra(tts.sochungnhan_namhai)
                            else:
                                infor_tts['hhjp_0131_1'] = '□'
                                infor_tts['hhjp_0131_2'] = '□'
                                infor_tts['hhjp_0131_3'] = '□'
                                infor_tts['hhjp_0131_4'] = '□'
                                infor_tts['hhjp_0131_5'] = '□'
                                infor_tts['hhjp_0131_6'] = '□'

                            tbl_tts.append(infor_tts)
                    i += 1
                else:
                    if item:
                        infor['td_0001'] = '2級'
                        for tts in item:
                            infor_tts = {}
                            infor_tts['hhjp_0001'] = format_hoso.kiemtra(tts.ten_lt_tts)
                            infor_tts['hhjp_0001_1'] = format_hoso.convert_date(tts.ketthuc_thucte_namba)
                            infor_tts['hhjp_0006'] = format_hoso.gender_check(tts.gtinh_tts)
                            infor_tts['hhjp_0003'] = format_hoso.kiemtra(tts.quoctich_tts.name_quoctich)
                            infor_tts['hhjp_0004'] = format_hoso.convert_date(tts.ngaysinh_tts)
                            infor_tts['hhjp_0131'] = format_hoso.kiemtra(tts.kithi_chuyennhuong_namba.ten_kithi)

                            infor_tts['hhjp_0131_7'] = '□'

                            if tts.ngayqua_lythuyet_motcongviec_namba and tts.ngayqua_thuchanh_motcongviec_namba:
                                infor_tts['td_0002'] = format_hoso.convert_date(
                                    tts.ngayqua_thuchanh_motcongviec_namba)
                            elif tts.ngayqua_thuchanh_motcongviec_namba:
                                infor_tts['td_0002'] = format_hoso.convert_date(
                                    tts.ngayqua_thuchanh_motcongviec_namba)
                            elif tts.ngayqua_lythuyet_motcongviec_namhai and tts.ngayqua_thuchanh_motcongviec_namba == False:
                                infor_tts['td_0002'] = format_hoso.convert_date(
                                    tts.ngayqua_lythuyet_motcongviec_namba)
                            else:
                                infor_tts['td_0002'] = '年　月　日'

                            if tts.ketqua_lythuyet_motcongviec_namba == 'Qua' and tts.ketqua_thuchanh_motcongviec_namba == 'Qua':
                                infor_tts['hhjp_0131_1'] = '■'
                                infor_tts['hhjp_0131_2'] = '■'
                                infor_tts['hhjp_0131_3'] = '□'
                                infor_tts['hhjp_0131_4'] = '□'
                                infor_tts['hhjp_0131_5'] = '□'
                                infor_tts['hhjp_0131_6'] = '□'
                            elif tts.ketqua_lythuyet_motcongviec_namba == 'Qua' and tts.ketqua_thuchanh_motcongviec_namba == 'Không qua':
                                infor_tts['hhjp_0131_1'] = '■'
                                infor_tts['hhjp_0131_2'] = '□'
                                infor_tts['hhjp_0131_3'] = '■'
                                infor_tts['hhjp_0131_4'] = '□'
                                infor_tts['hhjp_0131_5'] = '□'
                                infor_tts['hhjp_0131_6'] = '□'
                            elif tts.ketqua_lythuyet_motcongviec_namba == 'Không qua' and tts.ketqua_thuchanh_motcongviec_namba == 'Qua':
                                infor_tts['hhjp_0131_1'] = '■'
                                infor_tts['hhjp_0131_2'] = '□'
                                infor_tts['hhjp_0131_3'] = '□'
                                infor_tts['hhjp_0131_4'] = '■'
                                infor_tts['hhjp_0131_5'] = '□'
                                infor_tts['hhjp_0131_6'] = '□'
                            elif tts.ketqua_lythuyet_motcongviec_namba == 'Không qua' and tts.ketqua_thuchanh_motcongviec_namba == 'Không qua':
                                infor_tts['hhjp_0131_1'] = '□'
                                infor_tts['hhjp_0131_2'] = '□'
                                infor_tts['hhjp_0131_3'] = '□'
                                infor_tts['hhjp_0131_4'] = '□'
                                infor_tts['hhjp_0131_5'] = '■'
                                infor_tts['hhjp_0131_6'] = '□'
                            elif tts.ketqua_lythuyet_motcongviec_namba == False and tts.ketqua_thuchanh_motcongviec_namba == False:
                                infor_tts['hhjp_0131_1'] = '□'
                                infor_tts['hhjp_0131_2'] = '□'
                                infor_tts['hhjp_0131_3'] = '□'
                                infor_tts['hhjp_0131_4'] = '□'
                                infor_tts['hhjp_0131_5'] = '□'
                                infor_tts['hhjp_0131_6'] = '■'
                                infor_tts['hhjp_0911'] = format_hoso.kiemtra(tts.sochungnhan_namba)
                            else:
                                infor_tts['hhjp_0131_1'] = '□'
                                infor_tts['hhjp_0131_2'] = '□'
                                infor_tts['hhjp_0131_3'] = '□'
                                infor_tts['hhjp_0131_4'] = '□'
                                infor_tts['hhjp_0131_5'] = '□'
                                infor_tts['hhjp_0131_6'] = '□'

                            tbl_tts.append(infor_tts)
                if tbl_tts != []:
                    infor['tbl_thuctapsinh'] = tbl_tts
                    table_fill.append(infor)
            context['tts_xinghiep'] = table_fill

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_33(self, ma_thuctapsinh, document):
        print('33')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_33")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            context = {}
            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban)
            context[
                'hhjp_0905'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_daidien_han_nghiepdoan)
            context[
                'hhjp_0906'] = format_hoso.kiemtra(
                document.donhang_hoso.nghiepdoan_donhang.chucvu_daidien_phienam_nghiepdoan)
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_34(self, ma_thuctapsinh, document):
        print('34')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_34")], limit=1)
        if docs:

            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            if document.giaidoan_hoso == u'2 go':
                giaidoan = self.env['giaidoan.giaidoan'].search([('ten_giaidoan', '=', '２号－１年目')], limit=1)
            if document.giaidoan_hoso == u'3 go':
                giaidoan = self.env['giaidoan.giaidoan'].search([('ten_giaidoan', '=', '３号－１年目')], limit=1)
            donhang_congviec = self.env['chitiet.chitiet'].search(
                [('donhang_chitiet', '=', document.donhang_hoso.id), ('giaidoan', '=', giaidoan.id)], limit=1)
            context = {}

            for chinnhanh_chitiet in donhang_congviec.chinnhanh_chitiet:
                context['hhjp_0041_1'] = format_hoso.kiemtra(chinnhanh_chitiet.ten_chinhanh_han)
                context['hhjp_0042_1'] = format_hoso.kiemtra(chinnhanh_chitiet.diachi_chinhanh)
            for chinnhanh_chitiet_2 in donhang_congviec.chinnhanh_chitiet_2:
                context['hhjp_0041_2'] = format_hoso.kiemtra(chinnhanh_chitiet_2.ten_chinhanh_han)
                context['hhjp_0042_2'] = format_hoso.kiemtra(chinnhanh_chitiet_2.diachi_chinhanh)
            for chinnhanh_chitiet_3 in donhang_congviec.chinnhanh_chitiet_3:
                context['hhjp_0041_3'] = format_hoso.kiemtra(chinnhanh_chitiet_3.ten_chinhanh_han)
                context['hhjp_0042_3'] = format_hoso.kiemtra(chinnhanh_chitiet_3.diachi_chinhanh)

            context['hhjp_0313'] = format_hoso.convert_date(thuctapsinh.batdau_kehoach_namhai)
            context['hhjp_0314'] = format_hoso.convert_date(thuctapsinh.ketthuc_kehoach_namhai_go1)
            context['hhjp_0127'] = Listing(
                donhang_congviec.nguyenluyen_chitiet) if donhang_congviec.nguyenluyen_chitiet else ''
            context['hhjp_0128'] = Listing(
                donhang_congviec.congcu_maymoc_chitiet) if donhang_congviec.congcu_maymoc_chitiet else ''
            context['hhjp_0129'] = Listing(
                donhang_congviec.sanpham_chitiet) if donhang_congviec.sanpham_chitiet else ''
            context['hhjp_0121'] = Listing(
                donhang_congviec.noidung_congviec_mot) if donhang_congviec.noidung_congviec_mot else ''
            for danhsach_nhanvien in document.xinghiep_hoso.danhsach_nhanvien:
                if danhsach_nhanvien.vaitro_six == True:
                    context['hhjp_0121_15'] = format_hoso.kiemtra(danhsach_nhanvien.ten_han_nhanvien)
                    context['hhjp_0121_14'] = format_hoso.kiemtra(danhsach_nhanvien.kinhnghiem_nhanvien_chidaothuctap)
                    context['hhjp_0121_13'] = format_hoso.kiemtra(danhsach_nhanvien.chucdanh_han_nhanvien)
                    context['hhjp_0130'] = format_hoso.kiemtra(danhsach_nhanvien.bangcap_nhanvien)

            context['hhjp_0121_ts'] = format_hoso.kiemtra(donhang_congviec.coso_congviec_mot)
            context['hhjp_0121_t'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_mot)
            context['hhjp_0121_1'] = format_hoso.kiemtra(donhang_congviec.thangmot_thoigian_mot)
            context['hhjp_0121_2'] = format_hoso.kiemtra(donhang_congviec.thanghai_thoigian_mot)
            context['hhjp_0121_3'] = format_hoso.kiemtra(donhang_congviec.thangba_thoigian_mot)
            context['hhjp_0121_4'] = format_hoso.kiemtra(donhang_congviec.thangbon_thoigian_mot)
            context['hhjp_0121_5'] = format_hoso.kiemtra(donhang_congviec.thangnam_thoigian_mot)
            context['hhjp_0121_6'] = format_hoso.kiemtra(donhang_congviec.thangsau_thoigian_mot)
            context['hhjp_0121_7'] = format_hoso.kiemtra(donhang_congviec.thangbay_thoigian_mot)
            context['hhjp_0121_8'] = format_hoso.kiemtra(donhang_congviec.thangtam_thoigian_mot)
            context['hhjp_0121_9'] = format_hoso.kiemtra(donhang_congviec.thangchin_thoigian_mot)
            context['hhjp_0121_10'] = format_hoso.kiemtra(donhang_congviec.thangmuoi_thoigian_mot)
            context['hhjp_0121_11'] = format_hoso.kiemtra(donhang_congviec.thangmuoimot_thoigian_mot)
            context['hhjp_0121_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_mot)
            context['hhjp_0121_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_mot)

            context['hhjp_0122'] = Listing(
                donhang_congviec.noidung_congviec_hai) if donhang_congviec.noidung_congviec_hai else ''
            context['hhjp_0122_ts'] = format_hoso.kiemtra(donhang_congviec.coso_congviec_hai)
            context['hhjp_0122_t'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_hai)
            context['hhjp_0122_1'] = format_hoso.kiemtra(donhang_congviec.thangmot_thoigian_hai)
            context['hhjp_0122_2'] = format_hoso.kiemtra(donhang_congviec.thanghai_thoigian_hai)
            context['hhjp_0122_3'] = format_hoso.kiemtra(donhang_congviec.thangba_thoigian_hai)
            context['hhjp_0122_4'] = format_hoso.kiemtra(donhang_congviec.thangbon_thoigian_hai)
            context['hhjp_0122_5'] = format_hoso.kiemtra(donhang_congviec.thangnam_thoigian_hai)
            context['hhjp_0122_6'] = format_hoso.kiemtra(donhang_congviec.thangsau_thoigian_hai)
            context['hhjp_0122_7'] = format_hoso.kiemtra(donhang_congviec.thangbay_thoigian_hai)
            context['hhjp_0122_8'] = format_hoso.kiemtra(donhang_congviec.thangtam_thoigian_hai)
            context['hhjp_0122_9'] = format_hoso.kiemtra(donhang_congviec.thangchin_thoigian_hai)
            context['hhjp_0122_10'] = format_hoso.kiemtra(donhang_congviec.thangmuoi_thoigian_hai)
            context['hhjp_0122_11'] = format_hoso.kiemtra(donhang_congviec.thangmuoimot_thoigian_hai)
            context['hhjp_0122_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_hai)
            context['hhjp_0122_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_hai)

            context['hhjp_0123'] = Listing(
                donhang_congviec.noidung_congviec_ba) if donhang_congviec.noidung_congviec_ba else ''
            context['hhjp_0123_ts'] = format_hoso.kiemtra(donhang_congviec.coso_congviec_ba)
            context['hhjp_0123_t'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_ba)
            context['hhjp_0123_1'] = format_hoso.kiemtra(donhang_congviec.thangmot_thoigian_ba)
            context['hhjp_0123_2'] = format_hoso.kiemtra(donhang_congviec.thanghai_thoigian_ba)
            context['hhjp_0123_3'] = format_hoso.kiemtra(donhang_congviec.thangba_thoigian_ba)
            context['hhjp_0123_4'] = format_hoso.kiemtra(donhang_congviec.thangbon_thoigian_ba)
            context['hhjp_0123_5'] = format_hoso.kiemtra(donhang_congviec.thangnam_thoigian_ba)
            context['hhjp_0123_6'] = format_hoso.kiemtra(donhang_congviec.thangsau_thoigian_ba)
            context['hhjp_0123_7'] = format_hoso.kiemtra(donhang_congviec.thangbay_thoigian_ba)
            context['hhjp_0123_8'] = format_hoso.kiemtra(donhang_congviec.thangtam_thoigian_ba)
            context['hhjp_0123_9'] = format_hoso.kiemtra(donhang_congviec.thangchin_thoigian_ba)
            context['hhjp_0123_10'] = format_hoso.kiemtra(donhang_congviec.thangmuoi_thoigian_ba)
            context['hhjp_0123_11'] = format_hoso.kiemtra(donhang_congviec.thangmuoimot_thoigian_ba)
            context['hhjp_0123_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_ba)
            context['hhjp_0123_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_ba)

            context['hhjp_0124'] = Listing(
                donhang_congviec.noidung_congviec_bon) if donhang_congviec.noidung_congviec_bon else ''
            context['hhjp_0124_ts'] = format_hoso.kiemtra(donhang_congviec.coso_congviec_bon)
            context['hhjp_0124_t'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_bon)
            context['hhjp_0124_1'] = format_hoso.kiemtra(donhang_congviec.thangmot_thoigian_bon)
            context['hhjp_0124_2'] = format_hoso.kiemtra(donhang_congviec.thanghai_thoigian_bon)
            context['hhjp_0124_3'] = format_hoso.kiemtra(donhang_congviec.thangba_thoigian_bon)
            context['hhjp_0124_4'] = format_hoso.kiemtra(donhang_congviec.thangbon_thoigian_bon)
            context['hhjp_0124_5'] = format_hoso.kiemtra(donhang_congviec.thangnam_thoigian_bon)
            context['hhjp_0124_6'] = format_hoso.kiemtra(donhang_congviec.thangsau_thoigian_bon)
            context['hhjp_0124_7'] = format_hoso.kiemtra(donhang_congviec.thangbay_thoigian_bon)
            context['hhjp_0124_8'] = format_hoso.kiemtra(donhang_congviec.thangtam_thoigian_bon)
            context['hhjp_0124_9'] = format_hoso.kiemtra(donhang_congviec.thangchin_thoigian_bon)
            context['hhjp_0124_10'] = format_hoso.kiemtra(donhang_congviec.thangmuoi_thoigian_bon)
            context['hhjp_0124_11'] = format_hoso.kiemtra(donhang_congviec.thangmuoimot_thoigian_bon)
            context['hhjp_0124_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_bon)
            context['hhjp_0124_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_bon)

            context['hhjp_0125'] = Listing(
                donhang_congviec.noidung_congviec_nam) if donhang_congviec.noidung_congviec_nam else ''
            context['hhjp_0125_ts'] = format_hoso.kiemtra(donhang_congviec.coso_congviec_nam)
            context['hhjp_0125_t'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_nam)
            context['hhjp_0125_1'] = format_hoso.kiemtra(donhang_congviec.thangmot_thoigian_nam)
            context['hhjp_0125_2'] = format_hoso.kiemtra(donhang_congviec.thanghai_thoigian_nam)
            context['hhjp_0125_3'] = format_hoso.kiemtra(donhang_congviec.thangba_thoigian_nam)
            context['hhjp_0125_4'] = format_hoso.kiemtra(donhang_congviec.thangbon_thoigian_nam)
            context['hhjp_0125_5'] = format_hoso.kiemtra(donhang_congviec.thangnam_thoigian_nam)
            context['hhjp_0125_6'] = format_hoso.kiemtra(donhang_congviec.thangsau_thoigian_nam)
            context['hhjp_0125_7'] = format_hoso.kiemtra(donhang_congviec.thangbay_thoigian_nam)
            context['hhjp_0125_8'] = format_hoso.kiemtra(donhang_congviec.thangtam_thoigian_nam)
            context['hhjp_0125_9'] = format_hoso.kiemtra(donhang_congviec.thangchin_thoigian_nam)
            context['hhjp_0125_10'] = format_hoso.kiemtra(donhang_congviec.thangmuoi_thoigian_nam)
            context['hhjp_0125_11'] = format_hoso.kiemtra(donhang_congviec.thangmuoimot_thoigian_nam)
            context['hhjp_0125_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_nam)
            context['hhjp_0125_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_nam)

            context['hhjp_0126'] = Listing(
                donhang_congviec.noidung_congviec_sau) if donhang_congviec.noidung_congviec_sau else ''
            context['hhjp_0126_ts'] = format_hoso.kiemtra(donhang_congviec.coso_congviec_sau)
            context['hhjp_0126_t'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_sau)
            context['hhjp_0126_1'] = format_hoso.kiemtra(donhang_congviec.thangmot_thoigian_sau)
            context['hhjp_0126_2'] = format_hoso.kiemtra(donhang_congviec.thanghai_thoigian_sau)
            context['hhjp_0126_3'] = format_hoso.kiemtra(donhang_congviec.thangba_thoigian_sau)
            context['hhjp_0126_4'] = format_hoso.kiemtra(donhang_congviec.thangbon_thoigian_sau)
            context['hhjp_0126_5'] = format_hoso.kiemtra(donhang_congviec.thangnam_thoigian_sau)
            context['hhjp_0126_6'] = format_hoso.kiemtra(donhang_congviec.thangsau_thoigian_sau)
            context['hhjp_0126_7'] = format_hoso.kiemtra(donhang_congviec.thangbay_thoigian_sau)
            context['hhjp_0126_8'] = format_hoso.kiemtra(donhang_congviec.thangtam_thoigian_sau)
            context['hhjp_0126_9'] = format_hoso.kiemtra(donhang_congviec.thangchin_thoigian_sau)
            context['hhjp_0126_10'] = format_hoso.kiemtra(donhang_congviec.thangmuoi_thoigian_sau)
            context['hhjp_0126_11'] = format_hoso.kiemtra(donhang_congviec.thangmuoimot_thoigian_sau)
            context['hhjp_0126_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_sau)
            context['hhjp_0126_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_sau)

            context['hhjp_0121_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian)
            context['hhjp_0122_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangmot)
            context['hhjp_0123_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thanghai)
            context['hhjp_0124_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangba)
            context['hhjp_0125_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangbon)
            context['hhjp_0126_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangnam)
            context['hhjp_0127_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangsau)
            context['hhjp_0128_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangbay)
            context['hhjp_0129_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangtam)
            context['hhjp_0130_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangchin)
            context['hhjp_0131_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangmuoi)
            context['hhjp_0132_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangmuoimot)
            context['hhjp_0133_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangmuoihai)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_35(self, ma_thuctapsinh, document):
        print('35')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_35")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            if document.giaidoan_hoso == u'2 go':
                giaidoan = self.env['giaidoan.giaidoan'].search([('ten_giaidoan', '=', '２号－２年目')], limit=1)
            if document.giaidoan_hoso == u'3 go':
                giaidoan = self.env['giaidoan.giaidoan'].search([('ten_giaidoan', '=', '３号－２年目')], limit=1)
            donhang_congviec = self.env['chitiet.chitiet'].search(
                [('donhang_chitiet', '=', document.donhang_hoso.id), ('giaidoan', '=', giaidoan.id)], limit=1)
            context = {}

            for chinnhanh_chitiet in donhang_congviec.chinnhanh_chitiet:
                context['hhjp_0041_1'] = format_hoso.kiemtra(chinnhanh_chitiet.ten_chinhanh_han)
                context['hhjp_0042_1'] = format_hoso.kiemtra(chinnhanh_chitiet.diachi_chinhanh)
            for chinnhanh_chitiet_2 in donhang_congviec.chinnhanh_chitiet_2:
                context['hhjp_0041_2'] = format_hoso.kiemtra(chinnhanh_chitiet_2.ten_chinhanh_han)
                context['hhjp_0042_2'] = format_hoso.kiemtra(chinnhanh_chitiet_2.diachi_chinhanh)
            for chinnhanh_chitiet_3 in donhang_congviec.chinnhanh_chitiet_3:
                context['hhjp_0041_3'] = format_hoso.kiemtra(chinnhanh_chitiet_3.ten_chinhanh_han)
                context['hhjp_0042_3'] = format_hoso.kiemtra(chinnhanh_chitiet_3.diachi_chinhanh)

            context['hhjp_0313'] = format_hoso.convert_date(thuctapsinh.ketthuc_kehoach_namhai_go1 + datetime.timedelta(
                days=1)) if thuctapsinh.ketthuc_kehoach_namhai_go1 else ''
            context['hhjp_0314'] = format_hoso.convert_date(thuctapsinh.ketthuc_kehoach_namhai)
            context['hhjp_0127'] = Listing(
                donhang_congviec.nguyenluyen_chitiet) if donhang_congviec.nguyenluyen_chitiet else ''
            context['hhjp_0128'] = Listing(
                donhang_congviec.congcu_maymoc_chitiet) if donhang_congviec.congcu_maymoc_chitiet else ''
            context['hhjp_0129'] = Listing(
                donhang_congviec.sanpham_chitiet) if donhang_congviec.sanpham_chitiet else ''
            context['hhjp_0121'] = Listing(
                donhang_congviec.noidung_congviec_mot) if donhang_congviec.noidung_congviec_mot else ''
            for danhsach_nhanvien in document.xinghiep_hoso.danhsach_nhanvien:
                if danhsach_nhanvien.vaitro_six == True:
                    context['hhjp_0121_15'] = format_hoso.kiemtra(danhsach_nhanvien.ten_han_nhanvien)
                    context['hhjp_0121_14'] = format_hoso.kiemtra(
                        int(danhsach_nhanvien.kinhnghiem_nhanvien_chidaothuctap) + 1)
                    context['hhjp_0121_13'] = format_hoso.kiemtra(danhsach_nhanvien.chucdanh_han_nhanvien)
                    context['hhjp_0130'] = format_hoso.kiemtra(danhsach_nhanvien.bangcap_nhanvien)
            context['hhjp_0121_ts'] = format_hoso.kiemtra(donhang_congviec.coso_congviec_mot)
            context['hhjp_0121_t'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_mot)
            context['hhjp_0121_1'] = format_hoso.kiemtra(donhang_congviec.thangmot_thoigian_mot)
            context['hhjp_0121_2'] = format_hoso.kiemtra(donhang_congviec.thanghai_thoigian_mot)
            context['hhjp_0121_3'] = format_hoso.kiemtra(donhang_congviec.thangba_thoigian_mot)
            context['hhjp_0121_4'] = format_hoso.kiemtra(donhang_congviec.thangbon_thoigian_mot)
            context['hhjp_0121_5'] = format_hoso.kiemtra(donhang_congviec.thangnam_thoigian_mot)
            context['hhjp_0121_6'] = format_hoso.kiemtra(donhang_congviec.thangsau_thoigian_mot)
            context['hhjp_0121_7'] = format_hoso.kiemtra(donhang_congviec.thangbay_thoigian_mot)
            context['hhjp_0121_8'] = format_hoso.kiemtra(donhang_congviec.thangtam_thoigian_mot)
            context['hhjp_0121_9'] = format_hoso.kiemtra(donhang_congviec.thangchin_thoigian_mot)
            context['hhjp_0121_10'] = format_hoso.kiemtra(donhang_congviec.thangmuoi_thoigian_mot)
            context['hhjp_0121_11'] = format_hoso.kiemtra(donhang_congviec.thangmuoimot_thoigian_mot)
            context['hhjp_0121_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_mot)
            context['hhjp_0121_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_mot)

            context['hhjp_0122'] = Listing(
                donhang_congviec.noidung_congviec_hai) if donhang_congviec.noidung_congviec_hai else ''
            context['hhjp_0122_ts'] = format_hoso.kiemtra(donhang_congviec.coso_congviec_hai)
            context['hhjp_0122_t'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_hai)
            context['hhjp_0122_1'] = format_hoso.kiemtra(donhang_congviec.thangmot_thoigian_hai)
            context['hhjp_0122_2'] = format_hoso.kiemtra(donhang_congviec.thanghai_thoigian_hai)
            context['hhjp_0122_3'] = format_hoso.kiemtra(donhang_congviec.thangba_thoigian_hai)
            context['hhjp_0122_4'] = format_hoso.kiemtra(donhang_congviec.thangbon_thoigian_hai)
            context['hhjp_0122_5'] = format_hoso.kiemtra(donhang_congviec.thangnam_thoigian_hai)
            context['hhjp_0122_6'] = format_hoso.kiemtra(donhang_congviec.thangsau_thoigian_hai)
            context['hhjp_0122_7'] = format_hoso.kiemtra(donhang_congviec.thangbay_thoigian_hai)
            context['hhjp_0122_8'] = format_hoso.kiemtra(donhang_congviec.thangtam_thoigian_hai)
            context['hhjp_0122_9'] = format_hoso.kiemtra(donhang_congviec.thangchin_thoigian_hai)
            context['hhjp_0122_10'] = format_hoso.kiemtra(donhang_congviec.thangmuoi_thoigian_hai)
            context['hhjp_0122_11'] = format_hoso.kiemtra(donhang_congviec.thangmuoimot_thoigian_hai)
            context['hhjp_0122_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_hai)
            context['hhjp_0122_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_hai)

            context['hhjp_0123'] = Listing(
                donhang_congviec.noidung_congviec_ba) if donhang_congviec.noidung_congviec_ba else ''
            context['hhjp_0123_ts'] = format_hoso.kiemtra(donhang_congviec.coso_congviec_ba)
            context['hhjp_0123_t'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_ba)
            context['hhjp_0123_1'] = format_hoso.kiemtra(donhang_congviec.thangmot_thoigian_ba)
            context['hhjp_0123_2'] = format_hoso.kiemtra(donhang_congviec.thanghai_thoigian_ba)
            context['hhjp_0123_3'] = format_hoso.kiemtra(donhang_congviec.thangba_thoigian_ba)
            context['hhjp_0123_4'] = format_hoso.kiemtra(donhang_congviec.thangbon_thoigian_ba)
            context['hhjp_0123_5'] = format_hoso.kiemtra(donhang_congviec.thangnam_thoigian_ba)
            context['hhjp_0123_6'] = format_hoso.kiemtra(donhang_congviec.thangsau_thoigian_ba)
            context['hhjp_0123_7'] = format_hoso.kiemtra(donhang_congviec.thangbay_thoigian_ba)
            context['hhjp_0123_8'] = format_hoso.kiemtra(donhang_congviec.thangtam_thoigian_ba)
            context['hhjp_0123_9'] = format_hoso.kiemtra(donhang_congviec.thangchin_thoigian_ba)
            context['hhjp_0123_10'] = format_hoso.kiemtra(donhang_congviec.thangmuoi_thoigian_ba)
            context['hhjp_0123_11'] = format_hoso.kiemtra(donhang_congviec.thangmuoimot_thoigian_ba)
            context['hhjp_0123_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_ba)
            context['hhjp_0123_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_ba)

            context['hhjp_0124'] = Listing(
                donhang_congviec.noidung_congviec_bon) if donhang_congviec.noidung_congviec_bon else ''
            context['hhjp_0124_ts'] = format_hoso.kiemtra(donhang_congviec.coso_congviec_bon)
            context['hhjp_0124_t'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_bon)
            context['hhjp_0124_1'] = format_hoso.kiemtra(donhang_congviec.thangmot_thoigian_bon)
            context['hhjp_0124_2'] = format_hoso.kiemtra(donhang_congviec.thanghai_thoigian_bon)
            context['hhjp_0124_3'] = format_hoso.kiemtra(donhang_congviec.thangba_thoigian_bon)
            context['hhjp_0124_4'] = format_hoso.kiemtra(donhang_congviec.thangbon_thoigian_bon)
            context['hhjp_0124_5'] = format_hoso.kiemtra(donhang_congviec.thangnam_thoigian_bon)
            context['hhjp_0124_6'] = format_hoso.kiemtra(donhang_congviec.thangsau_thoigian_bon)
            context['hhjp_0124_7'] = format_hoso.kiemtra(donhang_congviec.thangbay_thoigian_bon)
            context['hhjp_0124_8'] = format_hoso.kiemtra(donhang_congviec.thangtam_thoigian_bon)
            context['hhjp_0124_9'] = format_hoso.kiemtra(donhang_congviec.thangchin_thoigian_bon)
            context['hhjp_0124_10'] = format_hoso.kiemtra(donhang_congviec.thangmuoi_thoigian_bon)
            context['hhjp_0124_11'] = format_hoso.kiemtra(donhang_congviec.thangmuoimot_thoigian_bon)
            context['hhjp_0124_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_bon)
            context['hhjp_0124_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_bon)

            context['hhjp_0125'] = Listing(
                donhang_congviec.noidung_congviec_nam) if donhang_congviec.noidung_congviec_nam else ''
            context['hhjp_0125_ts'] = format_hoso.kiemtra(donhang_congviec.coso_congviec_nam)
            context['hhjp_0125_t'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_nam)
            context['hhjp_0125_1'] = format_hoso.kiemtra(donhang_congviec.thangmot_thoigian_nam)
            context['hhjp_0125_2'] = format_hoso.kiemtra(donhang_congviec.thanghai_thoigian_nam)
            context['hhjp_0125_3'] = format_hoso.kiemtra(donhang_congviec.thangba_thoigian_nam)
            context['hhjp_0125_4'] = format_hoso.kiemtra(donhang_congviec.thangbon_thoigian_nam)
            context['hhjp_0125_5'] = format_hoso.kiemtra(donhang_congviec.thangnam_thoigian_nam)
            context['hhjp_0125_6'] = format_hoso.kiemtra(donhang_congviec.thangsau_thoigian_nam)
            context['hhjp_0125_7'] = format_hoso.kiemtra(donhang_congviec.thangbay_thoigian_nam)
            context['hhjp_0125_8'] = format_hoso.kiemtra(donhang_congviec.thangtam_thoigian_nam)
            context['hhjp_0125_9'] = format_hoso.kiemtra(donhang_congviec.thangchin_thoigian_nam)
            context['hhjp_0125_10'] = format_hoso.kiemtra(donhang_congviec.thangmuoi_thoigian_nam)
            context['hhjp_0125_11'] = format_hoso.kiemtra(donhang_congviec.thangmuoimot_thoigian_nam)
            context['hhjp_0125_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_nam)
            context['hhjp_0125_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_nam)

            context['hhjp_0126'] = Listing(
                donhang_congviec.noidung_congviec_sau) if donhang_congviec.noidung_congviec_sau else ''
            context['hhjp_0126_ts'] = format_hoso.kiemtra(donhang_congviec.coso_congviec_sau)
            context['hhjp_0126_t'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_sau)
            context['hhjp_0126_1'] = format_hoso.kiemtra(donhang_congviec.thangmot_thoigian_sau)
            context['hhjp_0126_2'] = format_hoso.kiemtra(donhang_congviec.thanghai_thoigian_sau)
            context['hhjp_0126_3'] = format_hoso.kiemtra(donhang_congviec.thangba_thoigian_sau)
            context['hhjp_0126_4'] = format_hoso.kiemtra(donhang_congviec.thangbon_thoigian_sau)
            context['hhjp_0126_5'] = format_hoso.kiemtra(donhang_congviec.thangnam_thoigian_sau)
            context['hhjp_0126_6'] = format_hoso.kiemtra(donhang_congviec.thangsau_thoigian_sau)
            context['hhjp_0126_7'] = format_hoso.kiemtra(donhang_congviec.thangbay_thoigian_sau)
            context['hhjp_0126_8'] = format_hoso.kiemtra(donhang_congviec.thangtam_thoigian_sau)
            context['hhjp_0126_9'] = format_hoso.kiemtra(donhang_congviec.thangchin_thoigian_sau)
            context['hhjp_0126_10'] = format_hoso.kiemtra(donhang_congviec.thangmuoi_thoigian_sau)
            context['hhjp_0126_11'] = format_hoso.kiemtra(donhang_congviec.thangmuoimot_thoigian_sau)
            context['hhjp_0126_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_sau)
            context['hhjp_0126_12'] = format_hoso.kiemtra(donhang_congviec.thangmuoihai_thoigian_sau)

            context['hhjp_0121_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian)
            context['hhjp_0122_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangmot)
            context['hhjp_0123_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thanghai)
            context['hhjp_0124_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangba)
            context['hhjp_0125_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangbon)
            context['hhjp_0126_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangnam)
            context['hhjp_0127_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangsau)
            context['hhjp_0128_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangbay)
            context['hhjp_0129_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangtam)
            context['hhjp_0130_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangchin)
            context['hhjp_0131_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangmuoi)
            context['hhjp_0132_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangmuoimot)
            context['hhjp_0133_tong'] = format_hoso.kiemtra(donhang_congviec.tong_thoigian_thangmuoihai)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_36(self, ma_thuctapsinh, document):
        print('36')
        sotts = len(document.thuctapsinh_hoso)
        if sotts == 1:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_99")], limit=1)
        else:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_99_1")], limit=1)

        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)

            if document.ngay_lapvanban:
                year_now = document.ngay_lapvanban.year
                year_fill = year_now - 2019
                context['bc_0502'] = format_hoso.convert_date2(year_fill, document.ngay_lapvanban.month,
                                                               document.ngay_lapvanban.day)
            context['bc_0503'] = sotts * 3900
            context['bc_0504_1'] = '■'
            context['bc_0504_2'] = '□'

            if sotts == 1:
                context['bc_0505_1'] = '■'
                context['bc_0505_2'] = '□'
                context['bc_0506'] = format_hoso.kiemtra(thuctapsinh.quoctich_tts.name_quoctich)
                context['bc_0507'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
                context['bc_0512'] = ''
            else:
                context['bc_0505_1'] = '□'
                context['bc_0505_2'] = '■'
                context['bc_0512'] = sotts
                context['bc_0506'] = ''
                context['bc_0507'] = ''

            context['bc_0509'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            context['bc_0510'] = format_hoso.kiemtra(document.xinghiep_hoso.chucvu_daidien_han)
            context['bc_0511'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep)

            table_tbltts = []
            for thuctaps in document.thuctapsinh_hoso:
                infor = {}
                infor['hhjp_0001'] = format_hoso.kiemtra(thuctaps.ten_lt_tts)
                infor['hhjp_0002'] = format_hoso.kiemtra(thuctaps.ten_han_tts)
                infor['hhjp_0005'] = format_hoso.kiemtra(thuctaps.tuoi_tts)
                if thuctaps.gtinh_tts == 'Nam':
                    infor['hhjp_0006'] = '男'
                else:
                    infor['hhjp_0006'] = '女'
                infor['hhjp_0003'] = format_hoso.kiemtra(thuctaps.quoctich_tts.name_quoctich)
                table_tbltts.append(infor)

            context['tbl_tts'] = table_tbltts

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_50(self, ma_thuctapsinh, document):
        print('50')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_50")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            context = {}

            if document.ngay_lapvanban1:
                context['hhjp_0193'] = str(document.ngay_lapvanban1.year) + '年' + \
                                       str(document.ngay_lapvanban1.month) + '月' + \
                                       str(document.ngay_lapvanban1.day) + '日'
                context['hhjp_0193_v'] = 'Ngày ' + str(document.ngay_lapvanban1.day) + \
                                         ' tháng ' + str(document.ngay_lapvanban1.month) + \
                                         ' năm ' + str(document.ngay_lapvanban1.year)
            else:
                context['hhjp_0193'] = '年　月　日'
                context['hhjp_0193_v'] = 'Ngày tháng năm'

            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            if thuctapsinh.ten_han_tts:
                context['hhjp_0002'] = format_hoso.kiemtra(thuctapsinh.ten_han_tts)
            else:
                context['hhjp_0002'] = format_hoso.kiemtra(thuctapsinh.ten_katakana_tts)
            context['hhjp_0003'] = format_hoso.kiemtra(thuctapsinh.quoctich_tts.name_quoctich)
            context['hhjp_0003_v'] = format_hoso.kiemtra(thuctapsinh.quoctich_tts_v)

            if thuctapsinh.gtinh_tts == u'Nam':
                docdata.remove_shape(u'id="10" name="Oval 10"')
            elif thuctapsinh.gtinh_tts == u'Nữ':
                docdata.remove_shape(u'id="3" name="Oval 3"')
            else:
                docdata.remove_shape(u'id="10" name="Oval 10"')
                docdata.remove_shape(u'id="3" name="Oval 3"')

            if thuctapsinh.nguoikethon_tts == u'Có':
                docdata.remove_shape(u'id="5" name="Oval 5"')
            elif thuctapsinh.nguoikethon_tts == u'Không':
                docdata.remove_shape(u'id="4" name="Oval 4"')
            else:
                docdata.remove_shape(u'id="5" name="Oval 5"')
                docdata.remove_shape(u'id="4" name="Oval 4"')

            context['hhjp_0330'] = format_hoso.kiemtra(thuctapsinh.tiengmede_tts)
            context['hhjp_0330_v'] = format_hoso.kiemtra(thuctapsinh.tiengmede_tts_v)
            if thuctapsinh.ngaysinh_tts:
                context['hhjp_0004'] = str(thuctapsinh.ngaysinh_tts.year) + '年' + \
                                       str(thuctapsinh.ngaysinh_tts.month) + '月' + \
                                       str(thuctapsinh.ngaysinh_tts.day) + '日'

                context['hhjp_0004_v'] = 'Ngày ' + str(thuctapsinh.ngaysinh_tts.day) + \
                                         ' tháng ' + str(thuctapsinh.ngaysinh_tts.month) + \
                                         ' năm ' + str(thuctapsinh.ngaysinh_tts.year)

            else:
                context['hhjp_0004'] = '年　月　日'
                context['hhjp_0004_v'] = 'Ngày tháng năm'

            context['hhjp_0005'] = format_hoso.kiemtra(thuctapsinh.tuoi_tts_pc)
            context['hhjp_0005_v'] = format_hoso.kiemtra(thuctapsinh.tuoi_tts_pc)
            context['hhjp_0331'] = format_hoso.kiemtra(thuctapsinh.diachi_tts)
            context['hhjp_0331_v'] = format_hoso.kiemtra(thuctapsinh.diachi_tts_v).upper()

            hhjp_0342_1 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_hoctap_nam_1,
                                                              thuctapsinh.batdau_hoctap_thang_1)) if thuctapsinh.batdau_hoctap_nam_1 else ''
            hhjp_0343_1 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_hoctap_nam_1,
                                                              thuctapsinh.ketthuc_hoctap_thang_1)) if thuctapsinh.ketthuc_hoctap_nam_1 else ''
            context['hhjp_0342_1'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0342_1, hhjp_0343_1))

            context['hhjp_0344_1'] = format_hoso.kiemtra(thuctapsinh.truong_tts_1)

            hhjp_0342_v1 = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_hoctap_nam_1,
                                                                 thuctapsinh.batdau_hoctap_thang_1)) if thuctapsinh.batdau_hoctap_nam_1 else ''
            hhjp_0343_v1 = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_hoctap_nam_1,
                                                                 thuctapsinh.ketthuc_hoctap_thang_1)) if thuctapsinh.ketthuc_hoctap_nam_1 else ''
            context['hhjp_0342_v1'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0342_v1, hhjp_0343_v1))

            context['hhjp_0344_v1'] = format_hoso.kiemtra(thuctapsinh.truong_tts_v_1)

            hhjp_0342_2 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_hoctap_nam_2,
                                                              thuctapsinh.batdau_hoctap_thang_2)) if thuctapsinh.batdau_hoctap_nam_2 else ''
            hhjp_0343_2 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_hoctap_nam_2,
                                                              thuctapsinh.ketthuc_hoctap_thang_2)) if thuctapsinh.ketthuc_hoctap_nam_2 else ''
            context['hhjp_0342_2'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0342_2, hhjp_0343_2))
            context['hhjp_0344_2'] = format_hoso.kiemtra(thuctapsinh.truong_tts_2)
            hhjp_0342_v2 = format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_hoctap_nam_2,
                                                             thuctapsinh.batdau_hoctap_thang_2) if thuctapsinh.batdau_hoctap_nam_2 else ''
            hhjp_0343_v2 = format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_hoctap_nam_2,
                                                             thuctapsinh.ketthuc_hoctap_thang_2) if thuctapsinh.ketthuc_hoctap_nam_2 else ''
            context['hhjp_0342_v2'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0342_v2, hhjp_0343_v2))
            context['hhjp_0344_v2'] = format_hoso.kiemtra(thuctapsinh.truong_tts_v_2)

            hhjp_0342_3 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_hoctap_nam_3,
                                                              thuctapsinh.batdau_hoctap_thang_3)) if thuctapsinh.batdau_hoctap_nam_3 else ''
            hhjp_0343_3 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_hoctap_nam_3,
                                                              thuctapsinh.ketthuc_hoctap_thang_3)) if thuctapsinh.ketthuc_hoctap_nam_3 else ''

            context['hhjp_0342_3'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0342_3, hhjp_0343_3))
            context['hhjp_0344_3'] = format_hoso.kiemtra(thuctapsinh.truong_tts_3)
            hhjp_0342_v3 = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_hoctap_nam_3,
                                                                 thuctapsinh.batdau_hoctap_thang_3)) if thuctapsinh.batdau_hoctap_nam_3 else ''
            hhjp_0343_v3 = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_hoctap_nam_3,
                                                                 thuctapsinh.ketthuc_hoctap_thang_3)) if thuctapsinh.ketthuc_hoctap_nam_3 else ''
            context['hhjp_0344_v3'] = format_hoso.kiemtra(thuctapsinh.truong_tts_v_3)

            context['hhjp_0342_v3'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0342_v3, hhjp_0343_v3))

            ### Công tác 1
            hhjp_0347_1 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_congtac_tts_nam_1,
                                                              thuctapsinh.batdau_congtac_tts_thang_1)) if thuctapsinh.batdau_congtac_tts_nam_1 else ''

            if thuctapsinh.den_nay_1 == True:
                hhjp_0348_1 = '現在'
            else:
                hhjp_0348_1 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_congtac_tts_nam_1,
                                                                  thuctapsinh.ketthuc_congtac_tts_thang_1)) if thuctapsinh.ketthuc_congtac_tts_nam_1 else ''

            context['hhjp_0347_1'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_1, hhjp_0348_1))

            hhjp_0349_1 = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_1)
            hhjp_0350_1 = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_1.name_loaicongviec)
            context['hhjp_0349_1'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_1, hhjp_0350_1))

            hhjp_0347_1_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_congtac_tts_nam_1,
                                                                  thuctapsinh.batdau_congtac_tts_thang_1)) if thuctapsinh.batdau_congtac_tts_nam_1 else ''
            if thuctapsinh.den_nay_1 == True:
                hhjp_0348_1_v = 'Hiện nay'
            else:
                hhjp_0348_1_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_congtac_tts_nam_1,
                                                                      thuctapsinh.ketthuc_congtac_tts_thang_1)) if thuctapsinh.ketthuc_congtac_tts_nam_1 else ''
            context['hhjp_0347_1_v'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_1_v, hhjp_0348_1_v))

            hhjp_0349_1_v = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_v_1)
            hhjp_0350_1_v = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_1)
            context['hhjp_0349_1_v'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_1_v, hhjp_0350_1_v))

            ### Công tác 2
            hhjp_0347_2 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_congtac_tts_nam_2,
                                                              thuctapsinh.batdau_congtac_tts_thang_2)) if thuctapsinh.batdau_congtac_tts_nam_2 else ''

            if thuctapsinh.den_nay_2 == True:
                hhjp_0348_2 = '現在'
            else:
                hhjp_0348_2 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_congtac_tts_nam_2,
                                                                  thuctapsinh.ketthuc_congtac_tts_thang_2)) if thuctapsinh.ketthuc_congtac_tts_nam_2 else ''

            context['hhjp_0347_2'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_2, hhjp_0348_2))

            hhjp_0349_2 = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_2)
            hhjp_0350_2 = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_2.name_loaicongviec)
            context['hhjp_0349_2'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_2, hhjp_0350_2))

            hhjp_0347_2_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_congtac_tts_nam_2,
                                                                  thuctapsinh.batdau_congtac_tts_thang_2)) if thuctapsinh.batdau_congtac_tts_nam_2 else ''
            if thuctapsinh.den_nay_2 == True:
                hhjp_0348_2_v = 'Hiện nay'
            else:
                hhjp_0348_2_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_congtac_tts_nam_2,
                                                                      thuctapsinh.ketthuc_congtac_tts_thang_2)) if thuctapsinh.ketthuc_congtac_tts_nam_2 else ''

            context['hhjp_0347_2_v'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_2_v, hhjp_0348_2_v))

            hhjp_0349_2_v = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_v_2)
            hhjp_0350_2_v = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_2)
            context['hhjp_0349_2_v'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_2_v, hhjp_0350_2_v))

            ### Công tác 3
            hhjp_0347_3 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_congtac_tts_nam_3,
                                                              thuctapsinh.batdau_congtac_tts_thang_3)) if thuctapsinh.batdau_congtac_tts_nam_3 else ''

            if thuctapsinh.den_nay_3 == True:
                hhjp_0348_3 = '現在'
            else:
                hhjp_0348_3 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_congtac_tts_nam_3,
                                                                  thuctapsinh.ketthuc_congtac_tts_thang_3)) if thuctapsinh.ketthuc_congtac_tts_nam_3 else ''

            context['hhjp_0347_3'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_3, hhjp_0348_3))

            hhjp_0349_3 = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_3)
            hhjp_0350_3 = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_3.name_loaicongviec)
            context['hhjp_0349_3'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_3, hhjp_0350_3))

            hhjp_0347_3_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_congtac_tts_nam_3,
                                                                  thuctapsinh.batdau_congtac_tts_thang_3)) if thuctapsinh.batdau_congtac_tts_nam_3 else ''

            if thuctapsinh.den_nay_3 == True:
                hhjp_0348_3_v = 'Hiện nay'
            else:
                hhjp_0348_3_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_congtac_tts_nam_3,
                                                                      thuctapsinh.ketthuc_congtac_tts_thang_3)) if thuctapsinh.ketthuc_congtac_tts_nam_3 else ''

            context['hhjp_0347_3_v'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_3_v, hhjp_0348_3_v))

            hhjp_0349_3_v = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_v_3)
            hhjp_0350_3_v = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_3)
            context['hhjp_0349_3_v'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_3_v, hhjp_0350_3_v))

            ### Công tác 4
            hhjp_0347_4 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_congtac_tts_nam_4,
                                                              thuctapsinh.batdau_congtac_tts_thang_4)) if thuctapsinh.batdau_congtac_tts_nam_4 else ''

            if thuctapsinh.den_nay_4 == True:
                hhjp_0348_4 = '現在'
            else:
                hhjp_0348_4 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_congtac_tts_nam_4,
                                                                  thuctapsinh.ketthuc_congtac_tts_thang_4)) if thuctapsinh.ketthuc_congtac_tts_nam_4 else ''

            context['hhjp_0347_4'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_4, hhjp_0348_4))

            hhjp_0349_4 = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_4)
            hhjp_0350_4 = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_4.name_loaicongviec)
            context['hhjp_0349_4'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_4, hhjp_0350_4))
            hhjp_0347_4_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_congtac_tts_nam_4,
                                                                  thuctapsinh.batdau_congtac_tts_thang_4)) if thuctapsinh.batdau_congtac_tts_nam_4 else ''
            if thuctapsinh.den_nay_4 == True:
                hhjp_0348_4_v = 'Hiện nay'
            else:
                hhjp_0348_4_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_congtac_tts_nam_4,
                                                                      thuctapsinh.ketthuc_congtac_tts_thang_4)) if thuctapsinh.ketthuc_congtac_tts_nam_4 else ''
            context['hhjp_0347_4_v'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_4_v, hhjp_0348_4_v))

            hhjp_0349_4_v = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_v_4)
            hhjp_0350_4_v = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_4)
            context['hhjp_0349_4_v'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_4_v, hhjp_0350_4_v))

            ### Công tác 5
            hhjp_0347_5 = str(format_hoso.convert_date_hocvan(thuctapsinh.batdau_congtac_tts_nam_5,
                                                              thuctapsinh.batdau_congtac_tts_thang_5)) if thuctapsinh.batdau_congtac_tts_nam_5 else ''

            if thuctapsinh.den_nay_5 == True:
                hhjp_0348_5 = '現在'
            else:
                hhjp_0348_5 = str(format_hoso.convert_date_hocvan(thuctapsinh.ketthuc_congtac_tts_nam_5,
                                                                  thuctapsinh.ketthuc_congtac_tts_thang_5)) if thuctapsinh.ketthuc_congtac_tts_nam_5 else ''
            context['hhjp_0347_5'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_5, hhjp_0348_5))

            hhjp_0349_5 = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_5)
            hhjp_0350_5 = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_5.name_loaicongviec)
            context['hhjp_0349_5'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_5, hhjp_0350_5))

            hhjp_0347_5_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.batdau_congtac_tts_nam_5,
                                                                  thuctapsinh.batdau_congtac_tts_thang_5)) if thuctapsinh.batdau_congtac_tts_nam_5 else ''
            if thuctapsinh.den_nay_5 == True:
                hhjp_0348_5_v = 'Hiện nay'
            else:
                hhjp_0348_5_v = str(format_hoso.convert_date_hocvan_v(thuctapsinh.ketthuc_congtac_tts_nam_5,
                                                                      thuctapsinh.ketthuc_congtac_tts_thang_5)) if thuctapsinh.ketthuc_congtac_tts_nam_5 else ''

            context['hhjp_0347_5_v'] = format_hoso.kiemtra(format_hoso.convert_hocvan(hhjp_0347_5_v, hhjp_0348_5_v))

            hhjp_0349_5_v = format_hoso.kiemtra(thuctapsinh.donvi_congtac_tts_v_5)
            hhjp_0350_5_v = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_5)
            context['hhjp_0349_5_v'] = format_hoso.kiemtra(format_hoso.convert_congtac(hhjp_0349_5_v, hhjp_0350_5_v))

            if thuctapsinh.congviec_lienquan_1:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_1.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_1)
                context['ahh_0352'] = format_hoso.round_nam(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.round_nam(thuctapsinh.fill)
            elif thuctapsinh.congviec_lienquan_2:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_2.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_2)
                context['ahh_0352'] = format_hoso.round_nam(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.round_nam(thuctapsinh.fill)
            elif thuctapsinh.congviec_lienquan_3:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_3.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_3)
                context['ahh_0352'] = format_hoso.round_nam(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.round_nam(thuctapsinh.fill)
            elif thuctapsinh.congviec_lienquan_4:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_4.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_4)
                context['ahh_0352'] = format_hoso.round_nam(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.round_nam(thuctapsinh.fill)
            elif thuctapsinh.congviec_lienquan_5:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_5.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_5)
                context['ahh_0352'] = format_hoso.round_nam(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.round_nam(thuctapsinh.fill)
            elif thuctapsinh.congviec_lienquan_6:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_6.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_6)
                context['ahh_0352'] = format_hoso.round_nam(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.round_nam(thuctapsinh.fill)
            elif thuctapsinh.congviec_lienquan_7:
                context['ahh_0351'] = format_hoso.nganhnghe(thuctapsinh.congviec_congtac_tts_7.name_loaicongviec)
                context['a0351_v'] = format_hoso.kiemtra(thuctapsinh.congviec_congtac_tts_v_7)
                context['ahh_0352'] = format_hoso.round_nam(thuctapsinh.fill)
                context['a0352_v'] = format_hoso.round_nam(thuctapsinh.fill)

            if thuctapsinh.ngonngu_nhat_tts == False:
                docdata.remove_shape(u'id="6" name="Oval 6"')
            context['h_0355'] = format_hoso.kiemtra(thuctapsinh.ngonngu_nhat_tts)
            context['h_0355_v'] = format_hoso.kiemtra(thuctapsinh.ngonngu_nhat_tts_v)

            if thuctapsinh.ngonngu_anh_tts == False:
                docdata.remove_shape(u'id="7" name="Oval 7"')
            context['h_0357'] = format_hoso.kiemtra(thuctapsinh.ngonngu_anh_tts)
            context['h_0357_v'] = format_hoso.kiemtra(thuctapsinh.ngonngu_anh_tts_v)
            if thuctapsinh.ngonngu_khac_tts == False:
                docdata.remove_shape(u'id="2" name="Oval 2"')

            context['h_0358'] = format_hoso.kiemtra(thuctapsinh.ngonngu_khac_tts)
            context['h_0358_v'] = format_hoso.kiemtra(thuctapsinh.ngonngu_khac_tts_v)
            context['h_0359'] = format_hoso.kiemtra(thuctapsinh.trinhdo_khac_tts)
            context['h_0359_v'] = format_hoso.kiemtra(thuctapsinh.trinhdo_khac_tts_v)

            if thuctapsinh.lichsunhapcu_tts == 'Có':
                docdata.remove_shape(u'id="9" name="Oval 9"')
                context['hhjp_0361'] = thuctapsinh.ngaydinhat_tts if thuctapsinh.ngaydinhat_tts else ''
                context['hhjp_0362'] = thuctapsinh.onhatden_tts if thuctapsinh.onhatden_tts else ''

                if (thuctapsinh.batdau_kynang_tts and thuctapsinh.ketthuc_kynang_tts):
                    context['bs_0052'] = str(thuctapsinh.batdau_kynang_tts.year) + '年' + \
                                         str(thuctapsinh.batdau_kynang_tts.month) + '月' + \
                                         str(thuctapsinh.batdau_kynang_tts.day) + '日'
                    context['bs_0052_v'] = 'Ngày ' + str(thuctapsinh.batdau_kynang_tts.day) + \
                                           ' tháng ' + str(thuctapsinh.batdau_kynang_tts.month) + \
                                           ' năm ' + str(thuctapsinh.batdau_kynang_tts.year)
                    context['bs_0053'] = str(thuctapsinh.ketthuc_kynang_tts.year) + '年' + \
                                         str(thuctapsinh.ketthuc_kynang_tts.month) + '月' + \
                                         str(thuctapsinh.ketthuc_kynang_tts.day) + '日'
                    context['bs_0053_v'] = 'Ngày ' + str(thuctapsinh.ketthuc_kynang_tts.day) + \
                                           ' tháng ' + str(thuctapsinh.ketthuc_kynang_tts.month) + \
                                           ' năm ' + str(thuctapsinh.ketthuc_kynang_tts.year)
                    context['bs_0052_1'] = '■'
                else:
                    context['bs_0052'] = '年　月　日'
                    context['bs_0052_v'] = 'Ngày tháng năm'
                    context['bs_0053'] = '年　月　日'
                    context['bs_0053_v'] = 'Ngày tháng năm'
                    context['bs_0052_1'] = '□'

                if (thuctapsinh.batdau_xaydung_tts and thuctapsinh.ketthuc_xaydung_tts):
                    context['bs_0054'] = str(thuctapsinh.batdau_xaydung_tts.year) + '年' + \
                                         str(thuctapsinh.batdau_xaydung_tts.month) + '月' + \
                                         str(thuctapsinh.batdau_xaydung_tts.day) + '日'
                    context['bs_0054_v'] = 'Ngày ' + str(thuctapsinh.batdau_xaydung_tts.day) + \
                                           ' tháng ' + str(thuctapsinh.batdau_xaydung_tts.month) + \
                                           ' năm ' + str(thuctapsinh.batdau_xaydung_tts.year)
                    context['bs_0055'] = str(thuctapsinh.ketthuc_xaydung_tts.year) + '年' + \
                                         str(thuctapsinh.ketthuc_xaydung_tts.month) + '月' + \
                                         str(thuctapsinh.ketthuc_xaydung_tts.day) + '日'
                    context['bs_0055_v'] = 'Ngày ' + str(thuctapsinh.ketthuc_xaydung_tts.day) + \
                                           ' tháng ' + str(thuctapsinh.ketthuc_xaydung_tts.month) + \
                                           ' năm ' + str(thuctapsinh.ketthuc_xaydung_tts.year)
                    context['bs_0052_1'] = '■'

                else:
                    context['bs_0054'] = '年　月　日'
                    context['bs_0054_v'] = 'Ngày tháng năm'
                    context['bs_0055'] = '年　月　日'
                    context['bs_0055_v'] = 'Ngày tháng năm'
                    if (thuctapsinh.batdau_kynang_tts and thuctapsinh.ketthuc_kynang_tts):
                        context['bs_0052_1'] = '■'
                    else:
                        context['bs_0052_1'] = '□'

                if (thuctapsinh.batdau_ungvien_tts and thuctapsinh.ketthuc_ungvien_tts):
                    context['bs_0056'] = str(thuctapsinh.batdau_ungvien_tts.year) + '年' + \
                                         str(thuctapsinh.batdau_ungvien_tts.month) + '月' + \
                                         str(thuctapsinh.batdau_ungvien_tts.day) + '日'
                    context['bs_0056_v'] = 'Ngày ' + str(thuctapsinh.batdau_ungvien_tts.day) + \
                                           ' tháng ' + str(thuctapsinh.batdau_ungvien_tts.month) + \
                                           ' năm ' + str(thuctapsinh.batdau_ungvien_tts.year)
                    context['bs_0057'] = str(thuctapsinh.ketthuc_ungvien_tts.year) + '年' + \
                                         str(thuctapsinh.ketthuc_ungvien_tts.month) + '月' + \
                                         str(thuctapsinh.ketthuc_ungvien_tts.day) + '日'
                    context['bs_0057_v'] = 'Ngày ' + str(thuctapsinh.ketthuc_ungvien_tts.day) + \
                                           ' tháng ' + str(thuctapsinh.ketthuc_ungvien_tts.month) + \
                                           ' năm ' + str(thuctapsinh.ketthuc_ungvien_tts.year)
                    context['bs_0056_1'] = '■'
                else:
                    context['bs_0056'] = '年　月　日'
                    context['bs_0056_v'] = 'Ngày tháng năm'
                    context['bs_0057'] = '年　月　日'
                    context['bs_0057_v'] = 'Ngày tháng năm'
                    context['bs_0056_1'] = '□'
            else:
                docdata.remove_shape(u'id="8" name="Oval 8"')
                context['hhjp_0361'] = ''
                context['hhjp_0362'] = ''
                context['bs_0052'] = '年　月　日'
                context['bs_0052_v'] = 'Ngày tháng năm'
                context['bs_0053'] = '年　月　日'
                context['bs_0053_v'] = 'Ngày tháng năm'
                context['bs_0054'] = '年　月　日'
                context['bs_0054_v'] = 'Ngày tháng năm'
                context['bs_0055'] = '年　月　日'
                context['bs_0055_v'] = 'Ngày tháng năm'
                context['bs_0056'] = '年　月　日'
                context['bs_0056_v'] = 'Ngày tháng năm'
                context['bs_0057'] = '年　月　日'
                context['bs_0057_v'] = 'Ngày tháng năm'
                context['bs_0056_1'] = '□'
                context['bs_0052_1'] = '□'

            if thuctapsinh.kinhnghiem_tts == 'Có':
                docdata.remove_shape(u'id="12" name="Oval 12"')
            elif thuctapsinh.kinhnghiem_tts == 'Không':
                docdata.remove_shape(u'id="1" name="Oval 1"')
            else:
                docdata.remove_shape(u'id="1" name="Oval 1"')

            if thuctapsinh.phanloai_kehoach_thuctap_tts_1 == True:
                context['hhjp_0113_A'] = '■'
            else:
                context['hhjp_0113_A'] = '□'

            if thuctapsinh.phanloai_kehoach_thuctap_tts_2 == True:
                context['hhjp_0113_B'] = '■'
            else:
                context['hhjp_0113_B'] = '□'
            if thuctapsinh.phanloai_kehoach_thuctap_tts_3 == True:
                context['hhjp_0113_C'] = '■'
            else:
                context['hhjp_0113_C'] = '□'
            if thuctapsinh.phanloai_kehoach_thuctap_tts_4 == True:
                context['hhjp_0113_D'] = '■'
            else:
                context['hhjp_0113_D'] = '□'
            if thuctapsinh.phanloai_kehoach_thuctap_tts_5 == True:
                context['hhjp_0113_E'] = '■'
            else:
                context['hhjp_0113_E'] = '□'
            if thuctapsinh.phanloai_kehoach_thuctap_tts_6 == True:
                context['hhjp_0113_F'] = '■'
            else:
                context['hhjp_0113_F'] = '□'

            context['hhjp_0364'] = format_hoso.convert_date(thuctapsinh.knttkn_tu_tts)
            context['hhjp_0365'] = format_hoso.convert_date(thuctapsinh.knttkn_den_tts)

            if thuctapsinh.buocloaibo_tts == u'Có':
                docdata.remove_shape(u'id="14" name="Oval 14"')
                context['bs_0009'] = format_hoso.kiemtra(thuctapsinh.lydo_buocloaibo_tts)
                context['bs_0009_v'] = format_hoso.kiemtra(thuctapsinh.lydo_buocloaibo_tts_v)
            else:
                docdata.remove_shape(u'id="13" name="Oval 13"')
                context['bs_0009'] = ''
                context['bs_0009_v'] = ''

            context['hhjp_0373'] = format_hoso.kiemtra(thuctapsinh.thongtinkhac_tts)
            context['hhjp_0373_v'] = format_hoso.kiemtra(thuctapsinh.thongtinkhac_tts_v)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_51(self, ma_thuctapsinh, document):
        print('51')
        sothuctapsinh = int(len(document.thuctapsinh_hoso))
        if sothuctapsinh <= 3:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_51")], limit=1)
            print('file_51')
        elif sothuctapsinh > 3:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_51_1")], limit=1)
            print('file_51_1')
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)

            context = {}
            if sothuctapsinh <= 3:
                item = 0
                for thuctapsinh_hoso in document.thuctapsinh_hoso:
                    if item == 0:
                        context['hhjp_0001_1'] = format_hoso.kiemtra(thuctapsinh_hoso.ten_lt_tts)
                        context['hhjp_0003_1'] = format_hoso.kiemtra(thuctapsinh_hoso.quoctich_tts.name_quoctich)
                        item += 1
                    elif item == 1:
                        context['hhjp_0001_2'] = format_hoso.kiemtra(thuctapsinh_hoso.ten_lt_tts)
                        context['hhjp_0003_2'] = format_hoso.kiemtra(thuctapsinh_hoso.quoctich_tts.name_quoctich)
                        item += 1
                    elif item == 2:
                        context['hhjp_0001_3'] = format_hoso.kiemtra(thuctapsinh_hoso.ten_lt_tts)
                        context['hhjp_0003_3'] = format_hoso.kiemtra(thuctapsinh_hoso.quoctich_tts.name_quoctich)
                        break

                context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                context['hhjp_0097'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_han_nghiepdoan)
                if document.ngay_lapvanban1:
                    context['hhjp_0193'] = intern_utils.date_time_in_jp(document.ngay_lapvanban1.day,
                                                                        document.ngay_lapvanban1.month,
                                                                        document.ngay_lapvanban1.year)
                else:
                    context['hhjp_0193'] = '年　月　日'
                context['hhjp_0924'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_anh_phaicu)
                context['hhjp_0509_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.congtyphaicu_donhang.chucvu_han_phaicu)
                context['hhjp_0509'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_nguoidaidien)
            elif sothuctapsinh > 3:
                context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
                context['hhjp_0097'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_han_nghiepdoan)
                if document.ngay_lapvanban1:
                    context['hhjp_0193'] = intern_utils.date_time_in_jp(document.ngay_lapvanban1.day,
                                                                        document.ngay_lapvanban1.month,
                                                                        document.ngay_lapvanban1.year)
                else:
                    context['hhjp_0193'] = '年　月　日'
                context['hhjp_0924'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_anh_phaicu)
                context['hhjp_0509_1'] = format_hoso.kiemtra(
                    document.donhang_hoso.congtyphaicu_donhang.chucvu_han_phaicu)
                context['hhjp_0509'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_nguoidaidien)
                table_congtyphaicu = []
                for thuctapsinh_hoso in document.thuctapsinh_hoso:
                    infor_congtyphaicu = {}
                    infor_congtyphaicu['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh_hoso.ten_lt_tts)
                    infor_congtyphaicu['hhjp_0003'] = format_hoso.kiemtra(thuctapsinh_hoso.quoctich_tts.name_quoctich)

                    table_congtyphaicu.append(infor_congtyphaicu)

                context['tbl_thuctapsinh'] = table_congtyphaicu

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_52(self, ma_thuctapsinh, document):
        print('52')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_52")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            context = {}

            context['hhjp_0585'] = Listing(format_hoso.kiemtra(
                thuctapsinh.donhang_tts.daotaotruoc_donhang.tencoso_diachi_ngoai_1.ten_coquan_h))
            context['hhjp_0586'] = format_hoso.kiemtra(
                thuctapsinh.donhang_tts.daotaotruoc_donhang.tencoso_diachi_ngoai_1.nguoi_daidien)
            context['hhjp_0587'] = format_hoso.kiemtra(
                thuctapsinh.donhang_tts.daotaotruoc_donhang.tencoso_diachi_ngoai_1.diachi_coquan)
            context['hhjp_0587_1'] = format_hoso.kiemtra(
                thuctapsinh.donhang_tts.daotaotruoc_donhang.tencoso_diachi_ngoai_1.sdt_coquan).strip()
            context['hhjp_0587_2'] = format_hoso.kiemtra(
                thuctapsinh.donhang_tts.daotaotruoc_donhang.tencoso_diachi_ngoai_1.email)
            context['hhjp_0588'] = format_hoso.convert_date(
                thuctapsinh.donhang_tts.daotaotruoc_donhang.tencoso_diachi_ngoai_1.ngay_thanhlap)

            context[
                'hhjp_0589_1'] = '■' if thuctapsinh.donhang_tts.daotaotruoc_donhang.tencoso_diachi_ngoai_1.daotaotruoc else '□'
            context[
                'hhjp_0589_2'] = '■' if thuctapsinh.donhang_tts.daotaotruoc_donhang.tencoso_diachi_ngoai_1.khac_coquan else '□'
            context['hhjp_0589_3'] = format_hoso.kiemtra(
                thuctapsinh.donhang_tts.daotaotruoc_donhang.tencoso_diachi_ngoai_1.noidung_coquan)
            context['hhjp_0590'] = format_hoso.kiemtra(
                thuctapsinh.donhang_tts.daotaotruoc_donhang.tencoso_diachi_ngoai_1.loaicongviec_chinh)

            context['hhjp_0591'] = format_hoso.kiemtra(
                thuctapsinh.donhang_tts.daotaotruoc_donhang.tencoso_diachi_ngoai_1.tienvon)
            context['hhjp_0591_n'] = format_hoso.kiemtra(
                thuctapsinh.donhang_tts.daotaotruoc_donhang.tencoso_diachi_ngoai_1.tienvon_n)
            context['hhjp_0592'] = format_hoso.kiemtra(
                thuctapsinh.donhang_tts.daotaotruoc_donhang.tencoso_diachi_ngoai_1.doanhso)
            context['hhjp_0592_n'] = format_hoso.kiemtra(
                thuctapsinh.donhang_tts.daotaotruoc_donhang.tencoso_diachi_ngoai_1.doanhso_n)
            context['hhjp_0593'] = format_hoso.kiemtra(
                thuctapsinh.donhang_tts.daotaotruoc_donhang.tencoso_diachi_ngoai_1.sonhanvien)
            context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban1)
            context['hhjp_0595'] = '%s  %s' % (format_hoso.kiemtra(
                thuctapsinh.donhang_tts.daotaotruoc_donhang.tencoso_diachi_ngoai_1.chucvu_nguoilapvanban),
                                               format_hoso.kiemtra(
                                                   thuctapsinh.donhang_tts.daotaotruoc_donhang.tencoso_diachi_ngoai_1.ten_nguoilapvanban))

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_53(self, ma_thuctapsinh, document):
        print('53')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_53")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            daotaotruoc = self.env['daotao.daotaotruoc'].search(
                [('donhang_daotaotruoc', '=', document.donhang_hoso.id)], limit=1)
            context['hhjp_0605'] = format_hoso.convert_date(daotaotruoc.thoigian_batdau_daotaotruoc)
            context['hhjp_0606'] = format_hoso.convert_date(daotaotruoc.thoigian_ketthuc_daotaotruoc)
            table_lich = []
            for laplich_daotaotruoc in daotaotruoc.laplich_daotaotruoc:
                infor_lich = {}
                infor_lich['dttnc_0001'] = format_hoso.convert_date(laplich_daotaotruoc.ngay_laplichtruoc)
                if laplich_daotaotruoc.noidung_laplichtruoc.noidungdaotao == "休日":
                    dttnc_0002 = ''
                else:
                    dttnc_0002 = '%s ～ %s' % (format_hoso.convert_hour(daotaotruoc.batdau_sang),
                                              format_hoso.convert_hour(daotaotruoc.ketthuc_sang))
                infor_lich['dttnc_0002'] = dttnc_0002
                infor_lich['hhjp_0607'] = format_hoso.kiemtra(laplich_daotaotruoc.noidung_laplichtruoc.noidungdaotao)
                infor_lich['dttnc_0003'] = Listing(
                    '%s \n %s' % (format_hoso.kiemtra(laplich_daotaotruoc.congviec_giangvien_laplichtruoc),
                                  format_hoso.kiemtra(
                                      laplich_daotaotruoc.giangvien_laplichtruoc.ten_giaovien)))
                infor_lich['dttnc_0004'] = Listing(
                    format_hoso.kiemtra(laplich_daotaotruoc.coso_laplichtruoc.ten_coso_jp))
                infor_lich['dttnc_0005'] = format_hoso.kiemtra(laplich_daotaotruoc.ghichu_laplichtruoc)
                table_lich.append(infor_lich)
            context['tbl_lich'] = table_lich

            context['dttnc_0005'] = format_hoso.convert_date(daotaotruoc.ngay_lamdon)
            context['hhjp_0097'] = format_hoso.kiemtra(
                daotaotruoc.donhang_daotaotruoc.nghiepdoan_donhang.ten_han_nghiepdoan)
            context['hhjp_0103'] = format_hoso.kiemtra(
                daotaotruoc.donhang_daotaotruoc.nghiepdoan_donhang.ten_daidien_han_nghiepdoan)
            table_tts = []
            for thuctapsinh_hoso in document.thuctapsinh_hoso:
                infor_tts = {}
                infor_tts['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh_hoso.ten_lt_tts)
                infor_tts['hhjp_0902'] = format_hoso.convert_date(thuctapsinh_hoso.ngaynhapcanh_tts)
                infor_tts['hhjp_0599'] = format_hoso.kiemtra(thuctapsinh_hoso.thongtinkhac_tts)
                table_tts.append(infor_tts)
            context['tbl_tts'] = table_tts
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_54(self, ma_thuctapsinh, document):
        print('54')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_54")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            context = {}
            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            context['hhjp_0010_v'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_viet_xnghiep)
            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            context['hhjp_0001_v'] = format_hoso.kiemtra(thuctapsinh.ten_tv_tts)
            context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban1)
            if document.ngay_lapvanban1:
                context['hhjp_0193_v'] = ' Ngày ' + str(document.ngay_lapvanban1.day) + \
                                         ' tháng ' + str(document.ngay_lapvanban1.month) + \
                                         ' năm ' + str(document.ngay_lapvanban1.year)
            else:
                context['hhjp_0193_v'] = 'Ngày tháng năm'
            context['hhjp_0014'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep)
            context['hhjp_0014_v'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep_v)
            context['xn_0104'] = format_hoso.kiemtra(document.xinghiep_hoso.chucvu_daidien_han)
            context['xn_0104_v'] = format_hoso.kiemtra(document.xinghiep_hoso.chucvu_daidien_viet)
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_55(self, ma_thuctapsinh, document):
        print('55')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_55")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            hopdonglaodong = self.env['hopdong.hopdong'].search(
                [('thuctapsinh_hopdong', '=', thuctapsinh.id), ('donhang_hopdong', '=', document.donhang_hoso.id),
                 ('giaidoan_hopdong', '=', document.giaidoan_hoso)],
                limit=1)
            context = {}

            if document.ngay_lapvanban1 != False:
                context['hhjp_0193'] = str(document.ngay_lapvanban1.year) + '年' + \
                                       str(document.ngay_lapvanban1.month) + '月' + \
                                       str(document.ngay_lapvanban1.day) + '日'
                context['hhjp_0193_v'] = 'Ngày ' + str(document.ngay_lapvanban1.day) + \
                                         ' tháng ' + str(document.ngay_lapvanban1.month) + \
                                         ' năm ' + str(document.ngay_lapvanban1.year)
            else:
                context['hhjp_0193'] = '年　月　日'
                context['hhjp_0193_v'] = 'Ngày tháng năm'

            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            context['hhjp_0010_v'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_viet_xnghiep)
            context['hhjp_0011'] = format_hoso.kiemtra(document.xinghiep_hoso.diachi_han_xnghiep)
            context['hhjp_0011_v'] = format_hoso.kiemtra(document.xinghiep_hoso.diachi_viet_xnghiep)
            context['hhjp_0012'] = format_hoso.kiemtra(document.xinghiep_hoso.sdt_xnghiep).strip()
            context['xn_0104'] = format_hoso.kiemtra(document.xinghiep_hoso.chucvu_daidien_han)
            context['xn_0104_v'] = format_hoso.kiemtra(document.xinghiep_hoso.chucvu_daidien_viet)
            context['hhjp_0014'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep)
            context['hhjp_0014_v'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep_v)

            if hopdonglaodong.batdau_tgian_congviec:
                context['hhjp_0148'] = str(hopdonglaodong.batdau_tgian_congviec.year) + ' 年 ' + \
                                       str(hopdonglaodong.batdau_tgian_congviec.month) + ' 月 ' + \
                                       str(hopdonglaodong.batdau_tgian_congviec.day) + ' 日'

                context['hhjp_0148_v'] = 'Ngày ' + str(hopdonglaodong.batdau_tgian_congviec.day) + \
                                         ' tháng ' + str(hopdonglaodong.batdau_tgian_congviec.month) + \
                                         ' năm ' + str(hopdonglaodong.batdau_tgian_congviec.year)
            else:
                context['hhjp_0148'] = '年　月　日'
                context['hhjp_0148_v'] = 'Ngày tháng năm'

            if hopdonglaodong.kethuc_tgian_congviec:
                context['hhjp_0380'] = str(hopdonglaodong.kethuc_tgian_congviec.year) + '年' + \
                                       str(hopdonglaodong.kethuc_tgian_congviec.month) + '月' + \
                                       str(hopdonglaodong.kethuc_tgian_congviec.day) + '日'

                context['hhjp_0380_v'] = 'Ngày ' + str(hopdonglaodong.kethuc_tgian_congviec.day) + \
                                         ' tháng ' + str(hopdonglaodong.kethuc_tgian_congviec.month) + \
                                         ' năm ' + str(hopdonglaodong.kethuc_tgian_congviec.year)
            else:
                context['hhjp_0380'] = '年　月　日'
                context['hhjp_0380_v'] = 'Ngày tháng năm'

            if hopdonglaodong.dukien_nhapcanh != False:
                context['hhjp_0902'] = str(hopdonglaodong.dukien_nhapcanh.year) + '年' + \
                                       str(hopdonglaodong.dukien_nhapcanh.month) + '月' + \
                                       str(hopdonglaodong.dukien_nhapcanh.day) + '日'
                context['hhjp_0902_v'] = 'Ngày ' + str(hopdonglaodong.dukien_nhapcanh.day) + \
                                         ' tháng ' + str(hopdonglaodong.dukien_nhapcanh.month) + \
                                         ' năm ' + str(hopdonglaodong.dukien_nhapcanh.year)
            else:
                context['hhjp_0902'] = '年　月　日'
                context['hhjp_0902_v'] = 'Ngày tháng năm'

            if hopdonglaodong.giahan_hopdong == u'Có':
                context['hhjp_0381_1'] = '□'
                context['hhjp_0381_2'] = '■'
            elif hopdonglaodong.giahan_hopdong == u'Không':
                context['hhjp_0381_1'] = '■'
                context['hhjp_0381_2'] = '□'
            else:
                context['hhjp_0381_1'] = '□'
                context['hhjp_0381_2'] = '□'

            table_noilam = []
            for ten_noilam_han in hopdonglaodong.ten_noilam_han:
                info = {}
                info['hhjp_0041'] = format_hoso.kiemtra(ten_noilam_han.ten_chinhanh_han)
                info['hhjp_0041_v'] = format_hoso.kiemtra(ten_noilam_han.ten_chinhanh_viet)

                info['hhjp_0042'] = format_hoso.kiemtra(ten_noilam_han.diachi_chinhanh)
                info['hhjp_0042_v'] = format_hoso.kiemtra(ten_noilam_han.diachi_chinhanh_viet)

                table_noilam.append(info)
            context['tbl_noilamviec'] = table_noilam

            context['hhjp_0115'] = format_hoso.nganhnghe(hopdonglaodong.loai_nghe_nganhnghe_tts.name_loaicongviec)
            context['hhjp_0116'] = format_hoso.nganhnghe(hopdonglaodong.congviec_nganhnghe_tts.name_congviec)
            context['hhjp_0115_v'] = format_hoso.kiemtra(hopdonglaodong.loai_nghe_nganhnghe_tts_viet)
            context['hhjp_0116_v'] = format_hoso.kiemtra(hopdonglaodong.congviec_nganhnghe_tts_viet)

            context['hhjp_0903'] = format_hoso.convert(hopdonglaodong.batdau_codinh_sang_hopdong)
            context['hhjp_0382'] = format_hoso.convert(hopdonglaodong.kethuc_codinh_chieu_hopdong)
            context['hhjp_0383'] = format_hoso.convert(hopdonglaodong.laodong_gio_tts)
            context['hhjp_0903_v'] = format_hoso.convert_viet(hopdonglaodong.batdau_codinh_sang_hopdong)
            context['hhjp_0382_v'] = format_hoso.convert_viet(hopdonglaodong.kethuc_codinh_chieu_hopdong)
            context['hhjp_0383_v'] = format_hoso.convert_viet(hopdonglaodong.laodong_gio_tts)

            context['hhjp_0384'] = format_hoso.kiemtra(hopdonglaodong.donvithaydoi_tts)
            context['hhjp_0384_1'] = '■' if hopdonglaodong.donvithaydoi_tts else '□'
            context['hhjp_0384_v'] = format_hoso.kiemtra(hopdonglaodong.donvithaydoi_tts_viet)

            if hopdonglaodong.batdau_tgian_viecca_mot or hopdonglaodong.ketthuc_tgian_viecca_mot:
                context['hhjp_0384_2'] = '■'
            else:
                context['hhjp_0384_2'] = '□'

            # Viec_Ca_Mot
            context['hhjp_0386_1'] = format_hoso.convert(hopdonglaodong.batdau_tgian_viecca_mot)
            context['hhjp_0387_1'] = format_hoso.convert(hopdonglaodong.ketthuc_tgian_viecca_mot)
            context['hhjp_0386_1_ngay'] = format_hoso.kiemtra(hopdonglaodong.apdung_tgian_viecca_mot)
            context['hhjp_0387_1_gio'] = format_hoso.convert(hopdonglaodong.sogio_laodong_motngay_mot)

            context['hhjp_0386_1_v'] = format_hoso.convert_viet(hopdonglaodong.batdau_tgian_viecca_mot)
            context['hhjp_0387_1_v'] = format_hoso.convert_viet(hopdonglaodong.ketthuc_tgian_viecca_mot)
            context['hhjp_0386_1_ngay_v'] = format_hoso.kiemtra(hopdonglaodong.apdung_tgian_viecca_mot)
            context['hhjp_0387_1_gio_v'] = format_hoso.convert_viet(hopdonglaodong.sogio_laodong_motngay_mot)

            # Viec_Ca_Hai
            context['hhjp_0386_2'] = format_hoso.convert(hopdonglaodong.batdau_tgian_viecca_hai)
            context['hhjp_0387_2'] = format_hoso.convert(hopdonglaodong.ketthuc_tgian_viecca_hai)
            context['hhjp_0386_2_ngay'] = format_hoso.kiemtra(hopdonglaodong.apdung_tgian_viecca_hai)
            context['hhjp_0387_2_gio'] = format_hoso.convert(hopdonglaodong.sogio_laodong_motngay_hai)

            context['hhjp_0386_2_v'] = format_hoso.convert_viet(hopdonglaodong.batdau_tgian_viecca_hai)
            context['hhjp_0387_2_v'] = format_hoso.convert_viet(hopdonglaodong.ketthuc_tgian_viecca_hai)
            context['hhjp_0386_2_ngay_v'] = format_hoso.kiemtra(hopdonglaodong.apdung_tgian_viecca_hai)
            context['hhjp_0387_2_gio_v'] = format_hoso.convert_viet(hopdonglaodong.sogio_laodong_motngay_hai)

            # Viec_Ca_Ba
            context['hhjp_0386_3'] = format_hoso.convert(hopdonglaodong.batdau_tgian_viecca_ba)
            context['hhjp_0387_3'] = format_hoso.convert(hopdonglaodong.ketthuc_tgian_viecca_ba)
            context['hhjp_0386_3_ngay'] = format_hoso.kiemtra(hopdonglaodong.apdung_tgian_viecca_ba)
            context['hhjp_0387_3_gio'] = format_hoso.convert(hopdonglaodong.sogio_laodong_motngay_ba)

            context['hhjp_0386_3_v'] = format_hoso.convert_viet(hopdonglaodong.batdau_tgian_viecca_ba)
            context['hhjp_0387_3_v'] = format_hoso.convert_viet(hopdonglaodong.ketthuc_tgian_viecca_ba)
            context['hhjp_0386_3_ngay_v'] = format_hoso.kiemtra(hopdonglaodong.apdung_tgian_viecca_ba)
            context['hhjp_0387_3_gio_v'] = format_hoso.convert_viet(hopdonglaodong.sogio_laodong_motngay_ba)

            context['hhjp_0151'] = format_hoso.kiemtra(hopdonglaodong.thoigian_giailao)
            context['hhjp_0151_v'] = format_hoso.kiemtra(hopdonglaodong.thoigian_giailao)

            context['hhjp_0388_h'] = format_hoso.kiemtra(hopdonglaodong.giolaodong_motthang)
            context['hhjp_0388_p'] = format_hoso.kiemtra(hopdonglaodong.phutlaodong_motthang)
            context['hhjp_0388_v'] = '%s Giờ %s Phút' % (format_hoso.kiemtra(hopdonglaodong.giolaodong_motthang),
                                                         format_hoso.kiemtra(hopdonglaodong.phutlaodong_motthang))
            context['hhjp_0152'] = format_hoso.kiemtra(hopdonglaodong.giolaodong_motnam)
            context['hhjp_0152_v'] = format_hoso.kiemtra(hopdonglaodong.giolaodong_motnam)
            context['hhjp_0389'] = format_hoso.kiemtra(hopdonglaodong.namnhat_ngaylaodong)
            context['hhjp_0390'] = format_hoso.kiemtra(hopdonglaodong.namhai_ngaylaodong)
            context['hhjp_0391'] = format_hoso.kiemtra(hopdonglaodong.namba_ngaylaodong)

            context['hhjp_0392_1'] = '■' if hopdonglaodong.loadong_ngoaigioi == u'Có' else '□'
            context['hhjp_0392_0'] = '■' if hopdonglaodong.loadong_ngoaigioi == u'Không' else '□'

            noiquy = []
            noiquy_v = []
            if hopdonglaodong.tudieu_mot_hopdong and hopdonglaodong.dendieu_mot_hopdong:
                dieu1 = '第 ' + str(hopdonglaodong.tudieu_mot_hopdong) + ' 条～第 ' + str(
                    hopdonglaodong.dendieu_mot_hopdong) + ' 条 ,'
                dieu1_v = 'Từ điều ' + str(hopdonglaodong.tudieu_mot_hopdong) + ' đến Điều ' + str(
                    hopdonglaodong.dendieu_mot_hopdong)
                noiquy.append(dieu1)
                noiquy_v.append(dieu1_v)

            if hopdonglaodong.tudieu_hai_hopdong and hopdonglaodong.dendieu_hai_hopdong:
                dieu2 = '第 ' + str(hopdonglaodong.tudieu_hai_hopdong) + ' 条～第 ' + str(
                    hopdonglaodong.dendieu_hai_hopdong) + ' 条 ,'
                dieu2_v = 'Từ điều ' + str(hopdonglaodong.tudieu_hai_hopdong) + ' đến Điều ' + str(
                    hopdonglaodong.dendieu_hai_hopdong)
                noiquy.append(dieu2)
                noiquy_v.append(dieu2_v)

            if hopdonglaodong.tudieu_ba_hopdong and hopdonglaodong.dendieu_ba_hopdong:
                dieu3 = '第 ' + str(hopdonglaodong.tudieu_ba_hopdong) + ' 条～第 ' + str(
                    hopdonglaodong.dendieu_ba_hopdong) + ' 条 ,'
                dieu3_v = 'Từ điều ' + str(hopdonglaodong.tudieu_ba_hopdong) + ' đến Điều ' + str(
                    hopdonglaodong.dendieu_ba_hopdong)
                noiquy_v.append(dieu3_v)
                noiquy.append(dieu3)

            if noiquy != []:
                context['hhjp_nq'] = ','.join(noiquy)
            else:
                context['hhjp_nq'] = '第     条 ～ 第     条 '

            if noiquy_v != []:
                context['hhjp_nq_v'] = ','.join(noiquy_v)
            else:
                context['hhjp_nq_v'] = 'Từ điều      đến Điều     '

            ngaynghi_dinhki = []
            ngaynghi_dinhki_v = []
            if hopdonglaodong.ngaynghi_dinhki_thuhai == True:
                ngaynghi_dinhki.append('月')
                ngaynghi_dinhki_v.append('Thứ hai')

            if hopdonglaodong.ngaynghi_dinhki_thuba == True:
                ngaynghi_dinhki.append('火')
                ngaynghi_dinhki_v.append('Thứ ba')

            if hopdonglaodong.ngaynghi_dinhki_thutu == True:
                ngaynghi_dinhki.append('水')
                ngaynghi_dinhki_v.append('Thứ tư')

            if hopdonglaodong.ngaynghi_dinhki_thunam == True:
                ngaynghi_dinhki.append('木')
                ngaynghi_dinhki_v.append('Thứ năm')

            if hopdonglaodong.ngaynghi_dinhki_thusau == True:
                ngaynghi_dinhki.append('金')
                ngaynghi_dinhki_v.append('Thứ sáu')

            if hopdonglaodong.ngaynghi_dinhki_thubay == True:
                ngaynghi_dinhki.append('土')
                ngaynghi_dinhki_v.append('Thứ bảy')

            if hopdonglaodong.ngaynghi_dinhki_chunhat == True:
                ngaynghi_dinhki.append('日')
                ngaynghi_dinhki_v.append(u'Chủ nhật')

            # if hopdonglaodong.ngaynghi_dinhki_ngayle == True:
            #     ngaynghi_dinhki.append('祝')

            context['hhjp_0154'] = "、".join(ngaynghi_dinhki)
            context['hhjp_0154_v'] = "、".join(ngaynghi_dinhki_v)

            context['hhjp_0154_1'] = format_hoso.kiemtra(hopdonglaodong.ngaynghi_dinhki_khac)
            context['hhjp_0154_1_v'] = format_hoso.kiemtra(hopdonglaodong.ngaynghi_dinhki_khac_viet)

            context['hhjp_0393'] = format_hoso.kiemtra(hopdonglaodong.tongso_ngaynghi_nam)

            if hopdonglaodong.ngaynghi_khac == u'Tuần':
                context['hhjp_0396_1'] = format_hoso.kiemtra(hopdonglaodong.so_ngaynghi_khac)
            if hopdonglaodong.ngaynghi_khac == u'Tháng':
                context['hhjp_0396_1'] = format_hoso.kiemtra(hopdonglaodong.so_ngaynghi_khac)

            context['hhjp_0396_1_khac'] = format_hoso.kiemtra(hopdonglaodong.ngaynghi_khac_nhat)
            context['hhjp_0396_1_khac_v'] = format_hoso.kiemtra(hopdonglaodong.ngaynghi_khac_viet)

            noiquy_nn = []
            noiquy_nn_v = []
            if hopdonglaodong.tudieu_mot_ngaynghi and hopdonglaodong.dendieu_mot_ngaynghi:
                dieu_nn = '第 ' + str(hopdonglaodong.tudieu_mot_ngaynghi) + ' 条～第 ' + str(
                    hopdonglaodong.dendieu_mot_ngaynghi) + ' 条 ,'
                dieu_nnv = 'Từ điều ' + str(hopdonglaodong.tudieu_mot_ngaynghi) + ' đến Điều ' + str(
                    hopdonglaodong.dendieu_mot_ngaynghi)
                noiquy_nn.append(dieu_nn)
                noiquy_nn_v.append(dieu_nnv)

            if hopdonglaodong.tudieu_hai_ngaynghi and hopdonglaodong.dendieu_hai_ngaynghi:
                dieu_nn2 = '第 ' + str(hopdonglaodong.tudieu_hai_ngaynghi) + ' 条～第 ' + str(
                    hopdonglaodong.dendieu_hai_ngaynghi) + ' 条 ,'
                dieu_2_nnv = 'Từ điều ' + str(hopdonglaodong.tudieu_hai_ngaynghi) + ' đến Điều ' + str(
                    hopdonglaodong.dendieu_hai_ngaynghi)
                noiquy_nn.append(dieu_nn2)
                noiquy_nn_v.append(dieu_2_nnv)

            if noiquy_nn != []:
                context['hhjp_nn'] = ','.join(noiquy_nn)
            else:
                context['hhjp_nn'] = '第     条 ～ 第     条 '

            if noiquy_nn_v != []:
                context['hhjp_nn_v'] = ','.join(noiquy_nn_v)
            else:
                context['hhjp_nn_v'] = 'Từ điều      đến Điều     '
            context['hhjp_0394'] = format_hoso.kiemtra(hopdonglaodong.ngaynghiphep_coluong_lientuc_sauthang)

            if hopdonglaodong.duoi_sauthang_nghiphep_coluong == u'Có':
                context['hhjp_0395_1'] = '■'
                context['hhjp_0395_0'] = '□'
                docdata.remove_shape(u'id="3" name="Oval 3"')
            elif hopdonglaodong.duoi_sauthang_nghiphep_coluong == u'Không':
                context['hhjp_0395_0'] = '■'
                context['hhjp_0395_1'] = '□'
                docdata.remove_shape(u'id="2" name="Oval 2"')
            else:
                context['hhjp_0395_0'] = '□'
                context['hhjp_0395_1'] = '□'
                docdata.remove_shape(u'id="2" name="Oval 2"')
                docdata.remove_shape(u'id="3" name="Oval 3"')

            context['hhjp_0740'] = format_hoso.kiemtra(hopdonglaodong.nghiphep_coluong_thang)
            context['hhjp_0741'] = format_hoso.kiemtra(hopdonglaodong.nghiphep_coluong_ngay)
            context['hhjp_0740_v'] = format_hoso.kiemtra(hopdonglaodong.nghiphep_coluong_thang)
            context['hhjp_0741_v'] = format_hoso.kiemtra(hopdonglaodong.nghiphep_coluong_ngay)
            context['hhjp_0397'] = format_hoso.kiemtra(hopdonglaodong.nghiphep_nghicoluong)
            context['hhjp_0398'] = format_hoso.kiemtra(hopdonglaodong.nghikhac_khongluong)

            context['hhjp_0397_v'] = format_hoso.kiemtra(hopdonglaodong.nghiphep_nghicoluong_viet)
            context['hhjp_0398_v'] = format_hoso.kiemtra(hopdonglaodong.nghikhac_khongluong_viet)

            noiquy_np = []
            noiquy_np_v = []
            if hopdonglaodong.tudieu_mot_luong and hopdonglaodong.dendieu_mot_luong:
                dieu_np = '第 ' + str(hopdonglaodong.tudieu_mot_luong) + ' 条～第 ' + str(
                    hopdonglaodong.dendieu_mot_luong) + ' 条 ,'
                dieu_npv = 'Từ điều ' + str(hopdonglaodong.tudieu_mot_luong) + ' đến Điều ' + str(
                    hopdonglaodong.dendieu_mot_luong)
                noiquy_np.append(dieu_np)
                noiquy_np_v.append(dieu_npv)

            if hopdonglaodong.tudieu_hai_luong and hopdonglaodong.dendieu_hai_luong:
                dieu_np2 = '第 ' + str(hopdonglaodong.tudieu_hai_luong) + ' 条～第 ' + str(
                    hopdonglaodong.dendieu_hai_luong) + ' 条 ,'
                dieu_2_npv = 'Từ điều ' + str(hopdonglaodong.tudieu_hai_luong) + ' đến Điều ' + str(
                    hopdonglaodong.dendieu_hai_luong)
                noiquy_np.append(dieu_np2)
                noiquy_np_v.append(dieu_2_npv)

            if noiquy_np != []:
                context['hhjp_np'] = ','.join(noiquy_np)
            else:
                context['hhjp_np'] = '第     条 ～ 第     条 '

            if noiquy_np_v != []:
                context['hhjp_np_v'] = ','.join(noiquy_np_v)
            else:
                context['hhjp_np_v'] = 'Từ điều      đến Điều     '

            if hopdonglaodong.hinhthuc_luongthang:
                context['hhjp_0400_thang'] = format_hoso.place_value(hopdonglaodong.sotien_luongthang)
            elif hopdonglaodong.hinhthuc_luongngay:
                context['hhjp_0400_ngay'] = format_hoso.place_value(hopdonglaodong.sotien_luongngay)
            elif hopdonglaodong.hinhthuc_luonggio:
                context['hhjp_0400_gio'] = format_hoso.place_value(hopdonglaodong.sotien_luonggio)
            else:
                context['hhjp_0400_thang'] = ''
                context['hhjp_0400_ngay'] = ''
                context['hhjp_0400_gio'] = ''

            context['hhjp_0403'] = format_hoso.kiemtra(hopdonglaodong.lamthem_vuotqua_gio)
            if hopdonglaodong.lamthem_trongvong_giokhong == True:
                context['hhjp_0404'] = format_hoso.kiemtra(hopdonglaodong.lamthem_trongvong_gio)
            else:
                context['hhjp_0404'] = ''
            context['hhjp_0405'] = format_hoso.kiemtra(hopdonglaodong.theo_quydinh)
            context['hhjp_0406'] = format_hoso.kiemtra(hopdonglaodong.ngayle_theoquydinh)
            context['hhjp_0407'] = format_hoso.kiemtra(hopdonglaodong.ngayle_khong_theoquydinh)
            context['hhjp_0408'] = format_hoso.kiemtra(hopdonglaodong.laodong_bandem)

            if hopdonglaodong.ngaytinh_luong == u'Mỗi tháng':
                if hopdonglaodong.tinhluong_cuoithang == True:
                    context['hhjp_0409_1'] = '■'
                    context['hhjp_0409_2'] = '月末'
                    context['hhjp_0409_3'] = '□'
                    context['hhjp_0409_4'] = '____'
                    context['hhjp_0409_v'] = 'Ngày cuối hàng tháng'
                    context['hhjp_0409_v1'] = ''
                elif hopdonglaodong.tinhluong_cuoithang == False:
                    if hopdonglaodong.ngaytinh_luong_mot:
                        context['hhjp_0409_1'] = '■'
                        context['hhjp_0409_2'] = format_hoso.kiemtra(hopdonglaodong.ngaytinh_luong_mot)
                        context['hhjp_0409_3'] = '□'
                        context['hhjp_0409_4'] = '____'
                        context['hhjp_0409_v'] = format_hoso.kiemtra(hopdonglaodong.ngaytinh_luong_mot_v)
                        context['hhjp_0409_v1'] = ''
                    else:
                        context['hhjp_0409_1'] = '□'
                        context['hhjp_0409_2'] = '____'
                        context['hhjp_0409_3'] = '□'
                        context['hhjp_0409_4'] = '____'
                        context['hhjp_0409_v'] = ''
                        context['hhjp_0409_v1'] = ''

            elif hopdonglaodong.ngaytinh_luong == u'Mỗi tuần':
                if hopdonglaodong.tinhluong_cuoithang == True:
                    context['hhjp_0409_1'] = '■'
                    context['hhjp_0409_2'] = '月末'
                    context['hhjp_0409_v'] = 'Ngày cuối hàng tháng'

                elif hopdonglaodong.tinhluong_cuoithang == False:
                    if hopdonglaodong.ngaytinh_luong_mot:
                        context['hhjp_0409_1'] = '■'
                        context['hhjp_0409_2'] = format_hoso.kiemtra(hopdonglaodong.ngaytinh_luong_mot)
                        context['hhjp_0409_v'] = format_hoso.kiemtra(hopdonglaodong.ngaytinh_luong_mot_v)
                    else:
                        context['hhjp_0409_1'] = '□'
                        context['hhjp_0409_2'] = '____'
                        context['hhjp_0409_v'] = ''

                if hopdonglaodong.ngaytinh_luong_hai:
                    context['hhjp_0409_3'] = '■'
                    context['hhjp_0409_4'] = format_hoso.kiemtra(hopdonglaodong.ngaytinh_luong_hai)
                    context['hhjp_0409_v1'] = format_hoso.kiemtra(hopdonglaodong.ngaytinh_luong_hai_v)
                else:
                    context['hhjp_0409_3'] = '□'
                    context['hhjp_0409_4'] = ''
                    context['hhjp_0409_v1'] = ''

            if hopdonglaodong.ngaytra_luong == u'Mỗi tháng':
                if hopdonglaodong.traluong_cuoithang == True:
                    context['hhjp_0410_1'] = '■'
                    context['hhjp_0410_2'] = '月末'
                    context['hhjp_0410_3'] = '□'
                    context['hhjp_0410_4'] = '____'
                    context['hhjp_0410_v'] = 'Ngày cuối hàng tháng'
                    context['hhjp_0410_v1'] = ''

                elif hopdonglaodong.traluong_cuoithang == False:
                    if hopdonglaodong.ngaytra_luong_mot:
                        context['hhjp_0410_1'] = '■'
                        context['hhjp_0410_2'] = format_hoso.kiemtra(hopdonglaodong.ngaytra_luong_mot)
                        context['hhjp_0410_3'] = '□'
                        context['hhjp_0410_4'] = '____'
                        context['hhjp_0410_v'] = format_hoso.kiemtra(hopdonglaodong.ngaytra_luong_mot_v)
                        context['hhjp_0410_v1'] = ''
                    else:
                        context['hhjp_0410_1'] = '□'
                        context['hhjp_0410_2'] = '____'
                        context['hhjp_0410_3'] = '□'
                        context['hhjp_0410_4'] = '____'
                        context['hhjp_0410_v'] = ''
                        context['hhjp_0410_v1'] = ''

            elif hopdonglaodong.ngaytra_luong == u'Mỗi tuần':
                if hopdonglaodong.traluong_cuoithang == True:
                    context['hhjp_0410_1'] = '■'
                    context['hhjp_0410_2'] = '月末'
                    context['hhjp_0410_v'] = 'Ngày cuối hàng tháng'

                elif hopdonglaodong.traluong_cuoithang == False:
                    if hopdonglaodong.ngaytra_luong_mot:
                        context['hhjp_0410_1'] = '■'
                        context['hhjp_0410_2'] = format_hoso.kiemtra(hopdonglaodong.ngaytra_luong_mot)
                        context['hhjp_0410_v'] = format_hoso.kiemtra(hopdonglaodong.ngaytra_luong_mot_v)
                    else:
                        context['hhjp_0410_1'] = '□'
                        context['hhjp_0410_2'] = '____'
                        context['hhjp_0410_v'] = ''

                if hopdonglaodong.ngaytra_luong_hai:
                    context['hhjp_0410_3'] = '■'
                    context['hhjp_0410_4'] = format_hoso.kiemtra(hopdonglaodong.ngaytra_luong_hai)
                    context['hhjp_0410_v1'] = format_hoso.kiemtra(hopdonglaodong.ngaytra_luong_hai_v)
                else:
                    context['hhjp_0410_3'] = '□'
                    context['hhjp_0410_4'] = ''
                    context['hhjp_0410_v1'] = ''

            if hopdonglaodong.hinhthuc_thanhtoan == 'Tiền mặt':
                context['hhjp_0411_1'] = '■'
                context['hhjp_0411_2'] = '□'
                context['hhjp_0411_v'] = 'Trả tiền mặt'
            if hopdonglaodong.hinhthuc_thanhtoan == 'Chuyển khoản':
                context['hhjp_0411_1'] = '□'
                context['hhjp_0411_2'] = '■'
                context['hhjp_0411_v'] = 'Chuyển khoản ngân hàng'

            if hopdonglaodong.khautru_luong == 'Không':
                context['hhjp_0412_1'] = '■'
                context['hhjp_0412_2'] = '□'
                context['hhjp_0412_v'] = 'Không'
            elif hopdonglaodong.khautru_luong == 'Có':
                context['hhjp_0412_1'] = '□'
                context['hhjp_0412_2'] = '■'
                context['hhjp_0412_v'] = 'Có'
            else:
                context['hhjp_0412_1'] = '□'
                context['hhjp_0412_2'] = '□'
                context['hhjp_0412_v'] = ''

            if hopdonglaodong.tangluong == 'Có':
                context['hhjp_0413_1'] = '■'
                context['hhjp_0413_3'] = format_hoso.kiemtra(hopdonglaodong.thoidiem_tangluong)
                context[
                    'hhjp_0413_3_v'] = format_hoso.kiemtra(hopdonglaodong.thoidiem_tangluong_viet)
                context['hhjp_0413_2'] = '□'
            elif hopdonglaodong.tangluong == 'Không':
                context['hhjp_0413_1'] = '□'
                context['hhjp_0413_2'] = '■'
                context['hhjp_0413_3'] = ''
                context['hhjp_0413_3_v'] = ''
            else:
                context['hhjp_0413_1'] = '□'
                context['hhjp_0413_2'] = '□'
                context['hhjp_0413_3'] = ''
                context['hhjp_0413_3_v'] = ''

            if hopdonglaodong.thuong == 'Có':
                context['hhjp_0414_1'] = '■'
                context['hhjp_0414_3'] = format_hoso.kiemtra(hopdonglaodong.thoidiem_thuong)
                context['hhjp_0414_3_v'] = format_hoso.kiemtra(hopdonglaodong.thoidiem_thuong_viet)
                context['hhjp_0414_2'] = '□'
            elif hopdonglaodong.thuong == 'Không':
                context['hhjp_0414_2'] = '■'
                context['hhjp_0414_1'] = '□'
                context['hhjp_0414_3'] = ''
                context['hhjp_0414_3_v'] = ''
            else:
                context['hhjp_0414_2'] = '□'
                context['hhjp_0414_1'] = '□'
                context['hhjp_0414_3'] = ''
                context['hhjp_0414_3_v'] = ''

            if hopdonglaodong.trocap_thoiviec == 'Có':
                context['hhjp_0415_2'] = '□'
                context['hhjp_0415_1'] = '■'
                context['hhjp_0415_3'] = format_hoso.kiemtra(hopdonglaodong.thoidiem_trocap_thoiviec)
                context['hhjp_0415_3_v'] = format_hoso.kiemtra(hopdonglaodong.thoidiem_trocap_thoiviec_viet)
            elif hopdonglaodong.trocap_thoiviec == 'Không':
                context['hhjp_0415_1'] = '□'
                context['hhjp_0415_2'] = '■'
                context['hhjp_0415_3'] = ''
                context['hhjp_0415_3_v'] = ''
            else:
                context['hhjp_0415_1'] = '□'
                context['hhjp_0415_2'] = '□'
                context['hhjp_0415_3'] = ''
                context['hhjp_0415_3_v'] = ''

            if hopdonglaodong.phucap_ngung_kinhdoanh == 'Có':
                context['hhjp_0416'] = '■'
                context['hhjp_0416_1'] = format_hoso.kiemtra(hopdonglaodong.tyle_ngung_kinhdoanh)
                context['hhjp_0416_1_v'] = format_hoso.kiemtra(hopdonglaodong.tyle_ngung_kinhdoanh)
            elif hopdonglaodong.phucap_ngung_kinhdoanh == 'Không':
                context['hhjp_0416'] = '□'
                context['hhjp_0416_1'] = ''
                context['hhjp_0416_1_v'] = ''
            else:
                context['hhjp_0416'] = '□'
                context['hhjp_0416_1'] = ''
                context['hhjp_0416_1_v'] = ''

            context['hhjp_0417'] = format_hoso.kiemtra(hopdonglaodong.noptruoc_thoiviec)

            noiquy_tv = []
            noiquy_tvv = []
            if hopdonglaodong.tudieu_mot_thoiviec and hopdonglaodong.dendieu_mot_thoiviec:
                dieu_tv = '第 ' + str(hopdonglaodong.tudieu_mot_thoiviec) + ' 条～第 ' + str(
                    hopdonglaodong.dendieu_mot_thoiviec) + ' 条 ,'
                dieu_tvv = 'Từ điều ' + str(hopdonglaodong.tudieu_mot_thoiviec) + ' đến Điều ' + str(
                    hopdonglaodong.dendieu_mot_thoiviec)
                noiquy_tv.append(dieu_tv)
                noiquy_tvv.append(dieu_tvv)

            if hopdonglaodong.tudieu_hai_thoiviec and hopdonglaodong.dendieu_hai_thoiviec:
                dieu_tv = '第 ' + str(hopdonglaodong.tudieu_hai_thoiviec) + ' 条～第 ' + str(
                    hopdonglaodong.dendieu_hai_thoiviec) + ' 条 ,'
                dieu_tvv = 'Từ điều ' + str(hopdonglaodong.tudieu_hai_thoiviec) + ' đến Điều ' + str(
                    hopdonglaodong.dendieu_hai_thoiviec)
                noiquy_tv.append(dieu_tv)
                noiquy_tvv.append(dieu_tvv)

            if noiquy_tv != []:
                context['hhjp_tv'] = ','.join(noiquy_tv)
            else:
                context['hhjp_tv'] = '第     条 ～ 第     条 '

            if noiquy_tvv != []:
                context['hhjp_tvv'] = ','.join(noiquy_tvv)
            else:
                context['hhjp_tvv'] = 'Từ điều      đến Điều     '

            context['hhjp_0157'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.tennha_daingo_daotao_nhao)
            context['hhjp_0157_v'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.tennha_daingo_daotao_nhao_v)

            if document.donhang_hoso.donhang_daingo.loainha_daingo_daotao_nhao == 'Kí túc xá':
                context['hhjp_0482_1'] = '■'
                context['hhjp_0482_2'] = '□'
                context['hhjp_0482_3'] = '□'
                context['hhjp_0482'] = ''
                context['hhjp_0482_v'] = ''
            elif document.donhang_hoso.donhang_daingo.loainha_daingo_daotao_nhao == 'Nhà thuê':
                context['hhjp_0482_1'] = '□'
                context['hhjp_0482_2'] = '■'
                context['hhjp_0482_3'] = '□'
                context['hhjp_0482'] = ''
                context['hhjp_0482_v'] = ''
            elif document.donhang_hoso.donhang_daingo.loainha_daingo_daotao_nhao == 'Khác':
                context['hhjp_0482_1'] = '□'
                context['hhjp_0482_2'] = '□'
                context['hhjp_0482_3'] = '■'
                context['hhjp_0482'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.khac_loainha_daingo_daotao_nhao)
                context['hhjp_0482_v'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.khac_loainha_daingo_daotao_nhao_v)
            else:
                context['hhjp_0482_1'] = '□'
                context['hhjp_0482_2'] = '□'
                context['hhjp_0482_3'] = '□'
                context['hhjp_0482'] = ''
                context['hhjp_0482_v'] = ''

            if document.donhang_hoso.donhang_daingo.sobuudien_daingo_nhao:
                context['bs_0019'] = document.donhang_hoso.donhang_daingo.sobuudien_daingo_nhao[
                                     0:3] + '-' + document.donhang_hoso.donhang_daingo.sobuudien_daingo_nhao[3:]
            else:
                context['bs_0019'] = ''

            context['hhjp_0158'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.diachi_daingo_daotao_nhao)
            context['hhjp_0483'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.dienthoai_daingo_daotao_nhao)
            context['hhjp_0158_v'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.diachi_daingo_daotao_nhao_v)

            context['hhjp_0484'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.matbang_daingo_daotao_nhao)
            context['hhjp_0485'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.songuoi_daingo_daotao_nhao)
            context['hhjp_0486'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.dientich_nguoi_daingo_daotao_nhao)

            context['hhjp_0470_v'] = Listing(
                document.donhang_hoso.donhang_daingo.thuctapsinhtra_daingo_daotao_nhao_viet) if document.donhang_hoso.donhang_daingo.thuctapsinhtra_daingo_daotao_nhao_viet else ''
            context['hhjp_040'] = Listing(
                document.donhang_hoso.donhang_daingo.thuctapsinhtra_daingo_daotao_nhao) if document.donhang_hoso.donhang_daingo.thuctapsinhtra_daingo_daotao_nhao else ''

            if hopdonglaodong.huutri_baohiem == True:
                context['hhjp_0418_1'] = '■'
            else:
                context['hhjp_0418_1'] = '□'
            if hopdonglaodong.huwuquocgia_baohiem == True:
                context['hhjp_0418_2'] = '■'
            else:
                context['hhjp_0418_2'] = '□'
            if hopdonglaodong.yte_baohiem == True:
                context['hhjp_0418_3'] = '■'
            else:
                context['hhjp_0418_3'] = '□'
            if hopdonglaodong.yte_quocgia_baohiem == True:
                context['hhjp_0418_4'] = '■'
            else:
                context['hhjp_0418_4'] = '□'

            if hopdonglaodong.vieclam_baohiem == True:
                context['hhjp_0418_5'] = '■'
            else:
                context['hhjp_0418_5'] = '□'

            if hopdonglaodong.congnghiep_baohiem == True:
                context['hhjp_0418_6'] = '■'
            else:
                context['hhjp_0418_6'] = '□'

            if hopdonglaodong.khac_baohiem == True:
                context['hhjp_0418_7'] = '■'
                context['hhjp_0418_8'] = format_hoso.kiemtra(hopdonglaodong.noidung_khac_baohiem)
            else:
                context['hhjp_0418_7'] = '□'
                context['hhjp_0418_8'] = format_hoso.kiemtra(hopdonglaodong.noidung_khacviet_baohiem)

            context[
                'hhjp_0419'] = hopdonglaodong.nam_suckhoe_congty + ' 年 ' + hopdonglaodong.thang_suckhoe_congty + ' 月' if hopdonglaodong.nam_suckhoe_congty else ''
            context[
                'hhjp_0419_v'] = 'Năm ' + hopdonglaodong.nam_suckhoe_congty + ' Tháng ' + hopdonglaodong.thang_suckhoe_congty if hopdonglaodong.nam_suckhoe_congty else ''

            context[
                'hhjp_0420'] = hopdonglaodong.nam_suckhoe_dinhky_landau + ' 年 ' + hopdonglaodong.thang_suckhoe_dinhky_landau + ' 月' if hopdonglaodong.nam_suckhoe_dinhky_landau else ''
            context[
                'hhjp_0420_v'] = 'Năm ' + hopdonglaodong.nam_suckhoe_dinhky_landau + ' Tháng ' + hopdonglaodong.thang_suckhoe_dinhky_landau if hopdonglaodong.nam_suckhoe_dinhky_landau else ''

            if hopdonglaodong.tanso_khamlai_thangtuan == 'Tuần':
                context['hhjp_0421'] = '週'
                context['hhjp_0421_v'] = 'tuần'
            elif hopdonglaodong.tanso_khamlai_thangtuan == 'Năm':
                context['hhjp_0421'] = '年'
                context['hhjp_0421_v'] = 'năm'
            else:
                context['hhjp_0421_v'] = ''
                context['hhjp_0421'] = ''

            if hopdonglaodong.hinhthuc_luongthang:
                context['hhjp_0400_thang'] = format_hoso.place_value(hopdonglaodong.sotien_luongthang)
            elif hopdonglaodong.hinhthuc_luongngay:
                context['hhjp_0400_ngay'] = format_hoso.place_value(hopdonglaodong.sotien_luongngay)
            elif hopdonglaodong.hinhthuc_luonggio:
                context['hhjp_0400_gio'] = format_hoso.place_value(hopdonglaodong.sotien_luonggio)
            else:
                context['hhjp_0400_thang'] = ''
                context['hhjp_0400_ngay'] = ''
                context['hhjp_0400_gio'] = ''

            if hopdonglaodong.hinhthuc_luongthang:
                context['hhjp_0400_0'] = '■'
                context['hhjp_0400_1'] = '□'
                context['hhjp_0400_2'] = '□'
                context['hhjp_0422'] = format_hoso.place_value(hopdonglaodong.sotien_luongthang_tong)
                context['hhjp_0147'] = ''
            elif hopdonglaodong.hinhthuc_luongngay:
                context['hhjp_0400_0'] = '□'
                context['hhjp_0400_1'] = '■'
                context['hhjp_0400_2'] = '□'
                context['hhjp_0422'] = format_hoso.place_value(hopdonglaodong.tongtien_thang_luongngay_1)
                context['hhjp_0147'] = format_hoso.place_value(hopdonglaodong.tongtien_thang_luongngay_2)
            elif hopdonglaodong.hinhthuc_luonggio:
                context['hhjp_0400_0'] = '□'
                context['hhjp_0400_1'] = '□'
                context['hhjp_0400_2'] = '■'
                context['hhjp_0422'] = ''
                context['hhjp_0147'] = format_hoso.place_value(hopdonglaodong.sotien_luonggio_tong)
            else:
                context['hhjp_0400_0'] = '□'
                context['hhjp_0400_1'] = '□'
                context['hhjp_0400_2'] = '□'
                context['hhjp_0422'] = ''
                context['hhjp_0147'] = ''

            if hopdonglaodong.sotien_phuccap_nhao:
                context['hhjp_0425_1'] = '住宅'
                context['hhjp_0424_1'] = format_hoso.kiemtra(hopdonglaodong.sotien_phuccap_nhao)
                context['hhjp_0426_1'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_nhao)

                context['hhjp_0425_1_v'] = format_hoso.kiemtra(hopdonglaodong.khac_phucap_nhao_viet)
                context['hhjp_0426_1_v'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_nhao_viet)
            else:
                context['hhjp_0425_1'] = ''
                context['hhjp_0424_1'] = ''
                context['hhjp_0426_1'] = ''
                context['hhjp_0425_1_v'] = ''
                context['hhjp_0426_1_v'] = ''

            if hopdonglaodong.sotien_phuccap_an:
                context['hhjp_0425_2'] = '食事'
                context['hhjp_0424_2'] = format_hoso.kiemtra(hopdonglaodong.sotien_phuccap_an)
                context['hhjp_0426_2'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_an)

                context['hhjp_0425_2_v'] = format_hoso.kiemtra(hopdonglaodong.khac_phucap_an_viet)
                context['hhjp_0426_2_v'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_an_viet)
            else:
                context['hhjp_0425_2'] = ''
                context['hhjp_0424_2'] = ''
                context['hhjp_0426_2'] = ''
                context['hhjp_0425_2_v'] = ''
                context['hhjp_0426_2_v'] = ''

            if hopdonglaodong.sotien_phuccap_dilai:
                context['hhjp_0425_3'] = '通勤'
                context['hhjp_0424_3'] = format_hoso.kiemtra(hopdonglaodong.sotien_phuccap_dilai)
                context['hhjp_0426_3'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_dilai)

                context['hhjp_0425_3_v'] = format_hoso.kiemtra(hopdonglaodong.khac_phucap_dilai_viet)
                context['hhjp_0426_3_v'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_dilai_viet)
            else:
                context['hhjp_0425_3'] = ''
                context['hhjp_0424_3'] = ''
                context['hhjp_0426_3'] = ''
                context['hhjp_0425_3_v'] = ''
                context['hhjp_0426_3_v'] = ''

            if hopdonglaodong.sotien_phuccap_thue:
                context['hhjp_0425_4'] = '課税通勤'
                context['hhjp_0424_4'] = format_hoso.kiemtra(hopdonglaodong.sotien_phuccap_thue)
                context['hhjp_0426_4'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_thue)

                context['hhjp_0425_4_v'] = format_hoso.kiemtra(hopdonglaodong.khac_phucap_thue_viet)
                context['hhjp_0426_4_v'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_thue_viet)
            else:
                context['hhjp_0425_4'] = ''
                context['hhjp_0424_4'] = ''
                context['hhjp_0426_4'] = ''
                context['hhjp_0425_4_v'] = ''
                context['hhjp_0426_4_v'] = ''

            if hopdonglaodong.phucap_them != True:
                if hopdonglaodong.sotien_phuccap_mot:
                    context['hhjp_0425_5'] = format_hoso.kiemtra(hopdonglaodong.phuccap_mot)
                    context['hhjp_0424_5'] = format_hoso.kiemtra(hopdonglaodong.sotien_phuccap_mot)
                    context['hhjp_0426_5'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_mot)

                    context['hhjp_0425_5_v'] = format_hoso.kiemtra(hopdonglaodong.khac_phucap_mot_viet)
                    context['hhjp_0426_5_v'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_mot_viet)
                else:
                    context['hhjp_0425_5'] = ''
                    context['hhjp_0424_5'] = ''
                    context['hhjp_0426_5'] = ''
                    context['hhjp_0425_5_v'] = ''
                    context['hhjp_0426_5_v'] = ''

                if hopdonglaodong.sotien_phuccap_hai:
                    context['hhjp_0425_6'] = format_hoso.kiemtra(hopdonglaodong.phuccap_hai)
                    context['hhjp_0424_6'] = format_hoso.kiemtra(hopdonglaodong.sotien_phuccap_hai)
                    context['hhjp_0426_6'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_hai)

                    context['hhjp_0425_6_v'] = format_hoso.kiemtra(hopdonglaodong.khac_phucap_hai_viet)
                    context['hhjp_0426_6_v'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_hai_viet)
                else:
                    context['hhjp_0425_6'] = ''
                    context['hhjp_0424_6'] = ''
                    context['hhjp_0426_6'] = ''
                    context['hhjp_0425_6_v'] = ''
                    context['hhjp_0426_6_v'] = ''

                if hopdonglaodong.sotien_phuccap_ba:
                    context['hhjp_0425_7'] = format_hoso.kiemtra(hopdonglaodong.phuccap_ba)
                    context['hhjp_0424_7'] = format_hoso.kiemtra(hopdonglaodong.sotien_phuccap_ba)
                    context['hhjp_0426_7'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_ba)

                    context['hhjp_0425_7_v'] = format_hoso.kiemtra(hopdonglaodong.khac_phucap_ba_viet)
                    context['hhjp_0426_7_v'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_ba_viet)
                else:
                    context['hhjp_0425_7'] = ''
                    context['hhjp_0424_7'] = ''
                    context['hhjp_0426_7'] = ''
                    context['hhjp_0425_7_v'] = ''
                    context['hhjp_0426_7_v'] = ''

                if hopdonglaodong.sotien_phuccap_bon:
                    context['hhjp_0425_8'] = format_hoso.kiemtra(hopdonglaodong.phuccap_bon)
                    context['hhjp_0424_8'] = format_hoso.kiemtra(hopdonglaodong.sotien_phuccap_bon)
                    context['hhjp_0426_8'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_bon)

                    context['hhjp_0425_8_v'] = format_hoso.kiemtra(hopdonglaodong.khac_phucap_bon_viet)
                    context['hhjp_0426_8_v'] = format_hoso.kiemtra(hopdonglaodong.cachtinh_phucap_bon_viet)
                else:
                    context['hhjp_0425_8'] = ''
                    context['hhjp_0424_8'] = ''
                    context['hhjp_0426_8'] = ''
                    context['hhjp_0425_8_v'] = ''
                    context['hhjp_0426_8_v'] = ''

            context['hhjp_0701'] = format_hoso.place_value(hopdonglaodong.khoankhac_phucap_tts)

            context['hhjp_kk1'] = format_hoso.kiemtra(hopdonglaodong.noidung_khoanmuc_10)
            context['hhjp_kk1_v'] = format_hoso.kiemtra(hopdonglaodong.noidung_khoanmuc_viet_10)
            context['hhjp_kk1t'] = format_hoso.place_value(hopdonglaodong.sotien_khoanmuc_10)
            if hopdonglaodong.chiphi_khoanmuc_10 == True:
                context['hhjp_kk1_1'] = '実費'
                context['h2'] = 'Chi phí thực tế'
            else:
                context['hhjp_kk1_1'] = ''
                context['h2'] = ''

            context['hhjp_kk2'] = format_hoso.kiemtra(hopdonglaodong.noidung_khoanmuc_11)
            context['hhjp_kk2_v'] = format_hoso.kiemtra(hopdonglaodong.noidung_khoanmuc_viet_11)
            context['hhjp_kk2t'] = format_hoso.place_value(hopdonglaodong.sotien_khoanmuc_11)
            if hopdonglaodong.chiphi_khoanmuc_11 == True:
                context['hhjp_kk2_1'] = '実費'
                context['h3'] = 'Chi phí thực tế'
            else:
                context['hhjp_kk2_1'] = ''
                context['h3'] = ''

            context['hhjp_kk3'] = format_hoso.kiemtra(hopdonglaodong.noidung_khoanmuc_12)
            context['hhjp_kk3_v'] = format_hoso.kiemtra(hopdonglaodong.noidung_khoanmuc_viet_12)
            context['hhjp_kk3t'] = format_hoso.place_value(hopdonglaodong.sotien_khoanmuc_12)
            if hopdonglaodong.chiphi_khoanmuc_12 == True:
                context['hhjp_kk3_1'] = '実費'
                context['h4'] = 'Chi phí thực tế'
            else:
                context['hhjp_kk3_1'] = ''
                context['h4'] = ''

            context['hhjp_kk4'] = format_hoso.kiemtra(hopdonglaodong.noidung_khoanmuc_13)
            context['hhjp_kk4_v'] = format_hoso.kiemtra(hopdonglaodong.noidung_khoanmuc_viet_13)
            context['hhjp_kk4t'] = format_hoso.place_value(hopdonglaodong.sotien_khoanmuc_13)
            if hopdonglaodong.chiphi_khoanmuc_13 == True:
                context['hhjp_kk4_1'] = '実費'
                context['h5'] = 'Chi phí thực tế'
            else:
                context['hhjp_kk4_1'] = ''
                context['h5'] = ''

            # context['hhjp_kk5'] = format_hoso.kiemtra(hopdonglaodong.noidung_khoanmuc_14)
            # context['hhjp_kk5_v'] = format_hoso.kiemtra(hopdonglaodong.noidung_khoanmuc_viet_14)
            # context['hhjp_kk5t'] = format_hoso.place_value(hopdonglaodong.sotien_khoanmuc_14)
            # if hopdonglaodong.chiphi_khoanmuc_14 == True:
            #     context['hhjp_kk5_1'] = '実費'
            # else:
            #     context['hhjp_kk5_1'] = ''

            context['hhjp_0427'] = format_hoso.place_value(
                hopdonglaodong.sotien_khoanmuc_1 + hopdonglaodong.sotien_khoanmuc_2)
            context['hhjp_0428'] = format_hoso.place_value(
                hopdonglaodong.sotien_khoanmuc_3 + hopdonglaodong.sotien_khoanmuc_4 + hopdonglaodong.sotien_khoanmuc_5)
            context['hhjp_0429'] = format_hoso.place_value(hopdonglaodong.sotien_khoanmuc_6)
            context['hhjp_0430'] = format_hoso.place_value(hopdonglaodong.sotien_khoanmuc_7)
            if hopdonglaodong.chiphi_khoanmuc_7 == True:
                context['hhjp_0430_1'] = '実費'
            else:
                context['hhjp_0430_1'] = ''

            context['hhjp_0431'] = format_hoso.place_value(hopdonglaodong.sotien_khoanmuc_8)
            if hopdonglaodong.chiphi_khoanmuc_8 == True:
                context['hhjp_0431_1'] = '実費'
            else:
                context['hhjp_0431_1'] = ''

            context['h32'] = format_hoso.kiemtra(hopdonglaodong.sotien_khoanmuc_9)
            if hopdonglaodong.chiphi_khoanmuc_9 == True:
                context['hk'] = '実費'
                context['h1'] = 'Chi phí thực tế'
            else:
                context['hk'] = ''
                context['h1'] = ''

            context['hhjp_0433'] = format_hoso.place_value(hopdonglaodong.tong_sotien_khoanmuc)
            context['hhjp_0434'] = format_hoso.place_value(hopdonglaodong.thanhtoan_tienmat)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_56(self, ma_thuctapsinh, document):
        print('56')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_56")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            context = {}
            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            context['hhjp_0001_v'] = format_hoso.kiemtra(thuctapsinh.ten_tv_tts)
            if document.donhang_hoso.donhang_daingo.trocap_daotao_thangdau == u'Có':
                context['hhjp_0138_1'] = u'■'
                context['hhjp_0138_2'] = u'□'
                context['hhjp_0467'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.noidung_trocap_daotao_thangdau)
                context['hhjp_0467_v'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.noidung_trocap_daotao_thangdau_v)
            else:
                context['hhjp_0138_1'] = u'□'
                context['hhjp_0138_2'] = u'■'
                context['hhjp_0467'] = ''
                context['hhjp_0467_v'] = ''
            context['hhjp_0464'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.ghichu_trocap_daotao_thangdau)
            context['hhjp_0464_v'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.ghichu_trocap_daotao_thangdau_v)
            context['hhjp_0468'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.noidung_tts_tra_daingo_tienan)
            context['hhjp_0468_v'] = format_hoso.kiemtra(
                document.donhang_hoso.donhang_daingo.noidung_tts_tra_daingo_tienan_v)
            if document.donhang_hoso.donhang_daingo.trocap_daingo_tienan == u'Có':
                context['hhjp_0139_1'] = u'■'
                context['hhjp_0139_2'] = u'□'
                context['hhjp_0466'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.noidung_trocap_daingo_tienan)
                context['hhjp_0466_v'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.noidung_trocap_daingo_tienan_v)
            else:
                context['hhjp_0139_1'] = u'□'
                context['hhjp_0139_2'] = u'■'
                context['hhjp_0466'] = ''
                context['hhjp_0466_v'] = ''
            if document.donhang_hoso.donhang_daingo.tts_tra_daingo_tienan == u'Có':
                context['hhjp_0140_1'] = u'■'
                context['hhjp_0140_2'] = u'□'
                context['hhjp_0468'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.noidung_tts_tra_daingo_tienan)
                context['hhjp_0468_v'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.noidung_tts_tra_daingo_tienan_v)
            else:
                context['hhjp_0140_1'] = u'□'
                context['hhjp_0140_2'] = u'■'
                context['hhjp_0468'] = ''
                context['hhjp_0468_v'] = ''
            context['hhjp_0465'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.ghichu_daingo_tienan)
            context['hhjp_0465_v'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.ghichu_daingo_tienan_v)
            if document.donhang_hoso.donhang_daingo.trocap_daingo_nhao == u'Có':
                context['hhjp_0141_1'] = u'■'
                context['hhjp_0141_2'] = u'□'
                context['hhjp_0469'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.noidung_trocap_daingo_nhao)
                context['hhjp_0469_v'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.noidung_trocap_daingo_nhao_v)
            else:
                context['hhjp_0141_1'] = u'□'
                context['hhjp_0141_2'] = u'■'
                context['hhjp_0469'] = ''
                context['hhjp_0469_v'] = ''
            if document.donhang_hoso.donhang_daingo.trocap_daingo_nhao == u'Có':
                context['hhjp_0142_1'] = u'■'
                context['hhjp_0142_2'] = u'□'
                context['hhjp_0470_v'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.noidung_tts_tra_daingo_nhao_v)
                context['hhjp_0470'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.noidung_tts_tra_daingo_nhao)
            else:
                context['hhjp_0142_1'] = u'□'
                context['hhjp_0142_2'] = u'■'
                context['hhjp_0470'] = ''
                context['hhjp_0470_v'] = ''

            if document.donhang_hoso.donhang_daingo.loainha_daingo_nhao == 'Kí túc xá':
                docdata.remove_shape(u'id="9" name="Oval 9"')
                docdata.remove_shape(u'id="10" name="Oval 10"')
            elif document.donhang_hoso.donhang_daingo.loainha_daingo_nhao == 'Nhà thuê':
                docdata.remove_shape(u'id="10" name="Oval 10"')
                docdata.remove_shape(u'id="11" name="Oval 11"')
            elif document.donhang_hoso.donhang_daingo.loainha_daingo_nhao == 'Khác':
                docdata.remove_shape(u'id="9" name="Oval 0"')
                docdata.remove_shape(u'id="11" name="Oval 11"')

                context['hhjp_0473'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.khac_loainha_daingo_nhao)
                context['hhjp_0473_v'] = format_hoso.kiemtra(
                    document.donhang_hoso.donhang_daingo.khac_loainha_daingo_nhao_v)
            else:
                docdata.remove_shape(u'id="9" name="Oval 9"')
                docdata.remove_shape(u'id="10" name="Oval 10"')
                docdata.remove_shape(u'id="11" name="Oval 11"')
                context[
                    'hhjp_0473'] = ''
                context[
                    'hhjp_0473_v'] = ''
            context['hhjp_0144'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.tennha_daingo_nhao)
            context['hhjp_0144_v'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.tennha_daingo_nhao_v)

            if document.donhang_hoso.donhang_daingo.sobuudien_daingo_nhao:
                context['bs_0018'] = document.donhang_hoso.donhang_daingo.sobuudien_daingo_nhao[
                                     0:3] + '-' + document.donhang_hoso.donhang_daingo.sobuudien_daingo_nhao[3:]
            else:
                context['bs_0018'] = ''

            context['hhjp_0145'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.diachi_daingo_nhao)
            context['hhjp_0145_v'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.diachi_daingo_nhao_v)
            context['hhjp_0474'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.dienthoai_daingo_nhao)
            context['hhjp_0475'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.matbang_daingo_nhao)
            context['hhjp_0476'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.songuoi_daingo_nhao)
            context['hhjp_0477'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.dientich_nguoi_daingo_nhao)
            context['hhjp_0478'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.muckhac_daingo_khac)
            context['hhjp_0478_v'] = format_hoso.kiemtra(document.donhang_hoso.donhang_daingo.muckhac_daingo_khac_v)
            if document.ngay_lapvanban1:
                context['hhjp_0193'] = str(document.ngay_lapvanban1.year) + '年' + \
                                       str(document.ngay_lapvanban1.month) + '月' + \
                                       str(document.ngay_lapvanban1.day) + '日'
                context['hhjp_0193_v'] = ' Ngày ' + str(document.ngay_lapvanban1.day) + \
                                         ' tháng ' + str(document.ngay_lapvanban1.month) + \
                                         ' năm ' + str(document.ngay_lapvanban1.year)
            else:
                context['hhjp_0193'] = '年　月　日'
                context['hhjp_0193_v'] = 'Ngày tháng năm'
            context['hhjp_0905'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_nguoidaidien)
            context['hhjp_0905_v'] = format_hoso.kiemtra(
                document.donhang_hoso.congtyphaicu_donhang.daidien_latinh_phaicu)
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_57(self, ma_thuctapsinh, document):
        print('57')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_57")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            context = {}
            hopdonglaodong = self.env['hopdong.hopdong'].search(
                [('thuctapsinh_hopdong', '=', thuctapsinh.id), ('donhang_hopdong', '=', document.donhang_hoso.id),
                 ('giaidoan_hopdong', '=', document.giaidoan_hoso)],
                limit=1)
            context['hhjp_0003'] = format_hoso.kiemtra(thuctapsinh.quoctich_tts.name_quoctich)
            if document.ngay_lapvanban1:
                context['hhjp_0193'] = str(document.ngay_lapvanban1.year) + '年' + \
                                       str(document.ngay_lapvanban1.month) + '月' + \
                                       str(document.ngay_lapvanban1.day) + '日'
                context['hhjp_0193_v'] = ' Ngày ' + str(document.ngay_lapvanban1.day) + \
                                         ' tháng ' + str(document.ngay_lapvanban1.month) + \
                                         ' năm ' + str(document.ngay_lapvanban1.year)
            else:
                context['hhjp_0193'] = '年　月　日'
                context['hhjp_0193_v'] = 'Ngày tháng năm'
            if thuctapsinh.nganhnghe_tts:
                context['hhjp_0115'] = format_hoso.nganhnghe(thuctapsinh.nganhnghe_tts.name_loaicongviec)
                context['hhjp_0115_v'] = format_hoso.kiemtra(thuctapsinh.nganhnghe_tts_v).lower()
                if thuctapsinh.donhang_tts.dudinh_quaylai == True:
                    context['bs_0020'] = '技能実習開始前に所属していた勤務先に復職'
                    context[
                        'bs_0020_v'] = 'trở lại làm việc tại cơ quan phái cử phụ thuộc trước khi bắt đầu khóa thực tập kỹ năng.'
                elif thuctapsinh.donhang_tts.dudinh_khong_quaylai == True or thuctapsinh.donhang_tts.dudinh_khongco_ydinh == True:
                    context['bs_0020'] = '送出し機関の協力を得て' + str(format_hoso.nganhnghe(
                        thuctapsinh.nganhnghe_tts.name_loaicongviec)) + 'の技能を活用できる業種への就職を目指'
                    context['bs_0020_v'] = 'hướng tới việc làm có thể tận dụng được nghiệp vụ kỹ năng ' + str(
                        format_hoso.kiemtra(
                            thuctapsinh.nganhnghe_tts_v)) + ' cùng với sự hợp tác giúp đỡ của công ty phái cử'
                else:
                    context['bs_0020'] = '______________________'
                    context['bs_0020_v'] = '______________________'
            # elif hopdonglaodong.loai_nghe_nganhnghe_congviec:
            #     context['hhjp_0115'] = format_hoso.nganhnghe(
            #         hopdonglaodong.loai_nghe_nganhnghe_congviec.name_loaicongviec)
            #     context['hhjp_0115_v'] = format_hoso.kiemtra(hopdonglaodong.loai_nghe_nganhnghe_tts_viet)
            #     if thuctapsinh.donhang_tts.dudinh_quaylai == True:
            #         context['bs_0020'] = '技能実習開始前に所属していた勤務先に復職る'
            #         context[
            #             'bs_0020_v'] = 'trở lại làm việc tại cơ quan phái cử phụ thuộc trước khi bắt đầu khóa thực tập kỹ năng.'
            #     elif thuctapsinh.donhang_tts.dudinh_khong_quaylai == True or thuctapsinh.donhang_tts.dudinh_khongco_ydinh == True:
            #         context['bs_0020'] = '送出し機関の協力を得て' + str(format_hoso.nganhnghe(
            #             hopdonglaodong.loai_nghe_nganhnghe_congviec.name_loaicongviec)) + 'の技能を活用できる業種への就職を目指'
            #         context['bs_0020_v'] = 'hướng tới việc làm có thể tận dụng được nghiệp vụ kỹ năng ' + str(
            #             format_hoso.kiemtra(
            #                 hopdonglaodong.loai_nghe_nganhnghe_tts_viet)) + ' cùng với sự hợp tác giúp đỡ của công ty phái cử.'
            #     else:
            #         context['bs_0020'] = '______________________'
            #         context['bs_0020_v'] = '______________________'

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_58(self, ma_thuctapsinh, document):
        print('58')
        if document.donhang_hoso.tencoquan_ngoai_1.ten_cquan_cbi_nuocngoai:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_58")], limit=1)
        else:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_58_1")], limit=1)

        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)

            context = {}
            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            if thuctapsinh.ten_han_tts:
                context['hhjp_0002'] = format_hoso.kiemtra(thuctapsinh.ten_han_tts)
            else:
                context['hhjp_0002'] = format_hoso.kiemtra(thuctapsinh.ten_katakana_tts)
            context['hhjp_0924'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_anh_phaicu)
            context['hhjp_0924_v'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_viet_phaicu)
            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            context['hhjp_0010_v'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_viet_xnghiep)
            context['hhjp_0097_v'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_viet_nghiepdoan)
            context['hhjp_0097'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_han_nghiepdoan)

            context['hhjp_04941'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_1)
            context['hhjp_0494_v1'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_v_1)
            if document.donhang_hoso.ngaythu_1:
                if document.donhang_hoso.du_kien_1:
                    context['hhjp_04951'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_1.day,
                                                                         document.donhang_hoso.ngaythu_1.month,
                                                                         document.donhang_hoso.ngaythu_1.year) + '（予定）'
                    context['hhjp_0495_v1'] = Listing(intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_1.day,
                                                                                   document.donhang_hoso.ngaythu_1.month,
                                                                                   document.donhang_hoso.ngaythu_1.year) + "\n" + '(Dự định)')
                else:
                    context['hhjp_04951'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_1.day,
                                                                         document.donhang_hoso.ngaythu_1.month,
                                                                         document.donhang_hoso.ngaythu_1.year)
                    context['hhjp_0495_v1'] = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_1.day,
                                                                           document.donhang_hoso.ngaythu_1.month,
                                                                           document.donhang_hoso.ngaythu_1.year)
            else:
                context['hhjp_04951'] = ' '
                context['hhjp_049_v1'] = ' '
            context['bs_0021'] = format_hoso.place_value(document.donhang_hoso.sotien_viet_1)
            context['hhjp_0491'] = format_hoso.place_value(document.donhang_hoso.sotien_nhat_1)

            context['hhjp_04942'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_2)
            context['hhjp_0494_v2'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_v_2)
            if document.donhang_hoso.ngaythu_2:
                if document.donhang_hoso.du_kien_2:
                    context['hhjp_04952'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_2.day,
                                                                         document.donhang_hoso.ngaythu_2.month,
                                                                         document.donhang_hoso.ngaythu_2.year) + '（予定）'
                    context['hhjp_0495_v2'] = Listing(intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_2.day,
                                                                                   document.donhang_hoso.ngaythu_2.month,
                                                                                   document.donhang_hoso.ngaythu_2.year) + '\n (Dự định)')
                else:
                    context['hhjp_04952'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_2.day,
                                                                         document.donhang_hoso.ngaythu_2.month,
                                                                         document.donhang_hoso.ngaythu_2.year)
                    context['hhjp_0495_v2'] = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_2.day,
                                                                           document.donhang_hoso.ngaythu_2.month,
                                                                           document.donhang_hoso.ngaythu_2.year)
            else:
                context['hhjp_04952'] = ' '
                context['hhjp_0495_v2'] = ' '

            context['bs_0022'] = format_hoso.place_value(document.donhang_hoso.sotien_viet_2)
            context['hhjp_0492'] = format_hoso.place_value(document.donhang_hoso.sotien_nhat_2)

            context['hhjp_04943'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_3)
            context['hhjp_0494_v3'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_v_3)
            if document.donhang_hoso.ngaythu_3:
                if document.donhang_hoso.du_kien_3:
                    context['hhjp_04953'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_3.day,
                                                                         document.donhang_hoso.ngaythu_3.month,
                                                                         document.donhang_hoso.ngaythu_3.year) + '（予定）'
                    context['hhjp_0495_v3'] = Listing(intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_3.day,
                                                                                   document.donhang_hoso.ngaythu_3.month,
                                                                                   document.donhang_hoso.ngaythu_3.year) + '\n (Dự định)')
                else:
                    context['hhjp_04953'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_3.day,
                                                                         document.donhang_hoso.ngaythu_3.month,
                                                                         document.donhang_hoso.ngaythu_3.year)
                    context['hhjp_0495_v3'] = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_3.day,
                                                                           document.donhang_hoso.ngaythu_3.month,
                                                                           document.donhang_hoso.ngaythu_3.year)
            else:
                context['hhjp_04953'] = ' '
                context['hhjp_0495_v3'] = ' '

            context['bs_0023'] = format_hoso.place_value(document.donhang_hoso.sotien_viet_3)
            context['hhjp_0493'] = format_hoso.place_value(document.donhang_hoso.sotien_nhat_3)

            context['hhjp_04944'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_4)
            context['hhjp_0494_v4'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_v_4)
            if document.donhang_hoso.ngaythu_4:
                if document.donhang_hoso.du_kien_4:
                    context['hhjp_04954'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_4.day,
                                                                         document.donhang_hoso.ngaythu_4.month,
                                                                         document.donhang_hoso.ngaythu_4.year) + '（予定）'
                    hhjp_0495_v4 = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_4.day,
                                                                document.donhang_hoso.ngaythu_4.month,
                                                                document.donhang_hoso.ngaythu_4.year)
                    context['hhjp_0495_v4'] = Listing(hhjp_0495_v4 + '\n (Dự định)')
                else:
                    context['hhjp_04954'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_4.day,
                                                                         document.donhang_hoso.ngaythu_4.month,
                                                                         document.donhang_hoso.ngaythu_4.year)
                    context['hhjp_0495_v4'] = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_4.day,
                                                                           document.donhang_hoso.ngaythu_4.month,
                                                                           document.donhang_hoso.ngaythu_4.year)
            else:
                context['hhjp_04954'] = ' '
                context['hhjp_0495_v4'] = ' '
            context['bs_0024'] = format_hoso.place_value(document.donhang_hoso.sotien_viet_4)
            context['hhjp_0494'] = format_hoso.place_value(document.donhang_hoso.sotien_nhat_4)

            context['hhjp_04945'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_5)
            context['hhjp_0494_v5'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_v_5)
            if document.donhang_hoso.ngaythu_5:
                if document.donhang_hoso.du_kien_5:
                    context['hhjp_04955'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_5.day,
                                                                         document.donhang_hoso.ngaythu_5.month,
                                                                         document.donhang_hoso.ngaythu_5.year) + '（予定）'
                    hhjp_0495_v5 = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_5.day,
                                                                document.donhang_hoso.ngaythu_5.month,
                                                                document.donhang_hoso.ngaythu_5.year)
                    context['hhjp_0495_v5'] = Listing(hhjp_0495_v5 + '\n (Dự định)')
                else:
                    context['hhjp_04955'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_5.day,
                                                                         document.donhang_hoso.ngaythu_5.month,
                                                                         document.donhang_hoso.ngaythu_5.year)
                    context['hhjp_0495_v5'] = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_5.day,
                                                                           document.donhang_hoso.ngaythu_5.month,
                                                                           document.donhang_hoso.ngaythu_5.year)
            else:
                context['hhjp_04955'] = ' '
                context['hhjp_0495_v5'] = ' '
            context['bs_0025'] = format_hoso.place_value(document.donhang_hoso.sotien_viet_5)
            context['hhjp_0495'] = format_hoso.place_value(document.donhang_hoso.sotien_nhat_5)

            context['bs_261'] = format_hoso.place_value(document.donhang_hoso.tong_sotien_v)
            context['hhjp_0490'] = format_hoso.place_value(document.donhang_hoso.tong_sotien_n)
            # ----------------------------------------------------------------------------------------------
            context['hhjp_0585_1'] = format_hoso.kiemtra(
                document.donhang_hoso.tencoquan_ngoai_1.ten_cquan_cbi_nuocngoai)
            context['hhjp_0585_1_n'] = format_hoso.kiemtra(
                document.donhang_hoso.tencoquan_ngoai_1.ten_cquan_cbi_nuocngoai_n)
            context['tnc_0006_1'] = format_hoso.kiemtra(document.donhang_hoso.vaitro_phaicu_ngoai_v_1)
            context['tnc_0006_1_n'] = format_hoso.kiemtra(document.donhang_hoso.vaitro_phaicu_ngoai_1)
            context['tnc_0007_1'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_ngoai_1)
            context['tnc_0007_v1'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_ngoai_v_1)
            if document.donhang_hoso.ngaythu_ngoai_1:
                context['tnc_0008_1'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_ngoai_1.day,
                                                                     document.donhang_hoso.ngaythu_ngoai_1.month,
                                                                     document.donhang_hoso.ngaythu_ngoai_1.year)
                context['tnc_0008_v1'] = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_ngoai_1.day,
                                                                      document.donhang_hoso.ngaythu_ngoai_1.month,
                                                                      document.donhang_hoso.ngaythu_ngoai_1.year)
            else:
                context['tnc_0008_1'] = ' '
                context['tnc_0008_v1'] = ' '

            context['tnc_0009_1'] = format_hoso.place_value(document.donhang_hoso.sotien_viet_ngoai_1)
            context['tnc_0010_1'] = format_hoso.place_value(document.donhang_hoso.sotien_nhat_ngoai_1)

            context['hhjp_0585_2'] = format_hoso.kiemtra(
                document.donhang_hoso.tencoquan_ngoai_2.ten_cquan_cbi_nuocngoai)
            context['hhjp_0585_2_n'] = format_hoso.kiemtra(
                document.donhang_hoso.tencoquan_ngoai_2.ten_cquan_cbi_nuocngoai_n)
            context['tnc_0006_2'] = format_hoso.kiemtra(document.donhang_hoso.vaitro_phaicu_ngoai_v_2)
            context['tnc_0006_2_n'] = format_hoso.kiemtra(document.donhang_hoso.vaitro_phaicu_ngoai_2)
            context['tnc_0007_2'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_ngoai_2)
            context['tnc_0007_v2'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_ngoai_v_2)
            if document.donhang_hoso.ngaythu_ngoai_2:
                context['tnc_0008_2'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_ngoai_2.day,
                                                                     document.donhang_hoso.ngaythu_ngoai_2.month,
                                                                     document.donhang_hoso.ngaythu_ngoai_2.year)
                context['tnc_0008_v2'] = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_ngoai_2.day,
                                                                      document.donhang_hoso.ngaythu_ngoai_2.month,
                                                                      document.donhang_hoso.ngaythu_ngoai_2.year)
            else:
                context['tnc_0008_2'] = ' '
                context['tnc_0008_v2'] = ' '

            context['tnc_0009_2'] = format_hoso.place_value(document.donhang_hoso.sotien_viet_ngoai_2)
            context['tnc_0010_2'] = format_hoso.place_value(document.donhang_hoso.sotien_nhat_ngoai_2)

            context['hhjp_0585_3'] = format_hoso.kiemtra(
                document.donhang_hoso.tencoquan_ngoai_3.ten_cquan_cbi_nuocngoai)
            context['hhjp_0585_3_n'] = format_hoso.kiemtra(
                document.donhang_hoso.tencoquan_ngoai_3.ten_cquan_cbi_nuocngoai_n)
            context['tnc_0006_3'] = format_hoso.kiemtra(document.donhang_hoso.vaitro_phaicu_ngoai_v_3)
            context['tnc_0006_3_n'] = format_hoso.kiemtra(document.donhang_hoso.vaitro_phaicu_ngoai_3)
            context['tnc_0007_3'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_ngoai_3)
            context['tnc_0007_v3'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_ngoai_v_3)
            if document.donhang_hoso.ngaythu_ngoai_3:
                context['tnc_0008_3'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_ngoai_3.day,
                                                                     document.donhang_hoso.ngaythu_ngoai_3.month,
                                                                     document.donhang_hoso.ngaythu_ngoai_3.year)
                context['tnc_0008_v3'] = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_ngoai_3.day,
                                                                      document.donhang_hoso.ngaythu_ngoai_3.month,
                                                                      document.donhang_hoso.ngaythu_ngoai_3.year)
            else:
                context['tnc_0008_3'] = ' '
                context['tnc_0008_v3'] = ' '

            context['tnc_0009_3'] = format_hoso.place_value(document.donhang_hoso.sotien_viet_ngoai_3)
            context['tnc_0010_3'] = format_hoso.place_value(document.donhang_hoso.sotien_nhat_ngoai_3)

            context['hhjp_0585_4'] = format_hoso.kiemtra(
                document.donhang_hoso.tencoquan_ngoai_4.ten_cquan_cbi_nuocngoai)
            context['hhjp_0585_4_n'] = format_hoso.kiemtra(
                document.donhang_hoso.tencoquan_ngoai_4.ten_cquan_cbi_nuocngoai_n)
            context['tnc_0006_4'] = format_hoso.kiemtra(document.donhang_hoso.vaitro_phaicu_ngoai_v_4)
            context['tnc_0006_4_n'] = format_hoso.kiemtra(document.donhang_hoso.vaitro_phaicu_ngoai_4)
            context['tnc_0007_4'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_ngoai_4)
            context['tnc_0007_v4'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_ngoai_v_4)
            if document.donhang_hoso.ngaythu_ngoai_4:
                context['tnc_0008_4'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_ngoai_4.day,
                                                                     document.donhang_hoso.ngaythu_ngoai_4.month,
                                                                     document.donhang_hoso.ngaythu_ngoai_4.year)
                context['tnc_0008_v4'] = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_ngoai_4.day,
                                                                      document.donhang_hoso.ngaythu_ngoai_4.month,
                                                                      document.donhang_hoso.ngaythu_ngoai_4.year)
            else:
                context['tnc_0008_4'] = ' '
                context['tnc_0008_v4'] = ' '

            context['tnc_0009_4'] = format_hoso.place_value(document.donhang_hoso.sotien_viet_ngoai_4)
            context['tnc_0010_4'] = format_hoso.place_value(document.donhang_hoso.sotien_nhat_ngoai_4)

            context['hhjp_0585_5'] = format_hoso.kiemtra(
                document.donhang_hoso.tencoquan_ngoai_5.ten_cquan_cbi_nuocngoai)
            context['hhjp_0585_5_n'] = format_hoso.kiemtra(
                document.donhang_hoso.tencoquan_ngoai_5.ten_cquan_cbi_nuocngoai_n)
            context['tnc_0006_5'] = format_hoso.kiemtra(document.donhang_hoso.vaitro_phaicu_ngoai_v_5)
            context['tnc_0006_5_n'] = format_hoso.kiemtra(document.donhang_hoso.vaitro_phaicu_ngoai_5)
            context['tnc_0007_5'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_ngoai_5)
            context['tnc_0007_v5'] = format_hoso.kiemtra(document.donhang_hoso.loaiphi_ngoai_v_5)
            if document.donhang_hoso.ngaythu_ngoai_5:
                context['tnc_0008_5'] = intern_utils.date_time_in_jp(document.donhang_hoso.ngaythu_ngoai_5.day,
                                                                     document.donhang_hoso.ngaythu_ngoai_5.month,
                                                                     document.donhang_hoso.ngaythu_ngoai_5.year)
                context['tnc_0008_v5'] = intern_utils.date_time_in_vn(document.donhang_hoso.ngaythu_ngoai_5.day,
                                                                      document.donhang_hoso.ngaythu_ngoai_5.month,
                                                                      document.donhang_hoso.ngaythu_ngoai_5.year)
            else:
                context['tnc_0008_5'] = ' '
                context['tnc_0008_v5'] = ' '

            context['tnc_0009_5'] = format_hoso.place_value(document.donhang_hoso.sotien_viet_ngoai_5)
            context['tnc_0010_5'] = format_hoso.place_value(document.donhang_hoso.sotien_nhat_ngoai_5)

            context['tnc_0011'] = format_hoso.place_value(document.donhang_hoso.tong_ngoai_v)
            context['tnc_0012'] = format_hoso.place_value(document.donhang_hoso.tong_ngoai_n)

            context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban1)
            if document.ngay_lapvanban1 != False:
                context['hhjp_0193_v'] = ' Ngày ' + str(document.ngay_lapvanban1.day) + \
                                         ' tháng ' + str(document.ngay_lapvanban1.month) + \
                                         ' năm ' + str(document.ngay_lapvanban1.year)
            else:
                context['hhjp_0193_v'] = 'Ngày tháng năm'
            context[
                'hhjp_0376'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_nguoidaidien)
            context[
                'hhjp_0376_cv'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.chucvu_han_phaicu)
            context[
                'hhjp_0510'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.daidien_latinh_phaicu)
            context[
                'hhjp_0510_cv'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.chucvu_latinh_phaicu)
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_59(self, ma_thuctapsinh, document):
        print('59')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_59")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            context = {}

            context['hhjp_0790'] = format_hoso.convert_date(document.donhang_hoso.date_create_letter_promotion)
            context['hhjp_0791'] = format_hoso.kiemtra(document.donhang_hoso.coquan_nhanuoc_n)
            context['hhjp_0792_cv'] = format_hoso.kiemtra(document.donhang_hoso.position_person_sign_jp)
            context['hhjp_0792'] = format_hoso.kiemtra(document.donhang_hoso.person_sign_proletter_n)

            context['hhjp_7198'] = format_hoso.kiemtra(document.donhang_hoso.year_expire)
            context['hhjp_0924'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_anh_phaicu)
            context['hhjp_0500'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.sogiayphep_phaicu)
            context['hhjp_0376'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_nguoidaidien)
            context['hhjp_0511'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.diachi_tienganh)
            context['hhjp_0501'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_nguoidaidien)
            context['hhjp_0502'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.diachi_tienganh)
            if document.donhang_hoso.congtyphaicu_donhang.sdt_phaicu and document.donhang_hoso.congtyphaicu_donhang.fax_phaicu:
                context['hhjp_0503'] = '%s / %s' % (
                    format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.sdt_phaicu).strip(),
                    format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.fax_phaicu).strip())
            elif document.donhang_hoso.congtyphaicu_donhang.sdt_phaicu:
                context['hhjp_0503'] = format_hoso.kiemtra(
                    document.donhang_hoso.congtyphaicu_donhang.sdt_phaicu).strip()
            elif document.donhang_hoso.congtyphaicu_donhang.fax_phaicu:
                context['hhjp_0503'] = format_hoso.kiemtra(
                    document.donhang_hoso.congtyphaicu_donhang.fax_phaicu).strip()
            else:
                context['hhjp_0503'] = ''

            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            context['hhjp_0097'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_han_nghiepdoan)
            context['hhjp_0923'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_viet_nghiepdoan)
            context['hhjp_0100'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.so_giayphep_nghiepdoan)
            context['hhjp_0103'] = format_hoso.kiemtra(
                document.donhang_hoso.nghiepdoan_donhang.ten_daidien_han_nghiepdoan)
            context['hhjp_0103_a'] = format_hoso.kiemtra(
                document.donhang_hoso.nghiepdoan_donhang.ten_daidien_anh_nghiepdoan)
            context['hhjp_0098'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.diachi_nghiepdoan)
            context['hhjp_0098_a'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.diachi_nghiepdoan_anh)
            if document.donhang_hoso.nghiepdoan_donhang.dienthoai_nghiepdoan and document.donhang_hoso.nghiepdoan_donhang.fax_nghiepdoan:
                context['hhjp_0099'] = '%s / %s' % (
                    format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.dienthoai_nghiepdoan),
                    format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.fax_nghiepdoan))
            elif document.donhang_hoso.nghiepdoan_donhang.dienthoai_nghiepdoan:
                context['hhjp_0099'] = format_hoso.kiemtra(
                    document.donhang_hoso.nghiepdoan_donhang.dienthoai_nghiepdoan)
            elif document.donhang_hoso.nghiepdoan_donhang.fax_nghiepdoan:
                context['hhjp_0099'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.fax_nghiepdoan)
            else:
                context['hhjp_0099'] = ''

            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            context['hhjp_0014'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep)
            context['hhjp_0014_a'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep_v)
            context['hhjp_0010_v'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_viet_xnghiep)
            context['hhjp_0011'] = format_hoso.kiemtra(document.xinghiep_hoso.diachi_han_xnghiep)
            context['hhjp_0011_a'] = format_hoso.kiemtra(document.xinghiep_hoso.diachi_viet_xnghiep)
            if document.xinghiep_hoso.sdt_xnghiep and document.xinghiep_hoso.fax:
                context['hhjp_0012'] = '%s / %s' % (format_hoso.kiemtra(document.xinghiep_hoso.sdt_xnghiep).strip(),
                                                    format_hoso.kiemtra(document.xinghiep_hoso.fax).strip())
            elif document.xinghiep_hoso.sdt_xnghiep:
                context['hhjp_0012'] = format_hoso.kiemtra(document.xinghiep_hoso.sdt_xnghiep).strip()
            elif document.xinghiep_hoso.fax:
                context['hhjp_0012'] = format_hoso.kiemtra(document.xinghiep_hoso.fax).strip()
            else:
                context['hhjp_0012'] = ''

            if document.ngay_lapvanban1:
                context['hhjp_0193'] = intern_utils.date_time_in_jp(document.ngay_lapvanban1.day,
                                                                    document.ngay_lapvanban1.month,
                                                                    document.ngay_lapvanban1.year)
            else:
                context['hhjp_0193'] = '年　月　日'
            table_tts = []
            for thuctapsinh_hoso in document.thuctapsinh_hoso:
                infor = {}
                infor['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh_hoso.ten_lt_tts)
                infor['hhjp_0004'] = format_hoso.convert_date(thuctapsinh_hoso.ngaysinh_tts)
                if thuctapsinh.gtinh_tts == 'Nam':
                    infor['hhjp_0006'] = '男'
                else:
                    infor['hhjp_0006'] = '女'
                infor['hhjp_0345'] = format_hoso.nganhnghe(thuctapsinh_hoso.nganhnghe_tts.name_loaicongviec)
                infor['hhjp_0902_Y'] = format_hoso.kiemtra(thuctapsinh_hoso.donhang_tts.thoigian_donhang.year)
                infor['hhjp_0902_M'] = format_hoso.kiemtra(thuctapsinh_hoso.donhang_tts.thoigian_donhang.month)
                table_tts.append(infor)
            context['tbl_danhsachtts'] = table_tts

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_59_1(self, ma_thuctapsinh, document):
        print('59_1')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_59_1")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            context = {}
            if document.donhang_hoso.date_create_letter_promotion:
                context['hhjp_0790_a'] = str(
                    format_hoso.kiemtra(document.donhang_hoso.date_create_letter_promotion.day)) + '/' + \
                                         str(format_hoso.kiemtra(
                                             document.donhang_hoso.date_create_letter_promotion.month)) + '/' + \
                                         str(format_hoso.kiemtra(
                                             document.donhang_hoso.date_create_letter_promotion.year))
            else:
                context['hhjp_0790_a'] = ''
            context['hhjp_0791_a'] = format_hoso.kiemtra(document.donhang_hoso.coquan_nhanuoc)
            context['hhjp_0792_cv_a'] = format_hoso.kiemtra(document.donhang_hoso.position_person_sign)
            context['hhjp_0792_a'] = format_hoso.kiemtra(document.donhang_hoso.person_sign_proletter)

            context['hhjp_7198'] = format_hoso.kiemtra(document.donhang_hoso.year_expire)
            context['hhjp_0924'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_anh_phaicu)
            context['hhjp_0500'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.sogiayphep_phaicu)
            context['hhjp_0376'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_nguoidaidien)
            context['hhjp_0511'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.diachi_tienganh)
            context['hhjp_0501'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_nguoidaidien)
            context['hhjp_0502'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.diachi_tienganh)
            if document.donhang_hoso.congtyphaicu_donhang.sdt_phaicu and document.donhang_hoso.congtyphaicu_donhang.fax_phaicu:
                context['hhjp_0503'] = '%s / %s' % (
                    format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.sdt_phaicu).strip(),
                    format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.fax_phaicu).strip())
            elif document.donhang_hoso.congtyphaicu_donhang.sdt_phaicu:
                context['hhjp_0503'] = format_hoso.kiemtra(
                    document.donhang_hoso.congtyphaicu_donhang.sdt_phaicu).strip()
            elif document.donhang_hoso.congtyphaicu_donhang.fax_phaicu:
                context['hhjp_0503'] = format_hoso.kiemtra(
                    document.donhang_hoso.congtyphaicu_donhang.fax_phaicu).strip()
            else:
                context['hhjp_0503'] = ''

            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            context['hhjp_0097'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_han_nghiepdoan)
            context['hhjp_0923'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_viet_nghiepdoan)
            context['hhjp_0100'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.so_giayphep_nghiepdoan)
            context['hhjp_0103'] = format_hoso.kiemtra(
                document.donhang_hoso.nghiepdoan_donhang.ten_daidien_han_nghiepdoan)
            context['hhjp_0103_a'] = format_hoso.kiemtra(
                document.donhang_hoso.nghiepdoan_donhang.ten_daidien_anh_nghiepdoan)
            context['hhjp_0098'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.diachi_nghiepdoan)
            context['hhjp_0098_a'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.diachi_nghiepdoan_anh)
            if document.donhang_hoso.nghiepdoan_donhang.dienthoai_nghiepdoan and document.donhang_hoso.nghiepdoan_donhang.fax_nghiepdoan:
                context['hhjp_0099'] = '%s / %s' % (
                    format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.dienthoai_nghiepdoan),
                    format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.fax_nghiepdoan))
            elif document.donhang_hoso.nghiepdoan_donhang.dienthoai_nghiepdoan:
                context['hhjp_0099'] = format_hoso.kiemtra(
                    document.donhang_hoso.nghiepdoan_donhang.dienthoai_nghiepdoan)
            elif document.donhang_hoso.nghiepdoan_donhang.fax_nghiepdoan:
                context['hhjp_0099'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.fax_nghiepdoan)
            else:
                context['hhjp_0099'] = ''

            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            context['hhjp_0014'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep)
            context['hhjp_0014_a'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep_v)
            context['hhjp_0010_v'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_viet_xnghiep)
            context['hhjp_0011'] = format_hoso.kiemtra(document.xinghiep_hoso.diachi_han_xnghiep)
            context['hhjp_0011_a'] = format_hoso.kiemtra(document.xinghiep_hoso.diachi_viet_xnghiep)
            if document.xinghiep_hoso.sdt_xnghiep and document.xinghiep_hoso.fax:
                context['hhjp_0012'] = '%s / %s' % (format_hoso.kiemtra(document.xinghiep_hoso.sdt_xnghiep).strip(),
                                                    format_hoso.kiemtra(document.xinghiep_hoso.fax).strip())
            elif document.xinghiep_hoso.sdt_xnghiep:
                context['hhjp_0012'] = format_hoso.kiemtra(document.xinghiep_hoso.sdt_xnghiep).strip()
            elif document.xinghiep_hoso.fax:
                context['hhjp_0012'] = format_hoso.kiemtra(document.xinghiep_hoso.fax).strip()
            else:
                context['hhjp_0012'] = ''

            if document.ngay_lapvanban1:
                context['hhjp_0193'] = intern_utils.date_time_in_jp(document.ngay_lapvanban1.day,
                                                                    document.ngay_lapvanban1.month,
                                                                    document.ngay_lapvanban1.year)
            else:
                context['hhjp_0193'] = '年　月　日'

            table_tts1 = []
            for thuctapsinh_hoso1 in document.thuctapsinh_hoso:
                infortts = {}
                infortts['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh_hoso1.ten_lt_tts)
                if thuctapsinh_hoso1.ngaysinh_tts:
                    infortts['hhjp_0004_a'] = str(format_hoso.kiemtra(thuctapsinh_hoso1.ngaysinh_tts.day)) + '/' + \
                                              str(format_hoso.kiemtra(thuctapsinh_hoso1.ngaysinh_tts.month)) + '/' + \
                                              str(format_hoso.kiemtra(thuctapsinh_hoso1.ngaysinh_tts.year))
                else:
                    infortts['hhjp_0004_a'] = ''
                if thuctapsinh.gtinh_tts == 'Nam':
                    infortts['hhjp_0006_a'] = 'MALE'
                else:
                    infortts['hhjp_0006_a'] = 'FEMALE'
                infortts['hhjp_0345_a'] = format_hoso.kiemtra(thuctapsinh_hoso1.nganhnghe_tts_a)
                infortts['hhjp_0902_Y'] = format_hoso.kiemtra(thuctapsinh_hoso1.donhang_tts.thoigian_donhang.year)
                infortts['hhjp_0902_M'] = format_hoso.kiemtra(thuctapsinh_hoso1.donhang_tts.thoigian_donhang.month)
                table_tts1.append(infortts)
            context['tbl_intern'] = table_tts1

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_dsld(self, ma_thuctapsinh, document):
        print('DSLD')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "DSLD")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            tbl_tts1 = []
            for danhsachlaodong in document.thuctapsinh_hoso:
                if danhsachlaodong.diachi_tts_v:
                    x = danhsachlaodong.diachi_tts_v.split(",")
                else:
                    x = ''
                infor = {}

                infor['HHJP_0001_TV'] = format_hoso.kiemtra(danhsachlaodong.ten_tv_tts).upper()
                if danhsachlaodong.ngaysinh_tts:
                    infor['hhjp_0004_tv'] = str(format_hoso.kiemtra(danhsachlaodong.ngaysinh_tts.day)) + '/' + \
                                            str(format_hoso.kiemtra(danhsachlaodong.ngaysinh_tts.month)) + '/' + \
                                            str(format_hoso.kiemtra(danhsachlaodong.ngaysinh_tts.year))
                else:
                    infor['hhjp_0004_tv'] = ''
                if danhsachlaodong.gtinh_tts == 'Nam':
                    infor['hhjp_0006_tv'] = 'Nam'
                else:
                    infor['hhjp_0006_tv'] = 'Nữ'

                infor['hhjp_0345_a'] = format_hoso.kiemtra(danhsachlaodong.nganhnghe_tts_a)
                if danhsachlaodong.donhang_tts.thoigian_donhang:
                    infor['hhjp_0902_a'] = str(
                        format_hoso.kiemtra(danhsachlaodong.donhang_tts.thoigian_donhang.month)) + '/' + \
                                           str(format_hoso.kiemtra(danhsachlaodong.donhang_tts.thoigian_donhang.year))
                else:
                    infor['hhjp_0902_a'] = ''
                if x:
                    infor['hhjp_0331_t'] = format_hoso.kiemtra(x[2])
                    infor['hhjp_0331_h'] = format_hoso.kiemtra(x[1])
                    infor['hhjp_0331_x'] = format_hoso.kiemtra(x[0])
                else:
                    infor['hhjp_0331_t'] = ''
                    infor['hhjp_0331_h'] = ''
                    infor['hhjp_0331_x'] = ''
                infor['hhjp_0515'] = format_hoso.kiemtra(danhsachlaodong.identity)
                infor['hhjp_0515_sdt'] = format_hoso.kiemtra(danhsachlaodong.sdt_tts)

                tbl_tts1.append(infor)
            context['tbl_tts'] = tbl_tts1

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_60(self, ma_thuctapsinh, document):
        print('60')

        sothuctapsinh = int(len(document.thuctapsinh_hoso))

        if sothuctapsinh <= 3:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_60")], limit=1)
        elif sothuctapsinh > 3:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_60_1")], limit=1)

        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))

            docdata = DocxTemplate(stream)

            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)

            context = {}
            if sothuctapsinh <= 3:
                item = 1
                for thuctapsinh_hoso in document.thuctapsinh_hoso:
                    if item == 1:
                        context['hhjp_0001_1'] = format_hoso.kiemtra(thuctapsinh_hoso.ten_lt_tts)
                        item += 1
                    elif item == 2:
                        context['hhjp_0001_2'] = format_hoso.kiemtra(thuctapsinh_hoso.ten_lt_tts)
                        item += 1
                    elif item == 3:
                        context['hhjp_0001_3'] = format_hoso.kiemtra(thuctapsinh_hoso.ten_lt_tts)
                        break
            elif sothuctapsinh > 3:
                table_thuctapsinh = []
                for thuctapsinh_hoso in document.thuctapsinh_hoso:
                    infor_thuctapsinh = {}
                    infor_thuctapsinh['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh_hoso.ten_lt_tts)
                    table_thuctapsinh.append(infor_thuctapsinh)

                context['tbl_tts'] = table_thuctapsinh

            if thuctapsinh.noidung_xacminh_a_donhang_xinghiep == True:
                context['hhjp_tich_A'] = '■' if thuctapsinh.noidung_xacminh_a_donhang_xinghiep else '□'
            else:
                context['hhjp_tich_A'] = '□'

            context['hhjp_tich_B'] = '■' if thuctapsinh.noidung_xacminh_b_donhang_xinghiep else '□'

            if thuctapsinh.noidung_xacminh_b_donhang_xinghiep == True:
                context['hhjp_tich_a'] = '■' if thuctapsinh.noidung_xacminh_b_a_donhang_xinghiep else '□'
                context['hhjp_tich_b'] = '■' if thuctapsinh.noidung_xacminh_b_b_donhang_xinghiep else '□'
                context['hhjp_tich_c'] = '■' if thuctapsinh.noidung_xacminh_b_c_donhang_xinghiep else '□'
            else:
                context['hhjp_tich_a'] = '□'
                context['hhjp_tich_b'] = '□'
                context['hhjp_tich_c'] = '□'

            context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban1)
            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            context['hhjp_0014'] = format_hoso.kiemtra(document.xinghiep_hoso.daidien_han_xnghiep)
            context['xn_104'] = format_hoso.kiemtra(document.xinghiep_hoso.chucvu_daidien_han)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_61(self, ma_thuctapsinh, document):
        thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
        thuctapsinh_danhsach = self.env['thuctapsinh.thuctapsinh'].search(
            [('donhang_tts', '=', thuctapsinh.donhang_tts.id)])
        hopdonglaodong = self.env['hopdong.hopdong'].search(
            [('thuctapsinh_hopdong', '=', thuctapsinh.id), ('donhang_hopdong', '=', document.donhang_hoso.id),
             ('giaidoan_hopdong', '=', document.giaidoan_hoso)],
            limit=1)
        if len(thuctapsinh_danhsach) < 4:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_61")], limit=1)
            print(61)
        else:
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_61_1")], limit=1)
            print(61_1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)

            context = {}

            item = 1
            for tts in thuctapsinh_danhsach:
                if item == 1:
                    context['hhjp_0001'] = format_hoso.kiemtra(tts.ten_lt_tts)
                    if thuctapsinh.ten_han_tts:
                        context['hhjp_0001_h'] = format_hoso.kiemtra(tts.ten_han_tts)
                    else:
                        context['hhjp_0001_h'] = format_hoso.kiemtra(tts.ten_katakana_tts)
                    item += 1
                elif item == 2:
                    context['hhjp_0001_1'] = format_hoso.kiemtra(tts.ten_lt_tts)
                    if thuctapsinh.ten_han_tts:
                        context['hhjp_0001_h_1'] = format_hoso.kiemtra(tts.ten_han_tts)
                    else:
                        context['hhjp_0001_h_1'] = format_hoso.kiemtra(tts.ten_katakana_tts)
                    item += 1
                elif item == 3:
                    context['hhjp_0001_2'] = format_hoso.kiemtra(tts.ten_lt_tts)
                    if thuctapsinh.ten_han_tts:
                        context['hhjp_0001_h_2'] = format_hoso.kiemtra(tts.ten_han_tts)
                    else:
                        context['hhjp_0001_h_2'] = format_hoso.kiemtra(tts.ten_katakana_tts)
                    break

            context['hhjp_0193'] = format_hoso.convert_date(document.ngay_lapvanban1)

            context['hhjp_0204'] = format_hoso.kiemtra(thuctapsinh.donhang_tts.congtypc2.name)
            context['hhjp_0900'] = format_hoso.kiemtra(thuctapsinh.donhang_tts.bophan)
            context['hhjp_0901'] = format_hoso.nganhnghe(thuctapsinh.donhang_tts.nganhnghe_xnpc.name_loaicongviec)

            context['hhjp_0207'] = format_hoso.kiemtra(thuctapsinh.donhang_tts.congtypc2.position_person_sign_n)
            context['hhjp_0207_1'] = format_hoso.kiemtra(thuctapsinh.donhang_tts.congtypc2.director_n)
            if len(thuctapsinh_danhsach) > 3:
                table_tts = []
                for thuctapsinh_danhsach in thuctapsinh_danhsach:
                    infor_tts = {}
                    infor_tts['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh_danhsach.ten_lt_tts)
                    infor_tts['hhjp_0901'] = format_hoso.nganhnghe(
                        thuctapsinh_danhsach.donhang_tts.nganhnghe_xnpc.name_loaicongviec)
                    if thuctapsinh_danhsach.ten_han_tts:
                        infor_tts['hhjp_0001_h'] = format_hoso.kiemtra(thuctapsinh_danhsach.ten_han_tts)
                    else:
                        infor_tts['hhjp_0001_h'] = format_hoso.kiemtra(thuctapsinh_danhsach.ten_katakana_tts)
                    table_tts.append(infor_tts)
                context['tbl_tts'] = table_tts

            if thuctapsinh.donhang_tts.quatrinh_xnpc:
                context['hhjp_0332_1'] = '■'
                context['hhjp_0332_2'] = '□'
                context['hhjp_0332_3'] = '□'
                context['bs_0010'] = ''
                context['bs_0011'] = format_hoso.kiemtra(thuctapsinh.donhang_tts.quatrinh_xnpc_lydo)
            elif thuctapsinh.donhang_tts.quatrinh_nguyenvong:
                context['hhjp_0332_1'] = '□'
                context['hhjp_0332_2'] = '■'
                context['hhjp_0332_3'] = '□'
                context['bs_0010'] = ''
                context['bs_0011'] = ''
            elif thuctapsinh.donhang_tts.quatrinh_khac:
                context['hhjp_0332_1'] = '□'
                context['hhjp_0332_2'] = '□'
                context['hhjp_0332_3'] = '■'
                context['bs_0010'] = format_hoso.kiemtra(thuctapsinh.donhang_tts.quatrinh_khac_lydo)
                context['bs_0011'] = ''
            else:
                context['hhjp_0332_3'] = '□'
                context['hhjp_0332_1'] = '□'
                context['hhjp_0332_2'] = '□'
                context['bs_0011'] = ''
                context['bs_0010'] = ''

            if thuctapsinh.donhang_tts.mqh_voi_tts:
                context['hhjp_0333_1'] = '■'
                context['hhjp_0333_2'] = '□'
                context['hhjp_0333_3'] = '□'
            elif thuctapsinh.donhang_tts.mqh_tuthoiviec:
                context['hhjp_0333_1'] = '□'
                context['hhjp_0333_2'] = '■'
                context['hhjp_0333_3'] = '□'
            elif thuctapsinh.donhang_tts.mqh_khac:
                context['hhjp_0333_1'] = '□'
                context['hhjp_0333_2'] = '□'
                context['hhjp_0333_3'] = '■'
                context['bs_0010_a'] = format_hoso.kiemtra(thuctapsinh.donhang_tts.mqh_khac_lydo)
            else:
                context['hhjp_0333_3'] = '□'
                context['hhjp_0333_1'] = '□'
                context['hhjp_0333_2'] = '□'
                context['bs_0010_a'] = ''

            if thuctapsinh.donhang_tts.dudinh_quaylai:
                context['hhjp_0334_1'] = '■'
                context['hhjp_0334_2'] = '□'
                context['hhjp_0334_3'] = '□'
                context['hhjp_0204_kt'] = format_hoso.kiemtra(thuctapsinh.donhang_tts.dudinh_quaylai_tencty.name)
                context['hhjp_0900_kt'] = format_hoso.kiemtra(thuctapsinh.donhang_tts.dudinh_quaylai_bophan)
                context['hhjp_0901_kt'] = format_hoso.nganhnghe(
                    thuctapsinh.donhang_tts.dudinh_quaylai_cv.name_loaicongviec)
            elif thuctapsinh.donhang_tts.dudinh_khong_quaylai:
                context['hhjp_0334_1'] = '□'
                context['hhjp_0334_2'] = '■'
                context['hhjp_0334_3'] = '□'
                context['hhjp_0204_kt'] = ''
                context['hhjp_0900_kt'] = ''
                context['hhjp_0901_kt'] = ''
            elif thuctapsinh.donhang_tts.dudinh_khongco_ydinh:
                context['hhjp_0334_1'] = '□'
                context['hhjp_0334_2'] = '□'
                context['hhjp_0334_3'] = '■'
                context['hhjp_0204_kt'] = ''
                context['hhjp_0900_kt'] = ''
                context['hhjp_0901_kt'] = ''
            else:
                context['hhjp_0334_3'] = '□'
                context['hhjp_0334_1'] = '□'
                context['hhjp_0334_2'] = '□'
                context['hhjp_0204_kt'] = ''
                context['hhjp_0900_kt'] = ''
                context['hhjp_0901_kt'] = ''

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_62(self, document):
        print('62')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_62")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)

            context = {}
            daotaotruoc = self.env['daotao.daotaotruoc'].search(
                [('donhang_daotaotruoc', '=', document.donhang_hoso.id)], limit=1)
            context['ahhjp_0793_1'] = Listing(format_hoso.kiemtra(daotaotruoc.noidung_1))
            context['ahhjp_0794_1'] = Listing('%s \n %s' % (
                format_hoso.kiemtra(daotaotruoc.tencoquan_diachi_1.ten_han_nghiepdoan),
                format_hoso.kiemtra(daotaotruoc.diachi_coquan_1)))
            if daotaotruoc.uythac_1 == 'Có':
                docdata.remove_shape(u'id="15" name="Oval 15"')
            elif daotaotruoc.uythac_1 == 'Không':
                docdata.remove_shape(u'id="14" name="Oval 14"')
            else:
                docdata.remove_shape(u'id="15" name="Oval 15"')
                docdata.remove_shape(u'id="14" name="Oval 14"')
            context['ahhjp_0796_1'] = Listing(
                '%s \n %s' % (format_hoso.kiemtra(daotaotruoc.tencoso_diachi_1.ten_coso_jp),
                              format_hoso.kiemtra(daotaotruoc.diachi_coso_1)))
            context['ahhjp_0797_1'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_batdau_1)
            context['ahhjp_0798_1'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_ketthuc_1)
            context['ahhjp_0799_1'] = format_hoso.kiemtra(daotaotruoc.sogio_1)
            context['bhhjp_0793_2'] = Listing(format_hoso.kiemtra(daotaotruoc.noidung_2))
            context['bhhjp_0794_2'] = Listing('%s \n %s' % (
                format_hoso.kiemtra(daotaotruoc.tencoquan_diachi_2.ten_han_nghiepdoan),
                format_hoso.kiemtra(daotaotruoc.diachi_coquan_2)))
            if daotaotruoc.uythac_2 == 'Có':
                docdata.remove_shape(u'id="3" name="Oval 3"')
            elif daotaotruoc.uythac_2 == 'Không':
                docdata.remove_shape(u'id="2" name="Oval 2"')
            else:
                docdata.remove_shape(u'id="3" name="Oval 3"')
                docdata.remove_shape(u'id="2" name="Oval 2"')
            context['bhhjp_0796_2'] = Listing(
                '%s \n %s' % (format_hoso.kiemtra(daotaotruoc.tencoso_diachi_2.ten_coso_jp),
                              format_hoso.kiemtra(daotaotruoc.diachi_coso_2)))
            context['bhhjp_0797_2'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_batdau_2)
            context['bhhjp_0798_2'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_ketthuc_2)
            context['bhhjp_0799_2'] = format_hoso.kiemtra(daotaotruoc.sogio_2)
            context['chhjp_0793_3'] = Listing(format_hoso.kiemtra(daotaotruoc.noidung_3))
            context['chhjp_0794_3'] = Listing('%s \n %s' % (
                format_hoso.kiemtra(daotaotruoc.tencoquan_diachi_3.ten_han_nghiepdoan),
                format_hoso.kiemtra(daotaotruoc.diachi_coquan_3)))
            if daotaotruoc.uythac_3 == 'Có':
                docdata.remove_shape(u'id="5" name="Oval 5"')
            elif daotaotruoc.uythac_3 == 'Không':
                docdata.remove_shape(u'id="4" name="Oval 4"')
            else:
                docdata.remove_shape(u'id="5" name="Oval 5"')
                docdata.remove_shape(u'id="4" name="Oval 4"')

            context['chhjp_0796_3'] = Listing(
                '%s \n %s' % (format_hoso.kiemtra(daotaotruoc.tencoso_diachi_3.ten_coso_jp),
                              format_hoso.kiemtra(daotaotruoc.diachi_coso_3)))
            context['chhjp_0797_3'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_batdau_3)
            context['chhjp_0798_3'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_ketthuc_3)
            context['chhjp_0799_3'] = format_hoso.kiemtra(daotaotruoc.sogio_3)
            context['hhjp_0800'] = format_hoso.kiemtra(daotaotruoc.tongsogio)
            if daotaotruoc.noidung_ngoai_1:
                context['ahhjp_0801_1'] = Listing(format_hoso.kiemtra(daotaotruoc.noidung_ngoai_1))
                context['ahhjp_0802_1'] = Listing('%s \n %s' % (
                    format_hoso.kiemtra(daotaotruoc.ten_diachi_ngoai_1.ten_han_nghiepdoan),
                    format_hoso.kiemtra(daotaotruoc.diachi_coquan_ngoai_1)))
                context['ahhjp_0803_1'] = Listing(
                    '%s \n %s' % (format_hoso.kiemtra(daotaotruoc.tencoso_diachi_ngoai_1.ten_coquan_h),
                                  format_hoso.kiemtra(daotaotruoc.diachi_coso_ngoai_1)))
                context['ahhjp_0804_1'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_batdau_ngoai_1)
                context['ahhjp_0805_1'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_ketthuc_ngoai_1)
                context['ahhjp_0806_1'] = format_hoso.kiemtra(daotaotruoc.sogio_ngoai_1)
            if daotaotruoc.loai_tochuc_1 == 'Cơ quan công cộng':
                docdata.remove_shape(u'id="8" name="Oval 8"')
            elif daotaotruoc.loai_tochuc_1 == 'Cơ sở giáo dục':
                docdata.remove_shape(u'id="7" name="Oval 7"')
            else:
                docdata.remove_shape(u'id="8" name="Oval 8"')
                docdata.remove_shape(u'id="7" name="Oval 7"')
            if daotaotruoc.noidung_ngoai_2:
                context['bhhjp_0801_2'] = Listing(format_hoso.kiemtra(daotaotruoc.noidung_ngoai_2))
                context['bhhjp_0802_2'] = Listing('%s \n %s' % (
                    format_hoso.kiemtra(daotaotruoc.ten_diachi_ngoai_2.ten_han_nghiepdoan),
                    format_hoso.kiemtra(daotaotruoc.diachi_coquan_ngoai_2)))

                context['bhhjp_0803_2'] = Listing(
                    '%s \n %s' % (format_hoso.kiemtra(daotaotruoc.tencoso_diachi_ngoai_2.ten_coquan_h),
                                  format_hoso.kiemtra(daotaotruoc.diachi_coso_ngoai_2)))
                context['bhhjp_0804_2'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_batdau_ngoai_2)
                context['bhhjp_0805_2'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_ketthuc_ngoai_2)
                context['bhhjp_0806_2'] = format_hoso.kiemtra(daotaotruoc.sogio_ngoai_2)

            if daotaotruoc.loai_tochuc_2 == 'Cơ quan công cộng':
                docdata.remove_shape(u'id="11" name="Oval 11"')
            elif daotaotruoc.loai_tochuc_2 == 'Cơ sở giáo dục':
                docdata.remove_shape(u'id="10" name="Oval 10"')
            else:
                docdata.remove_shape(u'id="11" name="Oval 11"')
                docdata.remove_shape(u'id="10" name="Oval 10"')

            if daotaotruoc.noidung_ngoai_3:
                context['chhjp_0801_3'] = Listing(format_hoso.kiemtra(daotaotruoc.noidung_ngoai_3))
                context['chhjp_0802_3'] = Listing('%s \n %s' % (
                    format_hoso.kiemtra(daotaotruoc.ten_diachi_ngoai_3.ten_han_nghiepdoan),
                    format_hoso.kiemtra(daotaotruoc.diachi_coquan_ngoai_3)))
                context['chhjp_0803_3'] = Listing(
                    '%s \n %s' % (format_hoso.kiemtra(daotaotruoc.tencoso_diachi_ngoai_3.ten_coquan_h),
                                  format_hoso.kiemtra(daotaotruoc.diachi_coso_ngoai_3)))
                context['chhjp_0804_3'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_batdau_ngoai_3)
                context['chhjp_0805_3'] = format_hoso.convert_date(daotaotruoc.khoangthoigian_ketthuc_ngoai_3)
                context['chhjp_0806_3'] = format_hoso.kiemtra(daotaotruoc.sogio_ngoai_3)
            if daotaotruoc.loai_tochuc_3 == 'Cơ quan công cộng':
                docdata.remove_shape(u'id="13" name="Oval 13"')
            elif daotaotruoc.loai_tochuc_3 == 'Cơ sở giáo dục':
                docdata.remove_shape(u'id="6" name="Oval 6"')
            else:
                docdata.remove_shape(u'id="13" name="Oval 13"')
                docdata.remove_shape(u'id="6" name="Oval 6"')
            context['hhjp_0807'] = format_hoso.kiemtra(daotaotruoc.tongsogio_ngoai)
            table_tts = []
            for thuctapsinh_hoso in document.thuctapsinh_hoso:
                infor_tts = {}
                infor_tts['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh_hoso.ten_lt_tts)
                infor_tts['hhjp_0902'] = format_hoso.convert_date(thuctapsinh_hoso.ngaynhapcanh_tts)
                infor_tts['hhjp_0599'] = format_hoso.kiemtra(thuctapsinh_hoso.thongtinkhac_tts)
                table_tts.append(infor_tts)
            context['tbl_tts'] = table_tts
            context['hhjp_0596'] = format_hoso.convert_date(daotaotruoc.thoigian_batdau_daotaotruoc)
            context['hhjp_0597'] = format_hoso.convert_date(daotaotruoc.thoigian_ketthuc_daotaotruoc)
            context['hhjp_0600'] = format_hoso.convert_date(daotaotruoc.nguoi_lamdon)
            context['hhjp_0097'] = format_hoso.kiemtra(
                daotaotruoc.donhang_daotaotruoc.nghiepdoan_donhang.ten_han_nghiepdoan)
            context['hhjp_0103'] = '%s  %s' % (
                format_hoso.kiemtra(
                    daotaotruoc.donhang_daotaotruoc.nghiepdoan_donhang.chucvu_daidien_phienam_nghiepdoan),
                format_hoso.kiemtra(daotaotruoc.donhang_daotaotruoc.nghiepdoan_donhang.ten_daidien_han_nghiepdoan))

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_63(self, document):
        print('63')
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_63")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)

            context = {}
            daotaotruoc = self.env['daotao.daotaotruoc'].search(
                [('donhang_daotaotruoc', '=', document.donhang_hoso.id)], limit=1)

            context['hhjp_0097'] = format_hoso.kiemtra(daotaotruoc.tencoquan_diachi_1.ten_han_nghiepdoan)
            context['dttnc_0004'] = Listing(format_hoso.kiemtra(daotaotruoc.tencoso_diachi_1.ten_coso_jp))
            context['dttnc_0004_dc'] = format_hoso.kiemtra(daotaotruoc.tencoso_diachi_1.diachi_coso)
            context['dttnc_0004_dt'] = format_hoso.kiemtra(daotaotruoc.tencoso_diachi_1.sdt_coso).strip()
            context['hhjp_0605'] = format_hoso.convert_date(daotaotruoc.thoigian_batdau_daotaotruoc)
            context['hhjp_0606'] = format_hoso.convert_date(daotaotruoc.thoigian_ketthuc_daotaotruoc)

            table_dtt = []

            for thuctapsinh in document.thuctapsinh_hoso:
                infor = {}
                infor['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
                infor['hhjp_0902'] = format_hoso.convert_date(thuctapsinh.ngaysinh_tts)
                table_dtt.append(infor)
            context['tbl_tts'] = table_dtt
            context['hhjp_0799_1'] = format_hoso.kiemtra(daotaotruoc.sogio_1)
            context['hhjp_0799_2'] = format_hoso.kiemtra(daotaotruoc.sogio_2)
            context['hhjp_0799_3'] = format_hoso.kiemtra(daotaotruoc.sogio_3)
            context['hhjp_0800'] = format_hoso.kiemtra(daotaotruoc.tongsogio)
            context['hhjp_0193'] = format_hoso.convert_date(daotaotruoc.ngay_lamdon)
            context['hhjp_0595_cv'] = format_hoso.kiemtra(daotaotruoc.tencoso_diachi_1.chuc_vu)
            context['hhjp_0595'] = format_hoso.kiemtra(daotaotruoc.tencoso_diachi_1.nguoi_ky)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_64(self, ma_thuctapsinh, document):
        print('64')
        if document.giaidoan_pc == '1 go':
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_64")], limit=1)
        elif document.giaidoan_pc == '3 go':
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_64_QL")], limit=1)

        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            context = {}
            if document.donhang_hoso.date_expected_send_to_customer:
                context['nam_ky'] = format_hoso.kiemtra(document.donhang_hoso.date_expected_send_to_customer.year)
            else:
                context['nam_ky'] = '....'
            context['hhjp_0193'] = format_hoso.convert_date(document.donhang_hoso.date_expected_send_to_customer)
            context['hhjp_0112'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_anh_phaicu)
            context['hhjp_0376'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.ten_nguoidaidien)
            context['hhjp_0376_cv'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.chucvu_han_phaicu)
            context['hhjp_0512_han'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.diachi_tienganh)
            context['hhjp_0513'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.sdt_phaicu).strip()
            context['hhjp_0514'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.fax_phaicu).strip()
            context['hhjp_0001'] = format_hoso.kiemtra(thuctapsinh.ten_lt_tts)
            context['hhjp_0004'] = format_hoso.convert_date(thuctapsinh.ngaysinh_tts)
            context['hhjp_0515'] = format_hoso.kiemtra(thuctapsinh.identity)
            context['hhjp_0515_ngay'] = format_hoso.kiemtra(thuctapsinh.year_identity) + '年' + format_hoso.kiemtra(
                thuctapsinh.month_identity) + '月' + format_hoso.kiemtra(thuctapsinh.day_identity) + '日'
            context['hhjp_0515_nc'] = format_hoso.kiemtra(thuctapsinh.place_cmnd_n)
            context['hhjp_0515_tt'] = format_hoso.kiemtra(thuctapsinh.diachi_tts)
            context['hd_0001'] = format_hoso.kiemtra(thuctapsinh.passport_so_tts)
            if thuctapsinh.passport_phathanh_tts:
                context['hd_0001_hc'] = format_hoso.convert_date(thuctapsinh.passport_phathanh_tts)
            else:
                context['hd_0001_hc'] = ''
            context['hd_0001_nc'] = format_hoso.kiemtra(thuctapsinh.passport_noicap)
            context['hd_0002'] = format_hoso.kiemtra(thuctapsinh.contact_person_lt)
            context['hd_0003'] = format_hoso.kiemtra(thuctapsinh.contact_address_lt)
            context['hd_0004'] = format_hoso.kiemtra(thuctapsinh.contact_relative.name_jp)
            context['hd_0005'] = format_hoso.kiemtra(thuctapsinh.contact_phone)
            context['hd_0006'] = format_hoso.convert_date(document.donhang_hoso.congtyphaicu_donhang.ngayky_hiepdinh)
            context['hhjp_0097'] = format_hoso.kiemtra(document.donhang_hoso.nghiepdoan_donhang.ten_han_nghiepdoan)
            context['hd_0007_tn'] = format_hoso.kiemtra(document.donhang_hoso.year_expire)
            context['hd_0008'] = format_hoso.place_value(
                int(document.donhang_hoso.donhang_daingo.sotien_trocap_daotao_thangdau))
            context['hd_0009_tn'] = format_hoso.nganhnghe(thuctapsinh.nganhnghe_tts.name_loaicongviec)
            context['hhjp_0010'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_han_xnghiep)
            context['hhjp_0011'] = format_hoso.kiemtra(document.xinghiep_hoso.diachi_han_xnghiep)
            context['hhjp_0012'] = format_hoso.kiemtra(document.xinghiep_hoso.sdt_xnghiep).strip()
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_hoso_64_1(self, ma_thuctapsinh, document):
        print('64_1')
        if document.giaidoan_pc == '1 go':
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_64_1")], limit=1)
        elif document.giaidoan_pc == '3 go':
            docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_64_1_QL")], limit=1)

        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            thuctapsinh = self.env['thuctapsinh.thuctapsinh'].search([('ma_tts', '=', ma_thuctapsinh)], limit=1)
            context = {}
            if document.donhang_hoso.date_expected_send_to_customer:
                context['nam_kyhd'] = format_hoso.kiemtra(document.donhang_hoso.date_expected_send_to_customer.year)
            else:
                context['nam_kyhd'] = '....'
            if document.donhang_hoso.date_expected_send_to_customer:
                context['hhjp_0193'] = str(
                    format_hoso.kiemtra(document.donhang_hoso.date_expected_send_to_customer.day)) + '/' + str(
                    format_hoso.kiemtra(
                        document.donhang_hoso.date_expected_send_to_customer.month)) + '/' + str(
                    format_hoso.kiemtra(document.donhang_hoso.date_expected_send_to_customer.year))
            else:
                context['hhjp_0193'] = ''
            context['hhjp_0112_tv'] = format_hoso.kiemtra(
                document.donhang_hoso.congtyphaicu_donhang.ten_viet_phaicu).upper()
            context['hhjp_0376_tv'] = format_hoso.kiemtra(
                document.donhang_hoso.congtyphaicu_donhang.daidien_latinh_phaicu).upper()
            context['hhjp_0376_cv_tv'] = format_hoso.kiemtra(
                document.donhang_hoso.congtyphaicu_donhang.chucvu_latinh_phaicu)
            context['hhjp_0512'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.diachi_viet_pc)
            context['hhjp_0513'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.sdt_phaicu).strip()
            context['hhjp_0514'] = format_hoso.kiemtra(document.donhang_hoso.congtyphaicu_donhang.fax_phaicu)
            context['hhjp_0001_tv'] = format_hoso.kiemtra(thuctapsinh.ten_tv_tts).upper()
            if thuctapsinh.ngaysinh_tts:
                context['hhjp_0004'] = str(format_hoso.kiemtra(thuctapsinh.ngaysinh_tts.day)) + '/' + str(
                    format_hoso.kiemtra(thuctapsinh.ngaysinh_tts.month)) + '/' + str(
                    format_hoso.kiemtra(thuctapsinh.ngaysinh_tts.year))
            else:
                context['hhjp_0004'] = ''
            context['hhjp_0515'] = format_hoso.kiemtra(thuctapsinh.identity)
            if thuctapsinh.date_identity:
                context['hhjp_0515_ngay'] = format_hoso.kiemtra(thuctapsinh.day_identity) + '/' + format_hoso.kiemtra(
                    thuctapsinh.month_identity) + '/' + format_hoso.kiemtra(thuctapsinh.year_identity)
            else:
                context['hhjp_0515_ngay'] = ''
            context['hhjp_0515_nc'] = format_hoso.kiemtra(thuctapsinh.place_cmnd.name)
            context['hhjp_0515_tt'] = format_hoso.kiemtra(thuctapsinh.diachi_tts_v)
            context['hd_0001'] = format_hoso.kiemtra(thuctapsinh.passport_so_tts)
            if thuctapsinh.passport_phathanh_tts:
                context['hd_0001_hc'] = str(format_hoso.kiemtra(
                    thuctapsinh.passport_phathanh_tts.day)) + '/' + str(format_hoso.kiemtra(
                    thuctapsinh.passport_phathanh_tts.month)) + '/' + str(format_hoso.kiemtra(
                    thuctapsinh.passport_phathanh_tts.year))
            else:
                context['hd_0001_hc'] = ''
            context['hd_0001_nc'] = format_hoso.kiemtra(thuctapsinh.passport_noicap)
            context['hd_0002'] = format_hoso.kiemtra(thuctapsinh.contact_person).upper()
            context['hd_0003'] = format_hoso.kiemtra(thuctapsinh.contact_address)
            context['hd_0004'] = format_hoso.kiemtra(thuctapsinh.contact_relative.name)
            context['hd_0005'] = format_hoso.kiemtra(thuctapsinh.contact_phone)
            if document.donhang_hoso.congtyphaicu_donhang.ngayky_hiepdinh:
                context['hd_0006'] = str(format_hoso.kiemtra(
                    document.donhang_hoso.congtyphaicu_donhang.ngayky_hiepdinh.day)) + '/' + str(format_hoso.kiemtra(
                    document.donhang_hoso.congtyphaicu_donhang.ngayky_hiepdinh.month)) + '/' + str(format_hoso.kiemtra(
                    document.donhang_hoso.congtyphaicu_donhang.ngayky_hiepdinh.year))
            else:
                context['hd_0006'] = ''
            context['hhjp_0097_tv'] = format_hoso.kiemtra(
                document.donhang_hoso.nghiepdoan_donhang.ten_viet_nghiepdoan).upper()
            context['hd_0007'] = format_hoso.kiemtra(document.donhang_hoso.year_expire)
            context['hd_0008'] = format_hoso.place_value(
                int(document.donhang_hoso.donhang_daingo.sotien_trocap_daotao_thangdau))
            context['hd_0009'] = format_hoso.kiemtra(thuctapsinh.nganhnghe_tts_v)
            context['hhjp_0010_tv'] = format_hoso.kiemtra(document.xinghiep_hoso.ten_viet_xnghiep)
            context['hhjp_0011_tv'] = format_hoso.kiemtra(document.xinghiep_hoso.diachi_viet_xnghiep)
            context['hhjp_0012'] = format_hoso.kiemtra(document.xinghiep_hoso.sdt_xnghiep).strip()
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def testButton(self):
        view_id = self.env.ref('hoso.kieu_tree_view').id
        context = self._context.copy()
        return {
            'name': 'form_name',
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'hoso.kieu',
            'view_id': view_id,
            'type': 'ir.actions.act_window',
            'res_id': self.id,
            'context': context,
        }
