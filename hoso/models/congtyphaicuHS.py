from . import intern_utils
from datetime import datetime
from odoo import models, fields, api


class ThucTapSinhHoSo(models.Model):
    _inherit = "thuctapsinh.thuctapsinh"
    _order = 'id asc'

    tuoi_tts_pc = fields.Integer(string="Tuổi", compute='_tuoi_tts_pc')

    @api.depends('ngaysinh_tts')
    def _tuoi_tts_pc(self):
        ngay_sinh = self.donhang_tts.date_create_letter_promotion
        if self.ngaysinh_tts and self.donhang_tts.date_create_letter_promotion:
            tmp = ngay_sinh.year - self.ngaysinh_tts.year
            if ngay_sinh.month == self.ngaysinh_tts.month:
                if ngay_sinh.day < self.ngaysinh_tts.day:
                    tmp = tmp - 1
            elif ngay_sinh.month < self.ngaysinh_tts.month:
                tmp = tmp - 1
            self.tuoi_tts_pc = int(tmp)


    identity = fields.Char(string="CMND or TCC")  # hh_05
    place_cmnd = fields.Many2one(comodel_name="province", string="Nơi cấp(Tiếng việt)")  # hh_08
    place_cmnd_n = fields.Char(string="Nơi cấp(Tiếng nhật)", related="place_cmnd.name_in_jp")  # hh_08
    sdt_tts = fields.Char("Số điện thoại")  # hh_18

    # Ngày cấp---------------------------------------------------------------------------------------------
    day_identity = fields.Char("Ngày", size=2)  # hh_27
    month_identity = fields.Selection([('01', '01'), ('02', '02'), ('03', '03'), ('04', '04'),
                                       ('05', '05'), ('06', '06'), ('07', '07'), ('08', '08'),
                                       ('09', '09'), ('10', '10'), ('11', '11'), ('12', '12'), ], "Tháng")  # hh_28
    year_identity = fields.Char("Năm", size=4)  # hh_29
    date_identity = fields.Char("Ngày cấp", store=False, compute='_date_of_identity')  # hh_07

    @api.one
    @api.depends('day_identity', 'month_identity', 'year_identity')
    def _date_of_identity(self):
        if self.day_identity and self.month_identity and self.year_identity:
            self.date_identity = u"Ngày %s Tháng %s Năm %s" % (
                self.day_identity, self.month_identity, self.year_identity)
        elif self.month_identity and self.year_identity:
            self.date_identity = u"Tháng %s Năm %s" % (self.month_identity, self.year_identity)
        elif self.year_identity:
            self.date_identity = u"Năm %s" % (self.year_identity)
        else:
            self.date_identity = ""

    contact_person = fields.Char("Tên(Quốc mẫu)")  # hh_333
    contact_person_lt = fields.Char("Tên(Latinh)")  # hh_333

    @api.onchange('contact_person')
    def contact(self):
        if self.contact_person:
            name_bg = intern_utils.no_accent_vietnamese(str(self.contact_person))
            name_bg_upper = str(name_bg).upper()
            self.contact_person_lt = str(name_bg_upper)

    contact_relative = fields.Many2one('relation', "Quan hệ với TTS")  # hh_334
    contact_phone = fields.Char("Số điện thoại của người quan hệ với TTS")  # hh_335
    contact_address = fields.Char("Địa chỉ(Quốc mẫu)")  # hh_336
    contact_address_lt = fields.Char("Địa chỉ(Latinh)")  # hh_336

    @api.onchange('contact_address')
    def address(self):
        if self.contact_address:
            name_bg = intern_utils.no_accent_vietnamese(str(self.contact_address))
            name_bg_upper = str(name_bg).upper()
            self.contact_address_lt = str(name_bg_upper)

    time_start_at_pc = fields.Char("Thời gian bắt đầu làm công ty PC thứ 2(từ tháng, năm, đến nay) TIẾNG NHẬT",
                                   store=False, compute='_time_start_at_pc')  # hh_386

    time_start_at_pc_from_month = fields.Selection([('01', '01'), ('02', '02'), ('03', '03'), ('04', '04'),
                                                    ('05', '05'), ('06', '06'), ('07', '07'), ('08', '08'),
                                                    ('09', '09'), ('10', '10'), ('11', '11'), ('12', '12'), ],
                                                   "Tháng")  # hh_387

    time_start_at_pc_from_year = fields.Char("Năm", size=4)  # hh_388

    @api.multi
    @api.depends('time_start_at_pc_from_month', 'time_start_at_pc_from_year')
    def _time_start_at_pc(self):
        for rec in self:
            if rec.time_start_at_pc_from_year:
                rec.time_start_at_pc = intern_utils.date_time_in_jp(None, rec.time_start_at_pc_from_month,
                                                                    rec.time_start_at_pc_from_year)
            else:
                rec.time_start_at_pc = ""

            if rec.time_start_at_pc_from_year:
                rec.time_start_at_pc = rec.time_start_at_pc + u'～ 現在'

    time_at_pc_month = fields.Integer('Tổng thời gian làm việc tại công ty PC (tháng)')  # hh_389
    time_at_pc_year = fields.Integer('Tổng thời gian làm việc tại công ty PC (năm)')  # hh_390

    # familys = fields.One2many(comodel_name="intern.family", inverse_name="lienket", string="Gia đình")  # hh_80


class InternFamily(models.Model):
    _name = 'intern.family'
    _rec_name = 'relationship'

    lienket = fields.Many2one(comodel_name="thuctapsinh.thuctapsinh")  # hh_137
    name = fields.Char("Tên", required=True)  # hh_96
    relationship = fields.Char("Quan hệ", required=True)  # hh_97
    ages = fields.Integer("Tuổi")  # hh_98
    birth_year = fields.Integer("Năm sinh")  # hh_99
    job = fields.Char("Nghề nghiệp")  # hh_101
    live_together = fields.Boolean("Sống chung", default=10)  # hh_100

    @api.onchange('ages')
    def age_change(self):
        if self.ages:
            self.birth_year = (datetime.now().year) - self.ages
        else:
            self.birth_year = 0

    @api.multi
    def read(self, fields=None, load='_classic_read'):
        result = super(InternFamily, self).read(fields, load)
        for record in result:
            if 'birth_year' in record and 'ages' in record:
                record['ages'] = datetime.now().year - record['birth_year']
        return result

    sequence = fields.Integer('sequence', help="Sequence for the handle.")
