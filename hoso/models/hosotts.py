# -*- coding: utf-8 -*-

from odoo import models, fields, api


class HSThuc(models.Model):
    _name = 'hoso.thuctapsinh'

    hoso_tts = fields.Many2one(comodel_name='hoso.hoso')
    giaidoan = fields.Selection(string='Giai đoạn',
                                selection=[('1 go', '1 go'),
                                           ('2 go', '2 go'),
                                           ('3 go', '3 go')],
                                related='hoso_tts.giaidoan_hoso')

    giaidoan_pc = fields.Selection(string='Giai đoạn',
                                   selection=[('1 go', '1 go'),
                                              ('3 go', '3 go')],
                                   related='hoso_tts.giaidoan_pc')

    luachon = fields.Boolean(string='Tất cả')
    luachon1 = fields.Boolean(string='Tất cả')
    ma_tts = fields.Char(string='Mã')
    ten_han_tts = fields.Many2one(comodel_name='thuctapsinh.thuctapsinh', string='Tên thực tập sinh')
    file_1 = fields.Boolean(string='第１号  第１面技能実習計画認定申請書')
    file_2 = fields.Boolean(string='第１号  第２面技能実習計画')
    file_3 = fields.Boolean(string='第１号  第３面入国後講習実施予定表')
    file_4 = fields.Boolean(string='第１号　第４面　実習実施予定表')
    file_5 = fields.Boolean(string='第１号  第７面誓約事項')
    file_6 = fields.Boolean(string='第７号  実習実施者届出書')
    file_7 = fields.Boolean(string='参考１-１号、第１-25号 申請者の概要書')
    file_8 = fields.Boolean(string='参考１ー２号  申請者の誓約書')
    file_9 = fields.Boolean(string='参考１ー３号  技能実習生の履歴書')
    # file_10 = fields.Boolean(string='参考１ー４技能実習責任者の履歴書')
    file_10 = fields.Boolean(string='参考１－４号、第１-６号、第１－８号 技能実習責任者・技能実習指導員・生活指導員の履歴書')
    # file_11 = fields.Boolean(string='参考１ー５技能実習責任者の就任承諾書及び誓約書の写し')
    file_11 = fields.Boolean(string='参考１－５号、第１-７号、第１－９号 技能実習責任者・技能実習指導員・生活指導員の就任承諾書及び誓約書')
    file_12 = fields.Boolean(string='参考１ー６号  技能実習指導員の履歴書')
    file_13 = fields.Boolean(string='参考１ー７号  技能実習指導員の就任承諾書及び誓約書の写し')
    file_14 = fields.Boolean(string='参考１ー８号  生活指導員の履歴書')
    file_15 = fields.Boolean(string='参考１ー９号  生活指導員の就任承諾書及び誓約書の写し')
    file_16 = fields.Boolean(string='参考1-16号、第1-17号、第1-18号 技能実習生の報酬・宿泊施設・徴収費用についての説明書')
    file_17 = fields.Boolean(string='参考１-14号  技能実習のための雇用契約書')
    file_18 = fields.Boolean(string='参考１-14号  技能実習のための雇用契約書')
    file_19 = fields.Boolean(string='参考１-19号(Ｄ)  技能実習の期間中の待遇に関する重要事項説明書')
    # file_20 = fields.Boolean(string='参考様式第１-20号技能実習生の申告書')
    # file_21 = fields.Boolean(string='参考様式第１-21号 技能実習の準備に関し本国で支払った費用の明細書')
    file_22 = fields.Boolean(string='参考１ー２２号  技能実習を行わせる理由書')
    # file_23 = fields.Boolean(string='参考様式第１-23号 技能実習生の推薦状')
    file_24 = fields.Boolean(string='参考１ー２５号  技能実習生の名簿')
    file_25 = fields.Boolean(string='参考１ー２７号  同種業務従事経験等証明書-団体監理型技能実習')
    # file_26 = fields.Boolean(string='参考１ー２8外国の所属機関による証明書')
    # file_27 = fields.Boolean(string='参考１ー２９入国前講習実施)予定(表')
    file_28 = fields.Boolean(string='参考１ー３０号  複数の職種及び作業に係る技能実習を行わせる理由書')
    file_29 = fields.Boolean(string='申請する技能実習計画の対象となる技能実習生の名簿')
    file_30 = fields.Boolean(string='技能実習計画認定申請に係る提出書類一覧・確認表')
    file_31 = fields.Boolean(string='委任状')
    file_32 = fields.Boolean(string='参考1-24号 優良要件適合申告書（実習実施者）')
    file_33 = fields.Boolean(string='参考1－19号(Ｅ・Ｆ) 技能実習の期間中の待遇に関する重要事項説明書')
    file_34 = fields.Boolean(string='第１号　第５面　実習実施予定表　１年目')
    file_35 = fields.Boolean(string='第１号　第６面　実習実施予定表　２年目')

    file_36 = fields.Boolean(string='技能実習生候補者のリスト')  # --file_36 là file_23
    file_37 = fields.Boolean(string='認定申請手数料払込申告書')  # --file_36 là file_23

    file_50 = fields.Boolean(string='参考様式第1-3号　技能実習生の履歴書')
    file_51 = fields.Boolean(string='参考様式第１-10号')
    file_52 = fields.Boolean(string='参考様式第１-13号外国の準備機関の概要書及び誓約書')
    file_53 = fields.Boolean(string='参考様式第４-８号入国前講習実施記録')
    # file_54 = fields.Boolean(string='参考様式第１-14号 技能実習のための雇用契約書')
    # file_55 = fields.Boolean(string='参考様式第１-15号雇用条件書')
    # file_56 = fields.Boolean(string='参考様式第１-19号 技能実習の期間中の待遇に関する重要事項説明書')
    file_57 = fields.Boolean(string='参考様式第１-20号技能実習生の申告書')
    file_58 = fields.Boolean(string='参考様式第１-21号 技能実習の準備に関し本国で支払った費用の明細書')
    file_59 = fields.Boolean(string='参考様式第１-23号 技能実習生の推薦状')
    file_60 = fields.Boolean(string='参考１ー２７同種業務従事経験等証明書-団体監理型技能実習')
    file_61 = fields.Boolean(string='参考様式第1-28号　外国の所属機関による証明書（団体監理型技能実習）')
    file_62 = fields.Boolean(string='参考様式第1-29号　入国前講習実施（予定）表')
    file_63 = fields.Boolean(string='事前講習実施報告書')
    file_64 = fields.Boolean(string='日本への技能実習生派遣契約書')

    @api.onchange('luachon')
    def onchange_method(self):
        if self.luachon == True:
            self.file_1 = True
            self.file_2 = True
            self.file_3 = True
            self.file_4 = True
            self.file_5 = True
            # self.file_6 = True
            self.file_7 = True
            self.file_8 = True
            self.file_9 = True
            self.file_10 = True
            self.file_11 = True
            self.file_12 = True
            self.file_13 = True
            self.file_14 = True
            self.file_15 = True
            self.file_16 = True
            self.file_17 = True
            self.file_18 = True
            self.file_19 = True
            # self.file_20 = True
            # self.file_21 = True
            self.file_22 = True
            # self.file_23 = True
            self.file_24 = True
            self.file_25 = True
            # self.file_26 = True
            # self.file_27 = True
            self.file_28 = True
            self.file_29 = True
            self.file_30 = True
            self.file_31 = True
            self.file_32 = True
            self.file_33 = True
            self.file_34 = True
            self.file_35 = True
            self.file_37 = True

        elif self.luachon == False:
            self.file_1 = False
            self.file_2 = False
            self.file_3 = False
            self.file_4 = False
            self.file_5 = False
            # self.file_6 = False
            self.file_7 = False
            self.file_8 = False
            self.file_9 = False
            self.file_10 = False
            self.file_11 = False
            self.file_12 = False
            self.file_13 = False
            self.file_14 = False
            self.file_15 = False
            self.file_16 = False
            self.file_17 = False
            self.file_18 = False
            self.file_19 = False
            # self.file_20 = False
            # self.file_21 = False
            self.file_22 = False
            # self.file_23 = False
            self.file_24 = False
            self.file_25 = False
            # self.file_26 = False
            # self.file_27 = False
            self.file_28 = False
            self.file_29 = False
            self.file_30 = False
            self.file_31 = False
            self.file_32 = False
            self.file_33 = False
            self.file_34 = False
            self.file_35 = False
            self.file_37 = False

    @api.onchange('luachon1')
    def onchange_method_pc(self):
        if self.luachon1 == True:
            if self.giaidoan_pc == '1 go':
                self.file_50 = True

                self.file_51 = True
                self.file_52 = True
                self.file_57 = True

                self.file_58 = True
                self.file_59 = True
                self.file_60 = True

                self.file_61 = True
                self.file_62 = True
                self.file_53 = True
                self.file_63 = True
                self.file_64 = True
            elif self.giaidoan_pc == '3 go':
                self.file_50 = True
                self.file_51 = True
                self.file_52 = True
                self.file_57 = True
                self.file_58 = True
                self.file_59 = True
                self.file_61 = True
                self.file_64 = True

        elif self.luachon1 == False:
            self.file_50 = False
            self.file_51 = False
            self.file_52 = False
            self.file_57 = False
            self.file_58 = False
            self.file_59 = False
            self.file_60 = False
            self.file_61 = False
            self.file_62 = False
            self.file_53 = False
            self.file_63 = False
            self.file_64 = False
