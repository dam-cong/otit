    infor17 = {}
    infor17['dt5'] = format_hoso.kiemtra(daotaosau.thang_9)
    infor17['dt13'] = format_hoso.kiemtra(daotaosau.ngay_9)
    infor17['dt14'] = format_hoso.kiemtra(daotaosau.thu_9)
    if daotaosau.cangay_9 == True:
        infor17['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_9.noidungdaotao)
        infor17['dt7'] = u'◯' if daotaosau.coso_sang_9 else ''
        infor17['dt8'] = ''
        infor17['dt9'] = ''
        if daotaosau.cosao_daotao_a_9:
            infor17['dt10'] = u'①'
        elif daotaosau.cosao_daotao_b_9:
            infor17['dt10'] = u'②'
        elif daotaosau.cosao_daotao_c_9:
            infor17['dt10'] = u'③'
        else:
            infor17['dt10'] = ''
        infor17['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_9)
        infor17['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_9.ten_giangvien_phaply)
        table_daotaosau.append(infor17)

        infor18 = {}
        infor18['dt5'] = ''
        infor18['dt13'] = ''
        infor18['dt14'] = ''
        infor18['dt6'] = ''
        infor18['dt7'] = ''
        infor18['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_chieu_9.noidungdaotao)
        infor18['dt9'] = u'◯' if daotaosau.coso_chieu_chieu_9 else ''

        if daotaosau.coso_chieu_chieu_a_9:
            infor18['dt10'] = u'①'
        elif daotaosau.coso_chieu_chieu_b_9:
            infor18['dt10'] = u'②'
        elif daotaosau.coso_chieu_chieu_c_9:
            infor18['dt10'] = u'③'
        else:
            infor18['dt10'] = ''

        infor18['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_chieu_9)
        infor18['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_chieu_9.ten_giangvien_phaply)
        table_daotaosau.append(infor18)
    else:
        if daotaosau.ngaynghi_9:
            infor17['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_char_9)
            infor17['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_char_9)
        else:
            infor17['dt6'] = format_hoso.kiemtra(daotaosau.noidung_sang_9.noidungdaotao)
            infor17['dt8'] = format_hoso.kiemtra(daotaosau.noidung_chieu_9.noidungdaotao)
        infor17['dt7'] = u'◯' if daotaosau.coso_sang_9 else ''
        infor17['dt9'] = u'◯' if daotaosau.coso_chieu_9 else ''
        if daotaosau.cosao_daotao_a_9:
            infor17['dt10'] = u'①'
        elif daotaosau.cosao_daotao_b_9:
            infor17['dt10'] = u'②'
        elif daotaosau.cosao_daotao_c_9:
            infor17['dt10'] = u'③'
        else:
            infor17['dt10'] = ''

        infor17['dt11'] = format_hoso.kiemtra(daotaosau.thoigian_9)
        infor17['dt12'] = format_hoso.kiemtra(daotaosau.giaovien_9.ten_giangvien_phaply)
        table_daotaosau.append(infor17)