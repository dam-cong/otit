# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime


class DaoTaoTruoc(models.Model):
    _inherit = 'daotao.daotaotruoc'
    _rec_name = 'donhang_daotaotruoc'
    _order = 'id desc'



    batdau_sang = fields.Float(string='Bắt đầu - Sáng')  # dt_0001
    ketthuc_sang = fields.Float(string='Kế thúc - Sáng')  # dt_0002
    tonggio_daotao = fields.Integer(string='Tổng giờ đào tạo')

    thoigian_batdau_daotaotruoc = fields.Date(string='Bắt đầu')
    thoigian_ketthuc_daotaotruoc = fields.Date(string='Kết thúc')

