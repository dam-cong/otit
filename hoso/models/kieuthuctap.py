# -*- coding: utf-8 -*-

from odoo import models, fields, api


class KieuThucTap(models.Model):
    _name = 'hoso.kieu'
    _rec_name = 'tenkieu_thuctapsinh'

    tenkieu_thuctapsinh = fields.Char(string='Tên kiểu')