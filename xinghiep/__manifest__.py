# -*- coding: utf-8 -*-
{
    'name': "Xí nghiệp",
    'depends': ['base', 'congviec'],
    'data': [
        'security/group_user.xml',
        'security/ir.model.access.csv',
        'views/xinghiep.xml',
        'views/nhanvien.xml',
        'views/chinhanh.xml',
        'views/nhanviencapcao.xml',
    ],
}
