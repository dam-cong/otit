# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date


class NewModule(models.Model):
    _name = 'nhanvien.baigiang'
    _rec_name = 'ten_baigiang'
    _order = 'id asc'

    baigiang_nhanvien = fields.Many2one(comodel_name='nhanvien.nhanvien')
    ten_baigiang = fields.Char(string='Tên bài giảng')  # td_0007
    ngay_baigiang = fields.Date(string='Ngày tham gia bài giảng')  # td_0008


class NhanVien(models.Model):
    _name = 'nhanvien.nhanvien'
    _rec_name = 'ten_han_nhanvien'
    _order = 'id asc'

    nhanvien_xinghiep = fields.Many2one(comodel_name='xinghiep.xinghiep', string='Xí nghiệp')

    ten_phienam_nhanvien = fields.Char('Tên(Furi)')  # hhjp_0016_furi
    ten_han_nhanvien = fields.Char('Tên(Hán)')  # hhjp_0016_han
    gtinh_nhanvien = fields.Selection([('Nam', 'Nam'),
                                       ('Nữ', 'Nữ')], string='Giới tính')  # hhjp_0082
    ngaysinh_nhanvien = fields.Date('Ngày sinh')  # hhjp_0083
    diachi_nhanvien = fields.Char('Địa chỉ')  # bs_0002
    dienthoai_nhanvien = fields.Char('Số điện thoại')  # hhjp_0086
    # sobuudien_nhanvien = fields.Many2one(comodel_name='post.office', string='Số bưu điện')  # bs_0003
    sobuudien_nhanvien = fields.Char( string='Số bưu điện')  # bs_0003

    @api.onchange('sobuudien_nhanvien')
    def _onchange_sobuudien_nhanvien(self):
        if self.sobuudien_nhanvien:
            sobuudien_nhanvien = self.env['post.office'].search([('post', '=', self.sobuudien_nhanvien)], limit=1)
            self.diachi_nhanvien = sobuudien_nhanvien.kanji_address
        else:
            self.diachi_nhanvien = ''

    # quoctich_nhanvien = fields.Char('Quốc tịch')
    quoctich_nhanvien = fields.Many2one(comodel_name='quoctich.quoctich', string='Quốc tịch')  # hhjp_0085
    chucdanh_han_nhanvien = fields.Char('Chức vụ (Hán)')  # hhjp_0058
    chucdanh_furi_nhanvien = fields.Char('Chức vụ (Furi)')  # hhjp_0057
    label_vaitro = fields.Char('Vai trò')  #
    vaitro_three = fields.Boolean('Chịu trách nhiệm thực tập')  # hhjp_0016_trachnhiem
    vaitro_five = fields.Boolean('Chỉ đạo đời sống')  # hhjp_0016_doisong
    vaitro_six = fields.Boolean('Chỉ đạo thực tập')  # hhjp_0016_thuctap
    # noilam_nhanvien = fields.Many2one('xinghiep.chinhanh', string='Nơi làm việc')  # hhjp_0087,88,89
    noilam_nhanvien = fields.Many2many(comodel_name='xinghiep.chinhanh', string='Nơi làm việc')  # hhjp_0087,88,89
    congtac_nhanvien = fields.One2many('xinghiep.congtac', 'xinghiep_congtac',
                                       string='Học vấn, quá trình công tác')  # hhjp_0092,93
    bangcap_nhanvien = fields.Char(string="Bằng cấp, chứng chỉ")  # hhjp_0060
    kinhnghiem_quanly_nhanvien = fields.Text('Kinh nghiệm')  # hhjp_0094
    nam_kinhnghiem_quanly_nhanvien = fields.Text(string='Số năm kinh nghiệm')
    # nam_kinhnghiem_quanly_nhanvien_hien = fields.Integer(string='Số năm kinh nghiệm', compute='_kinhnghiem',
    #                                                      store='True')
    batdau_kinhnghiem_quanly_nhanvien = fields.Integer()

    dugiang_kynang_nhanvien = fields.Char('Quá trình dự giảng')  # hhjp_0095
    baigiang_nhanvien = fields.One2many(comodel_name='nhanvien.baigiang', inverse_name='baigiang_nhanvien',
                                        string='Bài giảng')

    congviec_nhanvien_chidaothuctap = fields.Char(string="Công việc")  # hhjp_0115
    kinhnghiem_nhanvien_chidaothuctap = fields.Integer(string="Số năm kinh nghiệm")  # hhjp_0077
    thuongtruc_nhanvien_chidaothuctap = fields.Selection(string="Thường trực hay không",
                                                         selection=[('Có', 'Có'), ('Không', 'Không')])  # bs_0041

    congviec_nhanvien_chidaothuctap_2 = fields.Char(string="Công việc")  # hhjp_0115
    kinhnghiem_nhanvien_chidaothuctap_2 = fields.Integer(string="Số năm kinh nghiệm")  # hhjp_0077
    thuongtruc_nhanvien_chidaothuctap_2 = fields.Selection(string="Thường trực hay không",
                                                         selection=[('Có', 'Có'), ('Không', 'Không')])  # bs_0041

    # Nghề nghiệp phải chuyển nhượng
    ma_congviec_nhanvien = fields.Char(string='Mã công việc', compute='_macongviec')
    nganhnghe_nhanvien = fields.Many2one(comodel_name='nganhnghe.nganhnghe', string='Ngành nghề')

    @api.onchange('nganhnghe_nhanvien')
    def onchange_master_nganhnghe_nhanvien(self):
        if self.nganhnghe_nhanvien:
            return {'domain': {
                'loainghe_nhanvien': [('nganhnghe_loaicongviec', '=', self.nganhnghe_nhanvien.id)]}}

    loainghe_nhanvien = fields.Many2one(comodel_name='loaicongviec.loaicongviec', string='Loại công việc')

    @api.onchange('loainghe_nhanvien')
    def onchange_master_loainghe_nhanvien(self):
        if self.loainghe_nhanvien:
            return {'domain': {
                'congviec_nhanvien': [('loaicongviec_congviec', '=', self.loainghe_nhanvien.id)]}}

    congviec_nhanvien = fields.Many2one(comodel_name='congviec.congviec', string='Công việc')

    @api.depends('congviec_nhanvien')
    def _macongviec(self):
        if self.congviec_nhanvien:
            self.ma_congviec_nhanvien = self.congviec_nhanvien.ma_congviec

    @api.onchange('vaitro_six')
    def onchange_nvchidaott(self):
        if self.vaitro_six == False:
            self.kinhnghiem_nhanvien_chidaothuctap = ''
            self.congviec_nhanvien_chidaothuctap = ''
            self.kinhnghiem_nhanvien_chidaothuctap_2 = ''
            self.congviec_nhanvien_chidaothuctap_2 = ''