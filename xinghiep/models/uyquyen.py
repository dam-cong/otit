# -*- coding: utf-8 -*-

from odoo import models, fields, api


class UyQuyen(models.Model):
    _name = 'xinghiep.uyquyen'
    _rec_name = 'coquan_uyquyen_xinghiep'
    _order = 'id desc'

    # xinghiep_uyquyen = fields.Many2one(comodel_name='xinghiep.xinghiep', string='Xí nghiệp')

    # 1. Cơ quan nhận ủy quyền
    coquan_uyquyen_xinghiep = fields.Many2one(comodel_name='xinghiep.xinghiep', string='Cơ quan ủy quyền')  # hhjp_0191
    nguoinhan_uyquyen_xinghiep = fields.Char(string='Người nhận ủy quyền', store=True,
                                             related='coquan_uyquyen_xinghiep.daidien_han_xnghiep')  # hhjp_0192
    daichi_uyquyen_xinghiep = fields.Char(string='Địa chỉ', store=True,
                                          related='coquan_uyquyen_xinghiep.diachi_han_xnghiep')  # hhjp_0189
    sobuudien_uyquyen_xinghiep = fields.Char(string='Số bưu điện', store=True,
                                                 related='coquan_uyquyen_xinghiep.sobuudien_xnghiep')  # hhjp_0189_1
    dienthoai_uyquyen_xinghiep = fields.Char(string='Số điện thoại', store=True,
                                             related='coquan_uyquyen_xinghiep.sdt_xnghiep')  # hhjp_0190

    # 2. Các mục ủy thác
    uythac_1_1_uyquyen_xinghiep = fields.Selection(string='Loại ủy thác', selection=[('Tôi', 'Tôi'), (
        'Công ty chúng tôi', 'Công ty chúng tôi')])
    uythac_1_2_uyquyen_xinghiep = fields.Char(string='Hồ sơ')  # hhjp_0188
    tongfile_uyquyen_xinghiep = fields.Integer(string='Tổng số file')

    # 3. Người nhận ủy quyền
    coquan_nguoi_uyquyen_xinghiep = fields.Char(string='Cơ quan ủy quyền')  # hhjp_0186
    nguoinhan_nguoi_uyquyen_xinghiep = fields.Char(string='Người nhận ủy quyền')  # hhjp_0187
    daichi_nguoi_uyquyen_xinghiep = fields.Char(string='Địa chỉ')  # hhjp_0184
    sobuudien_nguoi_uyquyen_xinghiep = fields.Char(string='Số bưu điện')  # hhjp_0184_1
    dienthoai_nguoi_uyquyen_xinghiep = fields.Char(string='Số điện thoại')  # hhjp_0185
