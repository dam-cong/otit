# -*- coding: utf-8 -*-

from odoo import models, fields, api

class xinghiepcongtac(models.Model):
    _name = 'xinghiep.congtac'
    _rec_name = 'noidung'
    _order = 'id asc'

    nam = fields.Char(string="Năm")  # bs_0037
    thang = fields.Selection([('1', 'Tháng 1'), ('2', 'Tháng 2'), ('3', 'Tháng 3'),
                              ('4', 'Tháng 4'), ('5', 'Tháng 5'), ('6', 'Tháng 6'),
                              ('7', 'Tháng 7'), ('8', 'Tháng 8'), ('9', 'Tháng 9'),
                              ('10', 'Tháng 10'), ('11', 'Tháng 11'), ('12', 'Tháng 12')], string='Tháng')  # bs_0038
    noidung = fields.Char(string="Nội dung")  # hhjp_0059
    xinghiep_congtac = fields.Many2one(comodel_name="xinghiep.nhanvien")