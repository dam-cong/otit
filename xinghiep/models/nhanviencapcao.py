# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Capcao(models.Model):
    _name = 'nhanvien.capcao'
    _rec_name = 'ten_han_nhanvien_cc'
    _order = 'id asc'

    nhanvien_capcao_xinghiep = fields.Many2one(comodel_name='xinghiep.xinghiep')
    ten_phienam_nhanvien_cc = fields.Char('Tên(Furl)')  # hhjp_0016_furi
    ten_han_nhanvien_cc = fields.Char('Tên(Hán)')  # hhjp_0016_han
    gtinh_nhanvien_cc = fields.Selection([('Nam', 'Nam'),
                                          ('Nữ', 'Nữ')], string='Giới tính', default='Nam')  # hhjp_0082
    thoigian_nhanvien_cc = fields.Selection([('Toàn thời gian', 'Toàn thời gian'),
                                          ('Bán thời gian', 'Bán thời gian')], string='Toàn thời gian/Bán thời gian')  # hhjp_0082
    ngaysinh_nhanvien_cc = fields.Date('Ngày sinh')  # hhjp_0083
    diachi_nhanvien_cc = fields.Char('Địa chỉ')  # bs_0002
    # sobuudien_nhanvien = fields.Many2one(comodel_name='post.office', string='Số bưu điện')  # bs_0003
    sobuudien_nhanvien = fields.Char(string='Số bưu điện')  # bs_0003

    @api.onchange('sobuudien_nhanvien')
    def _onchange_sobuudien_nhanvien(self):
        if self.sobuudien_nhanvien:
            sobuudien_nhanvien = self.env['post.office'].search([('post', '=', self.sobuudien_nhanvien)], limit=1)
            self.diachi_nhanvien_cc = sobuudien_nhanvien.kanji_address
        else:
            self.diachi_nhanvien_cc = ''

    # quoctich_nhanvien_cc = fields.Char('Quốc tịch')  # hhjp_0085
    quoctich_nhanvien_cc = fields.Many2one(comodel_name='quoctich.quoctich', string='Quốc tịch') # hhjp_0085
    dienthoai_nhanvien_cc = fields.Char('Số điện thoại')  # hhjp_0086
    chucdanh_han_nhanvien_cc = fields.Char('Chức vụ')  # bs_0001
    bangcap_nhanvien_cc = fields.Char(string="Bằng cấp chứng chỉ")
    noilam_nhanvien_cc = fields.Many2one('xinghiep.chinhanh', string='Nơi làm việc')  # hhjp_0087,88,89
    congtac_nhanvien_cc = fields.One2many('xinghiep.congtac', 'xinghiep_congtac',
                                          string='Học vấn, quá trình công tác')  # hhjp_0092,93
    kinhnghiem_quanly_nhanvien_cc = fields.Text('Kinh nghiệm')  # hhjp_0094
    dugiang_kynang_nhanvien_cc = fields.Char('Quá trình dự giảng')  # hhjp_0095