# -*- coding: utf-8 -*-

from odoo import models, fields, api

class xinghiep(models.Model):
    _name = 'xinghiep.xinghiep'
    _rec_name = 'ten_han_xnghiep'
    _order = 'id desc'

    # Thông tin cơ bản
    ten_furi_xnghiep = fields.Char('Tiếng Furi')  # hhjp_0009
    ten_han_xnghiep = fields.Char('Tiếng Hán', required=True)  # hhjp_0010
    ten_viet_xnghiep = fields.Char('Tiếng Anh')  # hhjp_0010_v
    diachi_han_xnghiep = fields.Char('Tiếng Hán')  # hhjp_0011
    diachi_viet_xnghiep = fields.Char('Tiếng Anh')  # hhjp_0011_v
    # sobuudien_xnghiep = fields.Many2one(comodel_name='post.office', string='Số bưu điện')
    sobuudien_xnghiep = fields.Char(string='Số bưu điện', required=True)

    @api.onchange('sobuudien_xnghiep')
    def _onchange_sobuudien_xnghiep(self):
        if self.sobuudien_xnghiep:
            sobuudien_xnghiep = self.env['post.office'].search([('post', '=', self.sobuudien_xnghiep)], limit=1)
            self.diachi_han_xnghiep = sobuudien_xnghiep.kanji_address
            self.diachi_viet_xnghiep = sobuudien_xnghiep.english_address
        else:
            self.diachi_han_xnghiep = ''

    sdt_xnghiep = fields.Char('Số điện thoại')  # hhjp_0012
    fax = fields.Char('Số FAX')  #
    phapnhan_xinghiep = fields.Char('Số đăng kí pháp nhân')  #
    sobaohiem_laodong = fields.Char('Số bảo hiểm lao động')  # hhjp_0027
    sobaohiem_laodong_2 = fields.Char('Số bảo hiểm lao động')  # hhjp_0027
    sanpham = fields.Char('Sản phẩm dịch vụ chính')  # hhjp_0019
    dky_kinhdoanh_xnghiep = fields.Char('Số đăng kí kinh doanh')  # hhjp_0015
    so_chapnhan_daotao = fields.Char('Số phê duyệt thực tập kỹ năng')  # hhjp_0811
    kehoach_cochua = fields.Boolean(string='実習中の技能実習計画なし')
    # sobaohiem_laodong = fields.Char(string='労働保険番号')

    ngay_chapnhan_daotao = fields.Date(string="Ngày phê duyệt thực tập sinh kỹ năng")
    ngay_batdau_daotao = fields.Date(string="Ngày bắt đầu đào tạo kỹ năng")

    so_chungnhan_daotao = fields.Char('Số chứng nhận')
    ghichu_chungnhan_daotao = fields.Char('Ghi chú')
    nam_doanhthu = fields.Char('Năm')
    title_nam_doanhthu = fields.Char('Năm trước')
    title_namtruocnua_doanhthu = fields.Char('Năm trước nữa')
    nam_doanhthu_2nam_truoc = fields.Char('Năm')
    ngay_chungnhan_daotao = fields.Date(string="Ngày chứng nhận")

    daidien_furi_xnghiep = fields.Char('Tiếng Furi')  # hhjp_0013
    daidien_han_xnghiep = fields.Char('Tiếng Hán', required=True)  # hhjp_0014
    daidien_han_xnghiep_v = fields.Char('Tiếng Anh')  # hhjp_0014_v
    chucvu_daidien_han = fields.Char('Tiếng Hán')  # xn_104
    chucvu_daidien_viet = fields.Char('Tiếng mẹ đẻ')  # xn_104_v

    so_nvien_xnghiep = fields.Integer('Nhân viên chính thức (tổng số)')  # hhjp_0020
    nvien_vphong_xnghiep = fields.Integer('Nhân viên văn phòng')  # hhjp_0021
    nvien_congxuong_xnghiep = fields.Integer('Nhân viên công xưởng')  # hhjp_0022

    @api.one
    @api.depends('nvien_vphong_xnghiep', 'nvien_congxuong_xnghiep')
    def _so_nvien_xnghiep(self):
        if self.nvien_vphong_xnghiep and self.nvien_congxuong_xnghiep:
            self.so_nvien_xnghiep = (self.nvien_vphong_xnghiep + self.nvien_congxuong_xnghiep)
        elif self.nvien_vphong_xnghiep:
            self.so_nvien_xnghiep = self.nvien_vphong_xnghiep
        elif self.nvien_congxuong_xnghiep:
            self.so_nvien_xnghiep = self.nvien_congxuong_xnghiep
        else:
            self.so_nvien_xnghiep = 0

    so_nvien_ttkn_xnghiep = fields.Integer('Nhân viên tại nơi thực tập kỹ năng (tổng số)',
                                           compute='_so_nvien_ttkn_xnghiep')  # bs_0033
    nvien_vphong_ttkn_xnghiep = fields.Integer('Nhân viên văn phòng tại nơi thực tập kỹ năng')  # bs_0034
    nvien_congxuong_ttkn_xnghiep = fields.Integer('Nhân viên công xưởng tại nơi thực tập kỹ năng')  # bs_0035
    nvien_nuocngoai_xnghiep = fields.Integer(string='Nhân viên người nước ngoài')
    nvien_thuongxuyen_tts_xnghiep = fields.Integer(string='Nhân viên đi làm thường xuyên bao gồm cả thực tập sinh')
    noidung_congviec_xinghiep = fields.Many2one(comodel_name='nganhnghe.nganhnghe', string='Nội dung công việc')
    noidung_congviec_khac_xinghiep = fields.Char(string='Công việc khác')
    kehoach_daotao_dautien = fields.Char(string='Kế hoạch đào tạo thực tập kỹ thuật đầu tiên')
    title_ten_xinghiep = fields.Char(string='Tên xí nghiệp')
    title_diachi_xinghiep = fields.Char(string='Địa chỉ xí nghiệp')
    title_chucvu_ndd_xinghiep = fields.Char(string='Chức vụ người đại diện')
    title_hoten_nguoidaidien_xinghiep = fields.Char(string='Họ và tên người đại diện')
    title_sonhanvien_ttkn = fields.Char(string='Số nhân viên tại nơi thực tập kỹ năng')
    title_phannganh = fields.Char(string='Phân ngành')
    title_sonhanvien_chinhthuc = fields.Char(string='Số nhân viên chính thức')

    @api.one
    @api.depends('nvien_vphong_ttkn_xnghiep', 'nvien_congxuong_ttkn_xnghiep')
    def _so_nvien_ttkn_xnghiep(self):
        if self.nvien_vphong_ttkn_xnghiep and self.nvien_congxuong_ttkn_xnghiep:
            self.so_nvien_ttkn_xnghiep = (self.nvien_vphong_ttkn_xnghiep + self.nvien_congxuong_ttkn_xnghiep)
        elif self.nvien_vphong_ttkn_xnghiep:
            self.so_nvien_ttkn_xnghiep = self.nvien_vphong_ttkn_xnghiep
        elif self.nvien_congxuong_ttkn_xnghiep:
            self.so_nvien_ttkn_xnghiep = self.nvien_congxuong_ttkn_xnghiep
        else:
            self.so_nvien_ttkn_xnghiep = 0

    sovon_xnghiep = fields.Integer('Số vốn')  # hhjp_0023
    sovon_xnghiep_2nam_truoc = fields.Integer('Số vốn')  # hhjp_0023

    # Ngành nghề
    nganhmax_xnghiep = fields.Many2one(comodel_name='phannganhlon.phannganhlon', string='Phân ngành lớn')  # hhjp_0017
    nganhmin_xnghiep = fields.Many2one(comodel_name='phannganhbe.phannganhbe', string='Phân ngành bé')  # hhjp_0018
    nganhtb_xnghiep = fields.Many2one(comodel_name='phannganhtb.phannganhtb', string='Phân ngành trung')  # hhjp_0018_1

    # @api.onchange('nganhmax_xnghiep')
    # def onchange_master_phannganhlon_phannganhtrung(self):
    #     if self.nganhmax_xnghiep:
    #         return {'domain': {'nganhtb_xnghiep': [
    #             ('phannnganhlon_phannganhtrung', '=', self.nganhmax_xnghiep.id)]}}

    # @api.onchange('nganhtb_xnghiep')
    # def onchange_master_loainghe_chitiet_congviec(self):
    #     if self.nganhtb_xnghiep:
    #         return {'domain': {
    #             'nganhmin_xnghiep': [('phannnganhtrung_phannganhbe', '=', self.nganhtb_xnghiep.id)]}}

    # Doanh thu năm trước
    doanhthu_namtruoc_xnghiep = fields.Integer('Doanh thu')  # hhjp_0024
    doanhthu_namtruoc_xnghiep_2nam_truoc = fields.Integer('Doanh thu')  # hhjp_0024
    loinhuan_namtruoc_xnghiep = fields.Integer('Lợi nhuận')  # hhjp_0025
    loinhuan_2namtruoc_xnghiep = fields.Integer('Lợi nhuận')  # hhjp_0025
    tinhtrang_namtruoc_xnghiep = fields.Selection([('Lãi', 'Lãi'), ('Lỗ', 'Lỗ')], string='Tình trạng')  # bs_0031
    tinhtrang_2namtruoc_xnghiep = fields.Selection([('Lãi', 'Lãi'), ('Lỗ', 'Lỗ')], string='Tình trạng')  # bs_0031
    loinhuanthuan_namtruoc_xnghiep = fields.Integer('Lợi nhuận thuần năm trước')  # hhjp_0026
    loinhuanthuan_2namtruoc_xnghiep = fields.Integer('Lợi nhuận thuần 2 năm trước')  # hhjp_0026
    tinhtrang_thuan_namtruoc_xnghiep = fields.Selection([('Lãi', 'Lãi'), ('Lỗ', 'Lỗ')],
                                                        string='Tình trạng')  # bs_0032

    tinhtrang_thuan_2namtruoc_xnghiep = fields.Selection([('Lãi', 'Lãi'), ('Lỗ', 'Lỗ')],
                                                         string='Tình trạng')  # bs_0032

    # xí nghiệp, Thực tập sinh
    # thực tập sinh đã tiếp nhận
    # thuctapsinhtiepnhan = fields.Boolean('Đã từng tiếp nhận TTS')  #
    tts_xnghiep = fields.One2many('soluong.soluong', 'xinghiep', string='Tiếp nhận thực tập sinh')  #

    # thực tập sinh đang tiếp nhận
    # hình thức xí nghiệp độc lập
    ttskn_first = fields.Integer('TTSKN số 1')  # hhjp_0918
    ttskn_second = fields.Integer('TTSKN số 2')  # hhjp_0920
    ttskn_third = fields.Integer('TTSKN số 3')  # hhjp_0922
    ttskn_chedocu_first = fields.Integer('Theo chế độ cũ')  # hhjp_0919
    ttskn_chedocu_second = fields.Integer('Theo chế độ cũ')  # hhjp_0921
    # Hình thức đoàn thể quản lý
    ttskn_first_dtql = fields.Integer('TTSKN số 1')  # hhjp_0030
    ttskn_second_dtql = fields.Integer('TTSKN số 2')  # hhjp_0534
    ttskn_third_dtql = fields.Integer('TTSKN số 3')  # hhjp_0536
    ttskn_chedocu_first_dtql = fields.Integer('Theo chế độ cũ')  # hhjp_0533
    ttskn_chedocu_second_dtql = fields.Integer('Theo chế độ cũ')  # hhjp_0535
    # Thực tập sinh tiếp nhận 3 năm gần đây
    tts_tn_mot = fields.Integer()  # hhjp_0537
    tts_tn_mot_cu = fields.Integer()  # hhjp_0538
    tts_tn_hai = fields.Integer()  # hhjp_0539
    tts_tn_hai_cu = fields.Integer()  # hhjp_0540
    tts_tn_ba = fields.Integer()  # hhjp_0541
    tts_tn_ba_cu = fields.Integer()  # hhjp_0542
    # Thực tập sinh về nước trước hạn 3 năm gần đây
    # 1go
    tts_vn_namnhat = fields.Integer()  # hhjp_0543
    tts_vn_namnhat_chedocu = fields.Integer()  # hhjp_0544
    tts_vn_namhai = fields.Integer()  # hhjp_0548
    tts_vn_namhai_chedocu = fields.Integer()  # hhjp_0549
    tts_vn_namba = fields.Integer()  # hhjp_0553
    tts_vn_namba_chedocu = fields.Integer()  # hhjp_0554
    # 2go
    tts_vn_namnhat_hai = fields.Integer()  # hhjp_0545
    tts_vn_namnhat_chedocu_hai = fields.Integer()  # hhjp_0546
    tts_vn_namhai_hai = fields.Integer()  # hhjp_0550
    tts_vn_namhai_chedocu_hai = fields.Integer()  # hhjp_0551
    tts_vn_namba_hai = fields.Integer()  # hhjp_0555
    tts_vn_namba_chedocu_hai = fields.Integer()  # hhjp_0556
    # 3go
    tts_vn_namnhat_ba = fields.Integer()  # hhjp_0547
    tts_vn_namhai_ba = fields.Integer()  # hhjp_0552
    tts_vn_namba_ba = fields.Integer()  # hhjp_0557
    # Thực tập sinh bỏ trốn 3 năm gần đây
    # 1go
    tts_vn_namnhat_bt = fields.Integer()  # hhjp_0558
    tts_vn_namnhat_chedocu_bt = fields.Integer()  # hhjp_0559
    tts_vn_namhai_bt = fields.Integer()  # hhjp_0563
    tts_vn_namhai_chedocu_bt = fields.Integer()  # hhjp_0564
    tts_vn_namba_bt = fields.Integer()  # hhjp_0568
    tts_vn_namba_chedocu_bt = fields.Integer()  # hhjp_0569
    # 2go
    tts_vn_namnhat_hai_bt = fields.Integer()  # hhjp_0560
    tts_vn_namnhat_chedocu_hai_bt = fields.Integer()  # hhjp_0561
    tts_vn_namhai_hai_bt = fields.Integer()  # hhjp_0565
    tts_vn_namhai_chedocu_hai_bt = fields.Integer()  # hhjp_0566
    tts_vn_namba_hai_bt = fields.Integer()  # hhjp_0570
    tts_vn_namba_chedocu_hai_bt = fields.Integer()  # hhjp_0571
    # 3go
    tts_vn_namnhat_ba_bt = fields.Integer()  # hhjp_0562
    tts_vn_namhai_ba_bt = fields.Integer()  # hhjp_0567
    tts_vn_namba_ba_bt = fields.Integer()  # hhjp_0572

    ghichu_botron = fields.Text(string="Ghi chú (Ngày tháng năm bỏ trốn)")  # hhjp_0573
    ghichu_khac_botron = fields.Text(string="Ghi chú (Khác)")  # bs_0036

    # Nhân viên xí nghiệp
    danhsach_nhanvien_cc = fields.One2many(comodel_name='nhanvien.capcao', inverse_name='nhanvien_capcao_xinghiep',
                                           string='Danh sách nhân viên cao cấp')
    danhsach_nhanvien = fields.One2many(comodel_name='nhanvien.nhanvien', inverse_name='nhanvien_xinghiep',
                                        string='Danh sách nhân viên')
    # Nghiệp vụ

    nganhnghe_congviec = fields.One2many(comodel_name='tonghop.tonghop', inverse_name='xinghiep_tonghop',
                                         string='Loại công việc')

    # chinhanh_xinghiep = fields.Many2many(comodel_name='xinghiep.chinhanh',
    #                                      string='Nơi thực tập sinh làm việc')  # bc_0010

    chinhanh_xinghiep = fields.One2many(comodel_name='xinghiep.chinhanh', inverse_name='xinghiep_cha',
                                        string='Nơi thực tập sinh làm việc')

    ten_nguoinhap_xinghiep = fields.Char(string='Họ và tên')
    chucvu_nguoinhap_xinghiep = fields.Char(string='Chức vụ')
    ngaynhap_xinghiep = fields.Date(string='Ngày nhập', default=fields.Date.today())

    @api.model
    def create(self, values):
        chamdiem = super(xinghiep, self).create(values)
        self.env['xinghiep.chamdiem'].create({'xinghiep_chamdiem': chamdiem.id})
        # Add code here
        return chamdiem

    def showpopup_chamdiem(self):
        view_id = self.env.ref('baocao.chamdiemxinghiep_form_view').id
        res_id_hs = self.env['xinghiep.chamdiem'].search([('xinghiep_chamdiem', '=', self.id)], limit=1)
        context = self._context.copy()
        context['form_view_initial_mode'] = 'edit'
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id,
            'res_model': 'xinghiep.chamdiem',
            'res_id': res_id_hs.id,
            'context': context,
            'target': 'new',
        }

    @api.multi
    def laydulieu_button(self):
        pass

    @api.multi
    def laydulieu_button_1(self):
        pass

    @api.multi
    def laydulieu_button_2(self):
        chinhanh_chinh = self.env['xinghiep.chinhanh'].search(
            [('xinghiep_cha', '=', self.id), ('ten_chinhanh_han', '=', self.ten_han_xnghiep)], limit=1)
        # vals_chinhanh = []
        if chinhanh_chinh:
            chinhanh_chinh.write(
                {'chinhanh_xinghiep_chinh': True, 'ten_chinhanh_han': self.ten_han_xnghiep,
                 'ten_chinhanh_viet': self.ten_viet_xnghiep,
                 'ten_chinhanh_furl': self.ten_furi_xnghiep, 'diachi_chinhanh': self.diachi_han_xnghiep,
                 'diachi_chinhanh_viet': self.diachi_viet_xnghiep,
                 'sdt_chinhanh': self.sdt_xnghiep,
                 'sobuudien_chinhanh': self.sobuudien_xnghiep, 'congviec': '',
                 'so_nhanvien_vanphong': self.nvien_vphong_xnghiep,
                 'so_nhanvien_congxuong': self.nvien_congxuong_xnghiep,
                 'tongso_nhanvien': self.so_nvien_xnghiep})
        else:
            if self.ten_han_xnghiep:
                vals_tts_row = {}
                vals_tts_row['xinghiep_cha'] = self.id
                vals_tts_row['chinhanh_xinghiep_chinh'] = True
                vals_tts_row['ten_chinhanh_han'] = self.ten_han_xnghiep
                vals_tts_row['ten_chinhanh_furl'] = self.ten_furi_xnghiep
                vals_tts_row['ten_chinhanh_viet'] = self.ten_viet_xnghiep
                vals_tts_row['sobuudien_chinhanh'] = self.sobuudien_xnghiep
                vals_tts_row['diachi_chinhanh'] = self.diachi_han_xnghiep
                vals_tts_row['diachi_chinhanh_viet'] = self.diachi_viet_xnghiep
                vals_tts_row['sdt_chinhanh'] = self.sdt_xnghiep
                vals_tts_row['so_nhanvien_vanphong'] = self.nvien_vphong_xnghiep
                vals_tts_row['so_nhanvien_congxuong'] = self.nvien_congxuong_xnghiep
                tts = self.env['xinghiep.chinhanh'].create(vals_tts_row)
                # vals_chinhanh.append(tts.id)
                self.chinhanh_xinghiep = [(4, tts.id)]

    @api.multi
    def laydulieu_button_3(self):
        pass
