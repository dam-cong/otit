# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ChiNhanh(models.Model):
    _name = 'xinghiep.chinhanh'
    _rec_name = 'ten_chinhanh_han'
    _order = 'id asc'

    xinghiep_cha = fields.Many2one('xinghiep.xinghiep', string='Xí nghiệp mẹ')  #
    chinhanh_xinghiep_chinh = fields.Boolean(string='Trụ sở chính')
    ten_chinhanh_han = fields.Char('Tên (Hán)')  # hhjp_0040
    ten_chinhanh_viet = fields.Char('Tên (Việt)')  # hhjp_0040_v
    ten_chinhanh_furl = fields.Char('Tên (Furi)')  # hhjp_0041
    diachi_chinhanh = fields.Char('Địa chỉ')  # hhjp_0042
    diachi_chinhanh_viet = fields.Char('Địa chỉ(Việt)')  # hhjp_0042_v
    sdt_chinhanh = fields.Char('Số điện thoại')  # hhjp_0043
    # sobuudien_chinhanh = fields.Many2one(comodel_name='post.office', string='Số bưu điện')  # bs_0005
    sobuudien_chinhanh = fields.Char(string='Số bưu điện')  # bs_0005

    @api.onchange('sobuudien_chinhanh')
    def _onchange_sobuudien_chinhanh(self):
        if self.sobuudien_chinhanh:
            sobuudien_chinhanh = self.env['post.office'].search([('post', '=', self.sobuudien_chinhanh)], limit=1)
            self.diachi_chinhanh = sobuudien_chinhanh.kanji_address
        else:
            self.diachi_chinhanh = ''

    congviec = fields.Char('Công việc')  # hhjp_0044
    so_nhanvien_vanphong = fields.Integer(string='Nhân viên văn phòng')
    so_nhanvien_congxuong = fields.Integer(string='Nhân viên công xưởng')

    tongso_nhanvien = fields.Integer(string="Tổng", store=True, compute='_tongso_nhanvien_chinhanh')  #

    @api.multi
    @api.depends('so_nhanvien_vanphong', 'so_nhanvien_congxuong')
    def _tongso_nhanvien_chinhanh(self):
        for i in self:
            if i.so_nhanvien_vanphong or i.so_nhanvien_congxuong:
                i.tongso_nhanvien = i.so_nhanvien_vanphong + i.so_nhanvien_congxuong
            else:
                i.tongso_nhanvien = 0
