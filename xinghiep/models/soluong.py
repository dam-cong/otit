# -*- coding: utf-8 -*-

from odoo import models, fields, api


class SoLuong(models.Model):
    _name = 'soluong.soluong'
    _rec_name = 'quoctich_tts_xnghiep'
    _order = 'id desc'

    # quoctich_tts_xnghiep = fields.Char('Quốc tịch')
    quoctich_tts_xnghiep = fields.Many2one(comodel_name='quoctich.quoctich', string='Quốc tịch')  # hhjp_0028
    soluong_tts_xnghiep = fields.Integer('Số lượng')  # hhjp_0029
    xinghiep = fields.Many2one(comodel_name="xinghiep.xinghiep")
