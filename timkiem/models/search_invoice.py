# -*- coding: utf-8 -*-

from odoo import models, fields, api


class SearchInvoice(models.Model):
    _name = 'search.invoice'
    _rec_name = 'enterprise'

    dispatched_company = fields.Many2one(comodel_name="congtyphaicu.congtyphaicu", string="Công ty phái cử",
                                         help="Công ty phái cử của đơn hàng muốn tìm kiếm")
    enterprise = fields.Many2one(comodel_name="xinghiep.xinghiep", string="Xí nghiệp",
                                 help="Xí nghiệp của đơn hàng muốn tìm kiếm")
    date_entry = fields.Date(string="Thời gian nhập quốc", help="Thời gian nhập quốc của đơn hàng muốn tìm kiếm")

    invoice_list = fields.Many2many(comodel_name="donhang.donhang", compute="_invoice_list",
                                    string="Danh sách đơn hàng",
                                    help="Danh sách đơn hàng lọc theo các điều kiện của người dùng")

    # invoices_promoted = fields.Many2many('intern.invoice', compute='_compute_invoice_promoted',
    #                                      string='Đơn hàng tiến cử')

    @api.multi
    @api.depends('enterprise', 'dispatched_company', 'date_entry')
    def _invoice_list(self):
        related_ids = []
        related_ids1 = []
        related_ids2 = []
        related_ids3 = []
        related_ids4 = []
        related_ids5 = []
        related_ids6 = []
        related_ids7 = []

        invoices3 = self.env['donhang.donhang'].search([])

        print(invoices3)
        for invoice in invoices3:
            related_ids.append(invoice.id)

        self.invoice_list = self.env['donhang.donhang'].search([('id', 'in', related_ids)])
        print(self.invoice_list, '0')

        if self.enterprise:
            invoice_enterprise = self.env['donhang.donhang'].search([('xinghiep_donhang', '=', self.enterprise.id), ])
            for invoice in invoice_enterprise:
                related_ids1.append(invoice.id)

            self.invoice_list = self.env['donhang.donhang'].search([('id', 'in', related_ids1)])
            print(self.invoice_list, '2')
        if self.dispatched_company:
            invoice_dispatched_company = self.env['donhang.donhang'].search(
                [('congtyphaicu_donhang', '=', self.dispatched_company.id), ])
            for invoice in invoice_dispatched_company:
                related_ids2.append(invoice.id)

            self.invoice_list = self.env['donhang.donhang'].search([('id', 'in', related_ids2)])
            print(self.invoice_list, '3')
        if self.date_entry:
            invoice_date_entry = self.env['donhang.donhang'].search(
                [('thoigian_donhang', '=', self.date_entry), ])
            for invoice in invoice_date_entry:
                related_ids3.append(invoice.id)

            self.invoice_list = self.env['donhang.donhang'].search([('id', 'in', related_ids3)])
            print(self.invoice_list, '4')

        if self.dispatched_company and self.enterprise:
            invoicese = self.env['donhang.donhang'].search([('xinghiep_donhang', '=', self.enterprise.id),
                                                            ('congtyphaicu_donhang', '=', self.dispatched_company.id)])
            for invoice in invoicese:
                related_ids4.append(invoice.id)

            self.invoice_list = self.env['donhang.donhang'].search([('id', 'in', related_ids4)])
            print(self.invoice_list, '5')

        if self.dispatched_company and self.date_entry:
            invoice1 = self.env['donhang.donhang'].search([('congtyphaicu_donhang', '=', self.dispatched_company.id),
                                                           ('thoigian_donhang', '=', self.date_entry)])
            for invoice in invoice1:
                related_ids5.append(invoice.id)

            self.invoice_list = self.env['donhang.donhang'].search([('id', 'in', related_ids5)])
            print(self.invoice_list, '6')

        if self.enterprise and self.date_entry:
            invoices2 = self.env['donhang.donhang'].search([('xinghiep_donhang', '=', self.enterprise.id),
                                                            ('thoigian_donhang', '=', self.date_entry)])
            for invoice in invoices2:
                related_ids6.append(invoice.id)

            self.invoice_list = self.env['donhang.donhang'].search([('id', 'in', related_ids6)])
            print(self.invoice_list, '7')

        if self.dispatched_company and self.enterprise and self.date_entry:
            invoices = self.env['donhang.donhang'].search([('xinghiep_donhang', '=', self.enterprise.id),
                                                           ('congtyphaicu_donhang', '=', self.dispatched_company.id),
                                                           ('thoigian_donhang', '=', self.date_entry)])
            print(invoices)
            for invoice in invoices:
                related_ids7.append(invoice.id)

            self.invoice_list = self.env['donhang.donhang'].search([('id', 'in', related_ids7)])
            print(self.invoice_list, '1')
