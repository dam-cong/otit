# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime
import logging
from tempfile import TemporaryFile, NamedTemporaryFile
from io import BytesIO, StringIO
import os
import codecs
from docxtpl import DocxTemplate
from docxtpl import DocxTemplate, Listing

_logger = logging.getLogger(__name__)
def kiemtra(data):
    if data:
        return data
    else:
        return ''

def convert_date(self):
    if self:
        return str(self.year) + '年' + \
               str(self.month) + '月' + \
               str(self.day) + '日'
    else:
        return '年　月　日'

class UyNhiem(models.Model):
    _name = 'thue.uynhiem'
    _rec_name = 'authuorized_name_original'

    date_mandate = fields.Date(string='Ngày ủy nhiệm')
    phonetic_name = fields.Char(string='Tên phiên âm')
    original_name = fields.Char(string='Tên hán')
    relationship_proxy = fields.Char(string='Quan hệ')
    post_office = fields.Char(string='Bưu điện')
    address = fields.Char(string='Địa chỉ')
    phone = fields.Char(string='Điện thoại')
    insurance_code = fields.Char(string='Mã số bảo hiểm',size=10)
    authuorized_name_phonetic = fields.Char(string='Tên phiên âm')
    authuorized_name_original = fields.Char(string='Tên Hán')
    name_before_getting_married = fields.Char(string='Tên trước khi lấy chồng')
    year_birth_authorized = fields.Selection(string='Năm sinh',
                                             selection=[('Meiji', 'Meiji'), ('Taishou', 'Taishou'),
                                                        ('Shouwa', 'Shouwa'), ('Heisei', 'Heisei'),
                                                        ('Reiwa', 'Reiwa')])

    day_identity = fields.Char("Ngày", size=2)  # hh_27
    month_identity = fields.Selection([('01', '01'), ('02', '02'), ('03', '03'), ('04', '04'),
                                       ('05', '05'), ('06', '06'), ('07', '07'), ('08', '08'),
                                       ('09', '09'), ('10', '10'), ('11', '11'), ('12', '12'), ], "Tháng")  # hh_28
    year_identity = fields.Char("Năm", size=4)  # hh_29
    date_identity = fields.Char("Ngày cấp", store=False, compute='_date_of_identity')  # hh_07

    @api.one
    @api.depends('day_identity', 'month_identity', 'year_identity')
    def _date_of_identity(self):
        if self.day_identity and self.month_identity and self.year_identity:
            self.date_identity = u"%s年%s月%s日" % (
                self.year_identity, self.month_identity, self.day_identity)
        elif self.month_identity and self.year_identity:
            self.date_identity = u"%s年%s月" % (self.year_identity, self.month_identity)
        elif self.year_identity:
            self.date_identity = u"%s年" % (self.year_identity)
        else:
            self.date_identity = ""

    sex_authorized = fields.Selection(string='Giới tính',
                                      selection=[('Nam', 'Nam'), ('Nữ', 'Nữ')])
    post_office_authorized = fields.Char(string='Bưu điện')
    address_authorized = fields.Char(string='Địa chỉ')
    phone_authorized = fields.Char(string='Điện thoại')
    address_authorized_resident = fields.Char(string='Địa chỉ ghi trên thẻ thường trú')
    content_authorized = fields.Selection(string='Nội dung ủy nhiệm',
                                          selection=[('1. Về thời gian tham gia bảo hiểm',
                                                      '1. Về thời gian tham gia bảo hiểm'),
                                                     ('2. Về số tiền bảo hiểm dự kiến',
                                                      '2. Về số tiền bảo hiểm dự kiến'),
                                                     ('3. Về yêu cầu thanh toán bảo hiểm',
                                                      '3. Về yêu cầu thanh toán bảo hiểm'),
                                                     ('4. Về các thủ tục cấp lại khác nhau',
                                                      '4. Về các thủ tục cấp lại khác nhau'),
                                                     ('5. Về thủ tục liên quan đến thương vong',
                                                      '5. Về thủ tục liên quan đến thương vong'),
                                                     ('6. Về thủ tục tham gia bảo hiểm quốc dân',
                                                      '6. Về thủ tục tham gia bảo hiểm quốc dân'),
                                                     (
                                                         '7. Về việc thanh toán, miễn giảm, chế độ thanh toán đặc biệt của sinh viên đối với chi phí bảo hiểm quốc dân ',
                                                         '7. Về việc thanh toán, miễn giảm, chế độ thanh toán đặc biệt của sinh viên đối với chi phí bảo hiểm quốc dân '),
                                                     ('8. Khác', '8. Khác')])
    content_authorized_other = fields.Char(string='Nội dung khác')

    @api.onchange('content_authorized')
    def onchange_content_authorized(self):
        if self.content_authorized != '8. Khác':
            self.content_authorized_other = ''



    payments = fields.Selection(string='Hình thức thanh toán',
                                selection=[('Mong muốn giao cho người được ủy nhiệm',
                                            'Mong muốn giao cho người được ủy nhiệm'),
                                           ('Mong muốn chuyển cho chính chủ', 'Mong muốn chuyển cho chính chủ')])

    insurance_code_lost = fields.Char(string='Mã số bảo hiểm')
    relationship_lost = fields.Char(string='Quan hệ')
    name_lost = fields.Char(string='Họ và tên')
    birth_lost_1 = state = fields.Selection(string="Ngày sinh", selection=[('明', '明'), ('大', '大'),('昭', '昭'),('平', '平'), ('令', '令'), ])
    birth_lost = fields.Date(string='Ngày sinh')

    @api.multi
    def thue_uynhiem_button(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/uynhiem/download/%s' % (self.id),
            'target': 'self', }

    @api.multi
    def baocao_uynhiem(self, uynhiem):
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_64")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}
            context['nk_0024'] = convert_date(uynhiem.date_mandate)
            context['nk_0017'] = kiemtra(uynhiem.phonetic_name)
            context['nk_0018'] = kiemtra(uynhiem.original_name)
            context['nk_0019'] = kiemtra(uynhiem.relationship_proxy)
            context['nk_0020'] = uynhiem.post_office[0:3] +'-'+uynhiem.post_office[3:] if uynhiem.post_office else ''
            context['nk_0021'] = uynhiem.phone.split("-")[0] if uynhiem.phone else ''
            context['nk_0022'] = uynhiem.phone.split("-")[1] +"-"+uynhiem.phone.split("-")[2] if uynhiem.phone else ''
            context['nk_0023'] = kiemtra(uynhiem.address)

            if uynhiem.insurance_code:
                context['nk_0025_1'] = uynhiem.insurance_code[0]
                context['nk_0025_2'] = uynhiem.insurance_code[1]
                context['nk_0025_3'] = uynhiem.insurance_code[2]
                context['nk_0025_4'] = uynhiem.insurance_code[3]
                context['nk_0025_5'] = uynhiem.insurance_code[4]
                context['nk_0025_6'] = uynhiem.insurance_code[5]
                context['nk_0025_7'] = uynhiem.insurance_code[6]
                context['nk_0025_8'] = uynhiem.insurance_code[7]
                context['nk_0025_9'] = uynhiem.insurance_code[8]
                context['nk_0025_10'] = uynhiem.insurance_code[9]
            else:
                context['nk_0025_1'] = ''
                context['nk_0025_2'] = ''
                context['nk_0025_3'] = ''
                context['nk_0025_4'] = ''
                context['nk_0025_5'] = ''
                context['nk_0025_6'] = ''
                context['nk_0025_7'] = ''
                context['nk_0025_8'] = ''
                context['nk_0025_9'] = ''
                context['nk_0025_10'] = ''

            context['nk_0026'] = kiemtra(uynhiem.authuorized_name_phonetic)
            context['nk_0027'] = kiemtra(uynhiem.authuorized_name_original)
            context['nk_0028'] = kiemtra(uynhiem.name_before_getting_married)

            if uynhiem.year_birth_authorized == u'Meiji':
                docdata.remove_shape(u'id="2" name="Oval 2"')
                docdata.remove_shape(u'id="3" name="Oval 3"')
                docdata.remove_shape(u'id="4" name="Oval 4"')
                docdata.remove_shape(u'id="5" name="Oval 5"')
            elif uynhiem.year_birth_authorized == u'Taishou':
                docdata.remove_shape(u'id="1" name="Oval 1"')
                docdata.remove_shape(u'id="3" name="Oval 3"')
                docdata.remove_shape(u'id="4" name="Oval 4"')
                docdata.remove_shape(u'id="5" name="Oval 5"')
            elif uynhiem.year_birth_authorized == u'Shouwa':
                docdata.remove_shape(u'id="1" name="Oval 1"')
                docdata.remove_shape(u'id="2" name="Oval 2"')
                docdata.remove_shape(u'id="4" name="Oval 4"')
                docdata.remove_shape(u'id="5" name="Oval 5"')
            elif uynhiem.year_birth_authorized == u'Heisei':
                docdata.remove_shape(u'id="1" name="Oval 1"')
                docdata.remove_shape(u'id="3" name="Oval 3"')
                docdata.remove_shape(u'id="2" name="Oval 2"')
                docdata.remove_shape(u'id="5" name="Oval 5"')
            elif uynhiem.year_birth_authorized == u'Reiwa':
                docdata.remove_shape(u'id="1" name="Oval 1"')
                docdata.remove_shape(u'id="3" name="Oval 3"')
                docdata.remove_shape(u'id="4" name="Oval 4"')
                docdata.remove_shape(u'id="2" name="Oval 2"')
            else:
                docdata.remove_shape(u'id="1" name="Oval 1"')
                docdata.remove_shape(u'id="3" name="Oval 3"')
                docdata.remove_shape(u'id="4" name="Oval 4"')
                docdata.remove_shape(u'id="2" name="Oval 2"')
                docdata.remove_shape(u'id="5" name="Oval 5"')

            context['nk_0029'] = kiemtra(uynhiem.date_identity)
            context['nk_0030'] = str(uynhiem.post_office_authorized)[0:3] +'-'+str(uynhiem.post_office_authorized)[3:] if uynhiem.post_office_authorized else ''
            context['nk_0032_1'] = uynhiem.phone_authorized.split("-")[0] if uynhiem.phone_authorized else ''
            context['nk_0032_2'] = uynhiem.phone_authorized.split("-")[1] + "-" + uynhiem.phone_authorized.split("-")[
                2] if uynhiem.phone_authorized else ''

            context['nk_0031'] = kiemtra(uynhiem.address_authorized)
            context['nk_0033'] = kiemtra(uynhiem.address_authorized_resident)

            if uynhiem.sex_authorized == u'Nam':
                docdata.remove_shape(u'id="327" name="Oval 327"')
            elif uynhiem.sex_authorized == u'Nữ':
                docdata.remove_shape(u'id="326" name="Oval 326"')
            else:
                docdata.remove_shape(u'id="327" name="Oval 327"')
                docdata.remove_shape(u'id="326" name="Oval 326"')


            if uynhiem.content_authorized == u'1. Về thời gian tham gia bảo hiểm':
                docdata.remove_shape(u'id="329" name="Oval 329"')
                docdata.remove_shape(u'id="330" name="Oval 330"')
                docdata.remove_shape(u'id="331" name="Oval 331"')
                docdata.remove_shape(u'id="332" name="Oval 332"')
                docdata.remove_shape(u'id="333" name="Oval 333"')
                docdata.remove_shape(u'id="334" name="Oval 334"')
                docdata.remove_shape(u'id="335" name="Oval 335"')
                context['nk_0034'] = ''
            elif uynhiem.content_authorized == u'2. Về số tiền bảo hiểm dự kiến':
                docdata.remove_shape(u'id="328" name="Oval 328"')
                docdata.remove_shape(u'id="330" name="Oval 330"')
                docdata.remove_shape(u'id="331" name="Oval 331"')
                docdata.remove_shape(u'id="332" name="Oval 332"')
                docdata.remove_shape(u'id="333" name="Oval 333"')
                docdata.remove_shape(u'id="334" name="Oval 334"')
                docdata.remove_shape(u'id="335" name="Oval 335"')
                context['nk_0034'] = ''
            elif uynhiem.content_authorized == u'3. Về yêu cầu thanh toán bảo hiểm':
                docdata.remove_shape(u'id="328" name="Oval 328"')
                docdata.remove_shape(u'id="329" name="Oval 329"')
                docdata.remove_shape(u'id="331" name="Oval 331"')
                docdata.remove_shape(u'id="332" name="Oval 332"')
                docdata.remove_shape(u'id="333" name="Oval 333"')
                docdata.remove_shape(u'id="334" name="Oval 334"')
                docdata.remove_shape(u'id="335" name="Oval 335"')
                context['nk_0034'] = ''
            elif uynhiem.content_authorized == u'4. Về các thủ tục cấp lại khác nhau':
                docdata.remove_shape(u'id="328" name="Oval 328"')
                docdata.remove_shape(u'id="329" name="Oval 329"')
                docdata.remove_shape(u'id="330" name="Oval 330"')
                docdata.remove_shape(u'id="332" name="Oval 332"')
                docdata.remove_shape(u'id="333" name="Oval 333"')
                docdata.remove_shape(u'id="334" name="Oval 334"')
                docdata.remove_shape(u'id="335" name="Oval 335"')
                context['nk_0034'] = ''
            elif uynhiem.content_authorized == u'5. Về thủ tục liên quan đến thương vong':
                docdata.remove_shape(u'id="328" name="Oval 328"')
                docdata.remove_shape(u'id="329" name="Oval 329"')
                docdata.remove_shape(u'id="330" name="Oval 330"')
                docdata.remove_shape(u'id="331" name="Oval 331"')
                docdata.remove_shape(u'id="333" name="Oval 333"')
                docdata.remove_shape(u'id="334" name="Oval 334"')
                docdata.remove_shape(u'id="335" name="Oval 335"')
                context['nk_0034'] = ''
            elif uynhiem.content_authorized == u'6. Về thủ tục tham gia bảo hiểm quốc dân':
                docdata.remove_shape(u'id="328" name="Oval 328"')
                docdata.remove_shape(u'id="329" name="Oval 329"')
                docdata.remove_shape(u'id="330" name="Oval 330"')
                docdata.remove_shape(u'id="331" name="Oval 331"')
                docdata.remove_shape(u'id="332" name="Oval 332"')
                docdata.remove_shape(u'id="334" name="Oval 334"')
                docdata.remove_shape(u'id="335" name="Oval 335"')
                context['nk_0034'] = ''
            elif uynhiem.content_authorized == u'7. Về việc thanh toán, miễn giảm, chế độ thanh toán đặc biệt của sinh viên đối với chi phí bảo hiểm quốc dân':
                docdata.remove_shape(u'id="328" name="Oval 328"')
                docdata.remove_shape(u'id="329" name="Oval 329"')
                docdata.remove_shape(u'id="330" name="Oval 330"')
                docdata.remove_shape(u'id="331" name="Oval 331"')
                docdata.remove_shape(u'id="332" name="Oval 332"')
                docdata.remove_shape(u'id="333" name="Oval 333"')
                docdata.remove_shape(u'id="335" name="Oval 335"')
                context['nk_0034'] = ''
            elif uynhiem.content_authorized == u'8. Khác':
                docdata.remove_shape(u'id="328" name="Oval 328"')
                docdata.remove_shape(u'id="329" name="Oval 329"')
                docdata.remove_shape(u'id="330" name="Oval 330"')
                docdata.remove_shape(u'id="331" name="Oval 331"')
                docdata.remove_shape(u'id="332" name="Oval 332"')
                docdata.remove_shape(u'id="333" name="Oval 333"')
                docdata.remove_shape(u'id="334" name="Oval 334"')
                context['nk_0034'] = kiemtra(uynhiem.content_authorized_other)
            else:
                docdata.remove_shape(u'id="328" name="Oval 328"')
                docdata.remove_shape(u'id="329" name="Oval 329"')
                docdata.remove_shape(u'id="330" name="Oval 330"')
                docdata.remove_shape(u'id="331" name="Oval 331"')
                docdata.remove_shape(u'id="332" name="Oval 332"')
                docdata.remove_shape(u'id="333" name="Oval 333"')
                docdata.remove_shape(u'id="334" name="Oval 334"')
                docdata.remove_shape(u'id="335" name="Oval 335"')
                context['nk_0034'] = ''

            if uynhiem.payments == u'Mong muốn giao cho người được ủy nhiệm':
                docdata.remove_shape(u'id="337" name="Oval 337"')
            elif uynhiem.payments == u'Mong muốn chuyển cho chính chủ':
                docdata.remove_shape(u'id="336" name="Oval 336"')
            else:
                docdata.remove_shape(u'id="336" name="Oval 336"')
                docdata.remove_shape(u'id="337" name="Oval 337"')

            context['nk_0035'] = kiemtra(uynhiem.insurance_code_lost)
            context['nk_0036'] = kiemtra(uynhiem.name_lost)
            context['nk_0037'] = kiemtra(uynhiem.relationship_lost)

            if uynhiem.birth_lost_1 == '明':
                docdata.remove_shape(u'id="9" name="Oval 9"')
                docdata.remove_shape(u'id="10" name="Oval 10"')
                docdata.remove_shape(u'id="11" name="Oval 11"')
                docdata.remove_shape(u'id="12" name="Oval 12"')
            elif uynhiem.birth_lost_1 == '大':
                docdata.remove_shape(u'id="8" name="Oval 8"')
                docdata.remove_shape(u'id="10" name="Oval 10"')
                docdata.remove_shape(u'id="11" name="Oval 11"')
                docdata.remove_shape(u'id="12" name="Oval 12"')
            elif uynhiem.birth_lost_1 == '昭':
                docdata.remove_shape(u'id="8" name="Oval 8"')
                docdata.remove_shape(u'id="9" name="Oval 9"')
                docdata.remove_shape(u'id="11" name="Oval 11"')
                docdata.remove_shape(u'id="12" name="Oval 12"')
            elif uynhiem.birth_lost_1 == '平':
                docdata.remove_shape(u'id="8" name="Oval 8"')
                docdata.remove_shape(u'id="9" name="Oval 9"')
                docdata.remove_shape(u'id="10" name="Oval 10"')
                docdata.remove_shape(u'id="12" name="Oval 12"')
            elif uynhiem.birth_lost_1 == '令':
                docdata.remove_shape(u'id="8" name="Oval 8"')
                docdata.remove_shape(u'id="9" name="Oval 9"')
                docdata.remove_shape(u'id="10" name="Oval 10"')
                docdata.remove_shape(u'id="11" name="Oval 11"')
            else:
                docdata.remove_shape(u'id="8" name="Oval 8"')
                docdata.remove_shape(u'id="9" name="Oval 9"')
                docdata.remove_shape(u'id="10" name="Oval 10"')
                docdata.remove_shape(u'id="11" name="Oval 11"')
                docdata.remove_shape(u'id="12" name="Oval 12"')
            context['nk_0038'] = kiemtra(uynhiem.date_identity)

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None