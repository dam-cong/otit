# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime
import logging
from tempfile import TemporaryFile, NamedTemporaryFile
from io import BytesIO, StringIO
import os
import codecs
from docxtpl import DocxTemplate
from docxtpl import DocxTemplate, Listing

_logger = logging.getLogger(__name__)
def kiemtra(data):
    if data:
        return data
    else:
        return ''

def convert_date(self):
    if self:
        return str(self.year) + '年' + \
               str(self.month) + '月' + \
               str(self.day) + '日'
    else:
        return '年　月　日'


class LuongHuu(models.Model):
    _name = 'thue.luonghuu'
    _rec_name = 'fullname_student'

    # Table1
    day_write = fields.Date(string='Ngày lập văn bản')
    yes_no_vt = fields.Selection(string='Có vĩnh trú hay không(ngày được phép)',
                                 selection=[('Có', 'Có'), ('Không', 'Không')])
    date_allowed = fields.Date(string='Ngày được phép')

    @api.onchange('yes_no_vt')
    def _onchange_yes_no_vt(self):
        if self.yes_no_vt:
            self.date_allowed = ''

    # Table2
    fullname_student = fields.Many2one(comodel_name='thuctapsinh.thuctapsinh', string="Họ và tên")
    birthday = fields.Date(string='Ngày sinh', related='fullname_student.ngaysinh_tts')
    nationality = fields.Char(string='Quốc tịch', related='fullname_student.quoctich_tts_v')
    address_after_leaveJP = fields.Text(string='Địa chỉ sau khi rời khỏi Nhật Bản')
    country = fields.Char(string='Quốc gia')
    city_after_leaveJP = fields.Char(string='Tên Tỉnh/Thành phố sau khi rời khỏi Nhật Bản')

    # Table3
    bank_name = fields.Char(string='Tên ngân hàng')
    bank_branch_name = fields.Char(string='Tên chi nhánh')
    bank_branch_address = fields.Text(string='Địa chỉ chi nhánh')
    bank_country = fields.Char(string='Quốc gia')
    bank_branch_city = fields.Char(string='Tỉnh/Thành phố chi nhánh')
    bank_account_number = fields.Char(string='Số tài khoản')
    bank_account_name = fields.Char(string='Tên tài khoản của người đăng ký')
    bank_account_name_jp = fields.Char(string='Tên tài khoản của người đăng ký')

    # Table4
    number_of_basic_benefits = fields.Char(string='Mã số trợ cấp cơ bản', size=4)
    number_of_basic_benefits_2 = fields.Char(string='Mã số trợ cấp cơ bản', size=6)

    number_of_retirement_regimes = fields.Char(string='Số hiệu - ký hiệu của từng chế độ lương hưu', size=4)
    number_of_retirement_regimes_2 = fields.Char(string='Số hiệu - ký hiệu của từng chế độ lương hưu', size=6)

    # Table5
    workplace_name = fields.Char(
        string="(1) Tên nơi làm việc (chủ tàu) và tên tàu thuyền nếu đó là thủy thủ của tàu thuyền đó")
    work_address = fields.Char(string="(2) Địa chỉ nơi làm việc (chủ tàu) hoặc địa chỉ khi tham gia trợ cấp quốc dân")
    work_time = fields.Char(string="(3) Thời gian làm việc hoặc thời gian tham gia trợ cấp quốc dân")
    classification_of_benefits = fields.Char(string="(4) Phân loại chế độ trợ cấp tham gia")

    workplace_name_1 = fields.Text()
    work_address_1 = fields.Text()
    work_time_1_1 = fields.Date()
    work_time_1_2 = fields.Date()
    classification_of_benefits_1 = fields.Selection(string="Phân loại chế độ trợ cấp",
                                                    selection=[('1.Trợ cấp quốc dân', '1.Trợ cấp quốc dân'),
                                                               ('2.Bảo hiểm trợ cấp phúc lợi xã hội',
                                                                '2.Bảo hiểm trợ cấp phúc lợi xã hội'),
                                                               ('3.Bảo hiểm trợ hàng hải',
                                                                '3.Bảo hiểm trợ hàng hải'),
                                                               ('4.Hiệp hội hỗ tương',
                                                                '4.Hiệp hội hỗ tương')])

    workplace_name_2 = fields.Text()
    work_address_2 = fields.Text()
    work_time_2_1 = fields.Date()
    work_time_2_2 = fields.Date()
    classification_of_benefits_2 = fields.Selection(string="Phân loại chế độ trợ cấp",
                                                    selection=[('1.Trợ cấp quốc dân', '1.Trợ cấp quốc dân'),
                                                               ('2.Bảo hiểm trợ cấp phúc lợi xã hội',
                                                                '2.Bảo hiểm trợ cấp phúc lợi xã hội'),
                                                               ('3.Bảo hiểm trợ hàng hải',
                                                                '3.Bảo hiểm trợ hàng hải'),
                                                               ('4.Hiệp hội hỗ tương',
                                                                '4.Hiệp hội hỗ tương')])

    workplace_name_3 = fields.Text()
    work_address_3 = fields.Text()
    work_time_3_1 = fields.Date()
    work_time_3_2 = fields.Date()
    classification_of_benefits_3 = fields.Selection(string="Phân loại chế độ trợ cấp",
                                                    selection=[('1.Trợ cấp quốc dân', '1.Trợ cấp quốc dân'),
                                                               ('2.Bảo hiểm trợ cấp phúc lợi xã hội',
                                                                '2.Bảo hiểm trợ cấp phúc lợi xã hội'),
                                                               ('3.Bảo hiểm trợ hàng hải',
                                                                '3.Bảo hiểm trợ hàng hải'),
                                                               ('4.Hiệp hội hỗ tương',
                                                                '4.Hiệp hội hỗ tương')])

    workplace_name_4 = fields.Text()
    work_address_4 = fields.Text()
    work_time_4_1 = fields.Date()
    work_time_4_2 = fields.Date()
    classification_of_benefits_4 = fields.Selection(string="Phân loại chế độ trợ cấp",
                                                    selection=[('1.Trợ cấp quốc dân', '1.Trợ cấp quốc dân'),
                                                               ('2.Bảo hiểm trợ cấp phúc lợi xã hội',
                                                                '2.Bảo hiểm trợ cấp phúc lợi xã hội'),
                                                               ('3.Bảo hiểm trợ hàng hải',
                                                                '3.Bảo hiểm trợ hàng hải'),
                                                               ('4.Hiệp hội hỗ tương',
                                                                '4.Hiệp hội hỗ tương')])

    @api.multi
    def thue_luonghuu_button(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/luonghuu/download/%s' % (self.id),
            'target': 'self', }

    @api.multi
    def baocao_luonghuu(self, luonghuu):
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_63")], limit=1)
        print('file_63')
        print(docs)

        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            context['nk_0001_y'] = kiemtra(luonghuu.day_write.year)
            context['nk_0001_m'] = kiemtra(luonghuu.day_write.month)
            context['nk_0001_d'] = kiemtra(luonghuu.day_write.day)

            if luonghuu.yes_no_vt == u'Có':
                docdata.remove_shape(u'id="259" name="Oval 259"')
                context['nk_0002'] = convert_date(luonghuu.date_allowed)
            elif luonghuu.yes_no_vt == u'Không':
                docdata.remove_shape(u'id="260" name="Oval 260"')
                context['nk_0002'] = ''
            else:
                docdata.remove_shape(u'id="259" name="Oval 259"')
                docdata.remove_shape(u'id="260" name="Oval 260"')
                context['nk_0002'] = ''

            context['nk_0003'] = kiemtra(luonghuu.fullname_student.ten_lt_tts)
            if luonghuu.birthday:
                context['nk_0004_y_1'] = str(luonghuu.birthday.year)[0]
                context['nk_0004_y_2'] = str(luonghuu.birthday.year)[1]
                context['nk_0004_y_3'] = str(luonghuu.birthday.year)[2]
                context['nk_0004_y_4'] = str(luonghuu.birthday.year)[3]
                if luonghuu.birthday.month < 10:
                    context['nk_0004_m_1'] = '0'
                    context['nk_0004_m_2'] = str(luonghuu.birthday.month)[0]
                else:
                    context['nk_0004_m_1'] = str(luonghuu.birthday.month)[0]
                    context['nk_0004_m_2'] = str(luonghuu.birthday.month)[1]


                if luonghuu.birthday.day < 10:
                    context['nk_0004_d_1'] = '0'
                    context['nk_0004_d_2'] = str(luonghuu.birthday.day)[0]
                else:
                    context['nk_0004_d_1'] = str(luonghuu.birthday.day)[0]
                    context['nk_0004_d_2'] = str(luonghuu.birthday.day)[1]
            else:
                context['nk_0004_y_1'] = ''
                context['nk_0004_y_2'] = ''
                context['nk_0004_y_3'] = ''
                context['nk_0004_y_4'] = ''
                context['nk_0004_m_1'] = ''
                context['nk_0004_m_2'] = ''
                context['nk_0004_d_1'] = ''
                context['nk_0004_d_2'] = ''

            context['nk_0005'] = kiemtra(luonghuu.nationality)
            context['nk_0006'] = kiemtra(luonghuu.address_after_leaveJP)
            context['nk_0006_tp'] = kiemtra(luonghuu.city_after_leaveJP)
            context['nk_0007'] = kiemtra(luonghuu.country)

            context['nk_0008'] = kiemtra(luonghuu.bank_name)
            context['nk_0009'] = kiemtra(luonghuu.bank_branch_name)
            context['nk_0010'] = kiemtra(luonghuu.bank_branch_address)
            context['nk_0010_tp'] = kiemtra(luonghuu.bank_branch_city)
            context['nk_0011'] = kiemtra(luonghuu.bank_country)
            context['nk_0012'] = kiemtra(luonghuu.bank_account_number)
            context['nk_0013'] = kiemtra(luonghuu.bank_account_name)
            context['nk_0014'] = kiemtra(luonghuu.bank_account_name_jp)

            if luonghuu.number_of_basic_benefits:
                context['nk_0015_1'] = luonghuu.number_of_basic_benefits[0]
                context['nk_0015_2'] = luonghuu.number_of_basic_benefits[1]
                context['nk_0015_3'] = luonghuu.number_of_basic_benefits[2]
                context['nk_0015_4'] = luonghuu.number_of_basic_benefits[3]
            else:
                context['nk_0015_1'] = ''
                context['nk_0015_2'] = ''
                context['nk_0015_3'] = ''
                context['nk_0015_4'] = ''

            if luonghuu.number_of_basic_benefits_2:
                context['nk_0015_5'] = luonghuu.number_of_basic_benefits_2[0]
                context['nk_0015_6'] = luonghuu.number_of_basic_benefits_2[1]
                context['nk_0015_7'] = luonghuu.number_of_basic_benefits_2[2]
                context['nk_0015_8'] = luonghuu.number_of_basic_benefits_2[3]
                context['nk_0015_9'] = luonghuu.number_of_basic_benefits_2[4]
                context['nk_0015_10'] = luonghuu.number_of_basic_benefits_2[5]
            else:
                context['nk_0015_5'] = ''
                context['nk_0015_6'] = ''
                context['nk_0015_7'] = ''
                context['nk_0015_8'] = ''
                context['nk_0015_9'] = ''
                context['nk_0015_10'] = ''


            if luonghuu.number_of_retirement_regimes:
                context['nk_0016_1'] = luonghuu.number_of_retirement_regimes[0]
                context['nk_0016_2'] = luonghuu.number_of_retirement_regimes[1]
                context['nk_0016_3'] = luonghuu.number_of_retirement_regimes[2]
                context['nk_0016_4'] = luonghuu.number_of_retirement_regimes[3]
            else:
                context['nk_0016_1'] = ''
                context['nk_0016_2'] = ''
                context['nk_0016_3'] = ''
                context['nk_0016_4'] = ''

            if luonghuu.number_of_retirement_regimes_2:
                context['nk_0016_5'] = luonghuu.number_of_retirement_regimes_2[0]
                context['nk_0016_6'] = luonghuu.number_of_retirement_regimes_2[1]
                context['nk_0016_7'] = luonghuu.number_of_retirement_regimes_2[2]
                context['nk_0016_8'] = luonghuu.number_of_retirement_regimes_2[3]
                context['nk_0016_9'] = luonghuu.number_of_retirement_regimes_2[4]
                context['nk_0016_10'] = luonghuu.number_of_retirement_regimes_2[5]
            else:
                context['nk_0016_5'] = ''
                context['nk_0016_6'] = ''
                context['nk_0016_7'] = ''
                context['nk_0016_8'] = ''
                context['nk_0016_9'] = ''
                context['nk_0016_10'] = ''

            context['nk_0018_1'] = kiemtra(luonghuu.workplace_name_1)
            context['nk_0019_1'] = kiemtra(luonghuu.work_address_1)
            context['nk_0020_1'] = convert_date(luonghuu.work_time_1_1) if luonghuu.work_time_1_1 else ''
            context['nk_0021_1'] = convert_date(luonghuu.work_time_1_2) if luonghuu.work_time_1_2 else ''
            if luonghuu.classification_of_benefits_1 == u'1.Trợ cấp quốc dân':
                docdata.remove_shape(u'id="262" name="Oval 262"')
                docdata.remove_shape(u'id="263" name="Oval 263"')
                docdata.remove_shape(u'id="264" name="Oval 264"')
            elif luonghuu.classification_of_benefits_1 == u'2.Bảo hiểm trợ cấp phúc lợi xã hội':
                docdata.remove_shape(u'id="261" name="Oval 261"')
                docdata.remove_shape(u'id="263" name="Oval 263"')
                docdata.remove_shape(u'id="264" name="Oval 264"')
            elif luonghuu.classification_of_benefits_1 == u'3.Bảo hiểm trợ hàng hải':
                docdata.remove_shape(u'id="261" name="Oval 261"')
                docdata.remove_shape(u'id="262" name="Oval 262"')
                docdata.remove_shape(u'id="264" name="Oval 264"')
            elif luonghuu.classification_of_benefits_1 == u'4.Hiệp hội hỗ tương':
                docdata.remove_shape(u'id="261" name="Oval 261"')
                docdata.remove_shape(u'id="263" name="Oval 263"')
                docdata.remove_shape(u'id="262" name="Oval 262"')
            else:
                docdata.remove_shape(u'id="261" name="Oval 261"')
                docdata.remove_shape(u'id="262" name="Oval 262"')
                docdata.remove_shape(u'id="263" name="Oval 263"')
                docdata.remove_shape(u'id="264" name="Oval 264"')

            context['nk_0018_2'] = kiemtra(luonghuu.workplace_name_2)
            context['nk_0019_2'] = kiemtra(luonghuu.work_address_2)
            context['nk_0020_2'] = convert_date(luonghuu.work_time_2_1) if luonghuu.work_time_2_1 else ''
            context['nk_0021_2'] = convert_date(luonghuu.work_time_2_2) if luonghuu.work_time_2_2 else ''
            if luonghuu.classification_of_benefits_2 == u'1.Trợ cấp quốc dân':
                docdata.remove_shape(u'id="266" name="Oval 266"')
                docdata.remove_shape(u'id="267" name="Oval 267"')
                docdata.remove_shape(u'id="268" name="Oval 268"')
            elif luonghuu.classification_of_benefits_2 == u'2.Bảo hiểm trợ cấp phúc lợi xã hội':
                docdata.remove_shape(u'id="265" name="Oval 265"')
                docdata.remove_shape(u'id="267" name="Oval 267"')
                docdata.remove_shape(u'id="268" name="Oval 268"')
            elif luonghuu.classification_of_benefits_2 == u'3.Bảo hiểm trợ hàng hải':
                docdata.remove_shape(u'id="266" name="Oval 266"')
                docdata.remove_shape(u'id="265" name="Oval 265"')
                docdata.remove_shape(u'id="268" name="Oval 268"')
            elif luonghuu.classification_of_benefits_2 == u'4.Hiệp hội hỗ tương':
                docdata.remove_shape(u'id="266" name="Oval 266"')
                docdata.remove_shape(u'id="267" name="Oval 267"')
                docdata.remove_shape(u'id="265" name="Oval 265"')
            else:
                docdata.remove_shape(u'id="265" name="Oval 265"')
                docdata.remove_shape(u'id="266" name="Oval 266"')
                docdata.remove_shape(u'id="267" name="Oval 267"')
                docdata.remove_shape(u'id="268" name="Oval 268"')

            context['nk_0018_3'] = kiemtra(luonghuu.workplace_name_3)
            context['nk_0019_3'] = kiemtra(luonghuu.work_address_3)
            context['nk_0020_3'] = convert_date(luonghuu.work_time_3_1) if luonghuu.work_time_3_1 else ''
            context['nk_0021_3'] = convert_date(luonghuu.work_time_3_2) if luonghuu.work_time_3_2 else ''
            if luonghuu.classification_of_benefits_3 == u'1.Trợ cấp quốc dân':
                docdata.remove_shape(u'id="270" name="Oval 270"')
                docdata.remove_shape(u'id="271" name="Oval 271"')
                docdata.remove_shape(u'id="272" name="Oval 272"')
            elif luonghuu.classification_of_benefits_3 == u'2.Bảo hiểm trợ cấp phúc lợi xã hội':
                docdata.remove_shape(u'id="269" name="Oval 269"')
                docdata.remove_shape(u'id="271" name="Oval 271"')
                docdata.remove_shape(u'id="272" name="Oval 272"')
            elif luonghuu.classification_of_benefits_3 == u'3.Bảo hiểm trợ hàng hải':
                docdata.remove_shape(u'id="269" name="Oval 269"')
                docdata.remove_shape(u'id="270" name="Oval 270"')
                docdata.remove_shape(u'id="272" name="Oval 272"')
            elif luonghuu.classification_of_benefits_3 == u'4.Hiệp hội hỗ tương':
                docdata.remove_shape(u'id="269" name="Oval 269"')
                docdata.remove_shape(u'id="271" name="Oval 271"')
                docdata.remove_shape(u'id="270" name="Oval 270"')
            else:
                docdata.remove_shape(u'id="269" name="Oval 269"')
                docdata.remove_shape(u'id="271" name="Oval 271"')
                docdata.remove_shape(u'id="272" name="Oval 272"')
                docdata.remove_shape(u'id="270" name="Oval 270"')

            context['nk_0018_4'] = kiemtra(luonghuu.workplace_name_4)
            context['nk_0019_4'] = kiemtra(luonghuu.work_address_4)
            context['nk_0020_4'] = convert_date(luonghuu.work_time_4_1) if luonghuu.work_time_4_1 else ''
            context['nk_0021_4'] = convert_date(luonghuu.work_time_4_2) if luonghuu.work_time_4_2 else ''
            if luonghuu.classification_of_benefits_4 == u'1.Trợ cấp quốc dân':
                docdata.remove_shape(u'id="274" name="Oval 274"')
                docdata.remove_shape(u'id="275" name="Oval 275"')
                docdata.remove_shape(u'id="276" name="Oval 276"')
            elif luonghuu.classification_of_benefits_4 == u'2.Bảo hiểm trợ cấp phúc lợi xã hội':
                docdata.remove_shape(u'id="273" name="Oval 273"')
                docdata.remove_shape(u'id="275" name="Oval 275"')
                docdata.remove_shape(u'id="276" name="Oval 276"')
            elif luonghuu.classification_of_benefits_4 == u'3.Bảo hiểm trợ hàng hải':
                docdata.remove_shape(u'id="273" name="Oval 273"')
                docdata.remove_shape(u'id="274" name="Oval 274"')
                docdata.remove_shape(u'id="276" name="Oval 276"')
            elif luonghuu.classification_of_benefits_4 == u'4.Hiệp hội hỗ tương':
                docdata.remove_shape(u'id="273" name="Oval 273"')
                docdata.remove_shape(u'id="275" name="Oval 275"')
                docdata.remove_shape(u'id="274" name="Oval 274"')
            else:
                docdata.remove_shape(u'id="273" name="Oval 273"')
                docdata.remove_shape(u'id="275" name="Oval 275"')
                docdata.remove_shape(u'id="276" name="Oval 276"')
                docdata.remove_shape(u'id="274" name="Oval 274"')

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None
