# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime
import logging
from tempfile import TemporaryFile, NamedTemporaryFile
from io import BytesIO, StringIO
import os
import codecs
from docxtpl import DocxTemplate
from docxtpl import DocxTemplate, Listing

_logger = logging.getLogger(__name__)

def kiemtra(data):
    if data:
        return data
    else:
        return ''

def convert_date(self):
    if self:
        return str(self.year) + '年' + \
               str(self.month) + '月' + \
               str(self.day) + '日'
    else:
        return '年　月　日'

class ThuNhap(models.Model):
    _name = 'thue.thunhap'
    _rec_name = 'taxpayer_name'

    director_tax_department = fields.Char(string='Cục trưởng cục thuế')
    date_filing_tax = fields.Date(string='Ngày nộp')
    post_office_tax = fields.Char(string='Bưu điện')
    address_taxt = fields.Char(string='Địa chỉ')
    payment_address_tax = fields.Selection([('Địa chỉ', 'Địa chỉ'),('Nơi cư trú', 'Nơi cư trú'),('Nơi làm việc', 'Nơi làm việc'),('Khác', 'Khác'),],string='Địa chỉ nộp thuế')
    phone_tax = fields.Char(string='Điện thoại')
    post_office_tax_1 = fields.Char(string='Bưu điện')
    payment_address_tax_1 = fields.Char(string='Địa chỉ nộp thuế(ngoài mục trên)')
    phone_tax_1 = fields.Char(string='Điện thoại')
    name_tax = fields.Char(string='Họ và tên')
    day_identity = fields.Char("Ngày", size=2)  # hh_27
    month_identity = fields.Selection([('01', '01'), ('02', '02'), ('03', '03'), ('04', '04'),
                                       ('05', '05'), ('06', '06'), ('07', '07'), ('08', '08'),
                                       ('09', '09'), ('10', '10'), ('11', '11'), ('12', '12'), ], "Tháng")  # hh_28
    year_identity = fields.Char("Năm", size=4)  # hh_29
    date_identity = fields.Char("Ngày cấp", store=False, compute='_date_of_identity')  # hh_07

    @api.one
    @api.depends('day_identity', 'month_identity', 'year_identity')
    def _date_of_identity(self):
        if self.day_identity and self.month_identity and self.year_identity:
            self.date_identity = u"%s年%s月%s日" % (
                self.year_identity, self.month_identity, self.day_identity)
        elif self.month_identity and self.year_identity:
            self.date_identity = u"%s年%s月" % (self.year_identity, self.month_identity)
        elif self.year_identity:
            self.date_identity = u"%s年" % (self.year_identity)
        else:
            self.date_identity = ""
    name_tax_han = fields.Char(string='Họ và tên(Hán)')
    brith_day_tax = fields.Selection(string='Năm sinh',
                                             selection=[('Taishou', 'Taishou'),
                                                        ('Shouwa', 'Shouwa'), ('Heisei', 'Heisei'),
                                                        ('Reiwa', 'Reiwa')])
    id_munber_tax = fields.Char(string='Mã số cá nhân')
    job_tax = fields.Char(string='Ngành nghề')
    apartment_number_tax = fields.Char(string='Số nhà(Furi)')
    apartment_number_tax_han = fields.Char(string='Số nhà(Hán)')

    taxpayer_name = fields.Char(string='Họ tên(Furi)')
    taxpayer_name_han = fields.Char(string='Họ tên(Hán)')
    post_taxpayer = fields.Char(string='Số bưu điện')
    taxpayer_address = fields.Char(string='Địa chỉ')
    taxpayer_address_2 = fields.Char(string='Địa chỉ nơi cư trú')
    taxpayer_relate = fields.Char(string='Quan hệ')
    taxpayer_job = fields.Char(string='Nghề nghiệp')
    taxpayer_phone = fields.Char(string='Điện thoại')

    taxpayer_address_residence = fields.Char(string='Địa chỉ hoặc nơi cưu trú')
    taxpayer_rationale = fields.Text(string='Lý do')

    consult_departure_date = fields.Date(string='Ngày xuất cảnh')
    consult_entry_date = fields.Date(string='Ngày nhập cảnh')

    income_japan = fields.Selection(string='Thu nhập được tạo ra ở Nhật Bản',
                                    selection=[('Kinh doanh', 'Kinh doanh'), ('Bất động sản', 'Bất động sản'),
                                               ('Lương', 'Lương'), ('Chuyển nhượng', 'Chuyển nhượng'),
                                               ('Ngoài các nguồn nếu trên', 'Ngoài các nguồn nếu trên')])
    addition_above_sources = fields.Char(string='Ngoài các nguồn nên trên')

    @api.onchange('income_japan')
    def _onchange_income_japan(self):
        if self.income_japan:
            self.addition_above_sources = ''

    sources_other = fields.Char(string='Nguồn khác')

    tax_accounting_related = fields.Char(string='Kế toán thuế có liên quan')
    tax_accounting_related_phone = fields.Char(string='Điện thoại')

    @api.multi
    def thue_thunhap_button(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/thunhap/download/%s' % (self.id),
            'target': 'self', }

    @api.multi
    def baocao_thunhap(self, thunhap):
        docs = self.env['thuctapsinh.baocao'].search([('name', '=', "file_65")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            context['nk_0039'] = kiemtra(thunhap.director_tax_department)
            if thunhap.date_filing_tax:
                context['nk_0040_y'] = thunhap.date_filing_tax.year
                context['nk_0040_m'] = thunhap.date_filing_tax.month
                context['nk_0040_d'] = thunhap.date_filing_tax.day
            else:
                context['nk_0040_y'] = ''
                context['nk_0040_m'] = ''
                context['nk_0040_d'] = ''


            if thunhap.payment_address_tax == 'Khác':
                context['nk_0043'] = thunhap.post_office_tax[0:3] + '-' + thunhap.post_office_tax[
                                                                          3:] if thunhap.post_office_tax else ''
                context['nk_0044'] = kiemtra(thunhap.address_taxt)
                context['nk_0045'] = kiemtra(thunhap.phone_tax)
                context['nk_0040'] = ''
                context['nk_0041'] = ''
                context['nk_0042'] = ''
            else:
                context['nk_0040'] = thunhap.post_office_tax[0:3] +'-'+thunhap.post_office_tax[3:] if thunhap.post_office_tax else ''
                context['nk_0041'] = kiemtra(thunhap.address_taxt)
                context['nk_0042'] = kiemtra(thunhap.phone_tax)
                context['nk_0043'] = ''
                context['nk_0044'] = ''
                context['nk_0045'] = ''

            if thunhap.payment_address_tax == 'Địa chỉ':
                docdata.remove_shape(u'id="18" name="Oval 18"')
                docdata.remove_shape(u'id="19" name="Oval 19"')
            elif thunhap.payment_address_tax == 'Nơi cư trú':
                docdata.remove_shape(u'id="17" name="Oval 17"')
                docdata.remove_shape(u'id="19" name="Oval 19"')
            elif thunhap.payment_address_tax == 'Nơi làm việc':
                docdata.remove_shape(u'id="17" name="Oval 17"')
                docdata.remove_shape(u'id="18" name="Oval 18"')
            elif thunhap.payment_address_tax == 'Khác':
                docdata.remove_shape(u'id="17" name="Oval 17"')
                docdata.remove_shape(u'id="18" name="Oval 18"')
                docdata.remove_shape(u'id="19" name="Oval 19"')
            else:
                docdata.remove_shape(u'id="17" name="Oval 17"')
                docdata.remove_shape(u'id="18" name="Oval 18"')
                docdata.remove_shape(u'id="19" name="Oval 19"')


            context['nk_0046'] = kiemtra(thunhap.name_tax)
            context['nk_0047'] = kiemtra(thunhap.name_tax_han)
            context['nk_0048'] = kiemtra(thunhap.date_identity)
            context['nk_0049'] = kiemtra(thunhap.id_munber_tax)
            context['nk_0050'] = kiemtra(thunhap.job_tax)

            context['nk_0051'] = kiemtra(thunhap.apartment_number_tax)
            context['nk_0052'] = kiemtra(thunhap.apartment_number_tax_han)
            context['nk_0053'] = thunhap.post_taxpayer[0:3] +'-'+thunhap.post_taxpayer[3:] if thunhap.post_taxpayer else ''
            context['nk_0054'] = kiemtra(thunhap.taxpayer_address)
            context['nk_0055'] = kiemtra(thunhap.taxpayer_address_2)
            context['nk_0056'] = kiemtra(thunhap.taxpayer_name)
            context['nk_0057'] = kiemtra(thunhap.taxpayer_name_han)
            context['nk_0058'] = kiemtra(thunhap.taxpayer_job)
            context['nk_0059'] = kiemtra(thunhap.taxpayer_relate)
            context['nk_0060'] = kiemtra(thunhap.taxpayer_phone)
            context['nk_0061'] = kiemtra(thunhap.taxpayer_address_residence)
            context['nk_0062'] = kiemtra(thunhap.taxpayer_rationale)

            if thunhap.consult_departure_date:
                context['nk_0063_y'] = thunhap.consult_departure_date.year
                context['nk_0063_m'] = thunhap.consult_departure_date.month
                context['nk_0063_d'] = thunhap.consult_departure_date.day
            else:
                context['nk_0063_y'] = ''
                context['nk_0063_m'] = ''
                context['nk_0063_d'] = ''

            if thunhap.consult_entry_date:
                context['nk_0064_y'] = thunhap.consult_entry_date.year
                context['nk_0064_m'] = thunhap.consult_entry_date.month
                context['nk_0064_d'] = thunhap.consult_entry_date.day
            else:
                context['nk_0064_y'] = ''
                context['nk_0064_m'] = ''
                context['nk_0064_d'] = ''

            context['nk_0066'] = kiemtra(thunhap.sources_other)
            context['nk_0067'] = kiemtra(thunhap.tax_accounting_related)
            context['nk_0068'] = kiemtra(thunhap.tax_accounting_related_phone)

            if thunhap.income_japan == u'Kinh doanh':
                docdata.remove_shape(u'id="9" name="Oval 9"')
                docdata.remove_shape(u'id="12" name="Oval 12"')
                docdata.remove_shape(u'id="15" name="Oval 15"')
                docdata.remove_shape(u'id="16" name="Oval 16"')
                context['nk_0065'] = ''
            elif thunhap.income_japan == u'Bất động sản':
                docdata.remove_shape(u'id="8" name="Oval 8"')
                docdata.remove_shape(u'id="12" name="Oval 12"')
                docdata.remove_shape(u'id="15" name="Oval 15"')
                docdata.remove_shape(u'id="16" name="Oval 16"')
                context['nk_0065'] = ''
            elif thunhap.income_japan == u'Lương':
                docdata.remove_shape(u'id="8" name="Oval 8"')
                docdata.remove_shape(u'id="9" name="Oval 9"')
                docdata.remove_shape(u'id="15" name="Oval 15"')
                docdata.remove_shape(u'id="16" name="Oval 16"')
                context['nk_0065'] = ''
            elif thunhap.income_japan == u'Chuyển nhượng':
                docdata.remove_shape(u'id="8" name="Oval 8"')
                docdata.remove_shape(u'id="9" name="Oval 9"')
                docdata.remove_shape(u'id="12" name="Oval 12"')
                docdata.remove_shape(u'id="16" name="Oval 16"')
                context['nk_0065'] = ''
            elif thunhap.income_japan == u'Ngoài các nguồn nếu trên':
                docdata.remove_shape(u'id="8" name="Oval 8"')
                docdata.remove_shape(u'id="9" name="Oval 9"')
                docdata.remove_shape(u'id="12" name="Oval 12"')
                docdata.remove_shape(u'id="15" name="Oval 15"')
                context['nk_0065'] = kiemtra(thunhap.addition_above_sources)
            else:
                docdata.remove_shape(u'id="8" name="Oval 8"')
                docdata.remove_shape(u'id="9" name="Oval 9"')
                docdata.remove_shape(u'id="12" name="Oval 12"')
                docdata.remove_shape(u'id="15" name="Oval 15"')
                docdata.remove_shape(u'id="16" name="Oval 16"')
                context['nk_0065'] = ''

            if thunhap.brith_day_tax == u'Taishou':
                docdata.remove_shape(u'id="4" name="Oval 4"')
                docdata.remove_shape(u'id="5" name="Oval 5"')
                docdata.remove_shape(u'id="11" name="Oval 11"')
            elif thunhap.brith_day_tax == u'Shouwa':
                docdata.remove_shape(u'id="3" name="Oval 3"')
                docdata.remove_shape(u'id="5" name="Oval 5"')
                docdata.remove_shape(u'id="11" name="Oval 11"')
            elif thunhap.brith_day_tax == u'Heisei':
                docdata.remove_shape(u'id="3" name="Oval 3"')
                docdata.remove_shape(u'id="4" name="Oval 4"')
                docdata.remove_shape(u'id="11" name="Oval 11"')
            elif thunhap.brith_day_tax == u'Reiwa':
                docdata.remove_shape(u'id="3" name="Oval 3"')
                docdata.remove_shape(u'id="4" name="Oval 4"')
                docdata.remove_shape(u'id="5" name="Oval 5"')
            else:
                docdata.remove_shape(u'id="3" name="Oval 3"')
                docdata.remove_shape(u'id="4" name="Oval 4"')
                docdata.remove_shape(u'id="5" name="Oval 5"')
                docdata.remove_shape(u'id="11" name="Oval 11"')


            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)

            tempFile.flush()
            tempFile.close()
            return tempFile
        return None