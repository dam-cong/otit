# -*- coding: utf-8 -*-
{
    'name': "Thuế",
    'depends': ['base', 'donhang'],
    'data': [
        'security/ir.model.access.csv',
        'views/luonghuu.xml',
        'views/uynhiem.xml',
        'views/thunhap.xml',
    ],
}
