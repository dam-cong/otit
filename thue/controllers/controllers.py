# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception, content_disposition
from io import BytesIO, StringIO
import os
import zipfile
import logging

_logger = logging.getLogger(__name__)


class Luonghuu(http.Controller):
    @http.route('/luonghuu/download/<string:id>', type='http', auth="public")
    def luonghuu(self, id=False, **kw):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        file_maps = {}
        luonghuu = request.env['thue.luonghuu'].browse(int(id))
        invoice = request.env['thue.luonghuu']
        doc_luonghuu = invoice.baocao_luonghuu(luonghuu)

        file_maps.update({u'Lương hưu.docx': doc_luonghuu.name})

        for key in file_maps:
            archive.write(file_maps[key], key)
            os.unlink(file_maps[key])

        archive.close()
        reponds.flush()
        ret_zip = reponds.getvalue()
        reponds.close()

        doc_luonghuu.name = 'LUONGHUU.zip'
        return request.make_response(ret_zip,
                                     [('Content-Type', 'application/zip'),
                                      ('Content-Disposition', content_disposition(doc_luonghuu.name))])

    @http.route('/thunhap/download/<string:id>', type='http', auth="public")
    def thunhap(self, id=False, **kw):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        file_maps = {}
        thunhap = request.env['thue.thunhap'].browse(int(id))
        invoice = request.env['thue.thunhap']
        doc_thunhap = invoice.baocao_thunhap(thunhap)

        file_maps.update({u'Thu nhập.docx': doc_thunhap.name})

        for key in file_maps:
            archive.write(file_maps[key], key)
            os.unlink(file_maps[key])

        archive.close()
        reponds.flush()
        ret_zip = reponds.getvalue()
        reponds.close()

        doc_thunhap.name = 'THUNHAP.zip'
        return request.make_response(ret_zip,
                                     [('Content-Type', 'application/zip'),
                                      ('Content-Disposition', content_disposition(doc_thunhap.name))])

    @http.route('/uynhiem/download/<string:id>', type='http', auth="public")
    def uynhiem(self, id=False, **kw):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        file_maps = {}
        uynhiem = request.env['thue.uynhiem'].browse(int(id))
        invoice = request.env['thue.uynhiem']
        doc_uynhiem = invoice.baocao_uynhiem(uynhiem)
        file_maps.update({u'Ủy nhiệm.docx': doc_uynhiem.name})

        for key in file_maps:
            archive.write(file_maps[key], key)
            os.unlink(file_maps[key])

        archive.close()
        reponds.flush()
        ret_zip = reponds.getvalue()
        reponds.close()

        doc_uynhiem.name = 'UYNHIEM.zip'
        return request.make_response(ret_zip,
                                     [('Content-Type', 'application/zip'),
                                      ('Content-Disposition', content_disposition(doc_uynhiem.name))])