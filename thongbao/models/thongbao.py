# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ThongBao(models.Model):
    _name = 'thongbao.lich'
    _rec_name = 'coso_kansa'

    coso_kansa = fields.Char(string='Cơ sở giám sát')
    xinghiep_thongbao = fields.Char(string='Xí nghiệp')
    diachixinghiep_thongbao = fields.Char(string='Địa chỉ xí nghiệp')

    res_model = fields.Char(string='Model', readonly=True)
    res_id = fields.Integer(string='Record ID', readonly=True)
    start_date = fields.Datetime(string='Ngày bắt đầu')
    end_date = fields.Datetime(string='Ngày kết thúc')

    # @api.multi
    # def call_action(self):
    #     # imd = self.env['ir.model.data']
    #     imd = self.env['ir.ui.view']
    #     ctx = {}
    #     form_view_id = imd.xmlid_to_res_id('thongbao.view_thongbao_lich_form')
    #     if form_view_id:
    #         result = {
    #             'type': 'ir.actions.act_window',
    #             'view_type': 'form',
    #             'view_mode': 'form',
    #             'res_model': '',
    #             'views': [(form_view_id, 'form')],
    #             'view_id': form_view_id,
    #             'res_id': '',
    #             'target': 'current',
    #             'context': ctx,
    #         }
    #         return result
