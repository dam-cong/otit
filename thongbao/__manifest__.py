# -*- coding: utf-8 -*-
{
    'name': "Thông báo",
    'depends': ['base'],
    'data': [
        'security/ir.model.access.csv',
        'views/thongbao.xml',
    ],
    'installable': True,
    'application': True,
}
