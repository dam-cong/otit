# -*- coding: utf-8 -*-

{
    'name': "Debranding Kit",
    'version': '12.0.0',
    'author': 'Planet-Odoo',
    "support": "http://www.planet-odoo.com/",
    'category': 'Tools',
    'depends': [
        'base',
        'web',
        'mail',
        'web_settings_dashboard',
        'portal',

    ],
    'data': [
        'views/data.xml',
        'views/views.xml',
        'views/js.xml',
        'views/webclient_templates.xml',
        'views/change_menu_color.xml',
        'security/ir.model.access.csv',

    ],
    'images': ['static/description/main.png'],
    'license': "LGPL-3",
    'auto_install': False,
    'installable': True
}
